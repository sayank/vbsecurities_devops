import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchSecurityComponent } from './search-security.component';

describe('SearchSecurityComponent', () => {
  let component: SearchSecurityComponent;
  let fixture: ComponentFixture<SearchSecurityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchSecurityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchSecurityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
