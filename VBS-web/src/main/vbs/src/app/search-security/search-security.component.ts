import {AfterViewChecked, ChangeDetectorRef, Component, OnInit, Renderer2, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SecurityService} from '../_services/security.service';
import {catchError, debounceTime, distinctUntilChanged, merge, switchMap, tap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {SecuritySearchParameters} from '../_models/securitySearchParameters';
import {Branch} from '../_models/branch';
import {Session} from '../_session/session';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FinancialInstitutionSearch} from '../_components/financialInstitutionSearch/financial-institution-search.component';
import {NameDescription} from '../_models/nameDescription';
import {SecuritySearchResultWrapper} from '../_models/securitySearchResultWrapper';
import {EscrowAgentSearchComponent} from '../_components/escrow-agent-search/escrow-agent-search.component';
import {ContactsService} from "../_services/contacts.service";
import {FinancialInstitutionService} from '../_services/financial-institution.service';
import {FinancialInstitutionType} from '../_models/financialInstitutionType';
import {Md5} from 'ts-md5';
import { PoolSecuritiesParameters } from '../_models/poolSecuritiesParameters';
import { HostListener } from "@angular/core";
import { SearchResultEnrolment } from '../_models/searchResultEnrolment';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'app-search-security',
  templateUrl: './search-security.component.html',
  styleUrls: ['./search-security.component.css'],
  providers: [SecurityService]
})
export class SearchSecurityComponent implements OnInit, AfterViewChecked {
	form: FormGroup;
	formResults: FormGroup;
	types: NameDescription[] = [];
	financialInstitutionTypes: FinancialInstitutionType[] = [];
	purposes: NameDescription[] = [];
	searching = false;
	searchingForm = false;
	searchParams: SecuritySearchParameters;
	results: SecuritySearchResultWrapper;
	searched: boolean = false;
	searchingResults: string;
	hideSearchingWhenUnsubscribed = new Observable(() => () => this.searching = false);
	branches: Branch[] = [];
	crmVbUrl: string = '';
	downloadingExcel: boolean = false;
	showInterest: boolean = false;
	securityPooled: boolean = false;
	poolSecurityModalForm = null;
	poolSecurityToBlanket = false;
	selected = [];
	poolingSecurities = false;
	poolingErrorMessages: any[];

    width = 0;

    @ViewChild('datatable') table: any;
	@ViewChild('completePoolModal') completePoolModal: any;


	constructor(private securityService: SecurityService, private session: Session, private contactsService: ContactsService,
		private fb: FormBuilder, private router: Router, private route: ActivatedRoute, private modalService: NgbModal,
		private renderer2: Renderer2, private financialInstitutionService: FinancialInstitutionService,
		private cdRef:ChangeDetectorRef,
		private sanitizer: DomSanitizer) {

	    this.types = this.session.getProperty("securityTypes");
        this.purposes = this.session.getProperty("securityPurposes");
        this.financialInstitutionTypes = this.session.getProperty("financialInstitutionTypes");
        this.crmVbUrl = this.session.getProperty("crmCompanyUrl");

        this.width = window.innerWidth;

	}

	instrumentNumberTypeahead = (text$: Observable<string>) =>
	text$.pipe(
		debounceTime(300),
		distinctUntilChanged(),
		tap(() => this.searching = true),
		switchMap(term => term.length <= 2 ? [] : this.securityService.instrumentNumberTypeahead(term)),
		catchError(() => {
			return of([]);
		}),
		tap(() => this.searching = false),
		merge(this.hideSearchingWhenUnsubscribed)
	);

	typeahead = (text$: Observable<string>) =>
	text$.pipe(
		debounceTime(300),
		distinctUntilChanged(),
		tap(() => this.searching = true),
		switchMap(term => term.length <= 2 ? [] : this.securityService.vbNumberTypeahead(term)),
		catchError(() => {
			return of([]);
		}),
		tap(() => this.searching = false),
		merge(this.hideSearchingWhenUnsubscribed)
	);

	typeaheadFinancial = (text$: Observable<string>) =>
	text$.pipe(
		debounceTime(300),
		distinctUntilChanged(),
		tap(() => this.searching = true),
		switchMap(term => ((term.length > 0 && !isNaN(Number(term))) || term.length > 2) ? this.financialInstitutionService.financialInstitutionTypeahead(term) : []),
		catchError(() => {
			return of([]);
		}),
		tap(() => this.searching = false),
		merge(this.hideSearchingWhenUnsubscribed)
	);

	financialInstitutionFormatter = (x: {id:number, name: string}) => {
		if(x.id !== undefined){
			return (x.id != null) ? x.id + ' - ' + x.name : '';
		}
		return x;
	}

	typeaheadEscrow = (text$: Observable<string>) =>
	text$.pipe(
		debounceTime(300),
		distinctUntilChanged(),
		tap(() => this.searching = true),
		switchMap(term => ((term.length > 0 && !isNaN(Number(term))) || term.length > 2) ? this.contactsService.escrowAgentTypeahead(term) : []),
		catchError(() => {
			return of([]);
		}),
		tap(() => this.searching = false),
		merge(this.hideSearchingWhenUnsubscribed)
	);

  contactFormatter = (x: {crmContactId:number, companyName: string}) => {
    if(x.crmContactId !== undefined){
      return (x.crmContactId != null) ? x.crmContactId + ((x.companyName != null) ? ' - ' + x.companyName : '') : '';
    }
    return x;
  }

	formKeyDown(event) {
		if(event.keyCode == 13) {
			this.search();
		}
	}

	search() {
		this.session.setSearchParams(this.searchParams);

		this.cleanFields();

		if(this.form.value.financialInstitutionId != null && this.form.value.financialInstitutionId != undefined && this.form.value.financialInstitutionId.id != undefined) {
			this.searchParams.financialInstitutionId = this.form.value.financialInstitutionId.id;
		}


        this.searched = false;
        this.results = null;
        this.searchingForm = true;
        this.downloadingExcel = false;
        this.cleanFields();

        this.securityService.searchSecurity(this.searchParams).subscribe(
            data => {
                this.showInterest = false;
                this.results = data;
                this.searched = true;
                this.searchingForm = false;
                this.session.storeLatestSecuritySeach(this.searchParams, this.results);
                if(this.results.securities.length == 1){
                    this.loadSecurity("", this.results.securities[0].securityNumber);
                }
            },
            errorResult => {
                if(errorResult.status == 422) {
                    let arr = {};
                    for (let err in errorResult.error.validationErrorList) {
                        if (arr[errorResult.error.validationErrorList[err].errorFieldName] === undefined) { arr[errorResult.error.validationErrorList[err].errorFieldName] = []; }
                        arr[errorResult.error.validationErrorList[err].errorFieldName].push(errorResult.error.validationErrorList[err].errorDescription);
                    }
                    for (let field in arr) {
                        if(field != null && this.form.get(field) != null){
                            this.form.get(field).setErrors({ 'serverErrors': arr[field] });
                        }
                    }

                    this.session.addError('Form failed validation.', errorResult);
                    this.searchingForm = false;
                } else {
                    this.session.addError('Error when Searching Securities', errorResult);
                    this.searchingForm = false;
                }
            }
        );

  	}

	private cleanFields() {
		let vb = (this.searchParams.vbNumber == null || this.searchParams.vbNumber.crmContactId === undefined) ? this.searchParams.vbNumber : this.searchParams.vbNumber.crmContactId;
		this.searchParams.vbNumber = vb;

		let financial = (this.searchParams.financialInstitutionId == null || this.searchParams.financialInstitutionId.id === undefined) ? this.searchParams.financialInstitutionId : this.searchParams.financialInstitutionId.id;
		this.searchParams.financialInstitutionId = financial;

		let escrow = (this.searchParams.escrowAgentContactId == null || this.searchParams.escrowAgentContactId.crmContactId === undefined) ? this.searchParams.escrowAgentContactId : this.searchParams.escrowAgentContactId.crmContactId;
		this.searchParams.escrowAgentContactId = escrow;
	}
    private showPooling(): boolean {
        return !this.session.getUser().hasPermission("CREATE_DEMAND_FINANCE") && !this.session.getUser().hasNoPermissions();
    }
  private loadBranches(financial) {
		if(financial != null){
			this.branches = []; let id = 0;
			if(financial.id !== undefined) {
				id = financial.id;
			}else if(financial.length > 0 && !isNaN(financial)){
				id = financial;
			}
            this.financialInstitutionService.getBranches(id).subscribe(
                branches => this.branches = branches,
                error => this.session.addError('Server error on loading Branches', error));
            if(id != 0){
			}
		} else {
			this.searchParams.branchId = null;
			if(this.form != null && this.form.controls['branchId'] != null) {
				this.form.controls['branchId'].patchValue(null);
			}
		}
	}

	loadSecurity(e: any, securityNumber: number){
		this.router.navigateByUrl("/security?id=" + securityNumber);
	}

	ngOnInit() {

		this.poolSecurityModalForm = this.fb.group({
			poolId: ['', Validators.required],
			unitsIntendedToCover: ['', Validators.required]
		});

		this.searchParams = this.session.getSearchParams();
		if(this.searchParams == null) {
			this.searchParams = new SecuritySearchParameters();
		}

		this.loadBranches(this.searchParams.financialInstitutionId);

		this.form = this.fb.group(this.searchParams);

		let f = this.form.controls['financialInstitutionId'] as FormControl;

		this.form.controls['financialInstitutionId'].valueChanges.pipe(debounceTime(300), distinctUntilChanged()).subscribe(financial => this.loadBranches(financial));
		this.form.valueChanges.subscribe(searchParams => this.searchParams = searchParams);
		this.form.controls['currentAmount'].setValidators(Validators.pattern(/^\d*\.?\d*$/));
		this.form.controls['currentAmountTo'].setValidators(Validators.pattern(/^\d*\.?\d*$/));
		this.form.controls['securityNumber'].setValidators(Validators.min(1));
		this.form.controls['poolId'].setValidators(Validators.min(1));
		this.form.controls['receivedDate'].setValidators(this.validateDate);
		this.form.controls['receivedDateBetweenTo'].setValidators(this.validateDate);
		this.form.setValidators(this.validateForm);

		let el = this.renderer2.selectRootElement('#securityNumberInput');
 		el.focus();

		this.route.queryParams.subscribe((params: Params) => {
			if (params['reset'] !== undefined && params['reset'] == "true") {
				this.session.storeLatestSecuritySeach(null, null);
				this.searchParams = new SecuritySearchParameters();
				this.session.setSearchParams(this.searchParams);
				this.form.patchValue(this.searchParams);
			}

			if (params['siteId'] !== undefined) {
				this.searchParams = new SecuritySearchParameters();
				this.searchParams.enrolmentNumber = params['siteId'];
				this.search();
				this.session.setSearchParams(this.searchParams);
				this.form.patchValue(this.searchParams);
			} else if(params['vbNumber'] !== undefined) {
				this.searchParams = new SecuritySearchParameters();
				this.searchParams.vbNumber = params['vbNumber'];
				this.search();
				this.session.setSearchParams(this.searchParams);
				this.form.patchValue(this.searchParams);
			} else if(params['financialInstitutionId'] !== undefined) {
				this.searchParams = new SecuritySearchParameters();
				this.searchParams.financialInstitutionId = params['financialInstitutionId'];
				this.search();
				this.session.setSearchParams(this.searchParams);
				this.form.patchValue(this.searchParams);

				this.financialInstitutionService.getFinancialInstitution(params['financialInstitutionId']).subscribe(
					financialInstitution => {
						this.form.controls['financialInstitutionId'].setValue(financialInstitution);
					},
					error => this.session.addError('Server error on loading Financial Institution', error)
				);
			}
		});

		let key = Md5.hashStr(JSON.stringify(this.searchParams)).toString();

		if (this.session.getLatestSecuritySearchHash() == key) {
			this.searched = true;
			this.results = this.session.getLatestSecuritySearchResult();
		}
	}

	validateDate(c: FormControl) {
		return (c.value == null || !Number.isNaN(Date.parse(c.value))) ? null : {
			  date: {
				valid: false
			  }
		};
	}

	validateForm(c: AbstractControl){
	  if(c.get('instrumentNumber').value != null && c.get('instrumentNumberSearchType').value == 'IN'){
	    let regxp = new RegExp("^([A-Z 0-9,-]*)$");
      if(!regxp.test(c.get('instrumentNumber').value)){
        return {instrumentNumber: { inRegXP: false } }
      }
    }
	  return null;
  }

	exportToExcel() {
		this.downloadingExcel = true;

        if(this.form.value.financialInstitutionId != null && this.form.value.financialInstitutionId != undefined && this.form.value.financialInstitutionId.id != undefined) {
            this.searchParams.financialInstitutionId = this.form.value.financialInstitutionId.id;
        }

		this.securityService.searchResultsExcel(this.searchParams, this.showInterest).subscribe(data => {
			const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
			let d = new Date();
			const element = document.createElement('a');
			element.href = URL.createObjectURL(blob);
			element.download = "security-search-results-"+d.getTime()+".xlsx";
			document.body.appendChild(element);
			element.click();
			element.remove();
		},
		error => {},
		() => {this.downloadingExcel = false;});
	}

	clear(){
		this.searchParams = new SecuritySearchParameters();
		this.form.patchValue(this.searchParams);
		this.results = null;
		this.searched = false;
		this.session.setSearchParams(this.searchParams);
		this.downloadingExcel = false;
		this.branches = [];
		this.router.navigate([], { replaceUrl: true});
	}

	textForSearchType(t: string): string{
		if(t == 'EQUALS'){
			return " = ";
		}else if(t == 'LESS_THAN'){
			return " < ";
		}else if(t == 'GREATER_THAN'){
			return " > ";
    }else if(t == 'EQUALS_LESS_THAN'){
      return " <= ";
    }else if(t == 'EQUALS_GREATER_THAN'){
      return " >= ";
		}else if(t == 'IN'){
			return "In";
    }else if(t == 'CONTAINS'){
      return "Contains";
    }else if(t == 'BEGIN_WITH'){
      return "Begins With";
    }else if(t == 'BETWEEN'){
      return "Between";
		}else{
			return '';
		}
	}

	showSearchModal(){
		let modalRef = this.modalService.open(FinancialInstitutionSearch, {size: 'lg'});
		modalRef.componentInstance.selected.subscribe(s => {
			this.form.controls['financialInstitutionId'].setValue(s);
		});
	}

	compareDate(date1: Date, date2: Date): number {
		let d1 = new Date(date1); let d2 = new Date(date2);
		if(d1.getTime() < d2.getTime()){
			return -1;
		}else if(d1.getTime() > d2.getTime()){
			return 1;
		}else{
			return 0;
		}
	}

	onPage(){
		let top = document.getElementsByTagName('datatable-header')[0] as HTMLElement;
		top.scrollIntoView();
	}

	showEscrowSearchModal(){
		let modalRef = this.modalService.open(EscrowAgentSearchComponent, {size: 'lg'});
		modalRef.componentInstance.selected.subscribe(s => {
			this.searchParams.escrowAgentContactId = s
			this.form.controls['escrowAgentContactId'].patchValue(s, {emitEvent: false});
		});
	}

  ngAfterViewChecked() {
    if (this.table !== undefined && this.table.rowDetail !== undefined) {
      this.table.rowDetail.expandAllRows();
      this.cdRef.detectChanges();
    }
	}

	onSelect(selection) {
		let rowsWithCurrentAmount = [];
		selection.selected.forEach(function(element, index, object) {
			if (element.currentAmount > 0) {
				rowsWithCurrentAmount.push(element);
			}
		});
		this.selected = rowsWithCurrentAmount;
		this.table.selected = this.selected;
	}

	completePool() {
		this.securityPooled = false;
		this.poolingErrorMessages = [];
		this.modalService.open(this.completePoolModal);		
	}

	doCompletePool() {
		this.poolingSecurities = true;
		let securityIds = [];
		this.selected.forEach(element => {
			securityIds.push(element.securityNumber);
		});

		let params:PoolSecuritiesParameters = new PoolSecuritiesParameters();
		params.securityIds = securityIds;
		params.poolId = this.poolSecurityModalForm.controls['poolId'].value;
		params.unitsIntendedToCover = this.poolSecurityModalForm.controls['unitsIntendedToCover'].value;
		params.securityPurposeId = 2;
		this.securityPooled = true;
		this.securityService.poolSecurities(params).subscribe(
			result => {
				this.poolingErrorMessages = ["Pooling successful."];				
				this.poolingSecurities = false;
				this.selected.forEach(element => {
					let changedSecurity = this.results.securities.find(s => s.securityNumber == element.securityNumber);
					if(changedSecurity != null) {
						changedSecurity.poolId = params.poolId.toString();
						let securityPurpose = this.purposes.find(p => p.id == params.securityPurposeId);
						if(securityPurpose != null) {
							changedSecurity.securityPurposeName = securityPurpose.description;
						}
					}

				});
			},
			error => {
				if (error.error.length > 0) {
					this.poolingErrorMessages = [];
					this.poolingErrorMessages.push("Pooling unsuccessful:");
					error.error.forEach(element => {
						this.poolingErrorMessages.push(element.errorDescription);
					});
				}
				this.poolingSecurities = false;
			}
		);
	}

	displayCheck(row) {
		return row.currentAmount > 0;
	}

	formatEnrollmentNumbers(enrolmentNumbers: SearchResultEnrolment[]): SafeHtml {
		if (!enrolmentNumbers || !enrolmentNumbers.length) {
			return '';
		}

		let formattedEnrolmentNumbers: string = '';
		
		enrolmentNumbers.forEach((enrolmentNumber, index) => {
			formattedEnrolmentNumbers += `${enrolmentNumber.enrolmentNumber}<br /><b>${enrolmentNumber.date}</b>`;
			if (index < enrolmentNumbers.length - 1) {
				formattedEnrolmentNumbers += '<br />';
			}
		});

		return this.sanitizer.bypassSecurityTrustHtml(formattedEnrolmentNumbers);
	}

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.width = event.target.innerWidth;
    }

    showScrollbar() {
      return this.width < 1300;
    }
}
