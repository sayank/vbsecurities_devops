import { Session } from '../_session/session';
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
	constructor(private router: Router, private session: Session) { }

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
	    if ( this.session.getUser() != null && this.session.getUser().userId != null ) {
			let expectedPermission = (route.data.expectedPermission == undefined) ? "" : route.data.expectedPermission;
			if(expectedPermission != "" && !this.session.getUser().hasPermission(expectedPermission)){
				this.router.navigateByUrl('', { queryParams: { returnUrl: state.url }});
				return false;
			}else{
				return true;
			}
		}else{
		    this.router.navigateByUrl(this.router.createUrlTree(['/login'],{queryParams: { 'returnUrl': state.url }}));
			return false;
		}
	}
}
