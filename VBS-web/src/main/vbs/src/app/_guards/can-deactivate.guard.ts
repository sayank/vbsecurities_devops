import { Injectable }    from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable }    from 'rxjs';
import { ModalService } from '../_services/modal.service';
import { map } from 'rxjs/operators';

export interface CanComponentDeactivate {
	canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable({
	providedIn: 'root',
})
export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {

	constructor(private modalService: ModalService) {};

	canDeactivate(component: CanComponentDeactivate): boolean | Observable<boolean> {
		// if there are no pending changes, just allow deactivation; else confirm first
        return component.canDeactivate ? true : this.openConfirmDialog();
	}

  	openConfirmDialog() {
		return this.modalService.confirm({title: "Unsaved changes", body: "There are unsaved changes. Do you want to discard the changes?", size: "lg", allowDismiss: false}).pipe(
			map(confirmation => {
				if(confirmation.result){
					return true;
				}else{
					return false;
				}
		}));
  	}
}
