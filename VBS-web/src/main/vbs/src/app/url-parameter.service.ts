import { Injectable } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { Location } from '@angular/common';

@Injectable({
	providedIn: 'root'
})
export class UrlParameterService {

	constructor(private route: ActivatedRoute, private location: Location, private router: Router) { }

	deleteUrlParameter(param: string) {
		this.setUrlParameter(param, null);
	}

	setUrlParameter(param: string, value: string) {
			
		let queryParams = this.getUrlParams();
		queryParams[param] = value;

		let navigationExtras: NavigationExtras = {
			relativeTo: this.route, 
			queryParams: queryParams
		}
		let tree = this.router.createUrlTree ([], navigationExtras);
		this.location.go(tree.toString());

	}

	getUrlParams() {
		let search = this.location.path();
		let params = {};
		if(search.indexOf('?') == -1) {
			return params;
		}
		let hashes = search.slice(search.indexOf('?') + 1).split('&')
		hashes.forEach(hash => {
			let [key, val] = hash.split('=')
			params[key] = decodeURIComponent(val)
		});
		return params;
	}
}
