import { Component } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { SecurityTypePurpose } from "../_components/securityTypePurpose/security-type-purpose.component";

@Component({
  	selector: 'app-index',
  	templateUrl: './index.component.html',
  	styleUrls: ['./index.component.css']
})
export class IndexComponent {

	constructor(private modalService: NgbModal) {

	}

  	chooseTypePurpose(){
		this.modalService.open(SecurityTypePurpose);
	}

}
