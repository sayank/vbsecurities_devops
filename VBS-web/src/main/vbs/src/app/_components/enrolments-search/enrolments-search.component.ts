import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Enrolment} from '../../_models/enrolment';
import {EnrolmentService} from '../../_services/enrolment.service';
import {Session} from '../../_session/session';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ReleaseService} from '../../_services/release.service';
import {ReleaseEnrolment} from "../../_models/releaseEnrolment";

@Component({
	selector: 'app-enrolments-search',
	templateUrl: './enrolments-search.component.html',
	styleUrls: ['./enrolments-search.component.css']
})
export class EnrolmentsSearchComponent implements OnInit {

	availableEnrolments: Enrolment[] = [];
	filteredEnrolments: Enrolment[] = [];
	selectedEnrolments: ReleaseEnrolment[] = [];
	failedEnrolment: string;
	notCondoConversion: string;
	addingEnrolment: boolean;
	loadingAvailable: boolean = true;

	private poolId: number;
	private onlyCondoConv: boolean = false;
	private securityIdentityTypeId: number;

	@Output() selected = new EventEmitter();

	constructor(private enrolmentService: EnrolmentService, private session: Session, private activeModal: NgbActiveModal, private releaseService: ReleaseService) { }


	ngOnInit() {
		this.failedEnrolment = null;
		this.addingEnrolment = false;
		this.notCondoConversion = null;
	}


	setOptions(poolId:number, onlyCondoConv: boolean, securityIdentityTypeId: number, selectedEnrolments: ReleaseEnrolment[], availableEnrolments: Enrolment[]) {
		this.loadingAvailable = true;
		this.poolId = poolId;
		this.selectedEnrolments = selectedEnrolments;

		if(onlyCondoConv != null) {
			this.onlyCondoConv = onlyCondoConv;
		}
		if(securityIdentityTypeId != null) {
			this.securityIdentityTypeId = securityIdentityTypeId;
		}

		if(availableEnrolments == null || availableEnrolments.length == 0) {
			this.enrolmentService.getAvailableEnrolments(this.poolId, this.onlyCondoConv, this.securityIdentityTypeId).subscribe(
				enrolments => {
					this.availableEnrolments = enrolments;
					if(this.selectedEnrolments.length > 0) {
						this.filterEnrolments('');
					} else {
						this.filteredEnrolments = enrolments;
					}
					this.loadingAvailable = false;
				},
				error => this.session.addError('Unable to search for enrolments', error)
			);
		} else {
			this.availableEnrolments = availableEnrolments;
			this.filterEnrolments('');
			this.loadingAvailable = false;
			this.addingEnrolment = false;
		}
	}

	

	addEnrolment(enrolmentNumber: string) {

		this.failedEnrolment = null;
		this.addingEnrolment = true;
		this.notCondoConversion = null;

		this.enrolmentService.getEnrolment(enrolmentNumber, this.poolId, this.securityIdentityTypeId).subscribe(
			enrolment => {
                this.addingEnrolment = false;
			    if(enrolment == null) {
			    	this.failedEnrolment = enrolmentNumber;
				} else if (!enrolment.condoConversion && this.onlyCondoConv){
					this.notCondoConversion = enrolmentNumber;
				} else {
					this.selected.next(enrolment);
					this.c();
				}
			},
			error => {
				this.session.addError('Server error, unable to add enrolment', error);
				this.addingEnrolment = false;
			}
		);
		return false;
	}

	filterEnrolments(enrolmentNumber: string) {
		if (this.selectedEnrolments.length > 0) {
			this.filteredEnrolments = this.availableEnrolments.filter(e => {
				return this.selectedEnrolments.find(se => se.enrolment != null && se.enrolment.enrolmentNumber == e.enrolmentNumber) == undefined && e.enrolmentNumber !=null && e.enrolmentNumber.includes(enrolmentNumber.toUpperCase())
			});
		} else {
			this.filteredEnrolments = this.availableEnrolments.filter(e => e.enrolmentNumber != null && e.enrolmentNumber.includes(enrolmentNumber.toUpperCase()));
		}
	}

	c(){ this.activeModal.close(); }

	d(){ this.activeModal.dismiss(); }

}
