import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrolmentsSearchComponent } from './enrolments-search.component';

describe('EnrolmentsSearchComponent', () => {
  let component: EnrolmentsSearchComponent;
  let fixture: ComponentFixture<EnrolmentsSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrolmentsSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrolmentsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
