import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SecurityTypePurpose } from '../securityTypePurpose/security-type-purpose.component';

@Component({
  selector: 'app-create-security',
  templateUrl: './create-security.component.html',
  styleUrls: ['./create-security.component.css']
})
export class CreateSecurityComponent implements OnInit {
	
	@ViewChild('openModal') openModal:ElementRef;

	constructor(private modalService: NgbModal) { }

	ngOnInit(){
		setTimeout(() => {
			this.openModal.nativeElement.click();
		}, 1);
	}
	openSelect(){
		this.modalService.open(SecurityTypePurpose);
	}

}
