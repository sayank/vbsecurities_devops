import {Component, EventEmitter, Output} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Observable, of} from "rxjs";
import {catchError, debounceTime, distinctUntilChanged, switchMap} from "rxjs/operators";
import {SecurityService} from "../../_services/security.service";
import {Session} from "../../_session/session";
import {ContactsService} from "../../_services/contacts.service";

@Component({
  selector: 'app-search-vb',
  templateUrl: './search-vb.component.html',
  styleUrls: ['./search-vb.component.css']
})
export class SearchVbComponent {

    showProgress:boolean = false;
    selectingVB:boolean = false;
    form:FormGroup = null;

    @Output() selected = new EventEmitter();

    constructor(private activeModal: NgbActiveModal, private securityService:SecurityService,
                private contactService: ContactsService,
                private fb:FormBuilder, private session:Session) {
        this.form = this.fb.group({'vbNumber': null});
    }

    typeahead = (text$: Observable<string>) =>
        text$.pipe(
            debounceTime(300),
            distinctUntilChanged(),
            switchMap(term => term.length <= 2 ? [] : this.securityService.vbNumberTypeahead(term)),
            catchError(() => {
                return of([]);
            }));

    companyContactFormatter = (x: { crmContactId: number, companyName: string }) => {
        if (x.crmContactId !== undefined) {
            return (x.crmContactId != null) ? x.crmContactId + ((x.companyName != null) ? ' - ' + x.companyName : '') : '';
        }
        return x;
    };

    addVb(){
        this.showProgress = true;
        let vb = this.form.get('vbNumber').value;
        if(vb.crmContactId === undefined){
            let v = vb as string;
            vb = 'B'+v.replace('B', '');
            this.contactService.getContactByCrmContactId(vb).subscribe(
                val => {this.selected.next(val);this.activeModal.close();},
                error => { this.session.addError('Server error on loading VB', error); this.showProgress = false; }
            );
        }else{
            this.selected.next(vb);
            this.activeModal.close();
        }
    }

    d(){
        this.activeModal.dismiss();
    }

    get disableAddButton():boolean{
        return (this.showProgress || this.form.get('vbNumber').value =='' || this.form.invalid || this.selectingVB);
    }
}
