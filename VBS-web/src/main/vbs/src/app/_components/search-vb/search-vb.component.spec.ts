import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchVbComponent } from './search-vb.component';

describe('SearchVbComponent', () => {
  let component: SearchVbComponent;
  let fixture: ComponentFixture<SearchVbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchVbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchVbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
