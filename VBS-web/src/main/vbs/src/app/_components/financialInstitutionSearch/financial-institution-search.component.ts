import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, AbstractControl } from '@angular/forms';
import { FinancialInstitution } from 'src/app/_models/financialInstitution';
import { FinancialInstitutionService } from 'src/app/_services/financial-institution.service';
import { FinancialInstitutionType } from 'src/app/_models/financialInstitutionType';
import {Session} from "../../_session/session";

@Component({
  selector: 'financial-institution-search',
  templateUrl: './financial-institution-search.component.html',
  providers: [FinancialInstitutionService]
})
export class FinancialInstitutionSearch implements OnInit {

	@Output() selected = new EventEmitter();
	@Input() onlyActiveSuretyCompanies: boolean = false;

	form: FormGroup;
	searched: boolean = false;
	results: FinancialInstitution[];
	types: FinancialInstitutionType[];
	searching: boolean = false;
	
	constructor(
		private activeModal: NgbActiveModal,
		private financialInstitutionService: FinancialInstitutionService,
        private session: Session
	) {}

	ngOnInit(){
	    this.types = this.session.getProperty("financialInstitutionTypes");

		this.form = new FormGroup({
			'name': new FormControl(null),
			'nameTo': new FormControl(null, ),
			'nameType': new FormControl("BEGINS_WITH"),
			'code': new FormControl(null),
			'codeTo': new FormControl(null),
			'codeType': new FormControl("EQUALS"), 
			'financialInstitutionTypeId': new FormControl(null)
		}, [searchValidator]);

		if(this.onlyActiveSuretyCompanies) {
			this.form.get('financialInstitutionTypeId').disable();
			this.form.get('financialInstitutionTypeId').patchValue(6);
		}
	}

	search(){
		if(this.onlyActiveSuretyCompanies) {
			this.form.get('financialInstitutionTypeId').patchValue(6);
			this.form.value.financialInstitutionTypeId = 6;
		}
		this.searched = false;
		this.results = [];
		this.searching = true;
		this.financialInstitutionService.financialInstitutionSearch(this.form.value).subscribe(results => {this.results = results; this.searched = true; this.searching = false;});
	}

	select(f:FinancialInstitution){
		if(this.onlyActiveSuretyCompanies) {
			if(this.compareDate(new Date(f.expiryDate), new Date()) != -1) {
				this.selected.next(f);
				this.c();
			}
		} else {
			this.selected.next(f);
			this.c();
		}
		
	}

	clear(){
		this.form.reset({
			'name': null,
			'nameTo': null,
			'nameType': "BEGINS_WITH",
			'code': null,
			'codeTo': null,
			'codeType': "EQUALS", 
			'financialInstitutionTypeId': null
		});
		this.results = null;
		this.searched = false;

		if(this.onlyActiveSuretyCompanies) {
			this.form.get('financialInstitutionTypeId').disable();
			this.form.get('financialInstitutionTypeId').patchValue(6);
		}
	}

	c(){ this.activeModal.close(); }

	d(){ this.activeModal.dismiss(); }

	textForSearchType(t: string): string{
		if(t == 'EQUALS'){
			return " = ";
		}else if(t == 'LESS_THAN'){
			return " < ";
		}else if(t == 'GREATER_THAN'){
			return " > ";
		}else if(t == 'BETWEEN'){
			return "Between";
		}else if(t == 'BEGINS_WITH'){
			return 'Begins With';
		}else if(t == 'IN'){
			return ' In ';
		}else if(t == 'NOT_EQUALS'){
			return ' != ';
		}else if(t == 'CONTAINS'){
			return 'Contains';
		}else{
			return '';
		}
	}

	compareDate(date1: Date, date2: Date): number {
		let d1 = new Date(date1); let d2 = new Date(date2);
		if(d1.getTime() < d2.getTime()){
			return -1;
		}else if(d1.getTime() > d2.getTime()){
			return 1;
		}else{
			return 0;
		}
	}

	fiClass(date: Date){
		return !(this.compareDate(date, new Date()) != -1) ? "expired" : "";
	}


	getFinancialInstitutionTypeName(id: number) {
		let financialInstitutionType = this.types.find(t => t.id === id);
		if(financialInstitutionType) {
			return financialInstitutionType.name;
		} else {
			return '';
		}
	}
}

export const searchValidator = (control: AbstractControl): {[key: string]: boolean} => {
	if(control.get('codeType').value == 'BETWEEN'){
		let code = control.get('code').value;
		let codeTo = control.get('codeTo').value;
		if(code != null && code.length > 0){
			if(isNaN(code) || codeTo == null || codeTo.length == 0 || isNaN(codeTo)){
				return {betweenNotComplete: true};
			}
		}
		if(codeTo != null && codeTo.length > 0){
			if(isNaN(codeTo) || code == null || code.length == 0 || isNaN(code)){
				return {betweenNotComplete: true};
			}
		}	
	} else if(control.get('codeType').value == 'IN'){
		let regxp = new RegExp("^([0-9,]*)$");
		if(!regxp.test(control.get('code').value)){
			return {code: true};
		}
	} else if(control.get('code').value != null && isNaN(control.get('code').value)){
		return {codeTo: true};
	}
	if(control.get('nameType').value == 'BETWEEN'){
		if(control.get('name').value == null || control.get('name').value.trim().length == 0){
			return {name: true};
		}
		if(control.get('nameTo').value == null || control.get('nameTo').value.trim().length == 0){
			return {nameTo: true};
		}
	}
	return null;
};
