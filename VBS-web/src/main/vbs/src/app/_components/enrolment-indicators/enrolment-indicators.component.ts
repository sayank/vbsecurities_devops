import {Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {EnrolmentService} from "../../_services/enrolment.service";
import {EnrolmentActivity} from "../../_models/enrolmentActivity";

@Component({
  selector: 'app-enrolment-indicators',
  templateUrl: './enrolment-indicators.component.html',
  styleUrls: ['./enrolment-indicators.component.css']
})
export class EnrolmentIndicatorsComponent {

    form:FormGroup = null;
    externalGroup:FormGroup = null;
    activities:EnrolmentActivity[] = null;

    @ViewChild("loadButton") loadButton;

    constructor(private activeModal: NgbActiveModal, private fb: FormBuilder, private enrolmentService: EnrolmentService) { }

    setFormGroup(formGroup:FormGroup) {
        this.externalGroup = formGroup;
        this.form = this.fb.group(this.externalGroup.getRawValue());
        if(this.form.get('excessDeposit').value==null) {
            this.form.get('excessDeposit').patchValue(false);
            this.form.get('excessDepositRelease').patchValue(false);
        }
		this.form.get('excessDeposit').valueChanges.subscribe(v => {
            this.form.get('excessDepositRelease').patchValue(this.form.get('excessDeposit').value);
        });
        this.form.get('excessDepositRelease').disable();
    }

    save(){
        this.externalGroup.patchValue(this.form.getRawValue());
        this.c();
    }

    loadEnrolmentActivities(){
        this.loadButton.nativeElement.disabled = true;
        this.loadButton.nativeElement.innerHTML = 'Loading...';
        this.enrolmentService.getEnrolmentActivities(this.form.get('enrolmentNumber').value).subscribe(
            activities => this.activities = activities,
            error => {
                this.loadButton.nativeElement.classList.add("btn-danger");
                this.loadButton.nativeElement.classList.remove("btn-info");
                this.loadButton.nativeElement.innerHTML = 'Failed to Load Activities';
            });
    }

    c(){ this.activeModal.close(); }

    d(){ this.activeModal.dismiss(); }

}
