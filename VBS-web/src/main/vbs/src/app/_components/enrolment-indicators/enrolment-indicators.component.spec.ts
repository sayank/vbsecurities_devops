import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrolmentIndicatorsComponent } from './enrolment-indicators.component';

describe('EnrolmentIndicatorsComponent', () => {
  let component: EnrolmentIndicatorsComponent;
  let fixture: ComponentFixture<EnrolmentIndicatorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrolmentIndicatorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrolmentIndicatorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
