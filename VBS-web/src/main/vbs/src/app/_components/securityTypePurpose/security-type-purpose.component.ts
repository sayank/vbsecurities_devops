import { Component, OnInit } from '@angular/core';
import { SecurityService } from 'src/app/_services/security.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Session } from 'src/app/_session/session';
import { Security } from 'src/app/_models/security';
import { Router } from '@angular/router';
import { NameDescription } from '../../_models/nameDescription';


@Component({
  selector: 'security-type-purpose',
  templateUrl: './security-type-purpose.component.html'
})
export class SecurityTypePurpose implements OnInit {

	form: FormGroup;
	securityTypes: NameDescription[] = [];
	securityPurposes: NameDescription[] = [];
	identityTypes: NameDescription[] = [];
	allSecurityPurposes: NameDescription[] = [];

	constructor(
		private session: Session, 
		private fb: FormBuilder, 
		private securityService: SecurityService,
		private activeModal: NgbActiveModal,
		private router: Router){

		this.form = this.fb.group({
			'securityTypeId': new FormControl('', Validators.required),
			'securityPurposeId': new FormControl('', Validators.required),
			'identityTypeId': new FormControl('')
		});

	}

	ngOnInit(){
	    this.securityTypes = this.session.getProperty("securityTypes");
		this.form.controls['securityTypeId'].setValue(this.securityTypes[0].id);
        this.form.controls['securityTypeId'].valueChanges.subscribe(v => {
            //If Pre-existing elements fund (5) is type, the only valid purpose is condo conversion (3)
			if(v == 5) {
				this.securityPurposes = this.securityPurposes.filter(s => s.id == 3);
				this.form.controls['securityPurposeId'].setValue(3, {emitEvent: false});
				} else if(this.securityPurposes.length == 1) {
				this.securityPurposes = this.allSecurityPurposes;
				}
				this.form.controls['securityPurposeId'].setValue(this.securityPurposes[0].id);
		});


		this.securityPurposes = this.session.getProperty("securityPurposes");
        this.allSecurityPurposes = this.session.getProperty("securityPurposes");
		this.form.controls['securityPurposeId'].setValue(this.securityPurposes[0].id);

        this.identityTypes = this.session.getProperty("identityTypes");
		this.form.controls['identityTypeId'].setValue(this.identityTypes[0].id);
	}

	submitForm(form){
		let sec = new Security();
		sec.securityPurposeId = form.form.value.securityPurposeId;
		sec.securityTypeId = form.form.value.securityTypeId;
		sec.identityTypeId = form.form.value.identityTypeId;

		if(sec.securityTypeId == 3){
			sec.originalAmount = 0;
			sec.currentAmount = 0;
		}

		this.session.setSecurity(sec);
		this.session.setCreateSecurity(sec);

		this.c();
		this.router.routeReuseStrategy.shouldReuseRoute = function(){
			return false;
		};
		this.router.navigateByUrl('/security').then(val => {
			if(val === null){
				//no nav occurred, thus navigating from same url.
			}
		});
		
	}

	c(){ this.activeModal.close(); }

	d(){ this.activeModal.dismiss(); }
}
