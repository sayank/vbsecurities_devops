import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstrumentNumberSearchComponent } from './instrument-number-search.component';

describe('InstrumentNumberSearchComponent', () => {
  let component: InstrumentNumberSearchComponent;
  let fixture: ComponentFixture<InstrumentNumberSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstrumentNumberSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstrumentNumberSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
