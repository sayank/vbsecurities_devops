import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SecurityService} from 'src/app/_services/security.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Security} from '../../_models/security';
import {SecuritySearchParameters} from '../../_models/securitySearchParameters';
import {Session} from '../../_session/session';
import {SecuritySearchResultWrapper} from '../../_models/securitySearchResultWrapper';

@Component({
  selector: 'app-instrument-number-search',
  templateUrl: './instrument-number-search.component.html',
  styleUrls: ['./instrument-number-search.component.css']
})
export class InstrumentNumberSearchComponent implements OnInit {

  
	@Output() selected = new EventEmitter();

	form: FormGroup;
	searched: boolean = false;
	results: SecuritySearchResultWrapper;
	searchingForm: boolean = false;
	searchParams: SecuritySearchParameters;

	
	constructor(
		private activeModal: NgbActiveModal,
		private securityService: SecurityService, 
		private session: Session,		 
		private fb: FormBuilder
	) {}

	ngOnInit(){
		this.searchParams = new SecuritySearchParameters();
		
		this.form = this.fb.group(this.searchParams);
		this.form.valueChanges.subscribe(searchParams => this.searchParams = searchParams);
		this.form.controls['securityNumber'].setValidators(Validators.min(1));
		this.form.controls['receivedDate'].setValidators(this.validateDate);
		this.form.controls['receivedDateBetweenTo'].setValidators(this.validateDate);
		
		this.searchParams.showRecentSecurities = true;
		this.search();
	}
	validateDate(c: FormControl) {	
		return (c.value == null || !Number.isNaN(Date.parse(c.value))) ? null : {
			  date: {
				valid: false
			  }
		};
	}
	currentAmountZero(currentAmount: number){
		return currentAmount == 0;
	}
	search() {
        this.searched = false;
        this.results = null;
        this.searchingForm = true;

        this.securityService.searchSecurity(this.searchParams).subscribe(
            data => {
                this.results = data;
                this.searched = true;
                this.searchingForm = false;
                this.searchParams.showRecentSecurities = false;
            },
            errorResult => {
                if(errorResult.status == 422){
                    let arr = {};
                    for (let err in errorResult.error) {
                        if (arr[errorResult.error[err].errorFieldName] === undefined) { arr[errorResult.error[err].errorFieldName] = []; }
                        arr[errorResult.error[err].errorFieldName].push(errorResult.error[err].errorDescription);
                    }
                    for (let field in arr) {
                        if(field != null && this.form.get(field) != null){
                            this.form.get(field).setErrors({ 'serverErrors': arr[field] });
                        }
                    }
                }else {
                    this.session.addError('Error when Searching Securities', errorResult);
                }
                this.searchingForm = false;
            }
        );

  	}
	select(f:Security){
		this.selected.next(f);
		this.c();
	}

	clear(){
		this.form.reset({
			'instrumentNumberSearchType': 'EQUALS', 
			'instrumentNumber': null, 
			'instrumentNumberTo': null, 
			'securityNumberSearchType': 'EQUALS', 
			'securityNumber': null, 
			'securityNumberTo': null, 
			'vbNumberType': 'EQUALS', 
			'vbNumber': null, 
			'receivedDateType': 'EQUALS', 
			'receivedDate': null, 
			'receivedDateBetweenTo': null, 
			'currentAmountType': 'EQUALS', 
			'currentAmount': null, 
			'currentAmountTo': null
		});
		this.results = null;
		this.searched = false;		
	}

	c(){ this.activeModal.close(); }

	d(){ this.activeModal.dismiss(); }

	textForSearchType(t: string): string{
		if(t == 'EQUALS'){
			return " = ";
		}else if(t == 'LESS_THAN'){
			return " < ";
		}else if(t == 'EQUALS_LESS_THAN'){
			return " =< ";
		}else if(t == 'GREATER_THAN'){
			return " > ";
		}else if(t == 'EQUALS_GREATER_THAN'){
			return " => ";
		}else if(t == 'BETWEEN'){
			return "Between";
		}else if(t == 'BEGIN_WITH'){
			return 'Begins With';
		}else if(t == 'IN'){
			return ' In ';
		}else if(t == 'NOT_EQUALS'){
			return ' != ';
		}else if(t == 'CONTAINS'){
			return 'Contains';
		}else{
			return '';
		}
	}

	compareDate(date1: Date, date2: Date): number {
		let d1 = new Date(date1); let d2 = new Date(date2);
		if(d1.getTime() < d2.getTime()){
			return -1;
		}else if(d1.getTime() > d2.getTime()){
			return 1;
		}else{
			return 0;
		}
	}

	compareToday(date: Date){
		return (this.compareDate(date, new Date()) != -1);
	}

	onPage(){
		let top = document.getElementsByClassName('modal-content')[0] as HTMLElement;
		top.scrollIntoView();
	}
}

export const searchValidator = (control: AbstractControl): {[key: string]: boolean} => {
	if(control.get('codeType').value == 'BETWEEN'){
		let code = control.get('code').value;
		let codeTo = control.get('codeTo').value;
		if(code != null && code.length > 0){
			if(isNaN(code) || codeTo == null || codeTo.length == 0 || isNaN(codeTo)){
				return {betweenNotComplete: true};
			}
		}
		if(codeTo != null && codeTo.length > 0){
			if(isNaN(codeTo) || code == null || code.length == 0 || isNaN(code)){
				return {betweenNotComplete: true};
			}
		}	
	} else if(control.get('codeType').value == 'IN'){
		let regxp = new RegExp("^([0-9,]*)$");
		if(!regxp.test(control.get('code').value)){
			return {code: true};
		}
	} else if(control.get('code').value != null && isNaN(control.get('code').value)){
		return {codeTo: true};
	}
	if(control.get('nameType').value == 'BETWEEN'){
		if(control.get('name').value == null || control.get('name').value.trim().length == 0){
			return {name: true};
		}
		if(control.get('nameTo').value == null || control.get('nameTo').value.trim().length == 0){
			return {nameTo: true};
		}
	}
	return null;

}
