import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NameDescription} from '../../_models/nameDescription';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ReleaseService} from '../../_services/release.service';
import {Session} from '../../_session/session';

@Component({
  selector: 'app-release-reason',
  templateUrl: './release-reason.component.html',
  styleUrls: ['./release-reason.component.css']
})
export class ReleaseReasonComponent implements OnInit {

    @Output() selected = new EventEmitter();
    @Input() existingReasons: NameDescription[] = [];

    form: FormGroup;
    showReasonsProgress: boolean = true;

    constructor(private activeModal: NgbActiveModal,
    private session: Session, private releaseService: ReleaseService, private fb: FormBuilder) { }

    ngOnInit() {
        let reasonsArray = this.fb.array([]);
        this.session.getProperty("releaseReasons").forEach(reason => {
            if (!(this.existingReasons.find(r => r.id == reason.id) != undefined)) {
                reasonsArray.controls.push(this.createFormGroup(reason));
            }
        });
        this.form = this.fb.group({'reasons': reasonsArray});
        this.showReasonsProgress = false;
    }

    createFormGroup(reason:NameDescription){
        return new FormGroup({id: new FormControl(reason.id), name: new FormControl(reason.name),
            description: new FormControl(reason.description), selected: new FormControl(false)});
    }

    noReasonsSelected(){
        let fa = this.form.get('reasons') as FormArray;
        for(let i=0;i<fa.controls.length;i++){
            if((fa.controls[i] as FormGroup).value.selected){return false;}
        }
        return true;
    }

    c(){ this.activeModal.close(); }

    save(){
        let fa = this.form.get('reasons') as FormArray;
        let ret:FormGroup[] = [];
        for(let i=0;i<fa.controls.length;i++){
            let g = fa.controls[i] as FormGroup;
            if(g.value.selected){
                ret.push(new FormGroup({id: new FormControl(g.value.id), name: new FormControl(g.value.name), description: new FormControl(g.value.description)}));
            }
        }
        this.selected.next(ret);
        this.c();
    }
}
