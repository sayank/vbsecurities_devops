import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReleaseReasonComponent } from './release-reason.component';

describe('ReleaseReasonComponent', () => {
  let component: ReleaseReasonComponent;
  let fixture: ComponentFixture<ReleaseReasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReleaseReasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReleaseReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
