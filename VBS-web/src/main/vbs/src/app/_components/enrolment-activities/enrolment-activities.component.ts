import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {EnrolmentPoolActivity} from "../../_models/enrolmentPoolActivity";
import {SecurityService} from "../../_services/security.service";
import {Session} from "../../_session/session";

@Component({
    selector: 'app-enrolment-activities',
    templateUrl: './enrolment-activities.component.html',
    styleUrls: ['./enrolment-activities.component.css']
})
export class EnrolmentActivitiesComponent implements OnInit {

	activities:EnrolmentPoolActivity[] = null;
	vbActivities:EnrolmentPoolActivity[] = null;
    securityId: number;
    poolId: number;
	status: string;
	progressing:boolean = true;

    constructor(private activeModal: NgbActiveModal, private securityService: SecurityService, private session: Session) {
    }

    ngOnInit() {
		this.status = 'Loading'
    }

    setSecurityId(securityId: number) {
        this.securityId = securityId;
        this.securityService.getEnrolmentActivities(securityId).subscribe(
			data => {this.activities = data; this.progressing = false; this.status = 'No Activities have been recorded.'},
			error => this.session.addError("Failed to load deleted enrolments activities", error)			
        );
    }

	setVbPoolId(poolId: number) {
        this.poolId = poolId;
        this.securityService.getDeletedVbActivities(poolId).subscribe(
			data => {this.vbActivities = data; this.status = 'No Activities have been recorded for VBs.'},
			error => this.session.addError("Failed to load deleted VB activities", error)			
        );
	}
	
    dismiss() {
        this.activeModal.dismiss();
    }

}
