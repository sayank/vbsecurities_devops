import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrolmentActivitiesComponent } from './enrolment-activities.component';

describe('EnrolmentActivitiesComponent', () => {
  let component: EnrolmentActivitiesComponent;
  let fixture: ComponentFixture<EnrolmentActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrolmentActivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrolmentActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
