import {Component, Input, OnInit} from '@angular/core';
import {SecurityService} from "../../_services/security.service";
import {FinancialInstitutionMaaPoa} from "../../_models/financialInstitutionMaaPoa";

@Component({
  selector: 'app-maa-details',
  templateUrl: './maa-details.component.html',
  styleUrls: ['./maa-details.component.css']
})
export class MaaDetailsComponent implements OnInit {

    @Input("securityId") securityId: any;

    details: FinancialInstitutionMaaPoa;

    constructor(private securityService:SecurityService) { }

    ngOnInit() {
        this.securityService.getMaaPoaDetails(this.securityId).subscribe(details => {this.details = details;/*console.log(details)*/});
    }

}
