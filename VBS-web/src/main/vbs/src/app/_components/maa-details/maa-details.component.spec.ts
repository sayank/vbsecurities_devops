import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaaDetailsComponent } from './maa-details.component';

describe('MaaDetailsComponent', () => {
  let component: MaaDetailsComponent;
  let fixture: ComponentFixture<MaaDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaaDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
