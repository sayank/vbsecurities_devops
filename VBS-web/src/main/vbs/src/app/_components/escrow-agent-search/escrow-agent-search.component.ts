import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Session} from '../../_session/session';
import {EscrowAgentSearchResultWrapper} from '../../_models/escrowAgentSearchResultWrapper';
import {EscrowAgentSearchParams} from '../../_models/escrowAgentSearchParams';
import {ContactsService} from "../../_services/contacts.service";
import {EscrowAgentSearchResult} from "../../_models/escrowAgentSearchResult";
import {Contact} from "../../_models/contact";

@Component({
  selector: 'app-escrow-agent-search',
  templateUrl: './escrow-agent-search.component.html',
  styleUrls: ['./escrow-agent-search.component.css']
})
export class EscrowAgentSearchComponent implements OnInit {

  
	@Output() selected = new EventEmitter();

	form: FormGroup;
	searched: boolean = false;
	results: EscrowAgentSearchResultWrapper;
	searching: boolean = false;
	searchingForm = false;
	searchParams: EscrowAgentSearchParams;
	securityType:number = null;

	constructor(
		private activeModal: NgbActiveModal,
		private contactsService: ContactsService,
		private session: Session,		 
		private fb: FormBuilder
	) {}

	ngOnInit(){
		this.searchParams = new EscrowAgentSearchParams();
		
		this.form = this.fb.group(this.searchParams);
		this.form.valueChanges.subscribe(searchParams => this.searchParams = searchParams);
	}
	
	search() {
		this.searched = false;
		this.results = null;
		this.searchingForm = true;
	
		this.contactsService.escrowAgentSearch(this.searchParams).subscribe(
			data => {
				this.results = data;
				this.searched = true;
				this.searchingForm = false;
			
			},
			error => {
				this.session.addError('Error when Searching Escrow Agents', error);
				this.searchingForm = false;
			}
		);
  	}
	select(f:EscrowAgentSearchResult){		
		if(this.securityType == null || f.licenceApplicationStatus != 'Only For PEF' || (this.securityType == 5 && f.licenceApplicationStatus == 'Only For PEF')) {
			let c = new Contact();
			c.id = f.id;
			c.crmContactId = f.crmContactId;
			c.companyName = f.escrowAgent;
			c.licenseStatus.description = f.escrowAgentLicenceStatus;
			c.escrowAgentLicenseApplicationStatus.description = f.licenceApplicationStatus;
			this.selected.next(c);
			this.c();
		}
	}

	clear(){
	  this.searchParams = new EscrowAgentSearchParams();
		this.form.patchValue(this.searchParams, {onlySelf: true});
		this.results = null;
		this.searched = false;		
	}

	c(){ this.activeModal.close(); }

	d(){ this.activeModal.dismiss(); }

	textForSearchType(t: string): string{
		if(t == 'EQUALS'){
			return " = ";
		}else if(t == 'LESS_THAN'){
			return " < ";
		}else if(t == 'GREATER_THAN'){
			return " > ";
		}else if(t == 'BETWEEN'){
			return "Between";
		}else if(t == 'BEGIN_WITH'){
			return 'Begins With';
		}else if(t == 'IN'){
			return ' In ';
		}else if(t == 'NOT_EQUAL'){
			return ' != ';
		}else if(t == 'CONTAINS'){
			return 'Contains';
		}else{
			return '';
		}
	}

	static compareDate(date1: Date, date2: Date): number {
		let d1 = new Date(date1); let d2 = new Date(date2);
		if(d1.getTime() < d2.getTime()){
			return -1;
		}else if(d1.getTime() > d2.getTime()){
			return 1;
		}else{
			return 0;
		}
	}

	compareToday(date: Date){
		return (EscrowAgentSearchComponent.compareDate(date, new Date()) != -1);
	}

	eaClass(r: EscrowAgentSearchResult): string {
	    return (r.escrowAgentLicenceStatus != null && r.escrowAgentLicenceStatus != 'Active' || this.securityType != null && this.securityType != 5 && r.licenceApplicationStatus == 'Only For PEF') ? "expired" : "";
    }

}

