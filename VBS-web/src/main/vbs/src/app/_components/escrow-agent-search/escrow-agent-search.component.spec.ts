import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EscrowAgentSearchComponent } from './escrow-agent-search.component';

describe('EscrowAgentSearchComponent', () => {
  let component: EscrowAgentSearchComponent;
  let fixture: ComponentFixture<EscrowAgentSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EscrowAgentSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EscrowAgentSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
