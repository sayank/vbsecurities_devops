import {Component, Inject, OnInit} from '@angular/core';
import {SecurityService} from 'src/app/_services/security.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {MODAL_DATA} from './vbs-modal-config';

@Component({
  selector: 'confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  providers: [SecurityService]
})
export class ConfirmationModal implements OnInit {

	title: string = '';
	body: string = '';
	allowDismiss: boolean = true;
    alertOnly: boolean = false;
	acceptButtonText: string = 'Yes';
	rejectButtonText: string = 'No';

	constructor(private activeModal: NgbActiveModal, @Inject(MODAL_DATA) public data: string[]) {
		this.title = data['title'];
		this.body = data['body'];
		this.allowDismiss = Boolean(data['allowDismiss']);
        if(data['alertOnly'] !== undefined){this.alertOnly = Boolean(data['alertOnly']);}
        if(this.alertOnly){
            this.acceptButtonText = "OK";
        }else{
            if(data['acceptButtonText'] !== undefined){this.acceptButtonText = data['acceptButtonText'];}
            if(data['rejectButtonText'] !== undefined){this.rejectButtonText = data['rejectButtonText'];}
        }
	}

	ngOnInit(){	}

	accept(){
		this.activeModal.close(true);
	}

	reject(){
		this.activeModal.close(false);
	}

	dismiss(){
		this.activeModal.dismiss();
	}
}
