export class VbsModalResult<closed = boolean, dismissed = boolean, result = any> {

	closed: boolean;
	dismissed: boolean;
	result: any;

}