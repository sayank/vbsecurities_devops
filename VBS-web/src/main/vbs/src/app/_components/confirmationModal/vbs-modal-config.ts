import { InjectionToken } from "@angular/core";

export class VbsModalConfig {
	title: string = '';
	body: string = '';
	size?: "sm" | "lg";
	centered?: boolean;
	allowDismiss?: boolean;
	acceptButtonText?: '';
	rejectButtonText?: '';
    alertOnly?: boolean;
}


export const MODAL_DATA: InjectionToken<any> = new InjectionToken<any>("MODAL_DATA");
