import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormGroup, FormControl, AbstractControl} from '@angular/forms';
import {ContactsService} from "../../_services/contacts.service";
import {Contact} from "../../_models/contact";
import {Session} from "../../_session/session";
import {log} from "util";

@Component({
    selector: 'contact-search',
    templateUrl: './contact-search.component.html',
    providers: [ContactsService]
})
export class ContactSearchComponent implements OnInit {

    @Output() selected = new EventEmitter();
    @Input() searchTerm: string;

    form: FormGroup;
    searched: boolean = false;
    results: Contact[];
    types: Map<number, string>;
    searching: boolean = false;

    constructor(
        private activeModal: NgbActiveModal,
        private contactsService: ContactsService,
        private session: Session
    ) {}

    ngOnInit() {
        this.form = new FormGroup({
            'nameType': new FormControl('CONTAINS'),
            'name': new FormControl(null)
        }, [searchValidator]);
        log(this.form.value);

        if (this.searchTerm != null && this.searchTerm.length > 0) {
            this.form.controls['name'].patchValue(this.searchTerm);
            this.search();
        }
    }

    search() {
        this.searched = false;
        this.results = [];
        this.searching = true;
        this.contactsService.searchCrmContacts(this.form.value.name, this.form.value.nameType).subscribe(
            results => {
                this.results = results;
                this.searched = true;
                this.searching = false;
            },
            error => {
                this.session.addError("Server error on contact search", error);
                this.searching = false;
                }
        );
    }

    select(f: Contact) {
        this.selected.next(f);
        this.c();
    }

    clear() {
        this.form.reset({
            'name': null,
            'nameType': "CONTAINS"
        });
        this.results = null;
        this.searched = false;
    }

    c() {
        this.activeModal.close();
    }

    d() {
        this.activeModal.dismiss();
    }

    textForSearchType(t: string): string {
        if (t == 'EQUALS') {
            return " = ";
        } else if (t == 'LESS_THAN') {
            return " < ";
        } else if (t == 'GREATER_THAN') {
            return " > ";
        } else if (t == 'BETWEEN') {
            return "Between";
        } else if (t == 'BEGINS_WITH') {
            return 'Begins With';
        } else if (t == 'IN') {
            return ' In ';
        } else if (t == 'NOT_EQUALS') {
            return ' != ';
        } else if (t == 'CONTAINS') {
            return 'Contains';
        } else {
            return '';
        }
    }

    compareName(v1, v2, c1: Contact, c2: Contact): number {
        let s1 = (c1.companyName != null) ? c1.companyName : c1.firstName + " " + c1.lastName;
        let s2 = (c2.companyName != null) ? c2.companyName : c2.firstName + " " + c2.lastName;
        return s1.localeCompare(s2);
    }

    formKeyDown(event) {
        if(event.keyCode == 13) {
            event.preventDefault();
            this.search();
        }
    }
}

export const searchValidator = (control: AbstractControl): { [key: string]: boolean } => {
    if (control.get('name').value == null || control.get('name').value.trim().length == 0) {
        return {name: true};
    }
    return null;
}
