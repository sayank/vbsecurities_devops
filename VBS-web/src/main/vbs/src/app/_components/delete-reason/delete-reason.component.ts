import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SecurityService} from "../../_services/security.service";
import {NameDescription} from "../../_models/nameDescription";
import {Session} from "../../_session/session";
import {FormBuilder, FormGroup} from "@angular/forms";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-delete-reason',
  templateUrl: './delete-reason.component.html',
  styleUrls: ['./delete-reason.component.css']
})
export class DeleteReasonComponent implements OnInit {

    deleteReasons:NameDescription[] = [];
    form: FormGroup = null;
    securityPurposeId:number = 0;

    @Output() selected = new EventEmitter();

    constructor(private securityService:SecurityService, private session: Session,
                private fb: FormBuilder, private activeModal: NgbActiveModal) {
        this.deleteReasons = this.session.getProperty("vbDeleteReasons");
    }

    ngOnInit() {
        this.form = this.fb.group({'deleteReason': 1});
    }

    save(){
        this.selected.next(this.form.get('deleteReason').value);
        this.activeModal.close();
    }

    dismiss(){ this.activeModal.dismiss(); }

}
