import {HttpClient} from "@angular/common/http";
import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable()
export class ManifestUtil {

    private manifestSubject:BehaviorSubject<any> = new BehaviorSubject<any>({});

    constructor(private http: HttpClient) {
        this.getManifest();
    }

    getGitSha(){
        return this.manifestSubject.getValue()["git-sha"];
    }

    getGitBranch(){
       return this.manifestSubject.getValue()["git-branch"];
    }

    getManifest():void {
        this.http.get<any>('/vbs/manifest').subscribe(manifest => this.manifestSubject.next(manifest));
    }
}
