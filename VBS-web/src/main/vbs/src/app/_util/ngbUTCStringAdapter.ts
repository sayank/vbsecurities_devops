import { Injectable } from "@angular/core";
import { NgbDateAdapter, NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";

@Injectable()
export class NgbUTCStringAdapter extends NgbDateAdapter<any> {

	fromModel(date): NgbDateStruct {

		if(date != null && date.value !== undefined) {
			date = date.value;
		}
		let result = (
					date && Number(date.substring(0, 4)) && 
					Number(date.substring(5, 7) + 1) && 
					Number(date.substring(8, 10))
				) ?
             	{
					year: Number(date.substring(0, 4)),
                    month: Number(date.substring(5, 7)),
					day: Number(date.substring(8, 10))
				} : null;

		return result;
	}

  	toModel(date: NgbDateStruct): string {
    	let result = date ? date.year.toString() + '-' + String('00' + date.month).slice(-2)
							+ '-' + String('00' + date.day).slice(-2) + 'T00:00:00.000' : null;
		return result;
  	}
}