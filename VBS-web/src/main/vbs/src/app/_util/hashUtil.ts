import { Md5 } from "ts-md5";
import { Injectable } from "@angular/core";

@Injectable()
export class HashUtil {

	public static getHash(object: any) : string {
		return <string>Md5.hashStr(JSON.stringify(object));
	}
}