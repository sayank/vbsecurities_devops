import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialInstitutionSearchComponent } from './financial-institution-search.component';

describe('FinancialInstitutionSearchComponent', () => {
  let component: FinancialInstitutionSearchComponent;
  let fixture: ComponentFixture<FinancialInstitutionSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinancialInstitutionSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialInstitutionSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
