import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Enrolment} from '../_models/enrolment';
import {map} from 'rxjs/operators';
import {EnrolmentActivity} from "../_models/enrolmentActivity";
import {CEEnrolmentDecision} from "../_models/ceEnrolmentDecision";

@Injectable({
	providedIn: 'root'
})

export class EnrolmentService {

	constructor(private http:HttpClient) { }

	private base:string = "/vbs/api/enrolment";

	getEnrolments(poolId: number): Observable<Enrolment[]> {
		let params = {poolId: poolId + ''};
		return this.http.get(this.base + '/enrolments', {params: params}).pipe(map(res => <Enrolment[]> res));
	}

	getCRMEnrolments(poolId: number): Observable<Enrolment[]> {
		let params = {poolId: poolId + ''};
		return this.http.get(this.base + '/getCRMEnrolmentList', {params: params}).pipe(map(res => <Enrolment[]> res));
	}

    getCRMEnrolment(enrolmentNumber: string): Observable<Enrolment> {
        let params = {enrolmentNumber: enrolmentNumber};
        return this.http.get(this.base + '/getCRMEnrolment', {params: params}).pipe(map(res => <Enrolment> res));
    }

	getEnrolment(enrolmentNumber: string, poolId: number, securityIdentityTypeId: number): Observable<Enrolment> {
		let params = {poolId: poolId + '', enrolmentNumber: enrolmentNumber, securityIdentityTypeId: securityIdentityTypeId != null ? securityIdentityTypeId + '' : ''};
		return this.http.get(this.base + '/enrolment', {params: params}).pipe(map(res => <Enrolment> res));
	}

	getEnrolmentForSecurity(enrolmentNumber: string, securityId: number): Observable<Enrolment> {
		let params = {securityId: securityId + '', enrolmentNumber: enrolmentNumber};
		return this.http.get(this.base + '/enrolmentForSecurity', {params: params}).pipe(map(res => <Enrolment> res));
	}

	getAvailableEnrolments(poolId: number, onlyCondoConv: boolean, securityIdentityTypeId: number): Observable<Enrolment[]> {
		let params = {poolId: poolId + '', onlyCondoConv: onlyCondoConv + '', securityIdentityTypeId: securityIdentityTypeId != null ? securityIdentityTypeId + '' : ''};
		return this.http.get(this.base + '/availableEnrolments', {params: params}).pipe(map(res => <Enrolment[]> res));
	}

	updateEnrolments(poolId: number, enrolments: Enrolment[]): Observable<any> {
		let params = {poolId: poolId + ''};
		return this.http.post(this.base + '/update', enrolments, {params: params});
	}

	getEnrolmentActivities(enrolmentNumber:string): Observable<EnrolmentActivity[]> {
        let params = {enrolmentNumber: enrolmentNumber};
        return this.http.get(this.base + '/getEnrolmentActivities', {params: params}).pipe(map(res => <EnrolmentActivity[]> res));
    }

    getCEEnrolmentDecision(enrolmentNumber:string): Observable<CEEnrolmentDecision> {
        let params = {enrolmentNumber: enrolmentNumber};
        return this.http.get(this.base + '/decision', {params: params}).pipe(map(res => <CEEnrolmentDecision> res));
    }

    updateCEEnrolmentDecision(decision: CEEnrolmentDecision): Observable<any> {
        return this.http.post(this.base + '/decision', decision);
    }

}
