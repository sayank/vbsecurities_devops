import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Release} from "../_models/release";
import {map} from "rxjs/operators";
import {Enrolment} from "../_models/enrolment";
import {PendingRelease} from "../_models/pendingRelease";
import {AutoReleaseRunSearchParameters} from "../_models/autoReleaseRunSearchParameters";
import {AutoReleaseRunSearchResult} from "../_models/autoReleaseRunSearchResult";
import {AutoReleaseResultWrapper} from "../_models/autoReleaseResultWrapper";
import {AutoReleaseResult} from "../_models/autoReleaseResult";
import {ReleaseSearchParameters} from "../_models/releaseSearchParameters";
import {AutoRelease} from "../_models/autoRelease";


@Injectable()
export class ReleaseService {

constructor(private http:HttpClient) { }
private base:string = "/vbs/api/release";

	getReleases(securityId: number): Observable<Release[]> {
		let params = {securityId: securityId + ''};
		return this.http.get(this.base + '/getReleases', {params: params}).pipe(map(res => <Release[]> res));
	}

	getEnrolmentsForRelease(securityId: number): Observable<Enrolment[]> {
		let params = {securityId: securityId + ''};
		return this.http.get(this.base + '/getEnrolmentsForRelease', {params: params}).pipe(map(res => <Enrolment[]> res));
	}

	getAutoReleaseResults(runId: number): Observable<AutoReleaseResultWrapper> {
		let params = {runId: runId + ''};
		return this.http.get(this.base + '/getAutoReleaseResults', {params: params}).pipe(map(res => <AutoReleaseResultWrapper> res));
	}

	saveAutoReleaseEnrolments(runId: number, autoReleaseResults: AutoReleaseResult[]): Observable<any> {
    let params = {runId: runId + ''};
		return this.http.post(this.base + '/saveAutoReleaseEnrolments', autoReleaseResults, {params: params});
	}

	startAutoReleaseProcess(): Observable<any> {
		return this.http.post(this.base + '/startAutoReleaseProcess', null);
	}

    finalizeAutoRelease(runId: number, autoReleaseResults: AutoReleaseResult[]): Observable<any> {
        let params = { runId: runId + '' };
        return this.http.post(this.base + '/finalizeAutoRelease', autoReleaseResults, { params: params });
    }

    existsRunInProgress(): Observable<boolean> {
        return this.http.get(this.base + '/existsRunInProgress').pipe(map(res => <boolean> res));
    }

	searchAutoReleaseResults(autoReleaseRunSearchParameters: AutoReleaseRunSearchParameters): Observable<AutoReleaseRunSearchResult[]> {
		return this.http.post(this.base + '/searchAutoReleaseResults', autoReleaseRunSearchParameters).pipe(map(res => <AutoReleaseRunSearchResult[]> res));
	}

	getAutoReleasePassedResultsExcel(runId: number): Observable<any> {
		return this.http.post(this.base + '/getAutoReleasePassedResultsExcel/'+ runId, {}, {responseType: 'blob'});
	}

	getAutoReleaseFailedResultsExcel(runId: number): Observable<any> {
		return this.http.post(this.base + '/getAutoReleaseFailedResultsExcel/'+ runId, {}, {responseType: 'blob'});
	}

	validateRelease(release: Release): Observable<any>{
		return this.http.post(this.base + '/validateRelease', release);
	}

	saveRelease(release: Release): Observable<Release>{
		return this.http.post(this.base + '/saveRelease', release).pipe(map(res => <Release> res));
	}

	setupRelease(releaseTypeId:number, securityId:number): Observable<Release> {
        let params = {securityId: securityId+'', releaseTypeId: releaseTypeId+''};
        return this.http.get(this.base + '/setupRelease', {params: params}).pipe(map(res => <Release> res));
    }

    getPendingReleases(): Observable<PendingRelease[]> {
        return this.http.get(this.base + '/getPendingReleases').pipe(map(res => <PendingRelease[]> res));
    }

    sendReleaseListToFMS(pendingReleases: PendingRelease[]): Observable<PendingRelease[]> {
        return this.http.post(this.base + '/sendReleaseListToFMS', pendingReleases).pipe(map(res => <PendingRelease[]> res));
    }

    getPendingAndSentReleases(): Observable<PendingRelease[]> {
        return this.http.get(this.base + '/getPendingAnsSentReleases').pipe(map(res => <PendingRelease[]> res));
	}

	getPendingAndSentReleasesExcel(): Observable<any> {
		return this.http.post(this.base + '/getPendingAndSentReleasesExcel', {}, {responseType: 'blob'});
	}

	searchReleases(releaseSearchParameters: ReleaseSearchParameters): Observable<PendingRelease[]> {
		return this.http.post(this.base + '/searchReleases', releaseSearchParameters).pipe(map(res => <PendingRelease[]> res));
	}

	getSearchedReleasesExcel(releaseSearchParameters: ReleaseSearchParameters): Observable<any> {
		return this.http.post(this.base + '/getSearchedReleasesExcel', releaseSearchParameters, {responseType: 'blob'});
	}

	cancelRelease(release: Release): Observable<Release>{
		return this.http.post(this.base + '/cancelRelease', release).pipe(map(res => <Release> res));
	}

	getAutoReleaseForSecurityId(securityId: number): Observable<AutoRelease[]> {
		let params = {securityId: securityId + ''};
		return this.http.get(this.base + '/getAutoReleaseForSecurityId', {params: params}).pipe(map(res => <AutoRelease[]> res));
	}

	getAutoReleaseForSecurityIdExcel(securityId: number): Observable<any> {
		let params = {securityId: securityId + ''};
		return this.http.post(this.base + '/getAutoReleaseForSecurityIdExcel', {}, {responseType: 'blob', params: params});
	}

	getReleasesExcel(securityId: number): Observable<any> {
		let params = {securityId: securityId + ''};
		return this.http.post(this.base + '/getReleasesExcel', {}, {responseType: 'blob', params: params});
	}
}
