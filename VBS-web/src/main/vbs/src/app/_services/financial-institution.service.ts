import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {FinancialInstitution} from '../_models/financialInstitution';
import {Branch} from '../_models/branch';
import {FinancialInstitutionAndBranch} from '../_models/financialInstitutionAndBranch';
import {ContactFinancialInstitution} from '../_models/contactFinancialInstitution';
import {FinancialInstitutionMaaPoa} from '../_models/financialInstitutionMaaPoa';
import {FIActivity} from "../_models/fiActivity";

@Injectable({
	providedIn: 'root'
})

export class FinancialInstitutionService {

	private base:string = "/vbs/api/financialInstitution";

	constructor(private http:HttpClient) { }

	financialInstitutionSearch(search: any): Observable<FinancialInstitution[]> {
		let params = {};
		params['codeType'] = search.codeType;
		params['nameType'] = search.nameType;
		if(search.code != null){
			params['code'] = search.code;
			if(search.codeType == 'BETWEEN'){
				params['codeTo'] = search.codeTo;
			}
		}
		if(search.name != null && search.name.trim().length > 0){
			params['name'] = search.name;
			if(search.nameType == 'BETWEEN'){
				params['nameTo'] = search.nameTo;
			}
		}
		if(search.financialInstitutionTypeId != null){
			params['financialInstitutionTypeId'] = search.financialInstitutionTypeId;
		}
		return this.http.get(this.base + '/financialSearch', {params: params}).pipe(map(res => <FinancialInstitution[]> res));
	}

	getFinancialInstitution(id: number): Observable<FinancialInstitution> {
		let params = {financialInstitutionId: id + ''};
		return this.http.get(this.base + '/getFinancialInstitution', {params: params}).pipe(map(res => <FinancialInstitution> res));
	}

	getBranches(financialInstitutionId: number): Observable<Branch[]> {
		let params = {financialInstitutionId: financialInstitutionId + ''};
		return this.http.get(this.base + '/getBranches', {params: params}).pipe(map(res => <Branch[]> res));
	}

	validateFinancialInstitution(financialInstitution: FinancialInstitution): Observable<any>{
		return this.http.post(this.base + '/validateFinancialInstitution', financialInstitution);
	}

	saveFinancialInstitution(financialInstitution: FinancialInstitution): Observable<FinancialInstitution>{
		return this.http.post(this.base + '/saveFinancialInstitution', financialInstitution).pipe(map(res => <FinancialInstitution> res));
	}

	financialInstitutionTypeahead(partial: string): Observable<FinancialInstitution> {
		let params = {partial: partial};
		return this.http.get(this.base + '/financialInstitutionTypeahead', {params: params}).pipe(map(res => <FinancialInstitution> res));
	}

	getFinancialInstitutionAndBranch(id: number): Observable<FinancialInstitutionAndBranch> {
		let params = {id: id + ''};
		return this.http.get(this.base + '/getFinancialInstitutionAndBranch', {params: params}).pipe(map(res => <FinancialInstitutionAndBranch> res));
	}

	getContactsForFinancialInstitution(financialInstitutionId: number): Observable<ContactFinancialInstitution[]> {
		let params = {financialInstitutionId: financialInstitutionId + ''};
		return this.http.get(this.base + '/getContactsForFinancialInstitution', {params: params}).pipe(map(res => <ContactFinancialInstitution[]> res));
	}

	saveFinancialInstitutionContacts(contactsFinancialInstitution: ContactFinancialInstitution[]): Observable<any> {
        let params = { };
        return this.http.post(this.base + '/saveFinancialInstitutionContacts', contactsFinancialInstitution, { params: params });
	}
		
	getMaaPoas(financialInstitutionId: number): Observable<FinancialInstitutionMaaPoa[]> {
		let params = {financialInstitutionId: financialInstitutionId + ''};
		return this.http.get(this.base + '/getMaaPoas', {params: params}).pipe(map(res => <FinancialInstitutionMaaPoa[]> res));
	}

	getFinancialInstitutionActivities(financialInstitutionId: number): Observable<FIActivity[]> {
        let params = {financialInstitutionId: financialInstitutionId + ''};
        return this.http.get(this.base + '/getActivities', {params: params}).pipe(map(res => <FIActivity[]> res));
    }

}
