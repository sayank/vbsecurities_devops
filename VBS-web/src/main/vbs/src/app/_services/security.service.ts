import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Contact} from '../_models/contact';
import {Security} from '../_models/security';
import {HistoryField} from '../_models/historyField';
import {SecuritySearchParameters} from '../_models/securitySearchParameters';
import {SecuritySearchResultWrapper} from '../_models/securitySearchResultWrapper';
import {FinancialInstitutionMaaPoa} from '../_models/financialInstitutionMaaPoa';
import {PoolSecuritiesParameters} from '../_models/poolSecuritiesParameters';
import {Alert} from "../_models/alert";
import { DeletedSecuritySearchParameters } from '../_models/deletedSecuritySearchParameters';
import { SecuritySearchResult } from '../_models/securitySearchResult';
import {EnrolmentPoolActivity} from "../_models/enrolmentPoolActivity";

@Injectable()
export class SecurityService {

	constructor(private http:HttpClient) { }

	private base:string = "/vbs/api/security";

	vbNumberTypeahead(partial: string): Observable<Contact> {
		let params = {partial: partial};
		return this.http.get('/vbs/api/lookup/vbTypeahead', {params: params}).pipe(map(res => <Contact> res));
	}

	instrumentNumberTypeahead(partial: string): Observable<string> {
		let params = {partial: partial};
		return this.http.get('/vbs/api/lookup/instrumentNumberTypeahead', {params: params}).pipe(map(res => <string> res));
	}
	
	allSecuritiesByInstrumentNumbersLike(partial: string): Observable<string> {
		let params = {partial: partial};
		return this.http.get('/vbs/api/lookup/allSecuritiesByInstrumentNumbersLike', {params: params}).pipe(map(res => <string> res));
	}

	getSecurity(id: number): Observable<Security> {
		let params = {id: id + ''};
		return this.http.get(this.base + '/getSecurity', {params: params}).pipe(map(res => <Security> res));
	}

    getEnrolmentActivities(securityId: number): Observable<EnrolmentPoolActivity[]> {
        let params = {securityId: securityId + ''};
        return this.http.get(this.base + '/getEnrolmentActivities', {params: params}).pipe(map(res => <EnrolmentPoolActivity[]> res));
	}
	
	getDeletedVbActivities(poolId: number): Observable<EnrolmentPoolActivity[]> {
        let params = {poolId: poolId + ''};
        return this.http.get(this.base + '/getDeletedVbActivities', {params: params}).pipe(map(res => <EnrolmentPoolActivity[]> res));
	}
	
	searchSecurity(securitySearchParameters: SecuritySearchParameters): Observable<SecuritySearchResultWrapper> {
		return this.http.post(this.base + '/searchSecurity', securitySearchParameters).pipe(map(res => <SecuritySearchResultWrapper> res));
	}

	searchResultsExcel(securitySearchParameters: SecuritySearchParameters, showInterest:boolean): Observable<any> {
		return this.http.post(this.base + '/searchResultsExcel/'+ showInterest, securitySearchParameters, {responseType: 'blob'});
	}

	getCommentHistory(id:number): Observable<HistoryField[]>{
		let params = {id: id + ''};
		return this.http.get(this.base + '/getHistoryComment', {params: params}).pipe(map(res => <HistoryField[]> res));
	}

	saveSecurity(security: Security, alertConfirms: string): Observable<Security>{
		let params = (alertConfirms != null) ? {displayedAlertsConfirmations: alertConfirms} : {};
	    return this.http.post(this.base + '/saveSecurity', security, {params: params}).pipe(map(res => <Security> res));
	}

	validateDeleteSecurity(securityId: number): Observable<any>{
		let params = {securityId: securityId + ''};
		return this.http.get(this.base + '/validateDeleteSecurity', {params: params});
	}

	deleteSecurity(securityId: number): Observable<any> {
		let params = {securityId: securityId + ''};
		return this.http.get(this.base + '/deleteSecurity', {params: params});
	}

	recalculateMaaPoaAmounts(security: Security): Observable<any> {
        let securityNew = security;
        securityNew.financialInstitutionMaaPoaDTO = security.financialInstitutionMaaPoaDTO;
        securityNew.originalAmount = security.originalAmount;
        securityNew.currentAmount = security.currentAmount;
		return this.http.post(this.base + '/recalculateMaaPoaAmounts', securityNew).pipe(map(res => <FinancialInstitutionMaaPoa> res));
	}

    getMaaPoaDetails(securityId: number):Observable<FinancialInstitutionMaaPoa> {
        let params = {securityId: securityId + ''};
        return this.http.get(this.base + '/getMaaPoaDetails', {params: params}).pipe(map(res => <FinancialInstitutionMaaPoa>res));
	}
	
	poolSecurities(params: PoolSecuritiesParameters): Observable<any> {
		return this.http.post(this.base + '/poolSecurities', params);		
	}

    validateAlert(alert: Alert): Observable<any> {
        return this.http.post(this.base + '/validateAlert', alert);
	}
	
	searchDeletedSecurity(deletedSecuritySearchParameters: DeletedSecuritySearchParameters): Observable<SecuritySearchResult[]> {
		return this.http.post(this.base + '/searchDeletedSecurity', deletedSecuritySearchParameters).pipe(map(res => <SecuritySearchResult[]> res));
	}

	searchDeletedSecurityExcel(deletedSecuritySearchParameters: DeletedSecuritySearchParameters): Observable<any> {
		return this.http.post(this.base + '/searchDeletedSecurityExcel', deletedSecuritySearchParameters, {responseType: 'blob'});
	}

}
