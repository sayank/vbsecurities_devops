import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {GenerateCorrespondence} from '../_models/generateCorrespondence';

@Injectable({
  providedIn: 'root'
})
export class CorrespondenceService {

	constructor(private http:HttpClient) { }

	private base:string = "/vbs/api/correspondence";

	generate(generateCorrespondenceRequest: GenerateCorrespondence): Observable<GenerateCorrespondence> {
		return this.http.post(this.base + '/generate', generateCorrespondenceRequest).pipe(map(res => <GenerateCorrespondence> res));
	}
}
