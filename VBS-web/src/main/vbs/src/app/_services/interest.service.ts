import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { InterestRate } from '../_models/interestRate';
import { map } from 'rxjs/operators';
import { SecurityInterestSearchParams } from '../_models/securityInterestSearchParams';
import { InterestSearchResult } from '../_models/InterestSearchResult';
import {SecuritySearchParameters} from "../_models/securitySearchParameters";
import { CashSecurityNoInterest } from '../_models/cashSecurityNoInterest';

@Injectable({
  providedIn: 'root'
})
export class InterestService {

	constructor(private http:HttpClient) { }
	private base:string = "/vbs/api/interest";
	
	getAllInterestRates(): Observable<InterestRate[]> {
		return this.http.get(this.base + '/interestRates').pipe(map(rates => <InterestRate[]> rates));
	}

  getNextValidStartDate(): Observable<any> {
	  return this.http.get(this.base + '/getNextValidStartDate').pipe(map(d => d));
  }

  validateInterestRate(interestRate: InterestRate): Observable<any>{
    return this.http.post(this.base + '/validateInterestRate', interestRate);
  }

	saveInterestRate(rate: InterestRate): Observable<InterestRate[]>{
    return this.http.post(this.base + '/saveInterestRate', rate).pipe(map(res => <InterestRate[]> res));
  }

  deleteInterestRate(rate: InterestRate): Observable<any>{
    return this.http.post(this.base + '/deleteInterestRate', rate);
  }

	searchInterestBySecurityId(securityInterestSearchParams: SecurityInterestSearchParams): Observable<InterestSearchResult[]> {
		return this.http.post(this.base + '/searchInterestBySecurityId', securityInterestSearchParams).pipe(map(res => <InterestSearchResult[]> res));
	}

  searchResultsExcel(securityInterestSearchParams: SecurityInterestSearchParams): Observable<any> {
    return this.http.post(this.base + '/searchResultsExcel', securityInterestSearchParams, {responseType: 'blob'});
  }

  searchCashSecuritiesWithoutInterest(): any {
    return this.http.get(this.base + '/getCashSecuritiesWithoutInterest').pipe(map(res => <CashSecurityNoInterest[]> res));        
  }

  searchNoInterestResultsExcel(): Observable<any> {
    return this.http.get(this.base + '/searchNoInterestResultsExcel', {responseType: 'blob'});
  }  
}
