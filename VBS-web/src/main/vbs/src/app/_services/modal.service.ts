import { Injectable, Injector, ReflectiveInjector } from '@angular/core';
import { NgbModal, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { VbsModalConfig, MODAL_DATA } from '../_components/confirmationModal/vbs-modal-config';
import { Observable, Subject } from 'rxjs';
import { VbsModalResult } from '../_components/confirmationModal/vbs-modal-result';
import { ConfirmationModal } from '../_components/confirmationModal/confirmation-modal.component';


@Injectable()
export class ModalService {

	constructor(private injector: Injector, private ngbModal: NgbModal) { }

	public confirm(config: VbsModalConfig): Observable<VbsModalResult> {
		return this.show(ConfirmationModal, config);
	}

    public alert(config: VbsModalConfig): Observable<VbsModalResult> {
        return this.show(ConfirmationModal, config);
    }

	public show(component: any, config: VbsModalConfig): Observable<VbsModalResult> {
		const modalInjector: ReflectiveInjector = this.setupInjector(config);

		const ngbModalConfig: NgbModalOptions = {
			backdrop: config.allowDismiss !== undefined ? ((config.allowDismiss) ? true: "static") : true,
			injector: modalInjector,
			keyboard: config.allowDismiss !== undefined ? config.allowDismiss : false,
			size: config.size !== undefined ? config.size : "sm",
			centered: config.centered !== undefined ? config.centered : true
		};

		const modalRef: NgbModalRef = this.ngbModal.open(component, ngbModalConfig);
		const response: Subject<VbsModalResult> = this.setupResponse(modalRef);

		return response;
	}

	private setupResponse(modalRef: NgbModalRef): Subject<VbsModalResult> {
		const response: Subject<VbsModalResult> = new Subject<VbsModalResult>();

		modalRef.result.then((closeResponse) => {response.next({
            closed: true, dismissed: false, result: closeResponse,
		});response.complete();}).catch((dismissReason) => {response.next({
			closed: false, dismissed: true, result: dismissReason,
		});response.complete();});

		return response;
	}

	private setupInjector(config: VbsModalConfig): ReflectiveInjector {
		return ReflectiveInjector.resolveAndCreate([{ 
			provide: MODAL_DATA, 
			useValue: {
				title: config.title, 
				body: config.body, 
				allowDismiss: config.allowDismiss !== undefined ? config.allowDismiss : true,
				acceptButtonText: config.acceptButtonText,
				rejectButtonText: config.rejectButtonText,
                alertOnly: config.alertOnly !== undefined ? config.alertOnly : false
			} 
		}], this.injector);
	}

}
