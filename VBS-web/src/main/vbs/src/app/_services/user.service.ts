import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../_models/user';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class UserService {

	constructor(private http: HttpClient) {
	}
  
	authenticate(username: string, password: string): Observable<User> {
	  const params = new HttpParams()
		.append('username', username)
		.append('password', password);
	  
	  return this.http.post<User>('/vbs/login', params).pipe(map(u => {
		  return Object.assign(new User, u);
	  }));
	}

	uiSetup(): Observable<any> {
        return this.http.get<any>('/vbs/uiSetup');
    }
	
	logout(): Observable<any> {
	  localStorage.clear();
	  return this.http.get('/vbs/logout');
	}
}
