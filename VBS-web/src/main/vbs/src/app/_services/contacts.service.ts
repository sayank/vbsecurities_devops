import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Contact} from "../_models/contact";
import {EscrowAgentSearchParams} from "../_models/escrowAgentSearchParams";
import {EscrowAgentSearchResultWrapper} from "../_models/escrowAgentSearchResultWrapper";

@Injectable({
	providedIn: 'root'
})

export class ContactsService {

	constructor(private http:HttpClient) { }

	private base:string = "/vbs/api/contacts";

  searchCrmContacts(search: string, type: string): Observable<Contact[]> {
    let params = {search: search, type: type};
		return this.http.get(this.base + '/searchCrmContacts', {params: params}).pipe(map(res => <Contact[]> res));
	}

  escrowAgentTypeahead(partial: string): Observable<Contact[]> {
    let params = {partial: partial};
    return this.http.get(this.base + '/escrowAgentTypeahead', {params: params}).pipe(map(res => <Contact[]> res));
  }

  escrowAgentSearch(escrowAgentSearchParams: EscrowAgentSearchParams): Observable<EscrowAgentSearchResultWrapper> {
    return this.http.post(this.base + '/escrowAgentSearch', escrowAgentSearchParams).pipe(map(res => <EscrowAgentSearchResultWrapper> res));
  }

  getLawyersByEscrowAgentCrmContactId(crmContactId: String): Observable<Contact[]> {
    let params = {crmContactId: crmContactId + ''};
    return this.http.get(this.base + '/getLawyersByEscrowAgentCrmContactId', {params: params}).pipe(map(res => <Contact[]> res));
  }

  getAssistantsByEscrowAgentCrmContactId(crmContactId: String): Observable<Contact[]> {
    let params = {crmContactId: crmContactId + ''};
    return this.http.get(this.base + '/getAssistantsByEscrowAgentCrmContactId', {params: params}).pipe(map(res => <Contact[]> res));
  }

  getContactByCrmContactId(crmContactId: string): Observable<Contact> {
    let params = {crmContactId: crmContactId + ''};
    return this.http.get(this.base + '/getContact', {params: params}).pipe(map(res => <Contact> res));
  }

}
