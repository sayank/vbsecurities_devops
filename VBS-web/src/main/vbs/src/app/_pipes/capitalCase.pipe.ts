import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ 
    name: "capitalCase"
})
export class CapitalCasePipe implements PipeTransform {
    transform(value: any, ...args: any[]): string {
        if (!value) {
            return value;
        }

        if (typeof value !== 'string') {
            return value;
        }

        return value.charAt(0).toUpperCase() + value.toLowerCase().slice(1, value.length);
    }
}