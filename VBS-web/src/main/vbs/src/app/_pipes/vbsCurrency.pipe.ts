import { Pipe, PipeTransform } from "@angular/core";

const PADDING = "000000";

@Pipe({ name: "vbsCurrency" })
export class VbsCurrencyPipe implements PipeTransform {

  private DECIMAL_SEPARATOR: string;
  private THOUSANDS_SEPARATOR: string;

  constructor() {
    // TODO comes from configuration settings
    this.DECIMAL_SEPARATOR = ".";
    this.THOUSANDS_SEPARATOR = ",";
  }

  transform(value: string | number, fractionSize: number = 2): string {
	let v = '' + value;
	if(!isNaN(Number(value)) && value != null && v.length > 0){
	    let rounded = this.round(value, fractionSize).toString();
        let [ integer, fraction = '0' ] = (rounded).toString().split(this.DECIMAL_SEPARATOR);
		fraction = fractionSize > 0 ? this.DECIMAL_SEPARATOR +
            (fraction + PADDING).substring(0, fractionSize) : "";
		integer = integer.replace(/\B(?=(\d{3})+(?!\d))/g, this.THOUSANDS_SEPARATOR);
		return integer + fraction;
	}
	return null;
  }

  parse(value: string, fractionSize: number = 2): string {	  
	let [ integer, fraction = "" ] = (value+'' || "").split(this.DECIMAL_SEPARATOR);
	integer = integer.replace(new RegExp(this.THOUSANDS_SEPARATOR, "g"), "");
	fraction = parseInt(fraction, 10) > 0 && fractionSize > 0 ? this.DECIMAL_SEPARATOR + (fraction + PADDING).substring(0, fractionSize) : "";
	return integer + fraction;
  }

  private round(num:string|number, decimals:number){
    return +(Math.round(Number.parseFloat(num+"e+"+decimals)) + "e-"+decimals);
  }

}
