
import {
//	ChangeDetectionStrategy,
	Component,
//	HostListener,
//	Inject,
	OnInit,
	Input,
	ElementRef,
	ViewChild, 
	Renderer
  } from '@angular/core';
  import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
  
  @Component({
	selector: 'app-delete-confirmation',
	templateUrl: './delete-confirmation.component.html',
	styleUrls: ['./delete-confirmation.component.css']
  })
  export class DeleteConfirmationComponent implements OnInit {
  
  
	@ViewChild('openButton') openButton:ElementRef;
  
	@Input() message: string;
  
	@Input() title: string;
  
	callback: () => void;
  
	constructor(private modalService: NgbModal, private renderer:Renderer) {}
  
	ngOnInit() {}
  
	public open(content) {
	  this.modalService.open(content).result.then((result) => {
		this.callback();
	  },
	  (reason) => {});
	  
	}
  
	public confirmDelete(callback: () => void ) {
	  this.callback = callback;
	  let event = new MouseEvent('click', {bubbles: true});
	  this.renderer.invokeElementMethod(this.openButton.nativeElement, 'dispatchEvent', [event]);   
	}
  
  
	setMessage(message:string) {
	  this.message = message;
	}
  
	setTitle(title: string) {
	  this.title = title;
	}
  
  }
  
