import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialInstitutionAlertComponent } from './financial-institution-alert.component';

describe('FinancialInstitutionAlertComponent', () => {
  let component: FinancialInstitutionAlertComponent;
  let fixture: ComponentFixture<FinancialInstitutionAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinancialInstitutionAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialInstitutionAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
