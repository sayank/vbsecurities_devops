import {Component, OnInit, Input, ViewChild, ElementRef} from '@angular/core';
import {FinancialInstitution} from 'src/app/_models/financialInstitution';
import {FinancialInstitutionAlertDetailComponent} from 'src/app/financial-institution-alert-detail/financial-institution-alert-detail.component';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {FinancialInstitutionAlert} from 'src/app/_models/financialInstitutionAlert';

@Component({
    selector: 'app-financial-institution-alert',
    templateUrl: './financial-institution-alert.component.html',
    styleUrls: ['./financial-institution-alert.component.css']
})
export class FinancialInstitutionAlertComponent implements OnInit {

    @Input() financialInstitution: FinancialInstitution = null;
    financialInstitutionId: number = null;
    mode: string = null;  //  "view", "insert" or "update"

    @ViewChild(FinancialInstitutionAlertDetailComponent) alertDetailViewChild: FinancialInstitutionAlertDetailComponent;
    @ViewChild('dataTable') dataTableViewChild: DatatableComponent;
    @ViewChild('alertDetail') alertDetail: ElementRef;

    constructor() {
    }

    ngOnInit() {
        this.mode = "view";
    }

    loadAlert(alert: FinancialInstitutionAlert) {
        this.mode = "update";
        this.alertDetailViewChild.loadAlert(alert);
        this.scrollToDetail();
    }

    newAlert() {
        this.mode = "insert";
        this.alertDetailViewChild.newAlert(this.financialInstitutionId);
        this.scrollToDetail();
    }

    scrollToDetail() {
        this.alertDetail.nativeElement.scrollIntoView();
    }

    deleteAlert(alert: FinancialInstitutionAlert) {
        let index = this.financialInstitution.alerts.indexOf(alert);
        this.financialInstitution.alerts.splice(index, 1);
        this.dataTableViewChild.rows = this.financialInstitution.alerts;
    }

    onFormClosed(alert: FinancialInstitutionAlert) {
        if (alert && this.mode == "insert") { // user applied changes
            this.financialInstitution.alerts.push(alert);
        }
        if (this.dataTableViewChild) { this.dataTableViewChild.rows = this.financialInstitution.alerts; }
        this.mode = "view";
    }

    modeTitle():string {
        return (this.mode === "insert") ? "Add an Alert" : "Edit an Alert";
    }

    rowCount() {
        let ret = 0;
        this.financialInstitution.alerts.forEach((element) => {
            if (element.alertType != null) {
                ret++;
            }
        });
        return ret;
    }

    get alertsTableEmpty(): boolean {
        return (this.financialInstitution.alerts.length == 1 && this.financialInstitution.alerts[0].alertType == null);
    }

    getAlertDescription(description: string) {
        return description != null ? description.replace(/\n/g, '<br/>') : '';
    }

}
