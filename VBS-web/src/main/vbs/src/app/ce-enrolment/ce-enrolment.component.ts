import {Component, OnInit} from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { SecurityTypePurpose } from "../_components/securityTypePurpose/security-type-purpose.component";
import {EnrolmentService} from "../_services/enrolment.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {map} from "rxjs/operators";
import {Security} from "../_models/security";
import {CEEnrolmentDecision} from "../_models/ceEnrolmentDecision";
import {Session} from "../_session/session";
import {NameDescription} from "../_models/nameDescription";

@Component({
  	selector: 'app-ce-enrolment',
  	templateUrl: './ce-enrolment.component.html',
  	styleUrls: ['./ce-enrolment.component.css']
})
export class CeEnrolmentComponent implements OnInit {

    decision:CEEnrolmentDecision;
    unitsError:boolean = false;
    disableButtons:boolean = false;
	securityTypes: NameDescription[] = [];
	identityTypes: NameDescription[] = [];

	constructor(private route: ActivatedRoute, private enrolmentService:EnrolmentService, private router: Router, private session: Session) {
		this.securityTypes = this.session.getProperty("securityTypes");
		this.identityTypes = this.session.getProperty("identityTypes");
    }

	ngOnInit(){
        this.route.queryParams.subscribe((params: Params) => {
            if(params['enrolmentNumber'] !== undefined){
                this.enrolmentService.getCEEnrolmentDecision(params['enrolmentNumber']).subscribe(d => this.decision = d);
            }else{
                this.router.navigateByUrl("/");
            }
        });
    }

    updateDecision(accept:boolean){
        this.unitsError = false;
	    if((accept && this.decision.unitsToCover != null) || !accept) {
            this.decision.decision = (accept) ? "YES" : "NO";
            this.enrolmentService.updateCEEnrolmentDecision(this.decision).subscribe(ok => this.router.navigateByUrl("/"),
                error => this.session.addError('Failed to execute this request.', error)
            );
        }else{
            this.unitsError = true;
        }
    }

    getTypeNameById(id:number){
	    return this.securityTypes.filter(t => t.id == id)[0].description;
    }
    getIdentityTypeNameById(id:number){
		if(id !== undefined && id != null){
	    	return this.identityTypes.filter(t => t.id == id)[0].description;
		}
		return null;
	}
}
