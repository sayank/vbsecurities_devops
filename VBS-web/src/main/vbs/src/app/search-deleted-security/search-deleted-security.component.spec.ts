import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchDeletedSecurityComponent } from './search-deleted-security.component';

describe('SearchDeletedSecurityComponent', () => {
  let component: SearchDeletedSecurityComponent;
  let fixture: ComponentFixture<SearchDeletedSecurityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchDeletedSecurityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchDeletedSecurityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
