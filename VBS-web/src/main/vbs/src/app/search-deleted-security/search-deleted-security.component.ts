import {Component, HostListener, OnInit} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SecurityService } from '../_services/security.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SecuritySearchResult } from '../_models/securitySearchResult';
import { DeletedSecuritySearchParameters } from '../_models/deletedSecuritySearchParameters';
import { Session } from '../_session/session';

@Component({
	selector: 'app-search-deleted-security',
	templateUrl: './search-deleted-security.component.html',
	styleUrls: ['./search-deleted-security.component.css']
})
export class SearchDeletedSecurityComponent implements OnInit {
	form: FormGroup;
	searching = false;
	searchingForm = false;
	searchParams: DeletedSecuritySearchParameters;
	results: SecuritySearchResult[] = [];
	searched: boolean = false;
	downloadingExcel: boolean = false;
    width = 0;
	
	constructor(
		private securityService: SecurityService, private fb: FormBuilder, 
		private router: Router, private route: ActivatedRoute, 
		private session: Session
	) {
        this.width = window.innerWidth;
    }
	
	ngOnInit() {
	    console.log('start');
		if(this.searchParams == null) {
			this.searchParams = new DeletedSecuritySearchParameters();
		}
		this.form = this.fb.group(this.searchParams);
		this.form.valueChanges.subscribe(searchParams => this.searchParams = searchParams);
	}

	search() {
		this.searched = false;
        this.results = null;
        this.searchingForm = true;
		this.downloadingExcel = false;

		this.securityService.searchDeletedSecurity(this.searchParams).subscribe(
            data => {
                this.results = data;
                this.searched = true;
                this.searchingForm = false;

            },
            errorResult => {
                if(errorResult.status == 422) {
                    let arr = {};
                    for (let err in errorResult.error.validationErrorList) {
                        if (arr[errorResult.error.validationErrorList[err].errorFieldName] === undefined) { arr[errorResult.error.validationErrorList[err].errorFieldName] = []; }
                        arr[errorResult.error.validationErrorList[err].errorFieldName].push(errorResult.error.validationErrorList[err].errorDescription);
                    }
                    for (let field in arr) {
                        if(field != null && this.form.get(field) != null){
                            this.form.get(field).setErrors({ 'serverErrors': arr[field] });
                        }
                    }

                    this.session.addError('Form failed validation.', errorResult);
                    this.searchingForm = false;
                } else {
                    this.session.addError('Error when Searching Securities', errorResult);
                    this.searchingForm = false;
                }
            }
        );
	}

	exportToExcel() {
		this.downloadingExcel = true;

		this.securityService.searchDeletedSecurityExcel(this.searchParams).subscribe(data => {
			const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
			let d = new Date();
			const element = document.createElement('a');
			element.href = URL.createObjectURL(blob);
			element.download = "deleted-security-search-results-"+d.getTime()+".xlsx";
			document.body.appendChild(element);
			element.click();
			element.remove();
		},
		error => {},
		() => {this.downloadingExcel = false;});
	}

	clear(){
		this.searchParams = new DeletedSecuritySearchParameters();
		this.form.patchValue(this.searchParams);
		this.results = null;
		this.searched = false;
		this.downloadingExcel = false;
		this.router.navigate([], { replaceUrl: true});
	}

	textForSearchType(t: string): string{
		if(t == 'EQUALS'){
			return " = ";
		}else if(t == 'LESS_THAN'){
			return " < ";
		}else if(t == 'GREATER_THAN'){
			return " > ";
		}else if(t == 'EQUALS_LESS_THAN'){
			return " <= ";
		}else if(t == 'EQUALS_GREATER_THAN'){
			return " >= ";
		}else if(t == 'IN'){
			return "In";
		}else if(t == 'CONTAINS'){
			return "Contains";
		}else if(t == 'BEGIN_WITH'){
			return "Begins With";
		}else if(t == 'BETWEEN'){
			return "Between";
		}else{
			return '';
		}
	}

	compareDate(date1: Date, date2: Date): number {
		let d1 = new Date(date1); let d2 = new Date(date2);
		if(d1.getTime() < d2.getTime()){
			return -1;
		}else if(d1.getTime() > d2.getTime()){
			return 1;
		}else{
			return 0;
		}
	}

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.width = event.target.innerWidth;
    }
    showScrollbar() {
        return this.width < 1300;
    }
	
}
