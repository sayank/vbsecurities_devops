import {Component} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {SecurityReleaseReportParameters} from '../_models/securityReleaseReportParameters';
import {Session} from "../_session/session";

@Component({
    selector: 'app-security-release-report',
    templateUrl: './security-release-report.component.html',
    styleUrls: ['./security-release-report.component.css']
})
export class SecurityReleaseReportComponent {

    parameters: SecurityReleaseReportParameters = new SecurityReleaseReportParameters();
    form: FormGroup = null;

    constructor(private fb: FormBuilder, private session: Session) {
        this.form = new FormGroup({
            'reportType': new FormControl("", [Validators.required]),
            'date': new FormControl(new Date().toISOString())
        }, {validators: releaseReportValidator});
    }

    onRunReport() {
        let link: string = this.session.getProperty("securityReleaseBase");

        let date: string = this.form.get('date').value;
        let mm = date.slice(5, 7);
        let dd = date.slice(8, 10);
        let yyyy = date.slice(0, 4);

        if(this.form.get('reportType').value < 4) {
            link = link + mm + "/" + dd + "/" + (Number.parseInt(yyyy)-this.form.get('reportType').value);
        }else if(this.form.get('reportType').value >= 4){
            for(let i=1;i<=7;i++){
                if(i>=4 || (i<4 && this.form.get('reportType').value == 7)){
                    link = link + mm + "/" + dd + "/" + (Number.parseInt(yyyy)-i);
                    if(i<7){ link = link + ";";}
                }
            }
        }
        window.open(this.session.getProperty("securityReleaseBase") + link, "_blank");
    }

}

export const releaseReportValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    if(control.get('reportType').value == null){
        return {'reportType': 'You must select a Report Type'};
    }
    if(control.get('date').value == null){
        return {'date': 'The Date field cannot be empty when the Report Type "All" is selected.'};
    }
    return null;
};
