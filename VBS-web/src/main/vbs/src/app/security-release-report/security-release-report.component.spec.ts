import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecurityReleaseReportComponent } from './security-release-report.component';

describe('SecurityReleaseReportComponent', () => {
  let component: SecurityReleaseReportComponent;
  let fixture: ComponentFixture<SecurityReleaseReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecurityReleaseReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecurityReleaseReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
