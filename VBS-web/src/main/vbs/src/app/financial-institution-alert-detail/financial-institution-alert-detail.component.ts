import { Component, OnInit, EventEmitter, Output, ViewChild, Input } from '@angular/core';
import { AlertType } from 'src/app/_models/alertType';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FinancialInstitutionAlert } from 'src/app/_models/financialInstitutionAlert';
import { Session } from '../_session/session';

@Component({
  selector: 'app-financial-institution-alert-detail',
  templateUrl: './financial-institution-alert-detail.component.html',
  styleUrls: ['./financial-institution-alert-detail.component.css']
})
export class FinancialInstitutionAlertDetailComponent implements OnInit {

  @Output() formClosed = new EventEmitter<FinancialInstitutionAlert>();

  @ViewChild('selectAlertType') selectAlertType = null; 

  alertTypes:AlertType[] = [];
  alert:FinancialInstitutionAlert = null;
  form:FormGroup = null;
  saving:boolean = false;

  @Input() modeTitle:string = "New Alert or Edit Alert";
  activateButtonText: string;

  constructor(private session:Session, private fb:FormBuilder) { }

  ngOnInit() {
    this.alertTypes = this.session.getProperty("alertTypes");
    // "note" type (4) is only for security
    this.alertTypes = this.alertTypes.slice(0, 3);

    
    this.alert = new FinancialInstitutionAlert();
    this.form = this.fb.group(this.alert);

    if (this.alert.endUser != null) {
      this.form.controls.description.disable();
      this.form.controls.alertType.disable();
      this.form.controls.startDate.disable();    
    } else {
      this.form.controls.description.enable();
      this.form.controls.alertType.enable();
      this.form.controls.startDate.enable();
    }
  }

  loadAlert(alert:FinancialInstitutionAlert) {
    if (alert != null) {      
      this.alert = alert;
      this.form.controls.id.setValue(alert.id);
      this.form.controls.alertType.setValue(alert.alertType.id);
      if (alert.name) {
        this.form.controls.name.setValue(alert.name);
      }
      if (alert.description) {
        this.form.controls.description.setValue(alert.description);
      }
      this.form.controls.createDate.setValue(alert.createDate);
      this.form.controls.createUser.setValue(alert.createUser);
      if (alert.startDate) {
        this.form.controls.startDate.setValue(alert.startDate);
      }
      if (alert.endUser) {
        this.activateButtonText = "Activate";
        this.form.disable();        
      } else {
        this.activateButtonText = "Deactivate";
        this.form.enable();
      }
    }
  }

  newAlert(financialInstitutionId:number) {    
    this.alert = new FinancialInstitutionAlert();
    this.alert.createDate = (new Date()).toISOString();
    this.alert.financialInstitutionId = financialInstitutionId;

    this.form.reset();

    let d = new Date();
    d.setHours(0,0,0,0);
    if(d.getTimezoneOffset() != 0){
      d.setMinutes(d.getMinutes() + -1*d.getTimezoneOffset());
    }

    this.form.enable();
    this.form.controls.startDate.setValue(d.toISOString());
    this.form.controls.createUser.setValue(this.session.getUser().userId);
    this.form.controls.createUser.disable();
    this.form.controls.createDate.setValue(d.toISOString());
    this.form.controls.createDate.enable();
  
    this.activateButtonText = "Deactivate";
  }

  save() {
    this.alert.id = this.form.controls.id.value;
    // FI id is already set
    let typeId = this.form.controls.alertType.value;
    this.alertTypes.forEach(element => {
      if (typeId == element.id) {
        this.alert.alertType = element;
      }
    });
    this.alert.name = this.form.controls.name.value;   
    this.alert.description = this.form.controls.description.value;   
    this.alert.createUser = this.form.controls.createUser.value; 
    this.alert.startDate = this.form.controls.startDate.value;
    if (this.alert.endUser == null) {
      this.alert.status = 'ACTIVE';
    } else {
      this.alert.status = 'INACTIVE';
    }
    this.formClosed.emit(this.alert);
  }

  cancel() {
    this.formClosed.emit(null);
  }

  toggleActive() {
    if (this.alert.endUser == null) {
      let d = new Date();
      this.alert.endUser = this.session.getUser().userId;
      this.activateButtonText = "Activate";
      this.form.disable();      
    } else {
      this.alert.endDate = null;
      this.alert.endUser = null;
      this.activateButtonText = "Deactivate";
      this.form.enable();      
    }
  }
}
