import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialInstitutionAlertDetailComponent } from './financial-institution-alert-detail.component';

describe('FinancialInstitutionAlertDetailComponent', () => {
  let component: FinancialInstitutionAlertDetailComponent;
  let fixture: ComponentFixture<FinancialInstitutionAlertDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinancialInstitutionAlertDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialInstitutionAlertDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
