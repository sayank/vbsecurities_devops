import {Component, OnInit} from '@angular/core';

import {CashSecurityNoInterest} from '../_models/cashSecurityNoInterest';
import {InterestService} from '../_services/interest.service';
import {Session} from '../_session/session';

@Component({
  selector: 'app-interest-report',
  templateUrl: './interest-report.component.html',
  styleUrls: ['./interest-report.component.css']
})
export class InterestReportComponent implements OnInit {

  results: CashSecurityNoInterest[];
	searching: boolean = false;
	downloadingExcel: boolean = false;

  constructor(private interestService: InterestService, private session: Session) {     
  }

	ngOnInit(): void {
		this.searchCashSecuritiesWithoutInterest();
	}

	searchCashSecuritiesWithoutInterest() {
		this.searching = true;
		this.results = null;
		this.interestService.searchCashSecuritiesWithoutInterest().subscribe(
			securitiseWithoutInterest => {
				this.results = securitiseWithoutInterest;
        this.searching = false;
			},
			error => {
				this.session.addError('Error when searching securities without interest', error);
				this.results = [];
			}
		);
	}

	exportToExcel() {
		this.downloadingExcel = true;
		this.interestService.searchNoInterestResultsExcel().subscribe(data => {
                        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
						let d = new Date();
						const element = document.createElement('a');
						element.href = URL.createObjectURL(blob);
						element.download = "no-interest-search-results-"+d.getTime()+".xlsx";
						document.body.appendChild(element);
						element.click();
						element.remove();
				},
				error => {console.log(error)},
				() => {this.downloadingExcel = false;});
	}

}
