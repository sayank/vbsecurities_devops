import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { NgbModule, NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './_layout/navbar/navbar.component';
import { FooterComponent } from './_layout/footer/footer.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { UserService } from './_services/user.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Session } from './_session/session';
import { AuthGuard } from './_guards/auth.guard';
import { SecurityComponent } from './security/security.component';
import { VbsCurrencyPipe } from './_pipes/vbsCurrency.pipe';
import { CurrencyMaskDirective } from './_directives/currencyFormatter.directive';
import { DeleteConfirmationComponent } from './delete-confirmation/delete-confirmation.component';
import { SearchSecurityComponent } from './search-security/search-security.component';
import { ShowValidation } from './_directives/showValidation.directive';
import { NgbUTCStringAdapter } from './_util/ngbUTCStringAdapter';
import { VbsHttpInterceptor } from './_session/vbsHttpInterceptor';
import { DateFormatPipe } from './_pipes/dateFormat.pipe';
import { SecurityParentComponent } from './security-parent/security-parent.component';
import { SecurityService } from './_services/security.service';
import { Location } from '@angular/common';
import { HomesCoveredComponent } from './homes-covered/homes-covered.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { HasPermission } from './_directives/hasPermission.directive';
import { FinancialInstitutionSearch } from './_components/financialInstitutionSearch/financial-institution-search.component';
import { SecurityTypePurpose } from './_components/securityTypePurpose/security-type-purpose.component';
import { UtilService } from './_services/util.service';
import { ReleaseComponent } from './release/release.component';
import { Nl2BrPipe } from './_pipes/nl2br.pipe';
import { ReleaseService } from './_services/release.service';
import { ConfirmationModal } from './_components/confirmationModal/confirmation-modal.component';
import { ModalService } from './_services/modal.service';
import { InterestComponent } from './interest/interest.component';
import { InterestService } from './_services/interest.service';
import { EscrowAgentSearchComponent } from './_components/escrow-agent-search/escrow-agent-search.component';
import { IndexComponent } from './index/index.component';
import { InstrumentNumberSearchComponent } from './_components/instrument-number-search/instrument-number-search.component';
import { EstimatedInterestAccrualsComponent } from './estimated-interest-accruals/estimated-interest-accruals.component';
import { EnrolmentsSearchComponent } from './_components/enrolments-search/enrolments-search.component';
import { ContactSearchComponent } from "./_components/contactSearch/contact-search.component";
import { CreateSecurityComponent } from './_components/create-security/create-security.component';
import {DisableControlDirective} from "./_directives/disableControl.directive";
import {ContactFormatterDirective} from "./_directives/contactFormatter.directive";
import { CorrespondenceComponent } from './correspondence/correspondence.component';
import { ClipboardModule } from 'ngx-clipboard';
import {InputCopyButtonDirective} from "./_directives/inputCopyButton.directive";
import {LacksPermission} from "./_directives/lacksPermission.directive";
import { ReleaseReasonComponent } from './_components/release-reason/release-reason.component';
import { FinancialInstitutionComponent } from './financial-institution/financial-institution.component';
import { PendingReleaseSummaryComponent } from './pending-release-summary/pending-release-summary.component';
import { FinancialInstitutionSearchComponent } from './financial-institution-search/financial-institution-search.component';
import { AutoReleaseComponent } from './auto-release/auto-release.component';
import { AutoReleaseResultComponent } from './auto-release-result/auto-release-result.component';
import { PendingReleaseReportComponent } from './pending-release-report/pending-release-report.component';
import { AlertComponent } from './alert/alert.component';
import { NgxTrimDirectiveModule } from 'ngx-trim-directive';
import {ManifestUtil} from "./_util/manifestUtil";
import {FinancialInstitutionAlertComponent} from "./financial-institution-alert/financial-institution-alert.component";
import {FinancialInstitutionAlertDetailComponent} from "./financial-institution-alert-detail/financial-institution-alert-detail.component";
import {MaaDetailsComponent} from "./_components/maa-details/maa-details.component";
import { SecurityReleaseReportComponent } from './security-release-report/security-release-report.component';
import { EnrolmentIndicatorsComponent } from './_components/enrolment-indicators/enrolment-indicators.component';
import { DeleteReasonComponent } from './_components/delete-reason/delete-reason.component';
import { SearchVbComponent } from './_components/search-vb/search-vb.component';
import { InterestReportComponent } from './interest-report/interest-report.component';
import { SearchDeletedSecurityComponent } from './search-deleted-security/search-deleted-security.component';
import { EnrolmentActivitiesComponent } from './_components/enrolment-activities/enrolment-activities.component';
import {CeEnrolmentComponent} from "./ce-enrolment/ce-enrolment.component";
import { CapitalCasePipe } from './_pipes/capitalCase.pipe';


@NgModule({
  declarations: [
	IndexComponent,
	AppComponent,
	NavbarComponent,
	FooterComponent,
	LoginComponent,
	SecurityComponent,
	VbsCurrencyPipe,
	CurrencyMaskDirective,
	DeleteConfirmationComponent,
	SearchSecurityComponent,
	ShowValidation,
	DateFormatPipe,
	SecurityParentComponent,
	HomesCoveredComponent,
	HasPermission,
    LacksPermission,
	FinancialInstitutionSearch,
	SecurityTypePurpose,
	ReleaseComponent,
	Nl2BrPipe,
	ConfirmationModal,
	InterestComponent,
	EscrowAgentSearchComponent,
	InstrumentNumberSearchComponent,
    ContactSearchComponent,
	InstrumentNumberSearchComponent,
	EstimatedInterestAccrualsComponent,
	EnrolmentsSearchComponent,
    CreateSecurityComponent,
    DisableControlDirective,
    ContactFormatterDirective,
    CorrespondenceComponent,
    InputCopyButtonDirective,
    ReleaseReasonComponent,
    FinancialInstitutionComponent,
	PendingReleaseSummaryComponent,
	FinancialInstitutionComponent,
    FinancialInstitutionSearchComponent,
    AutoReleaseComponent,
    AutoReleaseResultComponent,
    PendingReleaseReportComponent,
    AlertComponent,
    FinancialInstitutionAlertComponent,
    FinancialInstitutionAlertDetailComponent,
    MaaDetailsComponent,
    SecurityReleaseReportComponent,
    EnrolmentIndicatorsComponent,
    DeleteReasonComponent,
    SearchVbComponent,
    InterestReportComponent,
    SearchDeletedSecurityComponent,
    EnrolmentActivitiesComponent,
	CeEnrolmentComponent,
	CapitalCasePipe
  ],
  entryComponents: [
	  FinancialInstitutionSearch,
	  SecurityTypePurpose,
	  ConfirmationModal,
	  InstrumentNumberSearchComponent,
	  EscrowAgentSearchComponent,
	  EnrolmentsSearchComponent,
	  EscrowAgentSearchComponent,
	  ContactSearchComponent,
	  ReleaseReasonComponent,
      MaaDetailsComponent,
      EnrolmentIndicatorsComponent,
      EnrolmentActivitiesComponent,
      DeleteReasonComponent,
      SearchVbComponent
  ],
  imports: [
	NgbModule.forRoot(),
	BrowserModule,
	RouterModule,
	AppRoutingModule,
	FormsModule,
	ReactiveFormsModule,
	HttpClientModule,
	NgxDatatableModule,
    ClipboardModule,
    NgxTrimDirectiveModule
  ],
  providers: [UserService, AuthGuard, Session, VbsCurrencyPipe, SecurityService, UtilService, ModalService, ReleaseService, InterestService, ManifestUtil,
	Location,
	{provide: NgbDateAdapter, useClass: NgbUTCStringAdapter},
	{provide: HTTP_INTERCEPTORS, useClass: VbsHttpInterceptor, multi: true},
	{provide: LOCALE_ID, useValue: 'en_CA' },
	CapitalCasePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
