import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './_guards/auth.guard';
import { SearchSecurityComponent } from './search-security/search-security.component';
import { SecurityParentComponent } from './security-parent/security-parent.component';
import { InterestComponent } from './interest/interest.component';
import { IndexComponent } from './index/index.component';
import { CanDeactivateGuard } from './_guards/can-deactivate.guard';
import { CreateSecurityComponent } from './_components/create-security/create-security.component';
import { CorrespondenceComponent } from './correspondence/correspondence.component';
import { FinancialInstitutionComponent } from './financial-institution/financial-institution.component';
import { PendingReleaseSummaryComponent } from './pending-release-summary/pending-release-summary.component';
import { FinancialInstitutionSearchComponent } from './financial-institution-search/financial-institution-search.component';
import { AutoReleaseComponent } from './auto-release/auto-release.component';
import { AutoReleaseResultComponent } from './auto-release-result/auto-release-result.component';
import { PendingReleaseReportComponent } from './pending-release-report/pending-release-report.component';
import { SecurityReleaseReportComponent } from './security-release-report/security-release-report.component';
import { InterestReportComponent } from './interest-report/interest-report.component';
import { SearchDeletedSecurityComponent } from './search-deleted-security/search-deleted-security.component';
import {CeEnrolmentComponent} from "./ce-enrolment/ce-enrolment.component";


const routes: Routes = [
	{path: 'login', component: LoginComponent},
	{path: 'security', component: SecurityParentComponent, canActivate: [AuthGuard], canDeactivate: [CanDeactivateGuard]},
	{path: 'searchSecurity', component: SearchSecurityComponent, canActivate: [AuthGuard]},
	{path: 'createSecurity', component: CreateSecurityComponent, canActivate: [AuthGuard]},
	{path: 'interest', component: InterestComponent, canActivate: [AuthGuard]},
	{path: 'interestReport', component: InterestReportComponent, canActivate: [AuthGuard]},
	{path: 'correspondence', component: CorrespondenceComponent, canActivate: [AuthGuard], data: {expectedPermission: "CORRESPONDENCE"}},
	{path: 'financialInstitution', component: FinancialInstitutionComponent, canActivate: [AuthGuard], canDeactivate: [CanDeactivateGuard]},
	{path: 'financialInstitutionSearch', component: FinancialInstitutionSearchComponent, canActivate: [AuthGuard]},
	{path: 'pendingReleaseSummary', component: PendingReleaseSummaryComponent, canActivate: [AuthGuard]},
	{path: 'pendingReleaseReport', component: PendingReleaseReportComponent, canActivate: [AuthGuard]},
	{path: 'autoRelease', component: AutoReleaseComponent, canActivate: [AuthGuard]},
	{path: 'autoReleaseResult', component: AutoReleaseResultComponent, canActivate: [AuthGuard]},
	{path: 'securityReleaseReport', component: SecurityReleaseReportComponent, canActivate: [AuthGuard]},
	{path: 'searchDeletedSecurity', component: SearchDeletedSecurityComponent, canActivate: [AuthGuard]},
    {path: 'ce-blanket', component: CeEnrolmentComponent, canActivate: [AuthGuard]},
	{path: '', component: IndexComponent, canActivate: [AuthGuard]},
	{path: '**', redirectTo: '/', canActivate: [AuthGuard]}
  ];

  @NgModule({
    imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
    exports: [RouterModule]
})
export class AppRoutingModule { }
