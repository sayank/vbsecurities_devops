import {ChangeDetectorRef, Component, HostListener, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, FormControl} from "@angular/forms";
import {UrlParameterService} from "../url-parameter.service";
import {Session} from "../_session/session";
import {SecurityService} from "../_services/security.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {catchError, map} from "rxjs/operators";
import {NgbModal, NgbTabset} from "@ng-bootstrap/ng-bootstrap";
import {Security} from "../_models/security";
import {NameDescription} from "../_models/nameDescription";
import {BehaviorSubject, of} from "rxjs";
import {HashUtil} from "../_util/hashUtil";
import {EscrowAgentLawyerAssistant} from '../_models/escrowAgentLawyerAssistant';
import {VbsModalResult} from "../_components/confirmationModal/vbs-modal-result";
import {ModalService} from "../_services/modal.service";
import {Enrolment} from "../_models/enrolment";
import {Contact} from "../_models/contact";


@Component({
  selector: 'app-security-parent',
  templateUrl: './security-parent.component.html',
  styleUrls: ['./security-parent.component.css']
})
export class SecurityParentComponent implements OnInit {

    form:FormGroup = null;
    tabErrors: any[] = [];
    saving:boolean = false;
    securityTypes: NameDescription[] = [];
    securityPurposes: NameDescription[] = [];
    hash:string;
    alertConfirmsShown: string[] = [];
    originalPoolId:number = 0;
    originalOriginalAmount:number = 0;
    originalCurrentAmount:number = 0;
    originalSecurityTypeId:number = null;
    originalSecurityPurposeId:number = null;
    originalUnitsToCover:number = 0;
    alertOpen:boolean = false;
    canDeactivate:boolean = true;
	validateSaveResult = new BehaviorSubject("");
	currentPool:number = 0;
    currentTab = "tab-security";

    @ViewChild("tabs") tabSet: NgbTabset;
    @ViewChild("securityTypeModal", {read: TemplateRef}) securityTypeModal: TemplateRef<any>;
    @ViewChild("securityTypePurposeCancelModal", {read: TemplateRef}) securityTypepurposeCancelModal: TemplateRef<any>;

    originalTypePurposeChangeConfirmed: boolean = false;

	constructor(private fb: FormBuilder, private modalService: ModalService, private ngbModal: NgbModal,
	            private urlParameterService: UrlParameterService, private session: Session,
                private securityService: SecurityService, private route: ActivatedRoute, private router: Router,
                private cdRef:ChangeDetectorRef) {


	    this.securityTypes = this.session.getProperty("securityTypes");
	    this.securityPurposes = this.session.getProperty("securityPurposes");
	}

	ngOnInit(): void {
	    this.route.queryParams.subscribe((params: Params) => {
            if(params['id'] !== undefined){
                this.securityService.getSecurity(params['id']).subscribe(
                    security => {
                        this.formSetup(security);
                        this.route.queryParams.pipe(map(params => params['tab'])).subscribe(tab => {
                            if(tab != null) {
                                setTimeout(() => this.tabSet.select(tab),50);
                                this.currentTab = tab;
                            }
                        });
                    },
                    error => this.session.addError('Server error on loading security', error)
                );
            }else{
                let sec = this.session.getCreateSecurity();
                this.formSetup((sec != null) ? sec : new Security());
            }
		});
    }

	formSetup(security:Security): void{
	    this.hash = null;
	    this.canDeactivate = true;

        if(security.id != null){
            this.originalOriginalAmount = security.originalAmount;
            this.originalUnitsToCover = security.unitsToCover;
            this.originalPoolId = security.poolId;
            this.originalCurrentAmount = security.currentAmount;
            this.originalSecurityPurposeId = security.securityPurposeId;
            this.originalSecurityTypeId = security.securityTypeId;
            this.alertConfirmsShown = [];
        }

	    let form = this.fb.group(security);
	    form.setControl("enrolments", this.fb.array([]));
        if(security.enrolments.length > 0){
            security.enrolments.forEach(e => (form.get('enrolments') as FormArray).push(this.fb.group(e)));
        }else{
           (form.get('enrolments') as FormArray).push(this.fb.group(new Enrolment()));
        }
        form.setControl("vbList", this.fb.array([]));
        security.vbList.forEach(v => {
            let g = this.fb.group(v);
            (form.get('vbList') as FormArray).push(g);
        });

        form.setControl("jointBondSecurities", this.fb.array([]));
        if (security.jointBondSecurities != null) {
            security.jointBondSecurities.forEach(v => (form.get('jointBondSecurities') as FormArray).push(this.fb.group(v)));
        } else {
            security.jointBondSecurities = [];
        }

        form.valueChanges.subscribe(v => {
            if(this.hash != null){
                this.checkOriginalAmount();
                this.canDeactivate = (HashUtil.getHash(this.form.getRawValue()) == this.hash) || this.form.get('deleted').value == true;
			}
            this.cdRef.detectChanges();
        });
        if(security.securityStatusId == null){
            form.get('securityStatusId').patchValue(1, {emitEvent: false});
        }
	    form.setControl("alerts", this.fb.array(security.alerts));
        form.setControl("financialInstitutionAndBranch", this.fb.group(security.financialInstitutionAndBranch));
		form.setControl("escrowAgentLawyerAssistant", this.fb.group((security.escrowAgentLawyerAssistant == null) ? new EscrowAgentLawyerAssistant() : security.escrowAgentLawyerAssistant));

        this.disableFormFields(form);
        this.hash = HashUtil.getHash(form.getRawValue());
        this.validateSaveResult.next("");
	    this.form = form;
        this.form.updateValueAndValidity();
        this.cdRef.detectChanges();
		this.currentPool = this.form.get('poolId').value;
        this.originalTypePurposeChangeConfirmed = false;
    }

    private disableFormFields(form:FormGroup):void {
		if(!this.session.getUser().hasPermission("CREATE_SECURITY")){
			form.disable();
		}
		else{
			if(form.get('fullyReleased').value == true || form.get('deleted').value) {
				form.disable();
				form.get('comment').enable();
				form.get('alerts').enable();
			}else {
				form.enable();
                if(form.get('securityTypeId').value != 3 && form.get('securityTypeId').value != 5){
                    form.get('currentAmount').disable();
                }
				if(form.get('id').value != null){
					if(form.get('securityTypeId').value == 1){
						form.get('securityTypeId').disable();
						form.get('originalAmount').disable();
						form.get('receivedDate').disable();
						form.get('issuedDate').disable();
						form.get('instrumentNumber').disable();
                        for(let i=0;i<form.get('vbList').value.length;i++){
                            form.get('vbList.'+i+'.primaryVb').disable();
					    }
                    }
				}else{
					form.get('poolId').disable();
				}
			}
        }      
        if (form.get('fullyReleased').value && this.session.getUser().hasPermission('CHANGE_SECURITY_STATUS')) {
            form.get('securityStatusId').enable();
        }
	}

    validateDeleteSecurity(): void {
        this.saving = true;
        this.tabErrors = [];
        if(this.securityHasId) {
            this.securityService.validateDeleteSecurity(this.form.get('id').value).pipe(catchError(error => of(error))).subscribe(async result => {
                if (result != null) {
                    if (result.status === 422) {
                        let hasValidationErrors = await this.hasValidationErrors(result);
                        if(!hasValidationErrors){
                            this.delete();
                        }
                    } else {
                        this.session.addError('Server error while validating.', result);
                        this.saving = false;
                    }
                } else {
                    this.delete();
                }
            });
        }
    }

    private delete():void{
        this.modalService.confirm({title: "Delete Security", body: "Are you sure you want to delete this Security?", size: "lg", allowDismiss: false}).subscribe(
            confirmation => {
                if(confirmation.result){
                    this.securityService.deleteSecurity(this.form.get('id').value).subscribe(
                        result => this.router.navigate(['/searchSecurity']),
                        error => { this.session.addError('Server error on deleting Security', error); this.saving = false; }
                    );
                } else {
                    this.saving = false;
                }
            }
        );
    }

    validate(): void {
	    this.saving = true;
	    this.tabErrors = [];
        if(!this.form.get('monthlyReport').value){
			this.form.get('monthlyDate').patchValue(null);
		}
        this.confirmOriginalAmountAndPool().then(resolved => {
            this.confirmCurrentAmount().then(resolved => {
                this.confirmSecurityTypeChange().then( resolved => {
                    this.securityService.saveSecurity(this.form.getRawValue(), this.getAlertConfirmString()).subscribe(
                        security => {
                            if(this.form.get('id').value == null){
                                this.urlParameterService.setUrlParameter('id', security.id + '');
                            }
                            this.formSetup(security);
                            this.validateSaveResult.next('pass');
                            this.saving = false;
                            this.alertConfirmsShown = [];
                        }, error => {

                            if (error.status === 422) {
                                let hasValidationErrors = this.hasValidationErrors(error);
                                hasValidationErrors.then(value => {
                                    if(!value){
                                        this.validate();
                                    }else{
                                        this.saving = false;
                                    }
                                });
                            } else {
                                this.session.addError('Server error while updating the security.', error);
                                this.saving = false;
                            }
                        }
                    );
                })
            }, rejected => {
                this.form.controls['currentAmount'].patchValue(this.originalCurrentAmount);
                this.validateSaveResult.next('fail');
                this.saving = false;
            })
        },rejected => {

            this.saving = false;
        });
    }

    triggerSave(){
	    this.validate();
    }

    triggerReload(){
        this.securityService.getSecurity(this.form.get('id').value).subscribe(security => this.formSetup(security));
    }

    private addTabError(error:any):void {
	    let fieldName:string = error.errorFieldName;
	    if(fieldName.startsWith("enrolments.")){
            if(this.tabErrors["tab-homes-covered"] == undefined){ this.tabErrors["tab-homes-covered"] = 1; }else{ this.tabErrors["tab-homes-covered"]++; }
        }else if(fieldName.startsWith("alerts.")){
            if(this.tabErrors["tab-alerts"] == undefined){ this.tabErrors["tab-alerts"] = 1; }else{ this.tabErrors["tab-alerts"]++; }
        }else{
	        if(this.tabErrors["tab-security"] == undefined){ this.tabErrors["tab-security"] = 1; }else{ this.tabErrors["tab-security"]++; }
        }
    }

    confirmOriginalAmountAndPool(): Promise<any> {
        return new Promise((resolve, reject) => {
            //if(this.securityHasId && this.originalOriginalAmount < this.form.get('originalAmount').value && this.originalUnitsToCover == this.form.get('unitsToCover').value){
               // this.modalService.confirm({title: "Original Amount Increased", body: "Do you want to change the no. of unit intended to cover?", size: "lg", allowDismiss: false}).subscribe(
                 //   confirmation => {
                   //     if(confirmation.result){
                     //       reject();
                      //  }else{
                       //     this.confirmPoolId(resolve);
                       // }
                   // }
                //)
            //}else{
                this.confirmPoolId(resolve);
            //}
        });
    }

    private confirmSecurityTypeChange(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.securityHasId && (!this.isSecurityType(this.originalSecurityTypeId) || !this.isSecurityPurpose(this.originalSecurityPurposeId)) && !this.originalTypePurposeChangeConfirmed) {
                let warningModalRef = this.ngbModal.open(this.securityTypeModal);
                warningModalRef.result.then(
                    result => {
                    this.originalTypePurposeChangeConfirmed = true;
                    resolve();
                    },
                reason => {
                    this.form.get('securityTypeId').patchValue(this.originalSecurityTypeId);
                    this.form.get('securityPurposeId').patchValue(this.originalSecurityPurposeId);
                    this.ngbModal.open(this.securityTypepurposeCancelModal).result.then(
                        (result) => resolve(),
                        (reason) => {
                            reject();
                            this.saving = false;
                        });
                    });
            } else {
                resolve();
            }
        });
    }

    private confirmPoolId(resolve: (value?: any) => void) {
        if (this.securityHasId && this.originalPoolId != this.form.get('poolId').value) {
            this.modalService.confirm({ title: "Link to Another Pool", body: "VBs, Homes, and Units Intended to Cover will be loaded from the new Pool Number. Please click Yes to continue or No to cancel the change.", size: "lg", allowDismiss: false }).subscribe(confirmation => {
                if (!confirmation.result) {
                    this.form.get('poolId').patchValue(this.originalPoolId, {emitEvent: false});
                }
                resolve();
            });
        }
        else {
            resolve();
        }
    }

    private confirmCurrentAmount(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.securityHasId && this.originalCurrentAmount != this.form.get('currentAmount').value) {
                if(this.isSecurityType(5)) {
                    this.modalService.confirm({ title: "Current Amount has changed", body: "Current Amount has changed. Please add a manual activity to record this change. Please click Yes to continue or No to cancel the change.", size: "lg", allowDismiss: false }).subscribe(confirmation => {
                        if(confirmation.result){
                            resolve();
                        }else{
                            reject();
                        }
                    });
                }else {
                    resolve();
                }
            } else { resolve(); }
        });
    }

    private checkOriginalAmount() {
        let newOriginalAmount = Number.parseFloat(this.form.get('originalAmount').value + '');
        if(this.securityHasId && newOriginalAmount < this.originalOriginalAmount){
             this.modalService.alert({title: "Original Amount",
                 body: "You are not allowed to decrease the Original amount of security, any decrease should go through Reduction/Release", size: "lg", alertOnly: true });
            this.form.get('originalAmount').patchValue(this.originalOriginalAmount, {emitValue: false, onlySelf: true});
        } else {
            if(!this.securityHasId && !this.isSecurityType(3) && !this.isSecurityType(5)){
                if(this.form.get('currentAmount').value != this.form.get('originalAmount').value){
                    this.form.get('currentAmount').patchValue(this.form.get('originalAmount').value, {onlySelf:true});
                }
            }else if(this.originalOriginalAmount != this.form.get('originalAmount').value && (this.isSecurityType(4) || this.isSecurityType(6))){
                let currentAmount = Number.parseFloat(this.form.get('currentAmount').value + '');
                let amountDifference = this.originalOriginalAmount - currentAmount;
                this.form.get('currentAmount').patchValue(Number.parseFloat((newOriginalAmount - amountDifference).toFixed(2)), {emitValue: false, onlySelf: true});
                this.originalOriginalAmount = newOriginalAmount;
            }
        }
    }

    private async hasValidationErrors(result:any):Promise<boolean>{
        let arr = {};
        let vbCount = 0;
        let errorList = (result.error.validationErrorList != undefined) ? result.error.validationErrorList : result.error;
        for(let i=0; i<errorList.length; i++){
            if (errorList[i].errorType === undefined || errorList[i].errorType == 'VALIDATION_FAIL') {
                if (arr[errorList[i].errorFieldName] === undefined) {
                    arr[errorList[i].errorFieldName] = [];
                }
                arr[errorList[i].errorFieldName].push(errorList[i].errorDescription);
                if(!errorList[i].errorFieldName.startsWith('vbList') || vbCount == 0){
                    this.addTabError(errorList[i]);
                    if(errorList[i].errorFieldName.startsWith('vbList')){
                        vbCount++;
                    }
                }
            } else if (errorList[i].errorType == 'ALERT') {
                await this.modalService.confirm({
                    title: errorList[i].errorTitle, body: errorList[i].errorMessage,
                    size: "lg", allowDismiss: false, alertOnly: true}).toPromise();
                this.alertConfirmsShown.push(errorList[i].errorReturnCode);
            } else if (errorList[i].errorType == 'ALERT_FAIL') {
                await this.modalService.confirm({
                    title: errorList[i].errorTitle, body: errorList[i].errorMessage,
                    size: "lg", allowDismiss: false, alertOnly: true}).toPromise();
                this.alertConfirmsShown.push(errorList[i].errorReturnCode);
                if (arr[errorList[i].errorFieldName] === undefined) {
                    arr[errorList[i].errorFieldName] = [];
                }
                arr[errorList[i].errorFieldName].push(errorList[i].errorDescription);
                this.addTabError(errorList[i]);
            } else if (errorList[i].errorType == 'CONFIRM') {
                let c: VbsModalResult = await this.modalService.confirm({
                    title: errorList[i].errorTitle, body: errorList[i].errorMessage,
                    size: "lg", allowDismiss: false}).toPromise();
                if (!c.result) {
					if('DUPLICATE_RELEASE_DEMAND_FIRST' == errorList[i].errorReturnCode || 'DUPLICATE_RELEASE_FIRST'  == errorList[i].errorReturnCode || 'DUPLICATE_DEMAND_FIRST' == errorList[i].errorReturnCode){
						this.form.get('poolId').patchValue(this.currentPool, {emitEvent: false});
					}
					if('DTA_ORIGINAL_AMOUNT_CHANGE' == errorList[i].errorReturnCode){
						this.form.get('originalAmount').patchValue(this.originalOriginalAmount, {emitEvent: false});
					}
					if('DTA_CURRENT_AMOUNT_CHANGE' == errorList[i].errorReturnCode){
						this.form.get('currentAmount').patchValue(this.originalCurrentAmount, {emitEvent: false});
						
					}
                    if (arr[errorList[i].errorFieldName] === undefined) {
                        arr[errorList[i].errorFieldName] = [];
                    }
                    arr[errorList[i].errorFieldName].push(errorList[i].errorDescription);
                    this.addTabError(errorList[i]);
                } else {
                    this.alertConfirmsShown.push(errorList[i].errorReturnCode);
                }
            }else if (errorList[i].errorType == 'CONFIRM_SAVE') {
                let c: VbsModalResult = await this.modalService.confirm({
                    title: errorList[i].errorTitle, body: errorList[i].errorMessage,
					size: "lg", allowDismiss: false}).toPromise();
					if (!c.result) {
					
						if (arr[errorList[i].errorFieldName] === undefined) {
							arr[errorList[i].errorFieldName] = [];
						}
						arr[errorList[i].errorFieldName].push(errorList[i].errorDescription);
						this.addTabError(errorList[i]);
					}
				 this.alertConfirmsShown.push(errorList[i].errorReturnCode);				 
            }
        }

        if(Object.keys(arr).length){
            for (let field in arr) {
                if (field != null && this.form.get(field) != null) {
                    this.form.get(field).setErrors({'serverErrors': arr[field]});
                }
            }
            this.validateSaveResult.next('fail');
            this.saving = false;
            return true;
        }else{
            return false;
        }
    }

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (!this.canDeactivate) {
            $event.returnValue = "This message is displayed to the user in IE and Edge when they navigate without using Angular routing (type another URL/close the browser/etc)";
        }
    }

    /**
     * Helper Methods
     */
    hasAlertType(id:number):boolean {
        return this.form != null && this.form.getRawValue().alerts.find(a => (a.alertTypeId == id && a.endDate == null)) !== undefined;
    }

    showSaveButton(): boolean {
        return !this.session.getUser().hasPermission("CREATE_DEMAND_FINANCE") && !this.session.getUser().hasNoPermissions() && this.tabSet != null && this.tabSet.activeId != null &&
            !this.tabSet.activeId.includes('tab-release') && !this.tabSet.activeId.includes('tab-interest')
            && !(this.tabSet.activeId.includes('tab-alerts') && this.alertOpen);
    }



    onTabChange(event): void {
        this.urlParameterService.setUrlParameter('tab', event.nextId);
        this.currentTab = event.nextId;
        setTimeout(function(){}, 10);
    }

    isSecurityType(typeId:number): boolean {
	    if(this.form == null){
	        return false;
        }
	    return this.form.get('securityTypeId').value == typeId;
    }

    isSecurityPurpose(purposeId:number): boolean {
        if(this.form == null){
            return false;
        }
        return this.form.get('securityPurposeId').value == purposeId;
    }

    get securityHasId(): boolean {
        if(this.form == null){
            return false;
        }
        return this.form.get('id').value != null;
    }

    hasPermission(permission: string) {
        return this.session.getUser().hasPermission(permission);
    }

    get primaryVbNumber(): string {
        let vbList:Contact[] = this.form.getRawValue().vbList;
        if(vbList.length > 0){
            for(let i=0;i<vbList.length;i++){
                if(vbList[i].deleteReason == null && vbList[i].primaryVb){
                    return vbList[i].crmContactId;
                }
            }
        }
        return "";
    }

    getSecurityStatus() {
		if(this.form.get('deleted').value){
            return 'Deleted';
        }else if(!this.form.get('allocated').value && (this.form.get('securityTypeId').value == 1 || this.form.get('securityTypeId').value == 3)){
            return 'Not Allocated';
        }else if(this.form.get('fullyReleased').value){
            return 'Fully Released';
        }else if(!this.isSecurityType(3) && (this.form.get('originalAmount').value == null || this.form.get('originalAmount').value == 0)){
            return 'Closed';
        }else{
            return (this.form.get('originalAmount').value == this.form.get('currentAmount').value) ? 'Allocated' : 'Partially Released';
        }
    }

    private getAlertConfirmString():string{
        if(this.alertConfirmsShown.length > 0) {
            if(this.alertConfirmsShown.length == 1){
                return this.alertConfirmsShown[0];
            }else {
                let s = "";
                for (let i = 0; i < this.alertConfirmsShown.length; i++) {
                    s = s + this.alertConfirmsShown[i];
                    if(i < this.alertConfirmsShown.length-1){
                        s = s + "-";
                    }
                }
                return s;
            }
        }
        return null;
    }

    securityTypeDescription(securityTypeId:number):string{
        return this.securityTypes.find(t => t.id == securityTypeId).description;
    }

    securityPurposeDescription(securityPurposeId:number):string{
        return this.securityPurposes.find(p => p.id == securityPurposeId).description;
    }

    encodeURI(s:string):string{
        return encodeURI(s);
    }
}
