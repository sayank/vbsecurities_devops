import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecurityParentComponent } from './security-parent.component';

describe('SecurityParentComponent', () => {
  let component: SecurityParentComponent;
  let fixture: ComponentFixture<SecurityParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecurityParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecurityParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
