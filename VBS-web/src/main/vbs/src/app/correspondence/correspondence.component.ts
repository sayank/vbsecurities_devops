import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CorrespondenceService } from '../_services/correspondence.service';
import { NameDescription } from '../_models/nameDescription';
import { GenerateCorrespondence } from '../_models/generateCorrespondence';
import { Session } from '../_session/session';

@Component({
	selector: 'app-correspondence',
	templateUrl: './correspondence.component.html',
	styleUrls: ['./correspondence.component.css']
})
export class CorrespondenceComponent implements OnInit {

	form: FormGroup;
	generating = false;
	templates: NameDescription[] = [];

	constructor(private fb: FormBuilder, private correspondenceService: CorrespondenceService, private session: Session) { }

	ngOnInit() {
		this.form = this.fb.group(new GenerateCorrespondence());
		this.templates = this.session.getProperty("correspondenceTemplates");
	}

	generate() {
		this.correspondenceService.generate(this.form.value).subscribe(
			result => {
				let template = this.templates.find(t => t.id == result.templateId);
				let message = 'Generated correspondence. Security #: ' + result.securityId + ', template: ';
				if(template != null) {
					message += template.name;
				}
				this.session.addMessage(message);
			},
			error => this.session.addError('Failed to generate correspondence', error)			
		);
	}
}
