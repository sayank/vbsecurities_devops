import {Component, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from "@angular/forms";
import {Session} from "../_session/session";
import {Observable, of} from "rxjs";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {NgbModal, NgbTabset} from "@ng-bootstrap/ng-bootstrap";

import {FinancialInstitution} from "../_models/financialInstitution";
import {FinancialInstitutionService} from 'src/app/_services/financial-institution.service';
import {Branch} from '../_models/branch';
import {FinancialInstitutionType} from '../_models/financialInstitutionType';
import {HashUtil} from '../_util/hashUtil';
import {CanDeactivateGuard} from '../_guards/can-deactivate.guard';
import {ContactFinancialInstitution} from '../_models/contactFinancialInstitution';
import {ContactSearchComponent} from '../_components/contactSearch/contact-search.component';
import {FinancialInstitutionSigningOfficer} from '../_models/financialInstitutionSigningOfficer';
import {isArray} from 'util';
import {FinancialInstitutionMaaPoa} from '../_models/financialInstitutionMaaPoa';
import {FinancialInstitutionMaaPoaInstitution} from '../_models/financialInstitutionMaaPoaInstitution';
import {FinancialInstitutionSearch} from '../_components/financialInstitutionSearch/financial-institution-search.component';
import {catchError} from 'rxjs/operators';
import {UrlParameterService} from '../url-parameter.service';
import {Contact} from '../_models/contact';
import {FIActivity} from "../_models/fiActivity";

@Component({
	selector: 'app-financial-institution',
  	templateUrl: './financial-institution.component.html',
  	styleUrls: ['./financial-institution.component.css']
})
export class FinancialInstitutionComponent implements OnInit {

	form:FormGroup;
	saving:boolean = false;
	financialInstitution:FinancialInstitution;
	branches:Branch[];
	financialInstitutionTypes:FinancialInstitutionType[];
	crmCompanyUrl: string = '';
	crmCreateCompanyUrl: string = '';
	contentNavigatorUrl: string = '';
	hash: string;
	activities: FIActivity[];
	availableBranchesInFinancialInstitution:Map<number, Array<Branch>> = new Map();
	selectedTab: string = null;
	editForm:FormGroup = null;
	visible: boolean[] = [true, true, true, true];
	@ViewChild("tabs") tabSet: NgbTabset;

	constructor(
		private session:Session, private fb:FormBuilder, private route: ActivatedRoute, 
		private financialInstitutionService: FinancialInstitutionService, 
		private router: Router, 
		private canDeactivateGuard: CanDeactivateGuard, 
		private modalService: NgbModal, 
		private urlParameterService: UrlParameterService
	) { }

	ngOnInit() {
		this.branches = [];
		this.activities = [];

        this.crmCompanyUrl = this.session.getProperty("crmCompanyUrl");
        this.crmCreateCompanyUrl = this.session.getProperty("crmCreateCompanyUrl");
        this.financialInstitutionTypes = this.session.getProperty("financialInstitutionTypes");
        this.contentNavigatorUrl = this.session.getProperty("contentNavigatorFIUrl");

		this.route.queryParams.subscribe((params: Params) => {
			if(params['id'] !== undefined) {
				let financialInstitutionId = params['id'];
				this.loadExistingFinancialInstitution(financialInstitutionId);
			} else {
				this.financialInstitution = new FinancialInstitution();
				let d = new Date('2099-12-31');
				d.setHours(0,0,0,0);
				if(d.getTimezoneOffset() != 0){
					d.setMinutes(d.getMinutes() + -1*d.getTimezoneOffset());
				}
				this.financialInstitution.expiryDate = d.toISOString();
				this.form = this.fb.group(this.financialInstitution);
				this.hash = HashUtil.getHash(this.form.value);
			}
		});
	}

	loadExistingFinancialInstitution(financialInstitutionId: number) {
		this.financialInstitutionService.getFinancialInstitution(financialInstitutionId).subscribe(
			financialInstitution => {
				this.financialInstitution = financialInstitution;
				this.form = this.fb.group(this.financialInstitution);
				if(financialInstitution.contacts && isArray(financialInstitution.contacts)) {
					this.form.setControl('contacts', this.fb.array((financialInstitution.contacts.length > 0) ? this.createGroupsContacts(financialInstitution.contacts) : []));
				}
				if(financialInstitution.maaPoas && isArray(financialInstitution.maaPoas)) {
					this.form.setControl('maaPoas', this.fb.array((financialInstitution.maaPoas.length > 0) ? this.createGroupsMaaPoas(financialInstitution.maaPoas) : []));
				}

				this.loadAvailableBranches();

				this.hash = HashUtil.getHash(this.form.value);

				this.financialInstitutionService.getBranches(financialInstitutionId).subscribe(
					branches => {		
						this.branches = branches;
					},
					error => this.session.addError('Server error on loading Financial Institution Branches', error)
				);

				this.route.queryParams.subscribe((params: Params) => {
					if((this.selectedTab == null || this.selectedTab == '') && params['tab'] !== undefined) {
						this.selectedTab = params['tab'];
					}
					setTimeout(() => {
						if (this.tabSet !== undefined) {
							this.tabSet.select(this.selectedTab);
						}
					},1);
				});
			},
			error => this.session.addError('Server error on loading Financial Institution', error)
		);

		this.financialInstitutionService.getFinancialInstitutionActivities(financialInstitutionId).subscribe(
		    activities => this.activities = activities,
            error => this.session.addError('Failed to load activities', error));
	}

	onTabChange(event) {
		this.urlParameterService.setUrlParameter('tab', event.nextId);
		this.selectedTab = event.nextId;
	}
    showAddButtons(): boolean {
        return !this.session.getUser().hasNoPermissions();
    }
	createGroupsContacts(list: any[]): FormGroup[] {
        let groups: FormGroup[] = [];
        list.forEach(e => {
            let g = this.fb.group(e);
            if ((e as ContactFinancialInstitution) !== undefined) {
                //this.registerEnrolmentGroupListener(g);
            }
            groups.push(g);
        });
        return groups;
	}

	createGroupsSigningOfficers(list: any[]): FormGroup[] {
        let groups: FormGroup[] = [];
        list.forEach(e => {
            let g = this.fb.group(e);
            groups.push(g);
        });
        return groups;
	}

	createGroupsMaaPoas(list: FinancialInstitutionMaaPoa[]): FormGroup[] {
        let groups: FormGroup[] = [];
        list.forEach(e => {
			let g = this.fb.group(e);
			g.setControl('maaPoaInstitutions', this.fb.array((e.maaPoaInstitutions!=null && e.maaPoaInstitutions.length > 0) ? this.createGroupsMaaPoaInstitutions(e.maaPoaInstitutions) : []));
			g.setControl('signingOfficers', this.fb.array((e.signingOfficers!=null && e.signingOfficers.length > 0) ? this.createGroupsSigningOfficers(e.signingOfficers) : []));
			groups.push(g);
        });
        return groups;
	}

	createGroupsMaaPoaInstitutions(list: any[]): FormGroup[] {
        let groups: FormGroup[] = [];
        list.forEach(e => {
            let g = this.fb.group(e);
            groups.push(g);
        });
        return groups;
	}

	validateAndSave() {
		let financialInstitution = this.preserveAlerts();
        this.financialInstitutionService.validateFinancialInstitution(financialInstitution).pipe(catchError(error => of(error))).subscribe(result => {
            if (result != null) {
				if(result.status == 422){
                    let arr = {};
                    let count = 0;
                    for (let err in result.error) {
                        if (arr[result.error[err].errorFieldName] === undefined) { arr[result.error[err].errorFieldName] = []; }
                        arr[result.error[err].errorFieldName].push(result.error[err].errorDescription);
                    }
                    for (let field in arr) {
                        if (field != null && this.form.get(field) != null) {
                            this.form.get(field).setErrors({ 'serverErrors': arr[field] });
                            count++;
                        }
                    }
                }else{
                    this.session.addError("Server error on validation", result);
                }
			    this.saving = false;
			} else {
				this.save();
			}
		});
	}
	get getMaaPoaString():string{
		if(this.form.get('financialInstitutionTypeId') != null && this.form.get('financialInstitutionTypeId') != undefined){
			return this.form.get('financialInstitutionTypeId').value == 8 ? 'MAA' : 'POA';
		}
		return '';
	}
	save() {
		this.saving = true;
		let financialInstitution = this.preserveAlerts();
		this.financialInstitutionService.saveFinancialInstitution(financialInstitution).subscribe(
			financialInstitution => {
				this.form = null;
				this.saving = false;
				this.loadExistingFinancialInstitution(financialInstitution.id);
				
			},
			error => {
				this.saving = false;
				this.session.addError('Server error on saving Financial Institution', error);
			}
		);
	}

	//	FI alerts table is implemented in separate component, so this.form.getRawValue() will set alerts array to null
	preserveAlerts() {
		let tempAlerts = this.financialInstitution.alerts;		
		let financialInstitution = this.form.getRawValue() as FinancialInstitution;
		financialInstitution.alerts = tempAlerts;
		return financialInstitution;
	}

	get getSaveButtonText():string {
        return (this.saving) ? 'Saving' : (this.form.get('id').value != null) ? 'Save Financial Institution' : 'Add Financial Institution';
	}

	get canEditFinancialInstitutions():boolean {
		return (this.session.getUser().hasPermission('CREATE_EDIT_FINANCIAL_INSTITUTIONS') );
	}

	hasPermissionForDeleteEditMaaPoa():boolean {
		return (this.session.getUser().hasPermission('DELETE_EDIT_FI_MAAPOA') );
	}

	canDeactivate(): Observable<boolean> | boolean {
		return !this.hasUnsavedChanges();
	}

	hasUnsavedChanges() {
        return HashUtil.getHash(this.form.value) != this.hash;
	}
	
	addNewFinancialInstitution() {
		let canDeactivate = this.canDeactivateGuard.canDeactivate(this);
		if(typeof canDeactivate == 'boolean') {
			if(canDeactivate == true) {
				this.router.navigate(['financialInstitution']);
			}
		} else if(canDeactivate instanceof Observable) {
			canDeactivate.subscribe(
				result => {
					if(result) {
						this.router.navigate(['financialInstitution']);
					}
				}
			);
		}
	}

	deleteContact(i:number) {
		let faContacts = this.form.controls['contacts'] as FormArray;
		let contacts = faContacts.value as Array<ContactFinancialInstitution>;
		let contact = contacts[i];
		if(contact.id != null) {
			contact.deleted = true;
		} else {
			contacts.splice(i, 1);
		}		
		this.form.setControl('contacts', this.fb.array((contacts.length > 0) ? this.createGroupsContacts(contacts) : []));
	}

	showContactSearchModal() {
        let modalRef = this.modalService.open(ContactSearchComponent, {size: 'lg'});
        modalRef.componentInstance.selected.subscribe(s => {
			let selectedContact = s as Contact;
			if(s!=null) {
				let fa = this.form.controls['contacts'] as FormArray;
				let newContact = new ContactFinancialInstitution();
				newContact.financialInstitutionId = this.financialInstitution.id;
				newContact.contact = selectedContact;
				let d = new Date();
				d.setHours(0,0,0,0);
				if(d.getTimezoneOffset() != 0){
					d.setMinutes(d.getMinutes() + -1*d.getTimezoneOffset());
				}
				newContact.startDate = d.toISOString();
		
				let control = this.fb.group(newContact);
				fa.push(control);
			}
			
		});
		
		
	}
	
	addSigningOfficer(i:number) {
		let faMaaPoas = this.form.controls['maaPoas'] as FormArray;
		let fgMaaPoa = faMaaPoas.controls[i] as FormGroup;
		let faSigningOfficers = fgMaaPoa.controls['signingOfficers'] as FormArray;

		let signingOfficer = new FinancialInstitutionSigningOfficer();
		let g = this.fb.group(signingOfficer);
		faSigningOfficers.controls.push(g);

		let valueSigningOfficers = faSigningOfficers.value;
		valueSigningOfficers.push(signingOfficer);
	}

	deleteSigningOfficer(maaPoaIndex:number, rowIndex:number) {
		let faMaaPoas = this.form.controls['maaPoas'] as FormArray;
		let fgMaaPoa = faMaaPoas.controls[maaPoaIndex] as FormGroup;
		let faSigningOfficers = fgMaaPoa.controls['signingOfficers'] as FormArray;
		let signingOfficers = faSigningOfficers.getRawValue() as Array<FinancialInstitutionSigningOfficer>;

		if(signingOfficers[rowIndex].id != null) {
			signingOfficers[rowIndex].deleted = true;
		} else {
			signingOfficers.splice(rowIndex, 1);
		}
		fgMaaPoa.setControl('signingOfficers', this.fb.array((signingOfficers.length > 0) ? this.createGroupsSigningOfficers(signingOfficers) : []));
	}

	addMaaPoa() {
		let fa = this.form.controls['maaPoas'] as FormArray;
		let maaPoa = new FinancialInstitutionMaaPoa();
		maaPoa.financialInstitutionId = this.financialInstitution.id;
		let d = new Date('2099-12-31');
		d.setHours(0,0,0,0);
		if(d.getTimezoneOffset() != 0){
			d.setMinutes(d.getMinutes() + -1*d.getTimezoneOffset());
		}
		maaPoa.expirationDate = d.toISOString();
		maaPoa.maaPoaInstitutions = [];
		maaPoa.signingOfficers = [];

        let control = this.fb.group(maaPoa);
		control.setControl('maaPoaInstitutions', this.fb.array((maaPoa.maaPoaInstitutions.length > 0) ? this.createGroupsMaaPoaInstitutions(maaPoa.maaPoaInstitutions) : []));
		control.setControl('signingOfficers', this.fb.array((maaPoa.signingOfficers.length > 0) ? this.createGroupsSigningOfficers(maaPoa.signingOfficers) : []));
		fa.push(control);
	}

	deleteMaaPoa(i:number) {
		if(this.form.controls['maaPoas'] != null){
			let faMaaPoas = this.form.controls['maaPoas'] as FormArray;
			let fgMaaPoa = faMaaPoas.controls[i] as FormGroup;
			let maaPao = fgMaaPoa.value as FinancialInstitutionMaaPoa;
			if(maaPao.id != null) {
				maaPao.deleted = true;
				fgMaaPoa.patchValue(maaPao);
			} else {
				faMaaPoas.controls.splice(i, 1);
			}
		}
	}
	addMaaPoaInstitution(i:number) {
		let modalRef = this.modalService.open(FinancialInstitutionSearch, {size: 'lg'});
		modalRef.componentInstance.onlyActiveSuretyCompanies = true;
		modalRef.componentInstance.selected.subscribe(s => {
			if(s.financialInstitutionTypeId == 6) {
				let faMaaPoas = this.form.controls['maaPoas'] as FormArray;
				let fgMaaPoa = faMaaPoas.controls[i] as FormGroup;
				let faMaaPoaInstitutions = fgMaaPoa.controls['maaPoaInstitutions'] as FormArray;

				let maaPoaInstitution = new FinancialInstitutionMaaPoaInstitution();
				maaPoaInstitution.financialInstitution = s;
				let g = this.fb.group(maaPoaInstitution);
				faMaaPoaInstitutions.controls.push(g);

				let valueMaaPoaInstitutions = faMaaPoaInstitutions.value;
				valueMaaPoaInstitutions.push(maaPoaInstitution);

				this.financialInstitutionService.getBranches(maaPoaInstitution.financialInstitution.id).subscribe(
                    branches => {
						this.availableBranchesInFinancialInstitution.set(maaPoaInstitution.financialInstitution.id, branches);
                        if (branches != null && branches.length == 1) {
                            g.controls['branch'].patchValue(branches[0]);
						}
                    },
                    error => this.session.addError('Server error on loading Branches', error)
                );
			}
		});

		
	}

	deleteMaaPoaInstitution(maaPoaIndex:number, rowIndex:number) {
		let faMaaPoas = this.form.controls['maaPoas'] as FormArray;
		let fgMaaPoa = faMaaPoas.controls[maaPoaIndex] as FormGroup;
		let faMaaPoaInstitutions = fgMaaPoa.controls['maaPoaInstitutions'] as FormArray;
		let maaPoaInstitutions = faMaaPoaInstitutions.getRawValue() as Array<FinancialInstitutionMaaPoaInstitution>;

		if(maaPoaInstitutions[rowIndex].id != null) {
			maaPoaInstitutions[rowIndex].deleted = true;
		} else {
			maaPoaInstitutions.splice(rowIndex, 1);
		}
		fgMaaPoa.setControl('maaPoaInstitutions', this.fb.array((maaPoaInstitutions.length > 0) ? this.createGroupsMaaPoaInstitutions(maaPoaInstitutions) : []));
	}

	getFinancialInstitutionTypeName(id:number) {
		let financialInstitutionType = this.financialInstitutionTypes.find(f => f.id == id);
		if(financialInstitutionType && financialInstitutionType.name) {
			return financialInstitutionType.name;
		} else {
			return null;
		}
	}
	addContactLink(){
		return this.session.getProperty("crmCreatePersonUrl");
	}
	showAvailableAmountFi(){
		return this.form.get('maxSecurityAmount').value != 0
	}

    hasAlertType(type:string):boolean {
		let containsType = false;
		if(this.financialInstitution.alerts != null) {
			this.financialInstitution.alerts.forEach((alert) => {
				if (alert.alertType != null && alert.alertType.name === type && alert.endDate == null) {
					containsType = true;
					return;
				}
			});
		}
		return containsType;
	}

	get disableAmounts(): boolean {
        let financialInstitution = this.form.getRawValue() as FinancialInstitution;
	    return financialInstitution != null && financialInstitution.financialInstitutionTypeId == 8;
	}
	
	newBranch(){
		return new Branch();
	}
	
	compareBranch(a:Branch, b:Branch){
		return a && b ? a.id === b.id : a === b;
	}

	loadAvailableBranches() {
		let faMaaPoas = this.form.controls['maaPoas'] as FormArray;
		for (let indexMaaPoa = 0; indexMaaPoa < faMaaPoas.length; indexMaaPoa++) {
			let fgMaaPoa = faMaaPoas.controls[indexMaaPoa] as FormGroup;
			let faMaaPoaInstitutions = fgMaaPoa.controls['maaPoaInstitutions'] as FormArray;

			for (let indexMaaPoaInstitution = 0; indexMaaPoaInstitution < faMaaPoaInstitutions.controls.length; indexMaaPoaInstitution++) {
				let fgMaaPoaInstitution = faMaaPoaInstitutions.controls[indexMaaPoaInstitution] as FormGroup;
				let maaPoaInstitution = fgMaaPoaInstitution.getRawValue() as FinancialInstitutionMaaPoaInstitution;
				this.financialInstitutionService.getBranches(maaPoaInstitution.financialInstitution.id).subscribe(
					branches => {
						this.availableBranchesInFinancialInstitution.set(maaPoaInstitution.financialInstitution.id, branches);
					},
					error => this.session.addError('Server error on loading Branches', error)
				);
			}			
		}

	}

	listLengthForTab(list:any) {
		if(list !== undefined && list != null && isArray(list) && list.length > 0) {
			return '('+list.length+')';
		} else {
			return '';
		}
		
	}

	getContactAddresText(contact:Contact) {
		let addressText = '';
		if(contact.address !=null && contact.address.addressConcat != null && contact.address.addressConcat != '') {
			addressText = 'Address: ' + contact.address.addressConcat;
		} else if(contact.mailingAddress !=null && contact.mailingAddress.addressConcat != null && contact.mailingAddress.addressConcat != '') {
			addressText = 'Address: ' + contact.mailingAddress.addressConcat;
		}
		if(contact.emailAddress != null && contact.emailAddress != '') {
			addressText += '<br />Email: ' +  contact.emailAddress;
		}
		if(contact.phoneNumber != null && contact.phoneNumber != '') {
			addressText += '<br />Phone Number: ' +  contact.phoneNumber;
		}

		return addressText;
	}

	maaPoaTitleClass(date: Date){
		return !(this.compareDate(date, new Date()) != -1) ? "expired" : "";
	}

	compareDate(date1: Date, date2: Date): number {
		let d1 = new Date(date1); let d2 = new Date(date2);
		if(d1.getTime() < d2.getTime()){
			return -1;
		}else if(d1.getTime() > d2.getTime()){
			return 1;
		}else{
			return 0;
		}
	}

    toggleVisibility(i: number) {
	    this.visible[i] = !this.visible[i];
	    return false;
    }
}
