import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../_services/user.service';
import { Session } from '../_session/session';
import {SecurityService} from "../_services/security.service";
import { EnrolmentService } from '../_services/enrolment.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']  
})
export class LoginComponent implements OnInit {
	model: any = {};
	authenticationError: boolean;
	authenticationErrorReason: string;
	returnUrl: string;
	loggingIn: boolean = false;

  	constructor(			
			private router: Router,
			private route: ActivatedRoute,
			private userService: UserService,
			private session: Session,
			private securityService: SecurityService,
			private enrolmentService: EnrolmentService
		) { 
		sessionStorage.clear();
		localStorage.clear();
		this.route.queryParams.subscribe((params: Params) => {
			if (params['logout'] !== undefined) {
				this.userService.logout().subscribe(() => {});
			  	this.router.navigate(['.'], { relativeTo: this.route, queryParams: { logout : null}});
			}
		});
	    this.returnUrl = "" + this.route.snapshot.queryParams['returnUrl'] || '/';
	}

	ngOnInit() {
	}

	login(f) {
		if(f.form.valid){
			this.loggingIn = true;
			this.userService.authenticate(this.model.username, this.model.password).subscribe(
				data =>  {
					this.userService.uiSetup().subscribe(uiMap => {
                        this.session.setUser(data);
						this.session.setUiMap(uiMap);
                        this.loggingIn = false;
					    this.router.navigateByUrl('' + this.returnUrl);
					});
					this.session.login();
				},
				error => {
					switch(error.status){
						case 502: this.authenticationErrorReason = 'Unable to connect to Directory.'; break;
						case 500: this.authenticationErrorReason = 'Internal Server Error.'; break;
						case 400: this.authenticationErrorReason = 'Invalid Credentials'; break;
						default: this.authenticationErrorReason = 'Unknown Error: Unable to login.';
					}
					
					this.authenticationError = true;
					this.loggingIn = false;
				}  
			);
		}
	}
}
