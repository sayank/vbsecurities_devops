import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoReleaseResultComponent } from './auto-release-result.component';

describe('AutoReleaseResultComponent', () => {
  let component: AutoReleaseResultComponent;
  let fixture: ComponentFixture<AutoReleaseResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoReleaseResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoReleaseResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
