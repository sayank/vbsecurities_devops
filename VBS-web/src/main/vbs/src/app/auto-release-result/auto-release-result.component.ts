import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Session} from '../_session/session';
import {ActivatedRoute, Params} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UrlParameterService} from '../url-parameter.service';
import {ReleaseService} from '../_services/release.service';
import {AutoReleaseResultWrapper} from '../_models/autoReleaseResultWrapper';
import {DatatableComponent} from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-auto-release-result',
  templateUrl: './auto-release-result.component.html',
  styleUrls: ['./auto-release-result.component.css']
})

export class AutoReleaseResultComponent implements OnInit {
	form: FormGroup;
	autoReleaseResults: AutoReleaseResultWrapper = null;
	saving:boolean = false;
	runId:number;
	downloadingExcelPassedResults:boolean = false;
	downloadingExcelFailedResults:boolean = false;
	limit: number = 10;
	page: number = 0;
	passedLimit: number = 10;

	@ViewChild(DatatableComponent) table: DatatableComponent;
  	constructor(private fb: FormBuilder, private releaseService: ReleaseService, 
		protected session: Session, private route: ActivatedRoute, private modalService: NgbModal,
		private urlParameterService: UrlParameterService
	) { }

  	ngOnInit() {
		this.saving = false;
		this.route.queryParams.subscribe((params: Params) => {
			if(params['runId'] !== undefined){
				this.runId = params['runId'];
				this.loadAutoRelease(this.runId);
			}
		});
	}

	loadAutoRelease(runId:number) {
		this.releaseService.getAutoReleaseResults(runId).subscribe(
			autoReleaseResults => {
				this.autoReleaseResults = autoReleaseResults;
				this.autoReleaseResults.autoReleasePassedResults.sort((a, b) =>{
					if (a.vNumber < b.vNumber){
					  return 1;
					}
					if (a.vNumber > b.vNumber){
					  return -1;
					}
					return 0;
				});
				this.form = this.fb.group(autoReleaseResults);
			},
			error => this.session.addError('Server error on loading auto release results', error)
		);
	}

	save() {
		this.saving = true;

		this.releaseService.saveAutoReleaseEnrolments(this.runId, this.autoReleaseResults.autoReleasePassedResults).subscribe(
			() => this.saving = false,
			error => this.session.addError('Server error on saving auto release results', error)
		);
	}

	initiateFinalStep() {
		this.saving = true;

		this.releaseService.finalizeAutoRelease(this.runId, this.autoReleaseResults.autoReleasePassedResults).subscribe(
			autoReleaseFinalStepResult => {
				this.saving = false;
                this.autoReleaseResults.autoReleaseRun.runStatusId = 4;
                this.session.addMessage('Final step has been initiated');
			},
			error => {
                this.session.addError('Server error on initiating final step', error);
                this.saving = false;
            }
		);		
	}

	delete(autoReleaseId:number) {
		let autoReleaseForDelete = this.autoReleaseResults.autoReleasePassedResults.find(a => a.id == autoReleaseId);
		if(autoReleaseForDelete) {
            if(autoReleaseForDelete.deletedBy == null) {
                autoReleaseForDelete.deletedBy = this.session.getUser().userId;
            } else {
                autoReleaseForDelete.deletedBy = null;
            }			
		}
	}

	exportToExcelPassedResults() {
		this.downloadingExcelPassedResults = true;
		this.releaseService.getAutoReleasePassedResultsExcel(this.runId).subscribe(data => {
			const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
			let d = new Date();
			const element = document.createElement('a');
			element.href = URL.createObjectURL(blob);
			element.download = "auto-release-passed-results-"+d.getTime()+".xlsx";
			document.body.appendChild(element);
			element.click();
			element.remove();
		},
		error => {},
		() => {this.downloadingExcelPassedResults = false;});
	}

	exportToExcelFailedResults() {
		this.downloadingExcelFailedResults = true;
		this.releaseService.getAutoReleaseFailedResultsExcel(this.runId).subscribe(data => {
			const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
			let d = new Date();
			const element = document.createElement('a');
			element.href = URL.createObjectURL(blob);
			element.download = "auto-release-failed-results-"+d.getTime()+".xlsx";
			document.body.appendChild(element);
			element.click();
			element.remove();
		},
		error => {},
		() => {this.downloadingExcelFailedResults = false;});
	}

	get disableSave():boolean {
		return this.saving || this.autoReleaseResults.autoReleaseRun.runStatusId != 3;
	}

    compareDate(date1: Date, date2: Date): number {
        let d1 = new Date(date1); let d2 = new Date(date2);
        if(d1.getTime() < d2.getTime()){
            return -1;
        }else if(d1.getTime() > d2.getTime()){
            return 1;
        }else{
            return 0;
        }
	}
	changeFailedLimit(e){
		this.limit = parseInt(e.target.value, 10);
		this.table.recalculate();
	}

	changePassedLimit(e){
		this.passedLimit = parseInt(e.target.value, 10);
		this.table.recalculate();
	}
	viewAllFailed(){
		this.limit = 100000000;
		this.table.limit = this.limit;
		this.table.recalculate();
	}
	viewAllPassed(){
		this.passedLimit = 100000000;
		this.table.limit = this.passedLimit;
		this.table.recalculate();
	}
	
	onPage(pageInfo){
		this.page = pageInfo.offset;
		this.table.recalculate();
	}
}
