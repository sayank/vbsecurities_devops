import {ChangeDetectorRef, Component, ElementRef, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DatePipe} from '@angular/common';
import {FormArray, FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {FinancialInstitutionSearch} from '../_components/financialInstitutionSearch/financial-institution-search.component';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {Branch} from '../_models/branch';
import {FinancialInstitution} from '../_models/financialInstitution';
import {FinancialInstitutionService} from '../_services/financial-institution.service';
import {Session} from '../_session/session';
import {FinancialInstitutionMaaPoa} from '../_models/financialInstitutionMaaPoa';
import {catchError, debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {SecurityStatus} from '../_models/securityStatus';
import {SecurityService} from '../_services/security.service';
import {SecurityDeposit} from '../_models/securityDeposit';
import {NameDescription} from '../_models/nameDescription';
import {EscrowAgentSearchComponent} from '../_components/escrow-agent-search/escrow-agent-search.component';
import {Contact} from '../_models/contact';
import {ContactsService} from '../_services/contacts.service';
import {ContactSearchComponent} from '../_components/contactSearch/contact-search.component';
import {HistoryField} from '../_models/historyField';
import {DeleteReasonComponent} from "../_components/delete-reason/delete-reason.component";
import {SearchVbComponent} from "../_components/search-vb/search-vb.component";
import {Security} from '../_models/security';
import {DatatableComponent} from "@swimlane/ngx-datatable";


@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.css'],
  providers: [DatePipe]
})

export class SecurityComponent implements OnInit {
	@Input("form") form:FormGroup;
    @Input("activeTab") activeTab:string;

	branches: Branch[] = [];
	maaPoas: FinancialInstitutionMaaPoa[] = [];
    commentHistory: HistoryField[] = null;

    securityModalRef: NgbModalRef;
	allSecurityStatuses: SecurityStatus[] = [];
	allSecurityDeposit: SecurityDeposit[] = [];
	identityTypes: NameDescription[] = [];
	lawyers: Contact[] = [];
	assistants: Contact[] = [];
	thirdPartContactAddress:String = '';
	yesNoOptions = [{value: null, description: ''}, {value: 'YES', description: 'Yes'}, {value: 'NO', description: 'No'}];
	crmCompanyUrl: string = '';
    security: Security;
    monthList: any;
    yearList: number[];
    yellowStickyUrl: string;
    flagUrl: string;

	@ViewChild("commentHistoryModal", {read: TemplateRef}) commentHistoryModal: TemplateRef<any>;
    @ViewChild('reasonsModal') public reasonModal: NgbModal;
    @ViewChild('monthlyMonth') monthlyMonth: ElementRef;
    @ViewChild('monthlyYear') monthlyYear: ElementRef;
    @ViewChild('jbaTable') jbaTable: DatatableComponent;
    @ViewChild('vbTable') vbTable: DatatableComponent;
    @ViewChild('maaPoaTable') maaPoaTable: DatatableComponent;

	constructor(private ngbModal: NgbModal, private financialInstitutionService: FinancialInstitutionService,
                private session: Session, private securityService: SecurityService,
                private contactsService: ContactsService, private fb: FormBuilder, private cdRef:ChangeDetectorRef) {
        this.allSecurityStatuses = this.session.getProperty("securityStatuses");
        this.identityTypes = this.session.getProperty("identityTypes");
        this.allSecurityDeposit = this.session.getProperty("securityDeposits");
        this.crmCompanyUrl = this.session.getProperty("crmCompanyUrl");
        this.flagUrl = this.session.getProperty("flagUrl");
        this.yellowStickyUrl = this.session.getProperty("yellowStickyUrl");
        this.monthList = this.getMonthList();
        this.yearList = this.getYearList();
	}

    ngOnChanges(){
        (<FormGroup>this.form.controls['financialInstitutionAndBranch']).controls['financialInstitution'].valueChanges
            .pipe(debounceTime(300), distinctUntilChanged()).subscribe(v => {
            (<FormGroup>this.form.controls['financialInstitutionAndBranch']).controls['branch'].patchValue(this.newBranch());
            this.loadBranches(v);
            this.loadMaaPoas(v);
        });

        (<FormGroup>this.form.controls['escrowAgentLawyerAssistant']).controls['escrowAgent'].valueChanges.pipe(debounceTime(300), distinctUntilChanged()).subscribe(v => {
            this.loadLawyers(v);
            this.loadAssistants(v);
        });

        this.form.get('securityStatusId').valueChanges.subscribe(id => {
            if (this.securityHasId && id == 3) {
                this.securityModalRef = this.ngbModal.open(this.reasonModal);
            }
        });

        this.form.updateValueAndValidity();
        this.cdRef.detectChanges();

        if(this.activeTab == 'tab-security' && this.jbaTable != undefined) {
            setTimeout(function (d) {
                d.recalculate();
                d['cd'].markForCheck();
            }, 10, this.jbaTable);
        }
        if(this.activeTab == 'tab-security' && this.vbTable != undefined) {
            setTimeout(function (d) {
                d.recalculate();
                d['cd'].markForCheck();
            }, 10, this.vbTable);
        }
        if(this.activeTab == 'tab-security' && this.maaPoaTable != undefined) {
            setTimeout(function (d) {
                d.recalculate();
                d['cd'].markForCheck();
            }, 10, this.maaPoaTable);
        }
    }


    ngOnInit() {
        if(this.form.get('escrowAgentLawyerAssistant').value != null && this.form.get('escrowAgentLawyerAssistant').value.escrowAgent != null) {
			this.loadLawyers(this.form.get('escrowAgentLawyerAssistant').value.escrowAgent);
			this.loadAssistants(this.form.get('escrowAgentLawyerAssistant').value.escrowAgent);
		}
        this.loadBranches((<FormGroup>this.form.controls['financialInstitutionAndBranch']).controls['financialInstitution'].value);
        this.loadMaaPoas((<FormGroup>this.form.controls['financialInstitutionAndBranch']).controls['financialInstitution'].value);
	}

    addVb(){
        let modal = this.ngbModal.open(SearchVbComponent, {size: 'lg', centered: true});
        let searchVbComponent:SearchVbComponent = <SearchVbComponent> modal.componentInstance;
        searchVbComponent.selected.subscribe(vb => {
            if(this.form.get('vbList').value.length == 0 || !this.form.get('vbList').value.find(v => v.crmContactId == vb.crmContactId)){
                let grp = this.fb.group(vb);
                grp.patchValue({'primaryVb': this.form.getRawValue().vbList.find(v => v.deleteReason == null && v.primaryVb) == null});
                if(this.securityHasId && this.isSecurityType(1)){grp.get('primaryVb').disable();}
                (this.form.get('vbList') as FormArray).push(grp);
            }else{
                for(let i=0;i<this.form.get('vbList').value.length;i++){
                    if(this.form.get('vbList.'+i+'.crmContactId').value == vb.crmContactId){
                        if(this.form.get('vbList.'+i+'.deleteReason').value != null){
                            this.form.get('vbList.'+i+'.deleteReason').patchValue(null);
                            this.form.get('vbList.'+i+'.primaryVb').patchValue(this.filteredVbList.length == 1);
                        }else{
                            this.session.addError("Cannot add duplicate VB# "+vb.crmContactId, null);
                        }
                        break;
                    }
                }
            }
            if(this.form.get('vbList.0.crmContactId').value == null){
                (this.form.get('vbList') as FormArray).removeAt(0);
            }
        });
    }

    deleteVb(crmContactId:string){
        let modal = this.ngbModal.open(DeleteReasonComponent, {size: 'lg', centered: true, backdrop: "static"});
        let deleteReasonComponent:DeleteReasonComponent = <DeleteReasonComponent> modal.componentInstance;
        deleteReasonComponent.securityPurposeId = this.form.get('securityPurposeId').value;
        deleteReasonComponent.selected.subscribe(reasonId => {
            this.getFormGroupForVbNumber(crmContactId).get('deleteReason').patchValue(reasonId);
            this.getFormGroupForVbNumber(crmContactId).get('primaryVb').patchValue(false);
        });
    }

	typeaheadEscrow = (text$: Observable<string>) =>
	text$.pipe(
		debounceTime(300),
		distinctUntilChanged(),
		switchMap(term => ((term.length > 0 && !isNaN(Number(term))) || term.length > 2) ? this.contactsService.escrowAgentTypeahead(term) : []),
		catchError(() => {
			return of([]);
		})
	);

    companyContactFormatter = (x: { crmContactId: number, companyName: string }) => {
        if (x.crmContactId !== undefined) {
            return (x.crmContactId != null) ? x.crmContactId + ((x.companyName != null) ? ' - ' + x.companyName : '') : '';
        }
        return x;
    };

    searchAndReplaceCrmUrls(key: string, value: string, replace: string): string {
        if (!value) return null;

        return value.replace(key, replace);
    }
	typeaheadFinancial = (text$: Observable<string>) =>
	text$.pipe(
		debounceTime(300),
		distinctUntilChanged(),
		switchMap(term => ((term.length > 0 && !isNaN(Number(term))) || term.length > 2) ? this.financialInstitutionService.financialInstitutionTypeahead(term) : []),
		catchError(() => {
			return of([]);
		})
	);

	financialInstitutionFormatter = (x: {id:number, name: string}) => {
		if(x.id !== undefined){
		  return (x.id != null) ? x.id + ((x.name != null) ? ' - ' + x.name : '') : '';
		}
		return x;
	};

	showSearchModal(){
		let modalRef = this.ngbModal.open(FinancialInstitutionSearch, {size: 'lg'});
		modalRef.componentInstance.selected.subscribe(s => {
            this.form.get('financialInstitutionAndBranch.financialInstitution').patchValue(s);
			this.form.get('financialInstitutionAndBranch.branch').patchValue(this.newBranch());
            this.loadBranches(s);
            this.form.controls['financialInstitutionMaaPoaDTO'].patchValue(null);
            this.loadMaaPoas(s);
		});
	}

    loadThirdPartyContact() {
        let c = <Contact>this.form.controls['thirdPartyContact'].value;
        this.thirdPartContactAddress = "";
		if (c != null && c.id == null) {
            if (!isNaN(Number.parseInt(c.crmContactId.replace("B", "")))) {
                this.contactsService.getContactByCrmContactId(c.crmContactId).subscribe(contact => {
                    this.form.controls['thirdPartyContact'].patchValue(contact);
					this.getAddress(contact);
				});
			}
        }
	}

	openCommentHistory(event) {
		event.preventDefault();
		this.securityService.getCommentHistory(this.form.get('id').value).subscribe(h => this.commentHistory = h);
		this.ngbModal.open(this.commentHistoryModal, {size: 'lg'});
	}

	private loadAssistants(escrowAgent) {
        if (escrowAgent !== undefined && escrowAgent != null) {
            let id = 0;
            if (escrowAgent.crmContactId !== undefined && escrowAgent.crmContactId != null && !isNaN(escrowAgent.crmContactId)) {
                id = escrowAgent.crmContactId
            } else if (escrowAgent.length > 0 && !isNaN(escrowAgent)) {
                id = escrowAgent;
            }

            if (id > 0) {
                this.assistants = [];
                this.contactsService.getAssistantsByEscrowAgentCrmContactId(id.toString()).subscribe(
                    assistants => {
                        this.assistants = assistants;
                    },
                    error => this.session.addError('Server error on loading Assistants', error));
            }
        }
    }

	public loadBranches(financial) {
        if (financial !== undefined && financial != null) {
            let id = 0;
            if (financial.id != undefined && !isNaN(financial.id)) {
                id = financial.id;
            } else if (financial.length > 0 && !isNaN(financial)) {
				id = financial;
                let fi = new FinancialInstitution();
				fi.id = financial;
                this.form.get('financialInstitutionAndBranch.financialInstitution').patchValue(fi);
            }
            if (id > 0) {
                this.branches = [];
                this.financialInstitutionService.getBranches(id).subscribe(
                    branches => {
                        this.branches = branches;
                        if (this.branches != null && this.branches.length == 1 && this.form.get('financialInstitutionAndBranch.branch').value == null) {
                            this.form.get('financialInstitutionAndBranch.branch').patchValue(this.branches[0]);
						}
                    },
                    error => this.session.addError('Server error on loading Branches', error)
                );
            }
        }
	}

	private loadMaaPoas(financial: FinancialInstitution) {
        if (financial !== undefined && financial != null) {
            this.maaPoas = [];
            if (financial.financialInstitutionTypeId == 8 || financial.financialInstitutionTypeId == 9) {
                this.financialInstitutionService.getMaaPoas(financial.id).subscribe(
                    maaPoas => {
                        if(maaPoas!=null) {
                            this.maaPoas = maaPoas.filter(m => this.compareToday(new Date(m.expirationDate)));
                            if (this.maaPoas != null && this.maaPoas.length == 1) {
                                this.form.controls['financialInstitutionMaaPoaDTO'].patchValue(this.maaPoas[0]);
                                this.onMaaPoaChanged();
                            }
                        }
                    },
                    error => this.session.addError('Server error on loading MAA/POAs', error)
                );
			}
			else{
				this.form.controls['financialInstitutionMaaPoaDTO'].patchValue(null);
			}
        }
	}

	showEscrowSearchModal(){
        let modalRef = this.ngbModal.open(EscrowAgentSearchComponent, {size: 'lg'});
        modalRef.componentInstance.securityType = this.form.get('securityTypeId').value;
		modalRef.componentInstance.selected.subscribe(s => {
			this.form.get('escrowAgentLawyerAssistant.escrowAgent').patchValue(s);
		});
	}

	showContactSearchModal() {
        let modalRef = this.ngbModal.open(ContactSearchComponent, { size: 'lg' });
        if (this.form.get('thirdPartyContact').value != null && this.form.get('thirdPartyContact').value.firstName == null && this.form.get('thirdPartyContact').value.companyName == null && this.form.get('thirdPartyContact').value.crmContactId != null) {
            modalRef.componentInstance.searchTerm = this.form.get('thirdPartyContact').value.crmContactId;
        }
        modalRef.componentInstance.selected.subscribe(s => {
		this.form.controls['thirdPartyContact'].patchValue(s);
			this.getAddress(s);
        });
	}

	getAddress(contact:Contact){
		if(contact.address.addressConcat == "" && contact.mailingAddress.addressConcat == ""){
			this.thirdPartContactAddress = 'NA';
		}else if(contact.address.addressConcat != ""){
			this.thirdPartContactAddress =	contact.address.addressConcat;
		}else if(contact.mailingAddress.addressConcat != ""){
			this.thirdPartContactAddress = contact.mailingAddress.addressConcat;
		}
		this.thirdPartContactAddress = "Address: " +  this.thirdPartContactAddress;
    }

	compareToday(date: Date){
		return (this.compareDate(date, new Date()) != -1);
	}

	private compareDate(date1: Date, date2: Date): number {
		let d1 = new Date(date1); let d2 = new Date(date2);
		if(d1.getTime() < d2.getTime()){
			return -1;
		}else if(d1.getTime() > d2.getTime()){
			return 1;
		}else{
			return 0;
		}
	}

	compareBranch(a:Branch, b:Branch){
		return a && b ? a.id === b.id : a === b;
	}

	compareContact(a:Contact, b:Contact){
		return a && b ? a.id === b.id : a === b;
	}

	private newBranch(){
		return new Branch();
	}

	compareMaaPoa(a:FinancialInstitutionMaaPoa, b:FinancialInstitutionMaaPoa){
		return a && b ? a.id === b.id : a === b;
    }

	private loadLawyers(escrowAgent) {
        if (escrowAgent !== undefined && escrowAgent != null) {
            let id = 0;
            if (escrowAgent.crmContactId !== undefined && escrowAgent.crmContactId != null && !isNaN(escrowAgent.crmContactId)) {
                id = escrowAgent.crmContactId
            } else if (escrowAgent.length > 0 && !isNaN(escrowAgent)) {
                id = escrowAgent;
                let ea = new Contact();
				ea.crmContactId = escrowAgent;
				this.form.get('escrowAgentLawyerAssistant.escrowAgent').patchValue(ea);
            }
            if (id > 0) {
                this.lawyers = [];
                this.contactsService.getLawyersByEscrowAgentCrmContactId(id.toString()).subscribe(
                    lawyers => {
                        this.lawyers = lawyers;
                    },
                    error => this.session.addError('Server error on loading Lawyers', error));
            }
        }
    }

    onMaaPoaChanged():void {
        this.securityService.recalculateMaaPoaAmounts(this.form.getRawValue()).subscribe(
            maaPoa => {
                this.form.controls['financialInstitutionMaaPoaDTO'].patchValue(maaPoa);
            },
            error => this.session.addError('Server error on recalculating MAA/POAs amounts', error)
        );
    }

	get financialInstitutionLabel():string {
		if(this.form.get('financialInstitutionAndBranch.financialInstitution').value.financialInstitutionTypeId == 8){
			return 'MAA';
		}else if(this.form.get('financialInstitutionAndBranch.financialInstitution').value.financialInstitutionTypeId == 9){
			return 'POA';
		}
	}

	get getPrimaryVb():Contact {
		return this.form.get('vbList').value.find(v => (v.deleteReason == null && v.primaryVb));
	}

	hasAlert(row:Contact, id:string):boolean {
	    if(row.alert === undefined || row.alert == null){return false;}
	    return row.alert == id;
    }

    get isFullyReleased():boolean {
	    return !(this.form.get('fullyReleased').value == null || this.form.get('fullyReleased').value == false);
    }
	get getShowMandatory():boolean {
		return !(this.isSecurityType(3) && (this.form.get('securityStatusId').value == 1 || this.form.get('securityStatusId').value == 2));
	}

    isSecurityType(type:number):boolean {
	    return this.form.get('securityTypeId').value == type;
    }

    get securityHasId(): boolean {
	    return this.form.get('id').value != null;
    }

    get enrolmentList(): string {
	    if(this.form.get('enrolments.0') == null || this.form.get('enrolments.0.enrolmentNumber').value == null){return '';}
	    if(this.form.get('enrolments.1') === undefined){return this.form.get('enrolments.0.enrolmentNumber').value;}

	    let ret:string = '';
	    for(let i=0;i<this.form.get('enrolments').value.length;i++){
            ret += this.form.get('enrolments.'+i+'.enrolmentNumber').value;
            const status = this.form.get('enrolments.'+i+'.enrolmentStatus').value;
            if (status === 'CANCELLED') {
                ret += ' (CANCELLED)';
            }
            if(i+1 < this.form.get('enrolments').value.length){ret += ', ';}
        }
	    return ret;
    }

	get securitiesByVB() {
		let contact:Contact = this.getPrimaryVb;
		if (contact != null) {
			return this.session.getProperty("allSecuritiesUnderVB", contact.crmContactId);
		}
    }

    get filteredVbList():Contact[] {
	    let vbs:Contact[] = [];
	    if(this.form.get('vbList').value.length){
	        vbs = this.form.get('vbList').value.filter(v => v.deleteReason == null && v.crmContactId != null);
        }
        if(vbs.length == 0){vbs.push(new Contact());}
        return vbs;
    }

    get vbListIsEmpty():boolean{
	    return this.form.get('vbList.0') == null || this.form.get('vbList.0.crmContactId').value == null;
    }

    getFormGroupForVbNumber(crmContactId):FormGroup{
	    for(let i=0;i<this.form.get('vbList').value.length;i++){
            if(this.form.get('vbList.'+i+'.crmContactId').value == crmContactId){
                return this.form.get('vbList.'+i) as FormGroup;
            }
        }
    }

    getPrimaryVbFormControlForVbNumber(crmContactId):FormControl {
	    return this.getFormGroupForVbNumber(crmContactId).get('primaryVb') as FormControl;
    }

    getMaaPoaPercentageSum(): number {
        let maaPoa = this.form.controls['financialInstitutionMaaPoaDTO'].value as FinancialInstitutionMaaPoa;
        let sum = 0;
        if(maaPoa && maaPoa.maaPoaInstitutions && maaPoa.maaPoaInstitutions.length > 0) {
            maaPoa.maaPoaInstitutions.forEach(m => {
                sum += m.percentage;
            })
        }
        return sum;
    }

    getMaaPoaOriginalAmountSum(): number {
        let maaPoa = this.form.controls['financialInstitutionMaaPoaDTO'].value as FinancialInstitutionMaaPoa;
        let sum = 0;
        if(maaPoa && maaPoa.maaPoaInstitutions && maaPoa.maaPoaInstitutions.length > 0) {
            maaPoa.maaPoaInstitutions.forEach(m => {
                sum += m.originalAmountOnInstitution;
            })
        }
        return sum;
    }

    getMaaPoaCurrentAmountSum(): number {
        let maaPoa = this.form.controls['financialInstitutionMaaPoaDTO'].value as FinancialInstitutionMaaPoa;
        let sum = 0;
        if(maaPoa && maaPoa.maaPoaInstitutions && maaPoa.maaPoaInstitutions.length > 0) {
            maaPoa.maaPoaInstitutions.forEach(m => {
                sum += m.currentAmountOnInstitution;
            })
        }
        return sum;
    }

    get jointBondSecurities():Security[] {
	    let jointBondSecurities:Security[] = [];
	    if(this.form.get('jointBondSecurities').value.length) {
	        jointBondSecurities = this.form.get('jointBondSecurities').value;
        }
        return jointBondSecurities;
    }

    get defaultMonthlyYear():number{
	    let d = new Date();
	    return (d.getMonth() == 0) ? d.getFullYear()-1 : d.getFullYear();
    }

    get defaultMonthlyMonth():number{
	    let d = new Date();
	    return (d.getMonth() == 0) ? 11 : d.getMonth()-1;
    }

    getYearList():number[] {
	    let ret = [];
	    let d = new Date();
	    for(let i=d.getFullYear();i>=1980;i--){
	        ret.push(i);
        }
	    return ret;
    }

    getMonthList():any{
	    let ret = [];
	    ret.push({value: 0, name: "January"});
        ret.push({value: 1, name: "February"});
        ret.push({value: 2, name: "March"});
        ret.push({value: 3, name: "April"});
        ret.push({value: 4, name: "May"});
        ret.push({value: 5, name: "June"});
        ret.push({value: 6, name: "July"});
        ret.push({value: 7, name: "August"});
        ret.push({value: 8, name: "September"});
        ret.push({value: 9, name: "October"});
        ret.push({value: 10, name: "November"});
        ret.push({value: 11, name: "December"});
        return ret;
    }

    monthlyChange(){
	    let o = null;
	    if(this.form.get('monthlyReport').value){
	        let m = this.monthlyMonth.nativeElement.options[this.monthlyMonth.nativeElement.selectedIndex].value;
	        let y = this.monthlyYear.nativeElement.options[this.monthlyYear.nativeElement.selectedIndex].value;
	        o = new Date(y, m, 1, 0, 0, 0, 0);
            if(o.getTimezoneOffset() != 0){
                o.setMinutes(o.getMinutes() + -1*o.getTimezoneOffset());
            }
        }
        this.form.get('monthlyDate').patchValue(o);
    }

    onIdentityTypeChanged():void {
        if(this.form.get('fhUnitsToCover').value == null || this.form.get('ceUnitsToCover').value == null && this.form.get('identityTypeId').value != 3) {
            this.form.get('fhUnitsToCover').patchValue(this.form.get('unitsToCover').value);
            this.form.get('ceUnitsToCover').patchValue(this.form.get('unitsToCover').value);
        }
    }
}
