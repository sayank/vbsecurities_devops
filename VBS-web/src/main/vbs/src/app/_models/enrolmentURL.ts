export class EnrolmentURL {
	enrolmentLink:string = null;
	threeHundredSixtyLink:string = null;
	ceViewerLink:string = null;
	finalApprovalManagerDefault:string = null;
	vpManagerDefault:string = null;
	crmWarrantyServiceOrderLink:string = null;
	createPersonContactLink:string = null;
	allSecuritiesUnderVb:string = null;
	summaryBySecurityInstrumentUrl:string = null;
}
