import { Contact } from "./contact";

export class ContactFinancialInstitution {
    id:number = null;
	version:number = null;
	
	financialInstitutionId:number = null;
	contact:Contact = null;
	startDate:string = null;
	endDate:string = null;
	deleted:boolean = null;

	createDate:string = null;
	createUser:string = null;
	updateDate:string = null;
	updateUser:string = null;
}
