import {Security} from "./security";

export class CEEnrolmentDecision {

    public enrolmentNumber:string = null;
    public enrolmentAddress:string = null;
    public vbNumber:string = null;
    public vbName:string = null;
    public blanketSecurities:Security[] = [];
    public decision:string = null;
    public reason:string = null;
	public unitsToCover:number = null;
}
