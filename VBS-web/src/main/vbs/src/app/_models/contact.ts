import { NameDescription } from "./nameDescription";
import {Address} from "./address";

export class Contact {
  id:number = null;
  version:number = null;
  address:Address = null;
  mailingAddress:Address = null;
  crmContactId:string = null;
  emailAddress:string = null;
  phoneNumber:string = null;
  phoneNumberExt:string = null;
  firstName:string = null;
  lastName:string = null;
  companyName:string = null;
  sin:string = null;
  driversLicense:string = null;
  dob:string = null;
  licenseStatus:NameDescription = new NameDescription(null, null, null);
  escrowAgentLicenseApplicationStatus:NameDescription = new NameDescription(null, null, null);
  createDate:string = null;
  createUser:string = null;
  updateDate:string = null;
  updateUser:string = null;
  unwilling: string = null;
  accessibility: boolean = null;
  yellowSticky: boolean = null;
  primaryVb: boolean = null;
  deleteReason: string = null;
  alert: string = null;
  apVendorFlag: boolean = null;
}
