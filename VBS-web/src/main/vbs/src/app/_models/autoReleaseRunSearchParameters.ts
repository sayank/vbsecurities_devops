export class AutoReleaseRunSearchParameters {

    dateSearchType:string  = 'EQUALS';
    startDate:string = null;
    endDate:string = null;
}
