export class Branch {

    id:number = null;
    version:number = null;
    name:string = null;
    description:string = null;
    financialInstitutionId:number = null;
    addressConcat:string = null;
    
}