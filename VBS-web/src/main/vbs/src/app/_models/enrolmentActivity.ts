export class EnrolmentActivity {
    activityTime:string = null;
    fieldName:string = null;
    oldValue:string = null;
    newValue:string = null;
    userId:string = null;
}
