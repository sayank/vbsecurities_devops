import { NameDescription } from "./nameDescription";
import { ReleaseEnrolment } from "./releaseEnrolment";
import { Contact } from "./contact";
import { ReplacedBy } from "./replacedBy";


export class Release {

	public id:number = null;
  public version:number = null;

  public analystApprovalDateTime:string = null;
  public analystApprovalStatus:string = null;
  public analystApprovedBy:string = null;
  public analystRejectReason:number = null;
  public assignToVp:string = null;
  public authorizedReleaseAmount:number = null;
  public comment:string = null;
  public enrolments: ReleaseEnrolment[] = [];
  public fcmAmountRetained:number = null;
  public finalApprovalDate:string = null;
  public finalApprovalSentTo:string = null;
  public finalApprovalStatus:string = null;
  public finalApprovedBy:string = null;
  public fullRelease:boolean = null;
  public interestAmount:number = null;
  public principalAmount:number = null;
  public interestPeriodStartWarning:boolean = null;
  public luRetainAmount:number = null;
  public managerRejectReason:number = null;
  public payeeAddressConcat:string = null;
  public payTo:Contact = null;
  public providedByVb:Contact = null;
  public reasons:NameDescription[] = null;
  public recommendedRejectedBy:string = null;
  public referenceNumber:string = null;
  public status:string = null;
  public regularRelease:boolean = null;
  public releaseType:NameDescription = null;
  public replacedBy:ReplacedBy = null;
  public requestDate:string = null;
  public requestedAmount:number = null;
  public securityId:number = null;
  public wsInputRequested:boolean = null;
  public wsInputProvidedBy:string = null;
  public wsRecievedDate:string = null;
  public wsRecommendation:NameDescription = null;
  public wsRequestDate:string = null;
  public wsRequestor:string = null;
  public serviceOrderId:string = null; 
  public createDate:string = null;
  public createUser:string = null;
  public updateDate:string = null;
  public updateUser:string = null;
  public unwindProRata:number;
  public settlementInstructions:string;
  public chequeNumber:string;
  public chequeDate:string;
  public chequeStatus:number;
  public replacementChequeNumber:string;
  public replacementChequeDate:string;
  public replacementChequeStatus:string;
  public replacementPayee:string;
  public replacementPayeeCRMContactId:string;
  public voucherId:string;
  public apcsdVoucherUrl:string;
}
