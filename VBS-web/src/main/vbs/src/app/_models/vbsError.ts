import { HttpErrorResponse } from "@angular/common/http";

export class VbsError {
	httpError: HttpErrorResponse;
	message: string;

	constructor(message: string, httpError: HttpErrorResponse) {
		this.message = message;
		this.httpError = httpError;
		
	}
	
}