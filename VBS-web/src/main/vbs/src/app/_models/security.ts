import { FinancialInstitutionAndBranch } from "./financialInstitutionAndBranch";
import { EscrowAgentLawyerAssistant } from "./escrowAgentLawyerAssistant";
import { Contact } from "./contact";
import { Alert } from "./alert";
import { FinancialInstitutionMaaPoa } from "./financialInstitutionMaaPoa";
import {Enrolment} from "./enrolment";

export class Security {

    public id:number = null;
    public version:number = null;
    public poolId:number = null;
    public unitsToCover:number = null;
    public fhUnitsToCover:number = null;
    public ceUnitsToCover:number = null;
    public securityStatusId: number = null;
    public securityTypeId: number = null;
	public securityPurposeId: number = null;
	public identityTypeId: number = null;
    public instrumentNumber:string = null;
    public originalAmount:number = null;
    public currentAmount:number = null;
    public currentInterest:number = null;
    public issuedDate:string = null;
    public receivedDate:string = null;
    public ppsaNumber:string = null;
    public ppsaExpiryDate:boolean = null;
    public jointBond:boolean = null;
    public jointAut:boolean = null;
    public releaseFirst:string = null;
    public demandFirst:string = null;
    public escrowAgentLawyerAssistant:EscrowAgentLawyerAssistant = new EscrowAgentLawyerAssistant();
    public comment:string = null;
    public thirdParty:boolean = null;
    public thirdPartyContact: Contact = new Contact();
    public dtaAccountNumber:string = null;
    public financialInstitutionAndBranch: FinancialInstitutionAndBranch = new FinancialInstitutionAndBranch();
    public financialInstitutionMaaPoaDTO: FinancialInstitutionMaaPoa = new FinancialInstitutionMaaPoa();
    public ppsaFileNumber:string = null;
    public vbList: Contact[] = [];
    public availableAmount:number = null;
    public availableInterest:number = null;
    public createDate:string = null;
    public createUser:string = null;
    public updateDate:string = null;
    public updateUser:string = null;
    public interestPeriodStartWarning:boolean = null;
    public fullyReleased:boolean = null;
    public allocated:boolean = null;
    public unpostReason:string = null;
    public writeOff:boolean = null;
    public writeOffReason:string = null;
    public jointBondOriginalAmount:number = null;
    public jointBondCurrentAmount:number = null;
    public deleted:boolean = null;
    public pefAccountNumber: string = null;
    public enrolments:Enrolment[] = [];
	public alerts: Alert[] = [];
    public jointBondSecurities:Security[] = [];
    public linkedToMultipleCE:boolean = null;
    public monthlyDate:string = null;
	public monthlyReport:boolean = null;
	public linkedWithRelease:boolean = null;
    public securityDeposit: number = null;
	public primaryVb:Contact = null;


	public constructor(){
        let d = new Date();
        d.setHours(0,0,0,0);
        if(d.getTimezoneOffset() != 0){
            d.setMinutes(d.getMinutes() + -1*d.getTimezoneOffset());
        }
        this.issuedDate = d.toISOString();
        this.receivedDate = d.toISOString();
        this.originalAmount = 0;
    }
}
