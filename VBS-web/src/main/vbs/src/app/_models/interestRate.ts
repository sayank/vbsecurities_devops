export class InterestRate {
	public id: number = null;
	public version: number = null;
	public interestStartDate: string = null;
	public annualRate: string = null;
  public canEdit: boolean = true;
	public createUser: string = null;
	public createDate: string = null;
}
