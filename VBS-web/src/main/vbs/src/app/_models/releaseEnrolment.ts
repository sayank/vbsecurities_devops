import { Enrolment } from "./enrolment";

export class ReleaseEnrolment {
	public id:number = null;
	public version:number = null;
	public securityId:number = null;
	public enrolment: Enrolment = null;
	public authorizedAmount:number = null;
	public requestAmount:number = null;
	public analystApprovalStatus:string = null;
	public analystApprovedBy:string = null;
	public analystApprovalDateTime:string = null;
	public analystApprovalSentTo:string = null;
	public analystApprovalRequestor:string = null;
	public analystApprovedDate:string = null;
	public rejectReason:number = null;
	public fullRelease:string = null;
	public updateDate:string = null;
	public updateUser:string = null;
	public createDate:string = null;
	public createUser:string = null;
}