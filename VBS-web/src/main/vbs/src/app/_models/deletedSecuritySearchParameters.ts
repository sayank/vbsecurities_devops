
export class DeletedSecuritySearchParameters {
	public updateDateType: string = 'EQUALS';
	public updateDate:string = null;
	public updateDateBetweenTo:string = null;
}
