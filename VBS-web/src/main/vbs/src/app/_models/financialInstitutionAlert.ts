import {AlertType} from './alertType';

export class FinancialInstitutionAlert {

    public id:number = null;
    public version:number = null;
    public financialInstitutionId:number = null;
    public alertType:AlertType = null;
    public name:string = null;
    public description:string = null;
    public createDate:string = null;
    public createUser:string = null;
    public updateDate:string = null;
    public updateUser:string = null;
    public startDate:string = null;
    public endDate:string = null;
    public endUser:string = null;    
    public status:string = null;

}