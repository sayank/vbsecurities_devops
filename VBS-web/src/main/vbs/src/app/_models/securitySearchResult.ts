import { SearchResultEnrolment }from './searchResultEnrolment';

export class SecuritySearchResult {
	public securityNumber:number = null;
	public instrumentNumber:string = null;
	public receivedDate:number = null;
	public poolId:string = null;
	public jba:boolean = null;
	public securityTypeName:number = null;
	public vbNumber: number = null;
	public vbName: string = null;
	public multiVb:boolean = null;
	public securityPurposeName:string = null;
	public identityTypeName: string = null;
	public originalAmount:number = null;
	public currentAmount:number = null;
	public enrolments:SearchResultEnrolment[] = null;
	public dtaStatus:string = null;
	public dtaAccountNumber:string = null;
	public currentInterestAmount: number = null;
	public totalAvailableAmount: number = null;
	public maaPoaId: number = null;
	public selected: boolean = null;
}
