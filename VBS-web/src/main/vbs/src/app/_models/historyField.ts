export class HistoryField {
	public fieldName:string = null;
	public fieldValue:string = null;
	public updateTime:string = null;	
	public updateUser:string = null;
}