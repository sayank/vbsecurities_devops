import { Contact } from "./contact";
import { NameDescription } from "./nameDescription";

export class PendingRelease {
    vbNumber:string = null;
    vbName:string = null;
    instrumentNumber:string = null;
    releaseType:NameDescription = null;
    referenceNumber:string = null;
    payee:Contact = null;
    receivedPriorNov2004:boolean = null;
    currentInterestAmount:number = null;
    currentAmount:number = null;
    originalAmount:number = null;
    releasePercentage:number = null;
    managerApprovalDate:string = null;
    approvalManager:string = null;
    comment:string = null;
    interestCurrentAmountTotal:number = null;
    interestAuthorizedAmountTotal:number = null;
    authorizedAmount:number = null;
    interestAmount:number = null;
    securityId:number = null;
    releaseId:number = null;
    selected:boolean = false;
    releaseTypeName:string = null;
    payeeName:string = null;
    securityTypeName:string = null;
}