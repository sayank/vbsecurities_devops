export class NameDescription {

	public id: number;
	public name: string;
	public description: string;
	public inactive: boolean = false;
	public selected: boolean = false;

	constructor(id: number, name: string, description: string) {
		this.id = id;
		this.name = name;
		this.description = description
	}

}