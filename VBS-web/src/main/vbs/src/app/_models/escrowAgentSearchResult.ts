export class EscrowAgentSearchResult {
  public id:number = null;
  public crmContactId:string = null;
  public escrowAgent:string = null;
  public escrowAgentLicenceStatus:string = null;
  public licenceApplicationStatus:string = null;
  public alert:string = null;
}
