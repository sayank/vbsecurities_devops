export class Address {
	id:number = null;
	version:number = null;
	addressLine1:string = null;
	addressLine2:string = null;
	addressLine3:string = null;
	addressLine4:string = null;
	city:string = null;
	postalCode:string = null;
	province:string = null;
	country:string = null;
	createDate:string = null;
	createUser:string = null;
	updateDate:string = null;
	updateUser:string = null;
	addressConcat:string = null;
}
