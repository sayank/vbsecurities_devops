export class EnrolmentPoolActivity {

    vbNumber: string = null;
    enrolmentNumber: string = null;
    vbDeleteReason: string = null;
	enrollingVb: string = null;
	enrolmentType: string = null;
    enrolmentAddress: string = null;
    vbName: string = null;
    user: string = null;
    updateDate: string = null;

}