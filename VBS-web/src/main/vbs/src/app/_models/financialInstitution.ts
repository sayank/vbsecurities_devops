import { ContactFinancialInstitution } from "./contactFinancialInstitution";
import { FinancialInstitutionSigningOfficer } from "./financialInstitutionSigningOfficer";
import { FinancialInstitutionMaaPoa } from "./financialInstitutionMaaPoa";
import { FinancialInstitutionAlert } from "./financialInstitutionAlert";

export class FinancialInstitution {

    id:number = null;
    version:number = null;
    name:string = null;
	description:string = null;
	expiryDate:string = null;
	maxSecurityAmount:number = null;
	wnSecurityMaxAmount:number = null;
	financialInstitutionTypeId:number = null;
	maaPoas:FinancialInstitutionMaaPoa[] = null;
	contacts:ContactFinancialInstitution[] = null;
    alerts:FinancialInstitutionAlert[] = null;
	availableAmountFi: number = null;
	public createDate:string = null;
	public createUser:string = null;
	public updateDate:string = null;
	public updateUser:string = null;
}
