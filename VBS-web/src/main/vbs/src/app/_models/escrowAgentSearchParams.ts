
export class EscrowAgentSearchParams {
	public contactId:string = null;
	public contactIdTo:string = null;
	public escrowAgentName:string = null;
	public escrowAgentLicensStatusId:number = null;
	public licensApplicationStatusId: number  = null;
	public contactIdSearchType:string = 'BEGIN_WITH';
	public escrowAgentNameSearchType: string = 'BEGIN_WITH';
	public escrowAgentLicensStatusSearchType:string = 'EQUALS';
	public licensApplicationStatusSearchType:string= 'EQUALS';
}
