import { NameDescription } from "./nameDescription";

export class ReleaseSearchParameters {
    dateSearchType:string  = 'EQUALS';
    releaseUpdateStartDate:string = null;
    releaseUpdateEndDate:string = null;
    releaseTypes:NameDescription[] = null;
    releaseStatus:string = null;
}
