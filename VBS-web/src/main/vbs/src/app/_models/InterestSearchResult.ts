export class InterestSearchResult {
	periodType:string = null;
	year:number;
	fromDate:string;
	toDate:string;
	accruedInterest: number;
	discountAmount:number;
}