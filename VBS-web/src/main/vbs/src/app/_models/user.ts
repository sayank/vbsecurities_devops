import { Permission } from "./permission";

export class User {
  userId: string;
  emailAddress: string;
  lastName: string;
  firstName: string;
  supervisor: string;  
  permissions: Permission[];

  hasPermission(permission:string):boolean{
	for(let p in this.permissions){
		if(this.permissions[p].permissionName == permission){
			return true;
		}
	}
	return false;
  }
    hasNoPermissions():boolean{
        if(this.permissions.length == 0){
            return true;
        }
        return false;
    }
}
