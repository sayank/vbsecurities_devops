export class AutoReleaseResult {
	id:number = null;
	runId:number = null;
	securityId:number = null;
	releaseId:number = null;
	vNumber:number = null;
	vName:string = null;
	licenseStatus:string = null;
	instrumentNumber:string = null;
	enrolmentNumber:number = null;
	enrolmentAddress:string = null;
	securityType:string = null;
	releaseTimingType:string = null;
	deletedBy:string = null;
	eliminationCriteria:string = null;
	securityReceivedDate:string = null;
	warrantyStartDate:string = null;
	delete:boolean = null;
}
