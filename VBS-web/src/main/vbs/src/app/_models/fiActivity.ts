export class FIActivity {

    subject: string = null;
    oldValue: string = null;
    newValue: string = null;
    user: string = null;
    updateDate: string = null;

}
