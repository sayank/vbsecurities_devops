import { FinancialInstitutionMaaPoaInstitution } from "./financialInstitutionMaaPoaInstitution";
import {FinancialInstitution} from "./financialInstitution";
import { FinancialInstitutionSigningOfficer } from "./financialInstitutionSigningOfficer";

export class FinancialInstitutionMaaPoa {
    id:number = null;
	version:number = null;
	
	maaPoaSequence:number = null;
    expirationDate:string = null;
    adminAgentBroker:FinancialInstitution = null;
    maaPoaInstitutions:FinancialInstitutionMaaPoaInstitution[] = [];
	signingOfficers:FinancialInstitutionSigningOfficer[] = null;
	financialInstitutionId:number = null;
	deleted:boolean = null;

	createDate:string = null;
	createUser:string = null;
	updateDate:string = null;
	updateUser:string = null;
}
