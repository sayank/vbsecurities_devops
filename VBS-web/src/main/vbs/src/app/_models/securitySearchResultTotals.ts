export class SecuritySearchResultTotals {
	public totalOriginalAmount:number = null;
	public totalCurrentAmount:number = null;
	public totalCashAmount:number = null;
	public totalCurrentInterestAmount:number = null;
	public totalAvailableCashAmount:number = null;
    public totalOriginalAmountExcludingDtaPef:number = null;
    public totalCurrentAmountExcludingDtaPef:number = null;
}
