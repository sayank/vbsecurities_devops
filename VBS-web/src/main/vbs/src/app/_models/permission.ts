export class Permission {

	constructor(
		public id: number,
		public permissionName: string,
		public description: string
	 ){}

}