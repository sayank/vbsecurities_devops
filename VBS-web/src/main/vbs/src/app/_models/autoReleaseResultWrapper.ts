import { AutoReleaseResult } from "./autoReleaseResult";
import { AutoReleaseRunSearchResult } from "./autoReleaseRunSearchResult";

export class AutoReleaseResultWrapper {
	autoReleasePassedResults:AutoReleaseResult[] = [];
	autoReleaseFailedResults:AutoReleaseResult[] = [];
    finalStepCompleted:boolean = null;
    autoReleaseRun: AutoReleaseRunSearchResult = null;
}
