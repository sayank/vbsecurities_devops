export class PoolSecuritiesParameters {
    securityIds:number[]  = null;
    poolId:number = null;
    unitsIntendedToCover:number = null;
    securityPurposeId:number = null;
}
