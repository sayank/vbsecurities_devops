import { FinancialInstitution } from "./financialInstitution";
import { Branch } from "./branch";

export class FinancialInstitutionMaaPoaInstitution {
    id:number = null;
	version:number = null;
    
    financialInstitution:FinancialInstitution = null;
    branch:Branch = null;
    percentage:number = null;
    originalAmountOnInstitution:number = null;
    currentAmountOnInstitution:number = null;
    deleted:boolean = null;

	createDate:string = null;
	createUser:string = null;
	updateDate:string = null;
	updateUser:string = null;
}
