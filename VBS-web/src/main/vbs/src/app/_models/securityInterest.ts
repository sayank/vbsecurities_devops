export class SecurityInterest {
	id:number = null;
	version:number = null;
	securityId:number = null;
	interestRateId:number = null;
	interestAnnualRate:number = null;
	interestDate:string = null;
	dailyInterestAmount:number = null;
	totalInterestAmount:number = null;
	status: string = null;
	createDate:string = null;
	createUser:string = null;
	updateDate:string = null;
	updateUser:string = null;   
}