export class AutoRelease {
	runId:number = null;
	runDate:string = null;
	enrolmentNumber:string = null;
	enrolmentAddress:string = null;
	timingType:string = null;
	eliminations:string[] = null;
	releaseSeqNum:number = null;
	releaseDate:string = null;
	deletedBy:string = null;
}
