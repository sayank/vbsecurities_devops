export class AutoReleaseRunSearchResult {

    id:number = null;
    version:number = null;
    runStatusId:number = null;
    description:string = null;
	createUser:string = null;
	createDate:string = null;
	updateUser:string = null;
    updateDate:string = null;
}
