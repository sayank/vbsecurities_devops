import { SecuritySearchResultTotals } from "./securitySearchResultTotals";
import { SecuritySearchResult } from "./securitySearchResult";

export class SecuritySearchResultWrapper {
	public queryString:string = null;
	public totals:SecuritySearchResultTotals = null;
	public securities:SecuritySearchResult[] = [];
}
