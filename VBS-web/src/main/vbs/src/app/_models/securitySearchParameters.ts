
export class SecuritySearchParameters {
	public instrumentNumber:string = null;
	public instrumentNumberTo:string = null;
	public instrumentNumberSearchType:string = 'EQUALS';
	public vbNumberType: string = 'EQUALS';
	public vbNumber:any = null;
	public enrolmentNumber:string = null;
	public financialInstitutionId:any = null;
	public financialInstitutionTypeId: number = null;
	public branchId:number = null;
	public escrowAgentContactId:any = null;
	public escrowAgentContactIdTo:any = null;
	public escrowAgentContactIdType:string = 'EQUALS';
	public securityTypeId:number = null;
	public securityPurposeId:number = null;
	public currentAmountType:string = 'EQUALS';
	public currentAmount:number = null;
	public currentAmountTo:number = null;
	public securityNumber:string = null;
	public securityNumberTo:number = null;
	public securityNumberSearchType:string = 'EQUALS';
	public poolId:number = null;
	public receivedDateType: string = 'EQUALS';
	public receivedDate:number = null;
	public receivedDateBetweenTo:string = null;
	public showRecentSecurities:boolean = null;
}
