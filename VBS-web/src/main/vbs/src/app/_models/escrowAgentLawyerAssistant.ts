import { Contact } from "./contact";

export class EscrowAgentLawyerAssistant {
	escrowAgent:Contact = new Contact();
	lawyer:Contact = new Contact();
	assistant:Contact = new Contact();
}