import { EscrowAgentSearchResult } from "./escrowAgentSearchResult";

export class EscrowAgentSearchResultWrapper {
	public queryString:string = null;
	public escrowAgents:EscrowAgentSearchResult[] = [];
}
