import { FinancialInstitution } from "./financialInstitution";
import { Address } from "./address";
import { Branch } from "./branch";
import { FinancialInstitutionMaaPoa } from "./financialInstitutionMaaPoa";

export class FinancialInstitutionAndBranch {

    financialInstitution:FinancialInstitution = new FinancialInstitution();
    branch:Branch = new Branch();

}
