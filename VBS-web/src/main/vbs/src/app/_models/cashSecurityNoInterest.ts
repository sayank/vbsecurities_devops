export class CashSecurityNoInterest {

    private securityNumber: number = null;
	private vbNumber: string = null;
	private vbName: string = null;
	private instrumentNumber: string = null;
	private issuedDate: string = null;
	private receivedDate: string = null;
	private originalAmount: number = null;
	private currentAmount: number = null;
    private lastInterestCalculatedDate: string = null;
    private missingInterestDates: string = null;

}
