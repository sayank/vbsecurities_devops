export class SecurityInterestSearchParams {
    securityId: number = null;
    fromDate: string = null;
    toDate: string = this.today();
    periodType: string = 'DAILY';

    private today():string {
        let d = new Date();
        return d.getFullYear()+'-'+ ("0"+(d.getMonth()+1)).slice(-2)+'-'+("0"+d.getDate()).slice(-2)+'T00:00:00.000';
    }
}
