export class FinancialInstitutionSigningOfficer {
    id:number = null;
	version:number = null;
	
	financialInstitutionMaaPoaId:number = null;
	companyName:string = null;
	contactName:string = null;
	deleted:boolean = null;

	createDate:string = null;
	createUser:string = null;
	updateDate:string = null;
	updateUser:string = null;
}
