import { Component, OnInit } from '@angular/core';
import {ManifestUtil} from "../../_util/manifestUtil";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(public manifestUtil:ManifestUtil) { }

  ngOnInit() {
  }

  get currentYear():number {
      return new Date().getFullYear();
  }

}
