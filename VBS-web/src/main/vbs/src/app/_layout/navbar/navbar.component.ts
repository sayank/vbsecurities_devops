import { Component, OnInit} from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { UserService } from '../../_services/user.service';
import { Session } from '../../_session/session';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit{

	error: string = "";
	currentUrl: string = "";

constructor(
		private router: Router, 
		private session: Session
	) { }
	
	ngOnInit() {
		
	}

	ngAfterViewInit()	{
		this.router.events.subscribe(r => {
			if(r instanceof NavigationEnd){
				let n = r as NavigationEnd;
				this.currentUrl = n.url;
			}
		})
	}

	logout() {
		//this.router.navigateByUrl('/login');
		this.router.navigate(['/login'], { queryParams: { logout : "true"}});
	}

	getSession(){
		return this.session;
	}

	linkClass(page: string): string{
		return ("/"+page == this.currentUrl) ? "nav-link-selected" : "";
	}
}
