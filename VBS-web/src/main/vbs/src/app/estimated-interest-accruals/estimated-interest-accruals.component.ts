import {AfterViewInit, Component, Input, OnChanges} from '@angular/core';
import {InterestSearchResult} from '../_models/InterestSearchResult';
import {InterestService} from '../_services/interest.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {SecurityInterestSearchParams} from '../_models/securityInterestSearchParams';
import {Session} from '../_session/session';

@Component({
  selector: 'app-estimated-interest-accruals',
  templateUrl: './estimated-interest-accruals.component.html',
  styleUrls: ['./estimated-interest-accruals.component.css']
})
export class EstimatedInterestAccrualsComponent implements OnChanges {

    @Input("securityId") securityId:number;
    @Input("receivedDate") receivedDate:string;

	results: InterestSearchResult[];
	form: FormGroup;
	searchParams: SecurityInterestSearchParams = new SecurityInterestSearchParams();
	searching:boolean = false;
    downloadingExcel:boolean = false;

	constructor(private fb: FormBuilder, private interestService: InterestService, private session: Session) { }

	ngOnChanges(){
        if(this.searchParams.fromDate == null){
            this.searchParams.fromDate = this.receivedDate;
            this.searchParams.securityId = this.securityId;
            this.form = this.fb.group(this.searchParams);
        }
    }

	searchInterest() {
		this.searching = true;
		this.results = null;
		this.searchParams = this.form.getRawValue();
		this.interestService.searchInterestBySecurityId(this.searchParams).subscribe(
			securityInterest => {
				this.results = securityInterest;
                this.searching = false;
			},
			error => {
				this.session.addError('Error when retrieving interest accruals.', error);
				this.results = [];
			}
		);
	}

    exportToExcel() {
        this.downloadingExcel = true;
        this.interestService.searchResultsExcel(this.searchParams).subscribe(data => {
                const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                let d = new Date();
                const element = document.createElement('a');
                element.href = URL.createObjectURL(blob);
                element.download = "security-"+this.securityId+"-interest-"+d.getTime()+".xlsx";
                document.body.appendChild(element);
                element.click();
                element.remove();
            },
            error => {},
            () => {this.downloadingExcel = false;});
    }
}
