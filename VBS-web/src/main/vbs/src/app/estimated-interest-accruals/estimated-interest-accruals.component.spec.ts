import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstimatedInterestAccrualsComponent } from './estimated-interest-accruals.component';

describe('EstimatedInterestAccrualsComponent', () => {
  let component: EstimatedInterestAccrualsComponent;
  let fixture: ComponentFixture<EstimatedInterestAccrualsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstimatedInterestAccrualsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstimatedInterestAccrualsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
