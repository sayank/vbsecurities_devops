import { Component, OnInit } from '@angular/core';
import { Session } from './_session/session';
import { VbsError } from './_models/vbsError';
import { Title } from '@angular/platform-browser';
import { EnrolmentService } from './_services/enrolment.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

	errors: VbsError[] = [];
	messages: string[] = [];

	constructor(private enrolmentService: EnrolmentService, private titleService: Title, private session: Session){

	}

	ngOnInit(): void {
		
		this.titleService.setTitle('VBS');

		this.session.onError().subscribe(error => {
			if (error.httpError == null || error.httpError.status != 401) {
				this.errors.push(error)
				setTimeout(() => {
					this.close(error);
				}, 5000);
			}
		});
		this.session.onMessage().subscribe(message => {
			
			this.messages.push(message)
			setTimeout(() => {
				this.closeMessage(message);
			}, 5000);
		
		});

		this.session.onCloseMessage().subscribe(message => {
			this.closeMessage(message);
		});

	}

	close(error: VbsError) {
		let index = this.errors.indexOf(error);
		if(index >= 0) {
			this.errors.splice(index, 1);
		}
	}

	closeMessage(message: string) {
		let index = this.messages.indexOf(message);
		if(index >= 0) {
			this.messages.splice(index, 1);
		}
	}

}
