import {Directive, ElementRef, forwardRef, HostListener, Renderer2} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {Contact} from "../_models/contact";

const noop = () => { };

export const CONTACT_FORMATTER_CONTROL_VALUE_ACCESSOR: any = {
	provide: NG_VALUE_ACCESSOR,
	useExisting: forwardRef(() => ContactFormatterDirective),
	multi: true
};

@Directive({
	selector: '[contactFormatter][formControlName]',
	providers: [CONTACT_FORMATTER_CONTROL_VALUE_ACCESSOR]
})
export class ContactFormatterDirective implements ControlValueAccessor {
  private el: HTMLInputElement;
  private innerValue: any;

  constructor(private elementRef: ElementRef, private renderer: Renderer2) {
    this.el = elementRef.nativeElement;
  }

  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (a: any) => void = noop;

  // set getter
  get value(): any {
    return this.innerValue;
  }

  // set accessor including call the onchange callback
  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
    }
  }

  // From ControlValueAccessor interface
  writeValue(value: any) {
    if (value !== this.innerValue) {
      this.el.value = this.process(value);
      if (value) {
        this.renderer.setAttribute(this.elementRef.nativeElement, 'value', value);
      }
      this.innerValue = value;
    }
  }

  // From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  // From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  setDisabledState(isDisabled:boolean) {
   this.el.disabled = isDisabled;
  }

  // On Blue remove all symbols except last . and set to currency format
  @HostListener('blur', ['$event.target.value'])
  onBlur(value) {
    this.onTouchedCallback();
    this.el.value = this.process(value);
    this.onChangeCallback(this.innerValue);
    if (this.innerValue) {
      this.renderer.setAttribute(this.elementRef.nativeElement, 'value', this.innerValue);
    }
  }

  // On Change remove all symbols except last . and set to currency format
  @HostListener('change', ['$event.target.value'])
  onChange(value) {
    this.el.value = this.process(value);
    this.onChangeCallback(this.innerValue);
    if (this.innerValue) {
      this.renderer.setAttribute(this.elementRef.nativeElement, 'value', this.innerValue);
    }
  }

	process(value: any): string {
    let current:Contact = null;
    if(value != ""){
        current = value as Contact;
        if (current != null && current.crmContactId === undefined) {
            let crmContactId = (value as string).split(" - ")[0];
            if(this.innerValue != null && (this.innerValue as Contact).crmContactId == crmContactId){
                current = (this.innerValue as Contact);
            }else{
                current = new Contact();
                current.crmContactId = crmContactId;
            }
        }
    }
    this.innerValue = current;
		return this.getDisplayString(this.innerValue);
	}

	getDisplayString(contact: Contact): string {
		if(contact == null) {
			return "";
		}
		
		if (contact.id !== undefined) {
			if (contact.crmContactId != null) {
				return contact.crmContactId + ((contact.companyName != null) ? " - " + contact.companyName : ((contact.firstName != null) ? " - " + contact.firstName + " " + contact.lastName : ""));
			} else {
				return "";
			}
		} else {
			return contact.toString();
		}
	}
}
