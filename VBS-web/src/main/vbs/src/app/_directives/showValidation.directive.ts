import { Directive, ElementRef, HostListener, ApplicationRef, Component } from "@angular/core";
import { Injector, EmbeddedViewRef, ComponentFactoryResolver, AfterViewInit } from "@angular/core";
import { NgControl } from "@angular/forms";




@Directive({
  selector: '[showValidation][formControlName]'
})
export class ShowValidation implements AfterViewInit {
	fieldLabel: string;
	
	constructor(private el: ElementRef, private control : NgControl,
		private componentFactoryResolver: ComponentFactoryResolver,
      	private appRef: ApplicationRef,
      	private injector: Injector){}

	@HostListener('blur',['$event']) onEvent($event){
		this.collectErrors(true);
	}

	ngAfterViewInit(){
		//attempt to set fieldName based on label having for="<the form control name of the target form control>"
		let fcn = this.el.nativeElement.attributes['formcontrolname'];
		if(fcn != undefined && fcn != null){
			this.el.nativeElement.id = fcn.value;
			let lbl = document.querySelector('label[for=' + fcn.value + ']');
			if(lbl != undefined && lbl != null){
				this.fieldLabel = lbl.textContent;
			}
		}
		if(this.fieldLabel == null){
			//attempt based on known label locations relative to the input
			let parent = this.el.nativeElement.parentNode;
			if(this.el.nativeElement.getAttribute('data-label') != null) {
				this.fieldLabel = this.el.nativeElement.getAttribute('data-label');
			} else if(parent.previousElementSibling != null && parent.previousElementSibling.className.includes('col-form-label')){
			  this.fieldLabel = parent.parentNode.querySelector('label').textContent;
			}else if(parent.parentNode.previousElementSibling != null && parent.parentNode.previousElementSibling.className.includes('col-form-label')){
				this.fieldLabel = parent.parentNode.parentNode.querySelector('label').textContent;
			}else if(parent.className.includes('input-group') || parent.className.includes('col-md-6')) {
        let label = this.el.nativeElement.parentNode.parentNode.querySelector('label');
        if (label == null) {
          label = this.el.nativeElement.parentNode.parentNode.parentNode.querySelector('label');
        }
        if(label != null){this.fieldLabel = label.textContent;}else{this.fieldLabel = '';}
      }else if(parent.className.includes('datatable-body-cell-label')){
			  //TODO fix.
			}else{
			    this.fieldLabel = this.el.nativeElement.parentNode.querySelector('label').textContent;
			}
		}

		try{
			this.control.statusChanges.subscribe(change => this.collectErrors(false));
		}catch{
			
		}
	}

	collectErrors(emit:boolean){
		let errors = [];
		for(let e in this.control.errors){
			let msg;
			switch(e){
				//standard types
				case 'required': 
					msg = this.fieldLabel + ' is required.'; 
					errors.push(msg);
				break;
				case 'pattern': 
					msg = this.fieldLabel + ' is invalid.'; 
					errors.push(msg);
				break;
				case 'min': 
					msg = this.fieldLabel + ' must be at least ' + this.control.errors[e].min + '.'; 
					errors.push(msg);
				break;
				case 'ngbDate': 
					msg = this.fieldLabel + ' must be a valid date';
					errors.push(msg);
				break;
				case 'moreThanInterest':
					msg = this.fieldLabel + ' Cannot be more than Total the Interest Amount.';
					errors.push(msg);
				break;
				//server messages
				case 'serverErrors':
					for(let a in this.control.errors[e]){
						errors.push(this.control.errors[e][a]);
					}
				break;
			}
			
		}

		let div = this.el.nativeElement.parentNode.querySelectorAll('div.invalid-feedback[for="'+this.control.name+'"]');
		if(div != null && div.length){div[0].remove();}
		
		if(errors.length > 0){
			this.control.control.markAsDirty({onlySelf: true});
			let html = "";
			for(let a in errors){ if(errors[a].length){html += html + errors[a] + "<br>";}}
			this.el.nativeElement.parentNode.insertAdjacentHTML('beforeend', '<div class="invalid-feedback" for="' + this.control.name + '">' + html + '</div>');
		}
	}

}
