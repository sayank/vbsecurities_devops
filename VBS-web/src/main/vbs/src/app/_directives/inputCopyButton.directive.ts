import {Directive, ElementRef, Input, OnInit, Renderer2} from '@angular/core';
import {NgControl} from '@angular/forms';
import {Contact} from "../_models/contact";
import {ClipboardService} from "ngx-clipboard";
import {Branch} from "../_models/branch";

@Directive({
	selector: '[copyButton][formControlName]'
})
export class InputCopyButtonDirective implements OnInit {

  @Input("copyAddress") private copyAddress: Boolean = true;

  constructor(private el: ElementRef, private ngControl: NgControl, private renderer: Renderer2, private clipboard: ClipboardService) {}

  ngOnInit(): void {
    let iga:ElementRef = this.renderer.createElement('div');
    this.renderer.addClass(iga, "input-group-append");
    let igt:ElementRef = this.renderer.createElement('div');
    this.renderer.addClass(igt, "input-group-text");
    this.renderer.setStyle(igt, "cursor", "pointer");
    this.renderer.setAttribute(igt, "title", "Copy to Clipboard");
    let button:ElementRef = this.renderer.createElement('span');
    this.renderer.addClass(button, "oi");
    this.renderer.addClass(button, "oi-clipboard");

    let ig:ElementRef;
    if(this.el.nativeElement.parentNode.className.indexOf("input-group") != -1){
      ig = this.el.nativeElement.parentNode;
    }else {
      ig = this.renderer.createElement('div');
      this.renderer.addClass(ig, "input-group");
      this.renderer.appendChild(this.el.nativeElement.parentNode, ig);
      this.renderer.appendChild(ig, this.el.nativeElement);

    }
    this.renderer.appendChild(ig, iga);
    this.renderer.appendChild(iga, igt);
    this.renderer.appendChild(igt, button);

    this.renderer.listen(igt, 'click', () => {
      let s:string = "";
      if(this.ngControl.control.value != null) {
        if (this.ngControl.control.value.crmContactId !== undefined) {
          let contact = this.ngControl.control.value as Contact;
          s += (contact.companyName != null) ? contact.companyName: contact.firstName + " " + contact.lastName;
          if(this.copyAddress && contact.address != null) {
            s += "\r"+ contact.address.addressLine1 + "\r";
            if (contact.address.addressLine2 != null && contact.address.addressLine2.trim().length) {
              s += contact.address.addressLine2 + "\r";
            }
            if (contact.address.addressLine3 != null && contact.address.addressLine3.trim().length) {
              s += contact.address.addressLine3 + "\r";
            }
            if (contact.address.addressLine4 != null && contact.address.addressLine4.trim().length) {
              s += contact.address.addressLine4 + "\r";
            }
            s += contact.address.city + ", " + contact.address.province + ", " + contact.address.postalCode;
          }
        } else if (this.ngControl.control.value.financialInstitutionId !== undefined) {
          let branch = this.ngControl.control.value as Branch;
          s += branch.name;
          if(this.copyAddress) {
            s += "\r" + branch.description + "\r";
          }
        } else {
          s += this.ngControl.control.value;
        }
      }
      this.clipboard.copyFromContent(s);
    });
  }
}
