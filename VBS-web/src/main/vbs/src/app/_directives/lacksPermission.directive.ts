import { Directive, Input, ElementRef, TemplateRef, ViewContainerRef } from '@angular/core';
import { Session } from '../_session/session';
import { User } from '../_models/user';

@Directive({
  selector: '[lacksPermission]'
})
export class LacksPermission {

	constructor(
		private element: ElementRef,
		private templateRef: TemplateRef<any>,
		private viewContainer: ViewContainerRef,
		private session: Session
  	) {this.currentUser = this.session.getUser();}

  	currentUser:User;

  	ngOnInit() {
		
	}

  	@Input()
  	set lacksPermission(val) {
      let allow = true;
      for(let p in this.currentUser.permissions){
        if(this.currentUser.permissions[p].permissionName == val){
          allow = false;
          break;
        }
      }
      if(allow) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      } else {
        this.viewContainer.clear();
      }
  	}
}
