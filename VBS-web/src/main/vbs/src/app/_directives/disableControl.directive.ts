import {NgControl} from "@angular/forms";
import {Directive, DoCheck, Input, Optional} from "@angular/core";

@Directive({
    selector: '[formControl], [formControlName]'
})
export class DisableControlDirective implements DoCheck {
    constructor(@Optional() private control: NgControl) {}

    ngDoCheck(): void {
        if (this.control) {
            this.control.valueAccessor.setDisabledState(this.control.disabled);
        }
    }
}
