import {ChangeDetectorRef, Component, Input, OnChanges, OnInit, ViewChild, ElementRef, HostListener, AfterViewInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from "@angular/forms";
import {DatatableComponent} from "@swimlane/ngx-datatable";
import {Session} from "../_session/session";
import {ModalService} from "../_services/modal.service";
import {EnrolmentsSearchComponent} from "../_components/enrolments-search/enrolments-search.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {EnrolmentIndicatorsComponent} from "../_components/enrolment-indicators/enrolment-indicators.component";
import {EnrolmentService} from '../_services/enrolment.service';
import {Enrolment} from '../_models/enrolment';
import {EnrolmentActivitiesComponent} from "../_components/enrolment-activities/enrolment-activities.component";
import { BehaviorSubject } from 'rxjs';

@Component({
  	selector: 'app-homes-covered',
  	templateUrl: './homes-covered.component.html',
  	styleUrls: ['./homes-covered.component.css']
})
export class HomesCoveredComponent implements OnInit, OnChanges, AfterViewInit {

    @Input("form") form: FormGroup;
    @Input("tabChange") activeTab: string;

    selected:string = 'HOMES_COVERED';
    crmData:any = [];
    crmDataLoading:boolean = false;
    crmDataError:boolean = false;
    fieldMinSize: number = 56;
    calculating: boolean = false;

    @ViewChild('dataTable') dataTable: DatatableComponent;
    @ViewChild('datepickerContainer') datepickerContainer: ElementRef;

    @HostListener('window:resize', ['$event'])
	onResize(event) {
        this.bestFit(this.dataTable);
    }

	constructor(private session: Session, private modalService: ModalService, private enrolmentService: EnrolmentService,
                private ngbModal: NgbModal, private fb: FormBuilder, private cdRef: ChangeDetectorRef) {
	}

    ngOnChanges(){
	    this.form.updateValueAndValidity();
        this.cdRef.detectChanges();
        if(this.activeTab == 'tab-homes-covered') {
            setTimeout(() => {
                this.dataTable.recalculate();
                this.dataTable['cd'].markForCheck();
                setTimeout(() => this.bestFit(this.dataTable));
            }, 100);
        }
    }

    ngAfterViewInit(): void {
        this.bestFit(this.dataTable);
    }

  	ngOnInit() {
	    if(this.form.get('enrolments.0.enrolmentNumber').value != null){
            this.crmDataLoading = true;
            this.enrolmentService.getCRMEnrolments(this.form.get('poolId').value).subscribe(
                data => { this.addCrmData(data);this.crmDataLoading = false; },
                error => { this.crmDataError = true;this.crmDataLoading = false; }
            );
        }
	}

	addCrmData(wsEnrolments:Enrolment[]){
        wsEnrolments.forEach(wsEnrolment => {
            this.crmData[wsEnrolment.enrolmentNumber] = wsEnrolment;
        });
        setTimeout(() => {
            this.dataTable.recalculate();
            this.dataTable['cd'].markForCheck();
            setTimeout(() => this.bestFit(this.dataTable));
        }, 100);
    }
    addSingleCrmData(wsEnrolment:Enrolment){
        this.crmData[wsEnrolment.enrolmentNumber] = wsEnrolment;
    }

    openSearch(){
        let modal = this.ngbModal.open(EnrolmentsSearchComponent, {size: 'lg'});
        let searchComponent:EnrolmentsSearchComponent  = <EnrolmentsSearchComponent>modal.componentInstance;
        searchComponent.setOptions(this.form.get('poolId').value, this.form.get('securityTypeId').value == 5, this.form.get('identityTypeId').value, [], null);

        searchComponent.selected.subscribe(enrolment => {
            let fa = (this.form.get('enrolments.0.enrolmentNumber').value == null) ?
                this.fb.array([]) : (this.form.get('enrolments') as FormArray);
            let g = this.fb.group(enrolment);
            g.get('canDelete').setValue(true);
            fa.push(g);
            this.form.setControl('enrolments', fa);
            this.crmDataLoading = true;
            this.enrolmentService.getCRMEnrolment(enrolment.enrolmentNumber).subscribe(
                data => { this.addSingleCrmData(data);this.crmDataLoading = false; },
                error => { this.crmDataError = true;this.crmDataLoading = false; }
            );

        });
    }

    delete(rowIndex:number){
	    this.modalService.confirm({title: 'Delete Enrolment', size: 'sm', allowDismiss: false,
            body: 'Are you sure you want to delete '+this.form.get('enrolments.'+rowIndex+'.enrolmentNumber').value+'?<br/><br/>The deletion will be completed on saving.'})
            .subscribe(result => {
                if(result.result && !result.dismissed){
                    (this.form.get('enrolments') as FormArray).removeAt(rowIndex);
                    if(this.form.get('enrolments.0') == null){
                        (this.form.get('enrolments') as FormArray).push(this.fb.group(new Enrolment()));
                    }
                }
        });
    }

    openEnrolmentsIndicatorsModal(rowIndex:number){
        let modal = this.ngbModal.open(EnrolmentIndicatorsComponent, {size: 'lg'});
        let indicatorsComponent:EnrolmentIndicatorsComponent  = <EnrolmentIndicatorsComponent>modal.componentInstance;
        indicatorsComponent.setFormGroup((this.form.get('enrolments.'+rowIndex) as FormGroup));
    }

    openEnrolmentsActivitesModal(){
        let modal = this.ngbModal.open(EnrolmentActivitiesComponent, {size: 'lg'});
        let enrolmentActivitiesComponent:EnrolmentActivitiesComponent  = <EnrolmentActivitiesComponent>modal.componentInstance;
		enrolmentActivitiesComponent.setSecurityId(this.form.get('id').value);
		enrolmentActivitiesComponent.setVbPoolId(this.form.get('poolId').value);
    }

    changeSelected(newSelected:string){
        this.selected = newSelected;
	    setTimeout(() => {
            this.dataTable.recalculate();
            this.dataTable['cd'].markForCheck();
            setTimeout(() => this.bestFit(this.dataTable));
        }, 100);
    }

    get filteredEnrolmentList():Enrolment[] {
        let enrolments:Enrolment[] = [];
        if(this.form != null && this.form.get('enrolments').value.length){
            enrolments = this.form.get('enrolments').value.filter(v => v.enrolmentNumber != null);
        }
        if(enrolments.length == 0){enrolments.push(new Enrolment());}
        return enrolments;
    }

    /**
     * Helper methods
     */
    get unitText(): string {
        return this.form.get('securityTypeId').value == 5 ? 'Total # of Condo Unit' : 'Total # of Freehold Unit';
    }

    get totalFH(): number {
        return this.form.get('enrolments').value.filter(e => e.unitType === '002').length;
    }

    get totalCE(): number {
        return this.form.get('enrolments').value.filter(e => e.unitType === '001').length;
    }

    get totalUNIT(): number {
        return this.form.get('enrolments').value.filter(e => e.unitType === '003').length;
    }

    get coveredButtonText(): string {
        return this.form.get('securityTypeId').value == 5 ? 'PEF Tracking CE and Condo Units Covered' : 'Homes Covered';
    }

    get showFH(): boolean {
        return this.totalFH > 0 && this.form.get('securityTypeId').value != 5;
    }

    get canAdd():boolean {
        return this.session.getUser().hasPermission('CREATE_SECURITY') && this.form.get('fullyReleased').value != true;
    }

    crmEnrolment(enrolmentNumber:string):Enrolment{
        if(this.crmData[enrolmentNumber] === undefined){
            let e = new Enrolment();
            e.address = " ";
            return e;
        }
        return this.crmData[enrolmentNumber];
    }

    getErrorsForRow(id:number){
        return this.form.get('enrolments.'+id + '.enrolmentNumber').errors;
    }

    bestFit(d: DatatableComponent): void {
        const dbWrapper = d.element;
        let rowWrapper = d.element.getElementsByTagName('datatable-row-wrapper');
        if (rowWrapper && this.filteredEnrolmentList && this.filteredEnrolmentList.length > 0) {
            //release/1.2.5
            //bugfix/homes-covered-table
            console.info('Calculating table width...');
            const row: HTMLElement = <HTMLElement>rowWrapper[0].getElementsByTagName('datatable-body-row')[0];
            const cells = rowWrapper[0].getElementsByTagName('datatable-body-cell');
            let cell: HTMLElement = <HTMLElement>cells[cells.length-3];
            const longCols = this.selected === 'ALL' ? 2 : 1;
            const ngxDatatablesOffset = 1;

            if (dbWrapper.offsetWidth != 0) {
                if (dbWrapper.offsetWidth === row.offsetWidth) {
                    this.fieldMinSize = cell.offsetWidth;
                } else {
                    this.fieldMinSize = (dbWrapper.offsetWidth-row.offsetWidth+cell.offsetWidth-ngxDatatablesOffset)/longCols;
                }
                console.info('Done!');
                console.info('Veryfing table width...');
                if (dbWrapper.offsetWidth - row.offsetWidth > 5) {
                    console.warn('Not verified. Recalibrating...');
                    this.dataTable.recalculate();
                    setTimeout(() => this.bestFit(this.dataTable), 100);
                }
                console.info('Done!');
            }
        }
    }
}
