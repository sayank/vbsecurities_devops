import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomesCoveredComponent } from './homes-covered.component';

describe('HomesCoveredComponent', () => {
  let component: HomesCoveredComponent;
  let fixture: ComponentFixture<HomesCoveredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomesCoveredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomesCoveredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
