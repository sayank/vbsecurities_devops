import { Component, OnInit} from '@angular/core';
import { InterestService } from '../_services/interest.service';
import { InterestRate } from '../_models/interestRate';
import { Session } from '../_session/session';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { catchError, debounceTime, distinctUntilChanged } from "rxjs/operators";
import { of } from "rxjs";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { ModalService } from "../_services/modal.service";
import { UtilService } from "../_services/util.service";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";

@Component({
	selector: 'app-search-interest',
	templateUrl: './interest.component.html'
})
export class InterestComponent implements OnInit {

	rates: InterestRate[] = [];
	filteredRates: InterestRate[] = [];
    ratesLoaded: boolean = false;
	form: FormGroup;
	filterInput = new FormControl('');
	saving: boolean = false;
	minDate: NgbDateStruct = null;

	constructor(private fb: FormBuilder, private interestService: InterestService, private session: Session,
              private route: ActivatedRoute, private modalService: ModalService, private router: Router, private currencyMaskService: UtilService) {
    this.route.queryParams.subscribe((params: Params) => {
      if (params['add'] !== undefined) {
        this.setupForm(new InterestRate());
        this.router.navigate(['.'], { relativeTo: this.route, queryParams: { add : null},  queryParamsHandling: "merge" });
      }
    });

    this.interestService.getNextValidStartDate().subscribe(date => {
      this.minDate = {year: Number(date.substring(0, 4)), month: Number(date.substring(5, 7)), day: Number(date.substring(8, 10))};
    });
  }

	ngOnInit() {
		this.interestService.getAllInterestRates().subscribe(
			rates => {this.rates = rates; this.filteredRates = rates;this.ratesLoaded = true;},
			error => this.session.addError('Error when loading interest rates', error)
		);

		this.filterInput.valueChanges.pipe(debounceTime(300), distinctUntilChanged()).subscribe(s => {
      this.filter(s);
    });
	}

  save(){
	  this.saving = true;

    this.interestService.validateInterestRate(this.form.value).pipe(catchError(error => of(error))).subscribe(result => {
      if (result != null) {
        this.saving = false;
        switch (result.status) {
          case 422: //failed validation
            let arr = {};
            for (let err in result.error) {
              if (arr[result.error[err].errorFieldName] === undefined) { arr[result.error[err].errorFieldName] = []; }
              arr[result.error[err].errorFieldName].push(result.error[err].errorDescription);
            }
            for (let field in arr) {
              if(field != null && this.form.get(field) != null){
                this.form.get(field).setErrors({ 'serverErrors': arr[field] });
              }
            }
            break;
          default: this.session.addError('Error when adding interest rate', result);
        }
      } else {
        this.interestService.saveInterestRate(this.form.value).subscribe(
          interestRates => {
              this.saving = false;
              this.form = null;
              this.rates = interestRates;
              this.filter(null);
            },
          error => {
            this.saving = false;
            this.session.addError('Error when saving interest rate', error);
          }
        );
      }
    });
  }

  setupForm(rate:InterestRate){
	  this.form = this.fb.group(rate);

  }

  delete(){
	  this.saving = true;
    this.modalService.confirm({title: "Delete Interest Rate", body: "Are you sure you wish to delete this Interest Rate?", size: "sm", allowDismiss: false}).subscribe(confirmation => {
        if(confirmation.result){
          this.interestService.deleteInterestRate(this.form.value).subscribe(deleted => {
              this.rates = this.rates.filter(r => r.id != this.form.value.id);
              this.form = null;
              this.filter(null);
              this.saving = false;
            },
            error => {
              if(error.status == 422){
                this.session.addError('Attempted to delete an interest rate that cannot be deleted.', error);
              }else {
                this.session.addError('A server-side error occurred while attempting to delete.', error);
              }
              this.saving = false;
            }
          );
        }else{
          this.saving = false;
        }
      });
  }

  filter(s:string){
    this.filteredRates = [];
    if(s != null && s.length){
      for(let i=0;i<this.rates.length;i++){
        let r = this.rates[i];
        if(this.currencyMaskService.transform(r.annualRate.toString()).indexOf(s) >= 0 || r.createDate.indexOf(s) >= 0 ||
          r.createUser.toUpperCase().indexOf(s.toString().toUpperCase()) >= 0 || r.interestStartDate.indexOf(s) >= 0){
          this.filteredRates.push(r);
        }
      }
    }else{
      this.filterInput.setValue('');
      this.filteredRates = this.rates;
    }
  }

}
