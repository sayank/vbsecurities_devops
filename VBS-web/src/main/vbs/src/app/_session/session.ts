import {Injectable} from '@angular/core';
import {User} from '../_models/user';
import {Security} from '../_models/security';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';
import {VbsError} from '../_models/vbsError';
import {SecuritySearchParameters} from '../_models/securitySearchParameters';
import {Release} from '../_models/release';
import {Alert} from '../_models/alert';
import {SecurityInterest} from '../_models/securityInterest';
import {AutoReleaseRunSearchResult} from '../_models/autoReleaseRunSearchResult';
import {AutoReleaseRunSearchParameters} from '../_models/autoReleaseRunSearchParameters';
import {FinancialInstitution} from '../_models/financialInstitution';
import {Md5} from 'ts-md5';
import {Router} from "@angular/router";

@Injectable()
export class Session {

    private loginSubject = new Subject<any>();
	private errorSubject = new Subject<VbsError>();
	private messageSubject = new Subject<string>();
	private closeMessageSubject = new Subject<string>();
	private securitySubject:BehaviorSubject<Security>;
	private releasesSubject:BehaviorSubject<Release[]> = new BehaviorSubject([]);
	private releaseOpenSubject:BehaviorSubject<boolean> = new BehaviorSubject(false);
	private interestSubject = new Subject<SecurityInterest>();

	private alertsSubject:BehaviorSubject<Alert[]> = new BehaviorSubject([]);

	private latestSecuritySearchHash: any = null;
	private latestSecuritySearchResult: any = null;

	public constructor(private router:Router){}

    setUiMap(uiMap){
        localStorage.setItem('uiMap', JSON.stringify(uiMap));
    }

    getProperty(key:string, ...args: any[]){
        let map = JSON.parse(localStorage.getItem('uiMap'));
        if(map === undefined || map == null){
            this.router.navigate(['/login']);
        }else if(map[key] === undefined || map[key] == null){
            console.log("Properties key '"+ key + "' was undefined.");
        }else{
            if(args !== undefined && args.length > 0){
                if(args.length == 1){
                    return map[key] + args[0];
                }else if(args.length > 1){
                    return (map[key] as string).replace(/{(\d+)}/g, function(match, number) {
                        return typeof args[number] != 'undefined' ? args[number] : match;
                    });
                }
            }else{
                return map[key];
            }
        }
        return null;
    }

	getUser(): User { return Object.assign(new User, JSON.parse(localStorage.getItem('user'))); }

	setUser(user: User): void { localStorage.setItem('user', JSON.stringify(user)); }

	setCreateSecurity(security: Security) {
		localStorage.setItem('createSecurity', JSON.stringify(security));
	}

	getCreateSecurity(): Security { return Object.assign(new Security, JSON.parse(localStorage.getItem('createSecurity'))); }

	getSearchParams(): SecuritySearchParameters {
		return Object.assign(new SecuritySearchParameters, JSON.parse(localStorage.getItem('SecuritySearchParameters')));
	}

	setSearchParams(params: SecuritySearchParameters) {
		localStorage.setItem('SecuritySearchParameters', JSON.stringify(params));
	}

	setAutoReleaseSearch(autoReleaseRunSearchParameters: AutoReleaseRunSearchParameters, autoReleaseRunSearchResult: AutoReleaseRunSearchResult[]) {
		localStorage.setItem('autoReleaseRunSearchParameters', JSON.stringify(autoReleaseRunSearchParameters));
		localStorage.setItem('autoReleaseRunSearchResult', JSON.stringify(autoReleaseRunSearchResult));
	}

	getAutoReleaseSearchParameters(): AutoReleaseRunSearchParameters {
		return Object.assign(new AutoReleaseRunSearchParameters, JSON.parse(localStorage.getItem('autoReleaseRunSearchParameters')));
	}

	getAutoReleaseSearchResults(): AutoReleaseRunSearchResult[] {
		return Object.assign([], JSON.parse(localStorage.getItem('autoReleaseRunSearchResult')));
  	}
    
  	setFinancialInstitutionSearch(searchParameters: any, searchResult: FinancialInstitution[]) {
		localStorage.setItem('financialInstitutionSearchParameters', JSON.stringify(searchParameters));
		localStorage.setItem('financialInstitutionSearchResult', JSON.stringify(searchResult));
	}

	getFinancialInstitutionSearchParameters(): any {
		return JSON.parse(localStorage.getItem('financialInstitutionSearchParameters'));
	}

	getFinancialInstitutionSearchResults(): FinancialInstitution[] {
		return Object.assign([], JSON.parse(localStorage.getItem('financialInstitutionSearchResult')));
	}

	login() {
		this.loginSubject.next(null);
	}

	onLogin() {
		return this.loginSubject.asObservable();
	}

	addError(message: string, error: HttpErrorResponse) {
		this.errorSubject.next(new VbsError(message, error));
	}

	onError() {
		return this.errorSubject.asObservable();
	}

	addMessage(message: string) {
		this.messageSubject.next(message);
	}

	onMessage() {
		return this.messageSubject.asObservable();
	}

	closeMessage(message: string) {
		this.closeMessageSubject.next(message);
	}

	onCloseMessage() {
		return this.closeMessageSubject.asObservable();
	}

	getSecurity(): Observable<Security>{
		if(this.securitySubject == null || this.securitySubject === undefined){
			this.securitySubject = new BehaviorSubject<Security>(Object.assign(new Security, JSON.parse(localStorage.getItem('security'))));
		}
		return this.securitySubject.asObservable();
	}

	setSecurity(security:Security){
		localStorage.setItem('security', JSON.stringify(security));
		if(this.securitySubject !== null && this.securitySubject !== undefined){
			this.securitySubject.next(security);
		}else{
			this.securitySubject = new BehaviorSubject<Security>(security);
		}
	}

	getReleases():Observable<Release[]> {
		return this.releasesSubject.asObservable();
	}

	setReleases(releases: Release[]) {
		this.releasesSubject.next(releases);
	}

	getAlerts():Observable<Alert[]> {
		return this.alertsSubject.asObservable();
	}

	setAlerts(alerts: Alert[]) {
		this.alertsSubject.next(alerts);
	}

	getReleaseOpen():Observable<boolean> {
		return this.releaseOpenSubject.asObservable();
	}

	setReleaseOpen(open: boolean) {
		this.releaseOpenSubject.next(open);
	}

	getInterest() {
		return this.interestSubject.asObservable();
	}

	setInterest(interest: SecurityInterest) {
		this.interestSubject.next(interest);
	}

	storeLatestSecuritySeach(securitySearchParameters: any, securitySearchResult: any) {
		this.latestSecuritySearchHash = Md5.hashStr(JSON.stringify(securitySearchParameters));
		this.latestSecuritySearchResult = securitySearchResult;
	}

	getLatestSecuritySearchHash(): any {
		return this.latestSecuritySearchHash;
	}

	getLatestSecuritySearchResult() : any {
		return this.latestSecuritySearchResult;
	}
}

interface String {
    format(...replacements: string[]): string;
}
