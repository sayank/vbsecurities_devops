import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class VbsHttpInterceptor implements HttpInterceptor {

	constructor(private router: Router, private ngbModal: NgbModal) { }

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		    
		return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
			if (event instanceof HttpResponse) {
				//placeholder
			}
		}, (err: any) => {
			if (err instanceof HttpErrorResponse) {
				if (err.status === 401) {
					if(this.ngbModal.hasOpenModals()){this.ngbModal.dismissAll();}
					localStorage.clear();
					sessionStorage.clear();
					this.router.navigateByUrl('/login');
				}
			}
		}));
	}

}