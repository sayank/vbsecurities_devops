import { Component, OnInit, ViewChild } from '@angular/core';
import { ReleaseService } from '../_services/release.service';
import { PendingRelease } from '../_models/pendingRelease';
import { Session } from '../_session/session';
import { ModalService } from '../_services/modal.service';
import { ReleaseSearchParameters } from '../_models/releaseSearchParameters';
import { NameDescription } from '../_models/nameDescription';

@Component({
    selector: 'app-pending-release-summary',
    templateUrl: './pending-release-summary.component.html',
    styleUrls: ['./pending-release-summary.component.css']
})
export class PendingReleaseSummaryComponent implements OnInit {

    pendingReleases: PendingRelease[];
    loading: boolean;
    sending: boolean;
    downloading: boolean;
    sent: boolean;
    availableReleaseTypes: NameDescription[] = [];
    selectedReleaseTypes: NameDescription[] = [];
    errors: any;

    constructor(private releaseService: ReleaseService, private session:Session, private modalService: ModalService) { }

    ngOnInit() {
        this.sent = false;
        this.sending = false;
        this.availableReleaseTypes = [];
		this.availableReleaseTypes.push(new NameDescription(1, 'DC', 'Demand - Collector'));
		this.availableReleaseTypes.push(new NameDescription(3, 'RE', 'Release'));
        this.availableReleaseTypes.push(new NameDescription(4, 'RP', 'Replace'));

        this.selectedReleaseTypes = Object.assign(new Array<NameDescription>(), this.availableReleaseTypes);
        
        this.reload(); 
    }

    onPage(){
		let top = document.getElementsByTagName('datatable-header')[0] as HTMLElement;
		top.scrollIntoView();
    }
    
    reload() {
        this.loading = true;
        let searchParams = new ReleaseSearchParameters();
        searchParams.releaseStatus = 'PENDING';
        searchParams.releaseTypes = this.selectedReleaseTypes;
        
        this.releaseService.searchReleases(searchParams).subscribe(
            releases => {
                this.pendingReleases = releases; 
                this.loading = false;
                
            },
            error => {
                this.session.addError('Server error on loading pending releases', error);
                this.loading = false;
            }
        )
    }

    onSelectReleaseType(id:number, isChecked: boolean) {
		let existReleaseType = this.selectedReleaseTypes.find(r => r.id == id);
		if(!existReleaseType && isChecked) {
			let releaseType = this.availableReleaseTypes.find(r => r.id == id);
			this.selectedReleaseTypes.push(releaseType);
		} else if(existReleaseType && !isChecked) {
			let existReleaseTypeIndex = this.selectedReleaseTypes.findIndex(r => r.id == id);
			this.selectedReleaseTypes.splice(existReleaseTypeIndex, 1);
		}
	}

	isCheckedReleaseType(id:number) {
		let existReleaseType = this.selectedReleaseTypes.find(r => r.id == id);
		if(existReleaseType!=null) {
			return true;
		} else {
			return false;
		}
	}

    sendToFMS() {
        this.modalService.confirm(
            {
                title: "Send releases to FMS",
                body: "Are you sure you want to send the releases to FMS?",
                size: "lg",
                allowDismiss: false})
            .subscribe(
                confirmation => {
                    if (confirmation.result) {
                        this.sending = true;
                        this.errors = null;
                        this.releaseService.sendReleaseListToFMS(this.pendingReleases.filter(pendingRelease => pendingRelease.selected)).subscribe(
                            releases => {
                                this.sending = false;
                                this.sent = true;
                                this.reload();
                            }, 
                            error => {
                                this.sending = false;
                                switch (error.status) {
                                    case 422: {
                                        this.errors = error.error.validationErrorList;
                                        break;
                                    }
                                    default: {
                                        this.session.addError('Server error on sending releases to FMS', error);
                                        break;
                                    }
                                }
                            }
                        );
                    }
                }
            );
    }

    downloadToExcel(){
        this.downloading = true;
        let releaseSearchParameters : ReleaseSearchParameters = new ReleaseSearchParameters();
        releaseSearchParameters.releaseTypes = this.selectedReleaseTypes;
        releaseSearchParameters.releaseStatus = 'PENDING';
        this.releaseService.getSearchedReleasesExcel(releaseSearchParameters).subscribe(data => {
			const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
			let d = new Date();
			const element = document.createElement('a');
			element.href = URL.createObjectURL(blob);
			element.download = "pending-releases-summary-"+d.getTime()+".xlsx";
			document.body.appendChild(element);
			element.click();
            element.remove();
            this.downloading = false;
		},
		error => { 
            this.session.addError('Server error on exporting Releases', error);
            this.downloading = false;
        });;
    }

}
