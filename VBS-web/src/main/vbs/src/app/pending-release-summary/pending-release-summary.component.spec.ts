import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingReleaseSummaryComponent } from './pending-release-summary.component';

describe('PendingReleaseSummaryComponent', () => {
  let component: PendingReleaseSummaryComponent;
  let fixture: ComponentFixture<PendingReleaseSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingReleaseSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingReleaseSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
