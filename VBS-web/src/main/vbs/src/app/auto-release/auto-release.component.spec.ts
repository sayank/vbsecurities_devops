import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoReleaseComponent } from './auto-release.component';

describe('AutoReleaseComponent', () => {
  let component: AutoReleaseComponent;
  let fixture: ComponentFixture<AutoReleaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoReleaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoReleaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
