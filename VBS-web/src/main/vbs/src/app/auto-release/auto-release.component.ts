import {Component, OnInit} from '@angular/core';
import {Session} from '../_session/session';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {AutoReleaseRunSearchResult} from '../_models/autoReleaseRunSearchResult';
import {AutoReleaseRunSearchParameters} from '../_models/autoReleaseRunSearchParameters';
import {ReleaseService} from '../_services/release.service';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
	selector: 'app-auto-release',
	templateUrl: './auto-release.component.html',
	styleUrls: ['./auto-release.component.css']
})

export class AutoReleaseComponent implements OnInit {
	form: FormGroup;
	searched: boolean = false;
	results: AutoReleaseRunSearchResult[] = [];
	searching: boolean = false;
	searchParams: AutoReleaseRunSearchParameters;
	autoReleaseRunProcessInfo:string = null;
    saving: boolean = false;
    allowStart = false;

	constructor(
		private releaseService: ReleaseService,
		public session: Session,
        private fb: FormBuilder,
        private route: ActivatedRoute
	) { }

	ngOnInit() {
        this.releaseService.existsRunInProgress().subscribe(
            runInProgress => this.allowStart = !runInProgress,
            error => this.session.addError('Server error on loading existing run status', error)
        );

		this.searchParams = new AutoReleaseRunSearchParameters();
        
        this.route.queryParams.subscribe((params: Params) => {
			if(params['saved'] !== undefined){
                let saved = params['saved'];
                let savedSearchParameters = this.session.getAutoReleaseSearchParameters();
                let savedSearchResults = this.session.getAutoReleaseSearchResults();

				if(saved == 'true' && savedSearchParameters != null && savedSearchResults != null) {
                    this.searchParams = savedSearchParameters;
                    this.results = savedSearchResults;
                    if(this.results.length > 0) {
                        this.searched = true;
                    }
                }
			}
		});

		this.form = this.fb.group(this.searchParams);
		this.form.valueChanges.subscribe(searchParams => this.searchParams = searchParams);
		this.form.controls['startDate'].setValidators(this.validateDate);
		this.form.controls['endDate'].setValidators(this.validateDate);
		this.autoReleaseRunProcessInfo = null;
	}

	validateDate(c: FormControl) {
		return (c.value == null || !Number.isNaN(Date.parse(c.value))) ? null : {
			date: {
				valid: false
			}
		};
	}

	search() {
		this.searching = true;
		this.releaseService.searchAutoReleaseResults(this.searchParams).subscribe(
			data => {
				this.results = data
				this.searched = true;
                this.searching = false;
                this.session.setAutoReleaseSearch(this.searchParams, this.results);
			},
			error => {
				this.session.addError('Error when searchinga Auto Release', error);
				this.results = [];
				this.searching = false;
				this.searched = true;
			}
		);
	}

	saveSearch() {
		this.session.setAutoReleaseSearch(this.searchParams, this.results);
		this.session.addMessage('Search parameters and results has been saved.');
	}

	startAutoReleaseProcess() {
		this.saving = true;
		this.releaseService.startAutoReleaseProcess().subscribe(
			data => {
				this.session.addMessage("The auto release run has started");
				this.saving = false;
				this.allowStart = false;
			},
			error => {
				this.session.addError('Error when running AutoRelease', error);
				this.saving = false;
			}
		);
	}

	clear() {
		this.form.reset({
			'startDate': null,
			'endDate': null,
			'dateSearchType': "EQUALS",
		});
		this.results = null;
		this.searched = false;
		this.session.setAutoReleaseSearch(null, null);
	}

	textForSearchType(t: string): string {
		if (t == 'EQUALS') {
			return " = ";
		} else if (t == 'LESS_THAN') {
			return " < ";
		} else if (t == 'EQUALS_LESS_THAN') {
			return " =< ";
		} else if (t == 'GREATER_THAN') {
			return " > ";
		} else if (t == 'EQUALS_GREATER_THAN') {
			return " => ";
		} else if (t == 'BETWEEN') {
			return "Between";
		} else if (t == 'BEGINS_WITH') {
			return 'Begins With';
		} else if (t == 'IN') {
			return ' In ';
		} else if (t == 'NOT_EQUALS') {
			return ' != ';
		} else if (t == 'CONTAINS') {
			return 'Contains';
		} else {
			return '';
		}
	}
	
	onPage(){
		let top = document.getElementsByTagName('datatable-header')[0] as HTMLElement;
		top.scrollIntoView();
	}
}
