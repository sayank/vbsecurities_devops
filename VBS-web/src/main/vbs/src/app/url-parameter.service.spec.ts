import { TestBed } from '@angular/core/testing';

import { UrlParameterService } from './url-parameter.service';

describe('UrlParameterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UrlParameterService = TestBed.get(UrlParameterService);
    expect(service).toBeTruthy();
  });
});
