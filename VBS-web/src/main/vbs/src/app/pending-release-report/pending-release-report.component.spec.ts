import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingReleaseReportComponent } from './pending-release-report.component';

describe('PendingReleaseReportComponent', () => {
  let component: PendingReleaseReportComponent;
  let fixture: ComponentFixture<PendingReleaseReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingReleaseReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingReleaseReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
