import { Component, OnInit } from '@angular/core';
import { PendingRelease } from '../_models/pendingRelease';
import { ReleaseService } from '../_services/release.service';
import { Session } from '../_session/session';
import { FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';
import { ReleaseSearchParameters } from '../_models/releaseSearchParameters'
import { NameDescription } from '../_models/nameDescription';

@Component({
	selector: 'app-pending-release-report',
	templateUrl: './pending-release-report.component.html',
	styleUrls: ['./pending-release-report.component.css']
})
export class PendingReleaseReportComponent implements OnInit {
	form: FormGroup;
	pendingReleases: PendingRelease[];
	loading: boolean;
	downloadingExcelPendingSentReleases:boolean;
	searchParams: ReleaseSearchParameters;
	availableReleaseTypes: NameDescription[] = [];
	selectedReleaseTypes: NameDescription[] = [];

	constructor(
		private releaseService: ReleaseService, private session:Session, private fb: FormBuilder
	) { }

	ngOnInit() {
		this.loading = false;
		this.downloadingExcelPendingSentReleases = false;
		this.searchParams = new ReleaseSearchParameters();

		this.form = this.fb.group(this.searchParams);
		this.form.valueChanges.subscribe(searchParams => this.searchParams = searchParams);
		this.form.controls['releaseUpdateStartDate'].setValidators(this.validateDate);
		this.form.controls['releaseUpdateEndDate'].setValidators(this.validateDate);

		this.availableReleaseTypes = [];
		this.availableReleaseTypes.push(new NameDescription(1, 'DC', 'Demand - Collector'));
		this.availableReleaseTypes.push(new NameDescription(3, 'RE', 'Release'));
		this.availableReleaseTypes.push(new NameDescription(4, 'RP', 'Replace'));
        //this.reload();
	}

	onPage(){
		let top = document.getElementsByTagName('datatable-header')[0] as HTMLElement;
		top.scrollIntoView();
    }
    
    reload() {
		this.loading = true;
		this.searchParams.releaseTypes = this.selectedReleaseTypes;
		this.releaseService.searchReleases(this.searchParams).subscribe(
			releases => {
				this.pendingReleases = releases;
				this.loading = false;
			},
			error => {
				this.session.addError('Server error on loading pending and sent releases', error);
				this.pendingReleases = [];
				this.loading = false;
			}
		);
	}

	clear() {
		this.form.reset({
			'releaseUpdateStartDate': null,
			'releaseUpdateEndDate': null,
			'dateSearchType': "EQUALS",
			'releaseStatus': null
		});
		this.selectedReleaseTypes = [];
		this.pendingReleases = null;
		this.loading = false;
	}

	onSelectReleaseType(id:number, isChecked: boolean) {
		let existReleaseType = this.selectedReleaseTypes.find(r => r.id == id);
		if(!existReleaseType && isChecked) {
			let releaseType = this.availableReleaseTypes.find(r => r.id == id);
			this.selectedReleaseTypes.push(releaseType);
		} else if(existReleaseType && !isChecked) {
			let existReleaseTypeIndex = this.selectedReleaseTypes.findIndex(r => r.id == id);
			this.selectedReleaseTypes.splice(existReleaseTypeIndex, 1);
		}
	}

	isCheckedReleaseType(id:number) {
		let existReleaseType = this.selectedReleaseTypes.find(r => r.id == id);
		if(existReleaseType!=null) {
			return true;
		} else {
			return false;
		}
	}
	
	exportToExcelPendingSentReleases(event) {
		this.downloadingExcelPendingSentReleases = true;
		this.searchParams.releaseTypes = this.selectedReleaseTypes;
		this.releaseService.getSearchedReleasesExcel(this.searchParams).subscribe(data => {
			const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
			let d = new Date();
			const element = document.createElement('a');
			element.href = URL.createObjectURL(blob);
			element.download = "pending-sent-releases-"+d.getTime()+".xlsx";
			document.body.appendChild(element);
			element.click();
			element.remove();
		},
		error => {},
		() => {this.downloadingExcelPendingSentReleases = false;});
	}

	validateDate(c: FormControl) {
		return (c.value == null || !Number.isNaN(Date.parse(c.value))) ? null : {
			date: {
				valid: false
			}
		};
	}

	textForSearchType(t: string): string {
		if (t == 'EQUALS') {
			return " = ";
		} else if (t == 'LESS_THAN') {
			return " < ";
		} else if (t == 'EQUALS_LESS_THAN') {
			return " =< ";
		} else if (t == 'GREATER_THAN') {
			return " > ";
		} else if (t == 'EQUALS_GREATER_THAN') {
			return " => ";
		} else if (t == 'BETWEEN') {
			return "Between";
		} else if (t == 'BEGINS_WITH') {
			return 'Begins With';
		} else if (t == 'IN') {
			return ' In ';
		} else if (t == 'NOT_EQUALS') {
			return ' != ';
		} else if (t == 'CONTAINS') {
			return 'Contains';
		} else {
			return '';
		}
	}

}
