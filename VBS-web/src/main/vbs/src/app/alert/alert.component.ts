import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from "@angular/forms";
import {AlertType} from "../_models/alertType";
import {Alert} from "../_models/alert";
import {Session} from "../_session/session";
import {SecurityService} from "../_services/security.service";

@Component({
    selector: 'app-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnChanges{

    @Input('form') form: FormGroup;
    @Output() alertOpen = new EventEmitter<boolean>();

    index:number = null;
    allAlertTypes:AlertType[] = [];
    editForm:FormGroup = null;

    constructor(private session:Session, private securityService: SecurityService, private fb: FormBuilder, private cdRef: ChangeDetectorRef) {
        this.allAlertTypes = this.session.getProperty("alertTypes");
        this.allAlertTypes.splice(4,1);
    }

    ngOnChanges(){
        this.form.updateValueAndValidity();
        this.cdRef.detectChanges();
    }
    showAddButtons(): boolean {
        return !this.session.getUser().hasNoPermissions();
    }
    get alertTypes():AlertType[] {
        return this.allAlertTypes.filter(t => (this.editForm.get('id').value == null) || ((this.editForm.get('alertTypeId').value == 4) == (t.id == 4)));
    }

    loadAlert(rowIndex:number): void {
        let alert:Alert = this.form.get('alerts.'+rowIndex).value;
        this.index = rowIndex;
        this.editForm = this.fb.group(alert);
        this.alertOpen.emit(true);
    }

    newAlert() {
        this.editForm = this.fb.group(new Alert());
        this.alertOpen.emit(true);
    }

    deleteAlert(rowIndex:number): void {
        (this.form.get('alerts') as FormArray).removeAt(rowIndex);
    }

    save(): void{
        this.securityService.validateAlert(this.editForm.getRawValue()).subscribe(
            ok => {
                if(this.editForm.get('id').value == null){
                    (this.form.get('alerts') as FormArray).push(this.fb.group(this.editForm.getRawValue()));
                }else{
                    this.form.get('alerts.'+this.index).patchValue(this.editForm.getRawValue());
                }
                this.editForm = null;
                this.alertOpen.emit(false);
            }, errorResult => {
                let arr = {};
                let errorList = errorResult.error.validationErrorList;
                for(let i=0; i<errorList.length; i++) {
                    if (arr[errorList[i].errorFieldName] === undefined) {
                        arr[errorList[i].errorFieldName] = [];
                    }
                    arr[errorList[i].errorFieldName].push(errorList[i].errorDescription);
                }
                for (let field in arr) {
                    if (field != null && this.editForm.get(field) != null) {
                        this.editForm.get(field).setErrors({'serverErrors': arr[field]});
                    }
                }
            }
        );
    }

    cancel(): void {
        this.editForm = null;
        this.alertOpen.emit(false);
    }

    getNameForAlertType(alertTypeId: number): string {
        if (this.allAlertTypes.length == 0) {
            return '';
        }
        let iconName = "";
        let alertTypeName = this.allAlertTypes.find(t => t.id == alertTypeId).name.toLowerCase();
        if (alertTypeName == "note") {
            iconName = "t-icon t-icon-sticky-note"
        } else if (alertTypeName != "note") {
            iconName = "oi oi-flag " + alertTypeName;
        }
        return iconName;
    }

    getDescriptionForAlertType(alertTypeId: number): string {
        if(this.allAlertTypes.length == 0){return '';}
        return this.allAlertTypes.find(t => t.id == alertTypeId).description;
    }

    getAlertDescription(description: string) {
        return description!=null ? description.replace(/\n/g, '<br/>') : '';
    }
}
