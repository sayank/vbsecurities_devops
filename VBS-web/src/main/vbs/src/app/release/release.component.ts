import {
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    TemplateRef,
    ViewChild
} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {Session} from "../_session/session";
import {Release} from "../_models/release";
import {HashUtil} from "../_util/hashUtil";
import {ActivatedRoute, Params} from "@angular/router";
import {ReleaseService} from "../_services/release.service";
import {UrlParameterService} from "../url-parameter.service";
import {NameDescription} from "../_models/nameDescription";
import {ReleaseEnrolment} from "../_models/releaseEnrolment";
import {EnrolmentsSearchComponent} from "../_components/enrolments-search/enrolments-search.component";
import {NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {User} from "../_models/user";
import {ContactSearchComponent} from '../_components/contactSearch/contact-search.component';
import {ReleaseReasonComponent} from '../_components/release-reason/release-reason.component';
import {Contact} from '../_models/contact';
import {ContactsService} from '../_services/contacts.service';
import {BehaviorSubject, Observable, of, Subscription} from "rxjs";
import {catchError, debounceTime, distinctUntilChanged, switchMap} from "rxjs/operators";
import {SecurityService} from "../_services/security.service";
import {InstrumentNumberSearchComponent} from "../_components/instrument-number-search/instrument-number-search.component";
import {ReplacedBy} from "../_models/replacedBy";
import {AutoRelease} from '../_models/autoRelease';
import {UtilService} from "../_services/util.service";
import {ModalService} from "../_services/modal.service";
import {Enrolment} from '../_models/enrolment';
import {DatatableComponent} from "@swimlane/ngx-datatable";

@Component({
    selector: 'app-release',
    templateUrl: './release.component.html',
    styleUrls: ['./release.component.css']
})
export class ReleaseComponent implements OnInit, OnChanges {

    @Input("securityTypeId") securityTypeId: number;
    @Input("currentAmount") currentAmount: number;
    @Input("poolId") poolId: number;
    @Input("thirdPartyContact") securityThirdParty: Contact;
    @Input("canDeactivate") canDeactivate: any;
    @Input("validateSaveResult") validateSaveResult: BehaviorSubject<any>;
    @Input("securityStatusId") securityStatusId: number;
    @Input("availableAmount") availableAmount: number;
    @Input("homesCoveredEnrolments") homesCoveredEnrolments: Enrolment[];
    @Input("securityPurposeId") securityPurposeId: number;
    @Input("tabChange") activeTab: string;
    @Input("notAllowRelease") notAllowRelease: boolean;

    @Output() triggerSave = new EventEmitter();
    @Output() triggerReload = new EventEmitter();

    validateSaveSub:Subscription;

    releases: Release[] = null;
    autoReleases: AutoRelease[] = null;
    form: FormGroup = null;
    saving: boolean = false;
    creatingRelease: boolean = false;
    _securityId: number = null;

    hash: string = '';
    crmCompanyUrl: string = '';
    rejectReasonList: NameDescription[];
    managers: User[];
	sendManagementApprovalModalRef: NgbModalRef;
    addThirdPartyToReleaseModalRef: NgbModalRef;
    cancelReleaseModalRef: NgbModalRef;
    latestFinalApprovalSentTo: string = null;
    releaseStatus: string = null;
    downloadingExcelAutoReleases: boolean = false;
    downloadingExcelReleases: boolean = false;
    previousAnalystApprovalStatus: String = null;

    @ViewChild("sendManagementApprovalModal", {read: TemplateRef}) sendManagementApprovalModal: TemplateRef<any>;
    @ViewChild("addThirdPartyToReleaseModal", {read: TemplateRef}) addThirdPartyToReleaseModal: TemplateRef<any>;
    @ViewChild("cancelReleaseModal", {read: TemplateRef}) cancelReleaseModal: TemplateRef<any>;
    @ViewChild("openChequeInfoModal", {read: TemplateRef}) openChequeInfoModal: TemplateRef<any>;
    @ViewChild("notAllowedModal", {read: TemplateRef}) notAllowedModal: TemplateRef<any>;
    @ViewChild('dataTable') dataTable: DatatableComponent;
    @ViewChild('enrolmentsTable') enrolmentsTable: DatatableComponent;
    @ViewChild('autoReleaseTable') autoReleaseTable: DatatableComponent;

    constructor(private session: Session, private releaseService: ReleaseService, private contactsService: ContactsService,
                private fb: FormBuilder, private route: ActivatedRoute, private urlParameterService: UrlParameterService,
                private securityService: SecurityService, private ngbModal: NgbModal, private currency: UtilService,
                private modalService:ModalService, private cdRef: ChangeDetectorRef) {

    }

    @Input() set securityId(value: number) {
        if(value !== undefined && value != null){
            this._securityId = value;
            this.loadReleases();
            this.loadAutoReleases();
        }
    }

    ngOnChanges(){
        this.cdRef.detectChanges();
        if(this.activeTab == 'tab-release' && this.dataTable != undefined) {
            setTimeout(function(d){
                d.recalculate();
                d['cd'].markForCheck();
            }, 10, this.dataTable);
        }
        if(this.activeTab == 'tab-release' && this.enrolmentsTable != undefined) {
            setTimeout(function(d){
                d.recalculate();
                d['cd'].markForCheck();
            }, 10, this.enrolmentsTable);
        }
        if(this.activeTab == 'tab-release' && this.autoReleaseTable != undefined) {
            setTimeout(function(d){
                d.recalculate();
                d['cd'].markForCheck();
            }, 10, this.autoReleaseTable);
        }
    }

    ngOnInit(): void {
        this.crmCompanyUrl = this.session.getProperty("crmCompanyUrl");
        this.rejectReasonList = this.session.getProperty("releaseRejectReasons");
        console.log(this.rejectReasonList);
        this.managers = this.session.getProperty("managersList");
//        if(this._securityId != null){
//            this.securityService.getThirdPartyContact(this._securityId).subscribe(
//                thirdParty => {
//                    this.securityThirdParty = thirdParty;
//                },
//                error => this.session.addError('Server error on loading thirdParty', error));
//            }
        }

    loadReleases() {
        this.releaseService.getReleases(this._securityId).subscribe(
            releases => {
                this.releases = releases;
                this.session.setReleases(this.releases);
                this.route.queryParams.subscribe((params: Params) => {
                    if (params['referenceNumber'] !== undefined) {
                        let refNo = params['referenceNumber'];
                        let release = this.releases.find(r => r.referenceNumber == refNo);
                        if (release != null) {
                            this.loadRelease(release);
                        }

                    }
                });
            },
            error => this.session.addError('Server error on loading Releases', error));
    }

    loadAutoReleases() {
        this.releaseService.getAutoReleaseForSecurityId(this._securityId).subscribe(
            autoReleases => {
                this.autoReleases = autoReleases;
            },
            error => this.session.addError('Server error on loading Auto Releases', error));
    }

    loadRelease(release: Release) {
        this.hash = HashUtil.getHash(release);
        let form = this.fb.group(release);
        this.latestFinalApprovalSentTo = release.finalApprovalSentTo;
        this.previousAnalystApprovalStatus = release.analystApprovalStatus;
        this.releaseStatus = release.status;
        form.setControl('enrolments', this.fb.array((release.enrolments.length > 0) ? this.createGroups(release.enrolments) : [this.fb.group({})]));
        form.setControl('reasons', this.fb.array((release.reasons.length > 0) ? this.createGroups(release.reasons) : []));
		if(release.id != null){
        	form.addControl('linkEnrolments', new FormControl(release.enrolments.length > 0));
		}else{
			form.addControl('linkEnrolments', new FormControl(true));
		}
		this.form = form;
        this.disableFields();

        this.form.get('authorizedReleaseAmount').valueChanges.subscribe(v => {
            if (v != null && v >= 100000) {
                this.form.get('finalApprovalSentTo').patchValue(null);
            }
        });

        this.form.get('linkEnrolments').valueChanges.subscribe(v => {
            if (v) {
                this.calculateEnrolmentsTotals();
                this.form.get('analystApprovalStatus').patchValue(null, {emitEvent: false});
            } else {
                this.deleteAllEnrolments();
            }
        });

        this.form.get('regularRelease').valueChanges.subscribe(v => {
            if (this.showRegularRelease && !this.form.get('regularRelease').disabled) {
                this.form.get('regularRelease').disable({emitEvent: false});
                if (!v && this.form.get('enrolments').value.length > 0) {
                    this.form.get('linkEnrolments').setValue(null, {emitEvent: false});
                    this.form.setControl('enrolments', this.fb.array([]));
                }
            }
        });

        //revert back
        //this.form.get('analystApprovalStatus').valueChanges.subscribe(v => {
        //    this.form.get('analystApprovalStatus').markAsTouched();
        //    this.addThirdPartyFromSecurity();
        //});

        this.form.valueChanges.subscribe(release => {
            this.analystApprovalChange();
            this.amountChange();
        });

        if (this.form.get('wsInputRequested').value) {
            this.form.get('wsInputRequested').disable();
        }

        this.urlParameterService.setUrlParameter('referenceNumber', release.referenceNumber);
    }
	loadResultedInRelease(autoRelease: AutoRelease){
		let release = this.releases.filter(r => r.referenceNumber == this._securityId + '-' + autoRelease.releaseSeqNum);
		if(release.length > 0){
			this.loadRelease(release[0]);
		}
	}
    disableFields() {
        if (this.isFinalized()) {
            for (let c in this.form.controls) {
                this.form.get(c).disable();
            }
            this.form.get('comment').enable();
        }
    }

    createGroups(list: any[]): FormGroup[] {
        let groups: FormGroup[] = [];
        list.forEach(e => {
            let g = this.fb.group(e);
            if ((e as ReleaseEnrolment).enrolment !== undefined) {
                this.registerEnrolmentGroupListener(g);
            }
            groups.push(g);
        });
        return groups;
    }

    get getSelectedManager() {
        let finalApprovalSentTo = this.form.get('finalApprovalSentTo').value;
        let selectedManager = this.managers.find(m => m.userId == finalApprovalSentTo);
        if(selectedManager!=null) {
            return selectedManager.firstName + ' ' + selectedManager.lastName;
        } else {
            return '';
        }
    }

    getThridPartyContact() {
        if(this.securityThirdParty !=null && this.securityThirdParty.companyName != null) {
            return "Contact Id: " + this.securityThirdParty.crmContactId + ' Name: ' + this.securityThirdParty.companyName;
        } else if(this.securityThirdParty !=null) {
            return "Contact Id: " + this.securityThirdParty.crmContactId + ' Name: ' + this.securityThirdParty.firstName + '' + this.securityThirdParty.lastName;
        }
        return '';
    }


    save() {
        let release = this.form.getRawValue() as Release;

        if(release.finalApprovalSentTo != null && release.finalApprovalSentTo != this.latestFinalApprovalSentTo) {
            let selectedManager = this.managers.find(m => m.userId == release.finalApprovalSentTo);
            if(selectedManager!=null) {
                this.sendManagementApprovalModalRef = this.ngbModal.open(this.sendManagementApprovalModal);
				this.sendManagementApprovalModalRef.result.then(
					(result) => {
                        this.validateAndSave();
					},
					(reason) => {

					}
				);

            } else {
                this.validateAndSave();
            }
        } else {
            this.validateAndSave();
        }
    }

    validateAndSave() {
        this.saving = true;

        let release = this.form.getRawValue() as Release;
        release.securityId = this._securityId;

        if (release.releaseType.id != 3) {
            let replacedBy: any = release.replacedBy;
            if (replacedBy != null && replacedBy.id !== undefined) {
                release.replacedBy.securityId = replacedBy.id;
                release.replacedBy.instrumentNumber = replacedBy.instrumentNumber;
            } else if (replacedBy != null && replacedBy.securityNumber !== undefined) {
                release.replacedBy.securityId = replacedBy.securityNumber;
                release.replacedBy.instrumentNumber = replacedBy.instrumentNumber;
            } else if (replacedBy != null && replacedBy.securityNumber === undefined && replacedBy.id === undefined && replacedBy.securityId === undefined) {
                release.replacedBy = new ReplacedBy();
                release.replacedBy.instrumentNumber = replacedBy;
                this.form.controls['replacedBy'].patchValue(release.replacedBy, {emitEvent: false});
            }
        }

        if(release.payTo != null && release.payTo.crmContactId == null){
            release.payTo = null;
        }

        if (release.releaseType.id == 1){
            release.finalApprovalStatus = release.analystApprovalStatus;
            release.finalApprovedBy = release.analystApprovedBy;
            release.finalApprovalDate = release.analystApprovalDateTime;
        }

        if (!this.form.get('linkEnrolments').value || (release.enrolments.length == 1 && release.enrolments[0].id === undefined)) {
            release.enrolments = [];
        }

        this.releaseService.validateRelease(release).pipe(catchError(error => of(error))).subscribe(result => {
            if (result != null) {
                switch (result.status) {
                    case 422: //failed validation
                        let arr = {};
                        for (let err in result.error) {
                            if (result.error[err].errorType === undefined || result.error[err].errorType == 'VALIDATION_FAIL') {
                                if (arr[result.error[err].errorFieldName] === undefined) {
                                    arr[result.error[err].errorFieldName] = [];
                                }
                                arr[result.error[err].errorFieldName].push(result.error[err].errorDescription);
                            } else if (result.error[err].errorType == 'ALERT') {
                                if (result.error[err].errorCode = 'RELEASE_DATE_LATER_THAN_WSD') {
                                    this.modalService.confirm({
                                        title: result.error[err].errorTitle,
                                        body: result.error[err].errorMessage,
                                        size: "lg",
                                        allowDismiss: false,
                                        alertOnly: false,
                                        acceptButtonText: <any>'Confirm',
                                        rejectButtonText: <any>'Return'})
                                    .subscribe(({closed, dismissed, result}) => {
                                        if (result) {
                                            this.saveRelease(release);
                                        }
                                    });
                                }
                                // this.alertConfirmsShown.push(errorList[i].errorReturnCode);
                            }
                        }
                        for (let field in arr) {
                            if (field != null && this.form.get(field) != null){
                                this.form.get(field).setErrors({'serverErrors': arr[field]});
                            }
                        }
                        break;
                    default:
                        this.session.addError('Server error on saving release', result);
                }
                this.saving = false;
            } else {
                this.saveRelease(release);
            }
        });
    }

    private saveRelease(release: Release): void {
        this.releaseService.saveRelease(release).subscribe(
            savedRelease => {
                if(release.id != null) {
                    let index = this.releases.findIndex(r => r.id == savedRelease.id);
                    this.releases[index] = savedRelease;
                }else{
                    if (this.releases == null) {
                        this.releases = [];
                    }
                    this.releases.unshift(savedRelease);
                }
                this.session.setReleases(this.releases);
                this.saving = false;
                this.cancel();
                this.triggerReload.next("reload");
            },
            error => {
                this.saving = false;
                this.session.addError('Server error on saving release', error);
            }
        );
    }

    addEnrolment() {
        let modal = this.ngbModal.open(EnrolmentsSearchComponent, {size: 'lg'});
        let searchComponent: EnrolmentsSearchComponent = <EnrolmentsSearchComponent>modal.componentInstance;
        let enrolments = [];
        if (this.enrolmentsNotEmpty) {
            enrolments = this.form.get('enrolments').value;
        }
        if(this.securityPurposeId == 2) {
            searchComponent.setOptions(this.poolId, false, null, enrolments, null);
        } else {
            searchComponent.setOptions(this.poolId, false, null, enrolments, this.homesCoveredEnrolments);
        }
        searchComponent.selected.subscribe(enrolment => {
            let fa = this.form.controls['enrolments'] as FormArray;
            if (!this.enrolmentsNotEmpty) {
                fa.controls.splice(0, 1);
            }
            let releaseEnrolment = new ReleaseEnrolment();
            releaseEnrolment.enrolment = enrolment;
            let control = this.fb.group(releaseEnrolment);
            this.registerEnrolmentGroupListener(control);
            fa.push(control);
        });
    }

    registerEnrolmentGroupListener(g: FormGroup) {
        g.get('analystApprovalStatus').valueChanges.subscribe(v => {
            if (v == 'Rejected') {
                let fa = this.form.get('enrolments') as FormArray;
                for (let i = 0; i < fa.controls.length; i++) {
                    let g = fa.controls[i] as FormGroup;
                    g.get('analystApprovalStatus').patchValue('Rejected', {emitEvent: false});
                    g.get('authorizedAmount').patchValue(0, {emitEvent: false});
                }
            }
        });
        g.get('requestAmount').valueChanges.subscribe(v => this.calculateEnrolmentsTotals());
        g.get('authorizedAmount').valueChanges.subscribe(v => this.calculateEnrolmentsTotals());
    }
	openChequeInformation(){
		this.ngbModal.open(this.openChequeInfoModal);
	}
    calculateEnrolmentsTotals(): void {
        let totalRequestAmount = null;
        let totalAuthorizedAmount = null;
		if (this.enrolmentsNotEmpty) {
            let fa = this.form.controls['enrolments'] as FormArray;
            for (let i = 0; i < fa.controls.length; i++) {
                if (totalRequestAmount == null) {
                    totalRequestAmount = 0;
                    totalAuthorizedAmount = 0;
                }
                let g = fa.controls[i] as FormGroup;
                totalRequestAmount = +g.get('requestAmount').value + +totalRequestAmount;
                totalAuthorizedAmount = +g.get('authorizedAmount').value + +totalAuthorizedAmount;
            }
        }
        this.form.controls['requestedAmount'].patchValue(totalRequestAmount);
        this.form.controls['authorizedReleaseAmount'].patchValue(totalAuthorizedAmount);
    }

    deleteAllEnrolments() {
        this.form.setControl('enrolments', this.fb.array([this.fb.group({})]));
        this.calculateEnrolmentsTotals();
    }

    deleteEnrolment(enrolment: ReleaseEnrolment) {
        let fa = this.form.controls['enrolments'] as FormArray;
        let removeIndex;
        for (let i = 0; i < fa.controls.length; i++) {
            let g = fa.controls[i] as FormGroup;
            if (g.value.enrolment.enrolmentNumber == enrolment.enrolment.enrolmentNumber) {
                removeIndex = i;
                break;
            }
        }
        if (fa.controls.length == 1) {
            fa.controls.push(this.fb.group({}));
        }
        fa.controls.splice(removeIndex, 1);
        fa.updateValueAndValidity();
        this.calculateEnrolmentsTotals();
    }

    analystApprovalChange() {
        if (this.form.get('regularRelease').value && this.form.get('releaseType').value.id == 3 && this.form.get('linkEnrolments') && this.enrolmentsNotEmpty) {
            let status = {'Authorized': [], null: [], 'Rejected': []};
            let fa = this.form.get('enrolments') as FormArray;
            for (let i = 0; i < fa.controls.length; i++) {
                status[(fa.controls[i] as FormGroup).controls['analystApprovalStatus'].value].push(i);
            }
            if (status.Authorized.length == fa.controls.length) {
                this.form.get('analystApprovalStatus').patchValue('Authorized', {emitEvent: false});
            } else if (status.Rejected.length == fa.controls.length) {
                this.form.get('analystApprovalStatus').patchValue('Rejected', {emitEvent: false});
            } else {
                this.form.get('analystApprovalStatus').patchValue(null, {emitEvent: false});
            }
        }

        if (this.form.get('analystApprovalStatus').value == 'Rejected') {
            this.form.controls['finalApprovalSentTo'].patchValue(null, {emitEvent: false});
            this.form.controls['authorizedReleaseAmount'].patchValue(0, {emitEvent: false});
		}
        if (this.form.get('analystApprovalStatus').value == null) {
            this.form.controls['finalApprovalSentTo'].patchValue(null, {emitEvent: false});
        } else if (this.form.get('analystApprovalStatus').value == 'Authorized' && this.form.get('finalApprovalSentTo').value == null) {
            if ((this.securityTypeId == 3 && this.form.get('authorizedReleaseAmount').value == this.currentAmount) || (this.securityTypeId != 1 && this.form.get('authorizedReleaseAmount').value >= 5000000)) {
				this.form.controls['finalApprovalSentTo'].patchValue(this.session.getProperty("vpManagerDefault"), {emitEvent: false});
            } else if ((this.securityTypeId == 1 && this.form.get('authorizedReleaseAmount').value >= 100000) ||
                (this.form.get('finalApprovalSentTo').value == null && this.securityTypeId == 1 && this.form.get('authorizedReleaseAmount').value >= 100000)) {
                this.form.controls['finalApprovalSentTo'].patchValue(this.session.getProperty("vpManagerDefault"), {emitEvent: false});
            } else {
                this.form.controls['finalApprovalSentTo'].patchValue(this.session.getProperty("finalApprovalManagerDefault"), {emitEvent: false});
            }
        }

    }

    amountChange() {
        if (this.form.get('releaseType').value.id == 1) {
            this.form.get('authorizedReleaseAmount').patchValue(this.form.get('requestedAmount').value, {emitEvent: false});
        }
    }

    isFinalized(): boolean {
        return this.form.get('status').value == 'REJECTED' || this.form.get('finalApprovalStatus').value == 'Authorized' || this.form.get('finalApprovalStatus').value == 'Rejected' || (this.securityStatusId != null && this.securityStatusId == 7)	;
    }

    initiateRelease(releaseTypeId: number) {
        if (!this.notAllowRelease)  {
            if(!this.canDeactivate) {
                this.modalService.confirm({
                    title: "Unsaved Changes",
                    body: "There are unsaved changes on the security. <br>Do you want to save the changes and initiate release?",
                    size: "lg",
                    allowDismiss: false
                }).subscribe(result => {
                    if (result.result) {
                        this.validateSaveSub = this.validateSaveResult.subscribe(res => {
                            if (res == 'pass' || res == 'fail') {
                                if (res == 'pass') {
                                    this.setupRelease(releaseTypeId);
                                } else {
                                    this.creatingRelease = false;
                                }
                                this.validateSaveSub.unsubscribe();
                            }
                        });
                        this.creatingRelease = true;
                        this.triggerSave.next(true);
                    }
                });
            }else {
                this.setupRelease(releaseTypeId);
            }
        } else {
            this.ngbModal.open(this.notAllowedModal);
        }
    }

    setupRelease(releaseTypeId: number) {
        this.creatingRelease = true;
        this.releaseService.setupRelease(releaseTypeId, this._securityId).subscribe(release => {
            this.loadRelease(release);
            this.creatingRelease = false;
        });
    }

    cancel() {
        this.form = null;
        this.urlParameterService.deleteUrlParameter('referenceNumber');
    }

    hasUnsavedChanges() {
        if (this.form == null) {
            return false;
        }
        if (this.form.get('id').value == null) {
            return true;
        }
        let hash = HashUtil.getHash(this.form.value);
        return hash != this.hash;
    }

    compareDate(date1: Date, date2: Date): number {
        let d1 = new Date(date1);
        let d2 = new Date(date2);
        if (d1.getTime() < d2.getTime()) {
            return -1;
        } else if (d1.getTime() > d2.getTime()) {
            return 1;
        } else {
            return 0;
        }
    }

    get enrolmentsNotEmpty(): boolean {
        let fa = this.form.get('enrolments') as FormArray;
        if (fa.controls.length > 1) {
            return true;
        } else if (fa.controls.length == 1) {
            let g = fa.controls[0] as FormGroup;
			if (g.controls['id'] !== undefined) {

                return true;
            }
        }
        return false;
    }

    get showManagerSendApprovalTo(): boolean {
		return (this.form.get('analystApprovalStatus').value == 'Authorized' && !this.session.getUser().hasPermission("CREATE_DEMAND_FINANCE"));

    }

    get showManagerApproval(): boolean {
        return (this.form.get('finalApprovedBy').value == null && this.form.get('analystApprovalStatus').value == 'Authorized' && this.session.getUser().hasPermission("APPROVE_RELEASE_FINAL"));
    }

    get getSaveButtonText(): string {
        let id = this.form.get('id').value;
        let releaseTypeId = this.form.get('releaseType').value.id;

        if (this.saving) {
            return (id != null) ? 'Updating' : 'Saving';
        } else {
            if (releaseTypeId == 1 || releaseTypeId == 2) {
                return (id != null) ? 'Update Demand' : 'Add Demand';
            } else if (releaseTypeId == 3) {
                return (id != null) ? 'Update Release' : 'Complete Release';
            } else if (releaseTypeId == 4) {
                return (id != null) ? 'Update Replace' : 'Add Replace';
            }
        }
        return "";
    }

    get getCancelButtonText(): string {
        let releaseTypeId = this.form.get('releaseType').value.id;

        if (this.saving) {
            return 'Cancelling';
        } else {
            if (releaseTypeId == 1 || releaseTypeId == 2) {
                return 'Cancel Security Demand';
            } else if (releaseTypeId == 3) {
                return 'Cancel Security Release';
            } else if (releaseTypeId == 4) {
                return 'Cancel Security Replace';
            }
        }
        return "";
    }

    get showRegularRelease() {
        return (this.securityTypeId == 1 && this.form.get('releaseType').value.id == 3);
    }

    get showAnalystApprovalStatus(): boolean {
        let showApprovalStatus = false;
        if (this.form.get('releaseType').value.id == 1 || this.form.get('releaseType').value.id == 2 || this.form.get('releaseType').value.id == 4) {
            showApprovalStatus = true;
        } else {
            showApprovalStatus = !this.form.get('linkEnrolments').value;
        }
        return showApprovalStatus;
    }

    get showReplacement(): boolean {
        return (this.form.get('releaseType').value.id == 2 || this.form.get('releaseType').value.id == 4);
    }

    get showWarrantyServicesInput(): boolean {
        return !(this.form.get('releaseType').value.id != 3 || !this.enrolmentsNotEmpty || (this.form.get('enrolments') as FormArray).controls.length > 1 && this._securityId != 5);
    }

    get canInitiateRelease(): boolean {
        let canInitiateRelease:boolean = false;
        if(this.session.getUser().hasPermission("FINANCE_PERMISSION")) {
            canInitiateRelease = false;
        }

        else if(this.hasActiveDemand()){
            canInitiateRelease = false;
        }
        else if(!this.creatingRelease && this.securityTypeId != 3 && this.availableAmount != null && this.availableAmount > 0 && this.session.getUser().hasPermission("CREATE_SECURITY")) {
			canInitiateRelease = true;
		}
		else if(this.securityTypeId == 3 && (this.securityStatusId == 2 || this.securityStatusId == 4 || this.securityStatusId == 8 || this.securityStatusId == 9)){
			canInitiateRelease = true;
		}

		else {
            canInitiateRelease = false;
        }

        return !canInitiateRelease;
	}
    hasActiveDemand():boolean{
        let hasActiveDemand = false;
        this.releases.forEach(release => {
            if(release.releaseType.id == 1 && release.status == 'PENDING') {
                hasActiveDemand =  true;
            }
        });
        return hasActiveDemand;
    }

    get canInitiateDemandCollectorRelease(): boolean {
        let canInitiateDemandCollectorRelease:boolean = true;
        let existingReleases = this.releases.filter(r => r.releaseType.id == 1);

        if(existingReleases) { //&& isArray(existingReleases
            existingReleases.forEach(release => {
                if(release.analystApprovalStatus != "Rejected" && (release.finalApprovedBy == null || release.finalApprovedBy == undefined || release.finalApprovedBy == '')) {
                    canInitiateDemandCollectorRelease = false;
                }
            });
        }

        if(this.creatingRelease || this.currentAmount == null || this.currentAmount <= 0) {
            canInitiateDemandCollectorRelease = false;
        }

        return canInitiateDemandCollectorRelease;
    }

    get showInterestAmountColumn(): boolean {
        return (this.securityTypeId == 1 && this.session.getUser().hasPermission("FINANCE_PERMISSION"));
    }

    showContactSearchModal() {
        let modalRef = this.ngbModal.open(ContactSearchComponent, {size: 'lg'});
        let p = this.form.get('payTo');
        if (p.value != null && p.value.firstName == null && p.value.companyName == null && p.value.crmContactId != null) {
            modalRef.componentInstance.searchTerm = p.value.crmContactId;
        }
        modalRef.componentInstance.selected.subscribe(s => {
			this.form.controls['payTo'].setValue(s);
        });
    }

    showInstrumentSearchModal() {
        if (this.session.getUser().hasPermission("CREATE_DEMAND")) {
            let modalRef = this.ngbModal.open(InstrumentNumberSearchComponent, { size: 'lg' });
            modalRef.componentInstance.selected.subscribe(s => {
                (<FormGroup>this.form.controls['replacedBy']).patchValue(s, {emitEvent: false});
            });
        }
    }

    showReleaseReasonsModal() {
        let modalRef = this.ngbModal.open(ReleaseReasonComponent, {size: 'lg'});
        modalRef.componentInstance.existingReasons = this.form.get('reasons').value;
        modalRef.componentInstance.selected.subscribe(selectedReasons => {
            let fa = this.form.get('reasons') as FormArray;
            selectedReasons.forEach(r => {
                fa.controls.push(r);
            });
            this.form.setControl('reasons', fa);
        });
    }

    deleteReason(reason: NameDescription) {
        let fa = this.form.controls['reasons'] as FormArray;
        let removeIndex;
        for (let i = 0; i < fa.controls.length; i++) {
            let g = fa.controls[i] as FormGroup;
            if (g.value.id == reason.id) {
                removeIndex = i;
                break;
            }
        }
        fa.controls.splice(removeIndex, 1);
        fa.updateValueAndValidity();
    }

    loadThirdPartyContact() {
        console.log("1111");
        let c = <Contact>this.form.controls['payTo'].value;
        if (c != null && c.id == null && c.crmContactId != null) {
            if (!isNaN(Number.parseInt(c.crmContactId.replace("B", "")))) {
                this.contactsService.getContactByCrmContactId(c.crmContactId).subscribe(contact => {
					this.form.controls['payTo'].patchValue(contact);
                });
            }
        }
    }

    getSecurityNumber(x) {
		if(x !== undefined && x != null){
			if (x.id !== undefined) {
				return (x.id != null) ? x.id : '';
			}
			if (x.securityNumber !== undefined) {
				return (x.securityNumber != null) ? x.securityNumber : '';
			}
			if (x.securityId !== undefined) {
				return (x.securityId != null) ? x.securityId : '';
			}
			return x;
		}
    }

    securityInstrumentNumberFormatter = (x: { instrumentNumber: string }) => {
        return x.instrumentNumber;
    };

    instrumentNumberTypeahead = (text$: Observable<string>) =>
        text$.pipe(
            debounceTime(300),
            distinctUntilChanged(),
            switchMap(term => term.length <= 2 ? [] : this.securityService.allSecuritiesByInstrumentNumbersLike(term)),
            catchError(() => {
                return of([]);
            })
        );

    onSort(sorts: any) {
        let sort = sorts.sorts[0];
        let fa = this.form.controls['enrolments'] as FormArray;
        fa.controls.sort((a: FormGroup, b: FormGroup) => {
            let result = 0;
            let valA;
            let valB;
            if (sort.prop == 'enrolment' || sort.prop == 'requestAmount' || sort.prop == 'authorizedAmount') {
                if (sort.prop == 'enrolment') {
                    valA = Number.parseInt(a.value.enrolment.enrolmentNumber.replace('H', ''));
                    valB = Number.parseInt(b.value.enrolment.enrolmentNumber.replace('H', ''));
                } else if (sort.prop == 'requestAmount') {
                    valA = Number.parseInt(a.value.requestAmount);
                    valB = Number.parseInt(b.value.requestAmount);
                } else if (sort.prop == 'authorizedAmount') {
                    valA = Number.parseInt(a.value.authorizedAmount);
                    valB = Number.parseInt(b.value.authorizedAmount);
                }
                if (Number.isNaN(valA)) {
                    valA = 0;
                }
                if (Number.isNaN(valB)) {
                    valB = 0;
                }
                if (valA < valB) {
                    result = -1;
                }
                if (valA > valB) {
                    result = 1;
                }
            } else if (sort.prop == 'analystApprovalStatus' || sort.prop == 'analystApprovedBy' || sort.prop == 'authorizedAmount') {
                if (sort.prop == 'analystApprovalStatus') {
                    valA = a.value.analystApprovalStatus;
                    valB = b.value.analystApprovalStatus;
                } else if (sort.prop == 'analystApprovedBy') {
                    valA = a.value.analystApprovedBy;
                    valB = b.value.analystApprovedBy;
                } else if (sort.prop == 'rejectReason') {
                    valA = a.value.analystApprovedBy;
                    valB = b.value.analystApprovedBy;
                }
                if (valA == null) {
                    valA = '';
                }
                if (valB == null) {
                    valB = '';
                }
                result = valA.localeCompare(valB);
            } else if (sort.prop == 'analystApprovalDateTime') {
                result = this.compareDate(a.value.analystApprovalDateTime, b.value.analystApprovalDateTime);
            } else if (sort.prop == 'fullRelease') {
                valA = a.value.fullRelease;
                valB = b.value.fullRelease;
                if (valA && !valB) {
                    result = 1;
                }
                if (!valA && valB) {
                    result = -1;
                }
            }
            if (result != 0 && sort.dir == 'desc') {
                result = result * -1;
            }
            return result;
        });

        this.form.setControl('enrolments', fa);
    }

    loadReleaseReasons() {
        console.log("222");
        let c = <Contact>this.form.controls['payTo'].value;
        if (c.id == null) {
            if (!isNaN(Number.parseInt(c.crmContactId.replace("B", "")))) {
                this.contactsService.getContactByCrmContactId(c.crmContactId).subscribe(contact => {
                    this.form.controls['payTo'].patchValue(contact);
                });
            }
        }
    }

    managerRejectReason() {
        if (this.rejectReasonList != null && this.form != null && this.form.get('managerRejectReason') != null) {
            let reason = this.rejectReasonList.find(r => r.id == this.form.get('managerRejectReason').value);
            if (reason != null) {
                return reason.description;
            }
            return '';
        }
    }

	getFinalApprovalStatus(release:Release){
		let finalApprovalStatus = null;

		if(release.finalApprovalStatus == null && release.analystApprovalStatus == 'Rejected'){
		 	finalApprovalStatus = release.analystApprovalStatus;
		}
		else{
			finalApprovalStatus = release.finalApprovalStatus;
		}
		return finalApprovalStatus;
	}

	getFinalApprovalDate(release:Release){
		let finalApprovalDate = null;

		if(release.finalApprovalStatus == null && release.analystApprovalStatus == 'Rejected' || (release.analystApprovalStatus == 'Rejected' && release.finalApprovalStatus != null && release.finalApprovalDate == null)) {
			finalApprovalDate = release.analystApprovalDateTime;
		}
		else{
			finalApprovalDate = release.finalApprovalDate;
		}
		return finalApprovalDate;
	}
    getPayToString() {
		let payTo = "";

		if(this.form.get('payTo').value != null && this.form.get('payTo').value.crmContactId != null && this.form.get('payTo').value.companyName != null){
			payTo =  (this.form.get('payTo').value != null && this.form.get('payTo').value.crmContactId != null) ? this.form.get('payTo').value.companyName : this.form.get('providedByVb').value.companyName;
		}
		else if(this.form.get('payTo').value != null && this.form.get('payTo').value.crmContactId != null && (this.form.get('payTo').value.firstName != null || this.form.get('payTo').value.lastName != null)){
			payTo =  (this.form.get('payTo').value != null && this.form.get('payTo').value.crmContactId != null) ? this.form.get('payTo').value.firstName + " " + this.form.get('payTo').value.lastName : this.form.get('providedByVb').value.companyName;
        }
        else if([3,4].includes(this.form.get('releaseType').value.id) && [3,4,5,6].includes(this.securityTypeId)) {
            payTo = 'N/A';
		}
		else{
			payTo = this.form.get('providedByVb').value.companyName;
		}
		return payTo;
	}
    getPayToAddress() {
        let payTo = this.form.get('payTo').value;
        if(payTo != null) {
            if(payTo.address != null && payTo.address.addressConcat != null && payTo.address.addressConcat != '') {
                return payTo.address.addressConcat;
            } else if(payTo.mailingAddress != null && payTo.mailingAddress.addressConcat != null && payTo.mailingAddress.addressConcat != '') {
                return payTo.mailingAddress.addressConcat;
            }
            return '';
        }

        if ([3, 4].includes(this.form.get('releaseType').value.id) && [3, 4, 5, 6].includes(this.securityTypeId)) {
            return 'N/A'
        }

        return this.form.get('providedByVb').value.address.addressConcat;
	}

    get thirdPartContactAddress(){

        if(this.form == null || this.form.get('payTo') == null) {
            return '';
        }
        let contact: Contact = this.form.get('payTo').value;
        if(contact == null || contact.address == null) {
            return '';
        }


        let thirdPartContactAddress = '';

		if(contact.address.addressConcat == "" && contact.mailingAddress.addressConcat == ""){
			thirdPartContactAddress = 'NA';
		}else if(contact.address.addressConcat != ""){
			thirdPartContactAddress =	contact.address.addressConcat;
		}else if(contact.mailingAddress.addressConcat != ""){
			thirdPartContactAddress = contact.mailingAddress.addressConcat;
		}
        thirdPartContactAddress = "Address: " +  thirdPartContactAddress;
        return thirdPartContactAddress;
    }

    get canCancelRelease() {
        let release = this.form.getRawValue() as Release;

        if(release.releaseType.id == 3 && this.session.getUser().hasPermission("FINANCE_PERMISSION")) {
            return false;
        }
        else if(release.releaseType.id == 1 && !this.session.getUser().hasPermission("FINANCE_PERMISSION")) {
            return false;
        }
        else if(release.id != null && this.releaseStatus == 'PENDING' && (this.session.getUser().hasPermission("DELETE_SECURITY_CANCEL_RELEASE") || this.session.getUser().hasPermission("CREATE_DEMAND_FINANCE"))) {
            return true;
        }
        else {
            return false;
        }
    }

    cancelRelease() {
        this.cancelReleaseModalRef = this.ngbModal.open(this.cancelReleaseModal);
		this.cancelReleaseModalRef.result.then(
			(result) => {
                this.saving = true;
                let release = this.form.getRawValue() as Release;
                this.releaseService.cancelRelease(release).subscribe(
                    release => {
                        let index = this.releases.findIndex(r => r.id == release.id);
                        this.releases[index] = release;
                        this.session.setReleases(this.releases);
                        this.saving = false;
                        this.cancel();

                        this.triggerReload.next("reload");
                    },
                    error => {
                        this.saving = false;
                        this.session.addError('Server error on canceling release', error);
                    }
                );

			},
		    (reason) => {

			}
        );
    }

    exportToExcelAutoReleases() {
		this.downloadingExcelAutoReleases = true;
		this.releaseService.getAutoReleaseForSecurityIdExcel(this._securityId).subscribe(data => {
            this.downloadingExcelAutoReleases = false;

            const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
			let d = new Date();
			const element = document.createElement('a');
			element.href = URL.createObjectURL(blob);
			element.download = "auto-releases-"+d.getTime()+".xlsx";
			document.body.appendChild(element);
			element.click();
			element.remove();
		},
		error => {
            this.downloadingExcelAutoReleases = false;
        },);
    }

    exportToExcelReleases() {
		this.downloadingExcelReleases = true;

		this.releaseService.getReleasesExcel(this._securityId).subscribe(data => {
			const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
			let d = new Date();
			const element = document.createElement('a');
			element.href = URL.createObjectURL(blob);
			element.download = "releases-summary-"+d.getTime()+".xlsx";
			document.body.appendChild(element);
			element.click();
            element.remove();
            this.downloadingExcelReleases = false;
		},
		error => {
            this.session.addError('Server error on exporting Releases', error);
            this.downloadingExcelReleases = false;
        });
    }

	currencyString(v:number){
        if(v != null) {
            return this.currency.transform(v.toString());
        } else {
            return null;
        }
    }

    compareFinalApprovalDate(valueA, valueB, rowA, rowB, sortDirection) {
        let finalApprovalDate1 = null;
        let finalApprovalDate2 = null;

		if(rowA.finalApprovalStatus == null && rowA.analystApprovalStatus == 'Rejected'){
			finalApprovalDate1 = rowA.analystApprovalDateTime;
		} else {
			finalApprovalDate1 = rowA.finalApprovalDate;
        }
        if(rowB.finalApprovalStatus == null && rowB.analystApprovalStatus == 'Rejected'){
			finalApprovalDate2 = rowB.analystApprovalDateTime;
		} else {
			finalApprovalDate2 = rowB.finalApprovalDate;
		}

        let d1 = new Date(finalApprovalDate1);
        let d2 = new Date(finalApprovalDate2);
        if (d1.getTime() < d2.getTime()) {
            return -1;
        } else if (d1.getTime() > d2.getTime()) {
            return 1;
        } else {
            return 0;
        }
    }
    getTotalAuthorizedReleaseAmount():number {
        return (this.form.get('finalApprovalStatus').value  != null && this.form.get('finalApprovalStatus').value == 'Rejected') ? 0 : this.form.get('authorizedReleaseAmount').value;
    }
    getUniqueVbs(enrolments: ReleaseEnrolment[]): string[] {
        let uniqueVbs: string[] = new Array();
        if(enrolments!=null) {
            enrolments.forEach(enrolment => {
                if(uniqueVbs.indexOf(enrolment.enrolment.enrollingVb)<0) {
                    uniqueVbs.push(enrolment.enrolment.enrollingVb);
                }
            });
        }
        return uniqueVbs;
    }

    addThirdPartyFromSecurity() {
        if(this.form.get('analystApprovalStatus').value == 'Authorized' && this.previousAnalystApprovalStatus == null
        && this.securityThirdParty != null && this.form.controls['payTo'].value == null){

            this.addThirdPartyToReleaseModalRef = this.ngbModal.open(this.addThirdPartyToReleaseModal);
            this.addThirdPartyToReleaseModalRef.result.then(
                (result) => {
                    this.form.controls['payTo'].patchValue(this.securityThirdParty, {emitEvent: false});
                },
                (reason) => {
                }
            );
        }
    }
}
