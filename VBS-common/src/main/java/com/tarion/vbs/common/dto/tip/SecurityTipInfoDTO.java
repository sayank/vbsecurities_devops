/* 
 * 
 * SecuritySearchDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.tip;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Security TIP info DTO
 *
 * @author Joni Paananen
 * @since 2018-10-16
 * @version 1.0
 */
public class SecurityTipInfoDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String vbNumber;
	private Integer securityNumber;
	private String instrumentNumber;
	private BigDecimal currentAmount;
	private String enrolmentNumber;
	private String securityType;
	private boolean multiVb;

	public String getVbNumber() {
		return vbNumber;
	}

	public void setVbNumber(String vbNumber) {
		this.vbNumber = vbNumber;
	}

	public Integer getSecurityNumber() {
		return securityNumber;
	}

	public void setSecurityNumber(Integer securityNumber) {
		this.securityNumber = securityNumber;
	}

	public String getInstrumentNumber() {
		return instrumentNumber;
	}

	public void setInstrumentNumber(String instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}

	public BigDecimal getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(BigDecimal currentAmount) {
		this.currentAmount = currentAmount;
	}

	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}

	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}

	public String getSecurityType() {
		return securityType;
	}

	public void setSecurityType(String securityType) {
		this.securityType = securityType;
	}

	public boolean isMultiVb() {
		return multiVb;
	}

	public void setMultiVb(boolean multiVb) {
		this.multiVb = multiVb;
	}

	@Override
	public String toString() {
		return "SecurityTipInfoDTO{" +
				"vbNumber=" + vbNumber +
				", securityNumber=" + securityNumber +
				", instrumentNumber='" + instrumentNumber + '\'' +
				", currentAmount=" + currentAmount +
				", enrolmentNumber='" + enrolmentNumber + '\'' +
				", securityType='" + securityType + '\'' +
				", multiVb=" + multiVb +
				'}';
	}
}
