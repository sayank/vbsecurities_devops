/* 
 * 
 * FinancialInstitutionTypeDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.financialinstitution;

import com.tarion.vbs.common.dto.NameDescriptionDTO;

/**
 * FinancialInstitutionType DTO class used as transport for Financial Institution Type Lookup data
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-11-01
 * @version 1.0
 */
public class FinancialInstitutionTypeDTO extends NameDescriptionDTO {
	private static final long serialVersionUID = 1L;
	
	@Override
	public String toString() {
		return "FinancialInstitutionTypeDTO [id=" + id + ", name=" + name + ", description=" + description + "]";
	}	
}
