/* 
 * 
 * FinancialInstitutionAmountsDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.financialinstitution;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Original and Current Total Financial Institution Amounts DTO class 
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-02-12
 * @version 1.0
 */
public class FinancialInstitutionAmountsDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	protected BigDecimal totalOriginalAmount;
	protected BigDecimal totalCurrentAmount;
	
	public BigDecimal getTotalOriginalAmount() {
		return totalOriginalAmount;
	}
	public void setTotalOriginalAmount(BigDecimal totalOriginalAmount) {
		this.totalOriginalAmount = totalOriginalAmount;
	}
	public BigDecimal getTotalCurrentAmount() {
		return totalCurrentAmount;
	}
	public void setTotalCurrentAmount(BigDecimal totalCurrentAmount) {
		this.totalCurrentAmount = totalCurrentAmount;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((totalCurrentAmount == null) ? 0 : totalCurrentAmount.hashCode());
		result = prime * result + ((totalOriginalAmount == null) ? 0 : totalOriginalAmount.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinancialInstitutionAmountsDTO other = (FinancialInstitutionAmountsDTO) obj;
		if (totalCurrentAmount == null) {
			if (other.totalCurrentAmount != null)
				return false;
		} else if (!totalCurrentAmount.equals(other.totalCurrentAmount)) {
			return false;
		}
		if (totalOriginalAmount == null) {
			if (other.totalOriginalAmount != null)
				return false;
		} else if (!totalOriginalAmount.equals(other.totalOriginalAmount)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "FinancialInstitutionAmountsDTO [totalOriginalAmount=" + totalOriginalAmount + ", totalCurrentAmount=" + totalCurrentAmount + "]";
	}
	
	
	
	
}
