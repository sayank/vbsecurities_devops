package com.tarion.vbs.common.enums;

public enum EnrolmentStatus {
	CANCELLED("Cancelled"),
	ENROLLED("Enrolled"),
	PENDING("Pending"),
	REPLACED("Replaced");
	
	private String value;
	
	EnrolmentStatus(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return this.value;
	}
	
	@Override
	public String toString() {
		return this.value;
	}
}
