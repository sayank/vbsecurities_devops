/* 
 * 
 * VbDeleteReasonDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.security;

import com.tarion.vbs.common.dto.NameDescriptionDTO;

/**
 * Vb Delete Reason DTO class used for Vb Delete Reason Lookup
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-16
 * @version 1.0
 */
public class VbDeleteReasonDTO extends NameDescriptionDTO {
	private static final long serialVersionUID = 1L;
	

	
	@Override
	public String toString() {
		return "VbDeleteReasonDTO [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
	
}
