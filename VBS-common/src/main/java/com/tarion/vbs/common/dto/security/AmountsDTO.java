package com.tarion.vbs.common.dto.security;

import java.io.Serializable;
import java.math.BigDecimal;

public class AmountsDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String securityTypeDescription;
	private BigDecimal totalSecurityAmount;
	private BigDecimal totalCashAmount;
	private BigDecimal totalOriginalAmount;

	public String getSecurityTypeDescription() {
		return securityTypeDescription;
	}

	public void setSecurityTypeDescription(String securityTypeDescription) {
		this.securityTypeDescription = securityTypeDescription;
	}

	public BigDecimal getTotalSecurityAmount() {
		return totalSecurityAmount;
	}

	public void setTotalSecurityAmount(BigDecimal totalSecurityAmount) {
		this.totalSecurityAmount = totalSecurityAmount;
	}

	public BigDecimal getTotalCashAmount() {
		return totalCashAmount;
	}

	public void setTotalCashAmount(BigDecimal totalCashAmount) {
		this.totalCashAmount = totalCashAmount;
	}
	
	public BigDecimal getTotalOriginalAmount() {
		return totalOriginalAmount;
	}

	public void setTotalOriginalAmount(BigDecimal totalOriginalAmount) {
		this.totalOriginalAmount = totalOriginalAmount;
	}

	@Override
	public String toString() {
		return "VBAmountsDTO{" +
				"securityTypeDescription='" + securityTypeDescription + '\'' +
				", totalSecurityAmount=" + totalSecurityAmount +
				", totalCashAmount=" + totalCashAmount +
				", totalOriginalAmount=" + totalOriginalAmount +
				'}';
	}
}
