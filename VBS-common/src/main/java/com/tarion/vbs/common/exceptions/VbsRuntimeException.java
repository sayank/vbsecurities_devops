/* 
 * 
 * VBApplicationsProcessingException.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.exceptions;

/**
 * VBApplicationsException marks generic excpetion for VB Applications
 * 
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-01-05
 * @version 1.0
 */
public class VbsRuntimeException extends RuntimeException {
    
    private static final long serialVersionUID = 1L;
    private final String errorCode;
    
    public VbsRuntimeException() {
    	this.errorCode = null;
    }
    public VbsRuntimeException(String msg) {
    	super(msg);
    	this.errorCode = null;
    }
    public VbsRuntimeException(Throwable cause) {
    	super(cause);
    	this.errorCode = null;
    }
    public VbsRuntimeException(String msg, String errorCode) {
        super(msg);
        this.errorCode = errorCode;
    }
    public VbsRuntimeException(String msg, String errorCode, Throwable cause) {
        super(msg, cause);
        this.errorCode = errorCode;
    }
    public VbsRuntimeException(String msg, Throwable cause) {
    	super(msg, cause);
    	this.errorCode = null;
    }

    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Return the error code number, by first stripping off the beginning 
     * value <code>PI-</code>, <code>TB-</code>
     * @return Error code number
     */
    public int getErrorCodeNumber() {
        int errorCodeNumber = 0;
        if (errorCode != null && (errorCode.startsWith("PI") || errorCode.startsWith("TB"))) {
            try {
                String[] tokens = errorCode.split("PI-|TB-");
                String code = tokens[1];
               errorCodeNumber = Integer.parseInt(code);
            } catch (NumberFormatException nfe) {
                // Default to negative one if bad error code number
                errorCodeNumber = -1;
            }
        }
        return errorCodeNumber;
    }

    @Override
    public String toString() {
        StringBuilder theString = new StringBuilder();
        if (errorCode != null) {
            theString.append(errorCode);
            theString.append(": ");
        }
        theString.append(super.toString());
        return theString.toString();
    }
    
}
