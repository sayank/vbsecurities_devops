package com.tarion.vbs.common.dto.dashboard;

import java.io.Serializable;

public class ManagerReleaseDTO implements Serializable {
	
	private static final long serialVersionUID = 7863475831957588430L;
	
	private Long securityId;
	private Integer vbNumber;
	private String vbName;
	private Integer umbrellaId;
	private String umbrellaName;
	private String instrumentNumber;
	private String securityReferenceNumber;
	private String releaseReason;
	private String amountToBeReleased;
	private String sentFrom;
	private String sentTime;

	public Long getSecurityId() {
		return securityId;
	}
	public void setSecurityId(Long securityId) {
		this.securityId = securityId;
	}
	public Integer getVbNumber() {
		return vbNumber;
	}
	public void setVbNumber(Integer vbNumber) {
		this.vbNumber = vbNumber;
	}
	public String getVbName() {
		return vbName;
	}
	public void setVbName(String vbName) {
		this.vbName = vbName;
	}
	public Integer getUmbrellaId() {
		return umbrellaId;
	}
	public void setUmbrellaId(Integer umbrellaId) {
		this.umbrellaId = umbrellaId;
	}
	public String getUmbrellaName() {
		return umbrellaName;
	}
	public void setUmbrellaName(String umbrellaName) {
		this.umbrellaName = umbrellaName;
	}
	public String getInstrumentNumber() {
		return instrumentNumber;
	}
	public void setInstrumentNumber(String instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}
	public String getSecurityReferenceNumber() {
		return securityReferenceNumber;
	}
	public void setSecurityReferenceNumber(String securityReferenceNumber) {
		this.securityReferenceNumber = securityReferenceNumber;
	}
	public String getReleaseReason() {
		return releaseReason;
	}
	public void setReleaseReason(String releaseReason) {
		this.releaseReason = releaseReason;
	}
	public String getAmountToBeReleased() {
		return amountToBeReleased;
	}
	public void setAmountToBeReleased(String amountToBeReleased) {
		this.amountToBeReleased = amountToBeReleased;
	}
	public String getSentFrom() {
		return sentFrom;
	}
	public void setSentFrom(String sentFrom) {
		this.sentFrom = sentFrom;
	}
	public String getSentTime() {
		return sentTime;
	}
	public void setSentTime(String sentTime) {
		this.sentTime = sentTime;
	}

	@Override
	public String toString() {
		return "ManagerReleaseDTO{" +
				"securityId=" + securityId +
				", vbNumber=" + vbNumber +
				", vbName='" + vbName + '\'' +
				", umbrellaId=" + umbrellaId +
				", umbrellaName='" + umbrellaName + '\'' +
				", instrumentNumber='" + instrumentNumber + '\'' +
				", securityReferenceNumber='" + securityReferenceNumber + '\'' +
				", releaseReason='" + releaseReason + '\'' +
				", amountToBeReleased='" + amountToBeReleased + '\'' +
				", sentFrom='" + sentFrom + '\'' +
				", sentTime='" + sentTime + '\'' +
				'}';
	}
}