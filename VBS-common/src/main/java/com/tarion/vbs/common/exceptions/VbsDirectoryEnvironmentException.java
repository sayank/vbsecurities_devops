package com.tarion.vbs.common.exceptions;

/**
 * Exception thrown when the Active Directory environment is 
 * not working as expected.
 * 
 * @author sallen@tarion.com
 *
 */
public class VbsDirectoryEnvironmentException extends VbsCheckedException {

	private static final long serialVersionUID = 1L;
	
	public VbsDirectoryEnvironmentException() {

    }
    public VbsDirectoryEnvironmentException(String msg) {
        super(msg);
    }
    public VbsDirectoryEnvironmentException(Throwable cause) {
        super(cause);
    }
    public VbsDirectoryEnvironmentException(String msg, String errorCode) {
        super(msg, errorCode);
    }
    public VbsDirectoryEnvironmentException(String msg, String errorCode, Throwable cause) {
        super(msg, errorCode, cause);
    }
    public VbsDirectoryEnvironmentException(String msg, Throwable cause) {
        super(msg, cause);
    }
    
}