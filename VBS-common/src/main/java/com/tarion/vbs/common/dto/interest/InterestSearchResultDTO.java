/* 
 * 
 * InterestSearchResultDTO.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.interest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tarion.vbs.common.enums.InterestPeriodTypeEnum;

/**
 * Interest Search Result DTO class used as transport for Security Interest search result
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-02-07
 * @version 1.0
 */
public class InterestSearchResultDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private InterestPeriodTypeEnum periodType;
	private int year;
	private LocalDateTime fromDate;
	private LocalDateTime toDate;
	private BigDecimal accruedInterest;
	private BigDecimal discountAmount;
	
	public InterestPeriodTypeEnum getPeriodType() {
		return periodType;
	}
	public void setPeriodType(InterestPeriodTypeEnum periodType) {
		this.periodType = periodType;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public LocalDateTime getFromDate() {
		return fromDate;
	}
	public void setFromDate(LocalDateTime fromDate) {
		this.fromDate = fromDate;
	}
	public LocalDateTime getToDate() {
		return toDate;
	}
	public void setToDate(LocalDateTime toDate) {
		this.toDate = toDate;
	}

	public BigDecimal getAccruedInterest() {
		return accruedInterest;
	}
	public void setAccruedInterest(BigDecimal accruedInterest) {
		this.accruedInterest = accruedInterest;
	}
	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fromDate == null) ? 0 : fromDate.hashCode());
		result = prime * result + ((periodType == null) ? 0 : periodType.hashCode());
		result = prime * result + ((toDate == null) ? 0 : toDate.hashCode());
		result = prime * result + year;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InterestSearchResultDTO other = (InterestSearchResultDTO) obj;
		if (fromDate == null) {
			if (other.fromDate != null)
				return false;
		} else if (!fromDate.equals(other.fromDate)) {
			return false;
		}
		if (periodType != other.periodType)
			return false;
		if (toDate == null) {
			if (other.toDate != null)
				return false;
		} else if (!toDate.equals(other.toDate)) {
			return false;
		}
		// and finally if year is the same return true
		return year != other.year;
	}
	@Override
	public String toString() {
		return "InterestSearchResultDTO [periodType=" + periodType + ", year=" + year + ", fromDate=" + fromDate + ", toDate=" + toDate +  ", accruedInterest=" + accruedInterest
				+ ", discountAmount=" + discountAmount + "]";
	}
}
