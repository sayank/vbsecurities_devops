package com.tarion.vbs.common.dto.autorelease;

import java.io.Serializable;
import java.time.LocalDateTime;

public class AutoReleaseEnrolmentDataDTO implements Serializable {

    private static final long serialVersionUID = 1594327867691398395L;

    private Long autoReleaseRunId;
    private String enrolmentNumber;
    private LocalDateTime dateOfPossession;
    private long unresolvedCases;
    private boolean inventoryItem;

    public AutoReleaseEnrolmentDataDTO(Long autoReleaseRunId, String enrolmentNumber, LocalDateTime dateOfPossession, long unresolvedCases, boolean inventoryItem) {
        this.autoReleaseRunId = autoReleaseRunId;
        this.enrolmentNumber = enrolmentNumber;
        this.dateOfPossession = dateOfPossession;
        this.unresolvedCases = unresolvedCases;
        this.inventoryItem = inventoryItem;
    }

    public Long getAutoReleaseRunId() {
        return autoReleaseRunId;
    }

    public void setAutoReleaseRunId(Long autoReleaseRunId) {
        this.autoReleaseRunId = autoReleaseRunId;
    }

    public String getEnrolmentNumber() {
        return enrolmentNumber;
    }

    public void setEnrolmentNumber(String enrolmentNumber) {
        this.enrolmentNumber = enrolmentNumber;
    }

    public LocalDateTime getDateOfPossession() {
        return dateOfPossession;
    }

    public void setDateOfPossession(LocalDateTime dateOfPossession) {
        this.dateOfPossession = dateOfPossession;
    }

    public long getUnresolvedCases() {
        return unresolvedCases;
    }

    public void setUnresolvedCases(long unresolvedCases) {
        this.unresolvedCases = unresolvedCases;
    }

    public boolean isInventoryItem() {
        return inventoryItem;
    }

    public void setInventoryItem(boolean inventoryItem) {
        this.inventoryItem = inventoryItem;
    }

    @Override
    public String toString() {
        return "AutoReleaseEnrolmentDataDTO{" +
                "autoReleaseRunId=" + autoReleaseRunId +
                ", enrolmentNumber='" + enrolmentNumber + '\'' +
                ", dateOfPossession=" + dateOfPossession +
                ", unresolvedCases=" + unresolvedCases +
                ", inventoryItem=" + inventoryItem +
                '}';
    }
}
