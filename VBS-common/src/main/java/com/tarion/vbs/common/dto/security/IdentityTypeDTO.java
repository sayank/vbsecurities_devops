/* 
 * 
 * IdentityTypeDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.security;

import com.tarion.vbs.common.dto.NameDescriptionDTO;

/**
 * Identity Type DTO class used for Identity Type Lookup
 *
 * @author <A href="djordje.przulj@tarion.com">Djordje Przulj</A>
 * @since 2019-05-07
 * @version 1.0
 */
public class IdentityTypeDTO extends NameDescriptionDTO {
	private static final long serialVersionUID = 1L;
	

	
	@Override
	public String toString() {
		return "IdentityTypeDTO [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
	
}
