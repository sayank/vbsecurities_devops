/* 
 * 
 * VbsJAXBConversionException.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.exceptions;

/**
 * VbsJAXBConversionException marks generic exception for VB Applications
 * 
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-16
 * @version 1.0
 */
public class VbsJAXBConversionException extends VbsRuntimeException {
    
    private static final long serialVersionUID = 1L;
    
    public VbsJAXBConversionException() {
    	
    }
    public VbsJAXBConversionException(String msg) {
        super(msg);        
    }
    public VbsJAXBConversionException(Throwable cause) {
        super(cause);
    }
    public VbsJAXBConversionException(String msg, String errorCode) {
        super(msg, errorCode);
    }
    public VbsJAXBConversionException(String msg, String errorCode, Throwable cause) {
        super(msg, errorCode, cause);
    }
    public VbsJAXBConversionException(String msg, Throwable cause) {
        super(msg, cause);
    }
    
}
