/* 
 * 
 * FinancialInstitutionFullDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.financialinstitution;

import java.io.Serializable;

/**
 * FinancialInstitutionFull DTO class used as transport all Financial Institution fields
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-11-01
 * @version 1.0
 */
public class FinancialInstitutionBranchCompositeDTO implements Serializable {

	private static final long serialVersionUID = -6499466464588068595L;

	private FinancialInstitutionDTO financialInstitution;
	private BranchDTO branch;


	public FinancialInstitutionBranchCompositeDTO(){
		this.financialInstitution = new FinancialInstitutionDTO();
		this.branch = new BranchDTO();
	}
	public FinancialInstitutionBranchCompositeDTO(FinancialInstitutionDTO financialInstitution, BranchDTO branch){
		this.financialInstitution = financialInstitution;
		this.branch = branch;
	}

	public BranchDTO getBranch() {
		return branch;
	}

	public void setBranch(BranchDTO branch) {
		this.branch = branch;
	}

	public FinancialInstitutionDTO getFinancialInstitution() {
		return financialInstitution;
	}

	public void setFinancialInstitution(FinancialInstitutionDTO financialInstitution) {
		this.financialInstitution = financialInstitution;
	}

	@Override
	public String toString() {
		return "FinancialInstitutionBranchCompositeDTO{" +
				" branch=" + branch +
				", financialInstitution=" + financialInstitution +
				'}';
	}
}
