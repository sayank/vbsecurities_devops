/* 
 * 
 * AutoReleaseRunSearchParametersDTO.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.autorelease;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.tarion.vbs.common.enums.SearchTypeEnum;

/**
 * Auto Release Runs Search DTO class used as transport for Auto Release Search parameters
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-04-05
 * @version 1.0
 */
public class AutoReleaseRunSearchParametersDTO implements Serializable {

	private static final long serialVersionUID = 23L;
	

	private LocalDateTime startDate;
	private LocalDateTime endDate;
	private SearchTypeEnum dateSearchType;



    public LocalDateTime getStartDate() {
		return startDate;
	}



	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}



	public LocalDateTime getEndDate() {
		return endDate;
	}



	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}



	public SearchTypeEnum getDateSearchType() {
		return dateSearchType;
	}



	public void setDateSearchType(SearchTypeEnum dateType) {
		this.dateSearchType = dateType;
	}



	@Override
    public String toString() {
        return "SecuritySearchParametersDTO{" +
                " startDate=" + startDate +
                ", endDate=" + endDate +
                ", dateSearchType=" + dateSearchType +
                '}';
    }
}
