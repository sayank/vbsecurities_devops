/* 
 * 
 * DeletedSecuritySearchParameters.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.security;

import java.io.Serializable;
import java.time.LocalDateTime;
import com.tarion.vbs.common.enums.SearchTypeEnum;

/**
 * Deleted Security Search DTO class used as transport for Deleted Security Search parameters
 *
 * @author <A href="sbogicevic@tarion.com">Srdjan Bogicevic</A>
 * @since 2019-08-28
 * @version 1.0
 */
public class DeletedSecuritySearchParametersDTO implements Serializable {

	private static final long serialVersionUID = 23L;
	
	private LocalDateTime updateDate;
	private LocalDateTime updateDateBetweenTo;
    private SearchTypeEnum updateDateType;

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public LocalDateTime getUpdateDateBetweenTo() {
        return updateDateBetweenTo;
    }

    public void setUpdateDateBetweenTo(LocalDateTime updateDateBetweenTo) {
        this.updateDateBetweenTo = updateDateBetweenTo;
    }

    public SearchTypeEnum getUpdateDateType() {
        return updateDateType;
    }

    public void setUpdateDateType(SearchTypeEnum updateDateType) {
        this.updateDateType = updateDateType;
    }

    @Override
    public String toString() {
        return "DeletedSecuritySearchParametersDTO{" +
                ", updateDate=" + updateDate +
                ", updateDateBetweenTo=" + updateDateBetweenTo +
                ", updateDateType=" + updateDateType +
                '}';
    }
}
