package com.tarion.vbs.common.dto.enrolment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tarion.vbs.common.enums.EnrolmentStatus;

public class EnrolmentDTO implements Serializable {

	private static final long serialVersionUID = 3396158131803323235L;	

	private String enrolmentNumber;
	private String enrollingVb;
	private String vendor;
	private String builder;
	private String unitType;
	private Long homeCategoryId;
	private boolean condoConversion;
	private LocalDateTime warrantyStartDate;
	private boolean excessDeposit;
	private boolean excessDepositRelease;
	private boolean autoOneYear;
	private boolean autoOneYearRfc;
	private boolean autoTwoYear;
	private boolean canDelete;
	private Boolean isCE;
	private String address;
	private boolean multiSecurity;
	private Boolean releasePerBB28;
	private EnrolmentStatus enrolmentStatus;
	private LocalDateTime dopWsd;
	private String raHomeCategory;
	private Boolean openCases;
	private String condoCorpHomeOwnerName;
	private Long tntReceivedPercentage;
	private Boolean luReviewedDnd;
	private Boolean finalB19Accepted;
	private Long totalNumberOfCuEnrolled;
	private Long numberOfUnitsOsCcp;
	private Boolean twoYearsAfter;
	private String wsInputRequested;
	private LocalDateTime wsInputReceived;
	private BigDecimal fcmAmountRetained;
	private String wsFcr;
	private BigDecimal raAmount;
	private BigDecimal maxUnitDeposit;
	private BigDecimal inventoryAmount;
	private Boolean inventoryItem;
	private BigDecimal excessAmount;
	private BigDecimal allocatedAmount;
	private BigDecimal currentAllocatedAmount;
	private BigDecimal pefAmount;
	private Boolean clearanceLetterIssued;
	private LocalDateTime clearanceDate;

	public EnrolmentDTO(){
		this.allocatedAmount = BigDecimal.ZERO;
		this.currentAllocatedAmount = BigDecimal.ZERO;
		this.clearanceLetterIssued = false;
		this.clearanceDate = LocalDateTime.now();
		this.excessAmount = BigDecimal.ZERO;
		this.excessDeposit = false;
		this.excessDepositRelease = false;
		this.autoOneYear = true;
		this.autoOneYearRfc = true;
		this.autoTwoYear = true;
		this.inventoryAmount = BigDecimal.ZERO;
		this.maxUnitDeposit = BigDecimal.ZERO;
		this.pefAmount = BigDecimal.ZERO;
		this.raAmount = BigDecimal.ZERO;
	}

	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}
	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}
	public String getEnrollingVb() {
		return enrollingVb;
	}
	public void setEnrollingVb(String vbNumber) {
		this.enrollingVb = vbNumber;
	}	
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getBuilder() {
		return builder;
	}
	public void setBuilder(String builder) {
		this.builder = builder;
	}
	public String getUnitType() {
		return unitType;
	}
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	public Long getHomeCategoryId() {
		return homeCategoryId;
	}
	public void setHomeCategoryId(Long homeCategoryId) {
		this.homeCategoryId = homeCategoryId;
	}
	public boolean isCondoConversion() {
		return condoConversion;
	}
	public void setCondoConversion(boolean condoConversion) {
		this.condoConversion = condoConversion;
	}
	public LocalDateTime getWarrantyStartDate() {
		return warrantyStartDate;
	}
	public void setWarrantyStartDate(LocalDateTime warrantyStartDate) {
		this.warrantyStartDate = warrantyStartDate;
	}
	public boolean isAutoOneYear() {
		return autoOneYear;
	}
	public void setAutoOneYear(boolean autoOneYear) {
		this.autoOneYear = autoOneYear;
	}
	public boolean isAutoOneYearRfc() {
		return autoOneYearRfc;
	}
	public void setAutoOneYearRfc(boolean autoOneYearRfc) {
		this.autoOneYearRfc = autoOneYearRfc;
	}
	public boolean isAutoTwoYear() {
		return autoTwoYear;
	}
	public void setAutoTwoYear(boolean autoTwoYear) {
		this.autoTwoYear = autoTwoYear;
	}
	public void setExcessDeposit(boolean excessDeposit) {
		this.excessDeposit = excessDeposit;
	}
	public boolean isExcessDepositRelease() {
		return excessDepositRelease;
	}
	public void setExcessDepositRelease(boolean excessDepositRelease) {
		this.excessDepositRelease = excessDepositRelease;
	}
	public Boolean getIsCE() {
		return isCE;
	}
	public void setIsCE(Boolean isCE) {
		this.isCE = isCE;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Boolean getReleasePerBB28() {
		return releasePerBB28;
	}
	public void setReleasePerBB28(Boolean releasePerBB28) {
		this.releasePerBB28 = releasePerBB28;
	}
	public EnrolmentStatus getEnrolmentStatus() {
		return enrolmentStatus;
	}
	public void setEnrolmentStatus(EnrolmentStatus enrolmentStatus) {
		this.enrolmentStatus = enrolmentStatus;
	}
	public LocalDateTime getDopWsd() {
		return dopWsd;
	}
	public void setDopWsd(LocalDateTime dopWsd) {
		this.dopWsd = dopWsd;
	}
	public String getRaHomeCategory() {
		return raHomeCategory;
	}
	public void setRaHomeCategory(String raHomeCategory) {
		this.raHomeCategory = raHomeCategory;
	}
	public Boolean getOpenCases() {
		return openCases;
	}
	public void setOpenCases(Boolean openCases) {
		this.openCases = openCases;
	}
	public String getCondoCorpHomeOwnerName() {
		return condoCorpHomeOwnerName;
	}
	public void setCondoCorpHomeOwnerName(String condoCorpHomeOwnerName) {
		this.condoCorpHomeOwnerName = condoCorpHomeOwnerName;
	}
	public Long getTntReceivedPercentage() {
		return tntReceivedPercentage;
	}
	public void setTntReceivedPercentage(Long tntReceivedPercentage) {
		this.tntReceivedPercentage = tntReceivedPercentage;
	}
	public Boolean getLuReviewedDnd() {
		return luReviewedDnd;
	}
	public void setLuReviewedDnd(Boolean luReviewedDnd) {
		this.luReviewedDnd = luReviewedDnd;
	}
	public Boolean getFinalB19Accepted() {
		return finalB19Accepted;
	}
	public void setFinalB19Accepted(Boolean finalB19Accepted) {
		this.finalB19Accepted = finalB19Accepted;
	}
	public Long getTotalNumberOfCuEnrolled() {
		return totalNumberOfCuEnrolled;
	}
	public void setTotalNumberOfCuEnrolled(Long totalNumberOfCuEnrolled) {
		this.totalNumberOfCuEnrolled = totalNumberOfCuEnrolled;
	}
	public Long getNumberOfUnitsOsCcp() {
		return numberOfUnitsOsCcp;
	}
	public void setNumberOfUnitsOsCcp(Long numberOfUnitsOsCcp) {
		this.numberOfUnitsOsCcp = numberOfUnitsOsCcp;
	}
	public Boolean getTwoYearsAfter() {
		return twoYearsAfter;
	}
	public void setTwoYearsAfter(Boolean twoYearsAfter) {
		this.twoYearsAfter = twoYearsAfter;
	}
	public String getWsInputRequested() {
		return wsInputRequested;
	}
	public void setWsInputRequested(String wsInputRequested) {
		this.wsInputRequested = wsInputRequested;
	}
	public LocalDateTime getWsInputReceived() {
		return wsInputReceived;
	}
	public void setWsInputReceived(LocalDateTime wsInputReceived) {
		this.wsInputReceived = wsInputReceived;
	}
	public BigDecimal getFcmAmountRetained() {
		return fcmAmountRetained;
	}
	public void setFcmAmountRetained(BigDecimal fcmAmountRetained) {
		this.fcmAmountRetained = fcmAmountRetained;
	}
	public String getWsFcr() {
		return wsFcr;
	}
	public void setWsFcr(String wsFcr) {
		this.wsFcr = wsFcr;
	}
	public BigDecimal getRaAmount() {
		return raAmount;
	}
	public void setRaAmount(BigDecimal raAmount) {
		this.raAmount = raAmount;
	}
	public BigDecimal getMaxUnitDeposit() {
		return maxUnitDeposit;
	}
	public void setMaxUnitDeposit(BigDecimal maxUnitDeposit) {
		this.maxUnitDeposit = maxUnitDeposit;
	}
	public BigDecimal getInventoryAmount() {
		return inventoryAmount;
	}
	public void setInventoryAmount(BigDecimal inventoryAmount) {
		this.inventoryAmount = inventoryAmount;
	}
	public Boolean getInventoryItem() {
		return inventoryItem;
	}
	public void setInventoryItem(Boolean inventoryItem) {
		this.inventoryItem = inventoryItem;
	}
	public boolean getExcessDeposit() {
		return excessDeposit;
	}
	public BigDecimal getExcessAmount() {
		return excessAmount;
	}
	public void setExcessAmount(BigDecimal excessAmount) {
		this.excessAmount = excessAmount;
	}
	public BigDecimal getAllocatedAmount() {
		return allocatedAmount;
	}
	public void setAllocatedAmount(BigDecimal allocatedAmount) {
		this.allocatedAmount = allocatedAmount;
	}	
	public BigDecimal getCurrentAllocatedAmount() {
		return currentAllocatedAmount;
	}
	public void setCurrentAllocatedAmount(BigDecimal currentAllocatedAmount) {
		this.currentAllocatedAmount = currentAllocatedAmount;
	}
	public BigDecimal getPefAmount() {
		return pefAmount;
	}
	public void setPefAmount(BigDecimal pefAmount) {
		this.pefAmount = pefAmount;
	}
	public Boolean getClearanceLetterIssued() {
		return clearanceLetterIssued;
	}
	public void setClearanceLetterIssued(Boolean clearanceLetterIssued) {
		this.clearanceLetterIssued = clearanceLetterIssued;
	}
	public LocalDateTime getClearanceDate() {
		return clearanceDate;
	}
	public void setClearanceDate(LocalDateTime clearanceDate) {
		this.clearanceDate = clearanceDate;
	}
	public Boolean getMultiSecurity() {
		return multiSecurity;
	}
	public void setMultiSecurity(Boolean multiSecurity) {
		this.multiSecurity = multiSecurity;
	}

	public boolean isCanDelete() {
		return canDelete;
	}

	public void setCanDelete(boolean canDelete) {
		this.canDelete = canDelete;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((enrolmentNumber == null) ? 0 : enrolmentNumber.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnrolmentDTO other = (EnrolmentDTO) obj;
		if (enrolmentNumber == null) {
			if (other.enrolmentNumber != null)
				return false;
		} else if (!enrolmentNumber.equals(other.enrolmentNumber)) {
			return false;
		}
		return true;
	}
	@Override
	public String toString() {
		return "EnrolmentDTO [enrolmentNumber=" + enrolmentNumber + ", enrollingVb=" + enrollingVb + ", vendor="
				+ vendor + ", builder=" + builder + ", unitType=" + unitType + ", homeCategoryId=" + homeCategoryId
				+ ", condoConversion=" + condoConversion + ", warrantyStartDate=" + warrantyStartDate
				+ ", excessDeposit=" + excessDeposit + ", excessDepositRelease=" + excessDepositRelease
				+ ", autoOneYear=" + autoOneYear + ", autoOneYearRfc=" + autoOneYearRfc + ", autoTwoYear=" + autoTwoYear
				+ ", isCE=" + isCE + ", address=" + address + ", multiSecurity="
				+ multiSecurity + ", releasePerBB28=" + releasePerBB28 + ", enrolmentStatus=" + enrolmentStatus
				+ ", dopWsd=" + dopWsd + ", raHomeCategory=" + raHomeCategory + ", openCases=" + openCases
				+ ", condoCorpHomeOwnerName=" + condoCorpHomeOwnerName + ", tntReceivedPercentage="
				+ tntReceivedPercentage + ", luReviewedDnd=" + luReviewedDnd + ", finalB19Accepted=" + finalB19Accepted
				+ ", totalNumberOfCuEnrolled=" + totalNumberOfCuEnrolled + ", numberOfUnitsOsCcp=" + numberOfUnitsOsCcp
				+ ", twoYearsAfter=" + twoYearsAfter + ", wsInputRequested=" + wsInputRequested + ", wsInputReceived="
				+ wsInputReceived + ", fcmAmountRetained=" + fcmAmountRetained + ", wsFcr=" + wsFcr + ", raAmount="
				+ raAmount + ", maxUnitDeposit=" + maxUnitDeposit + ", inventoryAmount=" + inventoryAmount
				+ ", inventoryItem=" + inventoryItem + ", excessAmount=" + excessAmount + ", allocatedAmount="
				+ allocatedAmount + ", currentAllocatedAmount=" + currentAllocatedAmount + ", pefAmount=" + pefAmount
				+ ", clearanceLetterIssued=" + clearanceLetterIssued + ", clearanceDate=" + clearanceDate
				+ "]";
	}
	
}
