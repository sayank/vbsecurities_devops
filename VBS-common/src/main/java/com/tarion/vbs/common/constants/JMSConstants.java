/* 
 * 
 * JMSConstants.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.constants;

/**
 * Constants for JMS Messaging
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-03-28
 * @version 1.0
 */
public class JMSConstants {
	
    public static final String CRM_RESPONSE_SUCCESS = "P";
    public static final String CRM_RESPONSE_FAILURE = "F";

	public static final String SIGN_OFF_JMS_MESSAGE_RESPONSE_SUCCESS = "SUCCESS";
	public static final String SIGN_OFF_JMS_MESSAGE_RESPNOSE_FAILURE = "FAIL";

    public static final String CRM_Y = "Y";
    public static final String CRM_N = "N";

    public static final String FMS_N = "N";
    public static final String FMS_Y = "Y";
    public static final String FMS_PARTIAL = "P";
    public static final String FMS_TRANSACTION_CODE_RENEWAL = "RNW";
    public static final String FMS_TRANSACTION_CODE_REGISTRATION = "REG";
   
	public static final String VBS_XA_CF_JNDI = "jms/XALocalVbsCF";
	public static final String VBS_XA_TOPIC_CF_JNDI = "jms/XAVbsCF";

	public static final String FMS_REQUEST_QUEUE_NAME = "jms/VbsFmsRequestQueue";
	public static final String FMS_RESPONSE_QUEUE_NAME = "jms/VbsFmsResponseQueue";
	
	public static final String BSA_REQUEST_QUEUE_NAME = "jms/VbsBsaRequestQueue";
	public static final String BSA_RESPONSE_QUEUE_NAME = "jms/VbsBsaResponseQueue";
	
	public static final String CRM_UPDATE_QUEUE_NAME = "jms/VbsCrmUpdateQueue";
	public static final String CRM_REQUEST_QUEUE_NAME = "jms/VbsCrmRequestQueue";

	public static final String TIP_GENERATE_CORRESPONDENCE_QUEUE_NAME = "jms/GenerateCorrespondenceRequestQueue";

	public static final String TEST_JMS_PREFIX = "jmsPrefix";
	public static final String TEST_JMS_PREFIX_VALUE = "openejb:Resource/";

    // JMS Constants
	public static final String MESSAGE_TYPE = "MessageType";
    public static final String MESSAGE_NAME = "MessageName";
    public static final String DESTINATION_NODES = "DestinationNodes";
    public static final String DESTINATION_NODE = "DestinationNode";
    public static final String REQUESTING_NODE = "RequestingNode";
    public static final String FINAL_DESTINATION_NODE = "FinalDestinationNode";
    public static final String JMS_PROVIDER = "JMSProvider";
    public static final String VERSION = "version";
    public static final String JMS_MESSAGE_TYPE = "JMSMessageType";
    public static final String CRED = "Password";
    
	public static final String CRM_JMS_DESTINATION = "vbs.crm.jms_destination_node";
	public static final String FMS_JMS_DESTINATION = "vbs.fms.jms_destination_node";

    public static final String PING = "ping";

    // Internal Processing Queue Constants
    public static final String INTERNAL_QUEUE_NAME = "jms/VbsAsyncProcessingQueue";
    public static final String INTERNAL_QUEUE_NAME_SAF = "jms/VbsAsyncProcessingQueueSAF";



    private JMSConstants() {}

}
