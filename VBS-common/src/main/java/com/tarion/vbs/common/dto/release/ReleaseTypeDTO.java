/* 
 * 
 * ReleaseTypeDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.release;

import com.tarion.vbs.common.dto.NameDescriptionDTO;

/**
 * Release Type DTO class used for Release Types Lookup
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-03-15
 * @version 1.0
 */
public class ReleaseTypeDTO extends NameDescriptionDTO {
	private static final long serialVersionUID = 1L;
	

	
	@Override
	public String toString() {
		return "ReleaseTypeDTO [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
	
}
