package com.tarion.vbs.common.dto.dashboard;

import java.io.Serializable;

public class NewBlanketCEDTO implements Serializable {

    private static final long serialVersionUID = 1821630742300017920L;

    private int enrolmentNumber;
    private int vbNumber;
    private String vbName;
    private String enrolmentAddress;

    public int getEnrolmentNumber() {
        return enrolmentNumber;
    }

    public void setEnrolmentNumber(int enrolmentNumber) {
        this.enrolmentNumber = enrolmentNumber;
    }

    public int getVbNumber() {
        return vbNumber;
    }

    public void setVbNumber(int vbNumber) {
        this.vbNumber = vbNumber;
    }

    public String getVbName() {
        return vbName;
    }

    public void setVbName(String vbName) {
        this.vbName = vbName;
    }

    public String getEnrolmentAddress() {
        return enrolmentAddress;
    }

    public void setEnrolmentAddress(String enrolmentAddress) {
        this.enrolmentAddress = enrolmentAddress;
    }

    @Override
    public String toString() {
        return "NewBlanketCEDTO{" +
                "enrolmentNumber=" + enrolmentNumber +
                ", vbNumber=" + vbNumber +
                ", vbName='" + vbName + '\'' +
                ", enrolmentAddress='" + enrolmentAddress + '\'' +
                '}';
    }

}
