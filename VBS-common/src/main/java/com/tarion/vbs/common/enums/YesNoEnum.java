/* 
 * 
 * YesNoEnum.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.enums;

/**
 * Yes No Enum used for drop downs
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-01-05
 * @version 1.0
 */
public enum YesNoEnum {
    YES("Yes")
    , NO("No");
    
    private String value;

    YesNoEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
	
	@Override
	public String toString() {
		return this.value;
	}
}
