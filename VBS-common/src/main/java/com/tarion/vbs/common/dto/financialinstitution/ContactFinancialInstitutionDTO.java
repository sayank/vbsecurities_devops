/* 
 * 
 * ContactFinancialInstitutionDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.financialinstitution;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.tarion.vbs.common.dto.contact.ContactDTO;

/**
 * ContactFinancialInstitution DTO class used as transport for Contact of Financial Institution
 *
 * @author <A href="sbogicevic@tarion.com">Srdjan Bogicevic</A>
 * @since 2019-05-17
 * @version 1.0
 */
public class ContactFinancialInstitutionDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long version;
	
	private Long financialInstitutionId;
	private ContactDTO contact;
	private LocalDateTime startDate;
	private LocalDateTime endDate;
	private boolean deleted;

	private LocalDateTime createDate;
	private String createUser;
	private LocalDateTime updateDate;
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getFinancialInstitutionId() {
		return financialInstitutionId;
	}

	public void setFinancialInstitutionId(Long financialInstitutionId) {
		this.financialInstitutionId = financialInstitutionId;
	}

	public ContactDTO getContact() {
		return contact;
	}

	public void setContact(ContactDTO contact) {
		this.contact = contact;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
		}
		
		public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContactFinancialInstitutionDTO other = (ContactFinancialInstitutionDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ContactFinancialInstitutionDTO [id=" + id + ", version=" + version + 
				", startDate=" + startDate +
				", endDate=" + endDate +
				", deleted=" + deleted +
				"]";
	}	
}
