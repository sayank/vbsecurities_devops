package com.tarion.vbs.common.dto.autorelease;

import java.io.Serializable;
import java.util.List;

public class AutoReleaseResultWrapper implements Serializable {

	private static final long serialVersionUID = 5167793111561823908L;

	private List<AutoReleaseResultDTO> autoReleasePassedResults;
	private List<AutoReleaseResultDTO> autoReleaseFailedResults;
	private AutoReleaseRunSearchResultDTO autoReleaseRun;

	public List<AutoReleaseResultDTO> getAutoReleasePassedResults() {
		return autoReleasePassedResults;
	}
	public void setAutoReleasePassedResults(List<AutoReleaseResultDTO> autoReleasePassedResults) {
		this.autoReleasePassedResults = autoReleasePassedResults;
	}
	public List<AutoReleaseResultDTO> getAutoReleaseFailedResults() {
		return autoReleaseFailedResults;
	}
	public void setAutoReleaseFailedResults(List<AutoReleaseResultDTO> autoReleasefailedResults) {
		this.autoReleaseFailedResults = autoReleasefailedResults;
	}
	public AutoReleaseRunSearchResultDTO getAutoReleaseRun() {
		return autoReleaseRun;
	}
	public void setAutoReleaseRun(AutoReleaseRunSearchResultDTO autoReleaseRun) {
		this.autoReleaseRun = autoReleaseRun;
	}

	@Override
	public String toString() {
		return "AutoReleaseResultWrapper{" +
				"autoReleasePassedResults=" + autoReleasePassedResults +
				", autoReleaseFailedResults=" + autoReleaseFailedResults +
				", autoReleaseRun=" + autoReleaseRun +
				'}';
	}
}
