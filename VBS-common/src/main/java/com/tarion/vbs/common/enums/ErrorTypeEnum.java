package com.tarion.vbs.common.enums;

public enum ErrorTypeEnum {
    VALIDATION_FAIL, ALERT, ALERT_FAIL, CONFIRM, CONFIRM_SAVE;
}
