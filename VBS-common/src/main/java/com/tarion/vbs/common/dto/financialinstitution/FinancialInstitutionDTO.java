/* 
 * 
 * FinancialInstitutionDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.financialinstitution;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * FinancialInstitution DTO class used as transport for Financial Institution basic fields
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-11-01
 * @version 1.0
 */
public class FinancialInstitutionDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long version;
	private String name;
	private String description;
	private LocalDateTime expiryDate;
	private BigDecimal originalAmountOnInstitution;
	private BigDecimal currentAmountOnInstitution;

	private BigDecimal maxSecurityAmount;
	private BigDecimal wnSecurityMaxAmount;
	private BigDecimal availableAmountFi;
	private Long financialInstitutionTypeId;

	private List<FinancialInstitutionMaaPoaDTO> maaPoas;
	private List<ContactFinancialInstitutionDTO> contacts;
	private List<FinancialInstitutionAlertDTO> alerts;

	private LocalDateTime createDate;
	private String createUser;
	private LocalDateTime updateDate;
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(LocalDateTime expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getOriginalAmountOnInstitution() {
		return originalAmountOnInstitution;
	}

	public void setOriginalAmountOnInstitution(BigDecimal originalAmountOnInstitution) {
		this.originalAmountOnInstitution = originalAmountOnInstitution;
	}

	public BigDecimal getCurrentAmountOnInstitution() {
		return currentAmountOnInstitution;
	}

	public void setCurrentAmountOnInstitution(BigDecimal currentAmountOnInstitution) {
		this.currentAmountOnInstitution = currentAmountOnInstitution;
	}
	
	public Long getFinancialInstitutionTypeId() {
		return financialInstitutionTypeId;
	}

	public void setFinancialInstitutionTypeId(Long financialInstitutionTypeId) {
		this.financialInstitutionTypeId = financialInstitutionTypeId;
	}
	
	public BigDecimal getMaxSecurityAmount() {
		return maxSecurityAmount;
	}

	public void setMaxSecurityAmount(BigDecimal maxSecurityAmount) {
		this.maxSecurityAmount = maxSecurityAmount;
	}

	public BigDecimal getWnSecurityMaxAmount() {
		return wnSecurityMaxAmount;
	}

	public void setWnSecurityMaxAmount(BigDecimal wnSecurityMaxAmount) {
		this.wnSecurityMaxAmount = wnSecurityMaxAmount;
	}

	public List<FinancialInstitutionMaaPoaDTO> getMaaPoas() {
		return maaPoas;
	}

	public void setMaaPoas(List<FinancialInstitutionMaaPoaDTO> maaPoas) {
		this.maaPoas = maaPoas;
	}

	public List<ContactFinancialInstitutionDTO> getContacts() {
		return contacts;
	}

	public void setContacts(List<ContactFinancialInstitutionDTO> contacts) {
		this.contacts = contacts;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public BigDecimal getAvailableAmountFi() {
		return availableAmountFi;
	}

	public void setAvailableAmountFi(BigDecimal availableAmountFi) {
		this.availableAmountFi = availableAmountFi;
	}

	public void setAlerts(List<FinancialInstitutionAlertDTO> alerts) {
		this.alerts = alerts;
		
	}

	public List<FinancialInstitutionAlertDTO> getAlerts() {
		return this.alerts;
	}	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinancialInstitutionDTO other = (FinancialInstitutionDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "FinancialInstitutionDTO [id=" + id + ", version=" + version + 
				", name=" + name +
				", description=" + description +
				", expiryDate=" + expiryDate +
				", maxSecurityAmount=" + maxSecurityAmount +
				", wnSecurityMaxAmount=" + wnSecurityMaxAmount +
				", originalAmountOnInstitution=" + originalAmountOnInstitution +
				", currentAmountOnInstitution=" + currentAmountOnInstitution +
				", financialInstitutionTypeId=" + financialInstitutionTypeId +
				"]";
	}
}
