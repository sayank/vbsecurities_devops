package com.tarion.vbs.common.dto;

import com.tarion.vbs.common.util.VbsUtil;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class AddressDTO implements Serializable{


	private static final long serialVersionUID = -2155751056592901645L;

	private Long id;
	private Long version;
	private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String addressLine4;
    private String city;
    private String postalCode;
    private String province;
    private String country;
	private LocalDateTime createDate;
	private String createUser;
	private LocalDateTime updateDate;
	private String updateUser;
	private String addressConcat;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getAddressLine4() {
		return addressLine4;
	}

	public void setAddressLine4(String addressLine4) {
		this.addressLine4 = addressLine4;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getAddressConcat() {
		if(this.addressConcat == null){
			List<String> list = new LinkedList<>();
			if(!VbsUtil.isNullorEmpty(this.addressLine1)) {list.add(this.addressLine1);}
			if(!VbsUtil.isNullorEmpty(this.addressLine2)) {list.add(this.addressLine2);}
			if(!VbsUtil.isNullorEmpty(this.city)) {list.add(this.city);}
			if(!VbsUtil.isNullorEmpty(this.province)) {list.add(this.province);}
			if(!VbsUtil.isNullorEmpty(this.postalCode)) {list.add(this.postalCode);}
			this.addressConcat = String.join(", ", list);
		}
		return addressConcat;
	}

	public void setAddressConcat(String addressConcat) {
		this.addressConcat = addressConcat;
	}

	@Override
	public String toString() {
		return "AddressDTO{" +
				"id=" + id +
				", version=" + version +
				", addressLine1='" + addressLine1 + '\'' +
				", addressLine2='" + addressLine2 + '\'' +
				", addressLine3='" + addressLine3 + '\'' +
				", addressLine4='" + addressLine4 + '\'' +
				", city='" + city + '\'' +
				", postalCode='" + postalCode + '\'' +
				", province='" + province + '\'' +
				", country='" + country + '\'' +
				", createDate=" + createDate +
				", createUser='" + createUser + '\'' +
				", updateDate=" + updateDate +
				", updateUser='" + updateUser + '\'' +
				", addressConcat='" + addressConcat + '\'' +
				'}';
	}
}
