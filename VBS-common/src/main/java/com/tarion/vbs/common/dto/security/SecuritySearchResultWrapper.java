package com.tarion.vbs.common.dto.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SecuritySearchResultWrapper implements Serializable {

	private static final long serialVersionUID = 470394968898588466L;

	private String queryString;
	private SecuritySearchResultTotalsDTO totals;
	private List<SecuritySearchResultDTO> securities;
	
	public SecuritySearchResultWrapper() {
		this.totals = new SecuritySearchResultTotalsDTO();
		this.securities = new ArrayList<>();
	}	
	
	public String getQueryString() {
		return queryString;
	}
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}
	public SecuritySearchResultTotalsDTO getTotals() {
		return totals;
	}
	public void setTotals(SecuritySearchResultTotalsDTO totals) {
		this.totals = totals;
	}
	public List<SecuritySearchResultDTO> getSecurities() {
		return securities;
	}
	public void setSecurities(List<SecuritySearchResultDTO> securities) {
		this.securities = securities;
	}

}
