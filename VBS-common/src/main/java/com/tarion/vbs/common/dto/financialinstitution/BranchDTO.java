/* 
 * 
 * SecuBranchDTOrityDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.financialinstitution;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Branch DTO class used as transport for Branch fields
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-11-01
 * @version 1.0
 */
public class BranchDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long version;
	private String name;
	private String description;
	private Long financialInstitutionId;
	private String addressConcat;
	private String crmContactId;

    private LocalDateTime createDate;
	private String createUser;
	private LocalDateTime updateDate;
	private String updateUser;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getFinancialInstitutionId() {
		return financialInstitutionId;
	}
	public void setFinancialInstitutionId(Long financialInstitutionId) {
		this.financialInstitutionId = financialInstitutionId;
	}
	public String getAddressConcat() {
		return addressConcat;
	}
	public void setAddressConcat(String addressConcat) {
		this.addressConcat = addressConcat;
	}
	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public String getCrmContactId() {
		return crmContactId;
	}
	public void setCrmContactId(String crmContactId) {
		this.crmContactId = crmContactId;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BranchDTO other = (BranchDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "FinancialInstitutionEntity [id=" + id + ", version=" + version + 
				", name=" + name +
				", description=" + description +
				", financialInstitutionId=" + financialInstitutionId +
				", crmContactId=" + crmContactId +
 				"]";
	}	
}
