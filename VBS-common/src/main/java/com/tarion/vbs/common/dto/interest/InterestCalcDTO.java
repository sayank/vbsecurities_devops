package com.tarion.vbs.common.dto.interest;

import java.io.Serializable;
import java.math.BigDecimal;

public class InterestCalcDTO implements Serializable {

    private static final long serialVersionUID = -4950484262806456044L;

    private Long securityId;
    private BigDecimal interestAmount;
    private String vbNumber;
    private String instrumentNumber;

    public InterestCalcDTO(){}
    public InterestCalcDTO(Long securityId, BigDecimal interestAmount, String vbNumber, String instrumentNumber){
        this.securityId = securityId;
        this.interestAmount = interestAmount;
        this.vbNumber = vbNumber;
        this.instrumentNumber = instrumentNumber;

    }

    public Long getSecurityId() {
        return securityId;
    }

    public void setSecurityId(Long securityId) {
        this.securityId = securityId;
    }

    public BigDecimal getInterestAmount() {
        return interestAmount;
    }

    public void setInterestAmount(BigDecimal interestAmount) {
        this.interestAmount = interestAmount;
    }

    public String getVbNumber() {
        return vbNumber;
    }

    public void setVbNumber(String vbNumber) {
        this.vbNumber = vbNumber;
    }

    public String getInstrumentNumber() {
        return instrumentNumber;
    }

    public void setInstrumentNumber(String instrumentNumber) {
        this.instrumentNumber = instrumentNumber;
    }
}
