package com.tarion.vbs.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.tarion.vbs.common.constants.VbsConstants;

public class ValidationUtil {
	
	private ValidationUtil() {	}
	
	public static boolean isNumericStringValid(String stringValue) {
		return isNumericStringValid(stringValue, true);
	}
	
	public static boolean isNumericStringValid(String stringValue, boolean replaceDollar) {
		if (stringValue != null && stringValue.length() > 0) {
			try {
				stringValue = stringValue.replace(",", "").trim();
				if (replaceDollar) {
					stringValue = stringValue.replace("$", "");
				}
				Double.parseDouble(stringValue);
				return true;
			} 
			catch (Exception e) {
				return false;
			}
		}
		else {
			return true;
		}
	}

	public static boolean isNumberZeroOrNegative(Long number) {
		return (number.longValue() < 1);
	}
}
