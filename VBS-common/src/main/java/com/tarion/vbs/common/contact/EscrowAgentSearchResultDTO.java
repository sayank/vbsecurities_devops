package com.tarion.vbs.common.contact;

import java.io.Serializable;

public class EscrowAgentSearchResultDTO  implements Serializable{

	private static final long serialVersionUID = -1750882640695514736L;

	private Integer id;
	private String crmContactId;
	private String escrowAgent;
	private String escrowAgentLicenceStatus;
	private String licenceApplicationStatus;
	private String alert;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getAlert() {
		return alert;
	}
	public void setAlert(String alert) {
		this.alert = alert;
	}
	public String getCrmContactId() {
		return crmContactId;
	}
	public void setCrmContactId(String crmContactId) {
		this.crmContactId = crmContactId;
	}
	public String getEscrowAgent() {
		return escrowAgent;
	}
	public void setEscrowAgent(String escrowAgent) {
		this.escrowAgent = escrowAgent;
	}
	public String getEscrowAgentLicenceStatus() {
		return escrowAgentLicenceStatus;
	}
	public void setEscrowAgentLicenceStatus(String escrowAgentLicenceStatus) {
		this.escrowAgentLicenceStatus = escrowAgentLicenceStatus;
	}
	public String getLicenceApplicationStatus() {
		return licenceApplicationStatus;
	}
	public void setLicenceApplicationStatus(String licenceApplicationStatus) {
		this.licenceApplicationStatus = licenceApplicationStatus;
	}

	@Override
	public String toString() {
		return "EscrowAgentSearchResultDTO{" +
				"id=" + id +
				", crmContactId='" + crmContactId + '\'' +
				", escrowAgent='" + escrowAgent + '\'' +
				", escrowAgentLicenceStatus='" + escrowAgentLicenceStatus + '\'' +
				", licenceApplicationStatus='" + licenceApplicationStatus + '\'' +
				'}';
	}
}
