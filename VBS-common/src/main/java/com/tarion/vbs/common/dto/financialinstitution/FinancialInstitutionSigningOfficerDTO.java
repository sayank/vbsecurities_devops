/* 
 * 
 * FinancialInstitutionSigningOfficerDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.financialinstitution;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * FinancialInstitutionSigningOfficer DTO class used as transport for Signing Officer of Financial Institution
 *
 * @author <A href="sbogicevic@tarion.com">Srdjan Bogicevic</A>
 * @since 2019-05-17
 * @version 1.0
 */
public class FinancialInstitutionSigningOfficerDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
    private Long version;
	
	private Long financialInstitutionMaaPoaId;
	private String companyName;
	private String contactName;
	private boolean deleted;

	private LocalDateTime createDate;
	private String createUser;
	private LocalDateTime updateDate;
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getFinancialInstitutionMaaPoaId() {
		return financialInstitutionMaaPoaId;
	}

	public void setFinancialInstitutionMaaPoaId(Long financialInstitutionMaaPoaId) {
		this.financialInstitutionMaaPoaId = financialInstitutionMaaPoaId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
    }

	public boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinancialInstitutionSigningOfficerDTO other = (FinancialInstitutionSigningOfficerDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "FinancialInstitutionSigningOfficerDTO [id=" + id + ", version=" + version + 
				", companyName=" + companyName +
				", contactName=" + contactName +
				", deleted=" + deleted +
				"]";
	}	
}
