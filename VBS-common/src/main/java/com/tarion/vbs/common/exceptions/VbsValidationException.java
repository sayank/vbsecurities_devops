package com.tarion.vbs.common.exceptions;

import com.tarion.vbs.common.dto.ErrorDTO;

import java.util.List;

public class VbsValidationException extends RuntimeException {

    private static final long serialVersionUID = 3932062433836156095L;
    private List<ErrorDTO> errors;

    public VbsValidationException(List<ErrorDTO> errors){
        super("Validation failed with " + errors.size() + " errors");
        this.errors = errors;
    }

    public List<ErrorDTO> getErrors() {
        return errors;
    }

}
