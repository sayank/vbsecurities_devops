package com.tarion.vbs.common.dto;

import java.io.Serializable;

public class UrlsDTO implements Serializable {

	private static final long serialVersionUID = 3457271165001975552L;

	private String findAddUrl;
	private String allocateUrl;
	public String getFindAddUrl() {
		return findAddUrl;
	}
	public void setFindAddUrl(String findAddUrl) {
		this.findAddUrl = findAddUrl;
	}
	public String getAllocateUrl() {
		return allocateUrl;
	}
	public void setAllocateUrl(String allocateUrl) {
		this.allocateUrl = allocateUrl;
	}
	
	
}
