package com.tarion.vbs.common.dto.security;

import java.io.Serializable;
import java.math.BigDecimal;

public class SecuritySearchResultTotalsDTO implements Serializable {

	private static final long serialVersionUID = 4717296240611549134L;

	private BigDecimal totalOriginalAmount;
	private BigDecimal totalCurrentAmount;
	private BigDecimal totalCashAmount;
	private BigDecimal totalCurrentInterestAmount;
	private BigDecimal totalAvailableCashAmount;
	private BigDecimal totalOriginalAmountExcludingDtaPef;
	private BigDecimal totalCurrentAmountExcludingDtaPef;
	
	public SecuritySearchResultTotalsDTO() {
		this.totalOriginalAmount = BigDecimal.ZERO;
		this.totalCurrentAmount = BigDecimal.ZERO;
		this.totalCashAmount = BigDecimal.ZERO;
		this.totalCurrentInterestAmount = BigDecimal.ZERO;
		this.totalAvailableCashAmount = BigDecimal.ZERO;
		this.totalOriginalAmountExcludingDtaPef = BigDecimal.ZERO;
		this.totalCurrentAmountExcludingDtaPef = BigDecimal.ZERO;
	}
	
	public BigDecimal getTotalOriginalAmount() {
		return totalOriginalAmount;
	}
	public void addTotalOriginalAmount(BigDecimal totalOriginalAmount) {
		if(totalOriginalAmount != null) { this.totalOriginalAmount = this.totalOriginalAmount.add(totalOriginalAmount); }
	}
	public BigDecimal getTotalCurrentAmount() {
		return totalCurrentAmount;
	}
	public void addTotalCurrentAmount(BigDecimal totalCurrentAmount) {
		if(totalCurrentAmount != null) { this.totalCurrentAmount = this.totalCurrentAmount.add(totalCurrentAmount); }
	}
	public BigDecimal getTotalCashAmount() {
		return totalCashAmount;
	}
	public void addTotalCashAmount(BigDecimal totalCashAmount) {
		if(totalCashAmount != null) { this.totalCashAmount = this.totalCashAmount.add(totalCashAmount); }
	}
	public BigDecimal getTotalCurrentInterestAmount() {
		return totalCurrentInterestAmount;
	}
	public void addTotalCurrentInterestAmount(BigDecimal totalCurrentInterestAmount) {
		if(totalCurrentInterestAmount != null) { this.totalCurrentInterestAmount = this.totalCurrentInterestAmount.add(totalCurrentInterestAmount); }
	}
	public BigDecimal getTotalAvailableCashAmount() {
		return totalAvailableCashAmount;
	}
	public void addTotalAvailableCashAmount(BigDecimal totalAvailableCashAmount) {
		if(totalAvailableCashAmount != null) { this.totalAvailableCashAmount = this.totalAvailableCashAmount.add(totalAvailableCashAmount); }
	}
	public BigDecimal getTotalOriginalAmountExcludingDtaPef() {
		return totalOriginalAmountExcludingDtaPef;
	}
	public void addTotalOriginalAmountExcludingDtaPef(BigDecimal originalAmount) {
		if(originalAmount != null) { this.totalOriginalAmountExcludingDtaPef = this.totalOriginalAmountExcludingDtaPef.add(originalAmount); }
	}
	public BigDecimal getTotalCurrentAmountExcludingDtaPef() {
		return totalCurrentAmountExcludingDtaPef;
	}
	public void addTotalCurrentAmountExcludingDtaPef(BigDecimal currentAmount) {
		if(currentAmount != null) { this.totalCurrentAmountExcludingDtaPef = this.totalCurrentAmountExcludingDtaPef.add(currentAmount); }
	}

	@Override
	public String toString() {
		return "SecuritySearchResultTotalsDTO{" +
				"totalOriginalAmount=" + totalOriginalAmount +
				", totalCurrentAmount=" + totalCurrentAmount +
				", totalCashAmount=" + totalCashAmount +
				", totalCurrentInterestAmount=" + totalCurrentInterestAmount +
				", totalAvailableCashAmount=" + totalAvailableCashAmount +
				", totalOriginalAmountExcludingDtaPef=" + totalOriginalAmountExcludingDtaPef +
				", totalCurrentAmountExcludingDtaPef=" + totalCurrentAmountExcludingDtaPef +
				'}';
	}
}
