/* 
 * 
 * CashSecurityNoInterestDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.security;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * CashSecurityNoInterestDTO DTO class used as transport for Cash Security with no interest
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-06-13
 * @version 1.0
 */
public class CashSecurityNoInterestDTO implements Serializable {

	private static final long serialVersionUID = -3442227824249649415L;

	private Long securityNumber;
	private String vbNumber;
	private String vbName;
	private String instrumentNumber;
	private LocalDateTime issuedDate;
	private LocalDateTime receivedDate;
	private BigDecimal originalAmount;
	private BigDecimal currentAmount;
	private LocalDate lastInterestCalculatedDate;
	private String missingInterestDates;
	
	public Long getSecurityNumber() {
		return securityNumber;
	}
	public void setSecurityNumber(Long securityNumber) {
		this.securityNumber = securityNumber;
	}
	public String getVbNumber() {
		return vbNumber;
	}
	public void setVbNumber(String vbNumber) {
		this.vbNumber = vbNumber;
	}
	public String getVbName() {
		return vbName;
	}
	public void setVbName(String vbName) {
		this.vbName = vbName;
	}
	public String getInstrumentNumber() {
		return instrumentNumber;
	}
	public void setInstrumentNumber(String instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}
	public LocalDateTime getIssuedDate() {
		return issuedDate;
	}
	public void setIssuedDate(LocalDateTime issuedDate) {
		this.issuedDate = issuedDate;
	}
	public LocalDateTime getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(LocalDateTime receivedDate) {
		this.receivedDate = receivedDate;
	}
	public BigDecimal getOriginalAmount() {
		return originalAmount;
	}
	public void setOriginalAmount(BigDecimal originalAmount) {
		this.originalAmount = originalAmount;
	}
	public BigDecimal getCurrentAmount() {
		return currentAmount;
	}
	public void setCurrentAmount(BigDecimal currentAmount) {
		this.currentAmount = currentAmount;
	}
	public LocalDate getLastInterestCalculatedDate() {
		return lastInterestCalculatedDate;
	}
	public void setLastInterestCalculatedDate(LocalDate lastInterestCalculatedDate) {
		this.lastInterestCalculatedDate = lastInterestCalculatedDate;
	}

	public String getMissingInterestDates() {
		return missingInterestDates;
	}

	public void setMissingInterestDates(String missingInterestDates) {
		this.missingInterestDates = missingInterestDates;
	}

	@Override
	public String toString() {
		return "CashSecurityNoInterestDTO{" +
				"securityNumber=" + securityNumber +
				", vbNumber='" + vbNumber + '\'' +
				", vbName='" + vbName + '\'' +
				", instrumentNumber='" + instrumentNumber + '\'' +
				", issuedDate=" + issuedDate +
				", receivedDate=" + receivedDate +
				", originalAmount=" + originalAmount +
				", currentAmount=" + currentAmount +
				", lastInterestCalculatedDate=" + lastInterestCalculatedDate +
				", missingInterestDates='" + missingInterestDates + '\'' +
				'}';
	}
}
