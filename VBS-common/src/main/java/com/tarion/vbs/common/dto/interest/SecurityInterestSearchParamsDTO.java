package com.tarion.vbs.common.dto.interest;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.tarion.vbs.common.enums.InterestPeriodTypeEnum;

public class SecurityInterestSearchParamsDTO implements Serializable {


	private static final long serialVersionUID = 2184004481969524000L;

	private Long securityId;
	private LocalDateTime fromDate;
	private LocalDateTime toDate;
	private InterestPeriodTypeEnum periodType;

	public SecurityInterestSearchParamsDTO(){}
	public SecurityInterestSearchParamsDTO(Long securityId, LocalDateTime fromDate, LocalDateTime toDate, InterestPeriodTypeEnum periodType){
		this.securityId = securityId;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.periodType = periodType;
	}
	
	public Long getSecurityId() {
		return securityId;
	}
	public void setSecurityId(Long securityId) {
		this.securityId = securityId;
	}
	public InterestPeriodTypeEnum getPeriodType() {
		return periodType;
	}
	public void setPeriodType(InterestPeriodTypeEnum periodType) {
		this.periodType = periodType;
	}
	public LocalDateTime getFromDate() {
		return fromDate;
	}
	public void setFromDate(LocalDateTime fromDate) {
		this.fromDate = fromDate;
	}
	public LocalDateTime getToDate() {
		return toDate;
	}
	public void setToDate(LocalDateTime toDate) {
		this.toDate = toDate;
	}
	
	
}
