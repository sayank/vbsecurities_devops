package com.tarion.vbs.common.dto.security;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ReplaceBySearchParametersDTO implements Serializable{

	private static final long serialVersionUID = 5642893386926795073L;

	private Long securityNumberFrom;
	private Long securityNumberTo;
	private String securityNumberSearchType;
	private String instrumentNumberFrom;
	private String instrumentNumberTo;
	private String instrumentNumberSearchType;
	private String vbNumberFrom;
	private String vbNumberTo;
	private String vbNumberSearchType;
	private LocalDateTime recievedDateFrom;
	private LocalDateTime recievedDateTo;
	private String recievedDateSearchType;
	private BigDecimal currentAmountFrom;
	private BigDecimal currentAmountTo;
	private String currentAmountSearchType;
	
	public Long getSecurityNumberFrom() {
		return securityNumberFrom;
	}
	public void setSecurityNumberFrom(Long securityNumberFrom) {
		this.securityNumberFrom = securityNumberFrom;
	}
	public Long getSecurityNumberTo() {
		return securityNumberTo;
	}
	public void setSecurityNumberTo(Long securityNumberTo) {
		this.securityNumberTo = securityNumberTo;
	}
	public String getSecurityNumberSearchType() {
		return securityNumberSearchType;
	}
	public void setSecurityNumberSearchType(String securityNumberSearchType) {
		this.securityNumberSearchType = securityNumberSearchType;
	}
	public String getInstrumentNumberFrom() {
		return instrumentNumberFrom;
	}
	public void setInstrumentNumberFrom(String instrumentNumberFrom) {
		this.instrumentNumberFrom = instrumentNumberFrom;
	}
	public String getInstrumentNumberTo() {
		return instrumentNumberTo;
	}
	public void setInstrumentNumberTo(String instrumentNumberTo) {
		this.instrumentNumberTo = instrumentNumberTo;
	}
	public String getInstrumentNumberSearchType() {
		return instrumentNumberSearchType;
	}
	public void setInstrumentNumberSearchType(String instrumentNumberSearchType) {
		this.instrumentNumberSearchType = instrumentNumberSearchType;
	}
	public String getVbNumberFrom() {
		return vbNumberFrom;
	}
	public void setVbNumberFrom(String vbNumberFrom) {
		this.vbNumberFrom = vbNumberFrom;
	}
	public String getVbNumberTo() {
		return vbNumberTo;
	}
	public void setVbNumberTo(String vbNumberTo) {
		this.vbNumberTo = vbNumberTo;
	}
	public String getVbNumberSearchType() {
		return vbNumberSearchType;
	}
	public void setVbNumberSearchType(String vbNumberSearchType) {
		this.vbNumberSearchType = vbNumberSearchType;
	}
	public LocalDateTime getRecievedDateFrom() {
		return recievedDateFrom;
	}
	public void setRecievedDateFrom(LocalDateTime recievedDateFrom) {
		this.recievedDateFrom = recievedDateFrom;
	}
	public LocalDateTime getRecievedDateTo() {
		return recievedDateTo;
	}
	public void setRecievedDateTo(LocalDateTime recievedDateTo) {
		this.recievedDateTo = recievedDateTo;
	}
	public String getRecievedDateSearchType() {
		return recievedDateSearchType;
	}
	public void setRecievedDateSearchType(String recievedDateSearchType) {
		this.recievedDateSearchType = recievedDateSearchType;
	}
	public BigDecimal getCurrentAmountFrom() {
		return currentAmountFrom;
	}
	public void setCurrentAmountFrom(BigDecimal currentAmountFrom) {
		this.currentAmountFrom = currentAmountFrom;
	}
	public BigDecimal getCurrentAmountTo() {
		return currentAmountTo;
	}
	public void setCurrentAmountTo(BigDecimal currentAmountTo) {
		this.currentAmountTo = currentAmountTo;
	}
	public String getCurrentAmountSearchType() {
		return currentAmountSearchType;
	}
	public void setCurrentAmountSearchType(String currentAmountSearchType) {
		this.currentAmountSearchType = currentAmountSearchType;
	}
	
	@Override
	public String toString() {
		return "ReplaceSearchParameterDTO [securityNumberFrom=" + securityNumberFrom + ", securityNumberTo=" + securityNumberTo + ", vbNumberFrom=" + vbNumberFrom + ", vbNumberTo=" + vbNumberTo + ", recievedDateFrom=" + recievedDateFrom + ", recievedDateTo=" + recievedDateTo + ", currentAmountFrom="
				+ currentAmountFrom + ", currentAmountTo=" + currentAmountTo + ", securityNumberSearchType=" + securityNumberSearchType
				+ ", instrumentNumberSearchType=" + instrumentNumberSearchType + ", recievedDateSearchType=" + recievedDateSearchType
				+ ", currentAmountSearchType=" + currentAmountSearchType +"]";
	}
}
