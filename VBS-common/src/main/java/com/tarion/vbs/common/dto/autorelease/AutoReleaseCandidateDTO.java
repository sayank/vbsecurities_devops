package com.tarion.vbs.common.dto.autorelease;

import java.io.Serializable;

public class AutoReleaseCandidateDTO implements Serializable {

        private static final long serialVersionUID = 1l;

        private long autoReleaseRunId;
        private long securityId;
        private String enrolmentNumber;
        private String vbNumber;
        private long timingTypeId;

        public long getAutoReleaseRunId() {
                return autoReleaseRunId;
        }

        public void setAutoReleaseRunId(long autoReleaseRunId) {
                this.autoReleaseRunId = autoReleaseRunId;
        }

        public long getSecurityId() {
                return securityId;
        }

        public void setSecurityId(long securityId) {
                this.securityId = securityId;
        }

        public String getEnrolmentNumber() {
                return enrolmentNumber;
        }

        public void setEnrolmentNumber(String enrolmentNumber) {
                this.enrolmentNumber = enrolmentNumber;
        }

        public String getVbNumber() {
                return vbNumber;
        }

        public void setVbNumber(String vbNumber) {
                this.vbNumber = vbNumber;
        }

        public long getTimingTypeId() {
                return timingTypeId;
        }

        public void setTimingTypeId(long timingTypeId) {
                this.timingTypeId = timingTypeId;
        }

        @Override
        public String toString() {
                return "AutoReleaseCandidateDTO{" +
                        "autoReleaseRunId=" + autoReleaseRunId +
                        ", securityId=" + securityId +
                        ", enrolmentNumber='" + enrolmentNumber + '\'' +
                        ", vbNumber='" + vbNumber + '\'' +
                        ", timingTypeId=" + timingTypeId +
                        '}';
        }
}
