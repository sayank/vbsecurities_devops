package com.tarion.vbs.common.dto.correspondence;

import java.io.Serializable;

public class TokenDTO implements Serializable {

	private static final long serialVersionUID = 5038261437364800844L;
	
	private String token;
	private String value;
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getValue() {
		return value == null ? "" : value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "TokenDTO [token=" + token + ", value=" + value + "]";
	}
}
