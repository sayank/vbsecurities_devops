package com.tarion.vbs.common.dto.enrolment;

import java.io.Serializable;

public class EnrolmentPairDTO implements Serializable{

	private static final long serialVersionUID = -5797317296082083225L;
	
	private String enrolmentNumber;
	private String poolEnrolmentNumber;
	
	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}
	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}
	public String getPoolEnrolmentNumber() {
		return poolEnrolmentNumber;
	}
	public void setPoolEnrolmentNumber(String poolEnrolmentNumber) {
		this.poolEnrolmentNumber = poolEnrolmentNumber;
	}
	
	
	@Override
	public String toString() {
		return "EnrolmentPairDTO [enrolmentNumber=" + enrolmentNumber + ", poolEnrolmentNumber=" + poolEnrolmentNumber
				+ "]";
	}
}
