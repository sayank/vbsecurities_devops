/* 
 * 
 * HistoryFieldDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.security;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * History Field DTO class used as transport for History of different fields
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-22
 * @version 1.0
 */
public class HistoryFieldDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String fieldName;
	private String fieldValue;
	private LocalDateTime updateTime;
	private String updateUser;
	
	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String comment) {
		this.fieldValue = comment;
	}

	public LocalDateTime getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(LocalDateTime updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fieldValue == null) ? 0 : fieldValue.hashCode());
		result = prime * result + ((updateTime == null) ? 0 : updateTime.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoryFieldDTO other = (HistoryFieldDTO) obj;
		if (fieldValue == null) {
			if (other.fieldValue != null)
				return false;
		} else if (!fieldValue.equals(other.fieldValue)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "HistoryFieldDTO [fieldValue=" + fieldValue + 
				", fieldName=" + fieldName +
				", updateTime=" + updateTime + 
				", updateUser=" + updateUser + "]";
	}
	
}
