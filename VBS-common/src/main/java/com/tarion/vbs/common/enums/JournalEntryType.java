/* 
 * 
 * JournalEntryType.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.enums;

/**
 * Journal Entry Type enum used to mark status of Journal Entries
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-03-15
 * @version 1.0
 */
public enum JournalEntryType {
	SECURITY, RELEASE, INTEREST;
}
