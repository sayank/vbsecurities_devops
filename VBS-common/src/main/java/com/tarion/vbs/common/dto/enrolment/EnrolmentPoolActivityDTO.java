package com.tarion.vbs.common.dto.enrolment;

import java.io.Serializable;
import java.time.LocalDateTime;

public class EnrolmentPoolActivityDTO implements Serializable, Comparable<EnrolmentPoolActivityDTO> {

    private static final long serialVersionUID = 10l;
    private String vbNumber;
    private String enrolmentNumber;
    private String vbDeleteReason;
    private String enrollingVb;
    private String enrolmentType;
    private String enrolmentAddress;
    private String vbName;
    private String user;
    private LocalDateTime updateDate;

    public EnrolmentPoolActivityDTO() {
        super();
    }

    public EnrolmentPoolActivityDTO(String VBNumber, String enrolmentNumber, String vbDeleteReason, String enrollingVb,String enrolmentType,String enrolmentAddress,String vbName, String user, LocalDateTime updateDate) {
        this.vbNumber = VBNumber;
        this.enrolmentNumber = enrolmentNumber;
        this.vbDeleteReason = vbDeleteReason;
        this.enrollingVb = enrollingVb;
        this.enrolmentType = enrolmentType;
        this.enrolmentAddress = enrolmentAddress;
        this.vbName = vbName;
        this.user = user;
        this.updateDate = updateDate;
    }

    public String getVbNumber() {
        return vbNumber;
    }

    public void setVbNumber(String vbNumber) {
        this.vbNumber = vbNumber;
    }

    public String getEnrolmentNumber() {
        return enrolmentNumber;
    }

    public void setEnrolmentNumber(String enrolmentNumber) {
        this.enrolmentNumber = enrolmentNumber;
    }

    public String getVbDeleteReason() {
        return vbDeleteReason;
    }

    public void setVbDeleteReason(String vbDeleteReason) {
        this.vbDeleteReason = vbDeleteReason;
    }

    public String getEnrollingVb() {
        return enrollingVb;
    }

    public void setEnrollingVb(String enrollingVb) {
        this.enrollingVb = enrollingVb;
    }

    public String getEnrolmentType() {
        return enrolmentType;
    }

    public void setEnrolmentType(String enrolmentType) {
        this.enrolmentType = enrolmentType;
    }

    public String getEnrolmentAddress() {
        return enrolmentAddress;
    }

    public void setEnrolmentAddress(String enrolmentAddress) {
        this.enrolmentAddress = enrolmentAddress;
    }

    public String getVbName() {
        return vbName;
    }

    public void setVbName(String vbName) {
        this.vbName = vbName;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public int compareTo(EnrolmentPoolActivityDTO o) {
        int compare = o.getUpdateDate().compareTo(this.updateDate);
        if(compare == 0) {
            compare = this.enrolmentNumber.compareTo(o.enrolmentNumber);
        }
        return compare;
    }
}
