package com.tarion.vbs.common.enums;

public enum InterestStatusEnum {

    CREATED, CALCULATED, VERIFIED, SENT, COMPLETED, COMPLETE_WITH_FMS_ERROR

}