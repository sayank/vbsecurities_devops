package com.tarion.vbs.common.dto.tip;

import java.io.Serializable;
import java.util.Date;

import com.tarion.vbs.common.util.DateUtil;

public class ActivityDTO implements Serializable {

    private static final long serialVersionUID = -3827075075981364400L;

    private String systemActivityCode;
    private Long activityId;
    private Date activityDate;
    private String activityTypeName;
    private String subject;
    private String performedBy;
    private String oldValue;
    private String newValue;
    private String securityCode;

    public ActivityDTO(){}
    public ActivityDTO(Long entityId, String activityField, int revisionId, Date revisionDate, String activityName, String subject, String userId, String oldValue, String newValue){
        this.systemActivityCode = activityField;
        this.activityId = new Long(revisionId);
        //TODO check if this fixes TIP activities
//        this.activityDate = DateUtil.convertEnversRevisionDateToEasternTime(revisionDate);
        this.activityDate = revisionDate;
        this.activityTypeName = activityName;
        this.subject = subject;
        this.performedBy = userId;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.securityCode = entityId.toString();
    }

    public ActivityDTO(String subject, String oldValue, String newValue, Date revisionDate, String userId){
        this.activityDate = revisionDate;
        this.subject = subject;
        this.performedBy = userId;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public String getSystemActivityCode() {
        return systemActivityCode;
    }

    public void setSystemActivityCode(String systemActivityCode) {
        this.systemActivityCode = systemActivityCode;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Date getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(Date activityDate) {
        this.activityDate = activityDate;
    }

    public String getActivityTypeName() {
        return activityTypeName;
    }

    public void setActivityTypeName(String activityTypeName) {
        this.activityTypeName = activityTypeName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPerformedBy() {
        return performedBy;
    }

    public void setPerformedBy(String performedBy) {
        this.performedBy = performedBy;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    @Override
    public String toString() {
        return "ActivityDTO{" +
                "activityId=" + activityId +
                ", activityDate=" + activityDate +
                ", activityTypeName='" + activityTypeName + '\'' +
                ", subject='" + subject + '\'' +
                ", performedBy='" + performedBy + '\'' +
                ", oldValue='" + oldValue + '\'' +
                ", newValue='" + newValue + '\'' +
                ", securityCode='" + securityCode + '\'' +
                '}';
    }

}
