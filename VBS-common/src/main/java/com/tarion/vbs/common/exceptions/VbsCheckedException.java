package com.tarion.vbs.common.exceptions;


/***
 * Checked exception class for other classes 
 * to implement and simply situations where multiple
 * exceptions can be thrown from a single method
 * 
 * @author sallen@tarion.com
 *
 */
public class VbsCheckedException extends Exception {
	
	private static final long serialVersionUID = 4928648292987194962L;
	private final String errorCode;
    
    public VbsCheckedException() {
    	this.errorCode = null;
    }
    public VbsCheckedException(String msg) {
        super(msg);
        this.errorCode = null;
    }
    public VbsCheckedException(Throwable cause) {
        super(cause);
        this.errorCode = null;
    }
    public VbsCheckedException(String msg, String errorCode) {
        super(msg);
        this.errorCode = errorCode;
    }
    public VbsCheckedException(String msg, String errorCode, Throwable cause) {
        super(msg, cause);
        this.errorCode = errorCode;
    }
    public VbsCheckedException(String msg, Throwable cause) {
        super(msg, cause);
        this.errorCode = null;
    }

    public String getErrorCode() {
        return errorCode;
    }
    
}	