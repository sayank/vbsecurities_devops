/* 
 * 
 * ReleaseStatus.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.enums;

/**
 * Release Status enum used to mark status of Release
 *
 * @author <A href="djordje.przulj@tarion.com">Djordje Przulj</A>
 * @since 2019-02-18
 * @version 1.0
 */
public enum AlertStatus {
    ACTIVE, INACTIVE;
}
