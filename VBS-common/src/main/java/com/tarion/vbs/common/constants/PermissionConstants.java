package com.tarion.vbs.common.constants;

public class PermissionConstants {
	
	private PermissionConstants() { }
	
	public static final String P_CREATE_SECURITY = "CREATE_SECURITY";
	
	public static final String P_EDIT_SECURITY_COMMENT = "EDIT_SECURITY_COMMENT";
	
	public static final String P_CORRESPONDENCE = "CORRESPONDENCE";

	public static final String P_ADD_EDIT_INTEREST = "ADD_EDIT_INTEREST";

	public static final String P_DELETE_SECURITY_CANCEL_RELEASE = "DELETE_SECURITY_CANCEL_RELEASE";

	public static final String P_CREATE_DEMAND = "CREATE_DEMAND_FINANCE";

	public static final String P_APPROVE_RELEASE_FINAL = "APPROVE_RELEASE_FINAL";

	public static final String P_CREATE_DEMAND_FINANCE = "CREATE_DEMAND_FINANCE";

	public static final String P_LARGE_RELEASE_EMAIL = "LARGE_RELEASE_EMAIL";

	public static final String P_CREATE_EDIT_FINANCIAL_INSTITUTIONS = "CREATE_EDIT_FINANCIAL_INSTITUTIONS";

	public static final String P_PENDING_RELEASE = "PENDING_RELEASE";
	public static final String P_AUTO_RELEASE = "AUTO_RELEASE";

}