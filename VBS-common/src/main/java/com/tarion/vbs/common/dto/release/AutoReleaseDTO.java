package com.tarion.vbs.common.dto.release;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public class AutoReleaseDTO implements Serializable {

	private static final long serialVersionUID = 1291397760310682908L;
	private Long runId;
	private LocalDateTime runDate;
	private String enrolmentNumber;
	private String enrolmentAddress;
	private String timingType;
	private List<String> eliminations;
	private Long releaseSeqNum;
	private LocalDateTime releaseDate;
	private String deletedBy;

	public Long getRunId() {
		return runId;
	}

	public void setRunId(Long runId) {
		this.runId = runId;
	}

	public LocalDateTime getRunDate() {
		return runDate;
	}

	public void setRunDate(LocalDateTime runDate) {
		this.runDate = runDate;
	}

	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}

	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}

	public String getEnrolmentAddress() {
		return enrolmentAddress;
	}

	public void setEnrolmentAddress(String enrolmentAddress) {
		this.enrolmentAddress = enrolmentAddress;
	}

	public String getTimingType() {
		return timingType;
	}

	public void setTimingType(String timingType) {
		this.timingType = timingType;
	}

	public List<String> getEliminations() {
		return eliminations;
	}

	public void setEliminations(List<String> eliminations) {
		this.eliminations = eliminations;
	}

	public Long getReleaseSeqNum() {
		return releaseSeqNum;
	}

	public void setReleaseSeqNum(Long releaseSeqNum) {
		this.releaseSeqNum = releaseSeqNum;
	}

	public LocalDateTime getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(LocalDateTime releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	@Override
	public String toString() {
		return "AutoReleaseDTO{" +
				"runId=" + runId +
				", runDate=" + runDate +
				", enrolmentNumber='" + enrolmentNumber + '\'' +
				", enrolmentAddress='" + enrolmentAddress + '\'' +
				", timingType='" + timingType + '\'' +
				", eliminations=" + eliminations +
				", releaseSeqNum=" + releaseSeqNum +
				", releaseDate=" + releaseDate +
				", deletedBy='" + deletedBy + '\'' +
				'}';
	}
}
