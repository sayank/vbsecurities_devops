/* 
 * 
 * InterestRateDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.interest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tarion.vbs.common.enums.DailyInterestStatus;

/**
 * Interest Rate DTO class used as transport for Interest Rate Fields
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-12-13
 * @version 1.0
 */
public class SecurityInterestDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long version;
	private Long securityId;
	private Long interestRateId;
	private BigDecimal interestAnnualRate;
	private LocalDateTime interestDate;
	private BigDecimal dailyInterestAmount;
	private DailyInterestStatus status;

	private LocalDateTime createDate;
	private String createUser;
	private LocalDateTime updateDate;
	private String updateUser;    

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getSecurityId() {
		return securityId;
	}

	public void setSecurityId(Long securityId) {
		this.securityId = securityId;
	}

	public Long getInterestRateId() {
		return interestRateId;
	}

	public void setInterestRateId(Long interestRateId) {
		this.interestRateId = interestRateId;
	}

	public LocalDateTime getInterestDate() {
		return interestDate;
	}

	public void setInterestDate(LocalDateTime interestDate) {
		this.interestDate = interestDate;
	}

	public BigDecimal getDailyInterestAmount() {
		return dailyInterestAmount;
	}

	public void setDailyInterestAmount(BigDecimal dailyInterestAmount) {
		this.dailyInterestAmount = dailyInterestAmount;
	}

	public DailyInterestStatus getStatus() {
		return status;
	}

	public void setStatus(DailyInterestStatus status) {
		this.status = status;
	}
	
	

	public BigDecimal getInterestAnnualRate() {
		return interestAnnualRate;
	}

	public void setInterestAnnualRate(BigDecimal interestAnnualRate) {
		this.interestAnnualRate = interestAnnualRate;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SecurityInterestDTO other = (SecurityInterestDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}


	@Override
	public String toString() {
		return "SecurityInterestDTO [id=" + id + ", version=" + version + ", securityId=" + securityId + ", interestRateId=" + interestRateId + ", interestAnnualRate=" + interestAnnualRate + ", interestDate=" + interestDate
				+ ", dailyInterestAmount=" + dailyInterestAmount + ", status=" + status + ", createDate=" + createDate + ", createUser=" + createUser + ", updateDate=" + updateDate
				+ ", updateUser=" + updateUser + "]";
	}
	
}
