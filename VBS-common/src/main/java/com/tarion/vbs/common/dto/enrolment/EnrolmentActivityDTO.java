package com.tarion.vbs.common.dto.enrolment;

import java.io.Serializable;
import java.time.LocalDateTime;

public class EnrolmentActivityDTO implements Serializable {

    private static final long serialVersionUID = 6522612761253274193L;

    private LocalDateTime activityTime;
    private String fieldName;
    private String oldValue;
    private String newValue;
    private String userId;

    public EnrolmentActivityDTO(){}
    public EnrolmentActivityDTO(LocalDateTime activityTime, String fieldName, String oldValue, String newValue, String userId){
        this.activityTime = activityTime;
        this.fieldName = fieldName;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.userId = userId;
    }

    public LocalDateTime getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(LocalDateTime activityTime) {
        this.activityTime = activityTime;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
