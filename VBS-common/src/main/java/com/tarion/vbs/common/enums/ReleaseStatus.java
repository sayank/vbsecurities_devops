/* 
 * 
 * ReleaseStatus.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.enums;

/**
 * Release Status enum used to mark status of Release
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-02-18
 * @version 1.0
 */
public enum ReleaseStatus {
    INITIATED, PENDING, VOUCHER_CREATED, SENT, REJECTED, COMPLETED, CANCELLED;
}
