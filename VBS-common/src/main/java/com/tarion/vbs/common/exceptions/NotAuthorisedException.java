/* 
 * 
 * NotAuthorisedException.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.exceptions;

/**
 * NotAuthorisedException marks exception when user is not authorized
 * 
 * @author <a href="shadi.kajevand@tarion.com">Shadi Kajevand</a>
 * @since January 28, 2018
 * @version 1.0
 */
public class NotAuthorisedException extends RuntimeException {

	private static final long serialVersionUID = 1L;
    
   }
