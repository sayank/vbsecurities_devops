/* 
 * 
 * PermissionUserDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.user;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Permission User DTO class holds Permissions for user
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-01-05
 * @version 1.0
 */
public class UserPermissionDTO  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String userId;
	private LocalDateTime date;
	private String action;
	private PermissionDTO permissionDto;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public PermissionDTO getPermissionDto() {
		return permissionDto;
	}
	public void setPermissionDto(PermissionDTO permissionDto) {
		this.permissionDto = permissionDto;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

	
}
