package com.tarion.vbs.common.dto.alert;

import com.tarion.vbs.common.dto.NameDescriptionDTO;

/**
 * Alert Type DTO class used for Alter Type Lookup
 *
 * @author <A href="djordje.przulj@tarion.com">Djordje Przulj</A>
 * @since 2019-04-16
 * @version 1.0
 */
public class AlertTypeDTO extends NameDescriptionDTO {
	
	private static final long serialVersionUID = 1L;
	
	@Override
	public String toString() {
		return "AlertTypeDTO [id=" + id + ", name=" + name + ", description=" + description + "]";
	}

}
