package com.tarion.vbs.common.dto.correspondence;

import java.io.Serializable;
import java.util.List;

public class CorrespondenceWSResponse implements Serializable {

	private static final long serialVersionUID = 3891371545454493087L;

	private long releaseId;
	private String templateName;
	private List<TokenDTO> tokens;

	public long getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(long releaseId) {
		this.releaseId = releaseId;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public List<TokenDTO> getTokens() {
		return tokens;
	}

	public void setTokens(List<TokenDTO> tokens) {
		this.tokens = tokens;
	}

	@Override
	public String toString() {
		return "CorrespondenceWSResponse{" +
				"releaseId=" + releaseId +
				", templateName='" + templateName + '\'' +
				", tokens=" + tokens +
				'}';
	}
}
