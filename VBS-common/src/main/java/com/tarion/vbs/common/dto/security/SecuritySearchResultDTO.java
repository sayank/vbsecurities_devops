/* 
 * 
 * SecuritySearchDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.security;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Security Search VB DTO 
 *
 * @author Joni Paananen
 * @since 2018-10-16
 * @version 1.0
 */
public class SecuritySearchResultDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String instrumentNumber;
	private String dtaStatus;
	private String dtaAccountNumber;
	private Integer vbNumber;
	private String vbName;
	private String multiVb;
	private String jba;
	private String securityTypeName;
	private String securityPurposeName;
	private String identityTypeName;
	private BigDecimal currentAmount;
	private BigDecimal originalAmount;
	private BigDecimal currentInterestAmount;
	private BigDecimal totalAvailableAmount;
	private Integer securityNumber;
	private Integer poolId;
	private Integer maaPoaId;
	private LocalDateTime receivedDate;
	private List<SearchResultEnrolmentDTO> enrolments;
	private Boolean selected;

	public String getInstrumentNumber() {
		return instrumentNumber;
	}

	public void setInstrumentNumber(String instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}

	public String getDtaStatus() {
		return dtaStatus;
	}

	public void setDtaStatus(String dtaStatus) {
		this.dtaStatus = dtaStatus;
	}

	public String getDtaAccountNumber() {
		return dtaAccountNumber;
	}

	public void setDtaAccountNumber(String dtaAccountNumber) {
		this.dtaAccountNumber = dtaAccountNumber;
	}

	public Integer getVbNumber() {
		return vbNumber;
	}

	public void setVbNumber(Integer vbNumber) {
		this.vbNumber = vbNumber;
	}

	public String getVbName() {
		return vbName;
	}

	public void setVbName(String vbName) {
		this.vbName = vbName;
	}

	public String getMultiVb() {
		return multiVb;
	}

	public void setMultiVb(String multiVb) {
		this.multiVb = multiVb;
	}

	public String getJba() {
		return jba;
	}

	public void setJba(String jba) {
		this.jba = jba;
	}

	public String getSecurityTypeName() {
		return securityTypeName;
	}

	public void setSecurityTypeName(String securityTypeName) {
		this.securityTypeName = securityTypeName;
	}

	public String getSecurityPurposeName() {
		return securityPurposeName;
	}

	public void setSecurityPurposeName(String securityPurposeName) {
		this.securityPurposeName = securityPurposeName;
	}

	public String getIdentityTypeName() {
		return identityTypeName;
	}

	public void setIdentityTypeName(String identityTypeName) {
		this.identityTypeName = identityTypeName;
	}

	public BigDecimal getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(BigDecimal currentAmount) {
		this.currentAmount = currentAmount;
	}

	public BigDecimal getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(BigDecimal originalAmount) {
		this.originalAmount = originalAmount;
	}

	public BigDecimal getCurrentInterestAmount() {
		return currentInterestAmount;
	}

	public void setCurrentInterestAmount(BigDecimal currentInterestAmount) {
		this.currentInterestAmount = currentInterestAmount;
	}

	public BigDecimal getTotalAvailableAmount() {
		return totalAvailableAmount;
	}

	public void setTotalAvailableAmount(BigDecimal totalAvailableAmount) {
		this.totalAvailableAmount = totalAvailableAmount;
	}

	public Integer getSecurityNumber() {
		return securityNumber;
	}

	public void setSecurityNumber(Integer securityNumber) {
		this.securityNumber = securityNumber;
	}

	public Integer getPoolId() {
		return poolId;
	}

	public void setPoolId(Integer poolId) {
		this.poolId = poolId;
	}

	public Integer getMaaPoaId() {
		return maaPoaId;
	}

	public void setMaaPoaId(Integer maaPoaId) {
		this.maaPoaId = maaPoaId;
	}


	public LocalDateTime getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(LocalDateTime receivedDate) { this.receivedDate = receivedDate;}

	public List<SearchResultEnrolmentDTO> getEnrolments() {
		return enrolments;
	}

	public void setEnrolments(List<SearchResultEnrolmentDTO> enrolments) {
		this.enrolments = enrolments;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	
	public String serializeEnrolments() {
		if (this.enrolments == null || this.enrolments.isEmpty()) {
			return "";
		}

		return String.join(System.lineSeparator(), this.enrolments.stream().map(SearchResultEnrolmentDTO::toString).collect(Collectors.toList()));
	}

	@Override
	public String toString() {
		return "SecuritySearchResultDTO{" +
				"instrumentNumber='" + instrumentNumber + '\'' +
				", dtaAccountNumber='" + dtaAccountNumber + '\'' +
				", vbNumber=" + vbNumber +
				", vbName='" + vbName + '\'' +
				", multiVb='" + multiVb + '\'' +
				", jba='" + jba + '\'' +
				", securityTypeName='" + securityTypeName + '\'' +
				", securityPurposeName='" + securityPurposeName + '\'' +
				", identityTypeName='" + identityTypeName + '\'' +
				", currentAmount=" + currentAmount +
				", originalAmount=" + originalAmount +
				", currentInterestAmount=" + currentInterestAmount +
				", totalAvailableAmount=" + totalAvailableAmount +
				", securityNumber=" + securityNumber +
				", poolId=" + poolId +
				", maaPoaId=" + maaPoaId +
				", receivedDate=" + receivedDate +
				", enrolmentNumbers='" + enrolments + '\'' +
				'}';
	}
}