package com.tarion.vbs.common.enums;

public enum SearchTypeEnum {
	EQUALS, BETWEEN, LESS_THAN, GREATER_THAN, BEGIN_WITH, EQUALS_LESS_THAN, EQUALS_GREATER_THAN, IN, CONTAINS, NOT_EQUAL
}
