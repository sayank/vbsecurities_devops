/* 
 * 
 * NameDescriptionDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto;

import java.io.Serializable;

/**
 * Generic Id Name Description DTO class used as parent DTO or for convinience
 *
 * @author Joni Paananen
 */
public class NameDescriptionDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	protected Long id;
	protected String name;
	protected String description;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public NameDescriptionDTO(){}
	public NameDescriptionDTO(Long id, String name, String description){
		this.id = id;
		this.name = name;
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NameDescriptionDTO other = (NameDescriptionDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "NameDescriptionDTO [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
	
}
