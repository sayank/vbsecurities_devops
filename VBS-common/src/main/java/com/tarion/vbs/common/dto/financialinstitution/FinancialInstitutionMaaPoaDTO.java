/* 
 * 
 * FinancialInstitutionMaaPoaDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.financialinstitution;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * FinancialInstitutionMaaPoa DTO class used as transport for Financial Institution MAA or POA
 *
 * @author <A href="sbogicevic@tarion.com">Srdjan Bogicevic</A>
 * @since 2019-05-22
 * @version 1.0
 */
public class FinancialInstitutionMaaPoaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long version;
	
    private Long maaPoaSequence;
    private LocalDateTime expirationDate;
    private FinancialInstitutionDTO adminAgentBroker;
    private List<FinancialInstitutionMaaPoaInstitutionDTO> maaPoaInstitutions;
	private List<FinancialInstitutionSigningOfficerDTO> signingOfficers;
	private boolean deleted;
	private LocalDateTime createDate;
	private String createUser;
	private LocalDateTime updateDate;
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getMaaPoaSequence() {
		return maaPoaSequence;
	}

	public void setMaaPoaSequence(Long maaPoaSequence) {
		this.maaPoaSequence = maaPoaSequence;
	}

	public LocalDateTime getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDateTime expirationDate) {
		this.expirationDate = expirationDate;
	}

	public FinancialInstitutionDTO getAdminAgentBroker() {
		return adminAgentBroker;
	}

	public void setAdminAgentBroker(FinancialInstitutionDTO adminAgentBroker) {
		this.adminAgentBroker = adminAgentBroker;
	}

	public List<FinancialInstitutionMaaPoaInstitutionDTO> getMaaPoaInstitutions() {
		return maaPoaInstitutions;
	}

	public void setMaaPoaInstitutions(List<FinancialInstitutionMaaPoaInstitutionDTO> maaPoaInstitutions) {
		this.maaPoaInstitutions = maaPoaInstitutions;
	}

	public List<FinancialInstitutionSigningOfficerDTO> getSigningOfficers() {
		return signingOfficers;
	}

	public void setSigningOfficers(List<FinancialInstitutionSigningOfficerDTO> signingOfficers) {
		this.signingOfficers = signingOfficers;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		FinancialInstitutionMaaPoaDTO that = (FinancialInstitutionMaaPoaDTO) o;
		return deleted == that.deleted &&
				Objects.equals(id, that.id) &&
				Objects.equals(version, that.version) &&
				Objects.equals(maaPoaSequence, that.maaPoaSequence) &&
				Objects.equals(expirationDate, that.expirationDate) &&
				Objects.equals(adminAgentBroker, that.adminAgentBroker) &&
				Objects.equals(maaPoaInstitutions, that.maaPoaInstitutions) &&
				Objects.equals(signingOfficers, that.signingOfficers) &&
				Objects.equals(createDate, that.createDate) &&
				Objects.equals(createUser, that.createUser) &&
				Objects.equals(updateDate, that.updateDate) &&
				Objects.equals(updateUser, that.updateUser);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, version, maaPoaSequence, expirationDate, adminAgentBroker, maaPoaInstitutions, signingOfficers, deleted, createDate, createUser, updateDate, updateUser);
	}

	@Override
	public String toString() {
		return "FinancialInstitutionMaaPoaDTO{" +
				"id=" + id +
				", version=" + version +
				", maaPoaSequence=" + maaPoaSequence +
				", expirationDate=" + expirationDate +
				", adminAgentBroker=" + adminAgentBroker +
				", maaPoaInstitutions=" + maaPoaInstitutions +
				", signingOfficers=" + signingOfficers +
				", deleted=" + deleted +
				", createDate=" + createDate +
				", createUser='" + createUser + '\'' +
				", updateDate=" + updateDate +
				", updateUser='" + updateUser + '\'' +
				'}';
	}

}
