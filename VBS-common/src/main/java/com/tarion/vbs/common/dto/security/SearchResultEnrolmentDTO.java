package com.tarion.vbs.common.dto.security;

import java.io.Serializable;

public class SearchResultEnrolmentDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private String enrolmentNumber;
    private String date;

    public String getEnrolmentNumber() {
        return enrolmentNumber;
    }

    public void setEnrolmentNumber(String enrolmentNumber) {
        this.enrolmentNumber = enrolmentNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    @Override
    public String toString() {
    	return this.enrolmentNumber + " (" + this.date + ")";
    }
}
