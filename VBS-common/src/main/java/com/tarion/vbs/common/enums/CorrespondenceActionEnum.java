package com.tarion.vbs.common.enums;

public enum CorrespondenceActionEnum {

    EXCESS_CASH_DEPOSIT_VB,
    EXCESS_LC_DEPOSIT_VB,
    FULL_CASH_RELEASE_VB,
    FULL_LC_RELEASE_BANK,
    FULL_LC_RELEASE_VB,
    PARTIAL_CASH_RELEASE_VB,
    PARTIAL_LC_RELEASE_BANK,
    PARTIAL_LC_RELEASE_VB

}
