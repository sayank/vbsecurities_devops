package com.tarion.vbs.common.dto.enrolment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PoolEnrolments implements Serializable {

    private static final long serialVersionUID = -5797317296082083225L;

    private List<String> enrolmentsInPool;

    public List<String> getEnrolmentsInPool() {
        if(enrolmentsInPool == null) {
            enrolmentsInPool = new ArrayList<>();
        }
        return enrolmentsInPool;
    }

    public void setEnrolmentsInPool(List<String> enrolmentsInPool) {
        this.enrolmentsInPool = enrolmentsInPool;
    }

    @Override
    public String toString() {
        return "PoolEnrolments{" +
                "enrolmentsInPool=" + enrolmentsInPool +
                '}';
    }
}
