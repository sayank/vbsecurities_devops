package com.tarion.vbs.common.constants;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Constants for VB Securities
 *
 * @author Joni Paananen
 */
public class VbsConstants {
	
	private VbsConstants() {}
	
	public static final String FINANCIAL_INSTITUTION_ZERO = "0";
	public static final long FINANCIAL_INSTITUTION_CIBC_ID = 22L;
	public static final long FINANCIAL_INSTITUTION_TYPE_SURETY_COMPANIES = 6L;
	public static final long FINANCIAL_INSTITUTION_TYPE_ADMINISTRATIVE_AGENT = 8L;
	public static final long FINANCIAL_INSTITUTION_TYPE_ADMINISTRATIVE_BROKER = 9L;
	public static final String EXCEL_MIME_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	public static final String EXCEL_FILE_EXTENSION = ".xlsx";

	
	// Interest Constants
	public static final int DECIMAL_SCALE = 2;
	public static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_EVEN;
	public static final BigDecimal DAYS_IN_YEAR = new BigDecimal("365.0000");
	public static final BigDecimal BIGDECIMAL_HUNDRED = new BigDecimal("100.0000");
	public static final String DAYS_IN_THE_PAST_TO_CALCULATE_INTEREST = "vbs.interest.backdays";
	public static final String EARLIEST_DATE_TO_CALCULATE_INTEREST = "vbs.interest.earliest.date";
	public static final String CURRENCY_CANADIAN = "CAD";
	public static final BigDecimal DISCOUNT_AMOUNT_ZERO = BigDecimal.ZERO;
	public static final BigDecimal SEND_RELEASE_EMAIL_AMOUNT = new BigDecimal(100000);
	
	// Email constants
	public static final int EMAIL_MAX_ERROR_TEXT = 1024;
	public static final String SENT_SUCCESS = "SUCCESS";
	public static final String SENT_FAILURE = "FAILURE";
	public static final String EMAIL_NO_REPLY_ADDRESS = "no-reply@tarion.com";

	public static final String CRM_WS_OK = "OK";
	public static final String CRM_WS_PASS = "PASS";
	public static final String FMS_JMS_OK = "OK";

	public static final String CRM_JMS_STATUS_OK = "P";
	public static final String CRM_JMS_FAILURE = "F";
	
	public static final String VP_TO_APPROVE_RELEASE = "KBRODIE";
	public static final String CRM_FINANCE_WORKLIST_USER = "Finance-Treasury";
	public static final String CRM_FINANCE_WORKLIST_BUSINESS_PROCESS = "TWC_SEC_RELEASE_BP";
	public static final String CRM_FINANCE_WORKLIST_ACTIVITY_NAME = "TWC_SEC_RELEASE";
	public static final String CRM_FINANCE_WORKLIST_EVENT_NAME = "Worklist";
	public static final String CRM_FINANCE_WORKLIST_WL_NAME = "Security Release Worklist";
	public static final String CRM_FINANCE_WORKLIST_WORKLIST_PURPOSE = "UPD";
	public static final String CRM_FINANCE_WORKLIST_WORKLIST_PRIORITY = "2";
	public static final String CRM_FINANCE_WORKLIST_WORKLIST_DESCR = "Security Release";
	public static final String CRM_FINANCE_WORKLIST_WORKLIST_NOTES = "Security Release";


	// wsdl url constants
	public static final String CRM_VBS_WEB_SERVICE_WSDL_URL = "crm.ws.vbs.client.wsdl.url";
	public static final String CRM_VBA_WEB_SERVICE_WSDL_URL = "crm.ws.vba.client.wsdl.url";
	public static final String CRM_CONTACT_WEB_SERVICE_WSDL_URL = "crm.ws.contact.client.wsdl.url";
	public static final String CRM_WORKLIST_WEB_SERVICE_URL = "crm.ws.worklist.client.wsdl.url";
	public static final String ESP_EMAIL_WEB_SERVICE_WSDL_URL = "esp.ws.client.wsdl.url";
	public static final String VBA_WEB_SERVICE_WSDL_URL = "vba.ws.client.wsdl.url";
	public static final String VBA_VBS_WEB_SERVICE_WSDL_URL = "vba.vbs.ws.client.wsdl.url";
	public static final int TYPEAHEAD_MAX_RESULTS = 10;
	public static final int SECURITY_SEARCH_MAX_RESULTS = 5000;
	public static final int SECURITY_SEARCH_RECENT_MAX_RESULTS = 50;
	
	public static final String FAILED_TO_COPY_PROPERTIES = "Failed to copy properties";
	
	// FMS constants
	public static final String FMS_CASH_SECURITY = "CAS";
	public static final String FMS_DTA_SECURITY = "DTA";
	public static final String FMS_CASH_SECURITY_INTEREST = "INT";
	
	public static final String FMS_RELEASE_VOUCHER_CREATED = "C";
	public static final String FMS_RELEASE_PAYMENT_CREATED = "P";
	public static final String FMS_RELEASE_FAILURE = "F";
	
	
	// Security Type Constants
	public static final long SECURITY_TYPE_CASH = 1l;
	public static final long SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT = 3L;
	public static final long SECURITY_TYPE_LETTER_OF_CREDIT = 4l;
	public static final long SECURITY_TYPE_PRE_EXISTING_ELEMENTS_FUND = 5l;
	public static final long SECURITY_TYPE_SURETY_BOND = 6l;

	//Security Purpose Constants
	public static final long SECURITY_PURPOSE_DEPOSIT_AND_WARRANTY = 1l;
	public static final long SECURITY_PURPOSE_BLANKET_SECURITY = 2l;
	public static final long SECURITY_PURPOSE_CONDO_CONVERSION = 3l;
	public static final long SECURITY_PURPOSE_DEPOSIT = 4l;
	public static final long SECURITY_PURPOSE_WARRANTY = 5l;

	//Identity Type Constants
	public static final long IDENTITY_TYPE_CONDO = 1l;
	public static final long IDENTITY_TYPE_FREEHOLD = 2l;
	public static final long IDENTITY_TYPE_BOTH = 3l;

	//Config Table Contansts
	public static final String ACTIVE_DIRECTORY_DOMAIN_CONTEXT = "ad.domain.context";
	public static final String ACTIVE_DIRECTORY_SEARCH = "ad.domain.search";
	
	public static final String VBS_SYSTEM_USER_ID = "vbssystemuser";
	public static final String VBS_CRM_SYSTEM_USER_ID = "vbscrmsystemuser";
	public static final String VBS_FMS_SYSTEM_USER_ID = "vbsfmssystemuser";
	public static final String VBS_BSA_SYSTEM_USER_ID = "vbsbsasystemuser";
	
	public static final String PROPERTIES_FILE = "vbs.properties"; 

	public static final String TRUE = "TRUE";
	public static final String FALSE = "FALSE";
	public static final String Y = "Y";
	public static final String N = "N";
	
	public static final String ACTIVE_DIRECTORY_JNDI = "ldap/bsaAd";

	// Date Format constants
    public static final String CRM_DATE_FORMAT = "yyyy-MM-dd"; // 2018-06-20
    public static final String CRM_DATE_TIME_FORMAT = "yyyy-MM-dd-HH:mm:ss"; //"yyyy-MM-dd-HH.mm.ss.nnnnnn"; //2018-06-20-15.04.26.000000
    public static final String CRM_DATE_TIME_NANO_FORMAT = "yyyy-MM-dd-HH.mm.ss.nnnnnn"; //"yyyy-MM-dd-HH.mm.ss.nnnnnn"; //2018-06-20-15.04.26.000000
	public static final String BEACON_FILE_DATE_FORMAT = "yyyy/MM/dd HH:mm:ss"; // 20140304032149
	public static final String BHDR_FILE_DATE_FORMAT = "yyyyMMddHHmmss"; // 20140304032149
	public static final String FILE_DATE_FORMAT = "yyMMdd"; // 140304
    public static final String FMS_DATE_FORMAT = "yyyy-MM-dd"; // 2018-06-20
    
    //URLS
    public static final String CRM_COMPANY_URL = "crm.vb.url";
    public static final String CRM_ENROLMENT_URL = "crm.enrolment.url";
    public static final String CRM_WARRANTY_SERVICE_ORDER_URL = "crm.service.order.url";
    public static final String TRANSACTION_HISTORY_URL = "transaction_history.url.base";
    public static final String FINANCIAL_TRANSACTION_URL = "financial_transaction.url.base";
    public static final String CASH_SECURITY_REFUNDS_URL = "cash.security.refunds.url";
    public static final String WITHDRAW_URL = "dta.withdrawal.url";
    public static final String CONTENT_NAVIGATOR_URL = "cn.url.base";
    public static final String FMS_FIND_ADD_CHEQUE_URL = "find.add.cheque.url";
    public static final String FMS_ALLOCATE_CHEQUE_URL = "allocate.cheque.url";
    public static final String CE_VIEWER_URL = "ce.viewer.url";
    public static final String VIEWER_360_URL = "360.view.url";
    public static final String CRM_CREATE_COMPANY_URL = "create.company.url";
    public static final String CONTENT_NAVIGATOR_FI_DOCUMENTS_URL = "cn.fi.url";
	public static final String IA_ACTIVITIES_URL = "ia.activities.url";
	public static final String APCSD_VOUCHER_URL = "apcsd.voucher.url";
    public static final String CONTENT_NAVIGATOR_SECURITY_RELEASE_URL = "security.release.cn.url";
    public static final String VBS_BASE_URL = "vbs.base.url";
    public static final String CRM_FLAG_URL = "security.red.flag.url";
    public static final String CRM_YELLOW_STICKY_URL = "security.yellow.sticky.url";

    
    //Emails
    public static final String LARGE_RELEASE_EMAIL = "large.release.email";
    public static final String LARGE_RELEASE_EMAIL_SUBJECT = "large.release.email.subject";
	public static final String AUTO_RELEASE_FIRST_STEP_EMAIL = "autorelease.first.step.email";
	public static final String AUTO_RELEASE_FIRST_STEP_EMAIL_SUBJECT = "autorelease.first.step.email.subject";
	public static final String AUTO_RELEASE_FINAL_STEP_EMAIL = "autorelease.final.step.email";
	public static final String AUTO_RELEASE_FINAL_STEP_EMAIL_SUBJECT = "autorelease.final.step.email.subject";
	public static final String DTA_EMAIL = "dta.email";
	public static final String DTA_SUBJECT = "dta.email.subject";
	public static final String DTA_FAILED_EMAIL = "dta.email.lu.failed";
	public static final String DTA_FAILED_SUBJECT = "dta.email.subject.lu.failed";

    //RegExp
    public static final String CASH_INSTRUMENT_NUMBER_REGEXP = "^[A-Z]+ [0-9]+[A-Z]?$";
    public static final String CASH_INSTRUMENT_NUMBER_DD_REGEXP = "^DD[0-9]+$";
    
    //permissions constants
    public static final String MANAGER_ROLE_NAME = "MANAGEMENT";
    public static final String ANALYST_ROLE_NAME = "ANALYST";
    public static final String FINAL_APPROVAL_DEFAULT = "final.approval.manager.default";
    public static final String FINAL_APPROVAL_MANAGER_RELEASE_HIGH = "final.approval.manager.release.high";
	public static final String CREATE_PERSON_CONTACT_LINK = "crm.create.contact.url";
	public static final String ALL_SECURITIES_UNDER_VB = "security.release.all.securities.under.vb";
	public static final String SUMMARY_BY_SECURITY_URL = "security.release.summary.all";

	//Contact type


    //Contact type
	public static final long CONTACT_TYPE_PERSON = 1l;
	public static final long CONTACT_TYPE_COMPANY = 2l;
    public static final long CONTACT_TYPE_ESCROW_AGENT = 19l;
    public static final long CONTACT_TYPE_VB= 16l;
	//Contact
	public static final String CONTACT_TARION_ID= "12297617";

	//contact security role type
    public static final long CONTACT_SECURITY_ESCROW = 1l;
    public static final long CONTACT_SECURITY_LAWYER = 2l;
    public static final long CONTACT_SECURITY_ASSISTANT = 3l;
    public static final long CONTACT_SECURITY_THIRD_PARTY = 4l;

	//relationship types
	public static final long RELATIONSHIP_TYPE_LAWYER = 20166l;
	public static final long RELATIONSHIP_TYPE_ASSISTANT = 20167l;
	public static final long RELATIONSHIP_TYPE_UMBRELLA = 20062l;
	
	//Enrolment types
	public static final String ENROLMENT_UNIT_TYPE_CE = "001";
	public static final String ENROLMENT_UNIT_TYPE_FH = "002";
	public static final String ENROLMENT_UNIT_TYPE_CE_UNIT = "003";

	//Release Types
	public static final long RELEASE_TYPE_DEMAND_COLLECTOR = 1l;
	public static final long RELEASE_TYPE_DEMAND_LU = 2l;
	public static final long RELEASE_TYPE_RELEASE = 3l;
	public static final long RELEASE_TYPE_REPLACE = 4l;

	public static final String CREATE_DEMAND_FINANCE = "CREATE_DEMAND_FINANCE";

	//Auto release timing types
	public static final long AUTO_RELEASE_TIMING_TYPE_EXCESS_DEPOSIT = 1l;
	public static final long AUTO_RELEASE_TIMING_TYPE_ONE_YEAR = 2l;
	public static final long AUTO_RELEASE_TIMING_TYPE_RFC = 3l;
	public static final long AUTO_RELEASE_TIMING_TYPE_TWO_YEAR = 4l;

	//Auto Release run status
	public static final long AUTO_RELEASE_RUN_STATUS_INIT_PROCESSING = 1l;
	public static final long AUTO_RELEASE_RUN_STATUS_INIT_PROCESSING_CRITERIA = 2l;
	public static final long AUTO_RELEASE_RUN_STATUS_INIT_COMPLETE = 3l;
	public static final long AUTO_RELEASE_RUN_STATUS_FINAL_PROCESSING = 4l;
	public static final long AUTO_RELEASE_RUN_STATUS_FINAL_COMPLETE = 5l;

	//Auto Release enrolment status
	public static final long AUTO_RELEASE_ENROLMENT_STATUS_PROCESSING = 1l;
	public static final long AUTO_RELEASE_ENROLMENT_STATUS_INIT_COMPLETE = 2l;
	public static final long AUTO_RELEASE_ENROLMENT_STATUS_RELEASED = 3l;

	//Security Status Constants
	public static final long SECURITY_STATUS_PENDING_WITH_OS_FEE = 1l;
	public static final long SECURITY_STATUS_PENDING_WITH_FEE = 2l;
	public static final long SECURITY_STATUS_WITHDRAWN = 3l;
	public static final long SECURITY_STATUS_ACCEPTED = 4l;
	public static final long SECURITY_STATUS_CANCELLED = 5l;
	public static final long SECURITY_STATUS_REPLACED = 6l;
	public static final long SECURITY_STATUS_TERMINATED = 7l;
	public static final long SECURITY_STATUS_OTHER = 9l;

	//Release Reasons
	public static final long AUTO_RELEASE_REASON = 2l;
	public static final long RELEASE_DTA_MERGE = 22l;
	public static final long RELEASE_DTA_SPLIT = 21l;
	public static final long RELEASE_DTA_END_OF_PROJECT = 23l;
	public static final long RELEASE_CANCEL_PROJ_ENRO = 5l;
	public static final long RELEASE_DTA_REPLACED = 13l;

	//Reject Reasons
	public static final long REJECT_REASON_CANCEL_REVERSE_PAYMENT = 6l;

	//License Application Status
	public static final long LICENCE_APPLICATION_STATUS_PEF = 6l;

	public static final long LICENCE_STATUS_ACTIVE = 9l;

	//Alert type constants
	public static final long ALERT_TYPE_NOTE = 4l;

	public static final long HOME_CATEGORY_FREEHOLD = 1l;

	//Datamart Security Release Report
	public static final String DM_SECURITY_RELEASE_BASE = "dm.release.report.base";
	public static final String DM_SECURITY_RELEASE_ONE_YEAR = "dm.release.report.oneYear";
	public static final String DM_SECURITY_RELEASE_TWO_YEAR = "dm.release.report.twoYear";
	public static final String DM_SECURITY_RELEASE_THREE_YEAR = "dm.release.report.threeYear";
	public static final String DM_SECURITY_RELEASE_FOUR_SEVEN_YEAR = "dm.release.report.fourSevenYear";
	public static final String DM_SECURITY_RELEASE_ALL = "dm.release.report.all";

	public static final String FMS_CHEQUE_STATUS_CANCELLED = "C";
	public static final String SECURITY_GROUP_EMAIL = "security.group.email";

	public static final String CE_BLANKET_EMAIL_SUBJECT = "ce.blanket.email.subject";
	public static final String CE_BLANKET_EMAIL_BODY = "ce.blanket.email.body";

}
