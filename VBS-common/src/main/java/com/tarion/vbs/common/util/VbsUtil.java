/* 
 * 
 * VBApplicationsProcessingUtil.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.util;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.tarion.vbs.common.dto.security.SecuritySearchParametersDTO;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.exceptions.VbsJAXBConversionException;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;


/**
 * Utility class with utility static methods for VB Applications
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @date 2018-01-05
 * @version 1.0
 */
public class VbsUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(VbsUtil.class);
    private static final String B_FOR_BUILDER = "B";
	private static final String H_FOR_ENROLMENT = "H";
    private static Map<String, String> extensionMap;
    
    
    
    private VbsUtil() {}
    
    public static boolean getBooleanPrimitiveFromString(String crmBoolean) {
    	return (crmBoolean != null && (VbsConstants.TRUE.equalsIgnoreCase(crmBoolean) || VbsConstants.Y.equalsIgnoreCase(crmBoolean)));
    }
    
    public static String getStringForMoney(BigDecimal money) {
    	return (money != null) ? money.toString() : "";
    }
    
    public static BigDecimal parseMoney(String money) {
    	if(StringUtils.isEmpty(money)) {
    		return null;
    	}
    	money = money.replace("$", "");
    	money = money.replace(",", "");
    	return new BigDecimal(money);
    }

	public static Long parseLong(String value) {
		if(!NumberUtils.isParsable(value)) {
			return null;
		}
		return Long.parseLong(value);
	}


	public static boolean isSame(BigDecimal oldValue, BigDecimal newValue) {
    	boolean same = false;
    	if (oldValue == null && newValue == null) {
    		same = true;
    	}
    	if (oldValue != null && newValue != null && oldValue.compareTo(newValue) == 0) {
    		same = true;
    	}
    	return same;
    }

    public static UserDTO getSystemUserDTO() {
    	UserDTO systemUserDTO = new UserDTO();
    	systemUserDTO.setUserId(VbsConstants.VBS_SYSTEM_USER_ID);
    	
    	return systemUserDTO;
    }
    
    public static String getSearchLikeString(String value) {
    	if (value != null ) {
    		return "%" + value + "%";
    	}
    	return null;
    }
    
    public static String getSearchLikePrefixString(String value) {
    	if (value != null ) {
    		return value + "%";
    	}
    	return null;
    }

    public static String getSearchLikePrefixString(Long value) {
    	if (value != null ) {
    		return value + "%";
    	}
    	return null;
    }

    @SuppressWarnings("unchecked")
	public static <T> T convertXmlToJaxbClass(String xmlResponse, Class<T> clazz) {
		T response = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader stringReader = new StringReader(xmlResponse);
			response = (T) unmarshaller.unmarshal(stringReader);
		} catch (JAXBException e) {
			throw new VbsJAXBConversionException("Error converting xml to JAXB of type: " + clazz, e);
		}

		return response;
	}
    
    @SuppressWarnings("unchecked")
	public static Object convertXmltoJaxb(String xmlResponse) {
		try {
			String rootElement = "";
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

			dbf.setValidating(false);
			dbf.setNamespaceAware(true);
			dbf.setFeature("http://xml.org/sax/features/namespaces", false);
			dbf.setFeature("http://xml.org/sax/features/validation", false);
			dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
          
			DocumentBuilder builder = dbf.newDocumentBuilder();
			rootElement = builder.parse(new InputSource(new StringReader(xmlResponse))).getDocumentElement()
					.getNodeName();
			Field f = ClassLoader.class.getDeclaredField("classes");
			f.setAccessible(true);
			ClassLoader classLoader = VbsUtil.class.getClassLoader();
			List<Class<?>> classes = ((Vector<Class<?>>) f.get(classLoader)).stream().collect(Collectors.toList());
			for (Class<?> c1 : classes) {
				XmlRootElement[] annotations = c1.getAnnotationsByType(XmlRootElement.class);
				if(annotations.length == 1 && annotations[0].name().equals(rootElement)) {
					return VbsUtil.convertXmlToJaxbClass(xmlResponse, c1);
				}
			}
			throw new VbsJAXBConversionException("Unknown Class for given xml.");
		} catch (ParserConfigurationException | IOException | SAXException xmlException) {
			throw new VbsJAXBConversionException("An error occurred processing the xml.");
		} catch (IllegalAccessException | NoSuchFieldException | SecurityException classLoaderException) {
			throw new VbsJAXBConversionException(
					"A classloader related error occured when attempting xml conversion.");
		}
	}

	public static <T> String convertJaxbToXml(T jaxb, Class<T> clazz) {
		StringWriter stringWriter = new StringWriter();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(jaxb, stringWriter);
		} catch (JAXBException e) {
			throw new VbsJAXBConversionException("Error converting JAXB to xml ", e);
		}

		return stringWriter.toString();
	}

    /**
     * Check if the string is null or empty
     * 
     * @param aString
     *            String to test
     * @return true= null or empty, false otherwise
     */
    public static boolean isNullorEmpty(String aString) {
        boolean result = true;
        if (aString != null && aString.trim().length() > 0) {
            result = false;
        }
        return result;
    }
    
    /**
     * Check if the collection is null or empty
     * 
     * @param collection
     *            Collection to test
     * @return true= null or empty, false otherwise
     */
    public static boolean isNullorEmpty(final Collection<?> collection) {
    	return collection == null || collection.isEmpty();
    }    

    /**
     * Remove trailing spaces, if null, return null
     * 
     * @param aString
     *            String to test
     * @return aString without spaces, or null if it's null
     */
    public static String removeTrailingSpacesWithNull(String aString) {
        return aString == null ? null : aString.trim();
    }

    /**
     * Remove the prefix <code>H</code> from the enrolment number
     * 
     * @param oldEnrolmentNumber
     *            Enrolment number
     * @return Enrolment number without the leading 'H'
     */
    public static String removeEnrolmentStartingH(String oldEnrolmentNumber) {
        String enrolmentWithoutH = oldEnrolmentNumber;

        if (oldEnrolmentNumber != null && (oldEnrolmentNumber.startsWith("H") || oldEnrolmentNumber.startsWith("h"))) {
            enrolmentWithoutH = oldEnrolmentNumber.substring(1);
        }
        return enrolmentWithoutH;
    }
    
	/**
	 * Removes the b from vb.
	 * 
	 * @param vbNumbder
	 *            the vb numbder
	 * @return the string
	 */
	public static String removeBFromVB(String vbNumber) {
		if (vbNumber != null && vbNumber.length() > 1 && (B_FOR_BUILDER.equalsIgnoreCase(vbNumber.substring(0, 1)))) {
			return vbNumber.toUpperCase().substring(1);
		}
		return vbNumber;
	}

	public static String removeHFromEnrolment(String enrolmentNumber) {
		if (enrolmentNumber != null && enrolmentNumber.length() > 1 && (H_FOR_ENROLMENT.equalsIgnoreCase(enrolmentNumber.substring(0, 1)))) {
			return enrolmentNumber.toUpperCase().substring(1);
		}
		return enrolmentNumber;
	}
	
	/**
	 * Adds the b to vb.
	 * 
	 * @param vbNum
	 *            the vb num
	 * @return the string
	 */
	public static String addBToVb(String vbNum) {
		if (vbNum != null && vbNum.length() > 1 && (B_FOR_BUILDER.equalsIgnoreCase(vbNum.substring(0, 1)))) {
			return vbNum.toUpperCase();
		} else if (vbNum == null) {
			return vbNum;
		}
		return B_FOR_BUILDER + vbNum;
	}

	public static String toUpperCase(String name) {
		if (name != null) {
			return name.toUpperCase();
		}
		return null;
	}

	public static String addBToVb(Integer vbNum) {
		return addBToVb(vbNum.toString());
	}


    
    public static String getFormatedPhoneNumber(String number){
        String numberWithNoTrailingSpaces = removeTrailingSpacesWithNull(number);
        String formatedNumber = "";
        if(numberWithNoTrailingSpaces != null) {
	        String[] numbers = numberWithNoTrailingSpaces.split("/");
	        
	        if(numbers.length == 1){
	            return numberWithNoTrailingSpaces;
	        }
	        String areaCode = "(" + numbers[0] + ")";
	        String phoneNumber = numbers[1];
	        
	        formatedNumber = areaCode + phoneNumber;
        }
        return formatedNumber;
    }
    
    /**
     * Close the jdbc connections
     * 
     * @param rs
     *            Result set
     * @param ps
     *            prepared statement
     * @param con
     *            Connection
     */
    public static void closeConnections(ResultSet rs, PreparedStatement ps, Connection con) {
        if (rs != null) {
            try {
                rs.close();
            } catch (Exception e) {
            	//
            }
        }
        if (ps != null) {
            try {
                ps.close();
            } catch (Exception e) {
            	//
            }
        }
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                // problem during close, don't write out exception.
            }
        }
    }
    
	public static String getVbNumWithoutB(String oldVbNum) {
		String vbNumWithoutB = oldVbNum;

        if (oldVbNum != null && (oldVbNum.startsWith("B") || oldVbNum.startsWith("b"))) {
        	vbNumWithoutB = oldVbNum.substring(1);
        }
        return vbNumWithoutB;
	}
    
    /**
     * A Simple method that removes the H or B from a number if it exists
     * 
     * @param enrolNum
     * @return The enrolment number passed without an H or B
     */
    public static String stripHOrBFromNumber(String enrolNum) {
        if (enrolNum != null
                && enrolNum.length() > 1
                && ("h".equalsIgnoreCase(enrolNum.substring(0, 1)) || "b".equalsIgnoreCase(enrolNum
                        .substring(0, 1)))) {
            return enrolNum.substring(1);
        }
        return enrolNum;
    }
 
	public static String addHtoEnrolmentId(String enrolmentId){
		if (enrolmentId != null
                && enrolmentId.length() > 1
                && !("h".equalsIgnoreCase(enrolmentId.substring(0, 1)))) {
            return "H" + enrolmentId;
        }
		return enrolmentId;
	}

    public static Object unMarshallMsg(String msg, Object object) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(object.getClass());
        Unmarshaller u = jc.createUnmarshaller();
        return u.unmarshal(new StringReader(msg));
    }

    public static String marshalMessage(Object object) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(object.getClass());
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter writer = new StringWriter();
        m.marshal(object, writer);
        return writer.toString();
    }

	public static String maskCreditCard(String creditCardNumber){
	    StringBuilder maskedNumber = new StringBuilder();
	    int totalLen = creditCardNumber.length();
	    
	    for(int i = 0; i < totalLen - 4; i++){
	        maskedNumber.append("x");
	    }
	    maskedNumber.append(creditCardNumber.substring(totalLen - 4));
	    return maskedNumber.toString();
	}

	/**
	 * Trims string to the desired size in bytes. 
	 * 
	 * This is needed due to CM limitation on size of string in bytes. 
	 * 
	 * See http://jira.tarion.com:8090/display/KB/Fix+the+issue+in+PIT+admin+for+Load+CM+exception+for+confirmation+number
	 * and  http://www-01.ibm.com/support/docview.wss?uid=swg21618627 for details.
	 * 
	 */
	public static String trimToByteSize(String s, int len) {
		if (s == null){
			return null;
		}
		if (len > 200 || len <= 0){
			throw new IllegalArgumentException("The lenght must be greater than 0 and smaller than 200: " + len);
		}
		
		int bytesLen = 0;
		try{
			bytesLen = s.getBytes("UTF-8").length;
		} catch (UnsupportedEncodingException e){
			LoggerUtil.logError(logger, "Error in trimToByteSize for input: " + s, e);
			bytesLen = s.getBytes().length;
		}
		
		if (bytesLen <= len){
			logger.debug("TIPUtil.trimToByteSize - EXIT: {}", s);
			return s;
		} else {
			return trimToByteSize(StringUtils.chop(s), len);	
		}				
		
	}
	
	public static String getFileExtension(String filename, String mimeType) {
    	String[] suffixArray = getFileExtensionMap().get(mimeType).split(" ");
    	
    	for (int i = 0; i < suffixArray.length; i++) {
			if(filename.toLowerCase().endsWith("." + suffixArray[i])) {
				return null;
			}
		}
    		
    	return suffixArray[0];
    }
	
	public static String getFileExtension(String mimeType) {
    	String[] suffixArray = getFileExtensionMap().get(mimeType).split(" ");
    	if(suffixArray == null){
    		return null;
    	}
    		
    	return suffixArray[0];
    }
	
	public static void copyProperties(Object orig, Object dest) {
		try {
			BeanUtils.copyProperties(dest, orig);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new VbsRuntimeException("Failed to copy properties", e); 
		}
	}
	
	public static String getCurrencyFormattedString(BigDecimal number) {
		if (number != null) {
			NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.CANADA);
			nf.setMinimumFractionDigits(2);
			return nf.format(number);
		} else {
			return "";
		}
	}
	
	public static boolean equals(Long l1, Long l2) {
		if(l1 == null && l2 == null) {
			return true;
		}
		if(l1 == null || l2 == null) {
			return false;
		}
		return l1.equals(l2);
	}
	
	public static boolean isCRMTrue(String s) {
		return "Y".equalsIgnoreCase(s);
	}
	
	public static Boolean isCRMTrueFalseNull(String s) {
		if (VbsUtil.isNullorEmpty(s)) {
			return null;
		}
		return "Y".equalsIgnoreCase(s);
	}
	
	private static Map<String, String> getFileExtensionMap(){
		if(extensionMap == null) {
			extensionMap = new HashMap<>();
			
			extensionMap.put("application/octet-stream", "bin");
			extensionMap.put("image/bmp", "bmp dib");
			extensionMap.put("image/g3fax", "g3f");
			extensionMap.put("image/gif", "gif");
			extensionMap.put("image/jpeg", "jpeg jpg jpe jfif pjpeg pjp");
			extensionMap.put("image/tiff", "tif tiff");
			extensionMap.put("image/x-dcx", "dcx");
			extensionMap.put("image/x-pcx", "pcx");
			extensionMap.put("text/html", "html htm shtml plg");
			extensionMap.put("text/plain", "txt text");
			extensionMap.put("text/xml", "xml");
			extensionMap.put("application/dca-rft", "rtf");
			extensionMap.put("application/msword", "doc dot rtf");
			extensionMap.put("application/pdf", "pdf");
			extensionMap.put("text/rtf", "rtf");
			extensionMap.put("application/rtf", "rtf");
			extensionMap.put("application/vnd.framemaker", "fm frm");
			extensionMap.put("application/afp", "afp");
			extensionMap.put("application/vnd.ibm.modcap", "mda");
			extensionMap.put("application/vnd.lotus-1-2-3", "123 wk4 wk3 wk1 wks wg1");
			extensionMap.put("application/vnd.lotus-freelance", "prz pre");
			extensionMap.put("application/vnd.lotus-wordpro", "lwp sam mwp smm");
			extensionMap.put("application/vnd.ms-excel", "xls xlt xlm xld xla xlc xlw xll");
			extensionMap.put("application/vnd.ms-powerpoint", "ppt pot ppa pps pwz");
			extensionMap.put("application/vnd.visio", "vsd");
			extensionMap.put("application/wordperfect5.1", "wp wpd w51");
			extensionMap.put("image/tiff1stp", "tif tiff");
			extensionMap.put("application/vnd.openxmlformats-officedocument.wordprocessingml.document", "docx");
			extensionMap.put("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "xlsx");
			extensionMap.put("application/vnd.ms-outlook", "msg");
		}
		
		return extensionMap;
	}

	public static boolean onlyFinancialInstitutionSearch(SecuritySearchParametersDTO dto) {
		return dto.getFinancialInstitutionId() != null &&
				dto.getSecurityNumber() == null &&
				dto.getInstrumentNumber() == null &&
				dto.getEscrowAgentContactId() == null &&
				dto.getVbNumber() == null &&
				dto.getCurrentAmount() == null &&
				dto.getReceivedDate() == null &&
				dto.getPoolId() == null &&
				dto.getBranchId() == null &&
				dto.getFinancialInstitutionTypeId() == null &&
				dto.getSecurityTypeId() == null &&
				dto.getEnrolmentNumber() == null &&
				dto.getSecurityPurposeId() == null;
	}
}
