package com.tarion.vbs.common.dto.correspondence;

import java.io.Serializable;

public class GenerateCorrespondenceDTO implements Serializable {
	
	private static final long serialVersionUID = -4420688976013535341L;

	private Long templateId;
	private Long securityId;
	
	public Long getTemplateId() {
		return templateId;
	}
	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}
	public Long getSecurityId() {
		return securityId;
	}
	public void setSecurityId(Long securityId) {
		this.securityId = securityId;
	}

	@Override
	public String toString() {
		return "GenerateCorrespondenceDTO [templateId=" + templateId + ", securityId=" + securityId + "]";
	}

}
