package com.tarion.vbs.common.dto.release;

import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.common.enums.ReleaseStatus;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class PendingReleaseDTO implements Serializable {

	private static final long serialVersionUID = 6426858274844399336L;

	private String vbNumber;
	private String vbName;
	private String instrumentNumber;
	private ReleaseTypeDTO releaseType;
	private String referenceNumber;
	private ContactDTO payee;
	private boolean receivedPriorNov2004;
	private BigDecimal currentInterestAmount;
	private BigDecimal availableInterestAmount;
	private BigDecimal currentAmount;
	private BigDecimal originalAmount;
	private BigDecimal releasePercentage;
	private LocalDateTime managerApprovalDate;
	private String approvalManager;
	private String comment;
	private BigDecimal interestCurrentAmountTotal;
	private BigDecimal interestAuthorizedAmountTotal;
	private BigDecimal authorizedAmount;
	private BigDecimal interestAmount;
	private BigDecimal principalAmount;
	private Long releaseId;
	private Long securityId;
	private boolean selected;
	private String releaseTypeName;
	private String payeeName;
	private String securityTypeName;
	private ReleaseStatus releaseStatus;

	public String getVbNumber() {
		return vbNumber;
	}

	public void setVbNumber(String vbNumber) {
		this.vbNumber = vbNumber;
	}

	public String getVbName() {
		return vbName;
	}

	public void setVbName(String vbName) {
		this.vbName = vbName;
	}

	public String getInstrumentNumber() {
		return instrumentNumber;
	}

	public void setInstrumentNumber(String instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}

	public ReleaseTypeDTO getReleaseType() {
		return releaseType;
	}

	public void setReleaseType(ReleaseTypeDTO releaseType) {
		this.releaseType = releaseType;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public ContactDTO getPayee() {
		return payee;
	}

	public void setPayee(ContactDTO payee) {
		this.payee = payee;
	}

	public boolean isReceivedPriorNov2004() {
		return receivedPriorNov2004;
	}

	public void setReceivedPriorNov2004(boolean receivedPriorNov2004) {
		this.receivedPriorNov2004 = receivedPriorNov2004;
	}

	public BigDecimal getCurrentInterestAmount() {
		return currentInterestAmount;
	}

	public void setCurrentInterestAmount(BigDecimal currentInterestAmount) {
		this.currentInterestAmount = currentInterestAmount;
	}

	public BigDecimal getAvailableInterestAmount() {
		return availableInterestAmount;
	}

	public void setAvailableInterestAmount(BigDecimal availableInterestAmount) {
		this.availableInterestAmount = availableInterestAmount;
	}

	public BigDecimal getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(BigDecimal currentAmount) {
		this.currentAmount = currentAmount;
	}

	public BigDecimal getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(BigDecimal originalAmount) {
		this.originalAmount = originalAmount;
	}

	public BigDecimal getReleasePercentage() {
		return releasePercentage;
	}

	public void setReleasePercentage(BigDecimal releasePercentage) {
		this.releasePercentage = releasePercentage;
	}

	public LocalDateTime getManagerApprovalDate() {
		return managerApprovalDate;
	}

	public void setManagerApprovalDate(LocalDateTime managerApprovalDate) {
		this.managerApprovalDate = managerApprovalDate;
	}

	public String getApprovalManager() {
		return approvalManager;
	}

	public void setApprovalManager(String approvalManager) {
		this.approvalManager = approvalManager;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public BigDecimal getInterestCurrentAmountTotal() {
		return interestCurrentAmountTotal;
	}

	public void setInterestCurrentAmountTotal(BigDecimal interestCurrentAmountTotal) {
		this.interestCurrentAmountTotal = interestCurrentAmountTotal;
	}

	public BigDecimal getInterestAuthorizedAmountTotal() {
		return interestAuthorizedAmountTotal;
	}

	public void setInterestAuthorizedAmountTotal(BigDecimal interestAuthorizedAmountTotal) {
		this.interestAuthorizedAmountTotal = interestAuthorizedAmountTotal;
	}

	public BigDecimal getAuthorizedAmount() {
		return authorizedAmount;
	}

	public void setAuthorizedAmount(BigDecimal authorizedAmount) {
		this.authorizedAmount = authorizedAmount;
	}

	public BigDecimal getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(BigDecimal interestAmount) {
		this.interestAmount = interestAmount;
	}

	public BigDecimal getPrincipalAmount() {
		return principalAmount;
	}

	public void setPrincipalAmount(BigDecimal principalAmount) {
		this.principalAmount = principalAmount;
	}

	public Long getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(Long releaseId) {
		this.releaseId = releaseId;
	}

	public Long getSecurityId() {
		return securityId;
	}

	public void setSecurityId(Long securityId) {
		this.securityId = securityId;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getReleaseTypeName() {
		return releaseTypeName;
	}

	public void setReleaseTypeName(String releaseTypeName) {
		this.releaseTypeName = releaseTypeName;
	}

	public String getPayeeName() {
		return payeeName;
	}

	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	public String getSecurityTypeName() {
		return securityTypeName;
	}

	public void setSecurityTypeName(String securityTypeName) {
		this.securityTypeName = securityTypeName;
	}

	public ReleaseStatus getReleaseStatus() {
		return releaseStatus;
	}

	public void setReleaseStatus(ReleaseStatus releaseStatus) {
		this.releaseStatus = releaseStatus;
	}

	@Override
	public String toString() {
		return "PendingReleaseDTO{" +
				"vbNumber='" + vbNumber + '\'' +
				", vbName='" + vbName + '\'' +
				", instrumentNumber='" + instrumentNumber + '\'' +
				", releaseType=" + releaseType +
				", referenceNumber='" + referenceNumber + '\'' +
				", payee=" + payee +
				", receivedPriorNov2004=" + receivedPriorNov2004 +
				", currentInterestAmount=" + currentInterestAmount +
				", availableInterestAmount=" + availableInterestAmount +
				", currentAmount=" + currentAmount +
				", originalAmount=" + originalAmount +
				", releasePercentage=" + releasePercentage +
				", managerApprovalDate=" + managerApprovalDate +
				", approvalManager='" + approvalManager + '\'' +
				", comment='" + comment + '\'' +
				", interestCurrentAmountTotal=" + interestCurrentAmountTotal +
				", interestAuthorizedAmountTotal=" + interestAuthorizedAmountTotal +
				", authorizedAmount=" + authorizedAmount +
				", interestAmount=" + interestAmount +
				", principalAmount=" + principalAmount +
				", releaseId=" + releaseId +
				", securityId=" + securityId +
				", selected=" + selected +
				", releaseTypeName='" + releaseTypeName + '\'' +
				", payeeName='" + payeeName + '\'' +
				", securityTypeName='" + securityTypeName + '\'' +
				", releaseStatus=" + releaseStatus +
				'}';
	}
}
