package com.tarion.vbs.common.dto.autorelease;

import java.io.Serializable;
import java.time.LocalDateTime;

public class AutoReleaseResultDTO implements Serializable {

	private static final long serialVersionUID = 1291397760310682908L;
	private Long id;
	private Integer vNumber;
	private String vName;
	private String licenseStatus;	
	private LocalDateTime securityReceivedDate;	
	private String instrumentNumber;
	private Integer enrolmentNumber;
	private LocalDateTime warrantyStartDate;
	private String enrolmentAddress;
	private String securityType;
	private String releaseTimingType;
	private String deletedBy;
	private boolean delete;
	private String eliminationCriteria;
	private Long runId;
	private Long securityId;

	public Integer getvNumber() {
		return vNumber;
	}
	public void setvNumber(Integer vNumber) {
		this.vNumber = vNumber;
	}
	public String getvName() {
		return vName;
	}
	public void setvName(String vName) {
		this.vName = vName;
	}
	public String getVbNameNumber(){
		return vNumber + " - " + vName;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLicenseStatus() {
		return licenseStatus;
	}
	public void setLicenseStatus(String licenseStatus) {
		this.licenseStatus = licenseStatus;
	}
	public LocalDateTime getSecurityReceivedDate() {
		return securityReceivedDate;
	}
	public void setSecurityReceivedDate(LocalDateTime securityReceivedDate) {
		this.securityReceivedDate = securityReceivedDate;
	}
	public String getInstrumentNumber() {
		return instrumentNumber;
	}
	public void setInstrumentNumber(String instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}
	public Integer getEnrolmentNumber() {
		return enrolmentNumber;
	}
	public void setEnrolmentNumber(Integer enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}
	public LocalDateTime getWarrantyStartDate() {
		return warrantyStartDate;
	}
	public void setWarrantyStartDate(LocalDateTime warrantyStartDate) {
		this.warrantyStartDate = warrantyStartDate;
	}
	public String getEnrolmentAddress() {
		return enrolmentAddress;
	}
	public void setEnrolmentAddress(String enrolmentAddress) {
		this.enrolmentAddress = enrolmentAddress;
	}
	public String getSecurityType() {
		return securityType;
	}
	public void setSecurityType(String securityType) {
		this.securityType = securityType;
	}
	public String getReleaseTimingType() {
		return releaseTimingType;
	}
	public void setReleaseTimingType(String releaseTimingType) {
		this.releaseTimingType = releaseTimingType;
	}
	public String getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}
	public boolean isDelete() {
		return delete;
	}
	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	public String getEliminationCriteria() {
		return eliminationCriteria;
	}
	public void setEliminationCriteria(String eliminationCriteria) {
		this.eliminationCriteria = eliminationCriteria;
	}
	
	public Long getRunId() {
		return runId;
	}
	public void setRunId(Long runId) {
		this.runId = runId;
	}
	public Long getSecurityId() {
		return securityId;
	}
	public void setSecurityId(Long securityId) {
		this.securityId = securityId;
	}
	@Override
	public String toString() {
		return "AutoReleaseResultsDTO [vNumber=" + vNumber + ", vName=" + vName + ", licenseStatus="
				+ licenseStatus + ", securityReceivedDate=" + securityReceivedDate + ", instrumentNumber=" + instrumentNumber + ", securityId=" + securityId + ", enrolmentNumber=" + enrolmentNumber
				+ ", warrantyStartDate=" + warrantyStartDate + ", warrantyStartDate=" + warrantyStartDate
				+ ", enrolmentAddress=" + enrolmentAddress+ ", id=" + id + ", securityType=" + securityType
				+ ", releaseTimingType=" + releaseTimingType + ", deletedBy=" + deletedBy + ", runId=" + runId + ", delete=" + delete
				+ ", eliminationCriteria=" + eliminationCriteria + "]";
	}
}
