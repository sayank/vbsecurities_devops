package com.tarion.vbs.common.enums;

public enum CRMSearchTypeEnum {
	EQUALS("EQUALS"), CONTAINS("CONTAINS"), BEGINS_WITH("BEGINS_WITH");

	private String type;

	CRMSearchTypeEnum(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}
