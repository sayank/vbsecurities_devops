/* 
 * 
 * FinancialInstitutionMaaPoaInstitutionDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.financialinstitution;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * FinancialInstitutionMaaPoaInstitution DTO class used as transport for Financial Institution MAAPOA institution
 *
 * @author <A href="sbogicevic@tarion.com">Srdjan Bogicevic</A>
 * @since 2019-05-22
 * @version 1.0
 */
public class FinancialInstitutionMaaPoaInstitutionDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long version;
	
	private FinancialInstitutionDTO financialInstitution;
	private BranchDTO branch;
	private BigDecimal percentage;
	private BigDecimal originalAmountOnInstitution;
	private BigDecimal currentAmountOnInstitution;
	private boolean deleted;

	private LocalDateTime createDate;
	private String createUser;
	private LocalDateTime updateDate;
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public FinancialInstitutionDTO getFinancialInstitution() {
		return financialInstitution;
	}

	public void setFinancialInstitution(FinancialInstitutionDTO financialInstitution) {
		this.financialInstitution = financialInstitution;
	}

	public BranchDTO getBranch() {
		return branch;
	}

	public void setBranch(BranchDTO branch) {
		this.branch = branch;
	}

	public BigDecimal getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}
	
	public BigDecimal getOriginalAmountOnInstitution() {
		return originalAmountOnInstitution;
	}

	public void setOriginalAmountOnInstitution(BigDecimal originalAmountOnInstitution) {
		this.originalAmountOnInstitution = originalAmountOnInstitution;
	}

	public BigDecimal getCurrentAmountOnInstitution() {
		return currentAmountOnInstitution;
	}

	public void setCurrentAmountOnInstitution(BigDecimal currentAmountOnInstitution) {
		this.currentAmountOnInstitution = currentAmountOnInstitution;
	}

	public boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinancialInstitutionMaaPoaInstitutionDTO other = (FinancialInstitutionMaaPoaInstitutionDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "FinancialInstitutionMaaPoaDTO [id=" + id + ", version=" + version + 
				", percentage=" + percentage +
				", originalAmountOnInstitution=" + originalAmountOnInstitution +
				", currentAmountOnInstitution=" + currentAmountOnInstitution +
				", deleted=" + deleted +
				"]";
	}	
}
