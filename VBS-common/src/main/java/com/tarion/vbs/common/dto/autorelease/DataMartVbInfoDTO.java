package com.tarion.vbs.common.dto.autorelease;

import java.io.Serializable;
import java.math.BigDecimal;

public class DataMartVbInfoDTO implements Serializable {

    private static final long serialVersionUID = 1594327867691398395L;

    private int enrolmentPendingSecurity;
    private int activeReserve;
    private int demand;
    private int revoked;
    private BigDecimal currentAmount;
    private BigDecimal vbDebt;
    private BigDecimal pdogDebt;

    public int getEnrolmentPendingSecurity() {
        return enrolmentPendingSecurity;
    }

    public void setEnrolmentPendingSecurity(int enrolmentPendingSecurity) {
        this.enrolmentPendingSecurity = enrolmentPendingSecurity;
    }

    public int getActiveReserve() {
        return activeReserve;
    }

    public void setActiveReserve(int activeReserve) {
        this.activeReserve = activeReserve;
    }

    public int getDemand() {
        return demand;
    }

    public void setDemand(int demand) {
        this.demand = demand;
    }

    public int getRevoked() {
        return revoked;
    }

    public void setRevoked(int revoked) {
        this.revoked = revoked;
    }

    public BigDecimal getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(BigDecimal currentAmount) {
        this.currentAmount = currentAmount;
    }

    public BigDecimal getVbDebt() {
        return vbDebt;
    }

    public void setVbDebt(BigDecimal vbDebt) {
        this.vbDebt = vbDebt;
    }

    public BigDecimal getPdogDebt() {
        return pdogDebt;
    }

    public void setPdogDebt(BigDecimal pdogDebt) {
        this.pdogDebt = pdogDebt;
    }

    @Override
    public String toString() {
        return "DataMartVbInfoDTO{" +
                "enrolmentPendingSecurity=" + enrolmentPendingSecurity +
                ", activeReserve=" + activeReserve +
                ", demand=" + demand +
                ", revoked=" + revoked +
                ", currentAmount=" + currentAmount +
                ", vbDebt=" + vbDebt +
                ", pdogDebt=" + pdogDebt +
                '}';
    }
}
