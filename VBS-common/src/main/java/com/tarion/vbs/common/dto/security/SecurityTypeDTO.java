/* 
 * 
 * SecurityTypeDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.security;

import com.tarion.vbs.common.dto.NameDescriptionDTO;

/**
 * Security Type DTO class used for Security Type Lookup
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-16
 * @version 1.0
 */
public class SecurityTypeDTO extends NameDescriptionDTO {
	private static final long serialVersionUID = 1L;
	

	
	@Override
	public String toString() {
		return "SecurityTypeDTO [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
	
}
