package com.tarion.vbs.common.dto.autorelease;

import java.io.Serializable;

public class AutoReleaseVbApplicationStatusDTO implements Serializable {

    private static final long serialVersionUID = 1594327867691398395L;

    private Long autoReleaseRunId;
    private String vbNumber;
    private Long applicationStatus;

    public AutoReleaseVbApplicationStatusDTO(Long autoReleaseRunId, String vbNumber, Long applicationStatus) {
        this.autoReleaseRunId = autoReleaseRunId;
        this.vbNumber = vbNumber;
        this.applicationStatus = applicationStatus;
    }

    public Long getAutoReleaseRunId() {
        return autoReleaseRunId;
    }

    public void setAutoReleaseRunId(Long autoReleaseRunId) {
        this.autoReleaseRunId = autoReleaseRunId;
    }

    public String getVbNumber() {
        return vbNumber;
    }

    public void setVbNumber(String vbNumber) {
        this.vbNumber = vbNumber;
    }

    public Long getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(Long applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    @Override
    public String toString() {
        return "AutoReleaseVbApplicationStatusDTO{" +
                "autoReleaseRunId=" + autoReleaseRunId +
                ", vbNumber='" + vbNumber + '\'' +
                ", applicationStatus=" + applicationStatus +
                '}';
    }
}
