package com.tarion.vbs.common.exceptions;

public class OptimisticLockException extends RuntimeException {

	private static final long serialVersionUID = 6766921456122148080L;
	
	public OptimisticLockException(String message) {
		super(message);
	}

}
