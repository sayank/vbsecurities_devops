package com.tarion.vbs.common.enums;

public enum ReleaseApprovalStatusEnum {
    Authorized, Rejected
}
