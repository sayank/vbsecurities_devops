/* 
 * 
 * SecuritySearchParametersDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.security;

import java.io.Serializable;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tarion.vbs.common.enums.SearchTypeEnum;

/**
 * Security Search DTO class used as transport for Security Search parameters
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-16
 * @version 1.0
 */
public class SecuritySearchParametersDTO implements Serializable {

	private static final long serialVersionUID = 23L;
	
	private String instrumentNumber;
	private String instrumentNumberTo;
	private SearchTypeEnum instrumentNumberSearchType;
	private SearchTypeEnum vbNumberType;
	private String vbNumber;
	private String vbNumberTo;
	private SearchTypeEnum vbNumberSearchType;
	private String enrolmentNumber;
	private Long financialInstitutionId;
	private Long financialInstitutionTypeId;
	private Long branchId;
	private String escrowAgentContactId;
	private String escrowAgentContactIdTo;
	private SearchTypeEnum escrowAgentContactIdType;
	private Long securityTypeId;
	private Long securityPurposeId;
	private SearchTypeEnum currentAmountType;
	private BigDecimal currentAmount;
	private BigDecimal currentAmountTo;
	private String securityNumber;
	private String securityNumberTo;
	private SearchTypeEnum securityNumberSearchType;
	private Long poolId;
	private LocalDateTime receivedDate;
	private LocalDateTime receivedDateBetweenTo;
    private SearchTypeEnum receivedDateType;
    private boolean showRecentSecurities;

    public String getInstrumentNumber() {
        return instrumentNumber;
    }

    public void setInstrumentNumber(String instrumentNumber) {
        this.instrumentNumber = instrumentNumber;
    }

    public String getInstrumentNumberTo() {
        return instrumentNumberTo;
    }

    public void setInstrumentNumberTo(String instrumentNumberTo) {
        this.instrumentNumberTo = instrumentNumberTo;
    }

    public String getEscrowAgentContactIdTo() {
		return escrowAgentContactIdTo;
	}

	public void setEscrowAgentContactIdTo(String escrowAgentContactIdTo) {
		this.escrowAgentContactIdTo = escrowAgentContactIdTo;
	}

	public SearchTypeEnum getInstrumentNumberSearchType() {
        return instrumentNumberSearchType;
    }

    public void setInstrumentNumberSearchType(SearchTypeEnum instrumentNumberSearchType) {
        this.instrumentNumberSearchType = instrumentNumberSearchType;
    }

    public SearchTypeEnum getEscrowAgentContactIdType() {
		return escrowAgentContactIdType;
	}

	public void setEscrowAgentContactIdType(SearchTypeEnum escrowAgentContactIdType) {
		this.escrowAgentContactIdType = escrowAgentContactIdType;
	}

	public SearchTypeEnum getVbNumberType() {
        return vbNumberType;
    }

    public void setVbNumberType(SearchTypeEnum vbNumberType) {
        this.vbNumberType = vbNumberType;
    }

    public String getVbNumber() {
        return vbNumber;
    }

    public void setVbNumber(String vbNumber) {
        this.vbNumber = vbNumber;
    }

    public String getVbNumberTo() {
        return vbNumberTo;
    }

    public void setVbNumberTo(String vbNumberTo) {
        this.vbNumberTo = vbNumberTo;
    }

    public SearchTypeEnum getVbNumberSearchType() {
        return vbNumberSearchType;
    }

    public void setVbNumberSearchType(SearchTypeEnum vbNumberSearchType) {
        this.vbNumberSearchType = vbNumberSearchType;
    }

    public String getEnrolmentNumber() {
        return enrolmentNumber;
    }

    public void setEnrolmentNumber(String enrolmentNumber) {
        this.enrolmentNumber = enrolmentNumber;
    }

    public Long getFinancialInstitutionId() {
        return financialInstitutionId;
    }

    public void setFinancialInstitutionId(Long financialInstitutionId) {
        this.financialInstitutionId = financialInstitutionId;
    }
    
    public Long getFinancialInstitutionTypeId() {
		return financialInstitutionTypeId;
	}

	public void setFinancialInstitutionTypeId(Long financialInstitutionTypeId) {
		this.financialInstitutionTypeId = financialInstitutionTypeId;
	}

	public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

	public String getEscrowAgentContactId() {
		return escrowAgentContactId;
	}

	public void setEscrowAgentContactId(String escrowAgentContactId) {
		this.escrowAgentContactId = escrowAgentContactId;
	}

	public Long getSecurityTypeId() {
        return securityTypeId;
    }

    public void setSecurityTypeId(Long securityTypeId) {
        this.securityTypeId = securityTypeId;
    }

    public Long getSecurityPurposeId() {
        return securityPurposeId;
    }

    public void setSecurityPurposeId(Long securityPurposeId) {
        this.securityPurposeId = securityPurposeId;
    }

    public SearchTypeEnum getCurrentAmountType() {
        return currentAmountType;
    }

    public void setCurrentAmountType(SearchTypeEnum currentAmountType) {
        this.currentAmountType = currentAmountType;
    }

    public BigDecimal getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(BigDecimal currentAmount) {
        this.currentAmount = currentAmount;
    }

    public BigDecimal getCurrentAmountTo() {
        return currentAmountTo;
    }

    public void setCurrentAmountTo(BigDecimal currentAmountTo) {
        this.currentAmountTo = currentAmountTo;
    }

    public String getSecurityNumber() {
        return securityNumber;
    }

    public void setSecurityNumber(String securityNumber) {
        this.securityNumber = securityNumber;
    }

    public String getSecurityNumberTo() {
        return securityNumberTo;
    }

    public void setSecurityNumberTo(String securityNumberTo) {
        this.securityNumberTo = securityNumberTo;
    }

    public SearchTypeEnum getSecurityNumberSearchType() {
        return securityNumberSearchType;
    }

    public void setSecurityNumberSearchType(SearchTypeEnum securityNumberSearchType) {
        this.securityNumberSearchType = securityNumberSearchType;
    }

    public Long getPoolId() {
        return poolId;
    }

    public void setPoolId(Long poolId) {
        this.poolId = poolId;
    }

    public LocalDateTime getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(LocalDateTime receivedDate) {
        this.receivedDate = receivedDate;
    }

    public LocalDateTime getReceivedDateBetweenTo() {
        return receivedDateBetweenTo;
    }

    public void setReceivedDateBetweenTo(LocalDateTime receivedDateBetweenTo) {
        this.receivedDateBetweenTo = receivedDateBetweenTo;
    }

    public SearchTypeEnum getReceivedDateType() {
        return receivedDateType;
    }

    public void setReceivedDateType(SearchTypeEnum receivedDateType) {
        this.receivedDateType = receivedDateType;
    }

    public boolean isShowRecentSecurities() {
        return this.showRecentSecurities;
    }

    public void setShowRecentSecurities(boolean showRecentSecurities) {
        this.showRecentSecurities = showRecentSecurities;
    }

    @Override
    public String toString() {
        return "SecuritySearchParametersDTO{" +
                "instrumentNumber='" + instrumentNumber + '\'' +
                ", instrumentNumberTo='" + instrumentNumberTo + '\'' +
                ", instrumentNumberSearchType=" + instrumentNumberSearchType +
                ", vbNumberType=" + vbNumberType +
                ", vbNumber='" + vbNumber + '\'' +
                ", vbNumberTo='" + vbNumberTo + '\'' +
                ", vbNumberSearchType=" + vbNumberSearchType +
                ", enrolmentNumber='" + enrolmentNumber + '\'' +
                ", financialInstitutionId=" + financialInstitutionId +
                ", financialInstitutionTypeId=" + financialInstitutionTypeId +
                ", branchId=" + branchId +
                ", escrowAgentContactId=" + escrowAgentContactId +
                ", securityTypeId=" + securityTypeId +
                ", securityPurposeId=" + securityPurposeId +
                ", currentAmountType=" + currentAmountType +
                ", currentAmount=" + currentAmount +
                ", currentAmountTo=" + currentAmountTo +
                ", securityNumber='" + securityNumber + '\'' +
                ", securityNumberTo='" + securityNumberTo + '\'' +
                ", securityNumberSearchType=" + securityNumberSearchType +
                ", poolId=" + poolId +
                ", receivedDate=" + receivedDate +
                ", receivedDateBetweenTo=" + receivedDateBetweenTo +
                ", receivedDateType=" + receivedDateType +
                ", showRecentSecurities=" + showRecentSecurities +
                '}';
    }
}
