package com.tarion.vbs.common.dto.release;

import java.io.Serializable;

public class ReplacedByDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4290550018741783827L;

	private Long securityId;
	private String instrumentNumber;
	
	public Long getSecurityId() {
		return securityId;
	}
	public void setSecurityId(Long securityId) {
		this.securityId = securityId;
	}
	public String getInstrumentNumber() {
		return instrumentNumber;
	}
	public void setInstrumentNumber(String instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}
	
	
}
