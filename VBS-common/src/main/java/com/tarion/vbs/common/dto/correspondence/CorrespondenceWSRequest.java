package com.tarion.vbs.common.dto.correspondence;

import java.io.Serializable;

public class CorrespondenceWSRequest implements Serializable {

	private static final long serialVersionUID = 3891371545454493087L;

	private long sequenceNumber;
	private long securityNumber;
	private String templateName;

	public long getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(long sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public long getSecurityNumber() {
		return securityNumber;
	}

	public void setSecurityNumber(long securityNumber) {
		this.securityNumber = securityNumber;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	@Override
	public String toString() {
		return "CorrespondenceWSRequest{" +
				"sequenceNumber=" + sequenceNumber +
				", securityNumber=" + securityNumber +
				", templateName='" + templateName + '\'' +
				'}';
	}
}
