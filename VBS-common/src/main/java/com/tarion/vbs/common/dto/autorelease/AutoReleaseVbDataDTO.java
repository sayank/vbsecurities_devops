package com.tarion.vbs.common.dto.autorelease;

import java.io.Serializable;

public class AutoReleaseVbDataDTO implements Serializable {

    private static final long serialVersionUID = 1594327867691398395L;

    private Long autoReleaseRunId;
    private String vbNumber;
    private boolean underInvestigation;
    private String creditRating;
    private Long licenseStatus;
    private boolean activeAlert;
    private Long osCCPS;
    private String avgGuarantorCreditRating;
    private Long numGuarantors;
    private boolean unwillingUnable;

    public AutoReleaseVbDataDTO(Long autoReleaseRunId, String vbNumber, boolean underInvestigation, String creditRating, Long licenseStatus, boolean activeAlert, Long osCCPS, String avgGuarantorCreditRating, Long numGuarantors, boolean unwillingUnable) {
        this.autoReleaseRunId = autoReleaseRunId;
        this.vbNumber = vbNumber;
        this.underInvestigation = underInvestigation;
        this.creditRating = creditRating;
        this.licenseStatus = licenseStatus;
        this.activeAlert = activeAlert;
        this.osCCPS = osCCPS;
        this.avgGuarantorCreditRating = avgGuarantorCreditRating;
        this.numGuarantors = numGuarantors;
        this.unwillingUnable = unwillingUnable;
    }

    public Long getAutoReleaseRunId() {
        return autoReleaseRunId;
    }

    public void setAutoReleaseRunId(Long autoReleaseRunId) {
        this.autoReleaseRunId = autoReleaseRunId;
    }

    public String getVbNumber() {
        return vbNumber;
    }

    public void setVbNumber(String vbNumber) {
        this.vbNumber = vbNumber;
    }

    public boolean isUnderInvestigation() {
        return underInvestigation;
    }

    public void setUnderInvestigation(boolean underInvestigation) {
        this.underInvestigation = underInvestigation;
    }

    public String getCreditRating() {
        return creditRating;
    }

    public void setCreditRating(String creditRating) {
        this.creditRating = creditRating;
    }

    public Long getLicenseStatus() {
        return licenseStatus;
    }

    public void setLicenseStatus(Long licenseStatus) {
        this.licenseStatus = licenseStatus;
    }

    public boolean isActiveAlert() {
        return activeAlert;
    }

    public void setActiveAlert(boolean activeAlert) {
        this.activeAlert = activeAlert;
    }

    public Long getOsCCPS() {
        return osCCPS;
    }

    public void setOsCCPS(Long osCCPS) {
        this.osCCPS = osCCPS;
    }

    public String getAvgGuarantorCreditRating() {
        return avgGuarantorCreditRating;
    }

    public void setAvgGuarantorCreditRating(String avgGuarantorCreditRating) {
        this.avgGuarantorCreditRating = avgGuarantorCreditRating;
    }

    public Long getNumGuarantors() {
        return numGuarantors;
    }

    public void setNumGuarantors(Long numGuarantors) {
        this.numGuarantors = numGuarantors;
    }

    public boolean isUnwillingUnable() {
        return unwillingUnable;
    }

    public void setUnwillingUnable(boolean unwillingUnable) {
        this.unwillingUnable = unwillingUnable;
    }

    @Override
    public String toString() {
        return "AutoReleaseVbDataDTO{" +
                "autoReleaseRunId=" + autoReleaseRunId +
                ", vbNumber='" + vbNumber + '\'' +
                ", underInvestigation=" + underInvestigation +
                ", creditRating='" + creditRating + '\'' +
                ", licenseStatus=" + licenseStatus +
                ", activeAlert=" + activeAlert +
                ", osCCPS=" + osCCPS +
                ", avgGuarantorCreditRating='" + avgGuarantorCreditRating + '\'' +
                ", numGuarantors=" + numGuarantors +
                ", unwillingUnable=" + unwillingUnable +
                '}';
    }
}
