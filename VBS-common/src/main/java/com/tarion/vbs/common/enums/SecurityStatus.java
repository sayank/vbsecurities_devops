/* 
 * 
 * SecurityStatus.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.enums;

/**
 * Security Status enum used to mark status of Security
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-02-18
 * @version 1.0
 */
public enum SecurityStatus {
    VALID, WITHDRAWN;
    

}
