/* 
 * 
 * ErrorDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto;

import com.tarion.vbs.common.enums.ErrorTypeEnum;

import java.io.Serializable;

/**
 * Error DTO class holds service validation error information
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-19
 * @version 1.0
 */
public class ErrorDTO  implements Serializable {


    private static final long serialVersionUID = 8757099182564897138L;

    private String errorFieldName;
    private String errorDescription;
    private ErrorTypeEnum errorType;
    private String errorTitle;
    private String errorMessage;
    private String errorReturnCode;
    
    public ErrorDTO() {}

    public ErrorDTO(String errorDescription){
        this.errorDescription = errorDescription;
    }

    public ErrorDTO(String errorDescription, String errorFieldName) {
    	this.errorDescription = errorDescription;
    	this.errorFieldName = errorFieldName;
    	this.errorType = ErrorTypeEnum.VALIDATION_FAIL;
    }

    public ErrorDTO(String errorDescription, String errorFieldName, ErrorTypeEnum errorType, String errorTitle, String errorMessage, String errorReturnCode) {
        this.errorDescription = errorDescription;
        this.errorFieldName = errorFieldName;
        this.errorType = errorType;
        this.errorMessage = errorMessage;
        this.errorTitle = errorTitle;
        this.errorReturnCode = errorReturnCode;
    }

    public String getErrorFieldName() {
        return errorFieldName;
    }

    public void setErrorFieldName(String errorFieldName) {
        this.errorFieldName = errorFieldName;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public ErrorTypeEnum getErrorType() {
        return errorType;
    }

    public void setErrorType(ErrorTypeEnum errorType) {
        this.errorType = errorType;
    }

    public String getErrorTitle() {
        return errorTitle;
    }

    public void setErrorTitle(String errorTitle) {
        this.errorTitle = errorTitle;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorReturnCode() {
        return errorReturnCode;
    }

    public void setErrorReturnCode(String errorReturnCode) {
        this.errorReturnCode = errorReturnCode;
    }

    @Override
    public String toString() {
        return "ErrorDTO{" +
                "errorFieldName='" + errorFieldName + '\'' +
                ", errorDescription='" + errorDescription + '\'' +
                ", errorType=" + errorType +
                ", errorTitle='" + errorTitle + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", errorReturnCode='" + errorReturnCode + '\'' +
                '}';
    }
}
