package com.tarion.vbs.common.dto.enrolment;

import java.io.Serializable;

public class EnrolmentSecurityInfoDTO implements Serializable{

	private static final long serialVersionUID = -5797317296082083225L;
	
	private String enrolmentNumber;
	private int poolCount;
	private int unitsToCover;
	private PoolEnrolments poolEnrolments;
	private int enrolmentCountWithCurrentAmount;
	private int unitsToCoverWithCurrentAmount;
	
	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}
	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}
	public int getPoolCount() {
		return poolCount;
	}
	public void setPoolCount(int poolCount) {
		this.poolCount = poolCount;
	}
	public int getUnitsToCover() {
		return unitsToCover;
	}
	public void setUnitsToCover(int unitsToCover) {
		this.unitsToCover = unitsToCover;
	}

	public PoolEnrolments getPoolEnrolments() {
		if(poolEnrolments == null) {
			poolEnrolments = new PoolEnrolments();
		}
		return poolEnrolments;
	}

	public void setPoolEnrolments(PoolEnrolments poolEnrolments) {
		this.poolEnrolments = poolEnrolments;
	}

	public int getEnrolmentCountWithCurrentAmount() {
		return enrolmentCountWithCurrentAmount;
	}
	public void setEnrolmentCountWithCurrentAmount(int enrolmentCountWithCurrentAmount) {
		this.enrolmentCountWithCurrentAmount = enrolmentCountWithCurrentAmount;
	}
	public int getUnitsToCoverWithCurrentAmount() {
		return unitsToCoverWithCurrentAmount;
	}
	public void setUnitsToCoverWithCurrentAmount(int unitsToCoverWithCurrentAmount) {
		this.unitsToCoverWithCurrentAmount = unitsToCoverWithCurrentAmount;
	}

	@Override
	public String toString() {
		return "EnrolmentSecurityInfoDTO{" +
				"enrolmentNumber='" + enrolmentNumber + '\'' +
				", poolCount=" + poolCount +
				", unitsToCover=" + unitsToCover +
				", poolEnrolments=" + poolEnrolments +
				", enrolmentCountWithCurrentAmount=" + enrolmentCountWithCurrentAmount +
				", unitsToCoverWithCurrentAmount=" + unitsToCoverWithCurrentAmount +
				'}';
	}
}
