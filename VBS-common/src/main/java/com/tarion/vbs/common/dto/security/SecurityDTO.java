/* 
 * 
 * SecurityDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.security;

import com.tarion.vbs.common.contact.EscrowAgentLawyerAssistantDTO;
import com.tarion.vbs.common.dto.alert.AlertDTO;
import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionBranchCompositeDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionMaaPoaDTO;
import com.tarion.vbs.common.enums.YesNoEnum;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Security DTO class used as transport for Security basic fields
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-16
 * @version 1.0
 */
public class SecurityDTO implements Serializable {

	private static final long serialVersionUID = -3442227824249649415L;

	private Long id;
	private Long version;
	private Long poolId;
	private Integer unitsToCover;
	private Integer fhUnitsToCover;
	private Integer ceUnitsToCover;
	private Long securityStatusId;
	private Long securityTypeId;
	private Long securityPurposeId;
	private Long identityTypeId;	
	private String instrumentNumber;
	private BigDecimal originalAmount;
	private BigDecimal currentAmount;
	private BigDecimal currentInterest;
	private LocalDateTime issuedDate;
	private LocalDateTime receivedDate;
	private String ppsaNumber;
	private LocalDateTime ppsaExpiryDate;
	private boolean jointBond;
	private boolean jointAut;
	private YesNoEnum releaseFirst;
	private YesNoEnum demandFirst;
	private EscrowAgentLawyerAssistantDTO escrowAgentLawyerAssistant;
	private String comment;
	private boolean thirdParty;
	private ContactDTO thirdPartyContact;
	private String dtaAccountNumber;
	private FinancialInstitutionBranchCompositeDTO financialInstitutionAndBranch;
	private FinancialInstitutionMaaPoaDTO financialInstitutionMaaPoaDTO;
	private String ppsaFileNumber;
	private List<ContactDTO> vbList;
	private BigDecimal availableAmount;
	private BigDecimal availableInterest;
    private LocalDateTime createDate;
	private String createUser;
	private LocalDateTime updateDate;
	private String updateUser;
	private boolean interestPeriodStartWarning;
	private boolean fullyReleased;
	private boolean allocated;
	private String unpostReason;
	private boolean writeOff;
	private String writeOffReason;
	private BigDecimal jointBondOriginalAmount;
	private BigDecimal jointBondCurrentAmount;
	private boolean deleted;
	private String pefAccountNumber;
	private List<EnrolmentDTO> enrolments;
	private List<AlertDTO> alerts;
	private List<SecurityDTO> jointBondSecurities;
	private boolean linkedToMultipleCE;
	private LocalDateTime monthlyDate;
	private boolean monthlyReport;
	private Long securityDeposit;
	private ContactDTO primaryVb;

	public SecurityDTO(){
		this.enrolments = new ArrayList<>();
		this.alerts = new ArrayList<>();
		this.vbList = new ArrayList<>();
		this.jointBondSecurities = new ArrayList<>();
		this.financialInstitutionAndBranch = new FinancialInstitutionBranchCompositeDTO();
		//this.escrowAgentLawyerAssistant = new EscrowAgentLawyerAssistantDTO();
		//private FinancialInstitutionMaaPoaDTO financialInstitutionMaaPoaDTO;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getPoolId() {
		return poolId;
	}

	public void setPoolId(Long poolId) {
		this.poolId = poolId;
	}

	public Integer getUnitsToCover() {
		return unitsToCover;
	}

	public void setUnitsToCover(Integer unitsToCover) {
		this.unitsToCover = unitsToCover;
	}

	public Integer getFhUnitsToCover() {
		return fhUnitsToCover;
	}

	public void setFhUnitsToCover(Integer fhUnitsToCover) {
		this.fhUnitsToCover = fhUnitsToCover;
	}

	public Integer getCeUnitsToCover() {
		return ceUnitsToCover;
	}

	public void setCeUnitsToCover(Integer ceUnitsToCover) {
		this.ceUnitsToCover = ceUnitsToCover;
	}

	public Long getSecurityStatusId() {
		return securityStatusId;
	}

	public void setSecurityStatusId(Long securityStatusId) {
		this.securityStatusId = securityStatusId;
	}

	public Long getSecurityTypeId() {
		return securityTypeId;
	}

	public void setSecurityTypeId(Long securityTypeId) {
		this.securityTypeId = securityTypeId;
	}

	public Long getSecurityPurposeId() {
		return securityPurposeId;
	}

	public void setSecurityPurposeId(Long securityPurposeId) {
		this.securityPurposeId = securityPurposeId;
	}

	public Long getIdentityTypeId() {
		return identityTypeId;
	}

	public void setIdentityTypeId(Long identityTypeId) {
		this.identityTypeId = identityTypeId;
	}

	public String getInstrumentNumber() {
		return instrumentNumber;
	}

	public void setInstrumentNumber(String instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}

	public BigDecimal getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(BigDecimal originalAmount) {
		this.originalAmount = originalAmount;
	}

	public BigDecimal getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(BigDecimal currentAmount) {
		this.currentAmount = currentAmount;
	}

	public BigDecimal getCurrentInterest() {
		return currentInterest;
	}

	public void setCurrentInterest(BigDecimal currentInterest) {
		this.currentInterest = currentInterest;
	}

	public LocalDateTime getIssuedDate() {
		return issuedDate;
	}

	public void setIssuedDate(LocalDateTime issuedDate) {
		this.issuedDate = issuedDate;
	}

	public LocalDateTime getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(LocalDateTime receivedDate) {
		this.receivedDate = receivedDate;
	}

	public String getPpsaNumber() {
		return ppsaNumber;
	}

	public void setPpsaNumber(String ppsaNumber) {
		this.ppsaNumber = ppsaNumber;
	}

	public LocalDateTime getPpsaExpiryDate() {
		return ppsaExpiryDate;
	}

	public void setPpsaExpiryDate(LocalDateTime ppsaExpiryDate) {
		this.ppsaExpiryDate = ppsaExpiryDate;
	}

	public boolean isJointBond() {
		return jointBond;
	}

	public void setJointBond(boolean jointBond) {
		this.jointBond = jointBond;
	}

	public boolean isJointAut() {
		return jointAut;
	}

	public void setJointAut(boolean jointAut) {
		this.jointAut = jointAut;
	}

	public YesNoEnum getReleaseFirst() {
		return releaseFirst;
	}

	public void setReleaseFirst(YesNoEnum releaseFirst) {
		this.releaseFirst = releaseFirst;
	}

	public YesNoEnum getDemandFirst() {
		return demandFirst;
	}

	public void setDemandFirst(YesNoEnum demandFirst) {
		this.demandFirst = demandFirst;
	}

	public EscrowAgentLawyerAssistantDTO getEscrowAgentLawyerAssistant() {
		return escrowAgentLawyerAssistant;
	}

	public void setEscrowAgentLawyerAssistant(EscrowAgentLawyerAssistantDTO escrowAgentLawyerAssistant) {
		this.escrowAgentLawyerAssistant = escrowAgentLawyerAssistant;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isThirdParty() {
		return thirdParty;
	}

	public void setThirdParty(boolean thirdParty) {
		this.thirdParty = thirdParty;
	}

	public ContactDTO getThirdPartyContact() {
		return thirdPartyContact;
	}

	public void setThirdPartyContact(ContactDTO thirdPartyContact) {
		this.thirdPartyContact = thirdPartyContact;
	}

	public String getDtaAccountNumber() {
		return dtaAccountNumber;
	}

	public void setDtaAccountNumber(String dtaAccountNumber) {
		this.dtaAccountNumber = dtaAccountNumber;
	}

	public FinancialInstitutionBranchCompositeDTO getFinancialInstitutionAndBranch() {
		return financialInstitutionAndBranch;
	}

	public void setFinancialInstitutionAndBranch(FinancialInstitutionBranchCompositeDTO financialInstitutionAndBranch) {
		this.financialInstitutionAndBranch = financialInstitutionAndBranch;
	}

	public FinancialInstitutionMaaPoaDTO getFinancialInstitutionMaaPoaDTO() {
		return financialInstitutionMaaPoaDTO;
	}

	public void setFinancialInstitutionMaaPoaDTO(FinancialInstitutionMaaPoaDTO financialInstitutionMaaPoaDTO) {
		this.financialInstitutionMaaPoaDTO = financialInstitutionMaaPoaDTO;
	}

	public String getPpsaFileNumber() {
		return ppsaFileNumber;
	}

	public void setPpsaFileNumber(String ppsaFileNumber) {
		this.ppsaFileNumber = ppsaFileNumber;
	}

	public List<ContactDTO> getVbList() {
		return vbList;
	}

	public void setVbList(List<ContactDTO> vbList) {
		this.vbList = vbList;
	}

	public BigDecimal getAvailableAmount() {
		return availableAmount;
	}

	public void setAvailableAmount(BigDecimal availableAmount) {
		this.availableAmount = availableAmount;
	}

	public BigDecimal getAvailableInterest() {
		return availableInterest;
	}

	public void setAvailableInterest(BigDecimal availableInterest) {
		this.availableInterest = availableInterest;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public boolean isInterestPeriodStartWarning() {
		return interestPeriodStartWarning;
	}

	public void setInterestPeriodStartWarning(boolean interestPeriodStartWarning) {
		this.interestPeriodStartWarning = interestPeriodStartWarning;
	}

	public boolean isFullyReleased() {
		return fullyReleased;
	}

	public void setFullyReleased(boolean fullyReleased) {
		this.fullyReleased = fullyReleased;
	}

	public boolean isAllocated() {
		return allocated;
	}

	public void setAllocated(boolean allocated) {
		this.allocated = allocated;
	}

	public String getUnpostReason() {
		return unpostReason;
	}

	public void setUnpostReason(String unpostReason) {
		this.unpostReason = unpostReason;
	}

	public boolean isWriteOff() {
		return writeOff;
	}

	public void setWriteOff(boolean writeOff) {
		this.writeOff = writeOff;
	}

	public String getWriteOffReason() {
		return writeOffReason;
	}

	public void setWriteOffReason(String writeOffReason) {
		this.writeOffReason = writeOffReason;
	}

	public BigDecimal getJointBondOriginalAmount() {
		return jointBondOriginalAmount;
	}

	public void setJointBondOriginalAmount(BigDecimal jointBondOriginalAmount) {
		this.jointBondOriginalAmount = jointBondOriginalAmount;
	}

	public BigDecimal getJointBondCurrentAmount() {
		return jointBondCurrentAmount;
	}

	public void setJointBondCurrentAmount(BigDecimal jointBondCurrentAmount) {
		this.jointBondCurrentAmount = jointBondCurrentAmount;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getPefAccountNumber() {
		return pefAccountNumber;
	}

	public void setPefAccountNumber(String pefAccountNumber) {
		this.pefAccountNumber = pefAccountNumber;
	}

	public List<EnrolmentDTO> getEnrolments() {
		return enrolments;
	}

	public void setEnrolments(List<EnrolmentDTO> enrolments) {
		this.enrolments = enrolments;
	}

	public List<AlertDTO> getAlerts() {
		return alerts;
	}

	public void setAlerts(List<AlertDTO> alerts) {
		this.alerts = alerts;
	}

	public List<SecurityDTO> getJointBondSecurities() {
		return jointBondSecurities;
	}

	public void setJointBondSecurities(List<SecurityDTO> jointBondSecurities) {
		this.jointBondSecurities = jointBondSecurities;
	}

	public boolean isLinkedToMultipleCE() {
		return linkedToMultipleCE;
	}

	public void setLinkedToMultipleCE(boolean linkedToMultipleCE) {
		this.linkedToMultipleCE = linkedToMultipleCE;
	}

	public LocalDateTime getMonthlyDate() {
		return monthlyDate;
	}

	public void setMonthlyDate(LocalDateTime monthlyDate) {
		this.monthlyDate = monthlyDate;
	}

	public boolean isMonthlyReport() {
		return monthlyReport;
	}

	public void setMonthlyReport(boolean monthlyReport) {
		this.monthlyReport = monthlyReport;
	}

	public Long getSecurityDeposit() {
		return securityDeposit;
	}

	public void setSecurityDeposit(Long securityDeposit) {
		this.securityDeposit = securityDeposit;
	}

	public ContactDTO getPrimaryVb() {
		return primaryVb;
	}

	public void setPrimaryVb(ContactDTO primaryVb) {
		this.primaryVb = primaryVb;
	}

	@Override
	public String toString() {
		return "SecurityDTO{" +
				"id=" + id +
				", version=" + version +
				", poolId=" + poolId +
				", unitsToCover=" + unitsToCover +
				", fhUnitsToCover=" + fhUnitsToCover +
				", ceUnitsToCover=" + ceUnitsToCover +
				", securityStatusId=" + securityStatusId +
				", securityTypeId=" + securityTypeId +
				", securityPurposeId=" + securityPurposeId +
				", identityTypeId=" + identityTypeId +
				", instrumentNumber='" + instrumentNumber + '\'' +
				", originalAmount=" + originalAmount +
				", currentAmount=" + currentAmount +
				", currentInterest=" + currentInterest +
				", issuedDate=" + issuedDate +
				", receivedDate=" + receivedDate +
				", ppsaNumber='" + ppsaNumber + '\'' +
				", ppsaExpiryDate=" + ppsaExpiryDate +
				", jointBond=" + jointBond +
				", jointAut=" + jointAut +
				", releaseFirst=" + releaseFirst +
				", demandFirst=" + demandFirst +
				", escrowAgentLawyerAssistant=" + escrowAgentLawyerAssistant +
				", comment='" + comment + '\'' +
				", thirdParty=" + thirdParty +
				", thirdPartyContact=" + thirdPartyContact +
				", financialInstitutionAndBranch=" + financialInstitutionAndBranch +
				", financialInstitutionMaaPoaDTO=" + financialInstitutionMaaPoaDTO +
				", ppsaFileNumber='" + ppsaFileNumber + '\'' +
				", vbList=" + vbList +
				", availableAmount=" + availableAmount +
				", availableInterest=" + availableInterest +
				", createDate=" + createDate +
				", createUser='" + createUser + '\'' +
				", updateDate=" + updateDate +
				", updateUser='" + updateUser + '\'' +
				", interestPeriodStartWarning=" + interestPeriodStartWarning +
				", fullyReleased=" + fullyReleased +
				", allocated=" + allocated +
				", unpostReason='" + unpostReason + '\'' +
				", writeOff=" + writeOff +
				", writeOffReason='" + writeOffReason + '\'' +
				", jointBondOriginalAmount=" + jointBondOriginalAmount +
				", jointBondCurrentAmount=" + jointBondCurrentAmount +
				", deleted=" + deleted +
				", enrolments=" + enrolments +
				", alerts=" + alerts +
				", jointBondSecurities=" + jointBondSecurities +
				", linkedToMultipleCE=" + linkedToMultipleCE +
				'}';
	}
}
