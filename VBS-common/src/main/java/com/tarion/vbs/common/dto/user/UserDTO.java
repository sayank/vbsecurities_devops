/* 
 * 
 * UserDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.tarion.vbs.common.constants.VbsConstants;

/**
 * User DTO class holds basic logged in user information
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-01-05
 * @version 1.0
 */
public class UserDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String lastName;
	private String userId;
	private String emailAddress;
	private String supervisor;
	private String telephone;
	
	private List<PermissionDTO> permissions;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getSupervisor() {
		return supervisor;
	}
	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}
	public List<PermissionDTO> getPermissions() {
		return (permissions != null) ? permissions : new ArrayList<>();
	}
	public void setPermissions(List<PermissionDTO> permissions) {
		this.permissions = permissions;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	public boolean hasPermission(String permission) {
		
		if(StringUtils.equalsIgnoreCase(this.getUserId(), VbsConstants.VBS_SYSTEM_USER_ID)) {
			return true;
		}
		if(this.permissions != null && !this.permissions.isEmpty()) {
			for(PermissionDTO p : this.permissions) {
				if(p.getPermissionName().equals(permission)) {
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "UserDTO, userId: " + userId + ", firstName: " + firstName + ", lastName: " + lastName + ", emailAddress; " + emailAddress + ", supervisor: " + supervisor; 
	}
	
}
