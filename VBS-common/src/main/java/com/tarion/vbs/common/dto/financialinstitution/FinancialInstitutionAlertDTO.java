package com.tarion.vbs.common.dto.financialinstitution;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.tarion.vbs.common.dto.NameDescriptionDTO;
import com.tarion.vbs.common.enums.AlertStatus;

public class FinancialInstitutionAlertDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long version;

	private Long financialInstitutionId;
	private NameDescriptionDTO alertType;
	
	protected String name;
	protected String description;
	
    private LocalDateTime createDate;
	private String createUser;
	private LocalDateTime updateDate;
	private String updateUser;

	private LocalDateTime startDate;
	private LocalDateTime endDate;
	private String endUser;
	
	private AlertStatus status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getFinancialInstitutionId() {
		return financialInstitutionId;
	}

	public void setFinancialInstitutionId(Long finacialInstitutionId) {
		this.financialInstitutionId = finacialInstitutionId;
	}

	public NameDescriptionDTO getAlertType() {
		return alertType;
	}

	public void setAlertType(NameDescriptionDTO alertType) {
		this.alertType = alertType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public AlertStatus getStatus() {
		return status;
	}

	public void setStatus(AlertStatus status) {
		this.status = status;
	}
	
	public String getEndUser() {
		return endUser;
	}

	public void setEndUser(String endUser) {
		this.endUser = endUser;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinancialInstitutionAlertDTO other = (FinancialInstitutionAlertDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "AlertDTO [id=" + id + ", version=" + version + 
				", financialInstitutionId=" + financialInstitutionId +
				", alertType=" + alertType +
				", createDate=" + createDate +
				", createUser=" + createUser +
				", name=" + name +
				", description=" + description +
				", updateDate=" + updateDate +
				", updateUser=" + updateUser +
				", startDate=" + startDate +
				", endDate=" + endDate +
				", endUser=" + endUser +				
				", status=" + status +
				"]";
	}
}
