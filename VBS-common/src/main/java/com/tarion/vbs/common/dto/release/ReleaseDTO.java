package com.tarion.vbs.common.dto.release;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.tarion.vbs.common.dto.NameDescriptionDTO;
import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.common.enums.ReleaseApprovalStatusEnum;
import com.tarion.vbs.common.enums.ReleaseStatus;

/**
 * Release DTO provides operations for release
 *
 * @author <A href="shadi.kajevand@tarion.com">Shadi Kajevand</A>
 * @version 1.0
 * @since 2018-12-1
 */
public class ReleaseDTO implements Serializable {

	private static final long serialVersionUID = 2601624582125136780L;

	private Long id;
	private Long version;
	private String analystApprovalSentTo;
	private String analystApprovalRequestor;
	private LocalDateTime analystApprovalDateTime;
	private ReleaseApprovalStatusEnum analystApprovalStatus;
	private String analystApprovedBy;
	private long analystRejectReason;
	private String assignToVp;
	private BigDecimal authorizedReleaseAmount;
	private String comment;
	private List<ReleaseEnrolmentDTO> enrolments;
	private BigDecimal interestAmount;
	private BigDecimal principalAmount;
	private boolean interestPeriodStartWarning;
	private BigDecimal fcmAmountRetained;
	private ReleaseApprovalStatusEnum finalApprovalStatus;
	private String finalApprovalRequestor;
	private String finalApprovalSentTo;
	private String finalApprovedBy;
	private Boolean fullRelease;
	private LocalDateTime finalApprovalDate;
	private BigDecimal luRetainAmount;
	private long managerRejectReason;
	private ContactDTO payTo;
	private ContactDTO providedByVb;
	private List<NameDescriptionDTO> reasons;
	private String recommendedRejectedBy;
	private String referenceNumber;
	private Boolean regularRelease;
	private NameDescriptionDTO releaseType;
	private ReplacedByDTO replacedBy;
	private BigDecimal requestedAmount;
	private LocalDateTime requestDate;
	private Long securityId;
	private boolean wsInputRequested;
	private String wsInputProvidedBy;
	private LocalDateTime wsRecievedDate;
	private NameDescriptionDTO wsRecommendation;
	private LocalDateTime wsRequestDate;
	private String wsRequestor;
	private ReleaseStatus status;
	private LocalDateTime createDate;
	private String createUser;
	private LocalDateTime updateDate;
	private String updateUser;
	private String serviceOrderId;
	private BigDecimal unwindProRata;
	private String settlementInstructions;
	private String chequeNumber;
	private LocalDateTime chequeDate;
	private String chequeStatus;
	private String replacementChequeNumber;
	private LocalDateTime replacementChequeDate;
	private String replacementChequeStatus;
	private String replacementPayee;
	private String replacementPayeeCRMContactId;
	private String voucherId;

	public ReleaseDTO(){
		this.enrolments = new ArrayList<>();
		this.reasons = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getAnalystApprovalSentTo() {
		return analystApprovalSentTo;
	}

	public void setAnalystApprovalSentTo(String analystApprovalSentTo) {
		this.analystApprovalSentTo = analystApprovalSentTo;
	}

	public String getAnalystApprovalRequestor() {
		return analystApprovalRequestor;
	}

	public void setAnalystApprovalRequestor(String analystApprovalRequestor) {
		this.analystApprovalRequestor = analystApprovalRequestor;
	}

	public LocalDateTime getAnalystApprovalDateTime() {
		return analystApprovalDateTime;
	}

	public void setAnalystApprovalDateTime(LocalDateTime analystApprovalDateTime) {
		this.analystApprovalDateTime = analystApprovalDateTime;
	}

	public ReleaseApprovalStatusEnum getAnalystApprovalStatus() {
		return analystApprovalStatus;
	}

	public void setAnalystApprovalStatus(ReleaseApprovalStatusEnum analystApprovalStatus) {
		this.analystApprovalStatus = analystApprovalStatus;
	}

	public String getAnalystApprovedBy() {
		return analystApprovedBy;
	}

	public void setAnalystApprovedBy(String analystApprovedBy) {
		this.analystApprovedBy = analystApprovedBy;
	}

	public long getAnalystRejectReason() {
		return analystRejectReason;
	}

	public void setAnalystRejectReason(long analystRejectReason) {
		this.analystRejectReason = analystRejectReason;
	}

	public String getAssignToVp() {
		return assignToVp;
	}

	public void setAssignToVp(String assignToVp) {
		this.assignToVp = assignToVp;
	}

	public BigDecimal getAuthorizedReleaseAmount() {
		return authorizedReleaseAmount;
	}

	public void setAuthorizedReleaseAmount(BigDecimal authorizedReleaseAmount) {
		this.authorizedReleaseAmount = authorizedReleaseAmount;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<ReleaseEnrolmentDTO> getEnrolments() {
		return enrolments;
	}

	public void setEnrolments(List<ReleaseEnrolmentDTO> enrolments) {
		this.enrolments = enrolments;
	}

	public BigDecimal getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(BigDecimal interestAmount) {
		this.interestAmount = interestAmount;
	}

	public BigDecimal getPrincipalAmount() {
		return principalAmount;
	}

	public void setPrincipalAmount(BigDecimal principalAmount) {
		this.principalAmount = principalAmount;
	}

	public boolean isInterestPeriodStartWarning() {
		return interestPeriodStartWarning;
	}

	public void setInterestPeriodStartWarning(boolean interestPeriodStartWarning) {
		this.interestPeriodStartWarning = interestPeriodStartWarning;
	}

	public BigDecimal getFcmAmountRetained() {
		return fcmAmountRetained;
	}

	public void setFcmAmountRetained(BigDecimal fcmAmountRetained) {
		this.fcmAmountRetained = fcmAmountRetained;
	}

	public ReleaseApprovalStatusEnum getFinalApprovalStatus() {
		return finalApprovalStatus;
	}

	public void setFinalApprovalStatus(ReleaseApprovalStatusEnum finalApprovalStatus) {
		this.finalApprovalStatus = finalApprovalStatus;
	}

	public String getFinalApprovalRequestor() {
		return finalApprovalRequestor;
	}

	public void setFinalApprovalRequestor(String finalApprovalRequestor) {
		this.finalApprovalRequestor = finalApprovalRequestor;
	}

	public String getFinalApprovalSentTo() {
		return finalApprovalSentTo;
	}

	public void setFinalApprovalSentTo(String finalApprovalSentTo) {
		this.finalApprovalSentTo = finalApprovalSentTo;
	}

	public String getFinalApprovedBy() {
		return finalApprovedBy;
	}

	public void setFinalApprovedBy(String finalApprovedBy) {
		this.finalApprovedBy = finalApprovedBy;
	}

	public Boolean getFullRelease() {
		return fullRelease;
	}

	public void setFullRelease(Boolean fullRelease) {
		this.fullRelease = fullRelease;
	}

	public LocalDateTime getFinalApprovalDate() {
		return finalApprovalDate;
	}

	public void setFinalApprovalDate(LocalDateTime finalApprovalDate) {
		this.finalApprovalDate = finalApprovalDate;
	}

	public BigDecimal getLuRetainAmount() {
		return luRetainAmount;
	}

	public void setLuRetainAmount(BigDecimal luRetainAmount) {
		this.luRetainAmount = luRetainAmount;
	}

	public long getManagerRejectReason() {
		return managerRejectReason;
	}

	public void setManagerRejectReason(long managerRejectReason) {
		this.managerRejectReason = managerRejectReason;
	}

	public ContactDTO getPayTo() {
		return payTo;
	}

	public void setPayTo(ContactDTO payTo) {
		this.payTo = payTo;
	}

	public ContactDTO getProvidedByVb() {
		return providedByVb;
	}

	public void setProvidedByVb(ContactDTO providedByVb) {
		this.providedByVb = providedByVb;
	}

	public List<NameDescriptionDTO> getReasons() {
		return reasons;
	}

	public void setReasons(List<NameDescriptionDTO> reasons) {
		this.reasons = reasons;
	}

	public String getRecommendedRejectedBy() {
		return recommendedRejectedBy;
	}

	public void setRecommendedRejectedBy(String recommendedRejectedBy) {
		this.recommendedRejectedBy = recommendedRejectedBy;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public Boolean getRegularRelease() {
		return regularRelease;
	}

	public void setRegularRelease(Boolean regularRelease) {
		this.regularRelease = regularRelease;
	}

	public NameDescriptionDTO getReleaseType() {
		return releaseType;
	}

	public void setReleaseType(NameDescriptionDTO releaseType) {
		this.releaseType = releaseType;
	}

	public ReplacedByDTO getReplacedBy() {
		return replacedBy;
	}

	public void setReplacedBy(ReplacedByDTO replacedBy) {
		this.replacedBy = replacedBy;
	}

	public BigDecimal getRequestedAmount() {
		return requestedAmount;
	}

	public void setRequestedAmount(BigDecimal requestedAmount) {
		this.requestedAmount = requestedAmount;
	}

	public LocalDateTime getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(LocalDateTime requestDate) {
		this.requestDate = requestDate;
	}

	public Long getSecurityId() {
		return securityId;
	}

	public void setSecurityId(Long securityId) {
		this.securityId = securityId;
	}

	public boolean isWsInputRequested() {
		return wsInputRequested;
	}

	public void setWsInputRequested(boolean wsInputRequested) {
		this.wsInputRequested = wsInputRequested;
	}

	public String getWsInputProvidedBy() {
		return wsInputProvidedBy;
	}

	public void setWsInputProvidedBy(String wsInputProvidedBy) {
		this.wsInputProvidedBy = wsInputProvidedBy;
	}

	public LocalDateTime getWsRecievedDate() {
		return wsRecievedDate;
	}

	public void setWsRecievedDate(LocalDateTime wsRecievedDate) {
		this.wsRecievedDate = wsRecievedDate;
	}

	public NameDescriptionDTO getWsRecommendation() {
		return wsRecommendation;
	}

	public void setWsRecommendation(NameDescriptionDTO wsRecommendation) {
		this.wsRecommendation = wsRecommendation;
	}

	public LocalDateTime getWsRequestDate() {
		return wsRequestDate;
	}

	public void setWsRequestDate(LocalDateTime wsRequestDate) {
		this.wsRequestDate = wsRequestDate;
	}

	public String getWsRequestor() {
		return wsRequestor;
	}

	public void setWsRequestor(String wsRequestor) {
		this.wsRequestor = wsRequestor;
	}

	public ReleaseStatus getStatus() {
		return status;
	}

	public void setStatus(ReleaseStatus status) {
		this.status = status;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getServiceOrderId() {
		return serviceOrderId;
	}

	public void setServiceOrderId(String serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}

	public BigDecimal getUnwindProRata() {
		return unwindProRata;
	}

	public void setUnwindProRata(BigDecimal unwindProRata) {
		this.unwindProRata = unwindProRata;
	}

	public String getSettlementInstructions() {
		return settlementInstructions;
	}

	public void setSettlementInstructions(String settlementInstructions) {
		this.settlementInstructions = settlementInstructions;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public LocalDateTime getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(LocalDateTime chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getChequeStatus() {
		return chequeStatus;
	}

	public void setChequeStatus(String chequeStatus) {
		this.chequeStatus = chequeStatus;
	}

	public String getReplacementChequeNumber() {
		return replacementChequeNumber;
	}

	public void setReplacementChequeNumber(String replacementChequeNumber) {
		this.replacementChequeNumber = replacementChequeNumber;
	}

	public LocalDateTime getReplacementChequeDate() {
		return replacementChequeDate;
	}

	public void setReplacementChequeDate(LocalDateTime replacementChequeDate) {
		this.replacementChequeDate = replacementChequeDate;
	}

	public String getReplacementChequeStatus() {
		return replacementChequeStatus;
	}

	public void setReplacementChequeStatus(String replacementChequeStatus) {
		this.replacementChequeStatus = replacementChequeStatus;
	}

	public String getReplacementPayee() {
		return replacementPayee;
	}

	public void setReplacementPayee(String replacementPayee) {
		this.replacementPayee = replacementPayee;
	}

	public String getReplacementPayeeCRMContactId() {
		return replacementPayeeCRMContactId;
	}

	public void setReplacementPayeeCRMContactId(String replacementPayeeCRMContactId) {
		this.replacementPayeeCRMContactId = replacementPayeeCRMContactId;
	}

	public String getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}


	@Override
	public String toString() {
		return "ReleaseDTO{" +
				"id=" + id +
				", version=" + version +
				", analystApprovalSentTo='" + analystApprovalSentTo + '\'' +
				", analystApprovalRequestor='" + analystApprovalRequestor + '\'' +
				", analystApprovalDateTime=" + analystApprovalDateTime +
				", analystApprovalStatus=" + analystApprovalStatus +
				", analystApprovedBy='" + analystApprovedBy + '\'' +
				", analystRejectReason=" + analystRejectReason +
				", assignToVp='" + assignToVp + '\'' +
				", authorizedReleaseAmount=" + authorizedReleaseAmount +
				", comment='" + comment + '\'' +
				", enrolments=" + enrolments +
				", interestAmount=" + interestAmount +
				", principalAmount=" + principalAmount +
				", interestPeriodStartWarning=" + interestPeriodStartWarning +
				", fcmAmountRetained=" + fcmAmountRetained +
				", finalApprovalStatus=" + finalApprovalStatus +
				", finalApprovalRequestor='" + finalApprovalRequestor + '\'' +
				", finalApprovalSentTo='" + finalApprovalSentTo + '\'' +
				", finalApprovedBy='" + finalApprovedBy + '\'' +
				", finalApprovalDate=" + finalApprovalDate +
				", luRetainAmount=" + luRetainAmount +
				", managerRejectReason=" + managerRejectReason +
				", payTo=" + payTo +
				", providedByVb=" + providedByVb +
				", reasons=" + reasons +
				", recommendedRejectedBy='" + recommendedRejectedBy + '\'' +
				", referenceNumber='" + referenceNumber + '\'' +
				", regularRelease=" + regularRelease +
				", releaseType=" + releaseType +
				", replacedBy=" + replacedBy +
				", requestedAmount=" + requestedAmount +
				", requestDate=" + requestDate +
				", securityId=" + securityId +
				", wsInputRequested=" + wsInputRequested +
				", wsInputProvidedBy='" + wsInputProvidedBy + '\'' +
				", wsRecievedDate=" + wsRecievedDate +
				", wsRecommendation=" + wsRecommendation +
				", wsRequestDate=" + wsRequestDate +
				", wsRequestor='" + wsRequestor + '\'' +
				", status=" + status +
				", createDate=" + createDate +
				", createUser='" + createUser + '\'' +
				", updateDate=" + updateDate +
				", updateUser='" + updateUser + '\'' +
				", serviceOrderId='" + serviceOrderId + '\'' +
				", unwindProRata=" + unwindProRata +
				", settlementInstructions='" + settlementInstructions + '\'' +
				", chequeNumber='" + chequeNumber + '\'' +
				", chequeDate=" + chequeDate +
				", chequeStatus='" + chequeStatus + '\'' +
				", replacementChequeNumber='" + replacementChequeNumber + '\'' +
				", replacementChequeDate=" + replacementChequeDate +
				", replacementChequeStatus='" + replacementChequeStatus + '\'' +
				", replacementPayee='" + replacementPayee + '\'' +
				", replacementPayeeCRMContactId='" + replacementPayeeCRMContactId + '\'' +
				", voucherId='" + voucherId + '\'' +
				'}';
	}
}


