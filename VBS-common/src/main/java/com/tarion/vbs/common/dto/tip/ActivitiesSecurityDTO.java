package com.tarion.vbs.common.dto.tip;

import java.io.Serializable;

public class ActivitiesSecurityDTO implements Serializable {

    private static final long serialVersionUID = -3831506932193215660L;

    private Long securityId;
    private String vbNumber;
    private String vbName;
    private Double currentAmount;

    public ActivitiesSecurityDTO(Long securityId, String vbNumber, String vbName, Double currentAmount){
        this.securityId = securityId;
        this.vbNumber = vbNumber;
        this.vbName = vbName;
        this.currentAmount = currentAmount;
    }

    public ActivitiesSecurityDTO(){}

    public Long getSecurityId() {
        return securityId;
    }

    public void setSecurityId(Long securityId) {
        this.securityId = securityId;
    }

    public String getVbNumber() {
        return vbNumber;
    }

    public void setVbNumber(String vbNumber) {
        this.vbNumber = vbNumber;
    }

    public String getVbName() {
        return vbName;
    }

    public void setVbName(String vbName) {
        this.vbName = vbName;
    }

    public Double getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(Double currentAmount) {
        this.currentAmount = currentAmount;
    }

    @Override
    public String toString() {
        return "ActivitiesSecurityDTO{" +
                "securityId=" + securityId +
                ", vbNumber='" + vbNumber + '\'' +
                ", vbName='" + vbName + '\'' +
                ", currentAmount='" + currentAmount + '\'' +
                '}';
    }
}
