package com.tarion.vbs.common.dto.financialinstitution;

import java.io.Serializable;
import java.time.LocalDateTime;

public class FIActivityDTO implements Serializable, Comparable<FIActivityDTO> {

    private static final long serialVersionUID = 42L;

    private String subject;
    private String oldValue;
    private String newValue;
    private String user;
    private LocalDateTime updateDate;

    public FIActivityDTO(String subject, String oldValue, String newValue, String user, LocalDateTime updateDate) {
        this.subject = subject;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.user = user;
        this.updateDate = updateDate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "FIActivityDTO{" +
                "subject='" + subject + '\'' +
                ", oldValue='" + oldValue + '\'' +
                ", newValue='" + newValue + '\'' +
                ", user='" + user + '\'' +
                ", updateDate=" + updateDate +
                '}';
    }


    @Override
    public int compareTo(FIActivityDTO o) {
        int compare = o.getUpdateDate().compareTo(this.updateDate);
        if(compare == 0) {
            compare = this.subject.compareTo(o.getSubject());
        }
        return compare;
    }
}
