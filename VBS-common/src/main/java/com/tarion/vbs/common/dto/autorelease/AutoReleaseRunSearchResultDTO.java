/* 
 * 
 * AutoReleaseRunSearchResultDTO.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.autorelease;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Auto Release Runs Search Result DTO class used as transport for Auto Release Search Results
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-04-05
 * @version 1.0
 */
public class AutoReleaseRunSearchResultDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;	
	private Long version;
	private Long runStatusId;	
	private LocalDateTime createDate;
	private String createUser;
	private LocalDateTime updateDate;
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getRunStatusId() {
		return runStatusId;
	}

	public void setRunStatusId(Long runStatusId) {
		this.runStatusId = runStatusId;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public String toString() {
		return "AutoReleaseRunSearchResultDTO{" +
				"id=" + id +
				", version=" + version +
				", runStatusId=" + runStatusId +
				", createDate=" + createDate +
				", createUser='" + createUser + '\'' +
				", updateDate=" + updateDate +
				", updateUser='" + updateUser + '\'' +
				'}';
	}
}
