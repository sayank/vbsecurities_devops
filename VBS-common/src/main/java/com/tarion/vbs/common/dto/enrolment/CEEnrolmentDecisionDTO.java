package com.tarion.vbs.common.dto.enrolment;

import com.tarion.vbs.common.dto.security.SecurityDTO;
import com.tarion.vbs.common.enums.YesNoEnum;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CEEnrolmentDecisionDTO implements Serializable {

    private static final long serialVersionUID = 5544566816635986481L;

    private String enrolmentNumber;
    private String enrolmentAddress;
    private String vbNumber;
    private String vbName;
    private List<SecurityDTO> blanketSecurities;
    private YesNoEnum decision;
    private String reason;
    private Integer unitsToCover;

    public CEEnrolmentDecisionDTO(){
        this.blanketSecurities = new ArrayList<>();
    }

    public String getEnrolmentNumber() {
        return enrolmentNumber;
    }

    public void setEnrolmentNumber(String enrolmentNumber) {
        this.enrolmentNumber = enrolmentNumber;
    }

    public String getEnrolmentAddress() {
        return enrolmentAddress;
    }

    public void setEnrolmentAddress(String enrolmentAddress) {
        this.enrolmentAddress = enrolmentAddress;
    }

    public String getVbNumber() {
        return vbNumber;
    }

    public void setVbNumber(String vbNumber) {
        this.vbNumber = vbNumber;
    }

    public String getVbName() {
        return vbName;
    }

    public void setVbName(String vbName) {
        this.vbName = vbName;
    }

    public List<SecurityDTO> getBlanketSecurities() {
        return blanketSecurities;
    }

    public void setBlanketSecurities(List<SecurityDTO> blanketSecurities) {
        this.blanketSecurities = blanketSecurities;
    }

    public YesNoEnum getDecision() {
        return decision;
    }

    public void setDecision(YesNoEnum decision) {
        this.decision = decision;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getUnitsToCover() {
        return unitsToCover;
    }

    public void setUnitsToCover(Integer unitsToCover) {
        this.unitsToCover = unitsToCover;
    }

    @Override
    public String toString() {
        return "CEEnrolmentDecisionDTO{" +
                "enrolmentNumber='" + enrolmentNumber + '\'' +
                ", enrolmentAddress='" + enrolmentAddress + '\'' +
                ", vbNumber='" + vbNumber + '\'' +
                ", vbName='" + vbName + '\'' +
                ", blanketSecurities=" + blanketSecurities +
                ", decision=" + decision +
                ", reason='" + reason + '\'' +
                ", unitsToCover=" + unitsToCover +
                '}';
    }
}
