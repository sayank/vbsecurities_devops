package com.tarion.vbs.common.enums;

public enum InterestPeriodTypeEnum {
	DAILY, WEEKLY, MONTHLY, QUARTERLY, YEARLY;
}
