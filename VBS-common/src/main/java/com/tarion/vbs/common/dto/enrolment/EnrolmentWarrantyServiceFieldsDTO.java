package com.tarion.vbs.common.dto.enrolment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class EnrolmentWarrantyServiceFieldsDTO implements Serializable{

	private static final long serialVersionUID = 1l;
	
	private String enrolmentNumber;
	private boolean wsInputRequested;
	private LocalDateTime wsRequestedDate;
	private String  wsRequestor;
	private LocalDateTime wsReceivedDate;
	private String wsInputProvidedBy;
	private String wsRecommendation;
	private BigDecimal fcmAmountRetained;
	
	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}
	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}

	public boolean isWsInputRequested() {
		return wsInputRequested;
	}
	public void setWsInputRequested(boolean wsInputRequested) {
		this.wsInputRequested = wsInputRequested;
	}
	public LocalDateTime getWsRequestedDate() {
		return wsRequestedDate;
	}
	public void setWsRequestedDate(LocalDateTime wsRequestedDate) {
		this.wsRequestedDate = wsRequestedDate;
	}
	public String getWsRequestor() {
		return wsRequestor;
	}
	public void setWsRequestor(String wsRequestor) {
		this.wsRequestor = wsRequestor;
	}
	public LocalDateTime getWsReceivedDate() {
		return wsReceivedDate;
	}
	public void setWsReceivedDate(LocalDateTime wsReceivedDate) {
		this.wsReceivedDate = wsReceivedDate;
	}
	public String getWsInputProvidedBy() {
		return wsInputProvidedBy;
	}
	public void setWsInputProvidedBy(String wsInputProvidedBy) {
		this.wsInputProvidedBy = wsInputProvidedBy;
	}
	public String getWsRecommendation() {
		return wsRecommendation;
	}
	public void setWsRecommendation(String wsRecommendation) {
		this.wsRecommendation = wsRecommendation;
	}

	public BigDecimal getFcmAmountRetained() {
		return fcmAmountRetained;
	}

	public void setFcmAmountRetained(BigDecimal fcmAmountRetained) {
		this.fcmAmountRetained = fcmAmountRetained;
	}

	@Override
	public String toString() {
		return "EnrolmentWarrantyServiceFieldsDTO ["
				+ "enrolmentNumber=" + enrolmentNumber
				+ ", wsInputRequested=" + wsInputRequested
				+ ", wsRequestedDate=" + wsRequestedDate
				+ ", wsRequestor=" + wsRequestor
				+ ", wsReceivedDate=" + wsReceivedDate
				+ ", wsInputProvidedBy=" + wsInputProvidedBy
				+ ", wsRecommendation=" + wsRecommendation + "]";
	}
	
	
}
