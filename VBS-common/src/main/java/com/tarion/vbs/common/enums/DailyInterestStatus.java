/* 
 * 
 * DailyInterestStatus.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.enums;

/**
 * Daily Interest Status enum used to mark status of daily interest calculation records
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-12-19
 * @version 1.0
 */
public enum DailyInterestStatus {
    CALCULATED, SENT, COMPLETED, COMPLETED_WITH_FMS_ERROR;
    

}
