package com.tarion.vbs.common.exceptions;

public class SaveValidationFailureException extends RuntimeException {


    private static final long serialVersionUID = -2361671413902631744L;

    private final String errorCode;

    public SaveValidationFailureException() {
        this.errorCode = null;
    }
    public SaveValidationFailureException(String msg) {
        super(msg);
        this.errorCode = null;
    }
    public SaveValidationFailureException(Throwable cause) {
        super(cause);
        this.errorCode = null;
    }
    public SaveValidationFailureException(String msg, String errorCode) {
        super(msg);
        this.errorCode = errorCode;
    }
    public SaveValidationFailureException(String msg, String errorCode, Throwable cause) {
        super(msg, cause);
        this.errorCode = errorCode;
    }
    public SaveValidationFailureException(String msg, Throwable cause) {
        super(msg, cause);
        this.errorCode = null;
    }

    public String getErrorCode() {
        return errorCode;
    }

}
