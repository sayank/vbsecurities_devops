package com.tarion.vbs.common.dto.contact;

import com.tarion.vbs.common.dto.AddressDTO;
import com.tarion.vbs.common.dto.NameDescriptionDTO;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ContactDTO implements Serializable{

	private static final long serialVersionUID = 8909315653095041844L;
	private Long id;
	private Long version;
	private AddressDTO address;
	private AddressDTO mailingAddress;
	private String crmContactId;
	private String emailAddress;
	private String phoneNumber;
	private String phoneNumberExt;
	private String firstName;
	private String lastName;
	private String companyName;
	private String sin;
	private String driversLicense;
	private String dob;
    private NameDescriptionDTO licenseStatus;
    private NameDescriptionDTO escrowAgentLicenseApplicationStatus;
    private LocalDateTime createDate;
    private String createUser;
    private LocalDateTime updateDate;
    private String updateUser;
    private String unwilling;
	private boolean accessibility;
	private boolean yellowSticky;
	private boolean primaryVb;
	private Long deleteReason;
	private String alert;
    private boolean apVendorFlag;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public AddressDTO getMailingAddress() {
        return mailingAddress;
    }

    public void setMailingAddress(AddressDTO mailingAddress) {
        this.mailingAddress = mailingAddress;
    }

    public String getCrmContactId() {
        return crmContactId;
    }

    public void setCrmContactId(String crmContactId) {
        this.crmContactId = crmContactId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumberExt() {
        return phoneNumberExt;
    }

    public void setPhoneNumberExt(String phoneNumberExt) {
        this.phoneNumberExt = phoneNumberExt;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getSin() {
        return sin;
    }

    public void setSin(String sin) {
        this.sin = sin;
    }

    public String getDriversLicense() {
        return driversLicense;
    }

    public void setDriversLicense(String driversLicense) {
        this.driversLicense = driversLicense;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getUnwilling() {
        return unwilling;
    }

    public void setUnwilling(String unwilling) {
        this.unwilling = unwilling;
    }

    public boolean isAccessibility() {
        return accessibility;
    }

    public void setAccessibility(boolean accessibility) {
        this.accessibility = accessibility;
    }

    public boolean isYellowSticky() {
        return yellowSticky;
    }

    public void setYellowSticky(boolean yellowSticky) {
        this.yellowSticky = yellowSticky;
    }

    public boolean isPrimaryVb() {
        return primaryVb;
    }

    public void setPrimaryVb(boolean primaryVb) {
        this.primaryVb = primaryVb;
    }

    public Long getDeleteReason() {
        return deleteReason;
    }

    public void setDeleteReason(Long deleteReason) {
        this.deleteReason = deleteReason;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public NameDescriptionDTO getLicenseStatus() {
        return licenseStatus;
    }

    public void setLicenseStatus(NameDescriptionDTO licenseStatus) {
        this.licenseStatus = licenseStatus;
    }

    public NameDescriptionDTO getEscrowAgentLicenseApplicationStatus() {
        return escrowAgentLicenseApplicationStatus;
    }

    public void setEscrowAgentLicenseApplicationStatus(NameDescriptionDTO escrowAgentLicenseApplicationStatus) {
        this.escrowAgentLicenseApplicationStatus = escrowAgentLicenseApplicationStatus;
    }

    public boolean getApVendorFlag() {
        return apVendorFlag;
    }

    public void setApVendorFlag(boolean apVendorFlag) {
        this.apVendorFlag = apVendorFlag;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Override
    public String toString() {
        return "ContactDTO{" +
                "id=" + id +
                ", version=" + version +
                ", address=" + address +
                ", mailingAddress=" + mailingAddress +
                ", crmContactId='" + crmContactId + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", phoneNumberExt='" + phoneNumberExt + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", companyName='" + companyName + '\'' +
                ", sin='" + sin + '\'' +
                ", driversLicense='" + driversLicense + '\'' +
                ", dob='" + dob + '\'' +
                ", unwilling='" + unwilling + '\'' +
                ", accessibility=" + accessibility +
                ", yellowSticky=" + yellowSticky +
                ", primaryVb=" + primaryVb +
                ", deleteReason='" + deleteReason + '\'' +
                ", alert='" + alert + '\'' +
                ", licenseStatus=" + licenseStatus +
                ", escrowAgentLicenseApplicationStatus=" + escrowAgentLicenseApplicationStatus +
                ", createDate=" + createDate +
                ", createUser='" + createUser + '\'' +
                ", updateDate=" + updateDate +
                ", updateUser='" + updateUser + '\'' +
                '}';
    }
}
