/* 
 * 
 * DateUtil.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.util;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.tarion.vbs.common.constants.VbsConstants;


/**
 * This method is a simple Date Utility class to handle some common date
 * functions
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-01-05
 * @version 1.0
 */
public class DateUtil {

    private static final String DATE_PATTERN_19 = "yyyy-MM-dd HH:mm:ss";
    private static final String DATE_PATTERN_SHORT_TIME = "yyyy-MM-dd HH:mm";
	private static final String DATE_PATTERN_LONG_DATE = "MMMM dd, yyyy";
	private static final String DATE_PATTERN_MEDIUM_DATE = "MMM dd, yyyy";
	private static final String DATE_PATTERN_CRM_XML = "yyyy-MM-dd-HH:mm:ss";
	private static final String DATE_PATTERN_SHORT_DATE = "yyyy-MM-dd";
	public static final ZoneId DEFAULT_ZONE_ID = ZoneId.of("America/Toronto");
	private static final String DATE_PATTERN_SQL = "yyyy-MM-dd HH:mm:ss.SSS";
	
	private DateUtil() {}
	
	public static LocalDateTime convertXMLGregorianToLocalDateTime(XMLGregorianCalendar xmlCal) {
        Date returnDate = null;
    	if (xmlCal != null) {
    		returnDate = xmlCal.toGregorianCalendar().getTime();
    	}
    	return returnDate != null ? LocalDateTime.ofInstant(returnDate.toInstant(), ZoneId.systemDefault()) : null;        
    }
	
	public static XMLGregorianCalendar getXMLGregorianCalendarNow() {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		try {
			DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
			return datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
		} catch (DatatypeConfigurationException e) {
			return null;
		}
	}
	
	public static String getCrmXMLDateStringFromLocalDateTime(LocalDateTime date) {
		if(date == null){return "";}
		return DateTimeFormatter.ofPattern(DATE_PATTERN_CRM_XML).format(date);
	}
	
	public static XMLGregorianCalendar getXMLGregorianCalendarFromLocalDateTime(LocalDateTime date) {
		if (date == null) {
			return null;
		}
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(Date.from(date.atZone(ZoneId.systemDefault()).toInstant()));
		XMLGregorianCalendar xmlDate = null;
		try {
			xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
		} catch (Exception e) {
			// format was incorrect, nothing we can do
		}
		return xmlDate;
	}
	
    public static LocalDateTime getLocalDateTimeFromCrmDateString(String crmDateString) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(VbsConstants.CRM_DATE_FORMAT);
		LocalDateTime returnTime = null;
		if (crmDateString != null && !crmDateString.isEmpty()) {
			try {
				LocalDate d = LocalDate.parse(crmDateString, formatter);
				if(d.getYear() != 2999) {
					returnTime = d.atStartOfDay();
				}
			} catch (Exception e) {
				return getDateFromCrmDateTimeString(crmDateString);
				// nothing else keep it a null and caller will deal with it
			}
		}
		return returnTime;
	}

	public static LocalDateTime getDateFromCrmDateTimeString(String crmDateString) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(VbsConstants.CRM_DATE_TIME_FORMAT);
		LocalDateTime returnTime = null;
		if (crmDateString != null && !crmDateString.isEmpty()) {
			try {
				returnTime = LocalDateTime.parse(crmDateString, formatter);
			} catch (Exception e) {
				return getDateFromCrmDateTimeNanoString(crmDateString);
				// nothing else keep it a null and caller will deal with it
			}
		}
		return returnTime;
	}
	
	public static LocalDateTime getDateFromCrmDateTimeNanoString(String crmDateString) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(VbsConstants.CRM_DATE_TIME_NANO_FORMAT);
		LocalDateTime returnTime = null;
		if (crmDateString != null && !crmDateString.isEmpty()) {
			try {
				returnTime = LocalDateTime.parse(crmDateString, formatter);
			} catch (Exception e) {
				LoggerUtil.logError(DateUtil.class, "getDateFromCrmDateTimeNanoString", e, crmDateString);
				// nothing else keep it a null and caller will deal with it
			}
		}
		return returnTime;
	}
	
	public static LocalDateTime getLocalDateTimeNow() {
		return LocalDateTime.now(DEFAULT_ZONE_ID);
	}
	
	public static LocalDateTime getLocalDateTimeStartOfCurrentMonth() {
		LocalDateTime now = getLocalDateTimeNow();
		now = now.truncatedTo(ChronoUnit.DAYS);
		return now.minusDays(now.getDayOfMonth() - 1l);
	}


	public static LocalDateTime getLocalDateFromDate(Date date) {
		return 	LocalDateTime.ofInstant(date.toInstant(),
				DEFAULT_ZONE_ID);
	}

	public static LocalDateTime getLocalDateTimeNextYear() {
		return getLocalDateTimeNow().plusYears(1).truncatedTo(ChronoUnit.DAYS);
	}

	public static LocalDateTime getLocalDateTimeStartOfYear(int year) {
		return LocalDateTime.of(year, Month.JANUARY, 1, 0, 0);
	}

	public static LocalDateTime getLocalDateTimeStartOfDay() {
		return getLocalDateTimeNow().truncatedTo(ChronoUnit.DAYS);
	}
	
	public static LocalDateTime getLocalDateTimeStartOfDay(LocalDateTime time) {
		if (time != null) {
			return time.truncatedTo(ChronoUnit.DAYS);
		} 
		return null;
	}

	public static LocalDateTime getLocalDateTimeEndOfDay() {
		return getLocalDateTimeStartOfDay().plusDays(1l).minusNanos(1l);
	}
	
	// cannot use builtin LocalDateTime.MIN becasue SQL Server MIN is not that early
	public static LocalDateTime getLocalDateTimeMin() {
		return LocalDateTime.of(1900, 1, 1, 0, 0);
	}

	// cannot use builtin LocalDateTime.MIN becasue SQL Server MAX is not that early
	public static LocalDateTime getLocalDateTimeMax() {
		return LocalDateTime.of(2900, 1, 1, 0, 0);
	}

	public static LocalDateTime getLocalDateTimeEndOfMonth(LocalDateTime date) {
		return getLocalDateTimeStartOfMonth(date).plusMonths(1l).minusNanos(1l);
	}
	public static LocalDateTime getLocalDateTimeStartOfMonth(LocalDateTime date) {
		return LocalDateTime.of(date.getYear(), date.getMonth(), 1, 0, 0);
	}

	public static LocalDateTime getLocalDateTimeEndOfQuarter(LocalDateTime date) {
		return getLocalDateTimeStartOfQuarter(date).plusMonths(3l).minusNanos(1l);
	}

	public static LocalDateTime getLocalDateTimeStartOfQuarter(LocalDateTime date) {
		int month = date.getMonthValue();
		return LocalDateTime.of(date.getYear(), getQuarterForMonth(month), 1, 0, 0);
	}
	
	public static int getQuarterForMonth(int monthInYear) {
		return ((monthInYear - 1) / 3 * 3) + 1;
	}

	public static LocalDateTime getLocalDateTimeEndOfYear(LocalDateTime date) {
		return LocalDateTime.of(date.getYear(), 1, 1, 0, 0).plusYears(1l).minusNanos(1l);
	}
	public static LocalDateTime getLocalDateTimeStartOfYear(LocalDateTime date) {
		return LocalDateTime.of(date.getYear(), 1, 1, 0, 0);
	}

	public static LocalDateTime getLocalDateTimeEndOfWeek(LocalDateTime date) {
		return getLocalDateTimeStartOfWeek(date).plusWeeks(1l).minusNanos(1l);
	}
	
	public static int getWeekOfYear(LocalDateTime date) {
		return date.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
	}
	
	public static LocalDateTime getLocalDateTimeStartOfWeek(LocalDateTime date) {
		LocalDateTime startOfWeek = date.truncatedTo(ChronoUnit.DAYS);
		long dayOfWeek = (long)date.getDayOfWeek().getValue();
		if (dayOfWeek == 7l) {
			dayOfWeek = 1l;
		} else {
			dayOfWeek++;
		}
		return startOfWeek.minusDays(dayOfWeek - 1l);
	}

	public static LocalDateTime getLocalDateTimeFromTimestamp(Timestamp timestamp) {
		LocalDateTime returnValue = null;
		if (timestamp != null) {
			returnValue = timestamp.toLocalDateTime();
		}
		return returnValue;
	}

	public static String getDateNowFormattedForCm() {
		return format(LocalDateTime.now(), VbsConstants.CRM_DATE_FORMAT);
	}
	
	public static String getDateNowFormattedForCmLong() {
		return format(LocalDateTime.now(), DATE_PATTERN_19);
	}


	public static String getDateFormattedShortTime(LocalDateTime date){
		return format(date, DATE_PATTERN_SHORT_TIME);
	}

	public static String getDateFormattedForFms(LocalDateTime date) {
		return format(date, VbsConstants.FMS_DATE_FORMAT);
	}

	public static String getDateFormattedForFileName(LocalDateTime date) {
		return format(date, VbsConstants.FILE_DATE_FORMAT);
	}

	public static String getDateFormattedForCrm(LocalDateTime date) {
		return format(date, VbsConstants.CRM_DATE_FORMAT);
	}

	public static String getDateFormattedForCrmLong(LocalDateTime date) {
		return format(date, VbsConstants.CRM_DATE_TIME_FORMAT);
	}

	
	public static String getDateFormattedLong(LocalDateTime date) {
		return format(date, DATE_PATTERN_LONG_DATE);
	}
	
	public static String getDateFormattedMedium(LocalDateTime date) {
		return format(date, DATE_PATTERN_MEDIUM_DATE);
	}
	
	public static String getDateFormattedShort(LocalDateTime date) {
		return format(date, DATE_PATTERN_SHORT_DATE);
	}

	public static String getDateFormattedSQL(LocalDateTime date) {
		if(date == null) {
			return "NULL";
		}
		return "'" + format(date, DATE_PATTERN_SQL) + "'";
	}

	private static String format(LocalDateTime date, String pattern){
		if (date == null) { return null; }
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		return date.format(formatter);
	}

	public static boolean isDateGreaterThanToday(LocalDateTime date) {
		return date.isAfter(getLocalDateTimeNow());
	}
	public static boolean isDateBeforeToday(LocalDateTime date) {
		return date.isBefore(getLocalDateTimeNow());
	}

	public static Date getDateFromLocalDateTime(LocalDateTime d){
		if(d == null){return null;}
		return Date.from(d.atZone(ZoneId.systemDefault()).toInstant());
	}
	
//	public static Date convertEnversRevisionDateToEasternTime(Date date) {
//		LocalDateTime converted = LocalDateTime.ofInstant(date.toInstant(),ZoneId.of("Z"));
//		return getDateFromLocalDateTime(converted);
//	}
	
}
