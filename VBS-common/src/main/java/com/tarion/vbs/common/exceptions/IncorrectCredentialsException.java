package com.tarion.vbs.common.exceptions;

public class IncorrectCredentialsException extends VbsCheckedException {

	private static final long serialVersionUID = 1L;

	public IncorrectCredentialsException() {

    }
    public IncorrectCredentialsException(String msg) {
        super(msg);
    }
    public IncorrectCredentialsException(Throwable cause) {
        super(cause);
    }
    public IncorrectCredentialsException(String msg, String errorCode) {
        super(msg, errorCode);
    }
    public IncorrectCredentialsException(String msg, String errorCode, Throwable cause) {
        super(msg, errorCode, cause);
    }
    public IncorrectCredentialsException(String msg, Throwable cause) {
        super(msg, cause);
    }
    
}