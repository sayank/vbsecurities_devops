package com.tarion.vbs.common.dto.tip;

import java.io.Serializable;

public class ReleaseApproverDTO implements Serializable {

    private static final long serialVersionUID = -6637893225755966510L;

    private String userName;
    private String name;
    private String email;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "ReleaseApproverDTO{" +
                "userName='" + userName + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
