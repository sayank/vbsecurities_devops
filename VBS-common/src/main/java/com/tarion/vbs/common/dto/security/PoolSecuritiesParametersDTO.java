/* 
 * 
 * SecuritySearchParametersDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.security;

import java.io.Serializable;
import java.util.List;

/**
 * Pool Securities DTO class used as transport for Pool Security parameters
 *
 * @author <A href="djordje.przulj@tarion.com">Djordje Przulj</A>
 * @since 2019-07-03
 * @version 1.0
 */
public class PoolSecuritiesParametersDTO implements Serializable {

	private static final long serialVersionUID = 23L;
	
	private List<Long> securityIds;
	private Long poolId;
	private Long unitsIntendedToCover;	
	private Long securityPurposeId;

	public List<Long> getSecurityIds() {
		return securityIds;
	}

	public void setSecurityIds(List<Long> securityIds) {
		this.securityIds = securityIds;
	}

	public Long getUnitsIntendedToCover() {
		return unitsIntendedToCover;
	}

	public void setUnitsIntendedToCover(Long unitsIntendedToCover) {
		this.unitsIntendedToCover = unitsIntendedToCover;
	}

	public Long getSecurityPurposeId() {
        return securityPurposeId;
    }

    public void setSecurityPurposeId(Long securityPurposeId) {
        this.securityPurposeId = securityPurposeId;
    }

    public Long getPoolId() {
        return poolId;
    }

    public void setPoolId(Long poolId) {
        this.poolId = poolId;
    }

    @Override
    public String toString() {
        return "PoolSecuritiesParametersDTO{" +
                "securityIds=" + securityIds + 
                ", poolId=" + poolId +
                ", unitsIntendedToCover=" + unitsIntendedToCover +
                ", securityPurposeId=" + securityPurposeId +
                '}';
    }
}
