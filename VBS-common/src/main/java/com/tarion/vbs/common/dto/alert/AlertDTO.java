package com.tarion.vbs.common.dto.alert;

import com.tarion.vbs.common.enums.AlertStatus;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Alert DTO provides operations for alert
 *
 * @author <A href="djordje.przulj@tarion.com">Djordje Przulj</A>
 * @version 1.0
 * @since 2019-04-16
 */
public class AlertDTO implements Serializable {

	private static final long serialVersionUID = 2421268946686567929L;

	private Long id;
	private Long version;

	private Long securityId;
	private Long alertTypeId;
	
	protected String name;
	protected String description;
	
    private LocalDateTime createDate;
	private String createUser;
	private LocalDateTime updateDate;
	private String updateUser;

	private LocalDateTime startDate;
	private LocalDateTime endDate;
	private String endUser;
	
	private AlertStatus status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getSecurityId() {
		return securityId;
	}

	public void setSecurityId(Long securityId) {
		this.securityId = securityId;
	}

	public Long getAlertTypeId() {
		return alertTypeId;
	}

	public void setAlertTypeId(Long alertTypeId) {
		this.alertTypeId = alertTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public AlertStatus getStatus() {
		return status;
	}

	public void setStatus(AlertStatus status) {
		this.status = status;
	}
	
	public String getEndUser() {
		return endUser;
	}

	public void setEndUser(String endUser) {
		this.endUser = endUser;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		AlertDTO alertDTO = (AlertDTO) o;
		return id.equals(alertDTO.id) &&
				securityId.equals(alertDTO.securityId) &&
				alertTypeId.equals(alertDTO.alertTypeId) &&
				Objects.equals(name, alertDTO.name) &&
				Objects.equals(description, alertDTO.description) &&
				Objects.equals(startDate, alertDTO.startDate) &&
				Objects.equals(endDate, alertDTO.endDate) &&
				Objects.equals(endUser, alertDTO.endUser) &&
				status == alertDTO.status;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, securityId, alertTypeId, name, description, startDate, endDate, endUser, status);
	}

	@Override
	public String toString() {
		return "AlertDTO{" +
				"id=" + id +
				", version=" + version +
				", securityId=" + securityId +
				", alertTypeId=" + alertTypeId +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", createDate=" + createDate +
				", createUser='" + createUser + '\'' +
				", updateDate=" + updateDate +
				", updateUser='" + updateUser + '\'' +
				", startDate=" + startDate +
				", endDate=" + endDate +
				", endUser=" + endUser +				
				", status=" + status +
				'}';
	}
}
