package com.tarion.vbs.common.contact;

import java.io.Serializable;
import java.util.List;

public class EscrowAgnetSearchResultWrapper  implements Serializable{

	private static final long serialVersionUID = -7049096954079165252L;

	private String queryString;
	private List<EscrowAgentSearchResultDTO> escrowAgents;

	
	public List<EscrowAgentSearchResultDTO> getEscrowAgents() {
		return escrowAgents;
	}

	public void setEscrowAgents(List<EscrowAgentSearchResultDTO> escrowAgents) {
		this.escrowAgents = escrowAgents;
	}

	public String getQueryString() {
		return queryString;
	}

	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}
}
