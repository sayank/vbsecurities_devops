/* 
 * 
 * LoggerUtil.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Logger Utility class contains utility methods for easy logging
 * 
 */
public class LoggerUtil {
	
	private LoggerUtil() {}
	private static final String EXCEPTION_ERROR = "Exception in Service Layer: {} with parameters:";
	
	public static <T> long logEnter(Class<T> clazz, String methodName, Object... objects) {
		Logger logger = LoggerFactory.getLogger(clazz);
		logger.debug("ENTER: {}({})", methodName, objects);
		return System.currentTimeMillis();
	}
	
	public static <T> void logExit(Class<T> clazz, String methodName, long start, Object ret) {
		long runTime = System.currentTimeMillis() - start;
		Logger logger = LoggerFactory.getLogger(clazz);
		logger.debug("EXIT: {} returning({}) in {}ms", methodName, ret, runTime);
	}
	
	public static <T> void logExit(Class<T> clazz, String methodName, long start) {
		long runTime = System.currentTimeMillis() - start;
		Logger logger = LoggerFactory.getLogger(clazz);
		logger.debug("EXIT: {} in {}ms", methodName, runTime);
	}
	
	public static <T> void logInfo(Class<T> clazz, String message) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.info(message);
	}
	public static <T> void logWarn(Class<T> clazz, String message) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.warn(message);
	}
	public static <T> void logDebug(Class<T> clazz, String message) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.debug(message);
	}

	public static <T> void logError(Class<T> clazz, String methodName, Throwable ex, Object... objects) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.error(EXCEPTION_ERROR, methodName);
		if (objects != null && objects.length > 0) {
			for (Object o : objects) {
				logger.error("{}", (o == null) ? "null" : o);
			}
		}
		logger.error("Original exception was ", ex);
	}
	
	public static <T> void logError(Class<T> clazz, String methodName, String message) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.error("Exception in Service Layer: {} with parameters: {}", methodName, message);
	}

	public static <T> void logError(Class<T> clazz, String methodName) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.error(EXCEPTION_ERROR, methodName);
	}
	
	public static <T> void logErrorMessage(Class<T> clazz, String message) {
	    Logger logger = LoggerFactory.getLogger(clazz);
		logger.error(message);
	}

	
	/**
	 * Prints method entry message, with parameters, at debug level.
	 */
	public static void logDebugEnter(Logger logger, String methodName, Object... params) {
		if(logger.isDebugEnabled()) { logger.debug("ENTER: {}", buildMessage(methodName, params));}
	}
	/**
	 * Prints method exit message, at debug level, without calculating execution time.
	 */
	public static void logDebugExit(Logger logger, String methodName, Object returnValue) {
		logger.debug("EXIT: {} > {}", methodName, returnValue);
	}

	/**
	 * Log debug exit for void methods
	 */
	public static void logDebugExit(Logger logger, String methodName) {
		logger.debug("EXIT: {}", methodName);
	}
	
	private static String buildMessage(String methodName, Object[] params) {
		StringBuilder parameters = new StringBuilder(methodName).append("(");
		if (params.length > 0){
			parameters.append("param");
		}
		int paramCounter = 1;
		for (Object o : params) {
			if (paramCounter > 1) {
				parameters.append(", param");
			}
			parameters.append(paramCounter).append("=").append((o == null ? "null" : o.toString()));
			paramCounter++;
		}
		parameters.append(")");
		return parameters.toString();
	}

	public static void logError(Logger logger, String methodName, Throwable ex, Object... objects) {
		logger.error(EXCEPTION_ERROR, methodName);
		if (objects != null && objects.length > 0) {
			for (Object o : objects) {
				logger.error("{}", (o == null) ? "null" : o);
			}
		}
		logger.error("Original exception was ", ex);
	}

	public static void logError(Logger logger, String message) {
		logger.error(message);
	}

	public static void logInfo(Logger logger, String message) {
		logger.info(message);
	}

	public static void logDebug(Logger logger, String message) {
		logger.debug(message);
	}

	public static void logInfoExit(Logger logger, String methodName) {
		logger.info("EXIT: {}", methodName);
	}

}
