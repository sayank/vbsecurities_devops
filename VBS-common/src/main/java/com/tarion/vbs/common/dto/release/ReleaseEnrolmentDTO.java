package com.tarion.vbs.common.dto.release;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tarion.vbs.common.dto.enrolment.EnrolmentDTO;
import com.tarion.vbs.common.enums.ReleaseApprovalStatusEnum;

/**
 * Release Enrolment DTO provides operations for release 
 *
 * @author <A href="shadi.kajevand@tarion.com">Shadi Kajevand</A>
 * @since 2018-12-1
 * @version 1.0
 */
public class ReleaseEnrolmentDTO implements Serializable {

	private static final long serialVersionUID = -955477992433814509L;

	private Long id;
	private Long version;
	private Long releaseId;
	private EnrolmentDTO enrolment;
	private BigDecimal authorizedAmount;
	private BigDecimal requestAmount;
	private LocalDateTime analystApprovalDateTime;
	private String analystApprovedBy;
	private String analystApprovalSentTo;

	private LocalDateTime analystApprovedDate;
	private ReleaseApprovalStatusEnum analystApprovalStatus;
	
	private Long rejectReason;
	private Boolean fullRelease;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public Long getReleaseId() {
		return releaseId;
	}
	public void setReleaseId(Long releaseId) {
		this.releaseId = releaseId;
	}
	
	
	public LocalDateTime getAnalystApprovalDateTime() {
		return analystApprovalDateTime;
	}
	public void setAnalystApprovalDateTime(LocalDateTime analystApprovalDateTime) {
		this.analystApprovalDateTime = analystApprovalDateTime;
	}
	public EnrolmentDTO getEnrolment() {
		return enrolment;
	}
	public void setEnrolment(EnrolmentDTO enrolment) {
		this.enrolment = enrolment;
	}

	public BigDecimal getAuthorizedAmount() {
		return authorizedAmount;
	}
	public void setAuthorizedAmount(BigDecimal authorizedAmount) {
		this.authorizedAmount = authorizedAmount;
	}
	public String getAnalystApprovedBy() {
		return analystApprovedBy;
	}
	public void setAnalystApprovedBy(String analystApprovedBy) {
		this.analystApprovedBy = analystApprovedBy;
	}
	public String getAnalystApprovalSentTo() {
		return analystApprovalSentTo;
	}
	public void setAnalystApprovalSentTo(String analystApprovalSentTo) {
		this.analystApprovalSentTo = analystApprovalSentTo;
	}

	public LocalDateTime getAnalystApprovedDate() {
		return analystApprovedDate;
	}
	public void setAnalystApprovedDate(LocalDateTime analystApprovedDate) {
		this.analystApprovedDate = analystApprovedDate;
	}
	public BigDecimal getRequestAmount() {
		return requestAmount;
	}
	public void setRequestAmount(BigDecimal requestAmount) {
		this.requestAmount = requestAmount;
	}

	public ReleaseApprovalStatusEnum getAnalystApprovalStatus() {
		return analystApprovalStatus;
	}

	public void setAnalystApprovalStatus(ReleaseApprovalStatusEnum analystApprovalStatus) {
		this.analystApprovalStatus = analystApprovalStatus;
	}

	public Long getRejectReason() {
		return rejectReason;
	}
	public void setRejectReason(Long rejectReason) {
		this.rejectReason = rejectReason;
	}
	public Boolean isFullRelease() {
		return fullRelease;
	}
	public void setFullRelease(Boolean fullRelease) {
		this.fullRelease = fullRelease;
	}
}
