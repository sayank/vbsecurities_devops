package com.tarion.vbs.common.contact;

import java.io.Serializable;

import com.tarion.vbs.common.dto.contact.ContactDTO;

public class EscrowAgentLawyerAssistantDTO implements Serializable{


	private static final long serialVersionUID = 1L;

	private ContactDTO escrowAgent;
	private ContactDTO lawyer;
	private ContactDTO assistant;
	
	public ContactDTO getEscrowAgent() {
		return escrowAgent;
	}
	public void setEscrowAgent(ContactDTO escrowAgent) {
		this.escrowAgent = escrowAgent;
	}
	public ContactDTO getLawyer() {
		return lawyer;
	}
	public void setLawyer(ContactDTO lawyer) {
		this.lawyer = lawyer;
	}
	public ContactDTO getAssistant() {
		return assistant;
	}
	public void setAssistant(ContactDTO assistant) {
		this.assistant = assistant;
	}
}
