package com.tarion.vbs.common.contact;

import com.tarion.vbs.common.enums.SearchTypeEnum;

import java.io.Serializable;

public class EscrowAgentSearchParamsDTO implements Serializable {

	private static final long serialVersionUID = 706463850764992076L;

	private String contactId;
	private String contactIdTo;
	private String escrowAgentName;
	private SearchTypeEnum escrowAgentLicensStatusSearchType;
	private SearchTypeEnum licensApplicationStatusSearchType;
	private Long escrowAgentLicensStatusId;
	private Long licensApplicationStatusId;
	private SearchTypeEnum contactIdSearchType;
	private SearchTypeEnum escrowAgentNameSearchType;
	public String getContactId() {
		return contactId;
	}
	public void setContactId(String contactId) {
		this.contactId = contactId;
	}
	public String getContactIdTo() {
		return contactIdTo;
	}
	public void setContactIdTo(String contactIdTo) {
		this.contactIdTo = contactIdTo;
	}

	public String getEscrowAgentName() {
		return escrowAgentName;
	}
	public void setEscrowAgentName(String escrowAgentName) {
		this.escrowAgentName = escrowAgentName;
	}
	public Long getEscrowAgentLicensStatusId() {
		return escrowAgentLicensStatusId;
	}
	public void setEscrowAgentLicensStatusId(Long escrowAgentLicensStatusId) {
		this.escrowAgentLicensStatusId = escrowAgentLicensStatusId;
	}
	public Long getLicensApplicationStatusId() {
		return licensApplicationStatusId;
	}
	public void setLicensApplicationStatusId(Long licensApplicationStatusId) {
		this.licensApplicationStatusId = licensApplicationStatusId;
	}
	public SearchTypeEnum getEscrowAgentLicensStatusSearchType() {
		return escrowAgentLicensStatusSearchType;
	}
	public void setEscrowAgentLicensStatusSearchType(SearchTypeEnum escrowAgentLicensStatusSearchType) {
		this.escrowAgentLicensStatusSearchType = escrowAgentLicensStatusSearchType;
	}
	public SearchTypeEnum getLicensApplicationStatusSearchType() {
		return licensApplicationStatusSearchType;
	}
	public void setLicensApplicationStatusSearchType(SearchTypeEnum licensApplicationStatusSearchType) {
		this.licensApplicationStatusSearchType = licensApplicationStatusSearchType;
	}
	public SearchTypeEnum getContactIdSearchType() {
		return contactIdSearchType;
	}
	public void setContactIdSearchType(SearchTypeEnum contactIdSearchType) {
		this.contactIdSearchType = contactIdSearchType;
	}
	public SearchTypeEnum getEscrowAgentNameSearchType() {
		return escrowAgentNameSearchType;
	}
	public void setEscrowAgentNameSearchType(SearchTypeEnum escrowAgentNameSearchType) {
		this.escrowAgentNameSearchType = escrowAgentNameSearchType;
	}
	
}
