/* 
 * 
 * ReleaseSearchParametersDTO.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.common.dto.release;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import com.tarion.vbs.common.enums.ReleaseStatus;
import com.tarion.vbs.common.enums.SearchTypeEnum;

/**
 * Release Search DTO class used as transport for Release Search parameters
 *
 * @author <A href="sbogicevic@tarion.com">Srdjan Bogicevic</A>
 * @since 2019-04-18
 * @version 1.0
 */
public class ReleaseSearchParametersDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private LocalDateTime releaseUpdateStartDate;
	private LocalDateTime releaseUpdateEndDate;
    private SearchTypeEnum dateSearchType;
    private List<ReleaseTypeDTO> releaseTypes;
    private ReleaseStatus releaseStatus;

    public LocalDateTime getReleaseUpdateStartDate() {
		return releaseUpdateStartDate;
	}

	public void setReleaseUpdateStartDate(LocalDateTime releaseUpdateStartDate) {
		this.releaseUpdateStartDate = releaseUpdateStartDate;
	}

	public LocalDateTime getReleaseUpdateEndDate() {
		return releaseUpdateEndDate;
	}

	public void setReleaseUpdateEndDate(LocalDateTime releaseUpdateEndDate) {
		this.releaseUpdateEndDate = releaseUpdateEndDate;
	}

	public SearchTypeEnum getDateSearchType() {
		return dateSearchType;
	}

	public void setDateSearchType(SearchTypeEnum dateType) {
		this.dateSearchType = dateType;
    }
    
    public List<ReleaseTypeDTO> getReleaseTypes() {
        return this.releaseTypes;
    }

    public void setReleaseTypes(List<ReleaseTypeDTO> releaseTypes) {
        this.releaseTypes = releaseTypes;
    }

    public ReleaseStatus getReleaseStatus() {
        return this.releaseStatus;
    }

    public void setReleaseStatus(ReleaseStatus releaseStatus) {
        this.releaseStatus = releaseStatus;
    }

	@Override
    public String toString() {
        return "ReleaseSearchParametersDTO{" +
                " releaseUpdateStartDate=" + releaseUpdateStartDate +
                ", releaseUpdateEndDate=" + releaseUpdateEndDate +
                ", dateSearchType=" + dateSearchType +
                '}';
    }
}
