package com.tarion.vbs.enrolment;

import com.tarion.vbs.common.dto.enrolment.EnrolmentSecurityInfoDTO;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentPoolEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;

import javax.persistence.Tuple;
import java.util.List;

public interface EnrolmentDAO {

	List<EnrolmentPoolEntity> getEnrolmentsByPoolId(Long poolId);
	List<String> getAllEnrolmentsLike(String enrolmentNumber);
	List<Tuple> getAvailableEnrolmentsByPoolId(Long poolId, boolean onlyCondoConv, Long securityIdentityTypeId);
	List<EnrolmentPoolEntity> getEnrolmentPoolByEnrolmentNumber(List<String> enrolmentNumberList, Long poolId);
	boolean isMultiSecurity(String enrolmentNumber);
	EnrolmentEntity getEnrolmentByEnrolmentNumberPoolId(String enrolmentNumber, Long poolId, Long securityIdentityTypeId);
    long countSecuritiesByEnrolmentNumber(String enrolmentNumber);
	List<SecurityEntity> getSecuritiesByEnrolmentNumber(String enrolmentNumber);

	EnrolmentEntity getEnrolmentByEnrolmentNumberSecurityId(String enrolmentNumber, Long securityId);

	List<EnrolmentEntity> getUnreleasedEnrolmentsForSecurity(Long securityId);
	long getPoolUnitsToCoverByEnrolmentNumber(String enrolmentNumber);
	List<EnrolmentSecurityInfoDTO> getEnrolmentSecurityInfo(List<String> enrolments);
	List<EnrolmentEntity> getEnrolmentsByEnrolmentNumber(List<String> enrolmentNumbers);
	List<String> getCurrentEnrolmentsIn(List<String> enrolmentNumbers);
	List<String> getEnrolmentNumbersInAPoolByPoolId(Long poolId);
	public List<EnrolmentPoolEntity> getEnrolmentPoolByPoolIdAndVbNumber(Long poolId, String vbNumber);
    boolean poolIsLinkedToMultipleCE(Long poolId);
}
