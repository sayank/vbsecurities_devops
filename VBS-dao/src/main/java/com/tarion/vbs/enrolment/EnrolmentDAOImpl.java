package com.tarion.vbs.enrolment;

import java.time.LocalDateTime;
import java.util.*;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.*;

import org.hibernate.transform.Transformers;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.enrolment.EnrolmentPairDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentSecurityInfoDTO;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentPoolEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;

/**SELECT_ENROLMENT_BY_POOL_ID
 * EnrolmentDAO class for getting all enrolment data
 *
 * @author <A href="shadi.kajevand@tarion.com">Shadi Kajevand</A>
 * @since 2018-11-29
 * @version 1.0
 */

@Stateless(mappedName = "ejb/EnrolmentDAO")
@Interceptors(EjbLoggingInterceptor.class)
public class EnrolmentDAOImpl implements EnrolmentDAO {
	
	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;
	
	@Override
	@SuppressWarnings("unchecked")
	public List<EnrolmentPoolEntity> getEnrolmentsByPoolId(Long poolId) {
		Query query = vbsEm.createNativeQuery(EnrolmentPoolEntity.SELECT_ENROLMENT_BY_POOL_ID, EnrolmentPoolEntity.class);
		query.setParameter(1, poolId);
		return query.getResultList();
	}

	@Override
	public List<String> getEnrolmentNumbersInAPoolByPoolId(Long poolId) {
		TypedQuery<String> query = vbsEm.createNamedQuery(EnrolmentPoolEntity.SELECT_ENROLMENT_NUMBERS_BY_POOL_ID, String.class);
		query.setParameter(1, poolId);
		return query.getResultList();
	}

	@Override
	public List<EnrolmentPoolEntity> getEnrolmentPoolByEnrolmentNumber(List<String> enrolmentNumberList, Long poolId) {
		TypedQuery<EnrolmentPoolEntity> query = vbsEm.createNamedQuery(EnrolmentPoolEntity.SELECT_ENROLMENT_POOL_BY_ENROLMENT_NUMBER, EnrolmentPoolEntity.class);
		query.setParameter(1, enrolmentNumberList);
		query.setParameter(2, poolId);
		return query.getResultList();
	}

	@Override
	public List<EnrolmentPoolEntity> getEnrolmentPoolByPoolIdAndVbNumber(Long poolId, String vbNumber) {
		TypedQuery<EnrolmentPoolEntity> query = vbsEm.createNamedQuery(EnrolmentPoolEntity.SELECT_ENROLMENT_POOL_BY_POOL_ID_AND_VB_NUMBER, EnrolmentPoolEntity.class);
		query.setParameter(1, poolId);
		query.setParameter(2, vbNumber);
		return query.getResultList();
	}


	@Override
	public List<String> getAllEnrolmentsLike(String enrolmentNumber) {
		TypedQuery<String> query = vbsEm.createNamedQuery(EnrolmentEntity.SELECT_ALL_ENROLMENT_NUMBERS_LIKE, String.class).setMaxResults(VbsConstants.TYPEAHEAD_MAX_RESULTS);
		query.setParameter(1, VbsUtil.getSearchLikePrefixString(enrolmentNumber));
		return query.getResultList();
	}
	

	@Override
	public List<Tuple> getAvailableEnrolmentsByPoolId(Long poolId, boolean onlyCondoConv, Long securityIdentityTypeId) {
		TypedQuery<Tuple> query = vbsEm.createNamedQuery(EnrolmentEntity.SELECT_AVAILABLE_ENROLMENTS_BY_POOLID, Tuple.class);
		query.setParameter(1, LocalDateTime.now().minusYears(3));
		query.setParameter(2, poolId);
		query.setParameter(3, onlyCondoConv);
		List<String> allowedTypes = new ArrayList<>();
		if(securityIdentityTypeId != null && securityIdentityTypeId == 1 ) {
			allowedTypes.add(VbsConstants.ENROLMENT_UNIT_TYPE_CE);
		} else if(securityIdentityTypeId != null && securityIdentityTypeId == 2) {
			allowedTypes.add(VbsConstants.ENROLMENT_UNIT_TYPE_FH);
		}else {
			allowedTypes.add(VbsConstants.ENROLMENT_UNIT_TYPE_CE);
			allowedTypes.add(VbsConstants.ENROLMENT_UNIT_TYPE_FH);
		}
		return query.setParameter(4, allowedTypes).getResultList();
	}

	@Override
	public long countSecuritiesByEnrolmentNumber(String enrolmentNumber) {
		Query query = vbsEm.createNamedQuery(EnrolmentPoolEntity.SELECT_NUMBER_OF_SECURITY_BY_ENROLMENT_NUMBER);
		query.setParameter(1, enrolmentNumber);
		return (long)query.getSingleResult();
	}

	@Override
	public List<SecurityEntity> getSecuritiesByEnrolmentNumber(String enrolmentNumber) {
		TypedQuery<SecurityEntity> query = vbsEm.createNamedQuery(EnrolmentPoolEntity.SELECT_SECURITY_BY_ENROLMENT_NUMBER, SecurityEntity.class);
		query.setParameter(1, enrolmentNumber);
		return query.getResultList();
	}
	
	@Override
	public EnrolmentEntity getEnrolmentByEnrolmentNumberPoolId(String enrolmentNumber, Long poolId, Long securityIdentityTypeId) {
		TypedQuery<EnrolmentEntity> query = vbsEm.createNamedQuery(EnrolmentEntity.SELECT_ENROLMENT_BY_ENROLMENT_NUMBER_POOLID, EnrolmentEntity.class);
		query.setParameter(1, enrolmentNumber);
		query.setParameter(2, poolId);
		List<String> allowedTypes = new ArrayList<>();
		if(securityIdentityTypeId != null && securityIdentityTypeId == 1 ) {
			allowedTypes.add(VbsConstants.ENROLMENT_UNIT_TYPE_CE);
		}else if(securityIdentityTypeId != null && securityIdentityTypeId == 2) {
			allowedTypes.add(VbsConstants.ENROLMENT_UNIT_TYPE_FH);
		}else if(securityIdentityTypeId != null && securityIdentityTypeId == 3) {
			allowedTypes.add(VbsConstants.ENROLMENT_UNIT_TYPE_CE);
			allowedTypes.add(VbsConstants.ENROLMENT_UNIT_TYPE_FH);
		}else {
			allowedTypes.add(VbsConstants.ENROLMENT_UNIT_TYPE_CE);
			allowedTypes.add(VbsConstants.ENROLMENT_UNIT_TYPE_FH);
			allowedTypes.add(VbsConstants.ENROLMENT_UNIT_TYPE_CE_UNIT);
		}
		query.setParameter(3, allowedTypes);

		List<EnrolmentEntity> result = query.getResultList();
		if(result.isEmpty()) {
			return null;
		}
		return result.get(0);
	}

	@Override
	public EnrolmentEntity getEnrolmentByEnrolmentNumberSecurityId(String enrolmentNumber, Long securityId) {
		TypedQuery<EnrolmentEntity> query = vbsEm.createNamedQuery(EnrolmentEntity.SELECT_ENROLMENT_FOR_SECURITY, EnrolmentEntity.class);
		query.setParameter(1, securityId);
		query.setParameter(2, enrolmentNumber);
		List<EnrolmentEntity> result = query.getResultList();
		if(result.isEmpty()) {
			return null;
		}
		return result.get(0);
	}



	@Override
	public List<EnrolmentEntity> getUnreleasedEnrolmentsForSecurity(Long securityId) {
		TypedQuery<EnrolmentEntity> query = vbsEm.createNamedQuery(EnrolmentEntity.SELECT_UNRELEASED_ENROLMENTS_FOR_SECURITY, EnrolmentEntity.class);
		query.setParameter(1, securityId);
		return query.getResultList();
	}

	@Override
	public boolean isMultiSecurity(String enrolmentNumber) {
		Query query = vbsEm.createNamedQuery(EnrolmentPoolEntity.SELECT_MULTI_SECURITY);
		query.setParameter(1, enrolmentNumber);
		Long count = (Long)query.getSingleResult();
		return count > 1;
	}

	@Override
	public long getPoolUnitsToCoverByEnrolmentNumber(String enrolmentNumber) {
		Query query = vbsEm.createNamedQuery(EnrolmentPoolEntity.SELECT_POOL_UNITS_TO_COVER_BY_ENROLMENT_NUMBER);
		query.setParameter(1, enrolmentNumber);
		Long count = (Long)query.getSingleResult();
		return count == null ? 0 : count;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<EnrolmentEntity> getEnrolmentsByEnrolmentNumber(List<String> enrolmentNumbers) {
		Query query = vbsEm.createNamedQuery(EnrolmentEntity.SELECT_ENROLMENTS_BY_ENROLMENT_NUMBER);
		query.setParameter(1, enrolmentNumbers);
		return query.getResultList();
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<EnrolmentSecurityInfoDTO> getEnrolmentSecurityInfo(List<String> enrolments) {

		Query query = vbsEm.createNamedQuery(EnrolmentPoolEntity.SELECT_ENROLMENT_SECURITY_DATA);
		query.setParameter(1, enrolments);		
		//Replaced by @FunctionalInterface in Hibernate 6.0, removed in 5.3, both require JPA 2.2 which we cannot use on Weblogic, thus not an issue.
		List<EnrolmentSecurityInfoDTO> result = query.unwrap( org.hibernate.query.NativeQuery.class).setResultTransformer(Transformers.aliasToBean( EnrolmentSecurityInfoDTO.class)).getResultList();
		
		Map<String, EnrolmentSecurityInfoDTO> resultMap = new HashMap<>(enrolments.size());
		
		for (EnrolmentSecurityInfoDTO dto : result) {
			resultMap.put(dto.getEnrolmentNumber(), dto);
		}
		
		query = vbsEm.createNamedQuery(EnrolmentPoolEntity.SELECT_CE_ENROLMENTS_SAME_POOL);
		query.setParameter(1, enrolments);		
		List<EnrolmentPairDTO> ceEnrolmentsInPool = query.unwrap( org.hibernate.query.NativeQuery.class).setResultTransformer(Transformers.aliasToBean( EnrolmentPairDTO.class)).getResultList();
		for (EnrolmentPairDTO pairDTO : ceEnrolmentsInPool) {
			EnrolmentSecurityInfoDTO dto = resultMap.get(pairDTO.getEnrolmentNumber());
			if(dto != null) {
				dto.getPoolEnrolments().getEnrolmentsInPool().add(pairDTO.getPoolEnrolmentNumber());
			}
		}		
		
		return new ArrayList<>(resultMap.values());
	}

	@Override
	public List<String> getCurrentEnrolmentsIn(List<String> enrolmentNumbers) {
		Query query = vbsEm.createNamedQuery(EnrolmentEntity.SELECT_CURRENT_ENROLMENT_NUMBERS_IN);
		query.setParameter(1, enrolmentNumbers);
		return query.getResultList();
	}

	@Override
	public boolean poolIsLinkedToMultipleCE(Long poolId){
		Query query = vbsEm.createNamedQuery(EnrolmentEntity.SELECT_COUNT_CE_ENROLMENTS_FOR_POOL_ID);
		query.setParameter(1, poolId);
		Long count = (Long) query.getSingleResult();
		return count > 1;
	}
}
