/* 
 * 
 * EjbLoggingInterceptor.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.util;

import java.lang.reflect.Method;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This interceptor is used for looging ENTRY/EXIT points of all public methods
 * of EJBs. Example:
 *  
 *     @Interceptors(MyEjbLoggingInterceptor.class)
 */
public class EjbLoggingInterceptor {

	public EjbLoggingInterceptor() {
		//constructor
	}

	@AroundInvoke
	public Object logEntryAndExit(InvocationContext ic) throws Exception {
		Method method = ic.getMethod();
		String methodName = method.getName();
		Logger logger = LoggerFactory.getLogger(method.getDeclaringClass());
		logger.debug("ENTER: {}({})", methodName, ic.getParameters());
		long start = System.currentTimeMillis();
		Object ret = ic.proceed();
		long runTime = System.currentTimeMillis() - start;
		logger.debug("EXIT: {} returning({}) in {}ms", methodName, ret, runTime);
		return ret;
	}
}
