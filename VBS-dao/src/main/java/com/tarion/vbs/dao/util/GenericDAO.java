/* 
 * 
 * GenericDAO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.util;

import java.time.LocalDateTime;
import java.util.List;

import javax.ejb.Local;

import com.tarion.vbs.orm.entity.AuditedEntity;


/**
 * GenericDAO class for generic CRUD DB operations against generic tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-16
 * @version 1.0
 */
@Local
public interface GenericDAO {
	
	public <T> T findEntityById(Class<T> clazz, Long id);
	public <T> T findEntityById(Class<T> clazz, String id);
	
	public <T> Long getEntityVersionById(Class<T> clazz, Long id);
	public AuditedEntity addEntity(AuditedEntity entity, String createUser, LocalDateTime createDate);
	public AuditedEntity updateEntity(AuditedEntity entity, String updateUser, LocalDateTime updateDate);

    <T extends AuditedEntity> void updateEntityList(List<T> entityList, String updateUser, LocalDateTime updateDate);
	public void removeEntity(AuditedEntity entity, String updateUser, LocalDateTime updateDate);

    void detach(Object object);
}
