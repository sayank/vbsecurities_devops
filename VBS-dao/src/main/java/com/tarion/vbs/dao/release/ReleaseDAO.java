/* 
 * 
 * ReleaseDAO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.release;

import java.time.LocalDateTime;
import java.util.List;

import javax.ejb.Local;
import javax.persistence.Tuple;

import com.tarion.vbs.common.dto.enrolment.EnrolmentWarrantyServiceFieldsDTO;
import com.tarion.vbs.common.dto.release.PendingReleaseDTO;
import com.tarion.vbs.common.dto.release.ReleaseSearchParametersDTO;
import com.tarion.vbs.common.enums.ReleaseStatus;
import com.tarion.vbs.orm.entity.release.ReleaseEnrolmentEntity;
import com.tarion.vbs.orm.entity.release.ReleaseEntity;
import com.tarion.vbs.orm.entity.release.ReleaseReasonEntity;

/**
 * ReleaseDAO class for DB operations on Security and Release tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-01-08
 * @version 1.0
 */
@Local
public interface ReleaseDAO {

	List<ReleaseEntity> getReleases(Long securityId);

	List<ReleaseEnrolmentEntity> getEnrolmentsByReleaseId(Long releaseId);
	List<ReleaseEnrolmentEntity> getReleasesByEnrolmentNumber(String enrolmentNumber);
	List<ReleaseReasonEntity> getReleaseReasonsByReleaseId(Long releaseId);

	List<String> getEnrolmentNumberssByReleaseId(Long releaseId);

	long getNumberOfReleases(Long securityId);

	List<ReleaseEntity> getReleaseByStatus(ReleaseStatus releaseStatus);

	List<ReleaseEntity> getReleaseByStatuses(ReleaseStatus releaseStatus1, ReleaseStatus releaseStatus2);
	List<ReleaseEntity> getReleasesByStatusesFromToDate(List<ReleaseStatus> releaseStatuses, LocalDateTime startDate, LocalDateTime endDate);

    boolean securityHasCompletedPrincipalReleases(Long securityId);

    List<PendingReleaseDTO> searchReleases(ReleaseSearchParametersDTO params);
	List<EnrolmentWarrantyServiceFieldsDTO> getEnrolmentWarrantyServiceFieldsList(List<String> enrolmentNumbers);
	List<ReleaseEnrolmentEntity> getReleasesBySecurityId(Long securityId);
	long getNumberOfPendingApprovalReleaseByStatuses(Long securityId, ReleaseStatus releaseStatus);

    ReleaseEntity getReleaseBySecurityIdSequenceNumber(Long securityId, Long sequenceNumber);
	long getReleaseDemandCollectorByStatusAndSecurityId(ReleaseStatus releaseStatus, Long securityId) ;
	List<Tuple> getSecurityIdReleaseIdByStatus(ReleaseStatus releaseStatus, List<Long> securityIds);
}
