/* 
 * 
 * ReleaseDAOImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.release;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.autorelease.*;
import com.tarion.vbs.common.dto.release.AutoReleaseDTO;
import com.tarion.vbs.common.enums.SearchTypeEnum;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.AddressEntity;
import com.tarion.vbs.orm.entity.autorelease.*;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
import org.apache.commons.lang3.tuple.Pair;
import org.hibernate.transform.Transformers;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ReleaseDAO class for DB operations on Security and Release tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-01-08
 * @version 1.0
 */
@Stateless(mappedName = "ejb/AutoReleaseDAO")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class AutoReleaseDAOImpl implements AutoReleaseDAO {
	
	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;

	@PersistenceContext(unitName = "tdm")
	private EntityManager tdmEm;

	@Override
	public AutoReleaseRunEntity getAutoReleaseRunInProgress() {
		TypedQuery<AutoReleaseRunEntity> query = vbsEm.createNamedQuery(AutoReleaseRunEntity.SELECT_RUN_IN_PROGRESS, AutoReleaseRunEntity.class);
		query.setMaxResults(1);
		List<AutoReleaseRunEntity> result = query.getResultList();
		if(result.isEmpty()) {
			return null;
		}
		return result.get(0);
	}

	@Override
	public List<Pair<AutoReleaseRunEntity, Long>> searchAutoReleaseRuns(AutoReleaseRunSearchParametersDTO params) {
		String sql = 
				"SELECT o, o.runStatus.id " +
				"FROM AutoReleaseRunEntity o " + 
				"WHERE 1=1 ";
		
		LocalDateTime startDate = null;
		LocalDateTime endDate = null;

		if(params.getStartDate() != null){
			if(SearchTypeEnum.BETWEEN.equals(params.getDateSearchType()) && params.getEndDate() != null) {
				sql += " and o.createDate >= :startDate";
				startDate = params.getStartDate();

				sql += " and o.createDate < :endDate ";
				endDate = params.getEndDate().plusDays(1);
			} else if (SearchTypeEnum.EQUALS.equals(params.getDateSearchType())) {
				sql += " and o.createDate >= :startDate ";
				startDate = params.getStartDate();
				sql += " and o.createDate < :endDate ";
				endDate = params.getStartDate().plusDays(1);
			} else if (SearchTypeEnum.EQUALS_GREATER_THAN.equals(params.getDateSearchType())) {
				sql += " and o.createDate >= :startDate ";
				startDate = params.getStartDate();
			} else if (SearchTypeEnum.GREATER_THAN.equals(params.getDateSearchType())) {
				sql += " and o.createDate > :startDate ";
				startDate = params.getStartDate().plusDays(1);
			} else if (SearchTypeEnum.EQUALS_LESS_THAN.equals(params.getDateSearchType())) {
				sql += " and o.createDate < :endDate ";
				endDate = params.getStartDate().plusDays(1);
			} else if (SearchTypeEnum.LESS_THAN.equals(params.getDateSearchType())) {
				sql += " and o.createDate < :endDate ";
				endDate = params.getStartDate();
			}
		}
		
		sql += " ORDER BY o.createDate desc ";
		
        TypedQuery<Object[]> query = vbsEm.createQuery(sql, Object[].class);
        if (startDate != null) {
        	query.setParameter("startDate", startDate);
        }
        if (endDate != null) {
        	query.setParameter("endDate", endDate);
        }

        return query.getResultList().stream().map(array -> Pair.of((AutoReleaseRunEntity)array[0], (Long)array[1])).collect(Collectors.toList());
	}

	@Override
	public List<AutoReleaseEnrolmentEntity> getAutoReleaseEnrolmentsForRun(Long runId) {
		TypedQuery<AutoReleaseEnrolmentEntity> query = vbsEm.createNamedQuery(AutoReleaseEnrolmentEntity.SELECT_ALL_ENROLMENT_FOR_RUN, AutoReleaseEnrolmentEntity.class);
		query.setParameter(1, runId);
		return query.getResultList();
	}

	@Override
	public List<AutoReleaseEnrolmentEntity> getAutoReleaseEnrolmentsForRunSuccess(Long runId) {
		TypedQuery query = vbsEm.createNamedQuery(AutoReleaseEnrolmentEntity.SELECT_ALL_ENROLMENT_FOR_RUN_SUCCESS, AutoReleaseEnrolmentEntity.class);
		return query.setParameter(1, runId).getResultList();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<AutoReleaseResultDTO> getAutoReleaseResultsForRunSuccess(Long runId) {
		TypedQuery query = vbsEm.createNamedQuery(AutoReleaseEnrolmentEntity.SELECT_SEARCH_ENROLMENT_FOR_RUN_SUCCESS, Tuple.class);
		List<Tuple> ret = query.setParameter(1, runId).getResultList();
		return ret.stream().map(this::transformAutoReleaseEnrolmentsForRun).collect(Collectors.toList());
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<AutoReleaseResultDTO> getAutoReleaseResultsForRunEliminated(Long runId) {

		TypedQuery query = vbsEm.createNamedQuery(AutoReleaseEnrolmentEntity.SELECT_SEARCH_ENROLMENT_FOR_RUN_ELIMINATED, Tuple.class);
		List<Tuple> ret = query.setParameter(1, runId).getResultList();
		return ret.stream().map(this::transformAutoReleaseEnrolmentsForRun).collect(Collectors.toList());
	}

	@SuppressWarnings({"unchecked", "deprecation"})
	@Override
	public List<AutoReleaseCandidateDTO> getAutoReleaseCandidates() {

		Query query = vbsEm.createNamedQuery(EnrolmentEntity.SELECT_AUTO_RELEASE_SHORTLIST);

		//Replaced by @FunctionalInterface in Hibernate 6.0, removed in 5.3, both require JPA 2.2 which we cannot use on Weblogic, thus not an issue.
		return query.unwrap( org.hibernate.query.NativeQuery.class).setResultTransformer(Transformers.aliasToBean( AutoReleaseCandidateDTO.class)).getResultList();
	}

	@Override
	public Long getCountIncompleteEnrolmentsForRun(Long autoReleaseRunId) {
		TypedQuery<Long> query = vbsEm.createNamedQuery(AutoReleaseEnrolmentEntity.SELECT_COUNT_PROCESSING_FOR_RUN, Long.class);
		query.setParameter(1, autoReleaseRunId);
		return query.getSingleResult();
	}

	@Override
	public Long getCountReleasesInRun(Long autoReleaseRunId) {
		TypedQuery<Long> query = vbsEm.createNamedQuery(AutoReleaseEnrolmentEntity.SELECT_COUNT_RELEASES_IN_RUN, Long.class);
		query.setParameter(1, autoReleaseRunId);
		return query.getSingleResult();
	}

	@Override
	public void addAutoReleaseEnrolments(List<AutoReleaseCandidateDTO> candidates) {

        batchInsert("INSERT INTO [AUTO_RELEASE_ENROLMENT] ([VERSION], [AUTO_RELEASE_RUN_ID], [SECURITY_ID], [ENROLMENT_NUMBER], [TIMING_TYPE_ID], [STATUS_ID], [CREATE_DATE], [CREATE_USER]) VALUES ",
                candidates.stream().map(dto -> String.format(
                        " (1, %s, %s, '%s', %s, 1, getdate(), '%s'),",
                        dto.getAutoReleaseRunId(),
                        dto.getSecurityId(),
                        dto.getEnrolmentNumber(),
                        dto.getTimingTypeId(),
                        VbsConstants.VBS_SYSTEM_USER_ID)).collect(Collectors.toList()));
    }


    @Override
    public void addAutoReleaseEnrolmentDataEntities(List<AutoReleaseEnrolmentDataDTO> enrolments) {

        batchInsert("INSERT INTO AUTO_RELEASE_ENROLMENT_DATA (VERSION, AUTO_RELEASE_RUN_ID, ENROLMENT_NUMBER, DATE_OF_POSSESSION, UNRESOLVED_CASES, INVENTORY_ITEM, CREATE_DATE, CREATE_USER) VALUES ",
                enrolments.stream().map(dto -> String.format(
                        " (1, %s, '%s', %s, %s, %s, getdate(), '%s'),",
                        dto.getAutoReleaseRunId(),
                        dto.getEnrolmentNumber(),
                        DateUtil.getDateFormattedSQL(dto.getDateOfPossession()) ,
                        dto.getUnresolvedCases(),
                        dto.isInventoryItem() ? 1 : 0,
                        VbsConstants.VBS_SYSTEM_USER_ID)).collect(Collectors.toList()));


	}

    @Override
    public void addAutoReleaseVbDataEntities(List<AutoReleaseVbDataDTO> vbs) {

        batchInsert("INSERT INTO AUTO_RELEASE_VB_DATA (VERSION, AUTO_RELEASE_RUN_ID, VB_NUMBER, UNDER_INVESTIGATION, CREDIT_RATING, LICENSE_STATUS, ACTIVE_ALERT, OS_CCPS, AVG_GUARANTOR_CREDIT_RATING, NUM_GUARANTORS, UNWILLING_UNABLE, CREATE_DATE, CREATE_USER) VALUES ",
                vbs.stream().map(dto -> String.format(
                        " (1,       %s,                  '%s',      %s,                 '%s',           %s,             %s,           %s,       '%s',                       %s,             %s,               getdate(),   '%s'),",
                        //(VERSION, AUTO_RELEASE_RUN_ID, VB_NUMBER, UNDER_INVESTIGATION, CREDIT_RATING, LICENSE_STATUS, ACTIVE_ALERT, OS_CCPS, AVG_GUARANTOR_CREDIT_RATING, NUM_GUARANTORS, UNWILLING_UNABLE, CREATE_DATE, CREATE_USER) VALUES ",
                        dto.getAutoReleaseRunId(),
                        dto.getVbNumber(),
                        dto.isUnderInvestigation() ? 1 : 0,
                        dto.getCreditRating(),
                        dto.getLicenseStatus(),
                        dto.isActiveAlert() ? 1 : 0,
                        dto.getOsCCPS(),
                        dto.getAvgGuarantorCreditRating(),
                        dto.getNumGuarantors(),
                        dto.isUnwillingUnable() ? 1 : 0,
                        VbsConstants.VBS_SYSTEM_USER_ID)).collect(Collectors.toList()));
    }

	@Override
	public void addAutoReleaseVbApplicationStatusEntities(List<AutoReleaseVbApplicationStatusDTO> vbs) {

		batchInsert("INSERT INTO AUTO_RELEASE_VB_APPLICATION_STATUS (VERSION, AUTO_RELEASE_RUN_ID, VB_NUMBER, APPLICATION_STATUS, CREATE_DATE, CREATE_USER) VALUES ",
				vbs.stream().map(dto -> String.format(
						" (1, %s, '%s', %s, getdate(), '%s'),",
						dto.getAutoReleaseRunId(),
						dto.getVbNumber(),
						dto.getApplicationStatus(),
						VbsConstants.VBS_SYSTEM_USER_ID)).collect(Collectors.toList()));

	}

    private void batchInsert(String baseSql, List<String> values) {
        if(values.isEmpty()) {
            return;
        }

        //Doing native sql insert for performance reasons


        StringBuilder sql = new StringBuilder(baseSql);
        for (int i = 0; i < values.size(); i++) {

            sql.append(values.get(i));

            //MSSQL is set to accept at most 1000 rows per insert
            if(i > 0 && i % 990 == 0) {
                sql.deleteCharAt(sql.length() - 1);
                vbsEm.createNativeQuery(sql.toString()).executeUpdate();
                sql = new StringBuilder(baseSql);
            }
        }

        if(!sql.toString().equals(baseSql)) {
            sql.deleteCharAt(sql.length() - 1);
            vbsEm.createNativeQuery(sql.toString()).executeUpdate();
        }
    }

    @Override
	@SuppressWarnings({"deprecation", "unchecked"})
	public DataMartVbInfoDTO getDataMartVbInfo(String vbNumber) {
		String sql =
				"SELECT CASE " +
				"           WHEN A.HOME_PENDING_SEC_IND = 0 THEN 1 " +
				"           ELSE 0 " +
				"       END enrolmentPendingSecurity, " +
				"       CASE " +
				"           WHEN A.ACTIVE_RESERVE_IND = 0 THEN 0 " +
				"           ELSE 1 " +
				"       END activeReserve, " +
				"       isnull(A.TTL_CURR_ALL_SEC_AMT, 0) currentAmount, " +
				"       CASE " +
				"           WHEN A.AT_LEAST_ONE_SEC_DMD_IND = 0 THEN 0 " +
				"           ELSE 1 " +
				"       END demand, " +
				"       CASE " +
				"           WHEN A.CURR_RVK_RFUS_SPD_NOPED_IND = 0 THEN 0 " +
				"           ELSE 1 " +
				"       END revoked, " +
				"       D.DIR_TTL_OS_INCTAX_AMT vbDebt, " +
				"       D.TTL_PDOG_DEBT_OS_INCTAX_AMT pdogDebt " +
				"FROM DM_VB_DIM A INNER JOIN DM_DEBTOR D ON 'B'+convert(varchar(10), A.VB_REGISTRATION_NUM) = D.AR_CUST_ID " +
				"WHERE D.AR_CUST_ID = ?1 ";

		Query query = tdmEm.createNativeQuery(sql);
		query.setParameter(1, vbNumber);

		//Replaced by @FunctionalInterface in Hibernate 6.0, removed in 5.3, both require JPA 2.2 which we cannot use on Weblogic, thus not an issue.
		List<DataMartVbInfoDTO> results = query.unwrap( org.hibernate.query.NativeQuery.class).setResultTransformer(Transformers.aliasToBean( DataMartVbInfoDTO.class)).getResultList();

		if(results.isEmpty()) {
			return null;
		}
		return results.get(0);
	}

	@Override
	public AutoReleaseVbDataEntity getVbData(String vbNumber, Long runId) {
		TypedQuery<AutoReleaseVbDataEntity> query = vbsEm.createNamedQuery(AutoReleaseVbDataEntity.SELECT_WITH_VB_NUMBER_NUMBER_RUN_ID, AutoReleaseVbDataEntity.class);
		query.setParameter(1, vbNumber);
		query.setParameter(2, runId);
		List<AutoReleaseVbDataEntity> results = query.getResultList();
		if(results.isEmpty()) {
			return null;
		}
		return results.get(0);
	}

	@Override
	public AutoReleaseVbApplicationStatusEntity getVbApplicationStatus(String vbNumber, Long runId) {
		TypedQuery<AutoReleaseVbApplicationStatusEntity> query = vbsEm.createNamedQuery(AutoReleaseVbApplicationStatusEntity.SELECT_WITH_VB_NUMBER_NUMBER_RUN_ID, AutoReleaseVbApplicationStatusEntity.class);
		query.setParameter(1, vbNumber);
		query.setParameter(2, runId);
		List<AutoReleaseVbApplicationStatusEntity> results = query.getResultList();
		if(results.isEmpty()) {
			return null;
		}
		return results.get(0);
	}

	@Override
	public AutoReleaseEnrolmentDataEntity getEnrolmentData(String vbNumber, Long runId) {
		TypedQuery<AutoReleaseEnrolmentDataEntity> query = vbsEm.createNamedQuery(AutoReleaseEnrolmentDataEntity.SELECT_WITH_ENROLMENT_NUMBER_RUN_ID, AutoReleaseEnrolmentDataEntity.class);
		query.setParameter(1, vbNumber);
		query.setParameter(2, runId);
		List<AutoReleaseEnrolmentDataEntity> results = query.getResultList();
		if(results.isEmpty()) {
			return null;
		}
		return results.get(0);
	}

	@Override
	public List<Long> getEliminationsForEnrolment(Long autoReleaseEnrolmentId) {
		TypedQuery<Long> query = vbsEm.createNamedQuery(AutoReleaseEliminationEntity.SELECT_ALL_BY_AUTO_RELEASE_ENROLMENT_ID, Long.class);
		query.setParameter(1, autoReleaseEnrolmentId);
		return  query.getResultList();
	}

	@Override
	public List<AutoReleaseDTO> getAutoReleaseForSecurityId(Long securityId) {

		TypedQuery query = vbsEm.createNamedQuery(AutoReleaseEnrolmentEntity.SELECT_AUTO_RELEASE_FOR_SECURITY, Tuple.class);
		List<Tuple> ret = query.setParameter(1, securityId).getResultList();
		List<AutoReleaseDTO> list = ret.stream().map(this::transformAutoReleaseEnrolmentForSecurity).collect(Collectors.toList());
		List<AutoReleaseDTO> result = new ArrayList<>();

		if(list.size() > 1) {
			AutoReleaseDTO previous = list.get(0);
			for (int i = 1; i < list.size(); i++) {
				AutoReleaseDTO current = list.get(i);
				if(current.getEnrolmentNumber().equals(previous.getEnrolmentNumber()) && current.getRunId().longValue() == previous.getRunId().longValue()) {
					previous.getEliminations().addAll(current.getEliminations());
				} else {
					result.add(previous);
					previous = current;
				}
			}
			result.add(previous);
		} else {
			result = list;
		}

		return result;
	}

	private AutoReleaseDTO transformAutoReleaseEnrolmentForSecurity(Tuple t) {
		AutoReleaseDTO dto = new AutoReleaseDTO();
		dto.setRunId(t.get("runId", Long.class));
		dto.setDeletedBy(t.get("deletedBy", String.class));
		dto.setEliminations(new ArrayList<>());
		if(t.get("elimination") != null) {
			dto.getEliminations().add(t.get("elimination", String.class));
		}
		dto.setEnrolmentAddress(getAddressConcat(t.get("address", AddressEntity.class)));
		dto.setEnrolmentNumber(t.get("enrolmentNumber", String.class));
		dto.setReleaseDate(t.get("releaseDate", LocalDateTime.class));
		dto.setReleaseSeqNum(t.get("releaseSeqNum", Long.class));
		dto.setRunDate(t.get("runDate", LocalDateTime.class));
		dto.setTimingType(t.get("timingType", String.class));
		return dto;
	}


	private AutoReleaseResultDTO transformAutoReleaseEnrolmentsForRun(Tuple t) {
		AutoReleaseResultDTO dto = new AutoReleaseResultDTO();
		dto.setId(t.get("id", Long.class));
		String vbNumber = t.get("vbNumber", String.class);
		dto.setvNumber(vbNumber != null ? Integer.parseInt(vbNumber.replace("B", "")) : null);
		dto.setvName(t.get("vbName", String.class));
		dto.setLicenseStatus(t.get("licenseStatus", String.class));
		dto.setSecurityReceivedDate(t.get("securityReceivedDate", LocalDateTime.class));
		dto.setInstrumentNumber(t.get("instrumentNumber", String.class));
		String enrolmentNumber = t.get("enrolmentNumber", String.class);
		dto.setEnrolmentNumber(enrolmentNumber != null ? Integer.parseInt(enrolmentNumber.replace("H", "")) : null);
		dto.setWarrantyStartDate(t.get("warrantyStartDate", LocalDateTime.class));
		dto.setEnrolmentAddress(getAddressConcat(t.get("address", AddressEntity.class)));
		dto.setSecurityType(t.get("securityType", String.class));
		dto.setReleaseTimingType(t.get("releaseTimingType", String.class));
		dto.setEliminationCriteria(t.get("eliminationCriteria", String.class));
		dto.setRunId(t.get("runId", Long.class));
		dto.setSecurityId(t.get("securityId", Long.class));
		dto.setDeletedBy(t.get("deletedBy", String.class));
		if (dto.getDeletedBy() != null) {
			dto.setDelete(true);
		}

		return dto;
	}

	public String getAddressConcat(AddressEntity address) {
		if(address == null){
			return null;
		}
		List<String> list = new LinkedList<>();
		if(!VbsUtil.isNullorEmpty(address.getAddressLine1())) {list.add(address.getAddressLine1());}
		if(!VbsUtil.isNullorEmpty(address.getAddressLine2())) {list.add(address.getAddressLine2());}
		if(!VbsUtil.isNullorEmpty(address.getCity())) {list.add(address.getCity());}
		if(!VbsUtil.isNullorEmpty(address.getProvince())) {list.add(address.getProvince());}
		if(!VbsUtil.isNullorEmpty(address.getPostalCode())) {list.add(address.getPostalCode());}
		return String.join(", ", list);
	}



}
