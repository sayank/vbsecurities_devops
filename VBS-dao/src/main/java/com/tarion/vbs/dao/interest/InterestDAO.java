/* 
 * 
 * SecurityDAO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.interest;

import com.tarion.vbs.common.dto.interest.InterestCalcDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.orm.entity.interest.InterestCalculationEntity;
import com.tarion.vbs.orm.entity.interest.InterestEntity;
import com.tarion.vbs.orm.entity.interest.InterestRateEntity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.ejb.Local;
import javax.persistence.Tuple;

/**
 * SecurityDAO class for DB operations on Security Table
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-16
 * @version 1.0
 */
@Local
public interface InterestDAO {


	List<InterestRateEntity> getAllInterestRates();

	InterestRateEntity getApplicableInterestRateForDate(LocalDateTime date) throws VbsCheckedException;

	LocalDate findLastCalculatedDailyInterest();

	InterestEntity getLastCalculatedInterestForSecurity(long securityId);

	List<Tuple> getCashSecuritiesWithoutInterest(int daysInPast);

	List<Tuple> getSecurityInfoForInterestReport(int daysInPast);

	List<Tuple> getSecurityInfoForInterestDate(LocalDate date);

	InterestCalculationEntity createRunForDate(LocalDate date);

	List<LocalDate> findDatesRequiringInterestRun(int precedingDaysToCheck);

	List<InterestEntity> getInterestsForSecurityId(Long securityId);

	List<Tuple> findInterestsForSecurityBetweenDates(Long securityId, LocalDate fromDate, LocalDate toDate);

	void updateInterestCalculation(InterestCalculationEntity calc);

	List<InterestCalcDTO> insertInterestRecords(InterestCalculationEntity calc, List<Tuple> securityList);

}
