/* 
 * 
 * GenericDAOImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.util;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.exceptions.OptimisticLockException;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.orm.entity.AuditedEntity;
import org.apache.commons.beanutils.PropertyUtils;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


/**
 * GenericDAO class for generic CRUD DB operations against generic tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-16
 * @version 1.0
 */
@Stateless(mappedName = "ejb/GenericDAO")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class GenericDAOImpl implements GenericDAO {
	
	private static final String UPDATE_OLD_CLASS = "Tried to update old version of class: ";
	public static final long INITIAL_VERSION = 1l;
	
	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;
	
	@Override
	public <T> T findEntityById(Class<T> clazz, Long id) {
		return vbsEm.find(clazz, id);
	}

	@Override
	public <T> T findEntityById(Class<T> clazz, String id) {
		return vbsEm.find(clazz, id);
	}
	
	@Override
	public AuditedEntity addEntity(AuditedEntity entity, String createUser, LocalDateTime createDate) {
		if(entity == null){
			return null;
		}
		
		if (createUser != null) {
			entity.setCreateUser(createUser);
			entity.setUpdateUser(createUser);
		} else {
			if (entity.getCreateUser() == null) {
				entity.setCreateUser(VbsConstants.VBS_SYSTEM_USER_ID);
				entity.setUpdateUser(VbsConstants.VBS_SYSTEM_USER_ID);
			}
		}
		if (createDate != null) {
			entity.setCreateDate(createDate);
			entity.setUpdateDate(createDate);
		} else {
			if (entity.getCreateDate() == null) {
				entity.setCreateDate(DateUtil.getLocalDateTimeNow());
				entity.setUpdateDate(DateUtil.getLocalDateTimeNow());
			}
		}
		if (entity.getVersion() == null) {
			entity.setVersion(INITIAL_VERSION);
		}
		vbsEm.persist(entity);
		vbsEm.flush();
		return entity;
	}
	
	@Override
	public AuditedEntity updateEntity(AuditedEntity entity, String updateUser, LocalDateTime updateDate) {
		AuditedEntity returnObject = entity;
		try {
			if (updateDate != null) {
				entity.setUpdateDate(updateDate);
			} else {
				if (entity.getUpdateDate() == null) {
					entity.setUpdateDate(DateUtil.getLocalDateTimeNow());
				}
			}
			if (updateUser != null) {
				entity.setUpdateUser(updateUser);
			} else {
				if (entity.getUpdateUser() == null) {
					entity.setUpdateUser(VbsConstants.VBS_SYSTEM_USER_ID);
				}
			}
			returnObject = vbsEm.merge(entity);
			vbsEm.flush();
		} catch (javax.persistence.OptimisticLockException e) {			
			throw new OptimisticLockException(UPDATE_OLD_CLASS + entity.getClass());
		}
		
		return returnObject;
	}

	@Override
	public <T extends AuditedEntity> void updateEntityList(List<T> entityList, String updateUser, LocalDateTime updateDate) {
		try {
			entityList.forEach(e -> {
				e.setUpdateDate(updateDate);
				e.setUpdateUser(updateUser);
				vbsEm.merge(e);
			});
			vbsEm.flush();
			vbsEm.clear();
		} catch (javax.persistence.OptimisticLockException e) {
			Optional<T> first = entityList.stream().findFirst();
			throw new OptimisticLockException(UPDATE_OLD_CLASS + (first.isPresent() ? first.get().getClass() : "" ));
		}
	}

	@Override
	public void removeEntity(AuditedEntity entity, String updateUser, LocalDateTime updateDate) {
		if(entity != null) {
			if (updateUser != null) {
				entity.setUpdateUser(updateUser);
			} else {
				if (entity.getUpdateUser() == null) {
					entity.setUpdateUser(VbsConstants.VBS_SYSTEM_USER_ID);
				}
			}
			if (updateDate != null) {
				entity.setUpdateDate(updateDate);
			} else {
				if (entity.getUpdateDate() == null) {
					entity.setUpdateDate(DateUtil.getLocalDateTimeNow());
				}
			}
			entity = vbsEm.merge(entity);
			vbsEm.flush();
			vbsEm.remove(entity);
			vbsEm.flush();
		}
	}

	@Override
	public <T> Long getEntityVersionById(Class<T> clazz, Long id) {
		try {
			Object entity = findEntityById(clazz, id);
			return (Long) PropertyUtils.getSimpleProperty(entity, "version");
		} catch (Exception e) {
			throw new VbsRuntimeException("Failed to get version for class: " + clazz.getName() + ", id: " + id, e);
		}
	}

	@Override
	public void detach(Object object){
		vbsEm.detach(object);
	}
}
