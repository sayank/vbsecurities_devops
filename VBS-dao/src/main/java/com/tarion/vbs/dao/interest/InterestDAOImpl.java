/* 
 * 
 * SecurityDAOImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.interest;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.interest.InterestCalcDTO;
import com.tarion.vbs.common.enums.InterestStatusEnum;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.orm.entity.interest.InterestCalculationEntity;
import com.tarion.vbs.orm.entity.interest.InterestEntity;
import com.tarion.vbs.orm.entity.interest.InterestRateEntity;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.*;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Interest DAO
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-16
 * @version 1.0
 */
@Stateless(mappedName = "ejb/InterestDAO")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class InterestDAOImpl implements InterestDAO {
	
	@PersistenceContext(unitName = "vbs")
	EntityManager vbsEm;

	@EJB
	private GenericDAO genericDAO;

	@Override
	public List<InterestRateEntity> getAllInterestRates() {
		TypedQuery<InterestRateEntity> query = vbsEm.createNamedQuery(InterestRateEntity.SELECT_ALL_INTEREST_RATES, InterestRateEntity.class);
		return query.getResultList();
	}

	@Override
	public InterestRateEntity getApplicableInterestRateForDate(LocalDateTime date) throws VbsCheckedException {
		date = date.truncatedTo(ChronoUnit.DAYS);
		TypedQuery<InterestRateEntity> query = vbsEm.createNamedQuery(InterestRateEntity.SELECT_INTEREST_RATE_FOR_DATE, InterestRateEntity.class);
		query.setParameter(1, date);
		List<InterestRateEntity> rates = query.getResultList();
		if (rates.size() != 1) {
			throw new VbsCheckedException("No rate or more then one rate found for date: " + date);
		} 
		return rates.get(0);
	}

	@Override
	public LocalDate findLastCalculatedDailyInterest() {
		TypedQuery<LocalDate> query = vbsEm.createNamedQuery(InterestCalculationEntity.SELECT_LAST_CALCULATED_INTEREST_DATE, LocalDate.class);
		return (query.getResultList().isEmpty()) ? null : query.getResultList().get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public InterestEntity getLastCalculatedInterestForSecurity(long securityId) {
		String sql =
				"                select top 1 inter.ID from interest inter, interest_calculation intcalc " +
						"				where intcalc.ID = inter.interest_calculation_id " +
						"				and inter.security_id = ?1 "
						+ "             order by inter.ID desc ";

		Query query = vbsEm.createNativeQuery(sql);
		query.setParameter(1, securityId);
		List<Integer> ids = query.getResultList();
		if (!ids.isEmpty()) {
			return genericDAO.findEntityById(InterestEntity.class, ids.get(0).longValue());
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tuple> getCashSecuritiesWithoutInterest(int daysInPast) {

		String sql =
				"WITH SECURITIES (ID, RECEIVED_DATE) AS ( " +
				"  SELECT S.ID, S.RECEIVED_DATE FROM SECURITY S " +
				"  WHERE " +
				"  SECURITY_TYPE_ID = 1 AND CURRENT_AMOUNT > 1 AND ALLOCATED = 1 AND DELETED = 0 " +
				"), DATES AS ( " +
				"SELECT [DATE] = CAST(GETDATE()-%d as date) " +
				" UNION ALL SELECT [Date] = DATEADD(DAY, 1, [Date]) " +
				" FROM Dates " +
				" WHERE Date < DATEADD(DAY, -1, CAST(GETDATE() as date)) " +
				"), CHECK_DATES (SECURITY_ID, INTEREST_DATE) AS ( " +
				" SELECT " +
				" SECURITIES.ID, " +
				" D.DATE " +
				" FROM SECURITIES, DATES D " +
				" WHERE D.DATE >= SECURITIES.RECEIVED_DATE " +
				") " +
				"SELECT C.*" +
				"FROM CHECK_DATES C " +
				"WHERE NOT EXISTS ( "+
				"	SELECT 1 FROM INTEREST_CALCULATION IC JOIN INTEREST I ON IC.ID = I.INTEREST_CALCULATION_ID WHERE I.SECURITY_ID = C.SECURITY_ID AND IC.INTEREST_DATE = C.INTEREST_DATE " +
				") " +
				"ORDER BY SECURITY_ID, INTEREST_DATE " +
				"OPTION(MAXRECURSION %d)";

		sql = String.format(sql, daysInPast, daysInPast);
		Query query = vbsEm.createNativeQuery(sql, Tuple.class);
		return query.getResultList();
	}

	@Override
	public List<Tuple> getSecurityInfoForInterestReport(int daysInPast){
		String sql =
				"WITH SECURITIES (ID, RECEIVED_DATE) AS ( " +
				"SELECT S.ID, S.RECEIVED_DATE FROM SECURITY S " +
				"WHERE " +
				"SECURITY_TYPE_ID = 1 AND CURRENT_AMOUNT > 1 AND ALLOCATED = 1 AND DELETED = 0 " +
				"), DATES AS ( " +
				"SELECT [DATE] = CAST(GETDATE()-%d as date) " +
				"UNION ALL SELECT [Date] = DATEADD(DAY, 1, [Date]) " +
				"FROM Dates " +
				"WHERE Date < DATEADD(DAY, -1, CAST(GETDATE() as date))  " +
				"), CHECK_DATES (SECURITY_ID, INTEREST_DATE) AS ( " +
				"SELECT " +
				"SECURITIES.ID, " +
				"D.DATE " +
				"FROM SECURITIES, DATES D " +
				"WHERE D.DATE >= SECURITIES.RECEIVED_DATE " +
				"), SEC (SECURITY_ID) AS ( " +
				"SELECT " +
				"DISTINCT(C.SECURITY_ID) " +
				"FROM CHECK_DATES C " +
				"WHERE NOT EXISTS ( "+
				"	SELECT 1 FROM INTEREST_CALCULATION IC JOIN INTEREST I ON IC.ID = I.INTEREST_CALCULATION_ID WHERE I.SECURITY_ID = C.SECURITY_ID AND IC.INTEREST_DATE = C.INTEREST_DATE " +
				") " +
				") " +
				"SELECT " +
				"S.ID, " +
				"S.CURRENT_AMOUNT, " +
				"S.INSTRUMENT_NUMBER, " +
				"S.ISSUED_DATE, " +
				"S.RECEIVED_DATE, " +
				"S.ORIGINAL_AMOUNT, " +
				"VB.CRM_CONTACT_ID, " +
				"VB.COMPANY_NAME, " +
				"(SELECT MAX(IC.INTEREST_DATE) FROM INTEREST I JOIN INTEREST_CALCULATION IC ON IC.ID = I.INTEREST_CALCULATION_ID WHERE I.SECURITY_ID = S.ID) AS LAST_INTEREST_DATE " +
				"FROM SECURITY S " +
				"LEFT OUTER JOIN CONTACT VB ON S.PRIMARY_VB_ID = VB.ID " +
				"WHERE S.ID IN (SELECT SECURITY_ID FROM SEC) ORDER BY S.ID " +
				"OPTION(MAXRECURSION %d)";

		sql = String.format(sql, daysInPast, daysInPast);
		Query query = vbsEm.createNativeQuery(sql, Tuple.class);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	@ExcludeClassInterceptors
	public List<Tuple> getSecurityInfoForInterestDate(LocalDate date) {
		String sql =
					"SELECT " +
					"    S.ID, " +
					"   (CASE WHEN EXISTS(SELECT SA2.CURRENT_AMOUNT FROM SECURITY_AUD SA2 WHERE SA2.ID = S.ID AND SA2.REV <= R.REV) THEN " +
					"		(SELECT TOP 1 SA3.CURRENT_AMOUNT FROM SECURITY_AUD SA3 WHERE SA3.ID = S.ID AND SA3.REV <= R.REV ORDER BY SA3.REV DESC) ELSE " +
					"    	(SELECT TOP 1 SA4.CURRENT_AMOUNT FROM SECURITY_AUD SA4 WHERE SA4.ID = S.ID ORDER BY SA4.REV DESC) " +
					"    END ) AS CURRENT_AMOUNT, " +
					"   C.CRM_CONTACT_ID AS VB_NUMBER, " +
					"   S.INSTRUMENT_NUMBER " +
					"FROM REVINFO R, SECURITY S " +
					"JOIN CONTACT C ON C.ID  = S.PRIMARY_VB_ID " + //NOT OUTER JOIN - securities without a primary cant be sent to FMS anyway
					"WHERE S.SECURITY_TYPE_ID = 1 AND S.ALLOCATED = 1 AND S.CURRENT_AMOUNT > 1 AND S.DELETED = 0 AND S.RECEIVED_DATE <= ?1 " +
					"AND NOT EXISTS (SELECT I.ID FROM INTEREST I JOIN INTEREST_CALCULATION IC ON I.INTEREST_CALCULATION_ID = IC.ID WHERE I.SECURITY_ID = S.ID AND IC.INTEREST_DATE = ?2) " +
					"AND R.REV = (SELECT TOP 1 REV FROM REVINFO WHERE REVTSTMP < ?3 ORDER BY REV DESC) ";

		Query query = vbsEm.createNativeQuery(sql, Tuple.class);
		query.setParameter(1, date);
		query.setParameter(2, date);
		query.setParameter(3, date.atStartOfDay().atZone(DateUtil.DEFAULT_ZONE_ID).toInstant().toEpochMilli());

		return query.getResultList();
	}

	@Override
	public InterestCalculationEntity createRunForDate(LocalDate date) {
		InterestCalculationEntity calc = new InterestCalculationEntity();
		try {
			InterestRateEntity rate = getApplicableInterestRateForDate(date.atStartOfDay());
			calc.setInterestRate(rate);
			calc.setInterestDate(date);
			calc.setCalculationDate(LocalDateTime.now());
			calc.setStatus(InterestStatusEnum.CREATED);
		}catch(VbsCheckedException ce){
			throw new VbsRuntimeException(ce);
		}
		vbsEm.persist(calc);
		return calc;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LocalDate> findDatesRequiringInterestRun(int precedingDaysToCheck){
		String sql = "WITH SECURITIES (ID, RECEIVED_DATE) AS ( " +
				" SELECT S.ID, S.RECEIVED_DATE FROM SECURITY S " +
				" WHERE " +
				" SECURITY_TYPE_ID = 1 AND CURRENT_AMOUNT > 1 AND ALLOCATED = 1 AND DELETED = 0 " +
				"), DATES AS ( " +
				" SELECT [DATE] = CAST(GETDATE()-%d as date) " +
				"    UNION ALL SELECT [Date] = DATEADD(DAY, 1, [Date]) " +
				"    FROM Dates " +
				"    WHERE Date < DATEADD(DAY, -1, CAST(GETDATE() as date))  " +
				"), CHECK_DATES (SECURITY_ID, INTEREST_DATE) AS ( " +
				" SELECT " +
				" SECURITIES.ID, " +
				" D.DATE " +
				" FROM SECURITIES, DATES D " +
				" WHERE D.DATE >= SECURITIES.RECEIVED_DATE " +
				") " +

				"SELECT " +
				"DISTINCT(C.INTEREST_DATE) " +
				"FROM CHECK_DATES C " +
				"WHERE NOT EXISTS( " +
				"	SELECT 1 FROM INTEREST_CALCULATION IC JOIN INTEREST I ON IC.ID = I.INTEREST_CALCULATION_ID WHERE I.SECURITY_ID = C.SECURITY_ID AND IC.INTEREST_DATE = C.INTEREST_DATE " +
				") " +
				"OPTION(MAXRECURSION %d)";

		sql = String.format(sql, precedingDaysToCheck, precedingDaysToCheck);

		Query query = vbsEm.createNativeQuery(sql);
		List<Date> dates = query.getResultList();
		return dates.stream().map(Date::toLocalDate).collect(Collectors.toList());
	}

	@Override
	public List<InterestEntity> getInterestsForSecurityId(Long securityId) {
		TypedQuery<InterestEntity> query = vbsEm.createNamedQuery(InterestEntity.SELECT_INTEREST_ENTITY_BY_SECURITY_ID, InterestEntity.class);
		query.setParameter(1, securityId);
		return query.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	@ExcludeClassInterceptors
	public List<Tuple> findInterestsForSecurityBetweenDates(Long securityId, LocalDate fromDate, LocalDate toDate) {
        long start = LoggerUtil.logEnter(InterestDAO.class, "findInterestsForSecurityBetweenDates", securityId, fromDate, toDate);
		Query query = vbsEm.createNativeQuery(InterestEntity.SELECT_DAILY_INTERESTS_FOR_SECURITY_BETWEEN_DATES, Tuple.class);
		query.setParameter(1, securityId);
		query.setParameter(2, fromDate);
		query.setParameter(3, toDate);
        List<Tuple> ret = query.getResultList();
        LoggerUtil.logExit(InterestDAO.class, "findInterestsForSecurityBetweenDates", ret.size());
		return ret;
	}

	@Override
	public void updateInterestCalculation(InterestCalculationEntity calc) {
		vbsEm.merge(calc);
	}

	@Override
	public List<InterestCalcDTO> insertInterestRecords(InterestCalculationEntity calc, List<Tuple> securityList) {
		List<InterestCalcDTO> ret = new ArrayList<>();
		BigDecimal dailyPercent = calc.getInterestRate().getAnnualRate()
				.divide(BigDecimal.valueOf(100), 10, VbsConstants.ROUNDING_MODE)
				.divide(BigDecimal.valueOf(LocalDateTime.of(calc.getInterestDate().getYear(), 12, 31, 0, 0,0 ).getDayOfYear()), 10, VbsConstants.ROUNDING_MODE);

		try (Connection con = this.getVbsDataSourceConnection();
			 PreparedStatement ps = con.prepareStatement("INSERT INTO INTEREST (SECURITY_ID, INTEREST_CALCULATION_ID, INTEREST_AMOUNT) VALUES (?, ?, ?)")){
			for (Tuple security : securityList) {
				ps.setLong(1, security.get(0, Integer.class).longValue());
				ps.setLong(2, calc.getId());

				BigDecimal currentAmount = security.get(1, BigDecimal.class);
				BigDecimal interestAmount = currentAmount.multiply(dailyPercent).setScale(VbsConstants.DECIMAL_SCALE, VbsConstants.ROUNDING_MODE);
				ps.setBigDecimal(3, interestAmount);

				ret.add(new InterestCalcDTO(security.get(0, Integer.class).longValue(), interestAmount, security.get(2, String.class), security.get(3, String.class)));
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LoggerUtil.logError(InterestDAOImpl.class, "createRunForDate", "Error inserting interest calculations.");
		}

		calc.setStatus(InterestStatusEnum.CALCULATED);
		vbsEm.merge(calc);

		return ret;
	}

	private Connection getVbsDataSourceConnection() throws SQLException {
		return this.getDataSource().getConnection();
	}

	private DataSource getDataSource() {
		try {
			InitialContext ic = new InitialContext();
			return (DataSource) ic.lookup("jdbc/vbsVbsDS");
		} catch (NamingException e) {
			String errMsg = "Problem connection to datasource: " + "jdbc/vbsVbsDS";
			throw new VbsRuntimeException(errMsg, e);
		}
	}

}
