/* 
 * 
 * SecurityDAO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.security;

import com.tarion.vbs.common.dto.security.DeletedSecuritySearchParametersDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchParametersDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchResultDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchResultWrapper;
import com.tarion.vbs.common.dto.tip.SecurityTipInfoDTO;
import com.tarion.vbs.orm.entity.enrolment.CEEnrolmentDecisionEntity;
import com.tarion.vbs.orm.entity.security.MonthlyReportEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.Local;
import java.util.List;

/**
 * SecurityDAO class for DB operations on Security Table
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-16
 * @version 1.0
 */
@Local
public interface SecurityDAO {
	List<String> getAllVbNumbersLike(String vbNumber);
	List<String> getAllInstrumentNumbersLike(String instrumentNumber);
	Long getPoolForSecurityById(Long securityId);
	List<SecurityEntity> getAllSecuritiesByInstrumentNumbersLike(String instrumentNumber);

	MonthlyReportEntity getMonthlyReportForYearMonthBySecurity(int securityId, int year, int month);

    SecuritySearchResultWrapper searchSecurity(SecuritySearchParametersDTO searchParams, boolean displayAvailableAmount);
	long getNumSecuritiesWithInstrNumVbNumber(String instrumentNumber, List<String> vbNumbers);
	List<Long> getSecurityIdsWithInstrumentNumberFinancialInst(String instrumentNumber, Long financialInstitutionId);
	List<SecurityEntity> getSecurityByInstrumentNumber(String instrumentNumber);
	boolean securityWithTheSameInstrumentNumberExists(String instrumentNumber);
	List<SecurityEntity> getSecurityByVbNumber(String vbNumber);

    List<SecurityTipInfoDTO> getSecurityByVbNumberBlanket(String vbNumber);

    List<SecurityEntity> getSecurityByPoolId(Long poolId);
	List<SecurityEntity> getSecuritiesWithMaaPoa(Long maaPoaId);
	boolean monthlyReportExistForYearMonthBySecurity(int securityId, int year, int month);
	boolean allSecuritiesInPoolAreFreeholdType(Long poolId);
	boolean allSecuritiesInPoolAreBlanketPurpose(Long poolId);
	
	List<SecuritySearchResultDTO> searchDeletedSecurity(DeletedSecuritySearchParametersDTO searchParams);

    List<Long> getPoolIdsByVbBlanketSecurity(Long vbContactId);
    void addCEEnrolmentDecisionEntity(CEEnrolmentDecisionEntity entity);
	void saveCEEnrolmentDecisionEntity(CEEnrolmentDecisionEntity entity);
	List<SecurityEntity> getBlanketSecuritiesByEnrollingVb(String enrolmentNumber);
	CEEnrolmentDecisionEntity getOutstandingCEEnrolmentDecisionEntityByEnrolmentNumber(String enrolmentNumber);
	public boolean hasSecurityHadStatus(Long securityId, Long statusId);
}
