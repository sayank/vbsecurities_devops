/* 
 * 
 * JmsTransactionLogDAOImpl.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.util;

import java.util.List;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.tarion.vbs.orm.entity.JmsTransactionLogEntity;

/**
 * JmsTransactionLogDAOImpl class for DB operations on Jms Transaction Log Table
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-04-04
 * @version 1.0
 */
@Stateless(mappedName = "ejb/JmsTransactionLogDAO")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class JmsTransactionLogDAOImpl implements JmsTransactionLogDAO {
	
	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;

	@Override
	public List<JmsTransactionLogEntity> findTranscationLogLikeTrackingNumber(String trackingNumberLike) {
		TypedQuery<JmsTransactionLogEntity> query = vbsEm.createNamedQuery(JmsTransactionLogEntity.SELECT_TRANSACTIONS_LIKE_BY_TRACKING_NUMBER, JmsTransactionLogEntity.class);
		query.setParameter(1, trackingNumberLike + "%");
		return query.getResultList();
	}
	
	
}
