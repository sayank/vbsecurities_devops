/* 
 * 
 * SecurityDAOImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.financialinstitution;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionAmountsDTO;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.financialinstitution.BranchEntity;
import com.tarion.vbs.orm.entity.financialinstitution.ContactFinancialInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionAlertEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionMaaPoaEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionMaaPoaInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionSigningOfficerEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;

/**
 * LookupDAO class for getting all lookup data
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-16
 * @version 1.0
 */
@Stateless(mappedName = "ejb/FinancialInstitutionDAO")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class FinancialInstitutionDAOImpl implements FinancialInstitutionDAO {
	
	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;

	@SuppressWarnings("unchecked")
	@Override
	public List<FinancialInstitutionEntity> getAllFinancialInstitutions() {
		Query query = vbsEm.createNamedQuery(FinancialInstitutionEntity.SELECT_ALL_FINANCIAL_INSTITUTIONS);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	@ExcludeClassInterceptors
	public List<BranchEntity> getAllBranches() {
		Query query = vbsEm.createNamedQuery(BranchEntity.SELECT_ALL_BRANCHES);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FinancialInstitutionEntity> getFinancialInstitutionsForNameLike(String name, int maxResults) {
		Query query = vbsEm.createNamedQuery(FinancialInstitutionEntity.SELECT_ALL_FINANCIAL_INSTITUTIONS_NAME_LIKE);
		query.setParameter(1, VbsUtil.getSearchLikeString(name));
		if(maxResults != 0) { query.setMaxResults(maxResults); }
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FinancialInstitutionEntity> getFinancialInstitutionsForIdLike(String id, int maxResults) {
		Query query = vbsEm.createNamedQuery(FinancialInstitutionEntity.SELECT_ALL_FINANCIAL_INSTITUTIONS_ID_LIKE);
		query.setParameter(1, VbsUtil.getSearchLikeString(id));
		if(maxResults != 0) { query.setMaxResults(maxResults); }
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@ExcludeClassInterceptors
	public List<BranchEntity> getBranchesForFinancialInstitution(Long financialInstitutionId) {
		Query query = vbsEm.createNamedQuery(BranchEntity.SELECT_ALL_BRANCHES_FOR_FINANCIAL_INSTITUTION);
		query.setParameter(1, financialInstitutionId);
		return query.getResultList();
	}

	
	private String addJpqlForCodeType(String inputJpql, String code, String codeType, Long codeTo) {
		String jpql = inputJpql;
		if(!VbsUtil.isNullorEmpty(code)) {
			switch(codeType) {
				case "EQUALS":
					jpql += " AND f.id = :code ";
				break;
				case "NOT_EQUALS":
					jpql += " AND f.id != :code ";
				break;
				case "LESS_THAN":
					jpql += " AND f.id < :code ";
				break;
				case "GREATER_THAN":
					jpql += " AND f.id > :code ";
				break;
				case "IN":
					if(code.contains(",")) {
						// List of codes is retrieved separately
						jpql += " AND f.id IN :codes ";
					} 
				break;
				case "BETWEEN":
					if(codeTo != null) {
						jpql += " AND f.id BETWEEN :code AND :codeTo ";
					}
				break;
				default: break;
			}
		}
		return jpql;
	}
	
	private List<Long> getJpqlCodes(String code) {
		if(!VbsUtil.isNullorEmpty(code) && code.contains(",")) {
			List<String> codeList = Arrays.asList(code.split(","));
			return codeList.stream().map(Long::parseLong).collect(Collectors.toList());
		} 
		return Collections.emptyList();
	}
	
	private String addJpqlForName(String jpqlInput, String name, String nameType, String nameTo) {
		String jpql = jpqlInput;
		if(!VbsUtil.isNullorEmpty(name)) {
			if ("BEGINS_WITH".equals(nameType)) {
				jpql = addJpqlUpperLike(jpql);
			} else if ("CONTAINS".equals(nameType)) {
				jpql = addJpqlUpperLike(jpql);
			} else if ("EQUALS".equals(nameType)) {
				jpql = addJpqlUpperOperand(jpql, "=");
			} else if ("LESS_THAN".equals(nameType)) {
				jpql = addJpqlUpperOperand(jpql, "<");
			} else if ("GREATER_THAN".equals(nameType)) {
				jpql = addJpqlUpperOperand(jpql, ">");
			} else if ("BETWEEN".equals(nameType)) {
				jpql = addJpqlUpperBetween(jpql, nameTo);
			} else {
				throw new VbsRuntimeException("Unknown search type");
			}
		}
		return jpql;
	}
	
	private String addJpqlUpperBetween(String jpqlInput, String nameTo) {
		String jpql = jpqlInput;
		if(!VbsUtil.isNullorEmpty(nameTo)) {
			jpql += " AND UPPER(f.name) BETWEEN :name AND :nameTo";
		}
		return jpql;
	}
	
	private String addJpqlUpperLike(String jpqlInput) {
		String jpql = jpqlInput;
		jpql += " AND UPPER(f.name) LIKE :name";
		return jpql;
	}
	
	private String addJpqlUpperOperand(String jpqlInput, String operand) {
		String jpql = jpqlInput;
		jpql += " AND UPPER(f.name) " + operand + " :name";
		return jpql;
	}

	@Override
	public List<FinancialInstitutionEntity> getFinancialInstitutionsByCodeOrName(String code, Long codeTo, String codeType, String name, String nameTo, String nameType, Long financialInstitutionTypeId) {
		String jpql = "SELECT f FROM FinancialInstitutionEntity f WHERE 1=1 ";

		List<Long> codes = getJpqlCodes(code);
		if(!VbsUtil.isNullorEmpty(code)) {
			jpql = addJpqlForCodeType(jpql, code, codeType, codeTo);
		}
		if(!VbsUtil.isNullorEmpty(name)) {
			name = name.toUpperCase();
			jpql = addJpqlForName(jpql, name, nameType, nameTo);
		}
		
		if (financialInstitutionTypeId != null) {
			jpql += " AND f.financialInstitutionType.id = :financialInstitutionTypeId";
		}

		switch(nameType) {
			case "BEGINS_WITH":
				name = name + "%";
			break;
			case "CONTAINS":
				name = "%" + name + "%";
			break;
		}

		
		TypedQuery<FinancialInstitutionEntity> query = vbsEm.createQuery(jpql, FinancialInstitutionEntity.class);
		if(jpql.contains("f.id")) {
			if(codes.isEmpty()) {
				query.setParameter("code", Long.parseLong(code));
				if(jpql.contains("codeTo")) {
					query.setParameter("codeTo", codeTo);
				}
			} else {
				query.setParameter("codes", codes);
			}
		}
		if(jpql.contains("f.name")) {
			query.setParameter("name", name);
			if(jpql.contains("nameTo")) {
				query.setParameter("nameTo", nameTo);
			}
		}
		if (jpql.contains("financialInstitutionTypeId")) {
			query.setParameter("financialInstitutionTypeId", financialInstitutionTypeId);
		}
		
		return query.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public FinancialInstitutionAmountsDTO getDirectFinancialInstitutionAmounts(Long financialInstitutionId, boolean includePEFDTA) {
		Query query = vbsEm.createNamedQuery(SecurityEntity.SELECT_DIRECT_FINANCIAL_INSTITUTION_AMOUNTS);
		query.setParameter(1, financialInstitutionId);
		query.setParameter(2, includePEFDTA);
		List<Object[]> result = query.getResultList();
		if (result != null && !result.isEmpty()) {
			FinancialInstitutionAmountsDTO dto = new FinancialInstitutionAmountsDTO();
			dto.setTotalOriginalAmount((BigDecimal)result.get(0)[0]);
			dto.setTotalCurrentAmount((BigDecimal)result.get(0)[1]);
			return dto;
		}
		return null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public FinancialInstitutionAmountsDTO getMaaPoaFinancialInstitutionAmounts(Long financialInstitutionId, boolean includePEFDTA) {
		Query query = vbsEm.createNamedQuery(SecurityEntity.SELECT_MAA_POA_FINANCIAL_INSTITUTION_AMOUNTS);
		query.setParameter(1, financialInstitutionId);
		query.setParameter(2, includePEFDTA);
		List<Object[]> result = query.getResultList();
		if (result != null && !result.isEmpty()) {
			FinancialInstitutionAmountsDTO dto = new FinancialInstitutionAmountsDTO();
			dto.setTotalOriginalAmount((BigDecimal)result.get(0)[0]);
			dto.setTotalCurrentAmount((BigDecimal)result.get(0)[1]);
			return dto;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ContactFinancialInstitutionEntity> getContactsForFinancialInstitution(Long financialInstitutionId) {
		Query query = vbsEm.createNamedQuery(ContactFinancialInstitutionEntity.SELECT_ALL_CONTACTS_FOR_FINANCIAL_INSTITUTION_ID);
		query.setParameter(1, financialInstitutionId);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FinancialInstitutionSigningOfficerEntity> getSigningOfficersForFinancialInstitutionMaaPoa(Long financialInstitutionMaaPoaId) {
		Query query = vbsEm.createNamedQuery(FinancialInstitutionSigningOfficerEntity.SELECT_ALL_SIGNING_OFFICERS_FOR_MAAPOA_ID);
		query.setParameter(1, financialInstitutionMaaPoaId);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FinancialInstitutionMaaPoaEntity> getMaaPoasForFinancialInstitution(Long financialInstitutionId) {
		Query query = vbsEm.createNamedQuery(FinancialInstitutionMaaPoaEntity.SELECT_ALL_MAAPOA_FOR_FINANCIAL_INSTITUTION_ID);
		query.setParameter(1, financialInstitutionId);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FinancialInstitutionMaaPoaInstitutionEntity> getMaaPoaInstitutionsForMaaPoa(Long maaPoaId) {
		Query query = vbsEm.createNamedQuery(FinancialInstitutionMaaPoaInstitutionEntity.SELECT_ALL_MAAPOA_INSTITUTIONS_FOR_MAAPOA_ID);
		query.setParameter(1, maaPoaId);
		return query.getResultList();
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<FinancialInstitutionAlertEntity> getAlertsForFinancialInstitution(Long financialInstitutionId) {
        Query query = vbsEm.createNamedQuery(FinancialInstitutionAlertEntity.SELECT_ALERT_FOR_FINANCIAL_INSTITUTION_ID);
        query.setParameter(1, financialInstitutionId);
        return query.getResultList();
    }

    @Override
    public Long getFinancialInstitutionTypeId(Long financialInstitutionId) {
		TypedQuery<Long> query = vbsEm.createNamedQuery(FinancialInstitutionEntity.SELECT_TYPE_FOR_FINANCIAL_INSTITUTION_ID, Long.class);
		query.setParameter(1, financialInstitutionId);
		List<Long> result = query.getResultList();
		if(result.isEmpty()) {
			return null;
		}
		return result.get(0);
	}
}
