package com.tarion.vbs.dao.dashboard;

import java.util.List;

import javax.ejb.Local;

import com.tarion.vbs.common.dto.dashboard.ManagerReleaseDTO;
import com.tarion.vbs.orm.entity.enrolment.CEEnrolmentDecisionEntity;

@Local
public interface DashboardDAO {
	public List<ManagerReleaseDTO> getReleasesForManagerUserId(String userId);

    List<CEEnrolmentDecisionEntity> getOutstandingCEDecisions();
}
