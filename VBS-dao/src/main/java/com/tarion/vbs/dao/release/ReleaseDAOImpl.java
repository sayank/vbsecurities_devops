/* 
 * 
 * ReleaseDAOImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.release;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentWarrantyServiceFieldsDTO;
import com.tarion.vbs.common.dto.release.PendingReleaseDTO;
import com.tarion.vbs.common.dto.release.ReleaseSearchParametersDTO;
import com.tarion.vbs.common.dto.release.ReleaseTypeDTO;
import com.tarion.vbs.common.enums.ReleaseStatus;
import com.tarion.vbs.common.enums.SearchTypeEnum;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.release.ReleaseEnrolmentEntity;
import com.tarion.vbs.orm.entity.release.ReleaseEntity;
import com.tarion.vbs.orm.entity.release.ReleaseReasonEntity;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ReleaseDAO class for DB operations on Security and Release tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-01-08
 * @version 1.0
 */
@Stateless(mappedName = "ejb/ReleaseDAO")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class ReleaseDAOImpl implements ReleaseDAO {
	
	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;

	@Override
	public List<ReleaseEntity> getReleases(Long securityId) {
		TypedQuery<ReleaseEntity> query = vbsEm.createNamedQuery(ReleaseEntity.SELECT_RELEASES_FOR_SECURITY, ReleaseEntity.class);
		query.setParameter(1, securityId);
		return query.getResultList();
	}
	
	@Override
	public List<ReleaseEnrolmentEntity> getEnrolmentsByReleaseId(Long releaseId) {
		TypedQuery<ReleaseEnrolmentEntity> query = vbsEm.createNamedQuery(ReleaseEnrolmentEntity.SELECT_ENROLMENTS_BY_RELEASE_ID, ReleaseEnrolmentEntity.class);
		query.setParameter(1, releaseId);
		return query.getResultList();
	}

	@Override
	public List<String> getEnrolmentNumberssByReleaseId(Long releaseId) {
		TypedQuery<String> query = vbsEm.createNamedQuery(ReleaseEnrolmentEntity.SELECT_ENROLMENT_NUMBERS_BY_RELEASE_ID, String.class);
		query.setParameter(1, releaseId);
		return query.getResultList();
	}

	@Override
	public long getNumberOfReleases(Long securityId) {
		Query query = vbsEm.createNamedQuery(ReleaseEntity.SELECT_NUMBER_OF_RELEASES);
		query.setParameter(1, securityId);
		return (long)query.getSingleResult();
	}

	@Override
    public List<ReleaseReasonEntity> getReleaseReasonsByReleaseId(Long releaseId) {
        TypedQuery<ReleaseReasonEntity> query = vbsEm.createNamedQuery(ReleaseReasonEntity.SELECT_REASONS_BY_RELEASE_ID, ReleaseReasonEntity.class);
        query.setParameter(1, releaseId);
        return query.getResultList();
    }
	
	@Override
    public List<ReleaseEntity> getReleaseByStatus(ReleaseStatus releaseStatus) {
        TypedQuery<ReleaseEntity> query = vbsEm.createNamedQuery(ReleaseEntity.SELECT_RELEASES_BY_STATUS, ReleaseEntity.class);
        query.setParameter(1, releaseStatus);
        return query.getResultList();
    }

	@Override
	public List<Tuple> getSecurityIdReleaseIdByStatus(ReleaseStatus releaseStatus, List<Long> securityIds) {
		List<Long> releaseTypes = new ArrayList<>();
		
		releaseTypes.add(VbsConstants.RELEASE_TYPE_RELEASE);
		releaseTypes.add(VbsConstants.RELEASE_TYPE_REPLACE);
		releaseTypes.add(VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR);
		
		TypedQuery<Tuple> query = vbsEm.createNamedQuery(ReleaseEntity.SELECT_SECURITY_ID_RELEASE_ID_BY_STATUS, Tuple.class);
		
		query.setParameter(1, releaseStatus);
		query.setParameter(2, releaseTypes);
		query.setParameter(3, securityIds);

		return query.getResultList();
	}

	public long getReleaseDemandCollectorByStatusAndSecurityId(ReleaseStatus releaseStatus, Long securityId)  {
		Query query = vbsEm.createNamedQuery(ReleaseEntity.SELECT_DEMAND_COLLECTOR_RELEASES_BY_STATUS_SECURITY_ID);
		query.setParameter(1, releaseStatus);
		query.setParameter(2, securityId);
		return (long)query.getSingleResult();
	}

	@Override
    public List<ReleaseEntity> getReleaseByStatuses(ReleaseStatus releaseStatus1, ReleaseStatus releaseStatus2) {
        TypedQuery<ReleaseEntity> query = vbsEm.createNamedQuery(ReleaseEntity.SELECT_NUMBER_OF_RELEASES, ReleaseEntity.class);
        query.setParameter(1, releaseStatus1);
        query.setParameter(2, releaseStatus2);
        return query.getResultList();
    }

	@Override
	public long getNumberOfPendingApprovalReleaseByStatuses(Long securityId, ReleaseStatus releaseStatus) {
		Query query = vbsEm.createNamedQuery(ReleaseEntity.SELECT_PENDING_APPROVAL_RELEASES_BY_STATUS);
		query.setParameter(1, securityId);
		query.setParameter(2, releaseStatus);
		return (long)query.getSingleResult();
	}

    @Override
    public ReleaseEntity getReleaseBySecurityIdSequenceNumber(Long securityId, Long sequenceNumber) {
		TypedQuery<ReleaseEntity> query = vbsEm.createNamedQuery(ReleaseEntity.SELECT_RELEASE_BY_SECURITY_SEQUENCE, ReleaseEntity.class);
		query.setParameter(1, securityId);
		query.setParameter(2, sequenceNumber);
		return query.getSingleResult();
    }

    @Override
	public List<ReleaseEnrolmentEntity> getReleasesByEnrolmentNumber(String enrolmentNumber) {
		TypedQuery<ReleaseEnrolmentEntity> query = vbsEm.createNamedQuery(ReleaseEnrolmentEntity.SELECT_RELEASES_BY_ENROLMENT_NUMBER, ReleaseEnrolmentEntity.class);
		query.setParameter(1, enrolmentNumber);
		return query.getResultList();
	}

	@Override
	public List<ReleaseEnrolmentEntity> getReleasesBySecurityId(Long securityId) {
		TypedQuery<ReleaseEnrolmentEntity> query = vbsEm.createNamedQuery(ReleaseEnrolmentEntity.SELECT_ENROLMENTS_BY_SECURITY_ID, ReleaseEnrolmentEntity.class);
		query.setParameter(1, securityId);
		return query.getResultList();
	}

	@Override
	public List<ReleaseEntity> getReleasesByStatusesFromToDate(List<ReleaseStatus> releaseStatuses, LocalDateTime startDate, LocalDateTime endDate) {
        TypedQuery<ReleaseEntity> query = vbsEm.createNamedQuery(ReleaseEntity.SELECT_RELEASES_BY_STATUSES_FROM_TO_DATE, ReleaseEntity.class);
        query.setParameter(1, releaseStatuses);
        if (startDate != null) {
        	query.setParameter(2, startDate);
        } else {
        	query.setParameter(2, DateUtil.getLocalDateTimeMin());
        }
        if (endDate != null) {
        	query.setParameter(3, endDate);
        } else {
        	query.setParameter(3, DateUtil.getLocalDateTimeMax());
        }
        return query.getResultList();
	}
	
	@Override
	public List<EnrolmentWarrantyServiceFieldsDTO> getEnrolmentWarrantyServiceFieldsList(List<String> enrolmentNumbers) {
		TypedQuery<Tuple> query = vbsEm.createNamedQuery(ReleaseEntity.SELECT_WARRANTY_FIELDS_BY_ENROLMENT_NUMBER, Tuple.class);
		query.setParameter(1, enrolmentNumbers);
		List<Tuple> ret = query.getResultList();
		return ret.stream().map(this::transformEnrolmentWarrantyFields).collect(Collectors.toList());
	}

	@Override
	public boolean securityHasCompletedPrincipalReleases(Long securityId){
		Query query = vbsEm.createNamedQuery(ReleaseEntity.SELECT_NUMBER_OF_COMPLETED_PRINCIPAL_RELEASES);
		query.setParameter(1, securityId);
		long res = (long) query.getSingleResult();
		return res > 0;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<PendingReleaseDTO> searchReleases(ReleaseSearchParametersDTO params) {
		String sql = 
				"SELECT o.id as id, o.security.id as securityId, o.comment as comment, o.interestAmount as interestAmount, o.principalAmount as principalAmount, o.security.currentAmount as currentAmount, " +
				"	o.security.currentInterest as currentInterest, o.security.forAvailableInterest as forAvailableInterest, o.authorizedAmount as authorizedAmount, o.security.receivedDate as receivedDate, o.security.securityType.id as securityTypeId, " +
				" 	o.referenceNumber as referenceNumber, o.finalApprovalBy as finalApprovalBy, o.security.instrumentNumber as instrumentNumber, o.finalApprovalDate as finalApprovalDate,  " + 
				"	o.security.securityType.name as securityTypeName, o.security.originalAmount as originalAmount, o.releaseType.name as releaseTypeName, o.security.primaryVb.crmContactId AS vbNumber, o.security.primaryVb.companyName AS vbName, " +
				"	c.id as contactIdC, c.crmContactId as crmContactIdC, c.companyName as companyNameC, c.firstName as firstNameC, c.lastName as lastNameC,  " +
				"	o.security.primaryVb.id as contactIdVb, o.security.primaryVb.crmContactId as crmContactIdVb, o.security.primaryVb.companyName as companyNameVb, o.security.primaryVb.firstName as firstNameVb, o.security.primaryVb.lastName as lastNameVb,  " +
                "	o.status as releaseStatus " +
				"FROM ReleaseEntity o " + 
				"LEFT OUTER JOIN o.contact c " +
				"	WHERE o.security.currentAmount > 0 AND ( (o.releaseType.id IN (" + VbsConstants.RELEASE_TYPE_RELEASE + ", "+ VbsConstants.RELEASE_TYPE_REPLACE + ") and o.security.securityType.id = "+VbsConstants.SECURITY_TYPE_CASH+") " +
				"				OR o.releaseType.id NOT IN (" + VbsConstants.RELEASE_TYPE_RELEASE + ", "+ VbsConstants.RELEASE_TYPE_REPLACE + ") ) ";
		
		LocalDateTime startDate = null;
		LocalDateTime endDate = null;
		ReleaseStatus releaseStatus = null;
		List<Long> releaseTypes = null;

		if (params.getReleaseUpdateStartDate() != null) {
			if (SearchTypeEnum.BETWEEN.equals(params.getDateSearchType()) && params.getReleaseUpdateEndDate() != null) {
				sql += " and o.updateDate >= :startDate";
				startDate = params.getReleaseUpdateStartDate();

				sql += " and o.updateDate < :endDate ";
				endDate = params.getReleaseUpdateEndDate().plusDays(1);
			} else if (SearchTypeEnum.EQUALS.equals(params.getDateSearchType())) {
				sql += " and o.updateDate >= :startDate ";
				startDate = params.getReleaseUpdateStartDate();
				sql += " and o.updateDate < :endDate ";
				endDate = params.getReleaseUpdateStartDate().plusDays(1);
			} else if (SearchTypeEnum.EQUALS_GREATER_THAN.equals(params.getDateSearchType())) {
				sql += " and o.updateDate >= :startDate ";
				startDate = params.getReleaseUpdateStartDate();
			} else if (SearchTypeEnum.GREATER_THAN.equals(params.getDateSearchType())) {
				sql += " and o.updateDate > :startDate ";
				startDate = params.getReleaseUpdateStartDate().plusDays(1);
			} else if (SearchTypeEnum.EQUALS_LESS_THAN.equals(params.getDateSearchType())) {
				sql += " and o.updateDate < :endDate ";
				endDate = params.getReleaseUpdateStartDate().plusDays(1);
			} else if (SearchTypeEnum.LESS_THAN.equals(params.getDateSearchType())) {
				sql += " and o.updateDate < :endDate ";
				endDate = params.getReleaseUpdateStartDate();
			}
		}
		if(params.getReleaseTypes()!=null) {
			releaseTypes = new ArrayList<>();
			for (ReleaseTypeDTO releaseType : params.getReleaseTypes()) {
				releaseTypes.add(releaseType.getId());
			}
			if(!releaseTypes.isEmpty()) {
				sql += " and o.releaseType.id IN (:releaseTypes)";
			}
		}
		if(params.getReleaseStatus()!=null) {
			sql += " and o.status = :releaseStatus ";
			releaseStatus = params.getReleaseStatus();
		}else{
			sql += " and o.status IN (com.tarion.vbs.common.enums.ReleaseStatus.PENDING, com.tarion.vbs.common.enums.ReleaseStatus.SENT) ";
		}
		
		if(startDate == null && endDate == null && releaseStatus == null && (releaseTypes==null || releaseTypes.isEmpty())) {
			sql += " and o.updateDate >= DATEADD(MONTH, -3, GETDATE()) ";
		}

		sql += " ORDER BY o.id ";
		
        TypedQuery query = vbsEm.createQuery(sql, Tuple.class);
        if (startDate != null) {
        	query.setParameter("startDate", startDate);
        }
        if (endDate != null) {
        	query.setParameter("endDate", endDate);
		}
		if(releaseStatus != null) {
			query.setParameter("releaseStatus", releaseStatus);
		}
		if(releaseTypes!=null && !releaseTypes.isEmpty()) {
			query.setParameter("releaseTypes", releaseTypes);
		}

		List<Tuple> ret = query.getResultList();
		return ret.stream().map(this::transformReleaseForSearch).collect(Collectors.toList());
	}

	private EnrolmentWarrantyServiceFieldsDTO transformEnrolmentWarrantyFields(Tuple t) {
		EnrolmentWarrantyServiceFieldsDTO dto = new EnrolmentWarrantyServiceFieldsDTO();
		dto.setEnrolmentNumber(t.get("enrolmentNumber", String.class));
		dto.setWsInputProvidedBy(t.get("wsInputProvidedBy", String.class));
		dto.setWsInputRequested(t.get("wsInputRequested", Boolean.class));
		dto.setWsReceivedDate(t.get("wsReceivedDate", LocalDateTime.class));
		dto.setWsRecommendation(t.get("wsRecommendation", String.class));
		dto.setWsRequestedDate(t.get("wsRequestedDate", LocalDateTime.class));
		dto.setWsRequestor(t.get("wsRequestor", String.class));
		dto.setFcmAmountRetained(t.get("fcmAmountRetained", BigDecimal.class));
		
		return dto;
	}
	private PendingReleaseDTO transformReleaseForSearch(Tuple t) {
		PendingReleaseDTO dto = new PendingReleaseDTO();
		dto.setReleaseId(t.get("id", Long.class));
		dto.setSecurityId(t.get("securityId", Long.class));
		dto.setComment(t.get("comment", String.class));
		dto.setInterestAmount(t.get("interestAmount", BigDecimal.class));
		dto.setPrincipalAmount(t.get("principalAmount", BigDecimal.class));
		dto.setReferenceNumber(t.get("referenceNumber", String.class));
		dto.setApprovalManager(t.get("finalApprovalBy", String.class));
		dto.setCurrentAmount(t.get("currentAmount", BigDecimal.class));
		dto.setInstrumentNumber(t.get("instrumentNumber", String.class));
		dto.setManagerApprovalDate(t.get("finalApprovalDate", LocalDateTime.class));
		dto.setOriginalAmount(t.get("originalAmount", BigDecimal.class));
		dto.setCurrentInterestAmount(t.get("currentInterest", BigDecimal.class));
		BigDecimal forAvail = t.get("forAvailableInterest", BigDecimal.class);
		dto.setAvailableInterestAmount(dto.getCurrentInterestAmount().subtract(forAvail));
		dto.setReleaseTypeName(t.get("releaseTypeName", String.class));
		dto.setVbNumber(t.get("vbNumber", String.class));
		dto.setVbName(t.get("vbName", String.class));
		dto.setAuthorizedAmount(t.get("authorizedAmount", BigDecimal.class));
		dto.setSecurityTypeName(t.get("securityTypeName", String.class));
        dto.setReleaseStatus(t.get("releaseStatus", ReleaseStatus.class));

		String crmContactId = t.get("crmContactIdC", String.class);
		ContactDTO payee = new ContactDTO();
		if(!VbsUtil.isNullorEmpty(crmContactId)) {
			Long contactId = t.get("contactIdC", Long.class);
			String companyName = t.get("companyNameC", String.class);
			String firstName = t.get("firstNameC", String.class);
			String lastName = t.get("lastNameC", String.class);
			dto.setPayeeName(VbsUtil.isNullorEmpty(companyName) ? firstName + " " + lastName : companyName);
			payee.setId(contactId);
			payee.setCrmContactId(crmContactId);
			payee.setCompanyName(companyName);
			payee.setFirstName(firstName);
			payee.setLastName(lastName);
		} else {
			crmContactId = t.get("crmContactIdVb", String.class);
			if(!VbsUtil.isNullorEmpty(crmContactId)) {
				Long contactId = t.get("contactIdVb", Long.class);
				String companyName = t.get("companyNameVb", String.class);
				String firstName = t.get("firstNameVb", String.class);
				String lastName = t.get("lastNameVb", String.class);
				dto.setPayeeName(VbsUtil.isNullorEmpty(companyName) ? firstName + " " + lastName : companyName);
				payee.setId(contactId);
				payee.setCrmContactId(crmContactId);
				payee.setCompanyName(companyName);
				payee.setFirstName(firstName);
				payee.setLastName(lastName);
			}
		}
		dto.setPayee(payee);

		if(dto.getCurrentAmount() != null && dto.getCurrentInterestAmount() != null) {
			dto.setInterestCurrentAmountTotal(dto.getCurrentAmount().add(dto.getCurrentInterestAmount()));
		} else {
			dto.setInterestCurrentAmountTotal(dto.getCurrentAmount());
		}

		if (dto.getInterestAmount() != null) {
			dto.setInterestAuthorizedAmountTotal(dto.getPrincipalAmount().add(dto.getInterestAmount()));	
		}

		if(dto.getAuthorizedAmount()!=null && dto.getCurrentAmount() != null && dto.getCurrentAmount().compareTo(BigDecimal.ZERO) != 0) {
			if(dto.getReleaseTypeName().equalsIgnoreCase("DC")){
				dto.setReleasePercentage(dto.getPrincipalAmount().divide(dto.getCurrentAmount(), 10, VbsConstants.ROUNDING_MODE));
			}else {
				dto.setReleasePercentage(dto.getAuthorizedAmount().divide(dto.getCurrentAmount(), 10, VbsConstants.ROUNDING_MODE));
			}
		}
		LocalDateTime dateNovember2004 = LocalDateTime.of(2004, 11, 1, 0, 0);
		LocalDateTime receivedDate = t.get("receivedDate", LocalDateTime.class);
		Long securityTypeId = t.get("securityTypeId", Long.class);
		dto.setReceivedPriorNov2004((receivedDate.isBefore(dateNovember2004) && securityTypeId == VbsConstants.SECURITY_TYPE_CASH));

		return dto;
	}
	
}
