/* 
 * 
 * ReleaseDAO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.release;

import java.util.List;

import javax.ejb.Local;

import com.tarion.vbs.common.dto.autorelease.*;
import com.tarion.vbs.common.dto.release.AutoReleaseDTO;
import com.tarion.vbs.orm.entity.autorelease.*;
import org.apache.commons.lang3.tuple.Pair;

/**
 * ReleaseDAO class for DB operations on Security and Release tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-01-08
 * @version 1.0
 */
@Local
public interface AutoReleaseDAO {

    AutoReleaseRunEntity getAutoReleaseRunInProgress();

    List<Pair<AutoReleaseRunEntity, Long>> searchAutoReleaseRuns(AutoReleaseRunSearchParametersDTO params);

    List<AutoReleaseEnrolmentEntity> getAutoReleaseEnrolmentsForRun(Long runId);

    List<AutoReleaseEnrolmentEntity> getAutoReleaseEnrolmentsForRunSuccess(Long runId);

    List<AutoReleaseResultDTO> getAutoReleaseResultsForRunSuccess(Long runId);
	List<AutoReleaseResultDTO> getAutoReleaseResultsForRunEliminated(Long runId);

    List<AutoReleaseCandidateDTO> getAutoReleaseCandidates();

    Long getCountIncompleteEnrolmentsForRun(Long autoReleaseRunId);

    Long getCountReleasesInRun(Long autoReleaseRunId);

    void addAutoReleaseEnrolments(List<AutoReleaseCandidateDTO> candidates);

    void addAutoReleaseEnrolmentDataEntities(List<AutoReleaseEnrolmentDataDTO> enrolments);

    void addAutoReleaseVbDataEntities(List<AutoReleaseVbDataDTO> vbs);

    void addAutoReleaseVbApplicationStatusEntities(List<AutoReleaseVbApplicationStatusDTO> vbs);

    @SuppressWarnings("deprecation")
    DataMartVbInfoDTO getDataMartVbInfo(String vbNumber);

    AutoReleaseVbDataEntity getVbData(String vbNumber, Long runId);

    AutoReleaseVbApplicationStatusEntity getVbApplicationStatus(String vbNumber, Long runId);

    AutoReleaseEnrolmentDataEntity getEnrolmentData(String vbNumber, Long runId);

    List<Long> getEliminationsForEnrolment(Long autoReleaseEnrolmentId);

    List<AutoReleaseDTO> getAutoReleaseForSecurityId(Long securityId);
}
