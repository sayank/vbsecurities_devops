package com.tarion.vbs.dao.userprofile;

import java.time.LocalDateTime;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.orm.entity.userprofile.PermissionEntity;
import com.tarion.vbs.orm.entity.userprofile.RoleEntity;
import com.tarion.vbs.orm.entity.userprofile.RolePermissionEntity;
import com.tarion.vbs.orm.entity.userprofile.UserRoleEntity;

@Stateless(mappedName = "ejb/UserDAO")
@Interceptors(EjbLoggingInterceptor.class)
public class UserDAOImpl implements UserDAO {

	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;
	
	private static final String JPQL_USERS_WITH_PERMISSION = "SELECT ur.userId FROM UserRoleEntity ur "
			+ "JOIN RolePermissionEntity rpe ON ur.roleId = rpe.roleId "
			+ "WHERE rpe.permissionId = ?1";
	
	private static final String JPQL_USERS_WITH_PERMISSION_NAME = "SELECT ur.userId FROM UserRoleEntity ur "
			+ "JOIN RolePermissionEntity rpe ON ur.roleId = rpe.roleId "
			+ "JOIN PermissionEntity pe ON rpe.permissionId = pe.id "
			+ "WHERE pe.name = ?1";
	
	@EJB
	private GenericDAO genericDAO;
	
	@Override
	public RoleEntity getRoleById(Long id) {
		return vbsEm.find(RoleEntity.class, id);
	}

	@Override
	public List<RoleEntity> getAllRoles() {
		TypedQuery<RoleEntity> query = vbsEm.createNamedQuery(RoleEntity.SELECT_ALL_ROLES, RoleEntity.class);
		return query.getResultList();
	}

	@Override
	public List<RoleEntity> getRolesForUser(String userId) {
		TypedQuery<RoleEntity> query = vbsEm.createNamedQuery(RoleEntity.SELECT_ROLES_FOR_USER, RoleEntity.class);
		query.setParameter(1, userId.toUpperCase());
		return query.getResultList();
	}

	@Override
	public RoleEntity createRole(RoleEntity entity) {
		vbsEm.persist(entity);
		vbsEm.flush();
		return entity;
	}

	@Override
	public RoleEntity updateRole(RoleEntity entity) {
		entity = vbsEm.merge(entity);
		vbsEm.flush();
		return entity;
	}

	public RolePermissionEntity addPermissionToRole(RolePermissionEntity rpe) {
		vbsEm.persist(rpe);
		vbsEm.flush();
		return rpe;
	}

	@Override
	public void removePermissionFromRole(Long permissionId, Long roleId, UserDTO user) {
		TypedQuery<RolePermissionEntity> query = vbsEm.createNamedQuery(RolePermissionEntity.SELECT_ROLE_PERMISSION, RolePermissionEntity.class);
		query.setParameter(1, permissionId);
		query.setParameter(2, roleId);
		RolePermissionEntity entity = query.getSingleResult();
		genericDAO.removeEntity(entity, user.getUserId(), LocalDateTime.now());
	}

	@Override
	public PermissionEntity getPermissionById(Long id) {
		return vbsEm.find(PermissionEntity.class, id);
	}

	@Override
	public List<PermissionEntity> getAllPermissions() {
		TypedQuery<PermissionEntity> query = vbsEm.createNamedQuery(PermissionEntity.SELECT_ALL_PERMISSION, PermissionEntity.class);
		return query.getResultList();
	}

	@Override
	public List<PermissionEntity> getPermissionsByRole(Long roleId) {
		TypedQuery<PermissionEntity> query = vbsEm.createNamedQuery(PermissionEntity.SELECT_PERMISSIONS_BY_ROLE, PermissionEntity.class);
		query.setParameter(1, roleId);
		return query.getResultList();
	}

	@Override
	public List<PermissionEntity> getPermissionsForUser(String userId) {
		TypedQuery<PermissionEntity> query = vbsEm.createNamedQuery(PermissionEntity.SELECT_PERMISSIONS_FOR_USER, PermissionEntity.class);
		query.setParameter(1, userId.toUpperCase());
		return query.getResultList();
	}

	@Override
	public List<String> getUsersByPermissionId(Long permissionId) {
		TypedQuery<String> query = vbsEm.createQuery(JPQL_USERS_WITH_PERMISSION, String.class);
		query.setParameter(1, permissionId);
		return query.getResultList();
	}

	@Override
	public List<String> getUsersByPermissionName(String permissionName) {
		TypedQuery<String> query = vbsEm.createQuery(JPQL_USERS_WITH_PERMISSION_NAME, String.class);
		query.setParameter(1, permissionName);
		return query.getResultList();
	}

	@Override
	public PermissionEntity updatePermission(PermissionEntity entity) {
		entity = vbsEm.merge(entity);
		vbsEm.flush();
		return entity;
	}

	@Override
	public List<String> getUsersByRole(Long roleId) {
		TypedQuery<String> query = vbsEm.createNamedQuery(UserRoleEntity.SELECT_USERS_WITH_ROLE, String.class);
		query.setParameter(1, roleId);
		return query.getResultList();
	}

	@Override
	public List<String> getUsersByRoleName(String roleName) {
		TypedQuery<String> query = vbsEm.createNamedQuery(UserRoleEntity.SELECT_USERS_WITH_ROLE_NAME, String.class);
		query.setParameter(1, roleName);
		return query.getResultList();
	}

	@Override
	public UserRoleEntity addUserToRole(UserRoleEntity ure) {
		vbsEm.persist(ure);
		vbsEm.flush();
		return ure;
	}

	@Override
	public List<String> getUsersWithAnyRole() {
		TypedQuery<String> query = vbsEm.createNamedQuery(UserRoleEntity.SELECT_USERS_WITH_ANY_ROLE, String.class);
		return query.getResultList();
	}

}
