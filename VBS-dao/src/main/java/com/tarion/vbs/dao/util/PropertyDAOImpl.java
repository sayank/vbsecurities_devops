/* 
 * 
 * PropertyDAOImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.util;

import java.util.List;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.tarion.vbs.orm.entity.util.PropertiesEntity;


/**
 * PropertyDAO class for Loading of all database properties from VBS_CONFIG table
 *
 */
@Stateless(mappedName = "ejb/PropertyDAO")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class PropertyDAOImpl implements PropertyDAO {

	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;
	
	@Override
	@SuppressWarnings("unchecked")
	public List<PropertiesEntity> findAllVBApplicationsProperties() {
		Query query = vbsEm.createNamedQuery(PropertiesEntity.SELECT_ALL_PROPERTIES);
		return query.getResultList();
	}
}
