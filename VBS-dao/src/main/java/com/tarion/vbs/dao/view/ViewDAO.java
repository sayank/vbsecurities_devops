/* 
 * 
 * ViewDAO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.view;

import java.util.List;

import javax.ejb.Local;

import com.tarion.vbs.orm.entity.view.MsWordSecRelLtrViewEntity;


/**
 * ViewDAO class is loading VBS views
 *
 */
@Local
public interface ViewDAO {
	public MsWordSecRelLtrViewEntity findMsWordSecRelLtrByReferenceFieldName(String referenceFieldName);
	public List<MsWordSecRelLtrViewEntity> getAllMsWordSecRelLtrs();

}
