/* 
 * 
 * AuditQueryResultDTO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.util;

import org.hibernate.envers.RevisionType;

import com.tarion.vbs.orm.entity.AuditedEntity;
import com.tarion.vbs.orm.entity.auditlog.EnversRevEntity;


/**
 * HistoryDAO class for retrieving history of changes of all the entities
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-22
 * @version 1.0
 */
public class AuditQueryResult {
	
	private final AuditedEntity entity;
	private final EnversRevEntity tracking;
	private final RevisionType revisionType;
	
	@SuppressWarnings("rawtypes")
	public AuditQueryResult(Object[] a) {
		Class entityClass = a[0].getClass();
		this.entity = (AuditedEntity) entityClass.cast(a[0]);
		this.tracking = (EnversRevEntity) a[1];
		this.revisionType = (RevisionType) a[2];
	}

	public AuditedEntity getEntity() {
		return entity;
	}
	public EnversRevEntity getTracking() {
		return tracking;
	}
	public RevisionType getRevisionType() {
		return revisionType;
	}
	
}
