package com.tarion.vbs.dao.userprofile;

import java.util.List;

import javax.ejb.Local;

import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.orm.entity.userprofile.PermissionEntity;
import com.tarion.vbs.orm.entity.userprofile.RoleEntity;
import com.tarion.vbs.orm.entity.userprofile.RolePermissionEntity;
import com.tarion.vbs.orm.entity.userprofile.UserRoleEntity;

@Local
public interface UserDAO {

	public RoleEntity getRoleById(Long id);

	public List<RoleEntity> getAllRoles();

	public List<RoleEntity> getRolesForUser(String userId);

	public RoleEntity createRole(RoleEntity entity);

	public RoleEntity updateRole(RoleEntity entity);

	public RolePermissionEntity addPermissionToRole(RolePermissionEntity rpe);

	public void removePermissionFromRole(Long permissionId, Long roleId, UserDTO user);

	public PermissionEntity getPermissionById(Long id);

	public List<PermissionEntity> getAllPermissions();

	public List<PermissionEntity> getPermissionsByRole(Long roleId);

	public List<PermissionEntity> getPermissionsForUser(String userId);

	public List<String> getUsersByPermissionId(Long permissionId);

	public List<String> getUsersByPermissionName(String permissionName);

	public PermissionEntity updatePermission(PermissionEntity entity);

	public List<String> getUsersByRole(Long roleId);

	public List<String> getUsersByRoleName(String roleName);

	public UserRoleEntity addUserToRole(UserRoleEntity ure);

	public List<String> getUsersWithAnyRole();
	
	
}