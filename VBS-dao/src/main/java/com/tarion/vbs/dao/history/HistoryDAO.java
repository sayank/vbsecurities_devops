/* 
 * 
 * HistoryDAO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.history;

import com.tarion.vbs.common.dto.enrolment.EnrolmentActivityDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentPoolActivityDTO;
import com.tarion.vbs.common.dto.financialinstitution.FIActivityDTO;
import com.tarion.vbs.common.dto.security.HistoryFieldDTO;
import com.tarion.vbs.common.dto.tip.ActivityDTO;
import com.tarion.vbs.common.enums.OrderEnum;
import com.tarion.vbs.dao.util.AuditQueryResult;

import javax.ejb.Local;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * HistoryDAO class for retrieving history of changes of all the entities
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-22
 * @version 1.0
 */
@Local
public interface HistoryDAO {

    <T> List<AuditQueryResult> getRevisions(Class<T> entityClass, Long entityId, String propertyName, OrderEnum order);

    <T> List<AuditQueryResult> getSecurityRevisions(Long securityId, String propertyName, Boolean ignoreCurrentInterest, OrderEnum order);

    List<ActivityDTO> getSecurityActivities(Long securityId);

    List<EnrolmentActivityDTO> getEnrolmentActivities(String enrolmentNumber);

    List<FIActivityDTO> getFinancialInstitutionActivities(Long financialInstitutionId);

    String getPrimaryVbNumberForSecurityAtDate(Long securityId, LocalDate date);

    Number getRevisionAtDateTime(LocalDateTime dateTime);

    BigDecimal getSecurityCurrentAmountForSecurityAtRevision(Long securityId, Number revisionNumber);

    List<HistoryFieldDTO> getSecurityCommentHistory(Long securityId);

    List<EnrolmentPoolActivityDTO> deletedEnrolmentsActivities(Long securityId);
    List<EnrolmentPoolActivityDTO> deletedVbActivities(Long poolId);
}
