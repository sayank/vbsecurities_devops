/* 
 * 
 * ViewDAOImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.view;

import java.util.List;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.view.MsWordSecRelLtrViewEntity;


/**
 * ViewDAO class is loading VBS views
 *
 */
@Stateless(mappedName = "ejb/ViewDAO")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class ViewDAOImpl implements ViewDAO {

	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;
	
	@Override
	public MsWordSecRelLtrViewEntity findMsWordSecRelLtrByReferenceFieldName(String referenceFieldName) {
		TypedQuery<MsWordSecRelLtrViewEntity> query = vbsEm.createNamedQuery(MsWordSecRelLtrViewEntity.SELECT_MSWORD_SEC_REL_LTR_VW_BY_REFERENCE_FIELD_NAME, MsWordSecRelLtrViewEntity.class);
		query.setParameter(1, referenceFieldName);
		return query.getSingleResult();
	}
	
	@Override
	public List<MsWordSecRelLtrViewEntity> getAllMsWordSecRelLtrs() {
		TypedQuery<MsWordSecRelLtrViewEntity> query = vbsEm.createNamedQuery(MsWordSecRelLtrViewEntity.SELECT_ALL_MSWORD_SEC_REL_LTR_VW, MsWordSecRelLtrViewEntity.class);
		return query.getResultList();
	}
}
