/* 
 * 
 * SecurityDAOImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.security;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.security.*;
import com.tarion.vbs.common.dto.tip.SecurityTipInfoDTO;
import com.tarion.vbs.common.enums.SearchTypeEnum;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.financialinstitution.FinancialInstitutionDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.orm.entity.contact.ContactEntity;
import com.tarion.vbs.orm.entity.enrolment.CEEnrolmentDecisionEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentPoolEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionTypeEntity;
import com.tarion.vbs.orm.entity.security.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.jpa.QueryHints;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.*;
import java.math.BigDecimal;
import java.security.Security;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

/**
 * SecurityDAO class for getting all security data
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-16
 * @version 1.0
 */
@Stateless(mappedName = "ejb/SecurityDAO")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class SecurityDAOImpl implements SecurityDAO {

	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;

	@EJB
	private GenericDAO genericDAO;
	@EJB
	private FinancialInstitutionDAO financialInstitutionDAO;

	private static final String SECURITY_NUMBER = "securityNumber";
	private static final String INSTRUMENT_NUMBER = "instrumentNumber";
	private static final String ESCROW_AGENT_CRM_CONTACT_ID = "escrowAgentCrmContactId";
	private static final String VB_NUMBER = "vbNumber";

	private static final String ESCROW_AGENT_QUERY_STRING = "Escrow Agent = ";
	private static final String ESCROW_AGENT_ID_QUERY_STRING = "Escrow Agent Id = ";

	@Override
	public List<String> getAllInstrumentNumbersLike(String instrumentNumber) {
		TypedQuery<String> query = vbsEm
				.createNamedQuery(SecurityEntity.SELECT_ALL_INSTRUMENT_NUMBERS_LIKE, String.class)
				.setMaxResults(VbsConstants.TYPEAHEAD_MAX_RESULTS);
		query.setParameter(1, VbsUtil.getSearchLikePrefixString(instrumentNumber));
		return query.getResultList();
	}

	@Override
	public List<SecurityEntity> getSecurityByInstrumentNumber(String instrumentNumber) {
		TypedQuery<SecurityEntity> query = vbsEm.createNamedQuery(SecurityEntity.SELECT_SECURITY_BY_INSTRUMENT_NUMBER,
				SecurityEntity.class);
		query.setParameter(1, instrumentNumber);
		return query.getResultList();
	}
	
	@Override
	public boolean securityWithTheSameInstrumentNumberExists(String instrumentNumber) {
		TypedQuery<Long> query = vbsEm.createNamedQuery(SecurityEntity.COUNT_SECURITY_BY_SAME_INSTRUMENT_NUMBER,
				Long.class);
		query.setParameter(1, instrumentNumber);
		List<Long> result = query.getResultList();
		if (result.isEmpty()) {
			return false;
		}
		return result.get(0) > 0;
	}

	@Override
	public List<SecurityEntity> getSecurityByVbNumber(String vbNumber) {
		TypedQuery<SecurityEntity> query = vbsEm.createNamedQuery(SecurityEntity.SELECT_SECURITY_BY_VB_NUMBER,
				SecurityEntity.class);
		query.setParameter(1, vbNumber);
		return query.getResultList();
	}

	@Override
	public List<SecurityTipInfoDTO> getSecurityByVbNumberBlanket(String vbNumber) {
		TypedQuery<Tuple> query = vbsEm.createNamedQuery(SecurityEntity.SELECT_SECURITY_BY_VB_NUMBER_BLANKET, Tuple.class);
		query.setParameter(1, vbNumber);
		List<Tuple> res = query.getResultList();
		return res.stream().map(this::transformToTipInfoDTO).collect(Collectors.toList());
	}

	@Override
	public List<SecurityEntity> getSecurityByPoolId(Long poolId) {
		TypedQuery<SecurityEntity> query = vbsEm.createNamedQuery(SecurityEntity.SELECT_SECURITIES_BY_POOL_ID,
				SecurityEntity.class);
		query.setParameter(1, poolId);
		return query.getResultList();
	}

	@Override
	public List<SecurityEntity> getAllSecuritiesByInstrumentNumbersLike(String instrumentNumber) {
		TypedQuery<SecurityEntity> query = vbsEm
				.createNamedQuery(SecurityEntity.SELECT_ALL_SECURITIES_BY_INSTRUMENT_NUMBER_LIKE, SecurityEntity.class)
				.setMaxResults(VbsConstants.TYPEAHEAD_MAX_RESULTS);
		query.setParameter(1, VbsUtil.getSearchLikePrefixString(instrumentNumber));
		return query.getResultList();
	}

	@Override
	public Long getPoolForSecurityById(Long securityId) {
		TypedQuery<Long> query = vbsEm.createNamedQuery(SecurityEntity.SELECT_POOL_ID, Long.class);
		query.setParameter(1, securityId);
		List<Long> result = query.getResultList();
		if (result.isEmpty()) {
			return null;
		}
		return result.get(0);
	}

	@Override
	public List<String> getAllVbNumbersLike(String vbNumber) {
		TypedQuery<String> query = vbsEm.createNamedQuery(ContactEntity.SELECT_ALL_VB_NUMBERS_LIKE, String.class)
				.setMaxResults(VbsConstants.TYPEAHEAD_MAX_RESULTS);
		query.setParameter(1, VbsUtil.getSearchLikePrefixString(vbNumber));
		return query.getResultList();
	}

	@Override
	public boolean monthlyReportExistForYearMonthBySecurity(int securityId, int year, int month) {
		TypedQuery<MonthlyReportEntity> query = vbsEm.createNamedQuery(MonthlyReportEntity.SELECT_MONTHLY_REPORT_BY_SECURITY_YEAR_MONTH, MonthlyReportEntity.class);
		query.setParameter(1, securityId);
		query.setParameter(2, year);
		query.setParameter(3, month);
		return !query.getResultList().isEmpty();
	}

	public MonthlyReportEntity getMonthlyReportForYearMonthBySecurity(int securityId, int year, int month) {
		TypedQuery<MonthlyReportEntity> query = vbsEm.createNamedQuery(MonthlyReportEntity.SELECT_MONTHLY_REPORT_BY_SECURITY_YEAR_MONTH, MonthlyReportEntity.class);
		query.setParameter(1, securityId);
		query.setParameter(2, year);
		query.setParameter(3, month);
		List<MonthlyReportEntity> monthlyReportEntities = query.getResultList();
		if(monthlyReportEntities != null && monthlyReportEntities.size() == 1) {
			return monthlyReportEntities.get(0);
		} else if(monthlyReportEntities != null && monthlyReportEntities.size() > 1) {
			throw new VbsRuntimeException("More than one monthly report exists for security, month and year.");
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public SecuritySearchResultWrapper searchSecurity(SecuritySearchParametersDTO searchParams, boolean displayAvailableAmount) {
		LocalDateTime start = LocalDateTime.now();
		SecuritySearchResultWrapper ret = new SecuritySearchResultWrapper();
		List<String> queryStrings = new ArrayList<>();
		Map<String, Object> params = new LinkedHashMap<>();

		StringBuilder sb = new StringBuilder();
		sb.append("WITH MAIN_RESULTS (ID) AS ( ");
		sb.append(searchParams.isShowRecentSecurities() ? "SELECT TOP 100 S.ID FROM SECURITY S " : "SELECT S.ID FROM SECURITY S WHERE 1=1 and (deleted = 0) ");
		searchParamsSecurityNumber(searchParams, sb, queryStrings, params);
		searchParamsVbNumber(searchParams, sb, queryStrings, params);
		searchParamsEnrolmentNumber(searchParams, sb, queryStrings, params);
		searchParamsPoolId(searchParams, sb, queryStrings, params);
		searchParamsFinancialInstitutionIdBranchId(searchParams, sb, queryStrings, params);
		searchParamsEscrowAgentContactId(searchParams, sb, queryStrings, params);
		searchParamsFinancialInstitutionTypeId(searchParams, sb, queryStrings, params);

		searchParamsInstrumentNumber(searchParams, sb, queryStrings, params);
		searchParamsSecurityTypeId(searchParams, sb, queryStrings, params);
		searchParamsSecurityPurposeId(searchParams, sb, queryStrings, params);
		searchParamsReceivedDate(searchParams, sb, queryStrings, params);
		searchParamsCurrentAmount(searchParams, displayAvailableAmount, sb, queryStrings, params);

		if (!VbsUtil.isNullorEmpty(searchParams.getEnrolmentNumber())) {
			sb.append("), RESULTS (ID) AS ( " + "SELECT M.ID FROM MAIN_RESULTS M ");
			sb.append("UNION " + "SELECT S.ID FROM SECURITY S "
			+ "JOIN VB_POOL VBP ON VBP.POOL_ID = S.POOL_ID AND VBP.VB_DELETE_REASON_ID IS NULL "
			+ "JOIN ENROLMENT E ON E.ENROLLING_VB_CONTACT_ID = VBP.VB_CONTACT_ID "
			+ "WHERE E.ENROLMENT_NUMBER = :enrolmentNumber AND S.SECURITY_PURPOSE_ID = 2 "
			+ "AND 1 = "
			+ "(CASE WHEN S.IDENTITY_TYPE_ID = 2 AND E.UNIT_TYPE = 002 THEN 1 ELSE "
			+ "(CASE WHEN S.IDENTITY_TYPE_ID = 1 AND E.UNIT_TYPE != 002 THEN 1 ELSE "
			+ "(CASE WHEN S.IDENTITY_TYPE_ID = 3 OR S.IDENTITY_TYPE_ID IS NULL THEN 1 ELSE 0 END) END) END) ");

			searchParamsInstrumentNumber(searchParams, sb, queryStrings, params);
			searchParamsSecurityTypeId(searchParams, sb, queryStrings, params);
			searchParamsSecurityPurposeId(searchParams, sb, queryStrings, params);
			searchParamsReceivedDate(searchParams, sb, queryStrings, params);
			searchParamsCurrentAmount(searchParams, displayAvailableAmount, sb, queryStrings, params);
		}

		sb.append(") " + "SELECT " + "S.ID AS securityNumber, " + "S.INSTRUMENT_NUMBER AS instrumentNumber, "
				+ "S.DTA_ACCOUNT_NUMBER AS dtaAccountNumber, "
				+ "CAST(REPLACE(VB.CRM_CONTACT_ID, 'B', '') AS int) AS vbNumber, "
				+ "VB.COMPANY_NAME AS vbName, "
				+ "CASE WHEN ((SELECT COUNT(*) FROM VB_POOL WHERE POOL_ID = S.POOL_ID AND VB_DELETE_REASON_ID IS NULL) > 1) THEN 'Yes' ELSE 'No' END AS multiVb, "
				+ "CASE WHEN (S.JOINT_BOND = 0) THEN 'No' ELSE 'Yes' END AS jba, "
				+ "ST.DESCRIPTION AS securityTypeName, " + "SP.DESCRIPTION AS securityPurposeName, " + "IT.NAME AS identityTypeName, ");

		if (displayAvailableAmount) {
			sb.append("CASE WHEN (S.SECURITY_TYPE_ID = 1) THEN " +
					"S.CURRENT_AMOUNT - (SELECT COALESCE(SUM(R.PRINCIPAL_AMOUNT), 0) FROM RELEASE R WHERE R.SECURITY_ID = S.ID AND R.STATUS = 'PENDING') " +
					"ELSE S.CURRENT_AMOUNT END AS currentAmount, ");
			sb.append("CASE WHEN S.SECURITY_TYPE_ID != 1 OR S.UNPOST_DATE IS NOT NULL THEN 0 " +
					"ELSE CASE WHEN S.CURRENT_AMOUNT = 0 THEN 0 ELSE " +
					"(SELECT COALESCE(SUM(I.INTEREST_AMOUNT), 0) FROM INTEREST I WHERE I.SECURITY_ID = S.ID)" +
					"- (SELECT COALESCE(SUM(R.INTEREST_AMOUNT), 0) FROM RELEASE R WHERE R.SECURITY_ID = S.ID AND R.STATUS IN ('PENDING', 'VOUCHER_CREATED', 'SENT', 'COMPLETED')) " +
					"END END AS currentInterestAmount, ");

		} else {
			sb.append("S.CURRENT_AMOUNT AS currentAmount, ");
			sb.append("CASE WHEN S.SECURITY_TYPE_ID != 1 OR S.UNPOST_DATE IS NOT NULL THEN 0 " +
					"ELSE CASE WHEN S.CURRENT_AMOUNT = 0 THEN 0 ELSE " +
					"(SELECT COALESCE(SUM(I.INTEREST_AMOUNT), 0) FROM INTEREST I WHERE I.SECURITY_ID = S.ID)" +
					"- (SELECT COALESCE(SUM(R.INTEREST_AMOUNT), 0) FROM RELEASE R WHERE R.SECURITY_ID = S.ID AND R.STATUS IN ('VOUCHER_CREATED', 'SENT', 'COMPLETED')) " +
					"END END AS currentInterestAmount, ");
		}

		sb.append("S.ORIGINAL_AMOUNT AS originalAmount, "
				+ "S.POOL_ID AS poolId, " + "S.FINANCIAL_INSTITUTION_MAAPOA_ID AS maaPoaId, "
				+ "S.RECEIVED_DATE AS receivedDate, "
				+ "(SELECT STUFF((SELECT ',' + EP2.ENROLMENT_NUMBER + ':' + ISNULL(CONVERT(varchar, ENR2.WARRANTY_START_DATE, 23), '') FROM ENROLMENT_POOL EP2 INNER JOIN ENROLMENT ENR2 ON EP2.ENROLMENT_NUMBER = ENR2.ENROLMENT_NUMBER WHERE EP1.POOL_ID= EP2.POOL_ID FOR XML PATH(''), TYPE).value('.','varchar(max)'),1,1, '') AS ENROLMENT_NUMBERS "
				+ "FROM ENROLMENT_POOL EP1 " + "WHERE EP1.POOL_ID = S.POOL_ID " + "GROUP BY EP1.POOL_ID  "
				+ ") AS enrolmentNumbers, " + "SS.NAME AS dtaStatus ");

		if (!VbsUtil.isNullorEmpty(searchParams.getEnrolmentNumber())) {
			sb.append("FROM RESULTS RES, SECURITY S ");
		} else {
			sb.append("FROM MAIN_RESULTS RES, SECURITY S ");
		}
		sb.append("JOIN SECURITY_TYPE ST ON ST.ID = S.SECURITY_TYPE_ID "
				+ "JOIN SECURITY_PURPOSE SP ON SP.ID = S.SECURITY_PURPOSE_ID "
				+ "LEFT OUTER JOIN IDENTITY_TYPE IT ON IT.ID = S.IDENTITY_TYPE_ID "
				+ "LEFT OUTER JOIN CONTACT VB ON VB.ID = S.PRIMARY_VB_ID "
				+ "LEFT OUTER JOIN SECURITY_STATUS SS ON SS.ID = S.STATUS_ID " + "WHERE S.ID = RES.ID "
				+ "ORDER BY S.ID DESC");

		Query query = vbsEm.createNativeQuery(sb.toString(), Tuple.class) //.setMaxResults(maxResults)
				.setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false);
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}


		List<Tuple> res = query.getResultList();

		long diff = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli() - start.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
		LoggerUtil.logDebug(SecurityDAOImpl.class, diff + "ms to return results");
		ret.setSecurities(res.stream().map(this::transform).collect(Collectors.toList()));

		if (!queryStrings.isEmpty()) {
			ret.setQueryString(String.join(" AND ", queryStrings));
		}
		return ret;
	}

	private SecuritySearchResultDTO transform(Tuple t){
		SecuritySearchResultDTO dto = new SecuritySearchResultDTO();
		dto.setInstrumentNumber(t.get("instrumentNumber", String.class));
		dto.setDtaStatus(t.get("dtaStatus", String.class));
		dto.setDtaAccountNumber(t.get("dtaAccountNumber", String.class));
		dto.setVbNumber(t.get("vbNumber", Integer.class));
		dto.setVbName(t.get("vbName", String.class));
		dto.setMultiVb(t.get("multiVb", String.class));
		dto.setJba(t.get("jba", String.class));
		dto.setSecurityTypeName(t.get("securityTypeName", String.class));
		dto.setSecurityPurposeName(t.get("securityPurposeName", String.class));
		dto.setIdentityTypeName(t.get("identityTypeName", String.class));
		dto.setCurrentAmount(t.get("currentAmount", BigDecimal.class));
		dto.setOriginalAmount(t.get("originalAmount", BigDecimal.class));
		dto.setCurrentInterestAmount(t.get("currentInterestAmount", BigDecimal.class));
		dto.setSecurityNumber(t.get("securityNumber", Integer.class));
		dto.setPoolId(t.get("poolId", Integer.class));
		dto.setMaaPoaId(t.get("maaPoaId", Integer.class));
		dto.setReceivedDate(t.get("receivedDate", Timestamp.class).toLocalDateTime());
		dto.setEnrolments(transformEnrolmentNumbersToEnrolments(t.get("enrolmentNumbers", String.class)));
		return dto;
	}
	
	private List<SearchResultEnrolmentDTO> transformEnrolmentNumbersToEnrolments(String enrolmentNumbers) {
		List<SearchResultEnrolmentDTO> searchResultEnrolmentDTOList = new ArrayList<>();
		if (enrolmentNumbers == null) {
			return searchResultEnrolmentDTOList;
		}
		
		String[] searchResults = enrolmentNumbers.split(",");
		for (String result : searchResults) {
			searchResultEnrolmentDTOList.add(transformEnrolmentNumberToSearchResult(result));
		}
		
		return searchResultEnrolmentDTOList;
	}
	
	private SearchResultEnrolmentDTO transformEnrolmentNumberToSearchResult(String enrolmentNumber) {
		SearchResultEnrolmentDTO searchResultEnrolmentDTO = new SearchResultEnrolmentDTO();
		searchResultEnrolmentDTO.setEnrolmentNumber(transformSearchResultToEnrolmentNumber(enrolmentNumber));
		searchResultEnrolmentDTO.setDate(transformSearchResultToDate(enrolmentNumber));
		return searchResultEnrolmentDTO;
	}

	private String transformSearchResultToEnrolmentNumber(String enrolmentNumber) {
		String[] enrolmentParsed = transformEnrolmentNumberToArray(enrolmentNumber);
		if (enrolmentParsed != null && enrolmentParsed.length > 0) {
			return enrolmentParsed[0];
		}
		return "";
	}
	
	private String transformSearchResultToDate(String enrolmentNumber) {
		String[] enrolmentParsed = transformEnrolmentNumberToArray(enrolmentNumber);
		if (enrolmentParsed != null && enrolmentParsed.length > 1) {
			return enrolmentParsed[1];
		}
		return "";
	}
	
	private String[] transformEnrolmentNumberToArray(String enrolmentNumber) {
		if (enrolmentNumber == null || enrolmentNumber.equalsIgnoreCase("")) {
			return null;
		}
		
		if (enrolmentNumber.indexOf(':') < 0) {
			// Invalid string - throw exception or just return null?
			return null;
		}
		
		return enrolmentNumber.split(":");
	}

	private SecurityTipInfoDTO transformToTipInfoDTO(Tuple t) {
		SecurityTipInfoDTO dto = new SecurityTipInfoDTO();

		dto.setVbNumber(t.get("vbNumber", String.class));
		dto.setSecurityNumber(t.get("securityNumber", Integer.class));
		dto.setInstrumentNumber(t.get("instrumentNumber", String.class));
		dto.setCurrentAmount(t.get("currentAmount", BigDecimal.class));
		dto.setEnrolmentNumber(t.get("enrolmentNumber", String.class));
		dto.setSecurityType(t.get("securityType", String.class));
		int multiVB = t.get("multiVB", Integer.class);
		dto.setMultiVb(multiVB > 0);

		return dto;
	}


	private void searchParamsCurrentAmount(SecuritySearchParametersDTO searchParams, boolean displayAvailableAmount,
			StringBuilder sb, List<String> queryStrings, Map<String, Object> params) {
		String sql = "";

		String currentAmountSql = (displayAvailableAmount)
				? "(CASE WHEN (S.SECURITY_TYPE_ID = 1 AND S.CURRENT_AMOUNT > 0) THEN (SELECT S.CURRENT_AMOUNT - COALESCE(SUM(R.PRINCIPAL_AMOUNT), 0) FROM RELEASE R WHERE "
						+ "R.SECURITY_ID = S.ID AND R.STATUS = 'PENDING') ELSE S.CURRENT_AMOUNT END)"
				: "S.CURRENT_AMOUNT";

		if (searchParams.getCurrentAmount() != null) {
			if (searchParams.getCurrentAmountType().equals(SearchTypeEnum.BETWEEN)
					&& searchParams.getCurrentAmountTo() != null) {
				sql += " AND " + currentAmountSql + " BETWEEN :currentAmountFrom AND :currentAmountTo ";
				params.put("currentAmountFrom", searchParams.getCurrentAmount());
				params.put("currentAmountTo", searchParams.getCurrentAmountTo());
				queryStrings.add("Current Amount between " + searchParams.getCurrentAmount() + " & "
						+ searchParams.getCurrentAmountTo());
			} else {
				if (searchParams.getCurrentAmountType().equals(SearchTypeEnum.EQUALS)) {
					sql += " AND " + currentAmountSql + " = :currentAmount ";
					queryStrings.add("Current Amount = " + searchParams.getCurrentAmount());
				} else if (searchParams.getCurrentAmountType().equals(SearchTypeEnum.GREATER_THAN)) {
					sql += " AND " + currentAmountSql + " > :currentAmount ";
					queryStrings.add("Current Amount > " + searchParams.getCurrentAmount());
				} else if (searchParams.getCurrentAmountType().equals(SearchTypeEnum.LESS_THAN)) {
					sql += " AND " + currentAmountSql + " < :currentAmount ";
					queryStrings.add("Current Amount < " + searchParams.getCurrentAmount());
				} else if (searchParams.getCurrentAmountType().equals(SearchTypeEnum.EQUALS_GREATER_THAN)) {
					sql += " AND " + currentAmountSql + " >= :currentAmount ";
					queryStrings.add("Current Amount >= " + searchParams.getCurrentAmount());
				} else if (searchParams.getCurrentAmountType().equals(SearchTypeEnum.EQUALS_LESS_THAN)) {
					sql += " AND " + currentAmountSql + " <= :currentAmount ";
					queryStrings.add("Current Amount <= " + searchParams.getCurrentAmount());
				}
				params.put("currentAmount", searchParams.getCurrentAmount());
			}
		}
		sb.append(sql);
	}

	private void searchParamsReceivedDate(SecuritySearchParametersDTO searchParams, StringBuilder sb,
			List<String> queryStrings, Map<String, Object> params) {
		String sql = "";
		if (searchParams.getReceivedDate() != null) {
			if (searchParams.getReceivedDateType().equals(SearchTypeEnum.BETWEEN)
					&& searchParams.getReceivedDateBetweenTo() != null) {
				sql += " AND S.RECEIVED_DATE BETWEEN :receivedDateFrom AND :receivedDateTo ";
				params.put("receivedDateFrom", searchParams.getReceivedDate());
				params.put("receivedDateTo", searchParams.getReceivedDateBetweenTo());
				queryStrings
						.add("Received Date between " + DateUtil.getDateFormattedShort(searchParams.getReceivedDate())
								+ " & " + DateUtil.getDateFormattedShort(searchParams.getReceivedDateBetweenTo()));
			} else {
				if (searchParams.getReceivedDateType().equals(SearchTypeEnum.EQUALS)) {
					sql += " AND S.RECEIVED_DATE = :receivedDate ";
					queryStrings
							.add("Received Date = " + DateUtil.getDateFormattedShort(searchParams.getReceivedDate()));
				} else if (searchParams.getReceivedDateType().equals(SearchTypeEnum.GREATER_THAN)) {
					sql += " AND S.RECEIVED_DATE > :receivedDate ";
					queryStrings
							.add("Received Date > " + DateUtil.getDateFormattedShort(searchParams.getReceivedDate()));
				} else if (searchParams.getReceivedDateType().equals(SearchTypeEnum.LESS_THAN)) {
					sql += " AND S.RECEIVED_DATE < :receivedDate ";
					queryStrings
							.add("Received Date < " + DateUtil.getDateFormattedShort(searchParams.getReceivedDate()));
				} else if (searchParams.getReceivedDateType().equals(SearchTypeEnum.EQUALS_GREATER_THAN)) {
					sql += " AND S.RECEIVED_DATE >= :receivedDate ";
					queryStrings
							.add("Received Date >= " + DateUtil.getDateFormattedShort(searchParams.getReceivedDate()));
				} else if (searchParams.getReceivedDateType().equals(SearchTypeEnum.EQUALS_LESS_THAN)) {
					sql += " AND S.RECEIVED_DATE <= :receivedDate ";
					queryStrings
							.add("Received Date <= " + DateUtil.getDateFormattedShort(searchParams.getReceivedDate()));
				}

				params.put("receivedDate", searchParams.getReceivedDate());
			}
		}
		sb.append(sql);
	}

	private void searchParamsSecurityPurposeId(SecuritySearchParametersDTO searchParams, StringBuilder sb,
			List<String> queryStrings, Map<String, Object> params) {
		String sql = "";
		if (searchParams.getSecurityPurposeId() != null) {
			sql += " AND S.SECURITY_PURPOSE_ID = :securityPurposeId";
			params.put("securityPurposeId", searchParams.getSecurityPurposeId());
			SecurityPurposeEntity purpose = genericDAO.findEntityById(SecurityPurposeEntity.class,
					searchParams.getSecurityPurposeId());
			queryStrings.add("Security Purpose = " + purpose.getDescription());
		}
		sb.append(sql);
	}

	private void searchParamsSecurityTypeId(SecuritySearchParametersDTO searchParams, StringBuilder sb,
			List<String> queryStrings, Map<String, Object> params) {
		String sql = "";
		if (searchParams.getSecurityTypeId() != null) {
			sql += " AND S.SECURITY_TYPE_ID = :securityTypeId";
			params.put("securityTypeId", searchParams.getSecurityTypeId());
			SecurityTypeEntity type = genericDAO.findEntityById(SecurityTypeEntity.class,
					searchParams.getSecurityTypeId());
			queryStrings.add("Security Type = " + type.getDescription());
		}
		sb.append(sql);
	}

	private void searchParamsFinancialInstitutionTypeId(SecuritySearchParametersDTO searchParams, StringBuilder sb,
			List<String> queryStrings, Map<String, Object> params) {
		String sql = "";
		if (searchParams.getFinancialInstitutionTypeId() != null) {
			sql += "  AND EXISTS(SELECT FI.FINANCIAL_INSTITUTION_TYPE_ID FROM FINANCIAL_INSTITUTION FI "
					+ " JOIN BRANCH B ON FI.ID = B.FINANCIAL_INSTITUTION_ID " + "WHERE B.ID = S.BRANCH_ID "
					+ "AND FI.FINANCIAL_INSTITUTION_TYPE_ID = :financialInstitutionTypeId )";
			params.put("financialInstitutionTypeId", searchParams.getFinancialInstitutionTypeId());
			FinancialInstitutionTypeEntity type = genericDAO.findEntityById(FinancialInstitutionTypeEntity.class,
					searchParams.getFinancialInstitutionTypeId());
			queryStrings.add("Financial Institution Type = " + type.getName());
		}
		sb.append(sql);
	}

	private void searchParamsPoolId(SecuritySearchParametersDTO searchParams, StringBuilder sb,
			List<String> queryStrings, Map<String, Object> params) {
		String sql = "";
		if (searchParams.getPoolId() != null) {
			sql += " AND S.POOL_ID = :poolId";
			params.put("poolId", searchParams.getPoolId());
			queryStrings.add("Pool ID = " + searchParams.getPoolId());
		}
		sb.append(sql);
	}

	private void searchParamsFinancialInstitutionIdBranchId(SecuritySearchParametersDTO searchParams, StringBuilder sb,
			List<String> queryStrings, Map<String, Object> params) {
		String sql = "";
		if (searchParams.getFinancialInstitutionId() != null || searchParams.getBranchId() != null) {
			sql += " AND ( 1 = 0 OR ";
		}

		if (searchParams.getFinancialInstitutionId() != null) {
			Long type = financialInstitutionDAO.getFinancialInstitutionTypeId(searchParams.getFinancialInstitutionId());
			if (type != null && type == VbsConstants.FINANCIAL_INSTITUTION_TYPE_SURETY_COMPANIES) {
				sql += " S.FINANCIAL_INSTITUTION_MAAPOA_ID IN (SELECT FINANCIAL_INSTITUTION_MAAPOA_ID FROM FINANCIAL_INSTITUTION_MAAPOA_INSTITUTION WHERE FINANCIAL_INSTITUTION_ID = :financialInstitutionId) OR ";
				params.put("financialInstitutionId", searchParams.getFinancialInstitutionId());
				queryStrings.add("Financial Institution ID = " + searchParams.getFinancialInstitutionId());
			}

			if (searchParams.getBranchId() == null) {
				sql += " S.BRANCH_ID IN (SELECT ID FROM BRANCH WHERE FINANCIAL_INSTITUTION_ID = :financialInstitutionId) ) ";
				if (!params.containsKey("financialInstitutionId")) {
					params.put("financialInstitutionId", searchParams.getFinancialInstitutionId());
					queryStrings.add("Financial Institution ID = " + searchParams.getFinancialInstitutionId());
				}
			}
		}
		if (searchParams.getBranchId() != null) {
			sql += " S.BRANCH_ID = :branchId ) ";
			params.put("branchId", searchParams.getBranchId());
			queryStrings.add("Branch ID = " + searchParams.getBranchId());
		}
		sb.append(sql);
	}

	private void searchParamsEnrolmentNumber(SecuritySearchParametersDTO searchParams, StringBuilder sb,
			List<String> queryStrings, Map<String, Object> params) {
		String sql = "";
		if (!VbsUtil.isNullorEmpty(searchParams.getEnrolmentNumber())) {
			sql += " AND ((SELECT COUNT(ID) FROM ENROLMENT_POOL WHERE POOL_ID = S.POOL_ID AND ENROLMENT_NUMBER = :enrolmentNumber) > 0) ";
			params.put("enrolmentNumber", searchParams.getEnrolmentNumber());
			queryStrings.add("Enrolment Number = " + searchParams.getEnrolmentNumber());
		}
		sb.append(sql);
	}

	private void searchParamsVbNumber(SecuritySearchParametersDTO searchParams, StringBuilder sb,
			List<String> queryStrings, Map<String, Object> params) {
		String sql = "";
		if (!VbsUtil.isNullorEmpty(searchParams.getVbNumber())) {
			if (searchParams.getVbNumberType().equals(SearchTypeEnum.EQUALS)
					|| searchParams.getVbNumberType().equals(SearchTypeEnum.BEGIN_WITH)
					|| searchParams.getVbNumberType().equals(SearchTypeEnum.IN)
					|| searchParams.getVbNumberType().equals(SearchTypeEnum.CONTAINS)) {
				if (searchParams.getVbNumberType().equals(SearchTypeEnum.EQUALS)) {
					sql += " AND EXISTS(SELECT VBP.ID FROM VB_POOL VBP JOIN CONTACT VBC ON VBC.ID = VBP.VB_CONTACT_ID WHERE VBC.CRM_CONTACT_ID = :vbNumber AND VBP.POOL_ID = S.POOL_ID AND VBP.VB_DELETE_REASON_ID IS NULL) ";
					params.put(VB_NUMBER, searchParams.getVbNumber());
					queryStrings.add("V/B# = " + searchParams.getVbNumber());
				} else if (searchParams.getVbNumberType().equals(SearchTypeEnum.BEGIN_WITH)) {
					sql += " AND EXISTS(SELECT VBP.ID FROM VB_POOL VBP JOIN CONTACT VBC ON VBC.ID = VBP.VB_CONTACT_ID WHERE VBC.CRM_CONTACT_ID LIKE :vbNumber AND VBP.POOL_ID = S.POOL_ID AND VBP.VB_DELETE_REASON_ID IS NULL) ";
					queryStrings.add("V/B# begins with " + searchParams.getVbNumber());
					params.put(VB_NUMBER, searchParams.getVbNumber() + "%");
				} else if (searchParams.getVbNumberType().equals(SearchTypeEnum.IN)) {
					List<String> vbs = Arrays.asList(searchParams.getVbNumber().split(","));
					sql += " AND EXISTS(SELECT VBP.ID FROM VB_POOL VBP JOIN CONTACT VBC ON VBC.ID = VBP.VB_CONTACT_ID WHERE VBC.CRM_CONTACT_ID IN :vbNumbers AND VBP.POOL_ID = S.POOL_ID AND VBP.VB_DELETE_REASON_ID IS NULL) ";
					queryStrings.add("V/B# in (" + searchParams.getVbNumber() + ")");
					params.put("vbNumbers", vbs);
				} else if (searchParams.getVbNumberType().equals(SearchTypeEnum.CONTAINS)) {
					sql += " AND EXISTS(SELECT VBP.ID FROM VB_POOL VBP JOIN CONTACT VBC ON VBC.ID = VBP.VB_CONTACT_ID WHERE VBC.CRM_CONTACT_ID LIKE :vbNumber AND VBP.POOL_ID = S.POOL_ID AND VBP.VB_DELETE_REASON_ID IS NULL) ";
					queryStrings.add("V/B# contains " + searchParams.getVbNumber());
					params.put(VB_NUMBER, "%" + searchParams.getVbNumber() + "%");
				}
			} else {
				Long vbNum = Long.parseLong(searchParams.getVbNumber().toUpperCase().replace("B", ""));
				if (searchParams.getVbNumberType().equals(SearchTypeEnum.GREATER_THAN)) {
					sql += " AND EXISTS(SELECT VBP.ID FROM VB_POOL VBP JOIN CONTACT VBC ON VBC.ID = VBP.VB_CONTACT_ID WHERE CAST(REPLACE(VBC.CRM_CONTACT_ID, 'B', '') as INT) > :vbNumber AND VBP.POOL_ID = S.POOL_ID AND VBP.VB_DELETE_REASON_ID IS NULL) ";
					queryStrings.add("V/B# > " + searchParams.getVbNumber());
				} else if (searchParams.getVbNumberType().equals(SearchTypeEnum.LESS_THAN)) {
					sql += " AND EXISTS(SELECT VBP.ID FROM VB_POOL VBP JOIN CONTACT VBC ON VBC.ID = VBP.VB_CONTACT_ID WHERE CAST(REPLACE(VBC.CRM_CONTACT_ID, 'B', '') as INT) < :vbNumber AND VBP.POOL_ID = S.POOL_ID AND VBP.VB_DELETE_REASON_ID IS NULL) ";
					queryStrings.add("V/B# < " + searchParams.getVbNumber());
				} else if (searchParams.getVbNumberType().equals(SearchTypeEnum.EQUALS_GREATER_THAN)) {
					sql += " AND EXISTS(SELECT VBP.ID FROM VB_POOL VBP JOIN CONTACT VBC ON VBC.ID = VBP.VB_CONTACT_ID WHERE CAST(REPLACE(VBC.CRM_CONTACT_ID, 'B', '') as INT) >= :vbNumber AND VBP.POOL_ID = S.POOL_ID AND VBP.VB_DELETE_REASON_ID IS NULL) ";
					queryStrings.add("V/B# >= " + searchParams.getVbNumber());
				} else if (searchParams.getVbNumberType().equals(SearchTypeEnum.EQUALS_LESS_THAN)) {
					sql += " AND EXISTS(SELECT VBP.ID FROM VB_POOL VBP JOIN CONTACT VBC ON VBC.ID = VBP.VB_CONTACT_ID WHERE CAST(REPLACE(VBC.CRM_CONTACT_ID, 'B', '') as INT) <= :vbNumber AND VBP.POOL_ID = S.POOL_ID AND VBP.VB_DELETE_REASON_ID IS NULL) ";
					queryStrings.add("V/B# <= " + searchParams.getVbNumber());
				}
				params.put(VB_NUMBER, vbNum);
			}
		}
		sb.append(sql);
	}

	private void searchParamsEscrowAgentContactId(SecuritySearchParametersDTO searchParams, StringBuilder sb,
			List<String> queryStrings, Map<String, Object> params) {
		String sql = "";
		if (!VbsUtil.isNullorEmpty(searchParams.getEscrowAgentContactId())
				&& searchParams.getEscrowAgentContactIdType() != null) {
			if (searchParams.getEscrowAgentContactIdType().equals(SearchTypeEnum.EQUALS)) {
				sql += " AND EXISTS(SELECT C.CRM_CONTACT_ID FROM CONTACT C JOIN CONTACT_SECURITY CS ON C.ID = CS.CONTACT_ID AND  CS.CONTACT_SECURITY_ROLE_ID = 1 WHERE C.CRM_CONTACT_ID = :escrowAgentCrmContactId AND CS.SECURITY_ID = S.ID) ";
				params.put(ESCROW_AGENT_CRM_CONTACT_ID, searchParams.getEscrowAgentContactId());
				queryStrings.add(ESCROW_AGENT_QUERY_STRING + searchParams.getEscrowAgentContactId());
			} else if (searchParams.getEscrowAgentContactIdType().equals(SearchTypeEnum.LESS_THAN)) {
				sql += " AND EXISTS(SELECT C.CRM_CONTACT_ID FROM CONTACT C JOIN CONTACT_SECURITY CS ON C.ID = CS.CONTACT_ID AND  CS.CONTACT_SECURITY_ROLE_ID = 1 WHERE C.CRM_CONTACT_ID NOT LIKE 'B%' AND CAST(C.CRM_CONTACT_ID as INT) < :escrowAgentCrmContactId AND CS.SECURITY_ID = S.ID) ";
				params.put(ESCROW_AGENT_CRM_CONTACT_ID, Long.parseLong(searchParams.getEscrowAgentContactId()));
				queryStrings.add(ESCROW_AGENT_QUERY_STRING + searchParams.getEscrowAgentContactId());
			} else if (searchParams.getEscrowAgentContactIdType().equals(SearchTypeEnum.GREATER_THAN)) {
				sql += " AND EXISTS(SELECT C.CRM_CONTACT_ID FROM CONTACT C JOIN CONTACT_SECURITY CS ON C.ID = CS.CONTACT_ID AND  CS.CONTACT_SECURITY_ROLE_ID = 1 WHERE C.CRM_CONTACT_ID NOT LIKE 'B%' AND CAST(C.CRM_CONTACT_ID as INT) > :escrowAgentCrmContactId AND CS.SECURITY_ID = S.ID) ";
				params.put(ESCROW_AGENT_CRM_CONTACT_ID, Long.parseLong(searchParams.getEscrowAgentContactId()));
				queryStrings.add(ESCROW_AGENT_QUERY_STRING + searchParams.getEscrowAgentContactId());
			} else if (searchParams.getEscrowAgentContactIdTo() != null
					&& searchParams.getEscrowAgentContactIdType().equals(SearchTypeEnum.BETWEEN)) {
				sql += " AND EXISTS(SELECT C.CRM_CONTACT_ID FROM CONTACT C JOIN CONTACT_SECURITY CS ON C.ID = CS.CONTACT_ID AND  CS.CONTACT_SECURITY_ROLE_ID = 1 WHERE C.CRM_CONTACT_ID NOT LIKE 'B%' AND CAST(C.CRM_CONTACT_ID as INT) BETWEEN :escrowAgentCrmContactId AND :escrowAgentCrmContactIdTo AND CS.SECURITY_ID = S.ID) ";
				params.put(ESCROW_AGENT_CRM_CONTACT_ID, searchParams.getEscrowAgentContactId());
				params.put("escrowAgentCrmContactIdTo", searchParams.getEscrowAgentContactIdTo());
				queryStrings.add("Escrow Agent " + searchParams.getInstrumentNumber() + " & "
						+ searchParams.getInstrumentNumberTo());
			} else if (searchParams.getEscrowAgentContactIdType().equals(SearchTypeEnum.BEGIN_WITH)) {
				sql += " AND EXISTS(SELECT C.CRM_CONTACT_ID FROM CONTACT C JOIN CONTACT_SECURITY CS ON C.ID = CS.CONTACT_ID AND  CS.CONTACT_SECURITY_ROLE_ID = 1 WHERE C.CRM_CONTACT_ID LIKE :escrowAgentCrmContactId AND CS.SECURITY_ID = S.ID) ";
				params.put(ESCROW_AGENT_CRM_CONTACT_ID, searchParams.getEscrowAgentContactId() + "%");
				queryStrings.add(ESCROW_AGENT_QUERY_STRING + searchParams.getEscrowAgentContactId());
			} else if (searchParams.getEscrowAgentContactIdType().equals(SearchTypeEnum.EQUALS_GREATER_THAN)) {
				sql += " AND EXISTS(SELECT C.CRM_CONTACT_ID FROM CONTACT C JOIN CONTACT_SECURITY CS ON C.ID = CS.CONTACT_ID AND  CS.CONTACT_SECURITY_ROLE_ID = 1 WHERE C.CRM_CONTACT_ID NOT LIKE 'B%' AND CAST(C.CRM_CONTACT_ID as INT) >= :escrowAgentCrmContactId AND CS.SECURITY_ID = S.ID) ";
				params.put(ESCROW_AGENT_CRM_CONTACT_ID, searchParams.getEscrowAgentContactId());
				queryStrings.add(ESCROW_AGENT_ID_QUERY_STRING + searchParams.getEscrowAgentContactId());
			} else if (searchParams.getEscrowAgentContactIdType().equals(SearchTypeEnum.EQUALS_LESS_THAN)) {
				sql += " AND EXISTS(SELECT C.CRM_CONTACT_ID FROM CONTACT C JOIN CONTACT_SECURITY CS ON C.ID = CS.CONTACT_ID AND  CS.CONTACT_SECURITY_ROLE_ID = 1 WHERE C.CRM_CONTACT_ID NOT LIKE 'B%' AND CAST(C.CRM_CONTACT_ID as INT) <= :escrowAgentCrmContactId AND CS.SECURITY_ID = S.ID ) ";
				params.put(ESCROW_AGENT_CRM_CONTACT_ID, searchParams.getEscrowAgentContactId());
				queryStrings.add(ESCROW_AGENT_ID_QUERY_STRING + searchParams.getEscrowAgentContactId());
			} else if (searchParams.getEscrowAgentContactIdType().equals(SearchTypeEnum.IN)) {
				List<String> numList = Arrays.asList(searchParams.getEscrowAgentContactId().split(","));
				sql += " AND EXISTS(SELECT C.CRM_CONTACT_ID FROM CONTACT C JOIN CONTACT_SECURITY CS ON C.ID = CS.CONTACT_ID AND  CS.CONTACT_SECURITY_ROLE_ID = 1 WHERE C.CRM_CONTACT_ID IN :escrowAgentCrmContactId AND CS.SECURITY_ID = S.ID ) ";
				params.put(ESCROW_AGENT_CRM_CONTACT_ID, numList);
				queryStrings.add("Escrow Agent Id in (" + searchParams.getEscrowAgentContactId() + ")");
			} else if (searchParams.getEscrowAgentContactIdType().equals(SearchTypeEnum.CONTAINS)) {
				sql += " AND EXISTS(SELECT C.CRM_CONTACT_ID FROM CONTACT C JOIN CONTACT_SECURITY CS ON C.ID = CS.CONTACT_ID AND  CS.CONTACT_SECURITY_ROLE_ID = 1 WHERE C.CRM_CONTACT_ID LIKE :escrowAgentCrmContactId AND CS.SECURITY_ID = S.ID) ";
				params.put(ESCROW_AGENT_CRM_CONTACT_ID, searchParams.getEscrowAgentContactId());
				queryStrings.add(ESCROW_AGENT_ID_QUERY_STRING + "%" + searchParams.getEscrowAgentContactId() + "%");
			}
		}
		sb.append(sql);
	}

	private void searchParamsInstrumentNumber(SecuritySearchParametersDTO searchParams, StringBuilder sb,
			List<String> queryStrings, Map<String, Object> params) {
		String sql = "";
		if (!VbsUtil.isNullorEmpty(searchParams.getInstrumentNumber())
				&& searchParams.getInstrumentNumberSearchType() != null) {
			if (searchParams.getInstrumentNumberSearchType().equals(SearchTypeEnum.EQUALS)) {
				sql += " AND S.INSTRUMENT_NUMBER = :instrumentNumber ";
				params.put(INSTRUMENT_NUMBER, searchParams.getInstrumentNumber());
				queryStrings.add("Instrument Number = " + searchParams.getInstrumentNumber());
			} else if (searchParams.getInstrumentNumberSearchType().equals(SearchTypeEnum.LESS_THAN)) {
				sql += " AND S.INSTRUMENT_NUMBER < :instrumentNumber ";
				params.put(INSTRUMENT_NUMBER, searchParams.getInstrumentNumber());
				queryStrings.add("Instrument Number < " + searchParams.getInstrumentNumber());
			} else if (searchParams.getInstrumentNumberSearchType().equals(SearchTypeEnum.GREATER_THAN)) {
				sql += " AND S.INSTRUMENT_NUMBER > :instrumentNumber ";
				params.put(INSTRUMENT_NUMBER, searchParams.getInstrumentNumber());
				queryStrings.add("Instrument Number > " + searchParams.getInstrumentNumber());
			} else if (searchParams.getInstrumentNumberTo() != null
					&& searchParams.getInstrumentNumberSearchType().equals(SearchTypeEnum.BETWEEN)) {
				sql += " AND S.INSTRUMENT_NUMBER BETWEEN :instrumentNumberFrom AND :instrumentNumberTo ";
				params.put("instrumentNumberFrom", searchParams.getInstrumentNumber());
				params.put("instrumentNumberTo", searchParams.getInstrumentNumberTo());
				queryStrings.add("Security Number between " + searchParams.getInstrumentNumber() + " & "
						+ searchParams.getInstrumentNumberTo());
			} else if (searchParams.getInstrumentNumberSearchType().equals(SearchTypeEnum.BEGIN_WITH)) {
				sql += " AND S.INSTRUMENT_NUMBER LIKE :instrumentNumber ";
				params.put(INSTRUMENT_NUMBER, searchParams.getInstrumentNumber() + "%");
				queryStrings.add("Instrument Number begins with " + searchParams.getInstrumentNumber());
			} else if (searchParams.getInstrumentNumberSearchType().equals(SearchTypeEnum.EQUALS_GREATER_THAN)) {
				sql += " AND S.INSTRUMENT_NUMBER >= :instrumentNumber ";
				params.put(INSTRUMENT_NUMBER, searchParams.getInstrumentNumber());
				queryStrings.add("Instrument Number >= " + searchParams.getInstrumentNumber());
			} else if (searchParams.getInstrumentNumberSearchType().equals(SearchTypeEnum.EQUALS_LESS_THAN)) {
				sql += " AND S.INSTRUMENT_NUMBER <= :instrumentNumber ";
				params.put(INSTRUMENT_NUMBER, searchParams.getInstrumentNumber());
				queryStrings.add("Instrument Number <= " + searchParams.getInstrumentNumber());
			} else if (searchParams.getInstrumentNumberSearchType().equals(SearchTypeEnum.IN)) {
				List<String> numList = Arrays.asList(searchParams.getInstrumentNumber().split(","));
				sql += " AND S.INSTRUMENT_NUMBER IN :instrumentNumber ";
				params.put(INSTRUMENT_NUMBER, numList);
				queryStrings.add("Instrument Number in (" + searchParams.getInstrumentNumber() + ")");
			} else if (searchParams.getInstrumentNumberSearchType().equals(SearchTypeEnum.CONTAINS)) {
				sql += " AND S.INSTRUMENT_NUMBER LIKE :instrumentNumber ";
				params.put(INSTRUMENT_NUMBER, "%" + searchParams.getInstrumentNumber() + "%");
				queryStrings.add("Instrument Number contains " + searchParams.getInstrumentNumber());
			}
		}
		sb.append(sql);
	}

	private void searchParamsSecurityNumber(SecuritySearchParametersDTO searchParams, StringBuilder sb,
			List<String> queryStrings, Map<String, Object> params) {
		String sql = "";
		if (!VbsUtil.isNullorEmpty(searchParams.getSecurityNumber())
				&& searchParams.getSecurityNumberSearchType() != null) {
			if (searchParams.getSecurityNumberSearchType().equals(SearchTypeEnum.EQUALS)) {
				sql += " AND S.ID = :securityNumber ";
				params.put(SECURITY_NUMBER, Long.parseLong(searchParams.getSecurityNumber()));
				queryStrings.add("Security Number = " + searchParams.getSecurityNumber());
			} else if (searchParams.getSecurityNumberSearchType().equals(SearchTypeEnum.LESS_THAN)) {
				sql += " AND S.ID < :securityNumber ";
				params.put(SECURITY_NUMBER, Long.parseLong(searchParams.getSecurityNumber()));
				queryStrings.add("Security Number < " + searchParams.getSecurityNumber());
			} else if (searchParams.getSecurityNumberSearchType().equals(SearchTypeEnum.GREATER_THAN)) {
				sql += " AND S.ID > :securityNumber ";
				params.put(SECURITY_NUMBER, Long.parseLong(searchParams.getSecurityNumber()));
				queryStrings.add("Security Number > " + searchParams.getSecurityNumber());
			} else if (searchParams.getSecurityNumberTo() != null
					&& searchParams.getSecurityNumberSearchType().equals(SearchTypeEnum.BETWEEN)) {
				sql += " AND S.ID BETWEEN :securityNumberFrom AND :securityNumberTo ";
				params.put("securityNumberFrom", Long.parseLong(searchParams.getSecurityNumber()));
				params.put("securityNumberTo", Long.parseLong(searchParams.getSecurityNumberTo()));
				queryStrings.add("Security Number between " + searchParams.getSecurityNumber() + " & "
						+ searchParams.getSecurityNumberTo());
			} else if (searchParams.getSecurityNumberSearchType().equals(SearchTypeEnum.BEGIN_WITH)) {
				sql += " AND CONVERT(varchar(20), S.ID) LIKE :securityNumber ";
				params.put(SECURITY_NUMBER, searchParams.getSecurityNumber() + "%");
				queryStrings.add("Security Number begins with " + searchParams.getSecurityNumber());
			} else if (searchParams.getSecurityNumberSearchType().equals(SearchTypeEnum.CONTAINS)) {
				sql += " AND CONVERT(varchar(20), S.ID) LIKE :securityNumber ";
				params.put(SECURITY_NUMBER, "%" + searchParams.getSecurityNumber() + "%");
				queryStrings.add("Security Number contains " + searchParams.getSecurityNumber());
			} else if (searchParams.getSecurityNumberSearchType().equals(SearchTypeEnum.EQUALS_GREATER_THAN)) {
				sql += " AND S.ID >= :securityNumber ";
				params.put(SECURITY_NUMBER, searchParams.getSecurityNumber());
				queryStrings.add("Security Number >= " + searchParams.getSecurityNumber());
			} else if (searchParams.getSecurityNumberSearchType().equals(SearchTypeEnum.EQUALS_LESS_THAN)) {
				sql += " AND S.ID <= :securityNumber ";
				params.put(SECURITY_NUMBER, searchParams.getSecurityNumber());
				queryStrings.add("Security Number <= " + searchParams.getSecurityNumber());
			} else if (searchParams.getSecurityNumberSearchType().equals(SearchTypeEnum.IN)) {
				List<String> numList = Arrays.stream(searchParams.getSecurityNumber().split(",")).map(String::trim)
						.collect(Collectors.toList());
				List<Long> nums = numList.stream().map(Long::parseLong).collect(Collectors.toList());
				sql += " AND S.ID IN :securityNumber ";
				params.put(SECURITY_NUMBER, nums);
				queryStrings.add("Security Number in (" + searchParams.getSecurityNumber() + ")");
			}
		}
		sb.append(sql);
	}

	@Override
	public long getNumSecuritiesWithInstrNumVbNumber(String instrumentNumber, List<String> vbNumbers) {
		if (StringUtils.isEmpty(instrumentNumber) || vbNumbers == null || vbNumbers.isEmpty()) {
			return 0;
		}
		Query query = vbsEm.createNamedQuery(SecurityEntity.SELECT_NUMBER_OF_CASH_SECURITIES_BY_INSTR_NUM_VB);
		query.setParameter(1, instrumentNumber);
		query.setParameter(2, vbNumbers);
		return (long) query.getSingleResult();
	}

	@Override
	public List<Long> getSecurityIdsWithInstrumentNumberFinancialInst(String instrumentNumber, Long financialInstitutionId) {
		TypedQuery<Long> query = vbsEm.createNamedQuery(SecurityEntity.SELECT_SECURITY_ID_FOR_INSTR_NUM_FINANCIAL_INST, Long.class);
		query.setParameter(1, instrumentNumber.trim());
		query.setParameter(2, financialInstitutionId);
		return query.getResultList();
	}

	@Override
	public List<SecurityEntity> getSecuritiesWithMaaPoa(Long maaPoaId) {
		TypedQuery<SecurityEntity> query = vbsEm.createNamedQuery(SecurityEntity.SELECT_SECURITY_FOR_MAA_POA,
				SecurityEntity.class);
		query.setParameter(1, maaPoaId);
		return query.getResultList();
	}

	@Override
	public boolean allSecuritiesInPoolAreBlanketPurpose(Long poolId){
		Query query = vbsEm.createNativeQuery("SELECT COUNT(*) FROM SECURITY WHERE SECURITY_PURPOSE_ID != 2 AND POOL_ID = ?1");
		query.setParameter(1, poolId);
		Integer num = (Integer) query.getSingleResult();
		return num.intValue() == 0;
	}

	@Override
	public boolean allSecuritiesInPoolAreFreeholdType(Long poolId){
		Query query = vbsEm.createNativeQuery("SELECT COUNT(*) FROM SECURITY WHERE IDENTITY_TYPE_ID != 2 AND POOL_ID = ?1");
		query.setParameter(1, poolId);
		Integer num = (Integer) query.getSingleResult();
		return num.intValue() == 0;
	}

	private void deleteSecuritySearchParamsUpdateDate(DeletedSecuritySearchParametersDTO searchParams, StringBuilder sb, Map<String, Object> params) {
		String sql = "";
		if (searchParams.getUpdateDate() != null) {
			if (searchParams.getUpdateDateType().equals(SearchTypeEnum.BETWEEN)
					&& searchParams.getUpdateDateBetweenTo() != null) {
				sql += " AND S.UPDATE_DATE BETWEEN :updateDateFrom AND :updateDateTo ";
				params.put("updateDateFrom", searchParams.getUpdateDate());
				params.put("updateDateTo", searchParams.getUpdateDateBetweenTo());
			} else {
				if (searchParams.getUpdateDateType().equals(SearchTypeEnum.EQUALS)) {
					sql += " AND S.UPDATE_DATE = :updateDate ";
				} else if (searchParams.getUpdateDateType().equals(SearchTypeEnum.GREATER_THAN)) {
					sql += " AND S.UPDATE_DATE > :updateDate ";
				} else if (searchParams.getUpdateDateType().equals(SearchTypeEnum.LESS_THAN)) {
					sql += " AND S.UPDATE_DATE < :updateDate ";
				} else if (searchParams.getUpdateDateType().equals(SearchTypeEnum.EQUALS_GREATER_THAN)) {
					sql += " AND S.UPDATE_DATE >= :updateDate ";
				} else if (searchParams.getUpdateDateType().equals(SearchTypeEnum.EQUALS_LESS_THAN)) {
					sql += " AND S.UPDATE_DATE <= :updateDate ";
				}

				params.put("updateDate", searchParams.getUpdateDate());
			}
		}
		sb.append(sql);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SecuritySearchResultDTO> searchDeletedSecurity(DeletedSecuritySearchParametersDTO searchParams) {
		List<SecuritySearchResultDTO> ret = new ArrayList<SecuritySearchResultDTO>();

		Map<String, Object> params = new LinkedHashMap<>();

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT " 
				+ "S.ID AS securityNumber, " 
				+ "S.INSTRUMENT_NUMBER AS instrumentNumber, "
				+ "S.RECEIVED_DATE AS receivedDate, "
				+ "S.POOL_ID AS poolId, "
				+ "ST.DESCRIPTION AS securityTypeName, "
				+ "CAST(REPLACE(VB.CRM_CONTACT_ID, 'B', '') AS int) AS vbNumber, "
				+ "VB.COMPANY_NAME AS vbName, "
				+ "SP.DESCRIPTION AS securityPurposeName, "
				+ "S.ORIGINAL_AMOUNT AS originalAmount, "
				+ "S.CURRENT_AMOUNT AS currentAmount, ");

		sb.append("CASE WHEN S.SECURITY_TYPE_ID != 1 OR S.UNPOST_DATE IS NOT NULL THEN 0 " +
				"ELSE CASE WHEN S.CURRENT_AMOUNT = 0 THEN 0 ELSE " +
				"(SELECT COALESCE(SUM(I.INTEREST_AMOUNT), 0) FROM INTEREST I WHERE I.SECURITY_ID = S.ID)" +
				"- (SELECT COALESCE(SUM(R.INTEREST_AMOUNT), 0) FROM RELEASE R WHERE R.SECURITY_ID = S.ID AND R.STATUS IN ('VOUCHER_CREATED', 'SENT', 'COMPLETED')) " +
				"END END AS currentInterestAmount ");

		sb.append("FROM SECURITY S ");
		sb.append("JOIN SECURITY_TYPE ST ON ST.ID = S.SECURITY_TYPE_ID "
				+ "JOIN SECURITY_PURPOSE SP ON SP.ID = S.SECURITY_PURPOSE_ID "
				+ "LEFT OUTER JOIN CONTACT VB ON VB.ID = S.PRIMARY_VB_ID "
				+ "WHERE (DELETED = 1) AND S.SECURITY_TYPE_ID IN (" + VbsConstants.SECURITY_TYPE_CASH + ", " + VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT + ") ");

		deleteSecuritySearchParamsUpdateDate(searchParams, sb, params);
		sb.append("ORDER BY S.ID DESC ");

		Query query = vbsEm.createNativeQuery(sb.toString(), Tuple.class)
				.setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false);
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		List<Tuple> res = query.getResultList();
		ret = res.stream().map(this::transformSearchDeletedSecurity).collect(Collectors.toList());

		return ret;
	}

    @Override
    public List<Long> getPoolIdsByVbBlanketSecurity(Long vbContactId) {
		TypedQuery<Long> query = vbsEm.createNamedQuery(VbPoolEntity.SELECT_POOL_BY_BLANKET_VB_ID, Long.class);
		query.setParameter(1, vbContactId);
		return query.getResultList();
	}

    @Override
    public void addCEEnrolmentDecisionEntity(CEEnrolmentDecisionEntity entity) {
        vbsEm.persist(entity);
    }

    @Override
	public void saveCEEnrolmentDecisionEntity(CEEnrolmentDecisionEntity entity) {
		vbsEm.merge(entity);
	}

	@Override
	public List<SecurityEntity> getBlanketSecuritiesByEnrollingVb(String enrolmentNumber) {
		TypedQuery<SecurityEntity> query = vbsEm.createNamedQuery(SecurityEntity.SELECT_BLANKET_SECURITY_BY_ENROLMENT_ENROLLING_VB, SecurityEntity.class);
		query.setParameter(1, enrolmentNumber);
		return query.getResultList();
	}

	@Override
	public CEEnrolmentDecisionEntity getOutstandingCEEnrolmentDecisionEntityByEnrolmentNumber(String enrolmentNumber){
		TypedQuery<CEEnrolmentDecisionEntity> query = vbsEm.createNamedQuery(CEEnrolmentDecisionEntity.SELECT_OUTSTANDING_DECISION_BY_ENROLMENT_NUMBER, CEEnrolmentDecisionEntity.class);
		query.setParameter(1, enrolmentNumber);
		try{
			return query.getSingleResult();
		}catch(NoResultException e){
			throw new VbsRuntimeException("No outstanding decision found for enrolment number: "+enrolmentNumber);
		}
	}

    private SecuritySearchResultDTO transformSearchDeletedSecurity(Tuple t){
		SecuritySearchResultDTO dto = new SecuritySearchResultDTO();
		dto.setInstrumentNumber(t.get("instrumentNumber", String.class));
		dto.setVbNumber(t.get("vbNumber", Integer.class));
		dto.setVbName(t.get("vbName", String.class));
		dto.setSecurityTypeName(t.get("securityTypeName", String.class));
		dto.setSecurityPurposeName(t.get("securityPurposeName", String.class));
		dto.setCurrentAmount(t.get("currentAmount", BigDecimal.class));
		dto.setOriginalAmount(t.get("originalAmount", BigDecimal.class));
		dto.setCurrentInterestAmount(t.get("currentInterestAmount", BigDecimal.class));
		dto.setSecurityNumber(t.get("securityNumber", Integer.class));
		dto.setPoolId(t.get("poolId", Integer.class));
		dto.setReceivedDate(t.get("receivedDate", Timestamp.class).toLocalDateTime());
		return dto;
	}
	@Override
	public boolean hasSecurityHadStatus(Long securityId, Long statusId){
		Query query = vbsEm.createNamedQuery(SecurityEntity.SELECT_NUMBER_OF_SECURITY_AUD_BY_SECURITY_ID_STATUS);
		query.setParameter(1, securityId);
		query.setParameter(2, statusId);
		Integer count = (Integer)query.getSingleResult();
		return count.intValue() > 0;
	}
}
