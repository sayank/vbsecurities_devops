/* 
 * 
 * HistoryEnversDAOImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.history;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.enrolment.EnrolmentActivityDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentPoolActivityDTO;
import com.tarion.vbs.common.dto.financialinstitution.FIActivityDTO;
import com.tarion.vbs.common.dto.security.HistoryFieldDTO;
import com.tarion.vbs.common.dto.tip.ActivityDTO;
import com.tarion.vbs.common.enums.OrderEnum;
import com.tarion.vbs.common.enums.YesNoEnum;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.contact.ContactDAO;
import com.tarion.vbs.dao.financialinstitution.FinancialInstitutionDAO;
import com.tarion.vbs.dao.util.AuditQueryResult;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.orm.entity.auditlog.EnversRevEntity;
import com.tarion.vbs.orm.entity.contact.ContactEntity;
import com.tarion.vbs.orm.entity.contact.ContactSecurityEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentPoolEntity;
import com.tarion.vbs.orm.entity.financialinstitution.*;
import com.tarion.vbs.orm.entity.security.*;
import org.hibernate.Hibernate;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;


/**
 * HistoryDAO class for retrieving history of changes of all the entities
 * utilizing envers API
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-22
 * @version 1.0
 */
@Stateless(mappedName = "ejb/HistoryDAO")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class HistoryEnversDAOImpl implements HistoryDAO {
	
	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;

	@EJB
	private GenericDAO genericDAO;

	@EJB
	private FinancialInstitutionDAO financialInstitutionDAO;

	@EJB
	private ContactDAO contactDAO;

	@Override
	public <T> List<AuditQueryResult> getRevisions(Class<T> entityClass, Long entityId, String propertyName, OrderEnum order){
		AuditReader reader = AuditReaderFactory.get(vbsEm);
		AuditQuery query = reader.createQuery().forRevisionsOfEntity(entityClass, false, true)
				.add(AuditEntity.id().eq(entityId));

		return getAuditQueryResults(propertyName, order, query);
	}

	private <T> AuditQueryResult getRevisionByRevisionNumber(Class<T> entityClass, Long entityId, long revisionNumber ,String propertyName, OrderEnum order){
		AuditReader reader = AuditReaderFactory.get(vbsEm);
		AuditQuery query = reader.createQuery().forRevisionsOfEntity(entityClass, true, false) .addOrder( AuditEntity.revisionNumber().desc() )
				.add( AuditEntity.id().eq( entityId ) )
				.add( AuditEntity.revisionNumber().le(revisionNumber))
				.setMaxResults(1);
		return getAuditQueryResults(propertyName, order, query).get(0);
	}

	@Override
	public List<AuditQueryResult> getSecurityRevisions(Long securityId, String propertyName, Boolean ignoreCurrentInterest, OrderEnum order){
		AuditReader reader = AuditReaderFactory.get(vbsEm);
		AuditQuery query = reader.createQuery().forRevisionsOfEntity(SecurityEntity.class, false, true)
				.add(AuditEntity.id().eq(securityId));
		return getAuditQueryResults(propertyName, order, query);
	}

	private List<AuditQueryResult> getEnrolmentPoolRevisions(Long poolId){
		AuditReader reader = AuditReaderFactory.get(vbsEm);
		AuditQuery query = reader.createQuery().forRevisionsOfEntity(EnrolmentPoolEntity.class, false, true)
				.add(AuditEntity.relatedId("pool").eq(poolId)).add(AuditEntity.revisionType().eq(RevisionType.DEL));

		return getAuditQueryResults(null, null, query);
	}

	private List<AuditQueryResult> getEnrolmentPoolRevisions(Long poolId, int maxRevision){
		AuditReader reader = AuditReaderFactory.get(vbsEm);
		AuditQuery query = reader.createQuery().forRevisionsOfEntity(EnrolmentPoolEntity.class, false, true)
				.add(AuditEntity.relatedId("pool").eq(poolId))
				.add(AuditEntity.revisionNumber().maximize().computeAggregationInInstanceContext())
				.add(AuditEntity.revisionNumber().le(maxRevision));

		return getAuditQueryResults(null, null, query);
	}

	@Override
    @SuppressWarnings("unchecked")
	public List<ActivityDTO> getSecurityActivities(Long securityId){
		List<ActivityDTO> ret = new ArrayList<>();
		Map<Long, PoolTracking> poolTrack = new HashMap<>();

		populatePoolTracking(securityId, ret, poolTrack);

		poolTrack.forEach((key, value) -> {
            List<AuditQueryResult> poolRevisions = getRevisions(PoolEntity.class, value.getPoolId(), null, null);
            for (int j = 0; j < poolRevisions.size(); j++) {
                if (poolRevisions.get(j).getTracking().getRevisionDate().compareTo(value.getFrom()) >= 0 &&
                        poolRevisions.get(j).getTracking().getRevisionDate().compareTo(value.getTo()) < 0) {

                    PoolEntity poolCurr = (PoolEntity) poolRevisions.get(j).getEntity();
                    PoolEntity poolPrev = (j<poolRevisions.size()-1) ? (PoolEntity) poolRevisions.get(j + 1).getEntity() : null;
					if ((poolPrev == null || poolCurr.getUnitsToCover() != poolPrev.getUnitsToCover()) && poolCurr.getFHUnitsToCover() == null && poolCurr.getCEUnitsToCover() == null) {
                        ret.add(new ActivityDTO(securityId, "TWC_UNITS_TO_COVER",
                                poolRevisions.get(j).getTracking().getId(), poolRevisions.get(j).getTracking().getRevisionDate(),
                                "Units Intended To Cover", "Security Units Int.to Cover Change",
                                poolCurr.getUpdateUser(), (poolPrev != null) ? String.valueOf(poolPrev.getUnitsToCover()) : "", String.valueOf(poolCurr.getUnitsToCover())));
                    }
					if ((poolPrev == null || poolCurr.getFHUnitsToCover() != poolPrev.getFHUnitsToCover()) && poolCurr.getFHUnitsToCover() != null) {
						ret.add(new ActivityDTO(securityId, "TWC_FH_UNITS_TO_COVER",
								poolRevisions.get(j).getTracking().getId(), poolRevisions.get(j).getTracking().getRevisionDate(),
								"FH Units Intended To Cover", "Security Freehold Units Int.to Cover Change",
								poolCurr.getUpdateUser(), (poolPrev != null) ? String.valueOf(poolPrev.getFHUnitsToCover()) : "", String.valueOf(poolCurr.getFHUnitsToCover())));
					}
					if ((poolPrev == null || poolCurr.getCEUnitsToCover() != poolPrev.getCEUnitsToCover()) && poolCurr.getCEUnitsToCover() != null) {
						ret.add(new ActivityDTO(securityId, "TWC_CE_UNITS_TO_COVER",
								poolRevisions.get(j).getTracking().getId(), poolRevisions.get(j).getTracking().getRevisionDate(),
								"CE Units Intended To Cover", "Security CE Units Int.to Cover Change",
								poolCurr.getUpdateUser(), (poolPrev != null) ? String.valueOf(poolPrev.getCEUnitsToCover()) : "", String.valueOf(poolCurr.getCEUnitsToCover())));
					}
				}
            }



            AuditReader reader = AuditReaderFactory.get(vbsEm);
            AuditQuery query = reader.createQuery().forRevisionsOfEntity(VbPoolEntity.class, false, true)
					//.add(AuditEntity.property("vbDeleteReason").hasChanged())
					.add(AuditEntity.relatedId("pool").eq(value.getPoolId()))
                    .addOrder(AuditEntity.revisionNumber().desc());
            List<Object[]> results = query.getResultList();
            List<AuditQueryResult> vbPoolRevisions = results.stream().map(AuditQueryResult::new).collect(Collectors.toList());
            //split the list into revisions per VB.
            Map<Long, List<AuditQueryResult>> map = new HashMap<>();
            for (AuditQueryResult vbPoolRevision : vbPoolRevisions) {
                Long vbId = ((VbPoolEntity) vbPoolRevision.getEntity()).getVb().getId();
                List<AuditQueryResult> list = map.containsKey(vbId) ? map.get(vbId) : new ArrayList<>();
                list.add(vbPoolRevision);
                map.put(vbId, list);
            }

            for(Map.Entry<Long, List<AuditQueryResult>> entry : map.entrySet()){

                for(int k=0;k<entry.getValue().size();k++){
                    if (entry.getValue().get(k).getTracking().getRevisionDate().compareTo(value.getFrom()) >= 0 &&
                            entry.getValue().get(k).getTracking().getRevisionDate().compareTo(value.getTo()) < 0) {

                        VbPoolEntity vbCurr = (VbPoolEntity) entry.getValue().get(k).getEntity();
                        VbPoolEntity vbPrev = (k<entry.getValue().size()-1) ? (VbPoolEntity) entry.getValue().get(k + 1).getEntity() : null;

                        if(vbPrev == null || vbPrev.getVbDeleteReason() != null && vbCurr.getVbDeleteReason() == null) {
                            ret.add(new ActivityDTO(securityId, "COMPANYID",
                                    entry.getValue().get(k).getTracking().getId(), entry.getValue().get(k).getTracking().getRevisionDate(),
                                    "V/B #", "Security VB Changed",
                                    vbCurr.getUpdateUser(), null, vbCurr.getVb().getCrmContactId()));
                        }else if(vbPrev.getVbDeleteReason() == null && vbCurr.getVbDeleteReason() != null) {
                            if (vbCurr.getVbDeleteReason().getId().compareTo(2L) == 0 || vbCurr.getVbDeleteReason().getId().compareTo(3L) == 0) {
                                ret.add(new ActivityDTO(securityId, "COMPANYID",
                                        entry.getValue().get(k).getTracking().getId(), entry.getValue().get(k).getTracking().getRevisionDate(),
                                        "Blanket Security VB Removed", "All projects under VB completed/cancelled - Removed VB no. from Blanket Sec",
                                        vbCurr.getUpdateUser(), vbCurr.getVb().getCrmContactId(), null));
                            } else {
                                ret.add(new ActivityDTO(securityId, "COMPANYID",
                                        entry.getValue().get(k).getTracking().getId(), entry.getValue().get(k).getTracking().getRevisionDate(),
                                        "V/B #", "Security VB Changed",
                                        vbCurr.getUpdateUser(), vbPrev.getVb().getCrmContactId(), null));
                            }
                        }
                    }
                }
            }

			AuditQuery enrolmentPoolQuery = reader.createQuery().forRevisionsOfEntity(EnrolmentPoolEntity.class, false, true)
					.add(AuditEntity.relatedId("pool").eq(value.getPoolId()))
					.addOrder(AuditEntity.revisionNumber().desc());
			List<Object[]> enrolmentPoolResults = enrolmentPoolQuery.getResultList();
			List<AuditQueryResult> enrolmentPoolRevisions = enrolmentPoolResults.stream().map(AuditQueryResult::new).collect(Collectors.toList());
			List<String> enrNums = new ArrayList<>();
			for(int i=0;i<enrolmentPoolRevisions.size();i++){
				if (enrolmentPoolRevisions.get(i).getTracking().getRevisionDate().compareTo(value.getFrom()) >= 0 &&
						enrolmentPoolRevisions.get(i).getTracking().getRevisionDate().compareTo(value.getTo()) <= 0) {
					String num = ((EnrolmentPoolEntity) enrolmentPoolRevisions.get(i).getEntity()).getEnrolment().getEnrolmentNumber();
					if (!enrNums.contains(num)) { enrNums.add(num); }
				}
			}

			for(String enrolmentNumber: enrNums){
				AuditQuery enrolmentQuery = reader.createQuery().forRevisionsOfEntity(EnrolmentPoolEntity.class, false, true)
						.add(AuditEntity.relatedId("enrolment").eq(enrolmentNumber))
						.add(AuditEntity.relatedId("pool").eq(value.getPoolId()))
						.addOrder(AuditEntity.revisionNumber().desc());
				List<Object[]> enrolmentResults = enrolmentQuery.getResultList();
				List<AuditQueryResult> enrolmentRevisions = enrolmentResults.stream().map(AuditQueryResult::new).collect(Collectors.toList());
				for(int i=0;i<enrolmentRevisions.size();i++){
					if (enrolmentRevisions.get(i).getTracking().getRevisionDate().compareTo(value.getFrom()) >= 0 &&
							enrolmentRevisions.get(i).getTracking().getRevisionDate().compareTo(value.getTo()) <= 0) {
						EnrolmentPoolEntity enrCurr = (EnrolmentPoolEntity) enrolmentRevisions.get(i).getEntity();
						if (i == enrolmentRevisions.size() - 1) {
							if(enrCurr.getPefAmount() != null && enrCurr.getPefAmount().compareTo(BigDecimal.ZERO) != 0) {
								ret.add(new ActivityDTO(securityId, "PEF_AMOUNT",
										enrolmentRevisions.get(i).getTracking().getId(), enrolmentRevisions.get(i).getTracking().getRevisionDate(),
										"PEF Amount Changed", "PEF Amount at Condo Registration added for " + enrCurr.getEnrolment().getEnrolmentNumber(),
										enrCurr.getUpdateUser(), "", VbsUtil.getCurrencyFormattedString(enrCurr.getPefAmount())));
							}
						} else {
							//update
							EnrolmentPoolEntity enrPrev = (EnrolmentPoolEntity) enrolmentRevisions.get(i + 1).getEntity();
							if (bigDecimalNotEqual(enrCurr.getPefAmount(), enrPrev.getPefAmount())) {
								ret.add(new ActivityDTO(securityId, "PEF_AMOUNT",
										enrolmentRevisions.get(i).getTracking().getId(), enrolmentRevisions.get(i).getTracking().getRevisionDate(),
										"PEF Amount Changed", "PEF Amount at Condo Registration "+ ((enrPrev.getPefAmount() == null || enrPrev.getPefAmount().compareTo(BigDecimal.ZERO) == 0) ? "added" : "changed") + " for " + enrCurr.getEnrolment().getEnrolmentNumber(),
										enrCurr.getUpdateUser(), VbsUtil.getCurrencyFormattedString(enrPrev.getPefAmount()), VbsUtil.getCurrencyFormattedString(enrCurr.getPefAmount())));
							}
						}
					}
				}
			}
        });

		AuditReader reader = AuditReaderFactory.get(vbsEm);
		AuditQuery query = reader.createQuery().forRevisionsOfEntity(ContactSecurityEntity.class, false, true)
				.add(AuditEntity.relatedId("security").eq(securityId))
				.add(AuditEntity.relatedId("contactSecurityRole").eq(VbsConstants.CONTACT_SECURITY_ESCROW))
				.addOrder(AuditEntity.revisionNumber().desc());
		List<Object[]> results = query.getResultList();
		List<AuditQueryResult> escrowResults = results.stream().map(AuditQueryResult::new).collect(Collectors.toList());
		for(int i=0;i<escrowResults.size()-1;i++){
			ContactSecurityEntity escrowCurr = (ContactSecurityEntity) escrowResults.get(i).getEntity();
			ContactSecurityEntity escrowPrev = (ContactSecurityEntity) escrowResults.get(i + 1).getEntity();

			if(((escrowCurr.getContact() == null) != (escrowPrev.getContact() == null)) || (escrowCurr.getContact() != null && escrowPrev.getContact() != null &&
				(escrowCurr.getContact().getId().compareTo(escrowPrev.getContact().getId()) != 0))){
				ret.add(new ActivityDTO(securityId, "ESCROW_AGENT",
						escrowResults.get(i).getTracking().getId(), escrowResults.get(i).getTracking().getRevisionDate(),
						"Escrow Agent Name", "Escrow Agent Name Change - Take Over",
						escrowCurr.getUpdateUser(), (escrowPrev.getContact() != null) ? escrowPrev.getContact().getCompanyName() : "", (escrowCurr.getContact() != null) ? escrowCurr.getContact().getCompanyName() : ""));
			}
		}

		ret.addAll(this.getMonthlyReportActivities(securityId));
		return ret;
	}

	private void populatePoolTracking(Long securityId, Map<Long, PoolTracking> poolTrack) {
		populatePoolTracking(securityId, null, poolTrack);
	}

	private void populatePoolTracking(Long securityId, List<ActivityDTO> activityList, Map<Long, PoolTracking> poolTrack) {
		List<AuditQueryResult> revisions = getSecurityRevisions(securityId, null,true, OrderEnum.DESC);
		SecurityEntity curr = (SecurityEntity) revisions.get(0).getEntity();
		EnversRevEntity currRev = revisions.get(0).getTracking();

		//as a security can change pools any number of times during its lifetime,
		//we need to build a list of pools to which the security has belonged, and which dates it was in that pool.
		//then we need to select any relevant changes in that pool that were made while the security was a part of it.
		//this also applies for VB_POOL delete reason entries.

		PoolTracking p = new PoolTracking(
				curr.getPool().getId(),
				Date.from(curr.getCreateDate().atZone(ZoneId.systemDefault()).toInstant()),
				new Date(),
				currRev.getId());

		poolTrack.put(curr.getPool().getId(), p);
		for(int i=0;i<revisions.size();i++){
			curr = (SecurityEntity) revisions.get(i).getEntity();
			SecurityEntity prev = null;
			int prevRevision = revisions.get(i).getTracking().getId();
			if(i != revisions.size()-1){
				prev = (SecurityEntity) revisions.get(i+1).getEntity();
			}
			if(prev != null && !poolTrack.containsKey(prev.getPool().getId())){
				//update curr
				PoolTracking cu = poolTrack.get(curr.getPool().getId());
				cu.setFrom(revisions.get(i).getTracking().getRevisionDate());
				poolTrack.put(curr.getPool().getId(), cu);
				//add prev
				PoolTracking pr = new PoolTracking(
						prev.getPool().getId(),
						Date.from(prev.getCreateDate().atZone(ZoneId.systemDefault()).toInstant()),
						revisions.get(i).getTracking().getRevisionDate(),
						prevRevision);
				poolTrack.put(prev.getPool().getId(), pr);

				if(activityList != null) {
					ActivityDTO activity = new ActivityDTO(curr.getId(), "TWC_POOL_NBR",
							revisions.get(i).getTracking().getId(), revisions.get(i).getTracking().getRevisionDate(),
							"Pool ID", "Security Pool Num Change",
							curr.getUpdateUser(), prev.getPool().getId().toString(), curr.getPool().getId().toString());
					activityList.add(activity);
				}

			}else if(prev == null && activityList != null){
				ActivityDTO activity = new ActivityDTO(curr.getId(), "TWC_POOL_NBR",
						revisions.get(i).getTracking().getId(), revisions.get(i).getTracking().getRevisionDate(),
						"Pool ID", "Security Pool Num Change",
						curr.getUpdateUser(), "", curr.getPool().getId().toString());
				activityList.add(activity);
			}

			if(activityList != null) {
				AuditQueryResult prevResult = (i == revisions.size() - 1) ? null : revisions.get(i + 1);
				activityList.addAll(compareSecurityRevisions(revisions.get(i), prevResult));
			}
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<EnrolmentActivityDTO> getEnrolmentActivities(String enrolmentNumber){
		AuditReader reader = AuditReaderFactory.get(vbsEm);
		AuditQuery query = reader.createQuery().forRevisionsOfEntity(EnrolmentEntity.class, false, true)
				.add(AuditEntity.id().eq(enrolmentNumber))
				.addOrder(AuditEntity.revisionNumber().desc());
		List<Object[]> results = query.getResultList();
		List<AuditQueryResult> enrolmentResults = results.stream().map(AuditQueryResult::new).collect(Collectors.toList());

		List<EnrolmentActivityDTO> ret = new ArrayList<>();
		for(int i=0;i<enrolmentResults.size();i++){

			AuditQueryResult a = enrolmentResults.get(i);
			AuditQueryResult b = (i<enrolmentResults.size()-1) ? enrolmentResults.get(i+1) : null;

			EnrolmentEntity enrA = (EnrolmentEntity) a.getEntity();
			EnrolmentEntity enrB = (b != null) ? (EnrolmentEntity) b.getEntity() : null;

			if(enrB == null || enrA.isAutoOneYear() != enrB.isAutoOneYear()){
				ret.add(new EnrolmentActivityDTO(
					a.getTracking().getRevisionDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(),
					"Auto. Release 1 Yr", (enrB == null) ? "" : (enrB.isAutoOneYear()) ? "Yes" : "No", (enrA.isAutoOneYear()) ? "Yes" : "No", enrA.getUpdateUser()));
			}
			if(enrB == null || enrA.isAutoOneYearRfc() != enrB.isAutoOneYearRfc()){
				ret.add(new EnrolmentActivityDTO(
						a.getTracking().getRevisionDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(),
						"Auto. Release 1 Yr RFC", (enrB == null) ? "" : (enrB.isAutoOneYearRfc()) ? "Yes" : "No", (enrA.isAutoOneYearRfc()) ? "Yes" : "No", enrA.getUpdateUser()));
			}
			if(enrB == null || enrA.isAutoTwoYear() != enrB.isAutoTwoYear()){
				ret.add(new EnrolmentActivityDTO(
						a.getTracking().getRevisionDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(),
						"Auto. Release 2 Yr", (enrB == null) ? "" : (enrB.isAutoTwoYear()) ? "Yes" : "No", (enrA.isAutoTwoYear()) ? "Yes" : "No", enrA.getUpdateUser()));
			}
			if(enrB == null || enrA.isExcessDeposit() != enrB.isExcessDeposit()){
				ret.add(new EnrolmentActivityDTO(
						a.getTracking().getRevisionDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(),
						"Excess Deposit", (enrB == null) ? "" : (enrB.isExcessDeposit()) ? "Yes" : "No", (enrA.isExcessDeposit()) ? "Yes" : "No", enrA.getUpdateUser()));
			}
			if(enrB == null || enrA.isExcessDepositRelease() != enrB.isExcessDepositRelease()){
				ret.add(new EnrolmentActivityDTO(
						a.getTracking().getRevisionDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(),
						"Excess Deposit Release", (enrB == null) ? "" : (enrB.isExcessDepositRelease()) ? "Yes" : "No", (enrA.isExcessDepositRelease()) ? "Yes" : "No", enrA.getUpdateUser()));
			}
		}

		return ret;
	}

	@Override
	public List<FIActivityDTO> getFinancialInstitutionActivities(Long financialInstitutionId) {

		List<AuditQueryResult> revisions = getFIRevisions(financialInstitutionId);
		List<FIActivityDTO> ret = new ArrayList<>();

		for(int i=0;i<revisions.size()-1;i++){
			ret.addAll(compareFIRevisions(revisions.get(i), revisions.get(i+1)));
		}

		if(revisions.size() > 0) {
			FinancialInstitutionEntity first = (FinancialInstitutionEntity) revisions.get(revisions.size() - 1).getEntity();
			ret.add(new FIActivityDTO("Financial Institution Name", "", first.getName(), first.getUpdateUser(), first.getUpdateDate()));
			ret.add(new FIActivityDTO("Financial Institution Type", "", first.getFinancialInstitutionType() == null ? "" : first.getFinancialInstitutionType().getName(), first.getUpdateUser(), first.getUpdateDate()));
			ret.add(new FIActivityDTO("Description", "", first.getDescription(), first.getUpdateUser(), first.getUpdateDate()));
			ret.add(new FIActivityDTO("Maximum Security Amount", "", VbsUtil.getCurrencyFormattedString(first.getMaxSecurityAmount()), first.getUpdateUser(), first.getUpdateDate()));
			ret.add(new FIActivityDTO("Warning Max Security Amount", "", VbsUtil.getCurrencyFormattedString(first.getWnSecurityMaxAmount()), first.getUpdateUser(), first.getUpdateDate()));
			ret.add(new FIActivityDTO("Expiry Date", "", DateUtil.getDateFormattedShort(first.getExpiryDate()), first.getUpdateUser(), first.getUpdateDate()));
		}

        List<AuditQueryResult> contactRevisions = getFIContactsRevisions(financialInstitutionId);
        Map<Long, List<AuditQueryResult>> contactMap = new HashMap<>();
        for (AuditQueryResult rev: contactRevisions) {
            ContactFinancialInstitutionEntity entity = (ContactFinancialInstitutionEntity) rev.getEntity();
            List<AuditQueryResult> list = contactMap.get(entity.getId());
            if(list == null) {
                list = new ArrayList<>();
                contactMap.put(entity.getId(), list);
            }
            list.add(rev);
        }
        for (List<AuditQueryResult> contactRevs: contactMap.values() ) {
            for(int i=0;i<contactRevs.size()-1;i++){
                ret.addAll(compareFIContactsRevisions(contactRevs.get(i), contactRevs.get(i+1)));
            }
            if(contactRevs.size() > 0) {
                ContactFinancialInstitutionEntity first = (ContactFinancialInstitutionEntity) contactRevs.get(contactRevs.size()-1).getEntity();
                ContactEntity contact = genericDAO.findEntityById(ContactEntity.class, first.getContact().getId());
                if(first.getStartDate()!=null) {
                    ret.add(new FIActivityDTO("Added " + getSubject(contact, "Start Date"), "", DateUtil.getDateFormattedShort(first.getStartDate()), first.getUpdateUser(), first.getUpdateDate()));
                }
                if(first.getEndDate()!=null) {
                    ret.add(new FIActivityDTO("Added " + getSubject(contact, "End Date"), "", DateUtil.getDateFormattedShort(first.getEndDate()), first.getUpdateUser(), first.getUpdateDate()));
                }
            }
        }



		List<AuditQueryResult> maaPoaRevisions = getFIMAAPOARevisions(financialInstitutionId);
		Map<Long, List<AuditQueryResult>> maaPoaMap = new HashMap<>();
		for (AuditQueryResult rev: maaPoaRevisions) {
			FinancialInstitutionMaaPoaEntity entity = (FinancialInstitutionMaaPoaEntity) rev.getEntity();
			List<AuditQueryResult> list = maaPoaMap.get(entity.getId());
			if(list == null) {
				list = new ArrayList<>();
				maaPoaMap.put(entity.getId(), list);
			}
			list.add(rev);
		}
		boolean isAgent = VbsConstants.FINANCIAL_INSTITUTION_TYPE_ADMINISTRATIVE_AGENT == financialInstitutionDAO.getFinancialInstitutionTypeId(financialInstitutionId);
		for (List<AuditQueryResult> list: maaPoaMap.values()) {
			for(int i=0;i<list.size()-1;i++){
				ret.addAll(compareFIMAAPOAARevisions(list.get(i), list.get(i+1), isAgent));
			}
			if(list.size() > 0) {
				FinancialInstitutionMaaPoaEntity first = (FinancialInstitutionMaaPoaEntity) list.get(list.size()-1).getEntity();
				if(first.getExpirationDate() != null) {
					ret.add(new FIActivityDTO("Added " + (isAgent ? "MAA " : "POA ") + first.getMaaPoaSequence() + ", Expiration Date", "", DateUtil.getDateFormattedShort(first.getExpirationDate()), first.getUpdateUser(), first.getUpdateDate()));
				}
			}
		}
		for (Long maaPOAId: maaPoaMap.keySet()) {
			List<AuditQueryResult> officersRevisions = getFIOfficersRevisions(maaPOAId);
            Map<Long, List<AuditQueryResult>> map = new HashMap<>();
			for (AuditQueryResult officersRevision : officersRevisions) {
				Long soId = ((FinancialInstitutionSigningOfficerEntity) officersRevision.getEntity()).getId();
				List<AuditQueryResult> list = (map.containsKey(soId)) ? map.get(soId) : new ArrayList<>();
				list.add(officersRevision);
				map.put(soId, list);
			}

			for(Map.Entry<Long, List<AuditQueryResult>> entry : map.entrySet()){
				for(int i=0;i<entry.getValue().size();i++){
					AuditQueryResult resA = entry.getValue().get(i);
					AuditQueryResult resB = (i<entry.getValue().size()-1) ? entry.getValue().get(i+1) : null;
					ret.addAll(compareFIOfficersRevisions(resA, resB));
				}
			}
		}

		List<BranchEntity> branches = financialInstitutionDAO.getBranchesForFinancialInstitution(financialInstitutionId);
		for (BranchEntity branch: branches) {
			ret.add(new FIActivityDTO("Branch added", "", branch.getAddress() == null ? branch.getId().toString() : branch.getAddress().concatAddress(), branch.getCreateUser(), branch.getCreateDate()));
		}

		Collections.sort(ret);
		return ret;
	}

	private List<FIActivityDTO> compareFIRevisions(AuditQueryResult a, AuditQueryResult b) {
		List<FIActivityDTO> ret = new ArrayList<>();

		FinancialInstitutionEntity curr = (FinancialInstitutionEntity) a.getEntity();
		FinancialInstitutionEntity prev = (FinancialInstitutionEntity) b.getEntity();

		if (notEqual(curr.getName(), prev.getName())) {
			ret.add(new FIActivityDTO("Financial Institution Name", prev.getName(), curr.getName(), curr.getUpdateUser(), curr.getUpdateDate()));
		}
		if(((curr.getFinancialInstitutionType() == null) != (prev.getFinancialInstitutionType() == null)) 
			|| (curr.getFinancialInstitutionType() != null && prev.getFinancialInstitutionType() != null &&	curr.getFinancialInstitutionType().getId().compareTo(prev.getFinancialInstitutionType().getId()) != 0)) {

			String previousType = prev.getFinancialInstitutionType() == null ? "" : prev.getFinancialInstitutionType().getName();
			String currType = curr.getFinancialInstitutionType() == null ? "" : curr.getFinancialInstitutionType().getName();
			ret.add(new FIActivityDTO("Financial Institution Type", previousType, currType, curr.getUpdateUser(), curr.getUpdateDate()));
		}
		if (notEqual(curr.getDescription(), prev.getDescription())) {
			ret.add(new FIActivityDTO("Description", prev.getDescription(), curr.getDescription(), curr.getUpdateUser(), curr.getUpdateDate()));
		}
		if (notEqual(curr.getMaxSecurityAmount(), prev.getMaxSecurityAmount())) {
			ret.add(new FIActivityDTO("Maximum Security Amount", VbsUtil.getCurrencyFormattedString(prev.getMaxSecurityAmount()), VbsUtil.getCurrencyFormattedString(curr.getMaxSecurityAmount()), curr.getUpdateUser(), curr.getUpdateDate()));
		}
		if (notEqual(curr.getWnSecurityMaxAmount(), prev.getWnSecurityMaxAmount())) {
			ret.add(new FIActivityDTO("Warning Max Security Amount", VbsUtil.getCurrencyFormattedString(prev.getWnSecurityMaxAmount()), VbsUtil.getCurrencyFormattedString(curr.getWnSecurityMaxAmount()), curr.getUpdateUser(), curr.getUpdateDate()));
		}
		if (notEqual(curr.getExpiryDate(), prev.getExpiryDate())) {
			ret.add(new FIActivityDTO("Expiration Date", DateUtil.getDateFormattedShort(prev.getExpiryDate()), DateUtil.getDateFormattedShort(curr.getExpiryDate()), curr.getUpdateUser(), curr.getUpdateDate()));
		}

		return ret;
	}

	private List<FIActivityDTO> compareFIContactsRevisions(AuditQueryResult a, AuditQueryResult b) {
		List<FIActivityDTO> ret = new ArrayList<>();

		ContactFinancialInstitutionEntity curr = (ContactFinancialInstitutionEntity) a.getEntity();
		ContactFinancialInstitutionEntity prev = (ContactFinancialInstitutionEntity) b.getEntity();
		ContactEntity contact = genericDAO.findEntityById(ContactEntity.class, curr.getContact().getId());

		if (notEqual(curr.getStartDate(), prev.getStartDate())) {
			ret.add(new FIActivityDTO(getSubject(contact, "Start Date"), DateUtil.getDateFormattedShort(prev.getStartDate()), DateUtil.getDateFormattedShort(curr.getStartDate()), curr.getUpdateUser(), curr.getUpdateDate()));
		}
		if (notEqual(curr.getEndDate(), prev.getEndDate())) {
			ret.add(new FIActivityDTO(getSubject(contact, "End Date"), DateUtil.getDateFormattedShort(prev.getEndDate()), DateUtil.getDateFormattedShort(curr.getEndDate()), curr.getUpdateUser(), curr.getUpdateDate()));
		}
		if(a.getRevisionType() == RevisionType.DEL) {
            ret.add(new FIActivityDTO("Deleted " + getSubject(contact, null), "", "", curr.getUpdateUser(), curr.getUpdateDate()));
        }
		return ret;
	}

	private List<FIActivityDTO> compareFIMAAPOAARevisions(AuditQueryResult a, AuditQueryResult b, boolean isAgent) {
		List<FIActivityDTO> ret = new ArrayList<>();

		FinancialInstitutionMaaPoaEntity curr = (FinancialInstitutionMaaPoaEntity) a.getEntity();
		FinancialInstitutionMaaPoaEntity prev = (FinancialInstitutionMaaPoaEntity) b.getEntity();

		if (notEqual(curr.getExpirationDate(), prev.getExpirationDate())) {
			ret.add(new FIActivityDTO(isAgent ? "MAA " : "POA " + curr.getMaaPoaSequence() + ", Expiration Date", DateUtil.getDateFormattedShort(prev.getExpirationDate()), DateUtil.getDateFormattedShort(curr.getExpirationDate()), curr.getUpdateUser(), curr.getUpdateDate()));
		}
		if(a.getRevisionType() == RevisionType.DEL) {
			ret.add(new FIActivityDTO("Deleted " + getSubject(curr, isAgent), "", "", curr.getUpdateUser(), curr.getUpdateDate()));
		}
		return ret;
	}

	@Override
	public List<EnrolmentPoolActivityDTO> deletedEnrolmentsActivities(Long securityId) {

		LinkedHashMap<Long, PoolTracking> poolTrack = new LinkedHashMap<>();
		List<EnrolmentPoolActivityDTO> enrolmentPoolActivityList = new ArrayList<>();
		populatePoolTracking(securityId, poolTrack);

		List<Map.Entry<Long, PoolTracking>> poolTrackList = new LinkedList<>();
		poolTrackList.addAll(poolTrack.entrySet());



		for(int i = 0; i < poolTrackList.size(); i++) {
			Map.Entry<Long, PoolTracking> pool = poolTrackList.get(i);

			List<AuditQueryResult> revisions = getEnrolmentPoolRevisions(pool.getKey());
			for(AuditQueryResult revision: revisions) {
				Date revDate = revision.getTracking().getRevisionDate();
				if(revDate.before(pool.getValue().to) && revDate.after(pool.getValue().from)) {
					LocalDateTime deletedDate = DateUtil.getLocalDateFromDate(revision.getTracking().getRevisionDate());
					EnrolmentPoolEntity enrolmentPoolEntity = (EnrolmentPoolEntity) revision.getEntity();
					enrolmentPoolActivityList.add(getEnrolmentPoolActivityDTO(deletedDate, enrolmentPoolEntity));
				}
			}

			if(i < poolTrackList.size() - 1) {
				PoolTracking previous = poolTrackList.get(i+1).getValue();

				List<AuditQueryResult> lastRevisions = getEnrolmentPoolRevisions(previous.poolId, previous.getRevId());
				Collections.reverse(lastRevisions);
				Map<String, EnrolmentPoolEntity> enrolments = new HashMap<>();
				for (AuditQueryResult revision: lastRevisions) {
					EnrolmentPoolEntity enrolment = (EnrolmentPoolEntity)revision.getEntity();
					if(revision.getRevisionType() == RevisionType.DEL) {
						enrolments.remove(enrolment.getEnrolment().getEnrolmentNumber());
					} else {
						enrolments.put(enrolment.getEnrolment().getEnrolmentNumber(), enrolment);
					}
				}

				LocalDateTime deletedDate = DateUtil.getLocalDateFromDate(pool.getValue().from);
				for (EnrolmentPoolEntity enrolmentPoolEntity: enrolments.values()) {
					enrolmentPoolActivityList.add(getEnrolmentPoolActivityDTO(deletedDate, enrolmentPoolEntity));
				}
			}

		}

		return enrolmentPoolActivityList;
	}

	private EnrolmentPoolActivityDTO getEnrolmentPoolActivityDTO(LocalDateTime deletedDate, EnrolmentPoolEntity enrolmentPoolEntity) {
		String vbNumber = enrolmentPoolEntity.getEnrolment().getEnrollingVb() == null ? "" : enrolmentPoolEntity.getEnrolment().getEnrollingVb().getCrmContactId();
		String vbName = enrolmentPoolEntity.getEnrolment().getEnrollingVb().getCompanyName() == null ? "" : enrolmentPoolEntity.getEnrolment().getEnrollingVb().getCompanyName();
		String enrolmentAddress = enrolmentPoolEntity.getEnrolment().getAddress() == null ? "" : enrolmentPoolEntity.getEnrolment().getAddress().concatAddress();
		String unitType = null;
		if(enrolmentPoolEntity.getEnrolment().getUnitType() != null && enrolmentPoolEntity.getEnrolment().getUnitType().equalsIgnoreCase(VbsConstants.ENROLMENT_UNIT_TYPE_FH)){
			unitType = "FH";
		}else if(enrolmentPoolEntity.getEnrolment().getUnitType() != null && enrolmentPoolEntity.getEnrolment().getUnitType().equalsIgnoreCase(VbsConstants.ENROLMENT_UNIT_TYPE_CE)){
			unitType = "CE";
		}else if(enrolmentPoolEntity.getEnrolment().getUnitType() != null && enrolmentPoolEntity.getEnrolment().getUnitType().equalsIgnoreCase(VbsConstants.ENROLMENT_UNIT_TYPE_CE_UNIT)){
			unitType = "UNIT";
		}
		return new EnrolmentPoolActivityDTO(null, enrolmentPoolEntity.getEnrolment().getEnrolmentNumber(), null, vbNumber, unitType, enrolmentAddress, vbName, enrolmentPoolEntity.getUpdateUser(), deletedDate);
	}

	@Override
	public List<EnrolmentPoolActivityDTO> deletedVbActivities(Long poolId) {
		List<EnrolmentPoolActivityDTO> deletedVbActivityList = new ArrayList<>();
		List<VbPoolEntity> deletedVbListForPool = contactDAO.getDeletedVbsForPoolId(poolId);
		for(VbPoolEntity deletedVb: deletedVbListForPool) {
				String vbName = deletedVb.getVb() == null ? "" : deletedVb.getVb().getCompanyName();
				deletedVbActivityList.add(new EnrolmentPoolActivityDTO(deletedVb.getVb().getCrmContactId(), null, deletedVb.getVbDeleteReason().getDescription(), null,null, null, vbName, deletedVb.getUpdateUser(), deletedVb.getUpdateDate()));
			}
		return deletedVbActivityList;
	}


	private List<FIActivityDTO> compareFIOfficersRevisions(AuditQueryResult a, AuditQueryResult b) {
		List<FIActivityDTO> ret = new ArrayList<>();

		FinancialInstitutionSigningOfficerEntity curr = (FinancialInstitutionSigningOfficerEntity) a.getEntity();
		if(b == null){
			String name = (curr.getFinancialInstitutionMaaPoa().getFinancialInstitution().getFinancialInstitutionType().getId().compareTo(VbsConstants.FINANCIAL_INSTITUTION_TYPE_ADMINISTRATIVE_AGENT) == 0) ? "MAA" : "POA";
			ret.add(new FIActivityDTO("Added Signing Officer added to " + name + curr.getFinancialInstitutionMaaPoa().getMaaPoaSequence() + ", Company name", "", curr.getCompanyName(), curr.getUpdateUser(), curr.getUpdateDate()));
			ret.add(new FIActivityDTO("Added Signing Officer, Contact name", "", curr.getContactName(), curr.getUpdateUser(), curr.getUpdateDate()));
		}else{
			FinancialInstitutionSigningOfficerEntity prev = (FinancialInstitutionSigningOfficerEntity) b.getEntity();
			if (notEqual(curr.getCompanyName(), prev.getCompanyName())) {
				ret.add(new FIActivityDTO("Signing Officer Company Name", prev.getCompanyName(), curr.getCompanyName(), curr.getUpdateUser(), curr.getUpdateDate()));
			}
			if (notEqual(curr.getContactName(), prev.getContactName())) {
				ret.add(new FIActivityDTO("Signing Officer Contact Name", prev.getContactName(), curr.getContactName(), curr.getUpdateUser(), curr.getUpdateDate()));
			}
			if(a.getRevisionType() == RevisionType.DEL) {
				ret.add(new FIActivityDTO("Deleted " + getSubject(curr), "", "", curr.getUpdateUser(), curr.getUpdateDate()));
			}
		}

		return ret;
	}

	private String getSubject(ContactEntity contact, String field) {
		String name = contact.getContactType().getId() == VbsConstants.CONTACT_TYPE_PERSON ? contact.getFirstName() + " " + contact.getLastName() : contact.getCompanyName();
		return "Contact " + contact.getCrmContactId() + " - " + name + (field == null ? "" : ", " + field) ;
	}

	private String getSubject(FinancialInstitutionMaaPoaEntity financialInstitutionMaaPoaEntity, boolean isAgent) {
		String name = isAgent ? "MAA": "POA";
		Long sequenceNumber = financialInstitutionMaaPoaEntity.getMaaPoaSequence();
		return name + sequenceNumber ;
	}

	private String getSubject(FinancialInstitutionSigningOfficerEntity financialInstitutionSigningOfficerEntity) {
		String companyName = financialInstitutionSigningOfficerEntity.getCompanyName();
		String contactName = financialInstitutionSigningOfficerEntity.getContactName();
		return " Signing Officer: " + companyName + " " + contactName ;
	}

	private List<AuditQueryResult> getFIRevisions(Long financialInstitutionId){
		AuditReader reader = AuditReaderFactory.get(vbsEm);
		AuditQuery query = reader.createQuery().forRevisionsOfEntity(FinancialInstitutionEntity.class, false, true)
				.add(AuditEntity.id().eq(financialInstitutionId));

		return getAuditQueryResults(null, OrderEnum.DESC, query);
	}

	private List<AuditQueryResult> getFIContactsRevisions(Long financialInstitutionId){
		AuditReader reader = AuditReaderFactory.get(vbsEm);
		AuditQuery query = reader.createQuery().forRevisionsOfEntity(ContactFinancialInstitutionEntity.class, false, true)
				.add(AuditEntity.relatedId("financialInstitution").eq(financialInstitutionId));

		return getAuditQueryResults(null, OrderEnum.DESC, query);
	}

	public List<AuditQueryResult> getFIMAAPOARevisions(Long financialInstitutionId){
		AuditReader reader = AuditReaderFactory.get(vbsEm);
		AuditQuery query = reader.createQuery().forRevisionsOfEntity(FinancialInstitutionMaaPoaEntity.class, false, true)
				.add(AuditEntity.relatedId("financialInstitution").eq(financialInstitutionId));

		return getAuditQueryResults(null, OrderEnum.DESC, query);
	}

	private List<AuditQueryResult> getFIOfficersRevisions(Long financialInstitutionMaaPoaId){
		AuditReader reader = AuditReaderFactory.get(vbsEm);
		AuditQuery query = reader.createQuery().forRevisionsOfEntity(FinancialInstitutionSigningOfficerEntity.class, false, true)
				.add(AuditEntity.relatedId("financialInstitutionMaaPoa").eq(financialInstitutionMaaPoaId));

		return getAuditQueryResults(null, OrderEnum.DESC, query);
	}

	@SuppressWarnings("unchecked")
	private List<AuditQueryResult> getAuditQueryResults(String propertyName, OrderEnum order, AuditQuery query) {
		if(!VbsUtil.isNullorEmpty(propertyName)){
			query.add(AuditEntity.property(propertyName).hasChanged());
		}

		if(order == null || order.equals(OrderEnum.DESC)){
			query.addOrder(AuditEntity.revisionNumber().desc());
		}else{
			query.addOrder(AuditEntity.revisionNumber().asc());
		}
		List<Object[]> results = query.getResultList();

		return  results.stream().map(AuditQueryResult::new).collect(Collectors.toList());
	}

	private List<ActivityDTO> compareSecurityRevisions(AuditQueryResult a, AuditQueryResult b){
		List<ActivityDTO> ret = new ArrayList<>();

		SecurityEntity curr = (SecurityEntity) a.getEntity();
		SecurityEntity prev = (b != null) ? (SecurityEntity) b.getEntity() : null;

		List<ActivityConfig> activityFields = new ArrayList<>();

		if(curr.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_SURETY_BOND) == 0) {
			activityFields.add(new ActivityConfig("jointBond", "TWC_JOINT_BOND_IND", "Joint Bond Arrangement", "Joint Bond Arrangement Change"));
			if (curr.isJointBond()) {
				activityFields.add(new ActivityConfig("demandFirst", "TWC_DEMAND_FIRST", "Demand Security First", "Demand Security First Change"));
				activityFields.add(new ActivityConfig("releaseFirst", "TWC_RELEASE_FIRST", "Release Security First", "Release Security First Change"));
			}
		}
		activityFields.add(new ActivityConfig("status", "DTA_STATUS", "DTA Status", "DTA Status Changed"));
		activityFields.add(new ActivityConfig("dtaAccountNumber", "TWC_DTA_ACCT_NUM", "DTA Account Number", "DTA Account Number Changed"));
		activityFields.add(new ActivityConfig("pefAccountNumber", "TWC_PEF_ACCT_NUM", "PEF Account Number", "PEF Account Number Changed"));
		activityFields.add(new ActivityConfig("instrumentNumber", "TWC_INSTRUMENT_NBR", "Instrument Number", "Security Instrument Number Change"));
		activityFields.add(new ActivityConfig("securityPurpose", "TWC_SEC_PURPOSE", "Security Purpose", "Security Purpose Change"));
		activityFields.add(new ActivityConfig("securityType", "TWC_SEC_TYPE", "Security Type", "Security Type Change"));

		if (curr.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) == 0) {
			activityFields.add(new ActivityConfig("ppsaFileNumber", "TWC_SEC_PPSA_FILE_NUMBER", "PPSA_FILE_NUMBER", "PPSA File Number Change"));
		}

		if(!this.currentAmountHasReleaseRevision(a.getTracking().getId())) {
			activityFields.add(new ActivityConfig("currentAmount", "TWC_CURRENT_AMT", "Current Amount", "Security Current Amount Changed"));
		}
		activityFields.add(new ActivityConfig("originalAmount", "TWC_ORIG_AMT", "Original Amount", "Security Original Amount Changed"));
		activityFields.add(new ActivityConfig("ppsaNumber", "TWC_PPSA_NUMBER", "PPSA Number", "PPSA Number Change"));
		activityFields.add(new ActivityConfig("ppsaExpiryDate", "TWC_PPSA_EXPIRY", "PPSA Expiry Date", "PPSA Expiry Date Change"));
		activityFields.add(new ActivityConfig("branch", "TWC_FIN_INST_CD", "Security Financial Institution Name Change", "Security Financial Institution Name"));
        activityFields.add(new ActivityConfig("primaryVb", "PRIMARY_IND", "V/B #", "Security Provided by VB Change"));
		if(curr.getSecurityPurpose().getId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) == 0){
			activityFields.add(new ActivityConfig("identityType", "IDENTITY_TYPE", "Identity Type", "Identity Type Changed"));
		}

		for (ActivityConfig activityField : activityFields) {
			this.createActivity(ret, curr.getId(), a.getTracking().getId(), a.getTracking().getRevisionDate(), curr.getUpdateUser(), curr, prev, activityField);
		}
		return ret;
	}

	private boolean currentAmountHasReleaseRevision(int revisionId){
		String sql = "SELECT ID FROM RELEASE_AUD WHERE REV = ?1 AND (STATUS = 'COMPLETED' OR STATUS = 'SENT') AND status_MOD = 1";
		Query query = vbsEm.createNativeQuery(sql);
		query.setParameter(1, revisionId);
		return !query.getResultList().isEmpty();
	}

	private void createActivity(List<ActivityDTO> ret, Long id, int trackingId, Date revisionDate, String updateUser, Object a, Object b, ActivityConfig config){
		try {
			for(Method m : a.getClass().getMethods()) {
				if (m.getName().equalsIgnoreCase("get" + config.getPropertyName()) || m.getName().equalsIgnoreCase("is" + config.getPropertyName())) {
					boolean create;
					Object valA = m.invoke(a);
					Object valB = (b != null) ? (m.invoke(b)) : null;
					if(valA == null) {
						create = valB != null;
					}else{
						if (valA.getClass().getSimpleName().contains("Entity")) {
							valA = Hibernate.unproxy(valA);
							if (valB != null) {
								valB = Hibernate.unproxy(valB);
							}
						}
						if (valA.getClass().equals(String.class)) {
							create = (this.stringNotEqual((String) valA, (String) valB));
						} else if (valA.getClass().equals(boolean.class) || valA.getClass().equals(Boolean.class)) {
							create = (valB == null || Boolean.valueOf(valA.toString()).compareTo(Boolean.valueOf(valB.toString())) != 0);
						} else if (valA.getClass().equals(BigDecimal.class)) {
							create = (this.bigDecimalNotEqual((BigDecimal) valA, (BigDecimal) valB));
						} else if (valA.getClass().equals(LocalDateTime.class)) {
							create = (this.localDateTimeNotEqual((LocalDateTime) valA, (LocalDateTime) valB));
						} else if (valA.getClass().equals(YesNoEnum.class)) {
							create = (this.yesNoNotEqual((YesNoEnum) valA, (YesNoEnum) valB));
						} else if (valA.getClass().equals(BranchEntity.class)) {
							create = (this.branchFINotEqual((BranchEntity) valA, (BranchEntity) valB));
                        } else if (valA.getClass().equals(ContactEntity.class)) {
                            create = (this.contactNotEqual((ContactEntity) valA, (ContactEntity) valB));
						} else {
							create = (!valA.equals(valB));
							if(valA instanceof SecurityStatusEntity){
								if(((SecurityStatusEntity) valA).getId().compareTo(VbsConstants.SECURITY_STATUS_PENDING_WITH_FEE) == 0){
									ActivityDTO activity = new ActivityDTO(id, "TWC_DTA_SETUP", trackingId, revisionDate, "DTA Setup in Progress", "DTA Setup in Progress", updateUser, "", "");
									ret.add(activity);
								}else if(((SecurityStatusEntity) valA).getId().compareTo(VbsConstants.SECURITY_STATUS_TERMINATED) == 0){
									ActivityDTO activity = new ActivityDTO(id, "TWC_DTA_COLLAPSE", trackingId, revisionDate, "DTA Collapse in Progress", "DTA Collapse in Progress", updateUser, "", "");
									ret.add(activity);
								}
							}
						}
					}

					if (create) {
						ActivityDTO activity = new ActivityDTO(id, config.getActivityField(), trackingId, revisionDate, config.getActivityName(), config.getActivitySubject(), updateUser, this.getObjectValueAsString(valB), this.getObjectValueAsString(valA));
						ret.add(activity);
					}
					break;
				}
			}
		}catch(IllegalAccessException | InvocationTargetException | NullPointerException e){
			LoggerUtil.logError(this.getClass(), "Property Issue", e);
		}
	}

	private String getObjectValueAsString(Object o){
		if(o != null) {
			if (o.getClass().equals(String.class)) {
				return (String) o;
			} else if (o.getClass().equals(boolean.class) || o.getClass().equals(Boolean.class)) {
				return ((boolean) o) ? "Yes" : "No";
			} else if (o.getClass().equals(BigDecimal.class)) {
				return VbsUtil.getCurrencyFormattedString((BigDecimal) o);
			} else if (o.getClass().equals(LocalDateTime.class)) {
				return DateUtil.getDateFormattedShort((LocalDateTime) o);
			} else if (o.getClass().equals(YesNoEnum.class)) {
				return ((YesNoEnum) o).getValue();
			} else if (o.getClass().equals(BranchEntity.class)) {
                Long id = ((BranchEntity) o).getFinancialInstitution().getId();
                return genericDAO.findEntityById(FinancialInstitutionEntity.class, id).getName();
            } else if (o.getClass().equals(ContactEntity.class)) {
			    return ((ContactEntity) o).getCrmContactId();
			} else {
				try {
					return (String) o.getClass().getMethod("getDescription").invoke(o);
				} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException me) {
					return o.toString();
				}
			}
		}else{
			return "";
		}
	}

	private boolean stringNotEqual(String a, String b){
		if((a == null) != (b == null)){
			return true;
		}else if(a != null){
			return !a.equals(b);
		}
		return false;
	}

	private boolean bigDecimalNotEqual(BigDecimal a, BigDecimal b){
		if((a == null) != (b == null)){
			return true;
		}else if(a != null){
			return a.compareTo(b) != 0;
		}
		return false;
	}

	private boolean localDateTimeNotEqual(LocalDateTime a, LocalDateTime b){
		if((a == null) != (b == null)){
			return true;
		}else if(a != null){
			return a.compareTo(b) != 0;
		}
		return false;
	}

	private boolean yesNoNotEqual(YesNoEnum a, YesNoEnum b){
		if((a == null) != (b == null)){
			return true;
		}else if(a != null){
			return !a.equals(b);
		}
		return false;
	}

	private boolean branchFINotEqual(BranchEntity a, BranchEntity b){
		if((a == null) != (b == null)){
			return true;
		}else if(a != null){
			return a.getFinancialInstitution().getId().compareTo(b.getFinancialInstitution().getId()) != 0;
		}
		return false;
	}

    private boolean contactNotEqual(ContactEntity a, ContactEntity b){
        if((a == null) != (b == null)){
            return true;
        }else if(a != null){
            return a.getId().compareTo(b.getId()) != 0;
        }
        return false;
    }

	private boolean notEqual(Object a, Object b){
		return (a == null) != (b == null) || a != null && !a.equals(b);
	}

	private class ActivityConfig {
		private String propertyName;
		private String activityField;
		private String activityName;
		private String activitySubject;

		public ActivityConfig(String propertyName, String activityField, String activityName, String activitySubject) {
			this.activityField = activityField;
			this.propertyName = propertyName;
			this.activityName = activityName;
			this.activitySubject = activitySubject;
		}

		public String getPropertyName() {
			return propertyName;
		}

		public String getActivityField() {
			return activityField;
		}

		public String getActivityName() {
			return activityName;
		}

		public String getActivitySubject() {
			return activitySubject;
		}
	}


	private class PoolTracking {

		private Long poolId;
		private Date from;
		private Date to;
		private int revId;

		PoolTracking(Long poolId, Date from, Date to, int revId){
		    this.poolId = poolId;
		    this.from = from;
		    this.to = to;
		    this.revId = revId;
        }

		public Long getPoolId() {
			return poolId;
		}

		public void setPoolId(Long poolId) {
			this.poolId = poolId;
		}

		public Date getFrom() {
			return from;
		}

		public void setFrom(Date from) {
			this.from = from;
		}

		public Date getTo() {
			return to;
		}

		public void setTo(Date to) {
			this.to = to;
		}

		public int getRevId() {
			return revId;
		}

		public void setRevId(int revId) {
			this.revId = revId;
		}
	}

	@Override
	public String getPrimaryVbNumberForSecurityAtDate(Long securityId, LocalDate date) {
		Number revisionNumber = this.getRevisionAtDateTime(date.atStartOfDay());
		return getSecurityEntityAtDate(securityId, revisionNumber).getPrimaryVb().getCrmContactId();
	}

	@Override
	public Number getRevisionAtDateTime(LocalDateTime dateTime){
		AuditReader reader = AuditReaderFactory.get(vbsEm);
		return reader.getRevisionNumberForDate(DateUtil.getDateFromLocalDateTime(dateTime));
	}

	@Override
	public BigDecimal getSecurityCurrentAmountForSecurityAtRevision(Long securityId, Number revisionNumber) {
		return getSecurityEntityAtDate(securityId, revisionNumber).getCurrentAmount();
	}

	@Override
	public List<HistoryFieldDTO> getSecurityCommentHistory(Long securityId) {
		List<AuditQueryResult> history = this.getSecurityRevisions(securityId, "comment", false, OrderEnum.DESC);
		List<HistoryFieldDTO> ret = new ArrayList<>();
		history.forEach(h -> {
			HistoryFieldDTO hDTO = new HistoryFieldDTO();
			hDTO.setFieldName("comment");
			SecurityEntity sec = ((SecurityEntity) h.getEntity());
			hDTO.setFieldValue(sec.getComment());
			hDTO.setUpdateTime(sec.getUpdateDate());
			hDTO.setUpdateUser(sec.getUpdateUser());
			ret.add(hDTO);
		});
		return ret;
	}

	private SecurityEntity getSecurityEntityAtDate(Long securityId, Number revisionNumber) {
		AuditReader reader = AuditReaderFactory.get(vbsEm);
		AuditQuery query = reader.createQuery().forEntitiesAtRevision(SecurityEntity.class, revisionNumber);
		query.add(AuditEntity.id().eq(securityId));
		return (SecurityEntity) query.getSingleResult();
	}

	private List<ActivityDTO> getMonthlyReportActivities(Long securityId){
		List<ActivityDTO> ret = new ArrayList<>();
		List<MonthlyReportEntity> monthlyReportEntities = getMonthlyReportsBySecurity(securityId);
		for (MonthlyReportEntity monthlyReportEntity : monthlyReportEntities) {
			AuditReader reader = AuditReaderFactory.get(vbsEm);
			AuditQuery monthlyReportAuditQuery = reader.createQuery().forRevisionsOfEntity(MonthlyReportEntity.class, false, true)
					.add(AuditEntity.property("securityId").eq(monthlyReportEntity.getSecurityId()))
					.add(AuditEntity.property("reportYear").eq(monthlyReportEntity.getReportYear()))
					.add(AuditEntity.property("reportMonth").eq(monthlyReportEntity.getReportMonth()))
					.addOrder(AuditEntity.revisionNumber().desc());
			List<Object[]> monthlyReportAuditResults = monthlyReportAuditQuery.getResultList();
			List<AuditQueryResult> monthlyReportRevisions = monthlyReportAuditResults.stream().map(AuditQueryResult::new).collect(Collectors.toList());
			for (int i = 0; i < monthlyReportRevisions.size(); i++) {
				MonthlyReportEntity monthlyReportCurr = (MonthlyReportEntity) monthlyReportRevisions.get(i).getEntity();
				if (i == monthlyReportRevisions.size() - 1) {
					ret.add(new ActivityDTO(securityId, "TWC_DTA_MONTHLY_REPORT",
							monthlyReportRevisions.get(i).getTracking().getId(), monthlyReportRevisions.get(i).getTracking().getRevisionDate(),
							"DTA Monthly Report", "DTA Monthly Report Added",
							monthlyReportCurr.getUpdateUser(), "",
							monthlyReportCurr.getReportYear() + " - " + monthlyReportCurr.getReportMonth() + " : " + monthlyReportCurr.getSecurityDeposit().getDescription()));
				} else {
					MonthlyReportEntity monthlyReportPrev = (MonthlyReportEntity) monthlyReportRevisions.get(i + 1).getEntity();
					if (notEqual(monthlyReportCurr.getSecurityDeposit().getId(), monthlyReportPrev.getSecurityDeposit().getId())) {
						ret.add(new ActivityDTO(securityId, "TWC_DTA_MONTHLY_REPORT",
								monthlyReportRevisions.get(i).getTracking().getId(), monthlyReportRevisions.get(i).getTracking().getRevisionDate(),
								"DTA Monthly Report", "DTA Monthly Report Changed",
								monthlyReportCurr.getUpdateUser(),
								monthlyReportPrev.getReportYear() + " - " + monthlyReportPrev.getReportMonth() + " : " + monthlyReportPrev.getSecurityDeposit().getDescription(),
								monthlyReportCurr.getReportYear() + " - " + monthlyReportCurr.getReportMonth() + " : " + monthlyReportCurr.getSecurityDeposit().getDescription()));
					}
				}
			}
		}
		return ret;
	}

	private List<MonthlyReportEntity> getMonthlyReportsBySecurity(Long securityId) {
		TypedQuery<MonthlyReportEntity> query = vbsEm.createNamedQuery(MonthlyReportEntity.SELECT_MONTHLY_REPORT_BY_SECURITY_ID, MonthlyReportEntity.class);
		query.setParameter(1, securityId.intValue());
		return query.getResultList();
	}
}
