/* 
 * 
 * JmsTransactionLogDAO.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.util;

import java.util.List;

import javax.ejb.Local;

import com.tarion.vbs.orm.entity.JmsTransactionLogEntity;

/**
 * JmsTransactionLogDAO class for DB operations on Jms Transaction Log Table
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-04-04
 * @version 1.0
 */
@Local
public interface JmsTransactionLogDAO {
	public List<JmsTransactionLogEntity> findTranscationLogLikeTrackingNumber(String trackingNumberLike);
}
