/* 
 * 
 * EmailTransactionLogDAOImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.util;

import java.time.LocalDateTime;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.tarion.vbs.orm.entity.EmailTransactionLogEntity;

/**
 * EmailTransactionLogDAO class for DB operations on Email Transaction Log Table
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-01-30
 * @version 1.0
 */
@Stateless(mappedName = "ejb/EmailTransactionLogDAO")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class EmailTransactionLogDAOImpl implements EmailTransactionLogDAO {
	
	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;
	
	@EJB
	private GenericDAO genericDAO;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	/** need this method just for new transaction annotation */
	public EmailTransactionLogEntity addEntity(EmailTransactionLogEntity emailTransactionLogEntity, String createUser, LocalDateTime createDate) {
		return (EmailTransactionLogEntity) genericDAO.addEntity(emailTransactionLogEntity, createUser, createDate);
	}
	
}
