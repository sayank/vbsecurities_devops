package com.tarion.vbs.dao.contact;

import java.util.List;

import javax.ejb.Local;

import com.tarion.vbs.common.contact.EscrowAgentSearchParamsDTO;
import com.tarion.vbs.common.contact.EscrowAgnetSearchResultWrapper;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.orm.entity.contact.*;
import com.tarion.vbs.orm.entity.security.VbPoolEntity;

/**
 * ContactDAO class for DB operations on Contact tables
 *
 * @author <A href="shadi.kajevand@tarion.com">Shadi Kajevand</A>
 * @since 2019-01-15
 * @version 1.0
 */

@Local
public interface ContactDAO {

	public EscrowAgnetSearchResultWrapper searchEscrowAgent(EscrowAgentSearchParamsDTO escrowAgentSearchParamsDTO);
	public List<ContactEntity> getEscrowAgentsLike(String escrowAgentName);
	public List<ContactEntity> getEscrowAgentsCrmContactIdBeginsWith(String partial);
	public Long getContactIdByCrmContactId(String crmContactId);

    ContactEntity getContactBySecurityIdContactType(Long securityId, Long contactTypeId);
	ContactEntity getContactBySecurityIdContactSecurityRoleId(Long securityId, Long contactSecurityRoleId);

    public List<ContactEntity> getLawyersForEscrowAgent(String escrowAgentCrmContactId);
	public List<ContactEntity> getAssistantsForEscrowAgent(String escrowAgentCrmContactId);
	public ContactEntity getContactByCrmContactId(String crmContactId);
	public ContactEntity getContactByCrmContactIdAndRole(String crmContactId, long roleId);
	public ContactSecurityEntity getContactSecurityBySecurityIdContactSecurityRoleId(Long securityId,
			Long contactSecurityRoleId);

	public List<ContactEntity> getAllVbsForVbNumberLike(String vbNumberPrefix);
	public LicenseStatusEntity getLicenseStatusByCrmContactId(String crmContactId);
	public List<VbPoolEntity> getDeletedVbsForPoolId(Long poolId);
    List<VbPoolEntity> getVbsForPoolId(Long poolId);

	List<String> getVbNumbersForPoolId(Long poolId);

	List<VbPoolEntity> getAllVbsEvenDeletedForPoolId(Long poolId);

	VbPoolEntity getVbPool(Long poolId, String vbNumber) throws VbsCheckedException;

	public long getNumberOfVbsBySecurityId(Long securityId);
	
	List<ContactRelationshipEntity> getAllRelationshipsForContact(Long contactId);
	ContactRelationshipEntity getRelationshipByCrmRelId(String crmRelId);

}
