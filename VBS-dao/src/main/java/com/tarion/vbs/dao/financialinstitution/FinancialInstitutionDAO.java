/* 
 * 
 * FinancialInstitutionDAO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.financialinstitution;

import java.util.List;

import javax.ejb.Local;

import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionAmountsDTO;
import com.tarion.vbs.orm.entity.financialinstitution.BranchEntity;
import com.tarion.vbs.orm.entity.financialinstitution.ContactFinancialInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionAlertEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionMaaPoaEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionMaaPoaInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionSigningOfficerEntity;

/**
 * FinancialInstitutionDAO class for DB operations on Financial Instituion Tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-16
 * @version 1.0
 */
@Local
public interface FinancialInstitutionDAO {
	public List<FinancialInstitutionEntity> getAllFinancialInstitutions();
	public List<BranchEntity> getAllBranches();
	public List<FinancialInstitutionEntity> getFinancialInstitutionsForNameLike(String name, int maxResults);
	public List<FinancialInstitutionEntity> getFinancialInstitutionsForIdLike(String id, int maxResults);
	public List<BranchEntity> getBranchesForFinancialInstitution(Long financialInstitutionId);
	public List<FinancialInstitutionEntity> getFinancialInstitutionsByCodeOrName(String code, Long codeTo, String codeType, String name, String nameTo, String nameType, Long financialInstitutionTypeId);
	public FinancialInstitutionAmountsDTO getDirectFinancialInstitutionAmounts(Long financialInstitutionId, boolean includePEFDTA);
    public FinancialInstitutionAmountsDTO getMaaPoaFinancialInstitutionAmounts(Long financialInstitutionId, boolean includePEFDTA);
    public List<ContactFinancialInstitutionEntity> getContactsForFinancialInstitution(Long financialInstitutionId);
	public List<FinancialInstitutionSigningOfficerEntity> getSigningOfficersForFinancialInstitutionMaaPoa(Long financialInstitutionMaaPoaId);
	public List<FinancialInstitutionMaaPoaEntity> getMaaPoasForFinancialInstitution(Long financialInstitutionId);
	public List<FinancialInstitutionMaaPoaInstitutionEntity> getMaaPoaInstitutionsForMaaPoa(Long maaPoaId);
	public List<FinancialInstitutionAlertEntity> getAlertsForFinancialInstitution(Long financialInstitutionId);

    Long getFinancialInstitutionTypeId(Long financialInstitutionId);
}
