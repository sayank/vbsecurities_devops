/* 
 * 
 * PropertyDAO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.util;

import java.util.List;

import javax.ejb.Local;

import com.tarion.vbs.orm.entity.util.PropertiesEntity;


/**
 * PropertyDAO class for Loading of all database properties from VBS_CONFIG table
 *
 */
@Local
public interface PropertyDAO {
	List<PropertiesEntity> findAllVBApplicationsProperties();
}
