package com.tarion.vbs.dao.alert;

import java.util.List;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.alert.AlertEntity;

/**
 * AlertDAO class for DB operations on Alert tables
 *
 * @author <A href="djordje.przulj@tarion.com">Djordje Przulj</A>
 * @since 2019-04-16
 * @version 1.0
 */
@Stateless(mappedName = "ejb/AlertDAO")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class AlertDAOImpl implements AlertDAO {

	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;

	@Override
	public List<AlertEntity> getAlertsBySecurityId(Long securityId) {
		TypedQuery<AlertEntity> query = vbsEm.createNamedQuery(AlertEntity.SELECT_ALERT_FOR_SECURITY_ID, AlertEntity.class);
		query.setParameter(1, securityId);
		return query.getResultList();
	}
	
	@Override
	public List<AlertEntity> getAlertsBySecurityIdExceptNoteType(Long securityId) {
		TypedQuery<AlertEntity> query = vbsEm.createNamedQuery(AlertEntity.SELECT_ALERTS_FOR_SECURITY_ID_EXCEPT_NOTE, AlertEntity.class);
		query.setParameter(1, securityId);
		return query.getResultList();
	}
	
}
