/*
 *
 * LookupDAOImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved.
 *
 */
package com.tarion.vbs.dao.util;

import com.tarion.vbs.orm.entity.alert.AlertTypeEntity;
import com.tarion.vbs.orm.entity.contact.ContactSecurityRoleEntity;
import com.tarion.vbs.orm.entity.contact.EscrowLicenseApplicationStatusEntity;
import com.tarion.vbs.orm.entity.contact.LicenseStatusEntity;
import com.tarion.vbs.orm.entity.correspondence.CorrespondenceTemplateEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionTypeEntity;
import com.tarion.vbs.orm.entity.release.ReasonEntity;
import com.tarion.vbs.orm.entity.release.RejectReasonEntity;
import com.tarion.vbs.orm.entity.release.ReleaseTypeEntity;
import com.tarion.vbs.orm.entity.release.WarrantyServicesRecommendationTypeEntity;
import com.tarion.vbs.orm.entity.security.*;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.*;
import java.util.List;
import java.util.Optional;

/**
 * LookupDAO class for getting all lookup data
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-09
 * @version 1.0
 */
@Stateless(mappedName = "ejb/LookupDAO")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class LookupDAOImpl implements LookupDAO {

	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;

	private List<ReleaseTypeEntity> releaseTypes;
	private List<ReasonEntity> reasons;
	private List<RejectReasonEntity> rejectReasons;
	private List<SecurityStatusEntity> securityStatuses;
	private List<AlertTypeEntity> alertTypes;
	private List<SecurityPurposeEntity> securityPurposes;
	private List<SecurityTypeEntity> securityTypes;
	private List<IdentityTypeEntity> identityTypes;
	private List<SecurityDepositEntity> securityDeposits;
	private List<VbDeleteReasonEntity> vbDeleteReasons;
	private List<CorrespondenceTemplateEntity> correspondenceTemplates;
	private List<ContactSecurityRoleEntity> contactSecurityRoles;

	@SuppressWarnings("unchecked")
	@Override
	public List<ReleaseTypeEntity> getAllReleaseTypes() {
		if(this.releaseTypes == null) {
			Query query = vbsEm.createNamedQuery(ReleaseTypeEntity.SELECT_ALL_RELEASE_TYPES);
			this.releaseTypes = query.getResultList();
		}
		return this.releaseTypes;
	}

	@Override
	public ReleaseTypeEntity getReleaseTypeByReleaseTypeId(Long releaseTypeId){
		if(releaseTypeId == null){return null;}
		Optional<ReleaseTypeEntity> ret = this.getAllReleaseTypes().stream().filter(r -> r.getId().equals(releaseTypeId)).findFirst();
		return ret.orElse(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SecurityPurposeEntity> getAllSecurityPurposes() {
		if(this.securityPurposes == null) {
			Query query = vbsEm.createNamedQuery(SecurityPurposeEntity.SELECT_ALL_SECURITY_PURPOSE);
			this.securityPurposes = query.getResultList();
		}
		return this.securityPurposes;
	}

	@Override
	public SecurityPurposeEntity getSecurityPurposeById(Long id){
		if(id == null){return null;}
		Optional<SecurityPurposeEntity> ret = this.getAllSecurityPurposes().stream().filter(r -> r.getId().equals(id)).findFirst();
		return ret.orElse(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SecurityTypeEntity> getAllSecurityTypes() {
		if(this.securityTypes == null) {
			Query query = vbsEm.createNamedQuery(SecurityTypeEntity.SELECT_ALL_SECURITY_TYPE);
			this.securityTypes = query.getResultList();
		}
		return this.securityTypes;
	}

	@Override
	public SecurityTypeEntity getSecurityTypeById(Long id){
		if(id == null){return null;}
		Optional<SecurityTypeEntity> ret = this.getAllSecurityTypes().stream().filter(r -> r.getId().equals(id)).findFirst();
		return ret.orElse(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VbDeleteReasonEntity> getAllVbDeleteReasons() {
		if(this.vbDeleteReasons == null) {
			Query query = vbsEm.createNamedQuery(VbDeleteReasonEntity.SELECT_ALL_VB_DELETE_REASON);
			this.vbDeleteReasons = query.getResultList();
		}
		return this.vbDeleteReasons;
	}

	@Override
	public VbDeleteReasonEntity getVbDeleteReasonById(Long id){
		if(id == null){return null;}
		Optional<VbDeleteReasonEntity> ret = this.getAllVbDeleteReasons().stream().filter(r -> r.getId().equals(id)).findFirst();
		return ret.orElse(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FinancialInstitutionTypeEntity> getAllFinancialInstitutionTypes() {
		Query query = vbsEm.createNamedQuery(FinancialInstitutionTypeEntity.SELECT_ALL_FINANCIAL_INSTITUTION_TYPE);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LicenseStatusEntity> getAllLicenseStatuses() {
		Query query = vbsEm.createNamedQuery(LicenseStatusEntity.SELECT_ALL_LICENSE_STATUS);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SecurityStatusEntity> getAllSecurityStatuses() {
		if(this.securityStatuses == null) {
			Query query = vbsEm.createNamedQuery(SecurityStatusEntity.SELECT_ALL_SECURITY_STATUS);
			this.securityStatuses = query.getResultList();
		}
		return this.securityStatuses;
	}

	@Override
	public SecurityStatusEntity getSecurityStatusById(Long statusId){
		if(statusId == null){return null;}
		Optional<SecurityStatusEntity> ret = this.getAllSecurityStatuses().stream().filter(r -> r.getId().equals(statusId)).findFirst();
		return ret.orElse(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ReasonEntity> getAllReasons() {
		if(this.reasons == null) {
			Query query = vbsEm.createNamedQuery(ReasonEntity.SELECT_ALL_REASONS);
			this.reasons = query.getResultList();
		}
		return this.reasons;
	}

	@Override
	public ReasonEntity getReasonByReasonId(Long reasonId){
		if(reasonId == null){return null;}
		Optional<ReasonEntity> ret = this.getAllReasons().stream().filter(r -> r.getId().compareTo(reasonId) == 0).findFirst();
		return ret.orElse(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RejectReasonEntity> getAllRejectReasons() {
		if(this.rejectReasons == null) {
			Query query = vbsEm.createNamedQuery(RejectReasonEntity.SELECT_ALL_REJECT_REASONS);
			this.rejectReasons = query.getResultList();
		}
		return this.rejectReasons;
	}

	@Override
	public RejectReasonEntity getRejectReasonById(Long id){
		if(id == null){return null;}
		Optional<RejectReasonEntity> ret = this.getAllRejectReasons().stream().filter(r -> r.getId().compareTo(id) == 0).findFirst();
		return ret.orElse(null);
	}

	@Override
	public LicenseStatusEntity getLicenseStatusForName(String name) {
		if (name == null) {
			return null;
		}
		TypedQuery<LicenseStatusEntity> query = vbsEm.createNamedQuery(LicenseStatusEntity.SELECT_LICENSE_STATUS_FOR_NAME, LicenseStatusEntity.class);
		List<LicenseStatusEntity> l = query.setParameter(1, name.toUpperCase()).getResultList();
		if(l.isEmpty()){
			return null;
		}else if(l.size() > 1){
			throw new NonUniqueResultException();
		}
		return l.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EscrowLicenseApplicationStatusEntity> getAllEscrowLicenseApplicationStatuses() {
		Query query = vbsEm.createNamedQuery(EscrowLicenseApplicationStatusEntity.SELECT_ALL_ESCROW_LICENSE_APPLICATION_STATUS);
		return query.getResultList();
	}

	@Override
	public EscrowLicenseApplicationStatusEntity getEscrowLicenseApplicationStatusEntity(String name) {
		TypedQuery<EscrowLicenseApplicationStatusEntity> query = vbsEm.createNamedQuery(EscrowLicenseApplicationStatusEntity.SELECT_ESCROW_LICENSE_APPLICATION_STATUS_FOR_NAME, EscrowLicenseApplicationStatusEntity.class);
		List<EscrowLicenseApplicationStatusEntity> l = query.setParameter(1, name).getResultList();
		if(l.isEmpty()){
			return null;
		}else if(l.size() > 1){
			throw new NonUniqueResultException();
		}
		return l.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<IdentityTypeEntity> getAllIdentityTypes() {
		if(this.identityTypes == null) {
			Query query = vbsEm.createNamedQuery(IdentityTypeEntity.SELECT_ALL_IDENTITY_TYPE);
			this.identityTypes = query.getResultList();
		}
		return this.identityTypes;
	}

	@Override
	public IdentityTypeEntity getIdentityTypeById(Long id){
		if(id == null){return null;}
		Optional<IdentityTypeEntity> ret = this.getAllIdentityTypes().stream().filter(r -> r.getId().compareTo(id) == 0).findFirst();
		return ret.orElse(null);
	}

	@Override
	public WarrantyServicesRecommendationTypeEntity getWarrantyServicesRecommendationTypeEntity(String name) {
		TypedQuery<WarrantyServicesRecommendationTypeEntity> query = vbsEm.createNamedQuery(WarrantyServicesRecommendationTypeEntity.SELECT_WARRANTY_SERVICES_RECOMMENDATIONS_FOR_NAME, WarrantyServicesRecommendationTypeEntity.class);
		List<WarrantyServicesRecommendationTypeEntity> l = query.setParameter(1, name).getResultList();
		if(l.isEmpty()){
			return null;
		}else if(l.size() > 1){
			throw new NonUniqueResultException();
		}
		return l.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SecurityDepositEntity> getAllSecurityDeposit() {
		if(this.securityDeposits == null) {
			Query query = vbsEm.createNamedQuery(SecurityDepositEntity.SELECT_ALL_SECURITY_DEPOSIT);
			this.securityDeposits = query.getResultList();
		}
		return this.securityDeposits;
	}

	@Override
	public SecurityDepositEntity getSecurityDepositById(Long id){
		if(id == null){return null;}
		Optional<SecurityDepositEntity> ret = this.getAllSecurityDeposit().stream().filter(r -> r.getId().compareTo(id) == 0).findFirst();
		return ret.orElse(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AlertTypeEntity> getAllAlertTypes() {
		if(this.alertTypes == null) {
			Query query = vbsEm.createNamedQuery(AlertTypeEntity.SELECT_ALL_ALERT_TYPE);
			this.alertTypes = query.getResultList();
		}
		return this.alertTypes;
	}

	@Override
	public AlertTypeEntity getAlertTypeById(Long id){
		if(id == null){return null;}
		Optional<AlertTypeEntity> ret = this.getAllAlertTypes().stream().filter(a -> a.getId().compareTo(id) == 0).findFirst();
		return ret.orElse(null);
	}

	@Override
	public List<CorrespondenceTemplateEntity> getAllTemplateTypes(){
		if(this.correspondenceTemplates == null) {
			TypedQuery<CorrespondenceTemplateEntity> query = vbsEm.createNamedQuery(CorrespondenceTemplateEntity.SELECT_ALL_TEMPLATES, CorrespondenceTemplateEntity.class);
			this.correspondenceTemplates = query.getResultList();
		}
		return this.correspondenceTemplates;
	}

	@Override
	public CorrespondenceTemplateEntity getTemplateByTemplateId(Long id){
		if(id == null){return null;}
		Optional<CorrespondenceTemplateEntity> ret = this.getAllTemplateTypes().stream().filter(a -> a.getId().compareTo(id) == 0).findFirst();
		return ret.orElse(null);
	}

	@Override
	public List<ContactSecurityRoleEntity> getContactSecurityRoles(){
		if(this.contactSecurityRoles == null) {
			TypedQuery<ContactSecurityRoleEntity> query = vbsEm.createNamedQuery(ContactSecurityRoleEntity.SELECT_ALL_CONTACT_SECURITY_ROLES, ContactSecurityRoleEntity.class);
			this.contactSecurityRoles = query.getResultList();
		}
		return this.contactSecurityRoles;
	}

	@Override
	public ContactSecurityRoleEntity getContactSecurityRoleById(Long id){
		if(id == null){return null;}
		Optional<ContactSecurityRoleEntity> ret = this.getContactSecurityRoles().stream().filter(a -> a.getId().compareTo(id) == 0).findFirst();
		return ret.orElse(null);
	}
}
