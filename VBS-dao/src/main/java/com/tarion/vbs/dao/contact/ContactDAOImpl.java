package com.tarion.vbs.dao.contact;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.orm.entity.contact.*;
import org.hibernate.jpa.QueryHints;
import org.hibernate.transform.Transformers;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.contact.EscrowAgentSearchParamsDTO;
import com.tarion.vbs.common.contact.EscrowAgentSearchResultDTO;
import com.tarion.vbs.common.contact.EscrowAgnetSearchResultWrapper;
import com.tarion.vbs.common.enums.SearchTypeEnum;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.security.VbPoolEntity;

/**
 * ContactDAO class for DB operations on Contact tables
 *
 * @author <A href="shadi.kajevand@tarion.com">Shadi Kajevand</A>
 * @since 2019-01-15
 * @version 1.0
 */
@Stateless(mappedName = "ejb/ContactDAO")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class ContactDAOImpl implements ContactDAO{

	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;
	
	private static final String CONTACT_ID = "contactId";
	private static final String ESCROW_AGENT_NAME = "escrowAgentName";

	@Override
    public ContactEntity getContactBySecurityIdContactType(Long securityId, Long contactTypeId){
        TypedQuery<ContactEntity> query = vbsEm.createNamedQuery(ContactSecurityEntity.SELECT_ESCROW_AGENT_FOR_SECURITY_ID, ContactEntity.class);
        query.setParameter(1, securityId);
        query.setParameter(2, contactTypeId);
        try{
            return query.getSingleResult();
        }catch(NoResultException e){
            //there should only be 0 or 1 results for this, throw if not.
            return null;
        }
    }

	@Override
	public ContactEntity getContactBySecurityIdContactSecurityRoleId(Long securityId, Long contactSecurityRoleId){
		TypedQuery<ContactEntity> query = vbsEm.createNamedQuery(ContactSecurityEntity.SELECT_CONTACT_BY_SECURITY_ID_CONTACT_SECURITY_ROLE_ID, ContactEntity.class);
		query.setParameter(1, securityId);
		query.setParameter(2, contactSecurityRoleId);
		try{
			return query.getSingleResult();
		}catch(NoResultException e){
			//there should only be 0 or 1 results for this, throw if not.
			return null;
		}
	}

	@Override
	public ContactSecurityEntity getContactSecurityBySecurityIdContactSecurityRoleId(Long securityId, Long contactSecurityRoleId){
		TypedQuery<ContactSecurityEntity> query = vbsEm.createNamedQuery(ContactSecurityEntity.SELECT_CONTACT_SECURITY_BY_SECURITY_ID_CONTACT_SECURITY_ROLE_ID, ContactSecurityEntity.class);
		query.setParameter(1, securityId);
		query.setParameter(2, contactSecurityRoleId);
		try{
			return query.getSingleResult();
		}catch(NoResultException e){
			//there should only be 0 or 1 results for this, throw if not.
			return null;
		}
	}

	@Override
	public List<ContactEntity> getLawyersForEscrowAgent(String escrowAgentCrmContactId) {
		TypedQuery<ContactEntity> query = vbsEm.createNamedQuery(ContactEntity.SELECT_LAWYERS_BY_ESCROW_AGENT_ID, ContactEntity.class);
		query.setParameter(1, escrowAgentCrmContactId);
		query.setParameter(2, DateUtil.getLocalDateTimeNow());
		return query.getResultList();
	}

	@Override
	public ContactEntity getContactByCrmContactIdAndRole(String crmContactId, long roleId) {
		TypedQuery<ContactEntity> query = vbsEm.createNamedQuery(ContactSecurityEntity.SELECT_CONTACT_BY_CRM_CONTACT_ID_AND_ROLE, ContactEntity.class);
		query.setParameter(1, crmContactId);
		query.setParameter(2, roleId);

		List<ContactEntity> result = query.getResultList();
		if(result.isEmpty()) {
			return null;
		}

		return result.get(0);
	}
	
	@Override
	public Long getContactIdByCrmContactId(String crmContactId) {
		TypedQuery<Long> query = vbsEm.createNamedQuery(ContactEntity.SELECT_CONTACT_ID_BY_CRM_CONTACT_ID, Long.class);
		query.setParameter(1, crmContactId);
		return query.getSingleResult();
	}

	@Override
	public LicenseStatusEntity getLicenseStatusByCrmContactId(String crmContactId) {
		TypedQuery<LicenseStatusEntity> query = vbsEm.createNamedQuery(ContactEntity.SELECT_LICENSE_STATUS_BY_CRM_CONTACT_ID, LicenseStatusEntity.class);
		query.setParameter(1, crmContactId);
		List<LicenseStatusEntity> result = query.getResultList();
		if(result.isEmpty()) {
			LoggerUtil.logError(ContactDAOImpl.class, "getLicenseStatusByCrmContactId", "License status not found for: " + crmContactId);
			return null;
		}
		return result.get(0);
	}

	@Override
	public ContactEntity getContactByCrmContactId(String crmContactId) {
		if (crmContactId == null) {
			return null;
		}
		TypedQuery<ContactEntity> query = vbsEm.createNamedQuery(ContactEntity.SELECT_CONTACT_BY_CRM_CONTACT_ID, ContactEntity.class);
		List<ContactEntity> l = query.setParameter(1, crmContactId).getResultList();
		if(l.isEmpty()){
			return null;
		}else if(l.size() > 1){
			throw new NonUniqueResultException();
		}
		return l.get(0);
	}

	
	@Override
	public List<ContactEntity> getAssistantsForEscrowAgent(String escrowAgentCrmContactId) {
		TypedQuery<ContactEntity> query = vbsEm.createNamedQuery(ContactEntity.SELECT_ASSISTANTS_BY_ESCROW_AGENT_ID, ContactEntity.class);
		query.setParameter(1, escrowAgentCrmContactId);
		query.setParameter(2, DateUtil.getLocalDateTimeNow());
		return query.getResultList();
	}

	@Override
	public List<ContactEntity> getEscrowAgentsLike(String escrowAgentName) {
		TypedQuery<ContactEntity> query = vbsEm.createNamedQuery(ContactEntity.SELECT_ALL_ESCROW_AGENTS_LIKE, ContactEntity.class).setMaxResults(VbsConstants.TYPEAHEAD_MAX_RESULTS);
		query.setParameter(1, VbsUtil.getSearchLikePrefixString(escrowAgentName));
		return query.getResultList();
	}

	@Override
	public List<ContactEntity> getEscrowAgentsCrmContactIdBeginsWith(String partial) {
		TypedQuery<ContactEntity> query = vbsEm.createNamedQuery(ContactEntity.SELECT_ESCROW_AGENTS_CRM_CONTACT_ID_BEGINS_WITH, ContactEntity.class).setMaxResults(VbsConstants.TYPEAHEAD_MAX_RESULTS);
		query.setParameter(1, partial+"%");
		return query.getResultList();
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public EscrowAgnetSearchResultWrapper searchEscrowAgent(EscrowAgentSearchParamsDTO escrowAgentSearchParamsDTO){
		EscrowAgnetSearchResultWrapper ret = new EscrowAgnetSearchResultWrapper();
		List<String> queryStrings = new ArrayList<>();
		Map<String, Object> params = new LinkedHashMap<>();
		String sql =
				"SELECT " +
						"	C.ID AS id, " +
						"	C.CRM_CONTACT_ID AS crmContactId, " +
						"	C.COMPANY_NAME AS escrowAgent, " +
						"	C.ALERT AS alert, " +
						"	ls.DESCRIPTION AS escrowAgentLicenceStatus, " +
						"	elas.DESCRIPTION AS licenceApplicationStatus " +
						"FROM " +
						"	CONTACT C LEFT OUTER JOIN " +
						"	LICENSE_STATUS ls ON ls.ID = C.LICENSE_STATUS_ID LEFT OUTER JOIN " +
						"	ESCROW_LICENSE_APPLICATION_STATUS elas ON elas.ID = C.ESCROW_LICENSE_APPLICATION_STATUS_ID " + 
						"WHERE C.CONTACT_TYPE_ID = :contactType "; 

		params.put("contactType", VbsConstants.CONTACT_TYPE_ESCROW_AGENT );
		
		sql = addJpqlEscrowAgentContactId(escrowAgentSearchParamsDTO, sql, queryStrings, params);
		sql = addJpqlEscrowAgentName(escrowAgentSearchParamsDTO, sql, queryStrings, params);
		sql = addJpqlEscrowAgentLicenseStatus(escrowAgentSearchParamsDTO, sql, queryStrings, params);
		sql = addJpqlEscrowAgentLicenseApplicationStatus(escrowAgentSearchParamsDTO, sql, queryStrings, params);
		
		sql += " ORDER BY C.COMPANY_NAME ";

		Query query = vbsEm.createNativeQuery(sql).setMaxResults(VbsConstants.SECURITY_SEARCH_MAX_RESULTS).setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false);
		for(Map.Entry<String, Object> entry : params.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		//Replaced by @FunctionalInterface in Hibernate 6.0, removed in 5.3, both require JPA 2.2 which we cannot use on Weblogic, thus not an issue.
		ret.setEscrowAgents(query.unwrap( org.hibernate.query.NativeQuery.class).setResultTransformer(Transformers.aliasToBean( EscrowAgentSearchResultDTO.class)).getResultList());
		if(!queryStrings.isEmpty()) {ret.setQueryString(String.join(" AND ", queryStrings));}
		return ret;

	}
	
	private String addJpqlEscrowAgentLicenseApplicationStatus(EscrowAgentSearchParamsDTO escrowAgentSearchParamsDTO, String inputSql, List<String> queryStrings, Map<String, Object> params) {
		String sql = inputSql;
		if(escrowAgentSearchParamsDTO.getLicensApplicationStatusId() != null) {
			if(escrowAgentSearchParamsDTO.getLicensApplicationStatusSearchType().equals(SearchTypeEnum.EQUALS)) {
				sql += " AND C.ESCROW_LICENSE_APPLICATION_STATUS_ID = :licensApplicationStatusId ";
				params.put("licensApplicationStatusId", escrowAgentSearchParamsDTO.getLicensApplicationStatusId());
				queryStrings.add("licensApplicationStatusId = " + escrowAgentSearchParamsDTO.getLicensApplicationStatusId());
			} else if(escrowAgentSearchParamsDTO.getLicensApplicationStatusSearchType().equals(SearchTypeEnum.NOT_EQUAL)) {
				sql += " AND C.ESCROW_LICENSE_APPLICATION_STATUS_ID <> :licensApplicationStatusId ";
				params.put("licensApplicationStatusId", escrowAgentSearchParamsDTO.getLicensApplicationStatusId());
				queryStrings.add("licensApplicationStatusId = " + escrowAgentSearchParamsDTO.getLicensApplicationStatusSearchType());
			}
		}
		return sql;
	}
	
	private String addJpqlEscrowAgentLicenseStatus(EscrowAgentSearchParamsDTO escrowAgentSearchParamsDTO, String inputSql, List<String> queryStrings, Map<String, Object> params) {
		String sql = inputSql;
		if(escrowAgentSearchParamsDTO.getEscrowAgentLicensStatusId() != null) {
			if(escrowAgentSearchParamsDTO.getEscrowAgentLicensStatusSearchType().equals(SearchTypeEnum.EQUALS)) {
				sql += " AND C.LICENSE_STATUS_ID = :escrowAgentLicensStatusId ";
				params.put("escrowAgentLicensStatusId", escrowAgentSearchParamsDTO.getEscrowAgentLicensStatusId());
				queryStrings.add("escrowAgentLicensStatusId = " + escrowAgentSearchParamsDTO.getEscrowAgentName());
			} else if(escrowAgentSearchParamsDTO.getEscrowAgentLicensStatusSearchType().equals(SearchTypeEnum.NOT_EQUAL)) {
				sql += " AND C.LICENSE_STATUS_ID <> :escrowAgentLicensStatusId ";
				params.put("escrowAgentLicensStatusId", escrowAgentSearchParamsDTO.getEscrowAgentLicensStatusId());
				queryStrings.add("escrowAgentName = " + escrowAgentSearchParamsDTO.getEscrowAgentName());
			}
		}
		return sql;
	}
	
	private String addJpqlEscrowAgentName(EscrowAgentSearchParamsDTO escrowAgentSearchParamsDTO, String inputSql, List<String> queryStrings, Map<String, Object> params) {
		String sql = inputSql;
		if(isNotEmpty(escrowAgentSearchParamsDTO.getEscrowAgentName())) {
			if(escrowAgentSearchParamsDTO.getEscrowAgentNameSearchType().equals(SearchTypeEnum.EQUALS)) {
				sql += " AND UPPER(C.COMPANY_NAME) = UPPER(:escrowAgentName) ";
				params.put(ESCROW_AGENT_NAME, escrowAgentSearchParamsDTO.getEscrowAgentName());
				queryStrings.add("escrowAgentName = " + escrowAgentSearchParamsDTO.getEscrowAgentName());
			} else if(escrowAgentSearchParamsDTO.getEscrowAgentNameSearchType().equals(SearchTypeEnum.BEGIN_WITH)) {
				sql += " AND C.COMPANY_NAME LIKE :escrowAgentName ";
				params.put(ESCROW_AGENT_NAME, escrowAgentSearchParamsDTO.getEscrowAgentName() + "%");
				queryStrings.add("escrowAgentName begins with " + escrowAgentSearchParamsDTO.getEscrowAgentName());
			} else if(escrowAgentSearchParamsDTO.getEscrowAgentNameSearchType().equals(SearchTypeEnum.CONTAINS)){
				sql += " AND C.COMPANY_NAME LIKE :escrowAgentName ";
				params.put(ESCROW_AGENT_NAME, "%" + escrowAgentSearchParamsDTO.getEscrowAgentName() + "%");
				queryStrings.add("escrowAgentName contains " + escrowAgentSearchParamsDTO.getEscrowAgentName());
			}
		}
		return sql;
	}
	
	private String addJpqlEscrowAgentContactId(EscrowAgentSearchParamsDTO escrowAgentSearchParamsDTO, String inputSql, List<String> queryStrings, Map<String, Object> params) {
		String sql = inputSql;
		if(isNotEmpty(escrowAgentSearchParamsDTO.getContactId())) {
			if(escrowAgentSearchParamsDTO.getContactIdSearchType().equals(SearchTypeEnum.EQUALS)) {
				sql += " AND C.CRM_CONTACT_ID = :contactId ";
				params.put(CONTACT_ID, escrowAgentSearchParamsDTO.getContactId());
				queryStrings.add("ContactId = " + escrowAgentSearchParamsDTO.getContactId());
			}else if(escrowAgentSearchParamsDTO.getContactIdSearchType().equals(SearchTypeEnum.BEGIN_WITH)) {
				sql += " AND C.CRM_CONTACT_ID LIKE :contactId ";
				params.put(CONTACT_ID, escrowAgentSearchParamsDTO.getContactId()+"%");
				queryStrings.add("Contact Id begins with " + escrowAgentSearchParamsDTO.getContactId());
			}else if(escrowAgentSearchParamsDTO.getContactIdSearchType().equals(SearchTypeEnum.CONTAINS)) {
				sql += " AND C.CRM_CONTACT_ID LIKE :contactId ";
				params.put(CONTACT_ID, "%"+escrowAgentSearchParamsDTO.getContactId()+"%");
				queryStrings.add("Contact Id contains " + escrowAgentSearchParamsDTO.getContactId());
			}else if(escrowAgentSearchParamsDTO.getContactIdSearchType().equals(SearchTypeEnum.LESS_THAN)){
				sql += " AND C.CRM_CONTACT_ID < :contactId ";
				params.put(CONTACT_ID, escrowAgentSearchParamsDTO.getContactId());
				queryStrings.add("Contact Id < " + escrowAgentSearchParamsDTO.getContactId());
			}else if(escrowAgentSearchParamsDTO.getContactIdSearchType().equals(SearchTypeEnum.GREATER_THAN)){
				sql += " AND C.CRM_CONTACT_ID > :contactId ";
				params.put(CONTACT_ID, escrowAgentSearchParamsDTO.getContactId());
				queryStrings.add("Contact Id > " + escrowAgentSearchParamsDTO.getContactId());
			}else if(escrowAgentSearchParamsDTO.getContactIdSearchType() != null && escrowAgentSearchParamsDTO.getContactIdSearchType().equals(SearchTypeEnum.BETWEEN)){
				sql += " AND C.CRM_CONTACT_ID BETWEEN :contactId AND :contactIdTo ";
				params.put(CONTACT_ID,escrowAgentSearchParamsDTO.getContactId());
				params.put("contactIdTo", escrowAgentSearchParamsDTO.getContactIdTo());
				queryStrings.add("Contact Id between " + escrowAgentSearchParamsDTO.getContactId() + " & " + escrowAgentSearchParamsDTO.getContactIdTo());
			}else if(escrowAgentSearchParamsDTO.getContactIdSearchType().equals(SearchTypeEnum.EQUALS_GREATER_THAN)){
				sql += " AND C.CRM_CONTACT_ID >= :contactId ";
				params.put(CONTACT_ID, escrowAgentSearchParamsDTO.getContactId());
				queryStrings.add("Contact Id >= " + escrowAgentSearchParamsDTO.getContactId());
			}else if(escrowAgentSearchParamsDTO.getContactIdSearchType().equals(SearchTypeEnum.EQUALS_LESS_THAN)){
				sql += " AND C.CRM_CONTACT_ID <= :contactId ";
				params.put(CONTACT_ID, escrowAgentSearchParamsDTO.getContactId());
				queryStrings.add("Contact Id <= " + escrowAgentSearchParamsDTO.getContactId());
			}else if(escrowAgentSearchParamsDTO.getContactIdSearchType().equals(SearchTypeEnum.IN)){
				List<String> numList = Arrays.asList(escrowAgentSearchParamsDTO.getContactId().split(",")).stream().map(String::trim).collect(Collectors.toList());
				List<Long> nums = numList.stream().map(Long::parseLong).collect(Collectors.toList());
				sql += " AND C.CRM_CONTACT_ID IN :contactId ";
				params.put(CONTACT_ID, nums);
				queryStrings.add("Contact Id in (" + escrowAgentSearchParamsDTO.getContactId() +")");
			}
		}
		return sql;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ContactEntity> getAllVbsForVbNumberLike(String vbNumberPrefix) {
		Query query = vbsEm.createNamedQuery(ContactEntity.SELECT_ALL_VBS_VB_NUMBER_LIKE).setMaxResults(VbsConstants.TYPEAHEAD_MAX_RESULTS);
		query.setParameter(1, vbNumberPrefix + "%");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VbPoolEntity> getVbsForPoolId(Long poolId) {
		Query query = vbsEm.createNamedQuery(VbPoolEntity.SELECT_ALL_VBS_BY_POOLID);
		query.setParameter(1, poolId);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VbPoolEntity> getDeletedVbsForPoolId(Long poolId) {
		Query query = vbsEm.createNamedQuery(VbPoolEntity.SELECT_ALL_DELETED_VBS_BY_POOLID);
		query.setParameter(1, poolId);
		return query.getResultList();
	}

	@Override
	public List<String> getVbNumbersForPoolId(Long poolId) {
		TypedQuery query = vbsEm.createNamedQuery(VbPoolEntity.SELECT_ALL_VB_NUMBERS_BY_POOLID, String.class);
		query.setParameter(1, poolId);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VbPoolEntity> getAllVbsEvenDeletedForPoolId(Long poolId) {
		Query query = vbsEm.createNamedQuery(VbPoolEntity.SELECT_ALL_EVEN_DELETED_VBS);
		query.setParameter(1, poolId);
		return query.getResultList();
	}


	@SuppressWarnings("unchecked")
	@Override
	public VbPoolEntity getVbPool(Long poolId, String vbNumber) throws VbsCheckedException {
		VbPoolEntity vbSecurity = null;
		Query query = vbsEm.createNamedQuery(VbPoolEntity.SELECT_NOT_DELETED_VB);
		query.setParameter(1, poolId);
		query.setParameter(2, vbNumber);
		List<VbPoolEntity> vbs = query.getResultList();
		if (vbs != null && !vbs.isEmpty()) {
			if (vbs.size() > 1) {
				throw new VbsCheckedException("More then 1 entry found for securityId=" + poolId + " and vbNumber=" + vbNumber);
			} else {
				vbSecurity = vbs.get(0);
			}
		} else {
			// nothing no VB set on security - return null
		}
		return vbSecurity;
	}

	@Override
	public long getNumberOfVbsBySecurityId(Long securityId) {
		Query query = vbsEm.createNamedQuery(VbPoolEntity.SELECT_NUMBER_OF_VBS_BY_SECURITY_ID);
		query.setParameter(1, securityId);
		return (long)query.getSingleResult();
	}

	@Override
	public List<ContactRelationshipEntity> getAllRelationshipsForContact(Long contactId) {
		TypedQuery<ContactRelationshipEntity> query = vbsEm.createNamedQuery(ContactRelationshipEntity.SELECT_ALL_RELATIONSHIP_FOR_CONTACT_ID, ContactRelationshipEntity.class);
		query.setParameter(1, contactId);
		return query.getResultList();
	}

	@Override
	public ContactRelationshipEntity getRelationshipByCrmRelId(String crmRelId) {
		TypedQuery<ContactRelationshipEntity> query = vbsEm.createNamedQuery(ContactRelationshipEntity.SELECT_RELATIONSHIP_FOR_CRM_REL_ID, ContactRelationshipEntity.class);
		query.setParameter(1, crmRelId);
		List<ContactRelationshipEntity> result = query.getResultList();
		if(result.isEmpty()) {
			return null;
		}
		return result.get(0);
	}

}

