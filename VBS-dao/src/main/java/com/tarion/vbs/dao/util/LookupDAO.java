/* 
 * 
 * LookupDAO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.util;

import java.util.List;

import javax.ejb.Local;

import com.tarion.vbs.orm.entity.alert.AlertTypeEntity;
import com.tarion.vbs.orm.entity.contact.ContactSecurityRoleEntity;
import com.tarion.vbs.orm.entity.contact.EscrowLicenseApplicationStatusEntity;
import com.tarion.vbs.orm.entity.contact.LicenseStatusEntity;
import com.tarion.vbs.orm.entity.correspondence.CorrespondenceTemplateEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionTypeEntity;
import com.tarion.vbs.orm.entity.release.ReasonEntity;
import com.tarion.vbs.orm.entity.release.RejectReasonEntity;
import com.tarion.vbs.orm.entity.release.ReleaseTypeEntity;
import com.tarion.vbs.orm.entity.security.IdentityTypeEntity;
import com.tarion.vbs.orm.entity.release.WarrantyServicesRecommendationTypeEntity;
import com.tarion.vbs.orm.entity.security.SecurityDepositEntity;
import com.tarion.vbs.orm.entity.security.SecurityPurposeEntity;
import com.tarion.vbs.orm.entity.security.SecurityStatusEntity;
import com.tarion.vbs.orm.entity.security.SecurityTypeEntity;
import com.tarion.vbs.orm.entity.security.VbDeleteReasonEntity;

/**
 * LookupDAO class for getting all lookup data
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-09
 * @version 1.0
 */
@Local
public interface LookupDAO {

    ReleaseTypeEntity getReleaseTypeByReleaseTypeId(Long releaseTypeId);

    List<SecurityPurposeEntity> getAllSecurityPurposes();
    List<SecurityStatusEntity> getAllSecurityStatuses();

	SecurityPurposeEntity getSecurityPurposeById(Long id);

	List<SecurityTypeEntity> getAllSecurityTypes();

	SecurityTypeEntity getSecurityTypeById(Long id);

	public List<VbDeleteReasonEntity> getAllVbDeleteReasons();

	VbDeleteReasonEntity getVbDeleteReasonById(Long id);

	List<FinancialInstitutionTypeEntity> getAllFinancialInstitutionTypes();
	public List<LicenseStatusEntity> getAllLicenseStatuses();

    RejectReasonEntity getRejectReasonById(Long id);

    public LicenseStatusEntity getLicenseStatusForName(String name);

	IdentityTypeEntity getIdentityTypeById(Long id);

	public WarrantyServicesRecommendationTypeEntity getWarrantyServicesRecommendationTypeEntity(String name);
	public List<EscrowLicenseApplicationStatusEntity> getAllEscrowLicenseApplicationStatuses();
	public EscrowLicenseApplicationStatusEntity getEscrowLicenseApplicationStatusEntity(String name);

	List<ReasonEntity> getAllReasons();
	List<SecurityDepositEntity> getAllSecurityDeposit();

	SecurityDepositEntity getSecurityDepositById(Long id);

	List<AlertTypeEntity> getAllAlertTypes();

    ReasonEntity getReasonByReasonId(Long reasonId);
	SecurityStatusEntity getSecurityStatusById(Long statusId);
    List<RejectReasonEntity> getAllRejectReasons();
	public List<ReleaseTypeEntity> getAllReleaseTypes();
	public List<IdentityTypeEntity> getAllIdentityTypes();

	AlertTypeEntity getAlertTypeById(Long id);

    List<CorrespondenceTemplateEntity> getAllTemplateTypes();

    CorrespondenceTemplateEntity getTemplateByTemplateId(Long id);

    List<ContactSecurityRoleEntity> getContactSecurityRoles();

    ContactSecurityRoleEntity getContactSecurityRoleById(Long id);
}
