package com.tarion.vbs.dao.correspondence;

import java.util.List;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.*;

import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.correspondence.CorrespondenceTemplateEntity;
import com.tarion.vbs.orm.entity.correspondence.CorrespondenceTokenEntity;
/**
 * Correspondence DAO class for DB operations on correspondence tables
 *
 * @author <A href="joni.paananen@tarion.com">Joni Paananen</A>
 * @since 2019-02-19
 * @version 1.0
 */
@Stateless(mappedName = "ejb/CorrespondenceDAO")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class CorrespondenceDAOImpl implements CorrespondenceDAO{

	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;


	@Override
	public List<CorrespondenceTokenEntity> getTokensForTemplate(String templateName) {
		TypedQuery<CorrespondenceTokenEntity> query = vbsEm.createNamedQuery(CorrespondenceTokenEntity.SELECT_TOKENS_WITH_TEMPLATE, CorrespondenceTokenEntity.class);
		query.setParameter(1, templateName);
		return query.getResultList();
	}

	@Override
	public CorrespondenceTemplateEntity getCorrespondenceTemplateByName(String name){
		TypedQuery<CorrespondenceTemplateEntity> query = vbsEm.createNamedQuery(CorrespondenceTemplateEntity.SELECT_TEMPLATE_BY_NAME, CorrespondenceTemplateEntity.class);
		query.setParameter(1, name);
		try {
			return query.getSingleResult();
		}catch(Exception e){
			throw new VbsRuntimeException("CORR03 - Template not found, passed: " + name);
		}
	}
	
	@Override
	public Tuple getVbsTokenValuesForRelease(Long releaseId){
		String sql =
				"SELECT " +
				"    (CASE WHEN LU.FIRST_NAME IS NULL THEN NULL ELSE CONCAT(LU.FIRST_NAME, ' ', LU.LAST_NAME) END) AS LU_CONTACT, " +
				"    VBADDR.ADDRESS_LINE1 AS VB_ADD_1, " +
				"    VBADDR.ADDRESS_LINE2 AS VB_ADD_2, " +
				"    VBADDR.ADDRESS_LINE3 AS VB_ADD_3, " +
				"    VBADDR.ADDRESS_LINE4 AS VB_ADD_4, " +
				"    VBADDR.CITY AS VB_CITY, " +
				"    VBADDR.PROVINCE AS VB_PROVINCE, " +
				"    VBADDR.POSTAL_CODE AS VB_POSTAL, " +
				"    T.DESCRIPTION AS SECURITY_TYPE, " +
				"    S.INSTRUMENT_NUMBER AS INSTR_NUMBER, " +
				"    ((SELECT TOP 1 SAUD.CURRENT_AMOUNT FROM SECURITY_AUD SAUD WHERE SAUD.ID = S.ID AND SAUD.UPDATE_DATE < REL.FINAL_APPR_DATE ORDER BY REV DESC) - " +
				"	 (SELECT COALESCE(SUM(R.AUTHORIZED_AMOUNT),0) FROM RELEASE_AUD R WHERE R.ID IN ( " +
				"	 SELECT DISTINCT(R3.ID) FROM RELEASE_AUD R3 WHERE R3.SECURITY_ID = REL.SECURITY_ID AND R3.STATUS = 'PENDING' AND R3.FINAL_APPR_DATE < REL.FINAL_APPR_DATE " +
				"	 AND NOT EXISTS (SELECT R4.ID FROM RELEASE_AUD R4 WHERE R4.ID = R3.ID AND R4.STATUS IN ('SENT', 'VOUCHER_CREATED', 'CANCELLED', 'COMPLETED')) " +
				"	 AND EXISTS (SELECT R5.ID FROM RELEASE R5 WHERE R5.ID = R3.ID) " + //safety for releases deleted from the db but audits still exist
				"	 ) AND R.REV = (SELECT TOP 1 R3.REV FROM RELEASE_AUD R3 WHERE R3.ID = R.ID AND R3.STATUS = 'PENDING' AND R3.FINAL_APPR_DATE < REL.FINAL_APPR_DATE) " +
				"	 )) AS SREL_PREV_AMT, " +
				"    (SELECT CONCAT(ENRLADDR.ADDRESS_LINE1, " +
				"        CASE WHEN ENRLADDR.ADDRESS_LINE2 IS NOT NULL THEN CONCAT(', ', ENRLADDR.ADDRESS_LINE2) ELSE NULL END, " +
				"        CASE WHEN ENRLADDR.ADDRESS_LINE3 IS NOT NULL THEN CONCAT(', ', ENRLADDR.ADDRESS_LINE3) ELSE NULL END, " +
				"        CASE WHEN ENRLADDR.ADDRESS_LINE4 IS NOT NULL THEN CONCAT(', ', ENRLADDR.ADDRESS_LINE4) ELSE NULL END, " +
				"        ', ',ENRLADDR.CITY, " +
				"        ', ',ENRLADDR.PROVINCE, " +
				"        ', ',ENRLADDR.POSTAL_CODE) " +
				"    ) AS SREL_SITE_ADD, " +
				"    E.ENROLMENT_NUMBER AS SITE_ID_1, " +
				"    ((SELECT TOP 1 SAUD.CURRENT_AMOUNT FROM SECURITY_AUD SAUD WHERE SAUD.ID = S.ID AND SAUD.UPDATE_DATE < REL.FINAL_APPR_DATE ORDER BY REV DESC) - " +
				"	 (SELECT COALESCE(SUM(R.AUTHORIZED_AMOUNT),0) FROM RELEASE_AUD R WHERE R.ID IN ( " +
				"	 SELECT DISTINCT(R3.ID) FROM RELEASE_AUD R3 WHERE R3.SECURITY_ID = REL.SECURITY_ID AND R3.STATUS = 'PENDING' AND R3.FINAL_APPR_DATE < REL.FINAL_APPR_DATE " +
				"	 AND NOT EXISTS (SELECT R4.ID FROM RELEASE_AUD R4 WHERE R4.ID = R3.ID AND R4.STATUS IN ('SENT', 'VOUCHER_CREATED', 'CANCELLED', 'COMPLETED')) " +
				"	 AND EXISTS (SELECT R5.ID FROM RELEASE R5 WHERE R5.ID = R3.ID) " + //safety for releases deleted from the db but audits still exist
				"	 ) AND R.REV = (SELECT TOP 1 R3.REV FROM RELEASE_AUD R3 WHERE R3.ID = R.ID AND R3.STATUS = 'PENDING' AND R3.FINAL_APPR_DATE < REL.FINAL_APPR_DATE) " +
				"	 ) - REL.AUTHORIZED_AMOUNT) AS SREL_CURR_AMT, " +
				"    VB.COMPANY_NAME AS VB_NAME, " +
				"    VB.CRM_CONTACT_ID AS VB_NUMBER, " +
				"    REL.AUTHORIZED_AMOUNT AS SREL_APPROVED_AMT, " +
				"    B.NAME AS FIN_INST_NAME, " +
				"    BRANCHADDR.ADDRESS_LINE1 AS FIN_BR_ADD_1, " +
				"    BRANCHADDR.ADDRESS_LINE2 AS FIN_BR_ADD_2, " +
				"    BRANCHADDR.ADDRESS_LINE3 AS FIN_BR_ADD_3, " +
				"    BRANCHADDR.ADDRESS_LINE4 AS FIN_BR_ADD_4, " +
				"    BRANCHADDR.CITY AS FIN_BR_CITY, " +
				"    BRANCHADDR.PROVINCE AS FIN_BR_PROVINCE, " +
				"    BRANCHADDR.POSTAL_CODE AS FIN_BR_POSTAL, " +
				"    E.ENROLMENT_NUMBER AS HOME_ENROL_NUMBER, " +
				"    (SELECT CONCAT(VBADDR.ADDRESS_LINE1, " +
				"        CASE WHEN VBADDR.ADDRESS_LINE2 IS NOT NULL THEN CONCAT(', ', VBADDR.ADDRESS_LINE2) ELSE NULL END, " +
				"        CASE WHEN VBADDR.ADDRESS_LINE3 IS NOT NULL THEN CONCAT(', ', VBADDR.ADDRESS_LINE3) ELSE NULL END, " +
				"        CASE WHEN VBADDR.ADDRESS_LINE4 IS NOT NULL THEN CONCAT(', ', VBADDR.ADDRESS_LINE4) ELSE NULL END, " +
				"        ', ',VBADDR.CITY, " +
				"        ', ',VBADDR.PROVINCE, " +
				"        ', ',VBADDR.POSTAL_CODE) " +
				"    ) AS VB_ADD " +
				"FROM RELEASE REL " +
				"JOIN SECURITY S ON S.ID = REL.SECURITY_ID " +
				"JOIN SECURITY_TYPE T ON T.ID = S.SECURITY_TYPE_ID " +
				"JOIN CONTACT VB ON VB.ID = S.PRIMARY_VB_ID " +
				"JOIN ADDRESS VBADDR ON VB.ADDRESS_ID = VBADDR.ID " +
				"LEFT OUTER JOIN ENROLMENT E ON E.ENROLMENT_NUMBER = (SELECT TOP 1 RE.ENROLMENT_NUMBER FROM RELEASE_ENROLMENT RE WHERE RE.RELEASE_ID = REL.ID) " +
				"LEFT OUTER JOIN ADDRESS ENRLADDR ON ENRLADDR.ID = E.ADDRESS_ID " +
				"LEFT OUTER JOIN CONTACT LU ON LU.ID = ( " +
				"   SELECT TOP 1 R.TO_CONTACT_ID FROM CONTACT_RELATIONSHIP R " +
				"   WHERE R.FROM_CONTACT_ID = VB.ID AND R.RELATIONSHIP_TYPE_ID = 20149 " +
				"   AND (R.END_DATE IS NULL OR (R.START_DATE != R.END_DATE AND R.END_DATE >= GETDATE())) " +
				"	ORDER BY R.END_DATE DESC ) " +
				"LEFT OUTER JOIN BRANCH B ON B.ID = S.BRANCH_ID " +
				"LEFT OUTER JOIN ADDRESS BRANCHADDR ON B.ADDRESS_ID = BRANCHADDR.ID " +
				"WHERE REL.ID = ?1";

		Query query = vbsEm.createNativeQuery(sql, Tuple.class);
		query.setParameter(1, releaseId);
		return (Tuple) query.getSingleResult();
	}
	
}