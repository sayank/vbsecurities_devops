package com.tarion.vbs.dao.alert;

import java.util.List;

import com.tarion.vbs.orm.entity.alert.AlertEntity;

/**
 * ContactDAO interface for DB operations on Alert tables
 *
 * @author <A href="djordje.przulj@tarion.com">Djordje Przulj</A>
 * @since 2019-04-16
 * @version 1.0
 */

public interface AlertDAO {

	List<AlertEntity> getAlertsBySecurityId(Long securityId);
	List<AlertEntity> getAlertsBySecurityIdExceptNoteType(Long securityId);
}
