/* 
 * 
 * EmailTransactionLogDAO.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.util;

import java.time.LocalDateTime;

import javax.ejb.Local;

import com.tarion.vbs.orm.entity.EmailTransactionLogEntity;

/**
 * EmailTransactionLogDAO class for DB operations on Email Transaction Log Table
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-01-30
 * @version 1.0
 */
@Local
public interface EmailTransactionLogDAO {
	public EmailTransactionLogEntity addEntity(EmailTransactionLogEntity emailTransactionLogEntity, String createUser, LocalDateTime createDate);
}
