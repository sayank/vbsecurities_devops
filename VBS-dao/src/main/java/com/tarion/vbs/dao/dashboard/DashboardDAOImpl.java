package com.tarion.vbs.dao.dashboard;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.*;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.dashboard.ManagerReleaseDTO;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.enrolment.CEEnrolmentDecisionEntity;

@Stateless(mappedName = "ejb/DashboardDAO")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class DashboardDAOImpl implements DashboardDAO {
	
	private static final String JPQL_MANAGER_RELEASES = "SELECT " 
			+ "r.security.primaryVb.crmContactId AS vbNumber, r.security.primaryVb.companyName AS vbName, umbrella.crmContactId AS umbrellaId, umbrella.companyName AS umbrellaName, "
			+ "r.security.id AS securityId, r.security.instrumentNumber AS instrumentNumber, r.referenceNumber AS referenceNumber, "
			+ "r.authorizedAmount AS authorizedAmount,  "
	        + "rr.reason.description AS releaseReason,"
			+ "r.analystApprovalBy as analystApprovalBy, "
			+ "r.analystApprovalDate as analystApprovalDate "
			+ "FROM ReleaseEntity r "
			+ "JOIN ReleaseReasonEntity rr ON rr.id = ( "
	        + "     SELECT MAX(r2.id) FROM ReleaseReasonEntity r2 WHERE r2.release.id = r.id "
	        + ") "
	        + "LEFT JOIN ContactRelationshipEntity cr ON cr.toContact.id = r.security.primaryVb.id AND cr.relationshipType.id = " + VbsConstants.RELATIONSHIP_TYPE_UMBRELLA + " AND ?1 BETWEEN cr.startDate AND cr.endDate "
	        + "LEFT JOIN ContactEntity umbrella ON cr.fromContact = umbrella "
	        + "WHERE UPPER(r.finalApprovalSentTo) = UPPER(?2) AND r.finalApprovalStatus IS NULL ORDER BY r.analystApprovalDate DESC";
	
	@PersistenceContext(unitName = "vbs")
	private EntityManager vbsEm;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ManagerReleaseDTO> getReleasesForManagerUserId(String userId) {
		Query query = vbsEm.createQuery(JPQL_MANAGER_RELEASES, Tuple.class);
		List<Tuple> ret = query.setParameter(1, LocalDateTime.now()).setParameter(2, userId).getResultList();
		return ret.stream().map(this::transform).collect(Collectors.toList());
	}

    @Override
    public List<CEEnrolmentDecisionEntity> getOutstandingCEDecisions() {
        TypedQuery<CEEnrolmentDecisionEntity> query = vbsEm.createNamedQuery(CEEnrolmentDecisionEntity.SELECT_OUTSTANDING_NEW_CES, CEEnrolmentDecisionEntity.class);
		return query.getResultList();
    }

    private ManagerReleaseDTO transform(Tuple t) {
		ManagerReleaseDTO dto = new ManagerReleaseDTO();
		dto.setSecurityId(t.get("securityId", Long.class));
		dto.setAmountToBeReleased(VbsUtil.getCurrencyFormattedString(t.get("authorizedAmount", BigDecimal.class)));
		dto.setInstrumentNumber(t.get("instrumentNumber", String.class));
		dto.setReleaseReason(t.get("releaseReason", String.class));
		dto.setSecurityReferenceNumber(t.get("referenceNumber", String.class));
		dto.setSentFrom(t.get("analystApprovalBy", String.class));
		dto.setSentTime(DateUtil.getDateFormattedShortTime(t.get("analystApprovalDate", LocalDateTime.class)));
		String umbrellaId = t.get("umbrellaId", String.class);
		if(!VbsUtil.isNullorEmpty(umbrellaId)) {
			dto.setUmbrellaId(Integer.parseInt(umbrellaId));
			dto.setUmbrellaName(t.get("umbrellaName", String.class));
		}
		dto.setVbName(t.get("vbName", String.class));
		dto.setVbNumber(Integer.parseInt(t.get("vbNumber", String.class).replace("B", "")));
		return dto;
	}
	
}
