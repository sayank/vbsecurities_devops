package com.tarion.vbs.dao.correspondence;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.Tuple;

import com.tarion.vbs.orm.entity.correspondence.CorrespondenceTemplateEntity;
import com.tarion.vbs.orm.entity.correspondence.CorrespondenceTokenEntity;

/**
 * Correspondence DAO class for DB operations on correspondence tables
 *
 * @author <A href="joni.paananen@tarion.com">Joni Paananen</A>
 * @since 2019-02-19
 * @version 1.0
 */

@Local
public interface CorrespondenceDAO {

	List<CorrespondenceTokenEntity> getTokensForTemplate(String templateName);
	CorrespondenceTemplateEntity getCorrespondenceTemplateByName(String name);
	Tuple getVbsTokenValuesForRelease(Long releaseId);

}
