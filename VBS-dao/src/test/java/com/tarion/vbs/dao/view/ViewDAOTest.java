/* 
 * 
 * ContactDAOTest.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.view;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.dao.test.Container;
import com.tarion.vbs.orm.entity.view.MsWordSecRelLtrViewEntity;

public class ViewDAOTest {

	private UserTransaction userTransaction;
	private ViewDAO viewDAO;

	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		viewDAO = Container.getBean(ViewDAOImpl.class);
	}

	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}

	@Test
	public void test() {
		assertNotNull(viewDAO);
	}
	

	
//	@Test
//	public void testFindMsWordSecRelLtrByReferenceFieldName() {
//		MsWordSecRelLtrViewEntity entity = viewDAO.findMsWordSecRelLtrByReferenceFieldName("38984-2");
//		assertNotNull(entity);
//	}
	
//	@Test
//	public void testGetAllMsWordSecRelLtrs() {
//		List<MsWordSecRelLtrViewEntity> list = viewDAO.getAllMsWordSecRelLtrs();
//		assertTrue(!list.isEmpty());
//	}
	
	
}
