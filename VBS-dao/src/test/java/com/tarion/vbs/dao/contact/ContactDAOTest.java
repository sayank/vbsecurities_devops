/* 
 * 
 * ContactDAOTest.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.contact;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.dao.util.GenericDAOImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.dao.test.Container;
import com.tarion.vbs.orm.entity.contact.ContactEntity;
import com.tarion.vbs.orm.entity.contact.ContactRelationshipEntity;
import com.tarion.vbs.orm.entity.contact.ContactTypeEntity;

public class ContactDAOTest {

	private UserTransaction userTransaction;
	private ContactDAO contactDAO;
	private GenericDAO genericDAO;

	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		contactDAO = Container.getBean(ContactDAOImpl.class);
		genericDAO = Container.getBean(GenericDAOImpl.class);
	}

	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}

	@Test
	public void test() {
		assertNotNull(contactDAO);
	}
	

	
	@Test
	public void testGetAllRelationshipsForContact() {
		List<ContactRelationshipEntity> relationships = contactDAO.getAllRelationshipsForContact(1l);
		assertTrue(relationships != null);
	}
	
	@Test
	public void testGetAssistantsForEscrowAgent() {
		List<ContactEntity> contacts = contactDAO.getAssistantsForEscrowAgent("3638");
		assertTrue(!contacts.isEmpty());
	}

	@Test
	public void testGetLawyersForEscrowAgent() {
		List<ContactEntity> contacts = contactDAO.getLawyersForEscrowAgent("3638");
		assertTrue(!contacts.isEmpty());
	}

	@Test
	public void testGetAllVbsForVbNumberLike() {
		assertNotNull(contactDAO);
		// Insert test Data
		insertTestVb("B01");
		insertTestVb("B02");
		List<ContactEntity> vbs = contactDAO.getAllVbsForVbNumberLike("B0");
		assertNotNull(vbs);
		assertTrue(vbs.size() > 1);

		//TODO fix this part of the test to find by CRM contact id instead.
		//ContactEntity testVb = genericDAO.findEntityById(ContactEntity.class, "B01");
		//assertNotNull(testVb);
	}

	// TODO rewrite this test. IDs cannot be hardcoded in the test
//	@Test
//	public void testGetVbsForPoolId() {
//		assertNotNull(vbDAO);
//		List<VbPoolEntity> vbs = vbDAO.getVbsForPoolId(368L);
//		assertNotNull(vbs);
//		assertTrue(vbs.size() > 1);
//	}


	private void insertTestVb(String vbNumber) {
		ContactEntity vb = new ContactEntity();
		vb.setCrmContactId(vbNumber);
		vb.setCompanyName("UnitTestVb");
		ContactTypeEntity vbType = genericDAO.findEntityById(ContactTypeEntity.class, VbsConstants.CONTACT_TYPE_VB);
		vb.setContactType(vbType);
		genericDAO.addEntity(vb, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
	}
}
