package com.tarion.vbs.dao.release;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.enrolment.EnrolmentWarrantyServiceFieldsDTO;
import com.tarion.vbs.common.dto.release.PendingReleaseDTO;
import com.tarion.vbs.common.dto.release.ReleaseSearchParametersDTO;
import com.tarion.vbs.common.dto.release.ReleaseTypeDTO;
import com.tarion.vbs.common.enums.ReleaseStatus;
import com.tarion.vbs.common.enums.SearchTypeEnum;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.dao.test.Container;
import com.tarion.vbs.dao.test.UnitTestObjectCreatorUtil;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.dao.util.GenericDAOImpl;
import com.tarion.vbs.orm.entity.release.ReleaseEnrolmentEntity;
import com.tarion.vbs.orm.entity.release.ReleaseEntity;
import com.tarion.vbs.orm.entity.security.PoolEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import com.tarion.vbs.orm.entity.security.SecurityPurposeEntity;
import com.tarion.vbs.orm.entity.security.SecurityTypeEntity;

public class ReleaseDAOTest {
	
	private GenericDAO genericDAO;
	private UserTransaction userTransaction;
	private ReleaseDAO releaseDAO;
	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		releaseDAO = Container.getBean(ReleaseDAOImpl.class);
		genericDAO = Container.getBean(GenericDAOImpl.class);

	}

	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}

	@Test
	public void testGetCollectorReleasebyStatusAndSecurityId() {
		long pendingRelease = releaseDAO.getReleaseDemandCollectorByStatusAndSecurityId(ReleaseStatus.PENDING, 32612l);
		assert(pendingRelease > 0);
	}

	@Test
	public void testGetEnrolmentsForRelease() {
		List<ReleaseEnrolmentEntity> releaseEnrolments1 = releaseDAO.getEnrolmentsByReleaseId(2L);
		assertNotNull(releaseEnrolments1);
		List<ReleaseEnrolmentEntity> releaseEnrolments2 = releaseDAO.getEnrolmentsByReleaseId(3L);
		assertNotNull(releaseEnrolments2);
	}

	@Test
	public void testGetReleasesByEnrolmentNumber() {
		List<ReleaseEnrolmentEntity> releaseEnrolments1 = releaseDAO.getReleasesByEnrolmentNumber("H2142618");
		assertNotNull(releaseEnrolments1);
	}

	@Test
	public void testAddRelease() {
		SecurityTypeEntity securityType = genericDAO.findEntityById(SecurityTypeEntity.class, 1l);
		SecurityPurposeEntity securityPurpose = genericDAO.findEntityById(SecurityPurposeEntity.class, 1l);
		SecurityEntity security = UnitTestObjectCreatorUtil.createSecurityEntity(securityType, securityPurpose);

		security = (SecurityEntity)genericDAO.addEntity(security, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
		PoolEntity pool = (PoolEntity)genericDAO.addEntity(UnitTestObjectCreatorUtil.createPoolEntity(), VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());

		security.setPool(pool);
		genericDAO.updateEntity(security, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
		ReleaseEntity newRelease = UnitTestObjectCreatorUtil.createReleaseEntity(security);
		newRelease.setWsInputRequested(true);
		ReleaseEntity release = (ReleaseEntity)genericDAO.addEntity(newRelease, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());

		assertNotNull(release);

		List<ReleaseEntity> releases = releaseDAO.getReleases(security.getId());
		assertTrue(releases.get(0).getId().equals(release.getId()));
	}

	@Test
	public void testUpdateReleaseEnrolmentEntity() {
		ReleaseEnrolmentEntity re = genericDAO.findEntityById(ReleaseEnrolmentEntity.class, 10l);
		if (re != null) {
			re.setAuthorizedAmount(new BigDecimal(125l));
			re = (ReleaseEnrolmentEntity)genericDAO.updateEntity(re, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
			assertNotNull(re);
		}
	}

	@Test
	public void testGetReleasesByStatusesFromToDate() {
		List<ReleaseStatus> releaseStatuses = new ArrayList<>();
		releaseStatuses.add(ReleaseStatus.PENDING);
		releaseStatuses.add(ReleaseStatus.SENT);
		LocalDateTime startDate = DateUtil.getLocalDateTimeNow().minusMonths(5l);
		LocalDateTime endDate = DateUtil.getLocalDateTimeNow();
		List<ReleaseEntity> releases = releaseDAO.getReleasesByStatusesFromToDate(releaseStatuses, startDate, endDate);
		assertNotNull(releases);
		releases = releaseDAO.getReleasesByStatusesFromToDate(releaseStatuses, null, null);
		assertNotNull(releases);
		releases = releaseDAO.getReleasesByStatusesFromToDate(releaseStatuses, startDate, null);
		assertNotNull(releases);
		releases = releaseDAO.getReleasesByStatusesFromToDate(releaseStatuses, null, endDate);
		assertNotNull(releases);
	}

	@Test
	public void testSearchReleases() {
		ReleaseSearchParametersDTO params = new ReleaseSearchParametersDTO();
		params.setDateSearchType(SearchTypeEnum.BETWEEN);
		params.setReleaseUpdateStartDate(DateUtil.getLocalDateTimeStartOfYear(2019));
		params.setReleaseUpdateEndDate(DateUtil.getLocalDateTimeNextYear());

		List<PendingReleaseDTO> releases = releaseDAO.searchReleases(params);
		assertNotNull(releases);
//		assertEquals(12, releases.size());

		params.setDateSearchType(SearchTypeEnum.EQUALS_GREATER_THAN);
		params.setReleaseUpdateStartDate(DateUtil.getLocalDateTimeStartOfYear(2018));
		params.setReleaseUpdateEndDate(null);

		releases = releaseDAO.searchReleases(params);
		assertNotNull(releases);
//		assertEquals(2225, releases.size());

		params.setDateSearchType(SearchTypeEnum.EQUALS_GREATER_THAN);
		params.setReleaseUpdateStartDate(DateUtil.getLocalDateTimeStartOfYear(2018));
		params.setReleaseUpdateEndDate(null);
		List<ReleaseTypeDTO> releaseTypes = new ArrayList<ReleaseTypeDTO>();
		ReleaseTypeDTO releaseType = new ReleaseTypeDTO();
		releaseType.setId(3l);
		releaseTypes.add(releaseType);
		releaseType = new ReleaseTypeDTO();
		releaseType.setId(4l);
		releaseTypes.add(releaseType);
		params.setReleaseTypes(releaseTypes);
		params.setReleaseStatus(ReleaseStatus.SENT);

		releases = releaseDAO.searchReleases(params);
		assertNotNull(releases);
//		assertEquals(99, releases.size());

	}

	@Test
	public void testGetEnrolmentWarrantyServiceFieldsList() {
		List<String> list = new ArrayList<>();
		list.add("H1053164");
		list.add("H1024429");
		List<EnrolmentWarrantyServiceFieldsDTO> wsDtos = releaseDAO.getEnrolmentWarrantyServiceFieldsList(list);
		if (wsDtos != null) {

		}
	}
	
}
