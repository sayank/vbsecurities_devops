package com.tarion.vbs.dao.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.NamingException;

public class Container {
	
	private static Container instance;

	private EJBContainer ejbContainer;

	private Container() {}
	
	public static Container getInstance() {
		if(instance == null) {
			instance = new Container();
		}
		return instance;
	}
	
	private static Context getContext() {
		return getInstance().getContainer().getContext();
	}
	
	public void startTheContainer() {
		final Properties p = new Properties();

		// This property makes load persistence.xml from test.persistence.xml
        p.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.client.LocalInitialContextFactory");
        p.setProperty("openejb.altdd.prefix", "test");

        // this created new data source and setup properties
        // you have to put database name and username in the url
        // and then repeat username in the properties and password as well
		p.put("vbs", "new://Resource?type=DataSource");
		p.put("vbs.JdbcDriver", "com.microsoft.sqlserver.jdbc.SQLServerDataSource");
		p.put("tdm", "new://Resource?type=DataSource");
		p.put("tdm.JdbcDriver", "com.microsoft.sqlserver.jdbc.SQLServerDataSource");
		try {
			p.put("vbs.JdbcUrl", System.getProperty("test.jdbc.url"));
			p.put("vbs.UserName", System.getProperty("test.jdbc.user"));
			p.put("vbs.Password", System.getProperty("test.jdbc.password"));
			p.put("tdm.JdbcUrl", System.getProperty("test.tdm.jdbc.url"));
			p.put("tdm.UserName", System.getProperty("test.tdm.jdbc.user"));
			p.put("tdm.Password", System.getProperty("test.tdm.jdbc.password"));
		} catch (Exception e) {
			try {
				// needed for Eclipse debugging because it does not get them from pom
				Properties temp = new Properties();
				temp.load(new FileInputStream("src/test/resources/test.vbs.properties"));
				p.put("vbs.JdbcUrl", temp.getProperty("test.jdbc.url"));
				p.put("vbs.UserName", temp.getProperty("test.jdbc.user"));
				p.put("vbs.Password", temp.getProperty("test.jdbc.password"));
				p.put("tdm.JdbcUrl", temp.getProperty("test.tdm.jdbc.url"));
				p.put("tdm.UserName", temp.getProperty("test.tdm.jdbc.user"));
				p.put("tdm.Password", temp.getProperty("test.tdm.jdbc.password"));

			} catch (IOException e1) {
				e1.printStackTrace();
				return;
			}

		}
		//Moved to project POM for CI.
		
		// TST DB
//		p.put("vbs.JdbcUrl", "jdbc:sqlserver://devtstmidbd01.cace181e6e585f025.database.windows.net:1433;databaseName=VBS_TST6;user=vbs_tst6_rw");
//		p.put("vbs.UserName", "vbs_tst6_rw");
//		p.put("vbs.Password", "Z9)r![5B&Bs-j=");
		
        // this created new data source and setup properties
        // you have to put database name and username in the url
        // and then repeat username in the properties and password as well
		ejbContainer = EJBContainer.createEJBContainer(p);
	}
	
	public void stopTheContainer() {
		if (ejbContainer != null) {
			ejbContainer.close();
		}
	}
	
	public EJBContainer getContainer() {
		return ejbContainer;
	}
	
	/**
	 * Returns the EJB. Must give the implementing class as parameter.
	 * example:
	 * Container.getBean(ApplicationDAOImpl.class);
	 * 
	 * NOT!
	 * 
	 * Container.getBean(ApplicationDAO.class)
	 * 
	 * @param clazz
	 * @return
	 * @throws NamingException
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getBean(Class<T> clazz) throws NamingException {
		
		String simpleName = clazz.getSimpleName();
		String jndiName = "java:global/VBS-dao/" + simpleName;
		
		Context context = Container.getContext();
		Object object = context.lookup(jndiName);
		return (T)object;
	}
}
