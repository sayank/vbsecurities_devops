package com.tarion.vbs.dao.release;

import com.tarion.vbs.common.dto.autorelease.AutoReleaseCandidateDTO;
import com.tarion.vbs.common.dto.autorelease.AutoReleaseResultDTO;
import com.tarion.vbs.common.dto.autorelease.AutoReleaseRunSearchParametersDTO;
import com.tarion.vbs.common.dto.release.AutoReleaseDTO;
import com.tarion.vbs.common.enums.SearchTypeEnum;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.dao.test.Container;
import com.tarion.vbs.orm.entity.autorelease.AutoReleaseRunEntity;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;
import java.util.List;

import static org.junit.Assert.assertNotNull;

public class AutoReleaseDAOTest {
	
	private UserTransaction userTransaction;
	private AutoReleaseDAO autoReleaseDAO;
	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		autoReleaseDAO = Container.getBean(AutoReleaseDAOImpl.class);

	}

	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}

	@Test
	public void testGetReleasesByStatusesFromToDate() {
		AutoReleaseRunSearchParametersDTO params = new AutoReleaseRunSearchParametersDTO();
		params.setDateSearchType(SearchTypeEnum.BETWEEN);
		params.setStartDate(DateUtil.getLocalDateTimeStartOfYear(2018));
		params.setEndDate(DateUtil.getLocalDateTimeNextYear());

		List<Pair<AutoReleaseRunEntity, Long>> runs = autoReleaseDAO.searchAutoReleaseRuns(params);
		assertNotNull(runs);

		params.setDateSearchType(SearchTypeEnum.LESS_THAN);
		params.setStartDate(DateUtil.getLocalDateTimeStartOfYear(2018));
		params.setEndDate(null);

		runs = autoReleaseDAO.searchAutoReleaseRuns(params);
		assertNotNull(runs);

		params.setDateSearchType(SearchTypeEnum.EQUALS_GREATER_THAN);
		params.setStartDate(DateUtil.getLocalDateTimeStartOfYear(2018));
		params.setEndDate(null);

		runs = autoReleaseDAO.searchAutoReleaseRuns(params);
		assertNotNull(runs);
	}

	@Test
	public void testGetAutoReleaseCandidates() {
		List<AutoReleaseCandidateDTO> result = autoReleaseDAO.getAutoReleaseCandidates();
		assertNotNull(result);
	}
	
	@Test
	public void testGetAutoReleaseEnrolmentsForRunSuccess() {
		List<AutoReleaseResultDTO> enrolmentSuccess = autoReleaseDAO.getAutoReleaseResultsForRunSuccess(1l);
		assertNotNull(enrolmentSuccess);
	}
	
	@Test
	public void testGetAutoReleaseEnrolmentsForRunEliminated() {
		List<AutoReleaseResultDTO> enrolmentEliminated = autoReleaseDAO.getAutoReleaseResultsForRunEliminated(1l);
		assertNotNull(enrolmentEliminated);
	}

	@Test
	public void testGetAutoReleaseForSecurityId() {
		List<AutoReleaseDTO> result = autoReleaseDAO.getAutoReleaseForSecurityId(30792L);
		assertNotNull(result);
	}
}
