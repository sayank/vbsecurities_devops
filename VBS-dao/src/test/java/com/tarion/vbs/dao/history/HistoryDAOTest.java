package com.tarion.vbs.dao.history;

import com.tarion.vbs.common.dto.financialinstitution.FIActivityDTO;
import com.tarion.vbs.dao.test.Container;
import com.tarion.vbs.dao.util.AuditQueryResult;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.dao.util.GenericDAOImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;
import java.util.List;

import static org.junit.Assert.assertNotNull;

public class HistoryDAOTest {

	private UserTransaction userTransaction;
	private HistoryDAO historyDAO;
	private GenericDAO genericDAO;
	
	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {
			userTransaction.begin();
		} catch (Exception e) {
			e.printStackTrace();
		}
		historyDAO = Container.getBean(HistoryEnversDAOImpl.class);
		genericDAO = Container.getBean(GenericDAOImpl.class);
	}

	@After
	public void cleanUp() {
		try {
			userTransaction.rollback();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test() {
		assertNotNull(historyDAO);
	}

}
