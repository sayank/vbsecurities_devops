/* 
 * 
 * ContactDAOTest.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.dashboard;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.common.dto.dashboard.ManagerReleaseDTO;
import com.tarion.vbs.dao.test.Container;

public class DashboardDAOTest {

	private UserTransaction userTransaction;
	private DashboardDAO dashboardDAO;
	

	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		dashboardDAO = Container.getBean(DashboardDAOImpl.class);
	}

	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}

	@Test
	public void test() {
		List<ManagerReleaseDTO> result = dashboardDAO.getReleasesForManagerUserId("BHAMA");
		assertNotNull(result);
	}	
}
