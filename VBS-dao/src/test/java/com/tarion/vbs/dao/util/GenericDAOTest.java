/* 
 * 
 * ContactDAOTest.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.util;

import static org.junit.Assert.assertNotNull;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.dao.test.Container;
import com.tarion.vbs.orm.entity.alert.AlertEntity;
import com.tarion.vbs.orm.entity.alert.AlertTypeEntity;
import com.tarion.vbs.orm.entity.autorelease.AutoReleaseEliminationEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;

public class GenericDAOTest {

	private UserTransaction userTransaction;
	private GenericDAO genericDAO;

	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		genericDAO = Container.getBean(GenericDAOImpl.class);
	}

	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}

	@Test
	public void test() {
		assertNotNull(genericDAO);
	}
	

	
	@Test
	public void testGetLicenseStatusForName() {
		AutoReleaseEliminationEntity entity = genericDAO.findEntityById(AutoReleaseEliminationEntity.class, 1l);
		if (entity == null) {
			entity = new AutoReleaseEliminationEntity();
			entity.setCreateUser(VbsConstants.VBS_SYSTEM_USER_ID);
			entity.setCreateDate(DateUtil.getLocalDateTimeNow());
			entity.setUpdateUser(VbsConstants.VBS_SYSTEM_USER_ID);
			entity.setUpdateDate(DateUtil.getLocalDateTimeNow());
			entity = (AutoReleaseEliminationEntity)genericDAO.addEntity(entity, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
		} else {
			entity.setUpdateUser(VbsConstants.VBS_SYSTEM_USER_ID);
			entity.setUpdateDate(DateUtil.getLocalDateTimeNow());
			entity = (AutoReleaseEliminationEntity)genericDAO.updateEntity(entity, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
		}
	}
	
	// Audit table test add
	@Test
	public void testAddEnrolment() {
		EnrolmentEntity enrolment = genericDAO.findEntityById(EnrolmentEntity.class, "H1000001");
		if (enrolment != null) {
			enrolment.setUpdateUser("test");
			genericDAO.updateEntity(enrolment, "test", DateUtil.getLocalDateTimeNow());
		} else {
			enrolment = new EnrolmentEntity();
			genericDAO.addEntity(enrolment, "test", DateUtil.getLocalDateTimeNow());
		}
	}
	@Test
	public void testAddAlert() {
		AlertEntity alert = genericDAO.findEntityById(AlertEntity.class, 1l);
		if (alert != null) {
			alert.setUpdateUser("test");
			genericDAO.updateEntity(alert, "test", DateUtil.getLocalDateTimeNow());
		} else {
			alert = new AlertEntity();
			alert.setAlertType(genericDAO.findEntityById(AlertTypeEntity.class, 1l));
			alert.setSecurity(genericDAO.findEntityById(SecurityEntity.class, 1l));
			genericDAO.addEntity(alert, "test", DateUtil.getLocalDateTimeNow());
		}
	}
	
	
}
