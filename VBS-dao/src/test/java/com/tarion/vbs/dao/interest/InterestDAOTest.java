/* 
 * 
 * InterestDAOTest.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.interest;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.dao.test.Container;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.dao.util.GenericDAOImpl;
import com.tarion.vbs.orm.entity.interest.InterestEntity;
import com.tarion.vbs.orm.entity.interest.InterestRateEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.Tuple;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

public class InterestDAOTest {

	private GenericDAO genericDAO;
	private UserTransaction userTransaction;
	private InterestDAO interestDAO;

	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		genericDAO = Container.getBean(GenericDAOImpl.class);
		interestDAO = Container.getBean(InterestDAOImpl.class);
	}

	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}

	@Test
	public void test() {
		assertNotNull(interestDAO);
		assertNotNull(genericDAO);
	}


	@Test
	public void testGetAllInterestRate() {
		List<InterestRateEntity> rates = interestDAO.getAllInterestRates();
		assertNotNull(rates);
	}

	@Test
	public void testGetApplicableInterestRateForDate() {
		try {
			InterestRateEntity rate = addInterestRate();
			InterestRateEntity todaysRate = interestDAO.getApplicableInterestRateForDate(DateUtil.getLocalDateTimeNow());
			assertEquals(0, rate.getAnnualRate().compareTo(todaysRate.getAnnualRate()));
		} catch (VbsCheckedException e) {
			e.printStackTrace();
			fail();
		}
	}

	private InterestRateEntity addInterestRate() {
		InterestRateEntity rate = null;
		try {
			rate = interestDAO.getApplicableInterestRateForDate(DateUtil.getLocalDateTimeNow());
		} catch (VbsCheckedException e) {
			rate = null;
		}
		if (rate == null) {
			rate = new InterestRateEntity();
			rate.setInterestStartDate(DateUtil.getLocalDateTimeNow().minusDays(1l));
			rate.setAnnualRate(new BigDecimal("1.25"));
		
			rate = (InterestRateEntity)genericDAO.addEntity(rate, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
		}

		return rate;
	}

	@Test
	public void testGetCashSecuritiesWithoutInterest() {
		List<Tuple> securities = interestDAO.getCashSecuritiesWithoutInterest(30);
		assertNotNull(securities);
		assertFalse(securities.isEmpty());
	}

	@Test
	public void testGetLastCalculatedInterestForSecurity() {
		InterestEntity interest = interestDAO.getLastCalculatedInterestForSecurity(0);
		if (interest != null) {
			assertNotNull(interest);
		}
	}

	
}
