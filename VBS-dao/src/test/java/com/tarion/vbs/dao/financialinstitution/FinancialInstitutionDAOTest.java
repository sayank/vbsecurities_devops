/* 
 * 
 * VbDAOTest.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.financialinstitution;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionAmountsDTO;
import com.tarion.vbs.dao.test.Container;
import com.tarion.vbs.orm.entity.financialinstitution.BranchEntity;
import com.tarion.vbs.orm.entity.financialinstitution.ContactFinancialInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionMaaPoaEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionMaaPoaInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionSigningOfficerEntity;

public class FinancialInstitutionDAOTest {

	private UserTransaction userTransaction;
	private FinancialInstitutionDAO financialInstitutionDAO;
	
	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		financialInstitutionDAO = Container.getBean(FinancialInstitutionDAOImpl.class);
	}

	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}

	@Test
	public void test() {
		assertNotNull(financialInstitutionDAO);
	}
	
	@Test
	public void testGetAllFinancialInstitutionsForNameLike() {
		List<FinancialInstitutionEntity> entities = financialInstitutionDAO.getFinancialInstitutionsForNameLike("Royal", 10);
		assertNotNull(entities);
	}

	@Test
	public void testGetAllFinancialInstitutions() {
		List<FinancialInstitutionEntity> entities = financialInstitutionDAO.getAllFinancialInstitutions();
		assertNotNull(entities.size() > 1);
	}

	@Test
	public void testGetAllBranches() {
		List<BranchEntity> entities = financialInstitutionDAO.getAllBranches();
		assertNotNull(entities.size() > 1);
	}
	
	@Test
	public void testGetFinancialInstitutionAmounts() {
		FinancialInstitutionAmountsDTO amounts = financialInstitutionDAO.getDirectFinancialInstitutionAmounts(VbsConstants.FINANCIAL_INSTITUTION_CIBC_ID, false);
		assertNotNull(amounts);
	}

	@Test
	public void testGetContactsForFinancialInstitution() {
		List<ContactFinancialInstitutionEntity> contacts = financialInstitutionDAO.getContactsForFinancialInstitution(VbsConstants.FINANCIAL_INSTITUTION_CIBC_ID);
		assertNotNull(contacts);
	}

	@Test
	public void testGetMaaPoasForFinancialInstitution() {
		List<FinancialInstitutionMaaPoaEntity> maaPoas = financialInstitutionDAO.getMaaPoasForFinancialInstitution(VbsConstants.FINANCIAL_INSTITUTION_CIBC_ID);
		assertNotNull(maaPoas);
	}
	
	@Test
	public void testGetMaaPoaInstitutionsForMaaPoa() {
		List<FinancialInstitutionMaaPoaEntity> maaPoas = financialInstitutionDAO.getMaaPoasForFinancialInstitution(VbsConstants.FINANCIAL_INSTITUTION_CIBC_ID);
		if (!maaPoas.isEmpty()) {
			List<FinancialInstitutionMaaPoaInstitutionEntity> maaPoaInstitutions = financialInstitutionDAO.getMaaPoaInstitutionsForMaaPoa(maaPoas.get(0).getId());
			List<FinancialInstitutionSigningOfficerEntity> signingOfficers = financialInstitutionDAO.getSigningOfficersForFinancialInstitutionMaaPoa(maaPoas.get(0).getId());
			assertNotNull(maaPoaInstitutions);
			assertNotNull(signingOfficers);
		}
	}
	
	@Test
	public void testGetMaaPoaFinancialInstitutionAmounts() {
		FinancialInstitutionAmountsDTO dto = financialInstitutionDAO.getMaaPoaFinancialInstitutionAmounts(684l, false);
		if (dto != null) {
			assertNotNull(dto);
		}
	}
	
}
