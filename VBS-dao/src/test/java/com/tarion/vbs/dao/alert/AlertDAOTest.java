package com.tarion.vbs.dao.alert;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.dao.util.GenericDAOImpl;
import com.tarion.vbs.orm.entity.alert.AlertTypeEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.dao.test.Container;
import com.tarion.vbs.orm.entity.alert.AlertEntity;

public class AlertDAOTest {

	private UserTransaction userTransaction;
	private AlertDAO alertDAO;
	private GenericDAO genericDAO;
	
	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {
			userTransaction.begin();
		} catch (Exception e) {
			e.printStackTrace();
		}
		alertDAO = Container.getBean(AlertDAOImpl.class);
		genericDAO = Container.getBean(GenericDAOImpl.class);
	}

	@After
	public void cleanUp() {
		try {
			userTransaction.rollback();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test() {
		assertNotNull(alertDAO);
	}

	// TODO fix this - make sure that you created all necessary data
//	@Test
	public void testGetAlertsBySecurityId() {
		List<AlertEntity> entities = alertDAO.getAlertsBySecurityId(123L);
		assertTrue(entities.size() == 3);
	}

	@Test
	public void insertAlert() {
		SecurityEntity sec = genericDAO.findEntityById(SecurityEntity.class, 123L);
		if(sec != null) {
			AlertEntity entity = new AlertEntity();
			entity.setAlertType(genericDAO.findEntityById(AlertTypeEntity.class, 1L));
			entity.setDescription("Test Desc");
			entity.setName("Test Name");
			entity.setSecurity(sec);
			entity.setStartDate(DateUtil.getLocalDateTimeNow());
			entity = (AlertEntity) genericDAO.addEntity(entity, "jpaananen", DateUtil.getLocalDateTimeNow());

			entity = genericDAO.findEntityById(AlertEntity.class, entity.getId());
			assertTrue(entity.getName().equals("Test Name"));
		}
	}
}
