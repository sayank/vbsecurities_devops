package com.tarion.vbs.dao.test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.orm.entity.financialinstitution.BranchEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionTypeEntity;
import com.tarion.vbs.orm.entity.release.ReleaseEntity;
import com.tarion.vbs.orm.entity.security.PoolEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import com.tarion.vbs.orm.entity.security.SecurityPurposeEntity;
import com.tarion.vbs.orm.entity.security.SecurityTypeEntity;

public class UnitTestObjectCreatorUtil {
	
	public static SecurityEntity createSecurityEntity(SecurityTypeEntity securityType, SecurityPurposeEntity securityPurpose) {
		SecurityEntity security = new SecurityEntity();
		security.setReceivedDate(DateUtil.getLocalDateTimeNow());
		security.setIssuedDate(DateUtil.getLocalDateTimeNow());
		security.setOriginalAmount(new BigDecimal("10"));
		security.setSecurityType(securityType);
		security.setSecurityPurpose(securityPurpose);
		security.setInstrumentNumber("RA 12345");

		return security;
	}
	
	public static PoolEntity createPoolEntity() {
		PoolEntity poolEntity = new PoolEntity();
		poolEntity.setCreateDate(DateUtil.getLocalDateTimeNow());
		poolEntity.setUnitsToCover(1);
		poolEntity.setUpdateDate(DateUtil.getLocalDateTimeNow());
		poolEntity.setUpdateUser(VbsConstants.VBS_SYSTEM_USER_ID);
		poolEntity.setVersion(1l);

		return poolEntity;
	}
	
	public static ReleaseEntity createReleaseEntity(SecurityEntity security) {
		
		LocalDateTime date = DateUtil.getLocalDateTimeNow();
		
		ReleaseEntity release = new ReleaseEntity();
		release.setCreateDate(date);
		release.setCreateUser(VbsConstants.VBS_SYSTEM_USER_ID);
		release.setSequenceNumber(1l);
		release.setVersion(1l);
		release.setSecurity(security);
		
		return release;
	}
	
	public static FinancialInstitutionEntity createFinancialInstitutionEntity(FinancialInstitutionTypeEntity type) {
		FinancialInstitutionEntity entity = new FinancialInstitutionEntity();
		entity.setDescription("Test");
		entity.setExpiryDate(DateUtil.getLocalDateTimeNow().plusYears(10));
		entity.setFinancialInstitutionType(type);
		entity.setName("Test");
		entity.setMaxSecurityAmount(new BigDecimal(999999));
		entity.setWnSecurityMaxAmount(new BigDecimal(999999));
		entity.setCreateDate(DateUtil.getLocalDateTimeNow());
		entity.setUpdateDate(DateUtil.getLocalDateTimeNow());
		entity.setUpdateUser(VbsConstants.VBS_SYSTEM_USER_ID);
		entity.setVersion(1l);

		return entity;
	}
	
	public static BranchEntity createBranchEntity(FinancialInstitutionEntity fin) {
		BranchEntity entity = new BranchEntity();
		entity.setId(1l);
		entity.setDescription("Test");
		entity.setFinancialInstitution(fin);
		entity.setName("Test");
		entity.setCreateDate(DateUtil.getLocalDateTimeNow());
		entity.setUpdateDate(DateUtil.getLocalDateTimeNow());
		entity.setUpdateUser(VbsConstants.VBS_SYSTEM_USER_ID);
		entity.setVersion(1l);

		return entity;
	}


}
