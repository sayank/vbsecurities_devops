package com.tarion.vbs.dao.interest;

import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;

import static org.mockito.Mockito.mock;

public class InterestCalcDAOImplTest {

    InterestDAOImpl dao;

    @Before
    public void setUp() throws Exception {
        this.dao = new InterestDAOImpl();
        this.dao.vbsEm = mock(EntityManager.class);
    }

    @Test
    public void createRunForDate() {
        //        Mockito.when(mockRepository.count()).thenReturn(123L);
        //selects securities that don't have an interest calc on that day.

        //should throw exception if date is in the future.
        //should throw exception if date is not covered by an interest rate.

        //fields should be filled.
        //entity should have been persisted

        //if no exception above was thrown, there should be > 0 interest entities for the calc.

        this.dao.createRunForDate(LocalDateTime.now().toLocalDate());
    }

    @Test
    public void calculateRun() {
        //should throw if interest rate doesnt exist for date

        //interest should be persisted to entities
        //interest calc percent etc should be verified (use different mathematical method to cross-check)

        //calc status change?
    }

    @Test
    public void getInterestsForCalculationId() {
        //if calc doesnt exist, throw

        //if calc exists, must always have interests.

        //interest list should thus be not empty.
    }

    @Test
    public void updateSecurityAmounts() {
        //calc should throw

        //amounts should be updated

        //calc status change?

        //jms message?
    }

    @Test
    public void getInterestCalculation() {
        //throw if not exist.

        //all fields should always be populated.
    }
}