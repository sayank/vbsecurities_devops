/* 
 * 
 * InterestCalcDAOTest.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.interest;

import com.tarion.vbs.dao.test.Container;
import com.tarion.vbs.orm.entity.interest.InterestEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.Tuple;
import javax.transaction.UserTransaction;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class InterestCalcDAOTest {

	private UserTransaction userTransaction;
	private InterestDAO interestDAO;

	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		interestDAO = Container.getBean(InterestDAOImpl.class);
	}

	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}

	@Test
	public void test() {
		assertNotNull(interestDAO);
	}
	

	@Test
	public void testGetAllInterestRate() {
		List<Tuple> securities = interestDAO.getCashSecuritiesWithoutInterest(30);
		assertNotNull(securities);
		assertTrue(!securities.isEmpty());
	}
	
	@Test
	public void testGetLastCalculatedInterestForSecurity() {
		InterestEntity interest = interestDAO.getLastCalculatedInterestForSecurity(0);
		if (interest != null) {
			assertNotNull(interest);
		}
	}
}
