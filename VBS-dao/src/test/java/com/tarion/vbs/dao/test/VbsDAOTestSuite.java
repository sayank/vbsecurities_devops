package com.tarion.vbs.dao.test;

import com.tarion.vbs.dao.alert.AlertDAOTest;
import com.tarion.vbs.dao.contact.ContactDAOTest;
import com.tarion.vbs.dao.dashboard.DashboardDAOTest;
import com.tarion.vbs.dao.enrolment.EnrolmentDAOTest;
import com.tarion.vbs.dao.financialinstitution.FinancialInstitutionDAOTest;
import com.tarion.vbs.dao.history.HistoryDAOTest;
import com.tarion.vbs.dao.interest.InterestCalcDAOTest;
import com.tarion.vbs.dao.interest.InterestDAOTest;
import com.tarion.vbs.dao.release.AutoReleaseDAOTest;
import com.tarion.vbs.dao.release.ReleaseDAOTest;
import com.tarion.vbs.dao.security.SecurityDAOTest;
import com.tarion.vbs.dao.util.GenericDAOTest;
import com.tarion.vbs.dao.util.LookupDAOTest;
import com.tarion.vbs.dao.view.ViewDAOTest;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({ 
	GenericDAOTest.class
//	, SecurityDAOTest.class
//	, ReleaseDAOTest.class
//	, FinancialInstitutionDAOTest.class
//	, EnrolmentDAOTest.class
//	, InterestDAOTest.class
//	, InterestCalcDAOTest.class
//	, ContactDAOTest.class
//	, DashboardDAOTest.class
//	, LookupDAOTest.class
//	, AutoReleaseDAOTest.class
//	, AlertDAOTest.class
//	, HistoryDAOTest.class
//	, ViewDAOTest.class
})
public class VbsDAOTestSuite {
	

	@BeforeClass
	public static void setUp()
	{
		Container.getInstance().startTheContainer();
	}

	@AfterClass
	public static void tearDown()
	{
		Container.getInstance().stopTheContainer();
	}
}
