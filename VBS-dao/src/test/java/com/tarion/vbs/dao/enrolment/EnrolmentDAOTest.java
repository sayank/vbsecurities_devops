package com.tarion.vbs.dao.enrolment;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.enrolment.EnrolmentSecurityInfoDTO;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.dao.test.Container;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.dao.util.GenericDAOImpl;
import com.tarion.vbs.enrolment.EnrolmentDAO;
import com.tarion.vbs.enrolment.EnrolmentDAOImpl;
import com.tarion.vbs.orm.entity.contact.ContactEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentPoolEntity;
import com.tarion.vbs.orm.entity.security.PoolEntity;
import com.tarion.vbs.orm.entity.security.VbPoolEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.Tuple;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class EnrolmentDAOTest {
	private EnrolmentDAO enrolmentDAO;
	private GenericDAO genericDAO;
	private UserTransaction userTransaction;

	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {
			userTransaction.begin();
		} catch (Exception e) {
			e.printStackTrace();
		}
		enrolmentDAO = Container.getBean(EnrolmentDAOImpl.class);
		genericDAO = Container.getBean(GenericDAOImpl.class);
	}

	@After
	public void cleanUp() {
		try {
			userTransaction.rollback();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// TODO rewrite this test. IDs cannot be hardcoded in the test
//	@Test
//	public void testGetAllEnrolmetNumbersLike() {
//		assertTrue(enrolmentDAO.getAllEnrolmentsLike("H1000023").size() > 0);
//	}

	@Test
	public void testGetEnrolmentByPoolId() {
		ContactEntity vb1 = insertTestVb("B999999998");
		ContactEntity vb2 = insertTestVb("B999999999");
		PoolEntity pool = insertTestPool();
		addVbToPool(vb2, pool);

		EnrolmentEntity enrolment =
		insertTestEnrolment("H99991111", vb1, vb2, false, false);
		insertTestEnrolment("H99991112", vb1, vb2, true, false);
		insertTestEnrolment("H99991113", vb1, vb2, false, true);
		insertTestEnrolment("H99991114", vb1, vb2, true, true);

		List<Tuple> availableEnrolments = enrolmentDAO.getAvailableEnrolmentsByPoolId(pool.getId(), false, null);
		assertTrue(availableEnrolments.size() == 2);
		availableEnrolments = enrolmentDAO.getAvailableEnrolmentsByPoolId(pool.getId(), true, null);
		assertTrue(availableEnrolments.size() == 1);
		availableEnrolments = enrolmentDAO.getAvailableEnrolmentsByPoolId(pool.getId(), false, null);
		assertTrue(availableEnrolments.size() == 4);
		availableEnrolments = enrolmentDAO.getAvailableEnrolmentsByPoolId(pool.getId(), true,  null);
		assertTrue(availableEnrolments.size() == 2);


		EnrolmentPoolEntity ep = insertTestEnrolmentPool(enrolment, pool);
		List<String> enrolmentNumberList = new ArrayList<>();
		enrolmentNumberList.add(ep.getEnrolment().getEnrolmentNumber());
		List<EnrolmentPoolEntity> enrolmentPool = enrolmentDAO.getEnrolmentPoolByEnrolmentNumber(enrolmentNumberList, pool.getId());
		assertTrue(enrolmentPool.size() > 0);
		List<EnrolmentPoolEntity> enrolments = enrolmentDAO.getEnrolmentsByPoolId(pool.getId());
		assertTrue(enrolments.size() > 0);

	}

	private EnrolmentPoolEntity insertTestEnrolmentPool(EnrolmentEntity enrolment, PoolEntity pool) {
		EnrolmentPoolEntity newEP = new EnrolmentPoolEntity();
		BigDecimal testAmount = new BigDecimal("10.10");
		newEP.setVersion(1l);
		newEP.setEnrolment(enrolment);
		newEP.setPool(pool);
		newEP.setAllocatedAmount(testAmount);
		newEP.setClearanceLetterIssued(false);
		newEP.setExcessAmount(testAmount);
		newEP.setInventoryAmount(testAmount);
		newEP.setMaxUnitDeposit(testAmount);
		newEP.setPefAmount(testAmount);
		newEP.setRaAmount(testAmount);
		newEP.setCurrentAllocatedAmount(testAmount);
		newEP = (EnrolmentPoolEntity) genericDAO.addEntity(newEP, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
		return newEP;
	}

	private ContactEntity insertTestVb(String vbNumber) {
		ContactEntity vb = new ContactEntity();
		vb.setCrmContactId(vbNumber);
		vb.setCompanyName("UnitTestVb");
		vb = (ContactEntity) genericDAO.addEntity(vb, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
		return vb;
	}

	private PoolEntity insertTestPool() {
		PoolEntity newPool = new PoolEntity();
		newPool.setVersion(1l);
		newPool.setUnitsToCover(1);
		newPool = (PoolEntity) genericDAO.addEntity(newPool, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
		return newPool;
	}

	private VbPoolEntity addVbToPool(ContactEntity vb, PoolEntity pool) {
		VbPoolEntity newVbPool = new VbPoolEntity();
		newVbPool.setVersion(1l);
		newVbPool.setCreateDate(DateUtil.getLocalDateTimeNow());
		newVbPool.setCreateUser("user");
		newVbPool.setVersion(1L);
		newVbPool.setPool(pool);
		newVbPool.setVb(vb);
		newVbPool = (VbPoolEntity) genericDAO.addEntity(newVbPool, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
		return newVbPool;
	}

	private EnrolmentEntity insertTestEnrolment(String enrolmentNumber, ContactEntity vb1, ContactEntity vb2) {
		return insertTestEnrolment(enrolmentNumber, vb1, vb2, false, false);
	}

	private EnrolmentEntity insertTestEnrolment(String enrolmentNumber, ContactEntity vb1, ContactEntity vb2, boolean condoConv, boolean ceUnit) {
		EnrolmentEntity newEnrolment = new EnrolmentEntity();
		newEnrolment.setEnrolmentNumber(enrolmentNumber);
		newEnrolment.setVersion(1l);
		newEnrolment.setEnrollingVb(vb1);
		newEnrolment.setVendor(vb1);
		newEnrolment.setBuilder(vb2);
		newEnrolment.setCondoConversion(condoConv);
		newEnrolment.setUnitType(ceUnit ? VbsConstants.ENROLMENT_UNIT_TYPE_CE_UNIT : VbsConstants.ENROLMENT_UNIT_TYPE_FH);
		newEnrolment = (EnrolmentEntity) genericDAO.addEntity(newEnrolment, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
		return newEnrolment;
	}
	
	@Test
	public void testGetEnrolmentsForRelease() {
		List<EnrolmentEntity> enrolments = enrolmentDAO.getUnreleasedEnrolmentsForSecurity(2957l);
		assertTrue(!enrolments.isEmpty());
	}
	
	@Test
	public void testGetPoolUnitsToCoverByEnrolmentNumber() {
		long count = enrolmentDAO.getPoolUnitsToCoverByEnrolmentNumber("H1001288");
		assertTrue(count >= 0);
	}
	
	@Test
	public void testGetEnrolmentSecurityInfo() {
		List<String> enrolments = Arrays.asList(new String[]{"H859208","H655637","H816271","H689183","H668121","H668120"});
		List<EnrolmentSecurityInfoDTO> data = enrolmentDAO.getEnrolmentSecurityInfo(enrolments);

		assertNotNull(data);
	}
	
	@Test
	public void testGetCurrentEnrolmentsIn() {
		List<String> enrolments = Arrays.asList(new String[]{"H859208","H655637","H816271","H689183","H668121","H668120"});
		List<String> currentEnrolments = enrolmentDAO.getCurrentEnrolmentsIn(enrolments);
		assertNotNull(currentEnrolments);
	}
	
	@Test
	public void testGetUnreleasedEnrolmentsForSecurity() {
		List<EnrolmentEntity> unreleasedEnrolments = enrolmentDAO.getUnreleasedEnrolmentsForSecurity(1l);
		assertNotNull(unreleasedEnrolments);
	}

}
