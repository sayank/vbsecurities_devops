/* 
 * 
 * ContactDAOTest.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.dao.test.Container;
import com.tarion.vbs.orm.entity.contact.LicenseStatusEntity;

public class LookupDAOTest {

	private UserTransaction userTransaction;
	private LookupDAO lookupDAO;

	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		lookupDAO = Container.getBean(LookupDAOImpl.class);
	}

	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}

	@Test
	public void test() {
		assertNotNull(lookupDAO);
	}
	

	
	@Test
	public void testGetLicenseStatusForName() {
		LicenseStatusEntity licenseStatus = lookupDAO.getLicenseStatusForName("act");
		assertTrue(licenseStatus.getId() == 9l);
	}
	
	
}
