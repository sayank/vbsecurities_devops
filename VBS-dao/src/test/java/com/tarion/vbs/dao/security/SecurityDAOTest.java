/* 
 * 
 * VbDAOTest.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.dao.security;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import com.tarion.vbs.common.dto.tip.SecurityTipInfoDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.security.DeletedSecuritySearchParametersDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchParametersDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchResultDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchResultWrapper;
import com.tarion.vbs.common.enums.SearchTypeEnum;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.dao.contact.ContactDAO;
import com.tarion.vbs.dao.contact.ContactDAOImpl;
import com.tarion.vbs.dao.test.Container;
import com.tarion.vbs.dao.test.UnitTestObjectCreatorUtil;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.dao.util.GenericDAOImpl;
import com.tarion.vbs.orm.entity.financialinstitution.BranchEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionTypeEntity;
import com.tarion.vbs.orm.entity.security.PoolEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import com.tarion.vbs.orm.entity.security.SecurityPurposeEntity;
import com.tarion.vbs.orm.entity.security.SecurityTypeEntity;

public class SecurityDAOTest {

	private ContactDAO contactDAO;
	private GenericDAO genericDAO;
	private UserTransaction userTransaction;
	private SecurityDAO securityDAO;

	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		contactDAO = Container.getBean(ContactDAOImpl.class);
		genericDAO = Container.getBean(GenericDAOImpl.class);
		securityDAO = Container.getBean(SecurityDAOImpl.class);
	}

	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}

	@Test
	public void test() {
		assertNotNull(contactDAO);
	}
	
	@Test
	public void testAddSecurity() {
		SecurityTypeEntity securityType = genericDAO.findEntityById(SecurityTypeEntity.class, 1l);
		SecurityPurposeEntity securityPurpose = genericDAO.findEntityById(SecurityPurposeEntity.class, 1l);
		SecurityEntity security = UnitTestObjectCreatorUtil.createSecurityEntity(securityType, securityPurpose);
		
		security = (SecurityEntity)genericDAO.addEntity(security, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
		security.setPool(genericDAO.findEntityById(PoolEntity.class, 1l));
		genericDAO.updateEntity(security, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
		
		testDuplicateInstrumentNumber(security);
	}
	
	private void testDuplicateInstrumentNumber(SecurityEntity security) {
		
		FinancialInstitutionTypeEntity type = genericDAO.findEntityById(FinancialInstitutionTypeEntity.class, 1l);
		FinancialInstitutionEntity fin = UnitTestObjectCreatorUtil.createFinancialInstitutionEntity(type);
		fin = (FinancialInstitutionEntity) genericDAO.addEntity(fin, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
		BranchEntity branch = UnitTestObjectCreatorUtil.createBranchEntity(fin);
		branch = (BranchEntity) genericDAO.addEntity(branch, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
		security.setBranch(branch);
		genericDAO.updateEntity(security, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
		
		List<Long> result = securityDAO.getSecurityIdsWithInstrumentNumberFinancialInst(security.getInstrumentNumber(), fin.getId());
		assertTrue(result.stream().anyMatch(id -> security.getId().longValue() == id.longValue()));
	}

	// TODO rewrite this test. IDs cannot be hardcoded in the test
//	@Test
//	public void testGetPool() {
//		assertTrue(securityDAO.getPoolForSecurityById(38422L) != null);
//	}

	@Test
	public void testGetAllInstrumentNumbersLike() {
		assertTrue(securityDAO.getAllInstrumentNumbersLike("SB").size() > 1);
	}
	
	// TODO rewrite this test. IDs cannot be hardcoded in the test
//	@Test
//	public void testGetAllVbNumbersLike() {
//		assertTrue(securityDAO.getAllVbNumbersLike("B12").size() > 1);
//	}

	@Test
	public void testGetAllEnrolmentsLike() {
		// TODO
//		assertTrue(securityDAO.getAllEnrolmentsLike("H").size() > 1);
	}
	@Test
	public void testGetAllEscrowAgentNamesLike() {
		// TODO
//		assertTrue(securityDAO.getAllEscrowAgentNamesLike("John").size() > 1);
	}
	
	@Test
	public void testSearch() {
		SecuritySearchParametersDTO searchParams = new SecuritySearchParametersDTO();
		searchParams.setVbNumber("B12");
		searchParams.setPoolId(2l);
		searchParams.setVbNumberType(SearchTypeEnum.EQUALS);
		searchParams.setFinancialInstitutionTypeId(4l);
		SecuritySearchResultWrapper result = securityDAO.searchSecurity(searchParams, true);
		assertNotNull(result);
	}

	@Test
	public void testGetSecurityByVBNumberBlanket() {
		List<SecurityTipInfoDTO> result = securityDAO.getSecurityByVbNumberBlanket("B46560");
		assertNotNull(result);
	}

	@Test
	public void testSearchDeletedSecurity() {
		DeletedSecuritySearchParametersDTO searchParams = new DeletedSecuritySearchParametersDTO();
		searchParams.setUpdateDateType(SearchTypeEnum.BETWEEN);
		searchParams.setUpdateDate(DateUtil.getLocalDateTimeNow().minusYears(3));
		searchParams.setUpdateDateBetweenTo(DateUtil.getLocalDateTimeNow());
		List<SecuritySearchResultDTO> result = securityDAO.searchDeletedSecurity(searchParams);
		assertNotNull(result);
//		assertTrue(result.size() > 0);
	}
}
