CREATE TABLE [dbo].[CE_ENROLMENT_DECISION](
  ENROLMENT_NUMBER varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  APPROVAL_DECISION varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  APPROVAL_ANALYST varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  APPROVAL_DATE datetime2 NULL,
  APPROVAL_REASON varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  CONSTRAINT [PK_CE_ENROLMENT_DECISION] PRIMARY KEY CLUSTERED
      (
       [ENROLMENT_NUMBER] ASC
          )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


INSERT INTO VBS_CONFIG (NAME, DESCRIPTION, VALUE) VALUES ('security.group.email', 'Email address for the Security Group', 'security@tariondev.com');
INSERT INTO VBS_CONFIG (NAME, DESCRIPTION, VALUE) VALUES ('ce.blanket.email.subject', 'CE Blanket Email Subject', 'CE %s added to Blanket');
INSERT INTO VBS_CONFIG (NAME, DESCRIPTION, VALUE) VALUES ('ce.blanket.email.body', 'CE Blanket Email Body', 'Enrolment Number: %s<br/>Enrolling VB: %s - %s<br/>Pool Number(s): %s<br/>Added By: %s at %s<br/>');