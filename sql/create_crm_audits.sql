--replace occurrences of DEV9PPSDBD01.CRDEV3
SET NOCOUNT ON

IF OBJECT_ID(N'dbo.DATAMIG_PSAUDIT') IS NOT NULL
    BEGIN
        DROP TABLE dbo.DATAMIG_PSAUDIT
    END

GO

SELECT *
INTO dbo.DATAMIG_PSAUDIT
FROM VTST3CRMDBD01.CRTST3.dbo.PSAUDIT y
WHERE y.RECNAME IN (
                    'TWC_SECURITY'
    ,'TWC_SEC_POOL'
    ,'TWC_SECURITY_VB'
    ,'TWC_SEC_HOME'
    ,'TWC_SEC_RELEASE'
    ,'TWC_FIN_INST_CD'
    )

GO

SET IDENTITY_INSERT [dbo].[POOL] ON;

WITH MISSING_POOLS (POOL_ID) AS (
    SELECT DISTINCT(CAST(OLDVALUE as INT)) FROM DATAMIG_PSAUDIT WHERE FIELDNAME = 'TWC_POOL_NBR' AND OLDVALUE != '' AND NOT EXISTS(SELECT ID FROM POOL WHERE ID = OLDVALUE)
    UNION
    SELECT DISTINCT(CAST(NEWVALUE as INT)) FROM DATAMIG_PSAUDIT WHERE FIELDNAME = 'TWC_POOL_NBR' AND OLDVALUE != '' AND NOT EXISTS(SELECT ID FROM POOL WHERE ID = NEWVALUE)
)

INSERT INTO POOL (ID, VERSION, UNITS_TO_COVER, FH_UNITS_TO_COVER, CE_UNITS_TO_COVER, CREATE_DATE, CREATE_USER, UPDATE_DATE, UPDATE_USER)
SELECT
    P.POOL_ID AS ID,
    0 AS VERSION,
    COALESCE((SELECT D.NEWVALUE FROM DATAMIG_PSAUDIT D WHERE D.FIELDNAME = 'TWC_UNITS_TO_COVER' AND D.KEY1 = P.POOL_ID AND D.AUDIT_STAMP = (SELECT MIN(D2.AUDIT_STAMP) FROM DATAMIG_PSAUDIT D2 WHERE D.FIELDNAME = 'TWC_UNITS_TO_COVER' AND D.KEY1 = D2.KEY1)), 0) AS UNITS_TO_COVER,
    NULL AS FH_UNITS_TO_COVER,
    NULL AS CE_UNITS_TO_COVER,
    (SELECT MIN(AUDIT_STAMP) FROM DATAMIG_PSAUDIT WHERE FIELDNAME = 'TWC_POOL_NBR' AND (OLDVALUE = P.POOL_ID OR NEWVALUE = P.POOL_ID)) AS CREATE_DATE,
    (SELECT AUDIT_OPRID FROM DATAMIG_PSAUDIT WHERE FIELDNAME = 'TWC_POOL_NBR' AND AUDIT_STAMP = (SELECT MIN(AUDIT_STAMP) FROM DATAMIG_PSAUDIT WHERE FIELDNAME = 'TWC_POOL_NBR' AND (OLDVALUE = P.POOL_ID OR NEWVALUE = P.POOL_ID))) AS CREATE_USER,
    (SELECT MAX(AUDIT_STAMP) FROM DATAMIG_PSAUDIT WHERE FIELDNAME = 'TWC_POOL_NBR' AND (OLDVALUE = P.POOL_ID OR NEWVALUE = P.POOL_ID)) AS UPDATE_DATE,
    (SELECT AUDIT_OPRID FROM DATAMIG_PSAUDIT WHERE FIELDNAME = 'TWC_POOL_NBR' AND AUDIT_STAMP = (SELECT MAX(AUDIT_STAMP) FROM DATAMIG_PSAUDIT WHERE FIELDNAME = 'TWC_POOL_NBR' AND (OLDVALUE = P.POOL_ID OR NEWVALUE = P.POOL_ID))) AS UPDATE_USER
FROM MISSING_POOLS P;
SET IDENTITY_INSERT [dbo].[POOL] OFF;

GO

--create missing VB POOL Entries
DECLARE @vbPoolNum INT;
DECLARE @vbContactId INT;
DECLARE @vbNumber VARCHAR(10);
DECLARE @sql NVARCHAR(MAX);
DECLARE @missing TABLE (POOL_ID INT, CONTACT_ID INT, VB_NUMBER VARCHAR(30));

DECLARE @missingVbPools CURSOR;

WITH PSAUDIT (POOL_ID, VB_NUMBER) AS (
    SELECT KEY1, KEY2 FROM VTST3CRMDBD01.CRTST3.dbo.PSAUDIT WHERE FIELDNAME = 'COMPANYID' GROUP BY KEY1, KEY2
), VBS (POOL_ID, CONTACT_ID, VB_NUMBER) AS (
    SELECT
        P.POOL_ID,
        C.ID,
        C.CRM_CONTACT_ID
    FROM PSAUDIT P
             JOIN CONTACT C ON C.CRM_CONTACT_ID = P.VB_NUMBER COLLATE DATABASE_DEFAULT
), MISSING (POOL_ID, CONTACT_ID, VB_NUMBER) AS (
    SELECT * FROM VBS V WHERE NOT EXISTS (SELECT * FROM VB_POOL P WHERE P.POOL_ID = V.POOL_ID AND P.VB_CONTACT_ID = V.CONTACT_ID)
)

INSERT INTO @missing SELECT * FROM MISSING;

SET @missingVbPools = CURSOR FOR SELECT * FROM @missing;
OPEN @missingVbPools;
FETCH NEXT
    FROM @missingVbPools INTO @vbPoolNum, @vbContactId, @vbNumber;
WHILE @@FETCH_STATUS = 0
BEGIN

    SET @sql =
                '
                INSERT INTO VB_POOL (VERSION, VB_CONTACT_ID, POOL_ID, VB_DELETE_REASON_ID, CREATE_DATE, CREATE_USER, UPDATE_DATE, UPDATE_USER)
                SELECT * FROM OPENQUERY(VTST3CRMDBD01, ''
                    SELECT
                        TOP 1
                        (SELECT COUNT(*) FROM CRTST3.dbo.PSAUDIT WHERE P1.FIELDNAME = FIELDNAME AND P1.KEY1 = KEY1 AND P1.KEY2 = KEY2) AS VERSION,
                        '+CAST(@vbContactId as VARCHAR)+' AS VB_CONTACT_ID,
				P1.KEY1 AS POOL_ID,
				CASE P1.AUDIT_ACTN WHEN ''''A'''' THEN NULL WHEN ''''C'''' THEN 4 WHEN ''''D'''' THEN 2 END AS VB_DELETE_REASON_ID,
				P2.AUDIT_STAMP AS CREATE_DATE,
				P2.AUDIT_OPRID AS CREATE_USER,
				P1.AUDIT_STAMP AS UPDATE_DATE,
				P1.AUDIT_OPRID AS UPDATE_USER
			FROM CRTST3.dbo.PSAUDIT P1
			JOIN CRTST3.dbo.PSAUDIT P2 ON P1.FIELDNAME = P2.FIELDNAME AND P1.KEY1 = P2.KEY1 AND P1.KEY2 = P2.KEY2
			AND P2.AUDIT_STAMP = (SELECT MIN(P3.AUDIT_STAMP) FROM CRTST3.dbo.PSAUDIT P3 WHERE P2.FIELDNAME = P3.FIELDNAME AND P2.KEY1 = P3.KEY1 AND P2.KEY2 = P3.KEY2 AND P2.AUDIT_ACTN = P3.AUDIT_ACTN)
			WHERE P1.FIELDNAME = ''''COMPANYID'''' AND P1.KEY1 = '+CAST(@vbPoolNum AS VARCHAR)+' AND P1.KEY2 = '''''+@vbNumber+'''''
			ORDER BY P1.AUDIT_STAMP DESC
		'')';

    EXEC sp_executesql @sql;

    FETCH NEXT FROM @missingVbPools INTO @vbPoolNum, @vbContactId, @vbNumber;
END;
CLOSE @missingVbPools;
DEALLOCATE @missingVbPools;

GO

SET IDENTITY_INSERT REVINFO ON
INSERT INTO REVINFO (REV, REVTSTMP) VALUES (0, 0);
SET IDENTITY_INSERT REVINFO OFF

GO

--add audit stamp if it doesn't exist
DECLARE @auditTables CURSOR;
DECLARE @tableName VARCHAR(200);
DECLARE @sql VARCHAR(max);
SET @auditTables = CURSOR FOR SELECT name FROM sys.tables WHERE NAME LIKE '%_AUD%' AND NAME NOT LIKE '%DATAMIG%';
OPEN @auditTables;
FETCH NEXT
    FROM @auditTables INTO @tableName;
WHILE @@FETCH_STATUS = 0
BEGIN
    IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @tableName AND COLUMN_NAME = 'AUDIT_STAMP')
        BEGIN
            SET @sql = 'ALTER TABLE ' + @tableName + ' ADD AUDIT_STAMP datetime2 NULL';
            EXEC(@sql);
        END;

    FETCH NEXT FROM @auditTables INTO @tableName;
END;
CLOSE @auditTables;
DEALLOCATE @auditTables;

--create audits for all tables excluding SECURITY/VB_POOL/POOL_AUD/FINANCIAL_INSTITUTION_AUD
DECLARE @cols CURSOR;
DECLARE @mainTable VARCHAR(200);
DECLARE @selectSQL VARCHAR(max);
DECLARE @colName VARCHAR(max);

SET @auditTables = CURSOR FOR SELECT name FROM sys.tables WHERE NAME LIKE '%_AUD%' AND NAME NOT LIKE '%DATAMIG%' AND NAME NOT IN ('SECURITY_AUD', 'VB_POOL_AUD', 'POOL_AUD', 'FINANCIAL_INSTITUTION_AUD');

OPEN @auditTables;
FETCH NEXT
    FROM @auditTables INTO @tableName;
WHILE @@FETCH_STATUS = 0
BEGIN
    SET @selectSQL = 'SELECT ';
    SET @mainTable = (SELECT REPLACE(@tableName, '_AUD', ''));

    SET @cols = CURSOR FOR SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION ASC;
    OPEN @cols;
    FETCH NEXT
        FROM @cols INTO @colName;
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @selectSQL = @selectSQL + (
            SELECT CASE
                       WHEN @colName = 'AUDIT_STAMP' THEN 'CREATE_DATE AS AUDIT_STAMP '
                       WHEN @colName LIKE '%_MOD' THEN '1 AS '+@colName+', '
                       WHEN @colName IN ('REV', 'REVTYPE') THEN '0 AS '+@colName+', '
                       ELSE @colName + ', '
                       END
        );
        FETCH NEXT FROM @cols INTO @colName;
    END;
    CLOSE @cols;
    DEALLOCATE @cols;
    SET @selectSQL = 'INSERT INTO '+@tableName+' ' + @selectSQL + 'FROM '+@mainTable;
    EXEC(@selectSQL);

    FETCH NEXT FROM @auditTables INTO @tableName;
END;
CLOSE @auditTables;
DEALLOCATE @auditTables;

GO
ALTER TABLE SECURITY_AUD DROP CONSTRAINT PK_SECURITY_AUD
GO
ALTER TABLE VB_POOL_AUD DROP CONSTRAINT PK_VB_POOL_AUD
GO
ALTER TABLE POOL_AUD DROP CONSTRAINT PK_POOL_AUD
GO
ALTER TABLE FINANCIAL_INSTITUTION_AUD DROP CONSTRAINT PK_FINANCIAL_INSTITUTION_AUD
GO

DECLARE @psAudits TABLE (
                            [AUDIT_OPRID] [varchar](30) NOT NULL,
                            [AUDIT_STAMP] [datetime] NULL,
                            [AUDIT_ACTN] [varchar](1) NOT NULL,
                            [RECNAME] [varchar](15) NOT NULL,
                            [FIELDNAME] [varchar](18) NOT NULL,
                            [OLDVALUE] [varchar](65) NOT NULL,
                            [NEWVALUE] [varchar](65) NOT NULL,
                            [KEY1] [varchar](65) NOT NULL
                        );
DECLARE @versionAuditStamp datetime2 = NULL;
DECLARE @versionAuditStampPlusOne datetime2 = NULL;
DECLARE @fiVersions TABLE (VERSION int, AUDIT_STAMP datetime2);
DECLARE @fiAudits TABLE (
                            [ID] [bigint] NULL,
                            [REV] [int] NOT NULL,
                            [REVTYPE] [smallint] NULL,
                            [VERSION] [bigint] NULL,
                            [version_MOD] [bit] NULL,
                            [FINANCIAL_INSTITUTION_TYPE_ID] [int] NULL,
                            [financialInstitutionType_MOD] [bit] NULL,
                            [NAME] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                            [name_MOD] [bit] NULL,
                            [DESCRIPTION] [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                            [description_MOD] [bit] NULL,
                            [EXPIRY_DATE] [datetime2](7) NULL,
                            [expiryDate_MOD] [bit] NULL,
                            [MAX_SECURITY_AMOUNT] [MONEY] NULL,
                            [maxSecurityAmount_MOD] [bit] NULL,
                            [WN_SECURITY_MAX_AMOUNT] [MONEY] NULL,
                            [wnSecurityMaxAmount_MOD] [bit] NULL,
                            [CREATE_DATE] [datetime2](7) NULL,
                            [createDate_MOD] [bit] NULL,
                            [CREATE_USER] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                            [createUser_MOD] [bit] NULL,
                            [UPDATE_DATE] [datetime2](7) NULL,
                            [updateDate_MOD] [bit] NULL,
                            [UPDATE_USER] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                            [updateUser_MOD] [bit] NULL,
                            [AUDIT_STAMP] [datetime2] NULL );

DECLARE @fiNum INT;
DECLARE @financialInstitutions CURSOR;
DECLARE @version INT;
SET @financialInstitutions = CURSOR FOR SELECT ID FROM FINANCIAL_INSTITUTION ORDER BY ID DESC;
OPEN @financialInstitutions;
FETCH NEXT
    FROM @financialInstitutions INTO @fiNum
WHILE @@FETCH_STATUS = 0
BEGIN
    DELETE FROM @fiVersions;
    DELETE FROM @psAudits;

    WITH STAMPS (AUDIT_STAMP) AS (
        SELECT DISTINCT(AUDIT_STAMP) FROM DATAMIG_PSAUDIT WHERE FIELDNAME IN ('TWC_WN_SEC_MAX_AMT','TWC_MAX_SEC_AMT','EXPIRY_DATE') AND KEY1 = @fiNum AND AUDIT_ACTN = 'C'
    ), VERSIONS (VERSION, AUDIT_STAMP) AS (
        SELECT
            CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM STAMPS WHERE AUDIT_STAMP = (SELECT CREATE_DATE FROM FINANCIAL_INSTITUTION WHERE ID = @fiNum)) THEN
                     ROW_NUMBER() OVER (ORDER BY AUDIT_STAMP ASC) ELSE (ROW_NUMBER() OVER (ORDER BY AUDIT_STAMP ASC) + 1) END AS VERSION,
            AUDIT_STAMP
        FROM STAMPS
    )

    INSERT INTO @fiVersions SELECT * FROM VERSIONS;
    INSERT INTO @psAudits
    SELECT AUDIT_OPRID, AUDIT_STAMP, AUDIT_ACTN, RECNAME, FIELDNAME, OLDVALUE, NEWVALUE, KEY1
    FROM DATAMIG_PSAUDIT WHERE FIELDNAME IN ('TWC_WN_SEC_MAX_AMT','TWC_MAX_SEC_AMT','EXPIRY_DATE') AND KEY1 = @fiNum;

    SET @version = COALESCE((SELECT MAX(VERSION) FROM @fiVersions), 1);
    WHILE @version > 0
    BEGIN
        SET @versionAuditStamp = (SELECT AUDIT_STAMP FROM @fiVersions WHERE VERSION = @version);
        IF EXISTS(SELECT ID FROM @fiAudits WHERE ID = @fiNum AND VERSION = (@version+1))
            BEGIN
                SET @versionAuditStampPlusOne = (SELECT AUDIT_STAMP FROM @fiVersions WHERE VERSION = (@version+1));
                INSERT INTO @fiAudits (ID, REV, REVTYPE, VERSION, version_MOD,
                                       FINANCIAL_INSTITUTION_TYPE_ID, financialInstitutionType_MOD,
                                       NAME, name_MOD, DESCRIPTION, description_MOD, EXPIRY_DATE, expiryDate_MOD, MAX_SECURITY_AMOUNT,
                                       maxSecurityAmount_MOD, WN_SECURITY_MAX_AMOUNT, wnSecurityMaxAmount_MOD, CREATE_DATE, createDate_MOD, CREATE_USER, createUser_MOD,
                                       UPDATE_DATE, updateDate_MOD, UPDATE_USER, updateUser_MOD, AUDIT_STAMP)
                SELECT
                    F.ID,
                    0,
                    CASE WHEN @version = 1 THEN 0 ELSE 1 END,
                    @version,
                    1,
                    F.FINANCIAL_INSTITUTION_TYPE_ID,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    F.NAME,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    F.DESCRIPTION,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    CASE WHEN @version = 1 AND EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'EXPIRY_DATE' AND AUDIT_ACTN = 'A')
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'EXPIRY_DATE' AND AUDIT_ACTN = 'A')
                         ELSE
                             CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'EXPIRY_DATE' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                      THEN (SELECT OLDVALUE FROM @psAudits WHERE FIELDNAME = 'EXPIRY_DATE' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                  ELSE F.EXPIRY_DATE END
                        END,
                    CASE WHEN @version = 1 THEN 1 ELSE
                        CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'EXPIRY_DATE' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END
                        END,

                    CASE WHEN @version = 1 AND EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_MAX_SEC_AMT' AND AUDIT_ACTN = 'A')
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_MAX_SEC_AMT' AND AUDIT_ACTN = 'A')
                         ELSE
                             CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_MAX_SEC_AMT' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                      THEN (SELECT OLDVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_MAX_SEC_AMT' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                  ELSE F.MAX_SECURITY_AMOUNT END
                        END,
                    CASE WHEN @version = 1 THEN 1 ELSE
                        CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_MAX_SEC_AMT' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END
                        END,

                    CASE WHEN @version = 1 AND EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_WN_SEC_MAX_AMT' AND AUDIT_ACTN = 'A')
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_WN_SEC_MAX_AMT' AND AUDIT_ACTN = 'A')
                         ELSE
                             CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_WN_SEC_MAX_AMT' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                      THEN (SELECT OLDVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_WN_SEC_MAX_AMT' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                  ELSE F.WN_SECURITY_MAX_AMOUNT END
                        END,
                    CASE WHEN @version = 1 THEN 1 ELSE
                        CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_WN_SEC_MAX_AMT' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END
                        END,
                    F.CREATE_DATE,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    F.CREATE_USER,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,

                    CASE WHEN @version = 1 THEN NULL ELSE @versionAuditStamp END,
                    1,
                    CASE WHEN @version = 1 THEN NULL ELSE (SELECT TOP 1 AUDIT_OPRID FROM @psAudits WHERE AUDIT_STAMP = @versionAuditStamp) END,
                    CASE
                        WHEN @version = 1 THEN NULL
                        WHEN ((SELECT TOP 1 AUDIT_OPRID FROM @psAudits WHERE AUDIT_STAMP = @versionAuditStamp) != (SELECT TOP 1 AUDIT_OPRID FROM @psAudits WHERE AUDIT_STAMP = (SELECT AUDIT_STAMP FROM @fiVersions WHERE VERSION = (@version-1)))) THEN 1
                        ELSE 0
                        END,
                    CASE WHEN @version = 1 THEN (SELECT CREATE_DATE FROM DATAMIG_FINANCIAL_INSTITUTION WHERE ID = F.ID) ELSE	@versionAuditStamp END
                FROM @fiAudits F
                WHERE F.ID = @fiNum AND F.VERSION = (@version+1)
            END
        ELSE
            BEGIN
                INSERT INTO @fiAudits (ID, REV, REVTYPE, VERSION, version_MOD,
                                       FINANCIAL_INSTITUTION_TYPE_ID, financialInstitutionType_MOD,
                                       NAME, name_MOD, DESCRIPTION, description_MOD, EXPIRY_DATE, expiryDate_MOD, MAX_SECURITY_AMOUNT,
                                       maxSecurityAmount_MOD, WN_SECURITY_MAX_AMOUNT, wnSecurityMaxAmount_MOD, CREATE_DATE, createDate_MOD, CREATE_USER, createUser_MOD,
                                       UPDATE_DATE, updateDate_MOD, UPDATE_USER, updateUser_MOD, AUDIT_STAMP)
                SELECT
                    F.ID,
                    0,
                    CASE WHEN @version = 1 THEN 0 ELSE 1 END, --REVTYPE
                    @version,
                    1, --version_MOD
                    F.FINANCIAL_INSTITUTION_TYPE_ID,
                    0, --financialInstitutionType_MOD
                    F.NAME,
                    0, --name_MOD
                    F.DESCRIPTION,
                    0, --description_MOD
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'EXPIRY_DATE' AND AUDIT_STAMP = @versionAuditStamp)
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'EXPIRY_DATE' AND AUDIT_STAMP = @versionAuditStamp)
                         ELSE F.EXPIRY_DATE
                        END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'EXPIRY_DATE' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END,

                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_MAX_SEC_AMT' AND AUDIT_STAMP = @versionAuditStamp)
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_MAX_SEC_AMT' AND AUDIT_STAMP = @versionAuditStamp)
                         ELSE F.MAX_SECURITY_AMOUNT
                        END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_MAX_SEC_AMT' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END,

                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_WN_SEC_MAX_AMT' AND AUDIT_STAMP = @versionAuditStamp)
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_WN_SEC_MAX_AMT' AND AUDIT_STAMP = @versionAuditStamp)
                         ELSE F.WN_SECURITY_MAX_AMOUNT
                        END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_WN_SEC_MAX_AMT' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END,

                    F.CREATE_DATE,
                    0,
                    F.CREATE_USER,
                    0,
                    F.UPDATE_DATE,
                    0,
                    F.UPDATE_USER,
                    0,
                    COALESCE(@versionAuditStamp, F.CREATE_DATE)
                FROM FINANCIAL_INSTITUTION F
                WHERE
                        F.ID = @fiNum
            END
        SET @version = @version-1;
    END;
    FETCH NEXT FROM @financialInstitutions INTO @fiNum;
END;
CLOSE @financialInstitutions;
DEALLOCATE @financialInstitutions;

INSERT INTO FINANCIAL_INSTITUTION_AUD
SELECT * FROM @fiAudits ORDER BY AUDIT_STAMP

GO

CREATE NONCLUSTERED INDEX [DATAMIG_PSAUDIT_VB_POOL] ON [dbo].[DATAMIG_PSAUDIT] ([FIELDNAME],[KEY2],[AUDIT_ACTN])

GO


DECLARE @psAudits TABLE (
                            [AUDIT_OPRID] [varchar](30) NOT NULL,
                            [AUDIT_STAMP] [datetime] NULL,
                            [AUDIT_ACTN] [varchar](1) NOT NULL,
                            [RECNAME] [varchar](15) NOT NULL,
                            [FIELDNAME] [varchar](18) NOT NULL,
                            [OLDVALUE] [varchar](65) NOT NULL,
                            [NEWVALUE] [varchar](65) NOT NULL,
                            [KEY1] [varchar](65) NOT NULL,
                            [KEY2] [varchar](65) NOT NULL
                        );

DECLARE @versionAuditStamp datetime2 = NULL;
DECLARE @versionAuditStampPlusOne datetime2 = NULL;
DECLARE @vbPoolVersions TABLE (VERSION int, AUDIT_STAMP datetime2);
DECLARE @vbPoolAudits TABLE (
                                [ID] [bigint] NOT NULL,
                                [REV] [int] NOT NULL,
                                [REVTYPE] [smallint] NULL,
                                [VERSION] [bigint] NULL,
                                [version_MOD] [bit] NULL,
                                [VB_CONTACT_ID] [int] NULL,
                                [vb_MOD] [bit] NULL,
                                [POOL_ID] [int] NULL,
                                [pool_MOD] [bit] NULL,
                                [VB_DELETE_REASON_ID] [int] NULL,
                                [vbDeleteReason_MOD] [bit] NULL,
                                [CREATE_DATE] [datetime2](7) NULL,
                                [createDate_MOD] [bit] NULL,
                                [CREATE_USER] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                [createUser_MOD] [bit] NULL,
                                [UPDATE_DATE] [datetime2](7) NULL,
                                [updateDate_MOD] [bit] NULL,
                                [UPDATE_USER] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                [updateUser_MOD] [bit] NULL,
                                [AUDIT_STAMP] [datetime2] NULL );


DECLARE @vbPoolNum INT;
DECLARE @poolNum INT;
DECLARE @vbNumber VARCHAR(10);
DECLARE @vbPools CURSOR;
DECLARE @version INT;
SET @vbPools = CURSOR FOR SELECT P.ID, P.POOL_ID, C.CRM_CONTACT_ID FROM VB_POOL P JOIN CONTACT C ON P.VB_CONTACT_ID = C.ID ORDER BY P.ID DESC;
OPEN @vbPools;
FETCH NEXT
    FROM @vbPools INTO @vbPoolNum, @poolNum, @vbNumber;
WHILE @@FETCH_STATUS = 0
BEGIN
    DELETE FROM @vbPoolVersions;
    DELETE FROM @psAudits;
    WITH STAMPS (AUDIT_STAMP) AS (
        SELECT DISTINCT(AUDIT_STAMP) FROM DATAMIG_PSAUDIT WHERE FIELDNAME IN ('COMPANYID') AND
                OLDVALUE != NEWVALUE AND
                KEY1 = @poolNum AND
                KEY2 = @vbNumber
                                                            AND AUDIT_ACTN IN ('C', 'A', 'D')
    ), VERSIONS (VERSION, AUDIT_STAMP) AS (
        SELECT
            CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM STAMPS WHERE AUDIT_STAMP = (SELECT CREATE_DATE FROM VB_POOL WHERE ID = @vbPoolNum))
                AND (SELECT CREATE_DATE FROM VB_POOL WHERE ID = @vbPoolNum) <= (SELECT MIN(AUDIT_STAMP) FROM STAMPS)
                     THEN
                     ROW_NUMBER() OVER (ORDER BY AUDIT_STAMP ASC) ELSE (ROW_NUMBER() OVER (ORDER BY AUDIT_STAMP ASC) + 1) END AS VERSION,
            AUDIT_STAMP
        FROM STAMPS
    )
    INSERT INTO @vbPoolVersions SELECT * FROM VERSIONS;
    INSERT INTO @psAudits SELECT AUDIT_OPRID, AUDIT_STAMP, AUDIT_ACTN, RECNAME, FIELDNAME, OLDVALUE, NEWVALUE, KEY1, KEY2 FROM DATAMIG_PSAUDIT WHERE FIELDNAME IN ('COMPANYID') AND KEY1 = @poolNum AND KEY2 = @vbNumber AND OLDVALUE != NEWVALUE;

    SET @version = COALESCE((SELECT MAX(VERSION) FROM @vbPoolVersions), 1);
    WHILE @version > 0
    BEGIN
        SET @versionAuditStamp = (SELECT AUDIT_STAMP FROM @vbPoolVersions WHERE VERSION = @version);
        IF EXISTS(SELECT ID FROM @vbPoolAudits WHERE ID = @vbPoolNum AND VERSION = (@version+1))
            BEGIN
                SET @versionAuditStampPlusOne = (SELECT AUDIT_STAMP FROM @vbPoolVersions WHERE VERSION = (@version+1));

                INSERT INTO @vbPoolAudits (
                    ID, REV, REVTYPE, VERSION, version_MOD,
                    VB_CONTACT_ID, vb_MOD, POOL_ID, pool_MOD, VB_DELETE_REASON_ID, vbDeleteReason_MOD,
                    CREATE_DATE, createDate_MOD, CREATE_USER, createUser_MOD, UPDATE_DATE, updateDate_MOD, UPDATE_USER, updateUser_MOD, AUDIT_STAMP)
                SELECT
                    P.ID,
                    0,
                    CASE WHEN @version = 1 THEN 0 ELSE 1 END,
                    @version,
                    1,
                    P.VB_CONTACT_ID,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    P.POOL_ID,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    CASE WHEN @version = 1 THEN NULL
                         ELSE
                             CASE
                                 WHEN (SELECT OLDVALUE FROM @psAudits WHERE FIELDNAME = 'COMPANYID' AND AUDIT_STAMP = @versionAuditStamp) = '' THEN NULL
                                 WHEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'COMPANYID' AND AUDIT_STAMP = @versionAuditStamp AND AUDIT_ACTN = 'D') = '' THEN 2
                                 ELSE 4
                                 END
                        END,
                    CASE WHEN @version = 1 THEN 1 ELSE
                        CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'COMPANYID' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END
                        END,

                    P.CREATE_DATE,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    P.CREATE_USER,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    CASE WHEN @version = 1 THEN NULL ELSE @versionAuditStamp END,
                    1,
                    CASE WHEN @version = 1 THEN NULL ELSE (SELECT TOP 1 AUDIT_OPRID FROM @psAudits WHERE AUDIT_STAMP = @versionAuditStamp) END,
                    CASE
                        WHEN @version = 1 THEN NULL
                        WHEN ((SELECT TOP 1 AUDIT_OPRID FROM @psAudits WHERE AUDIT_STAMP = @versionAuditStamp) != (SELECT TOP 1 AUDIT_OPRID FROM @psAudits WHERE AUDIT_STAMP = (SELECT AUDIT_STAMP FROM @vbPoolVersions WHERE VERSION = (@version-1)))) THEN 1
                        ELSE 0
                        END,
                    CASE
                        WHEN @version = 1 AND (SELECT CREATE_DATE FROM VB_POOL WHERE ID = P.ID) > (SELECT MIN(AUDIT_STAMP) FROM @vbPoolVersions) THEN (SELECT CREATE_DATE FROM POOL WHERE ID = P.POOL_ID)
                        WHEN @version = 1 THEN (SELECT CREATE_DATE FROM VB_POOL WHERE ID = P.ID)
                        ELSE @versionAuditStamp END
                FROM @vbPoolAudits P
                WHERE P.ID = @vbPoolNum AND P.VERSION = (@version+1)
            END
        ELSE
            BEGIN
                INSERT INTO @vbPoolAudits (
                    ID, REV, REVTYPE, VERSION, version_MOD,
                    VB_CONTACT_ID, vb_MOD, POOL_ID, pool_MOD, VB_DELETE_REASON_ID, vbDeleteReason_MOD,
                    CREATE_DATE, createDate_MOD, CREATE_USER, createUser_MOD, UPDATE_DATE, updateDate_MOD, UPDATE_USER, updateUser_MOD, AUDIT_STAMP)
                SELECT
                    P.ID,
                    0,
                    CASE WHEN @version = 1 THEN 0 ELSE 1 END, --REVTYPE
                    @version,
                    1, --version_MOD
                    P.VB_CONTACT_ID,
                    0,
                    P.POOL_ID,
                    0,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'COMPANY_ID' AND AUDIT_STAMP = @versionAuditStamp)
                             THEN
                             CASE
                                 WHEN (SELECT OLDVALUE FROM @psAudits WHERE FIELDNAME = 'COMPANY_ID' AND AUDIT_STAMP = @versionAuditStamp AND AUDIT_ACTN = 'A') = '' THEN NULL
                                 WHEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'COMPANY_ID' AND AUDIT_STAMP = @versionAuditStamp AND AUDIT_ACTN = 'C') = '' THEN 4
                                 WHEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'COMPANY_ID' AND AUDIT_STAMP = @versionAuditStamp AND AUDIT_ACTN = 'D') = '' THEN 2
                                 END
                         ELSE P.VB_DELETE_REASON_ID
                        END,
                    1,
                    P.CREATE_DATE,
                    0,
                    P.CREATE_USER,
                    0,
                    P.UPDATE_DATE,
                    0,
                    P.UPDATE_USER,
                    0,
                    COALESCE(@versionAuditStamp, P.CREATE_DATE)
                FROM VB_POOL P
                WHERE
                        P.ID = @vbPoolNum
            END
        SET @version = @version-1;
    END;
    FETCH NEXT FROM @vbPools INTO @vbPoolNum, @poolNum, @vbNumber;
END;
CLOSE @vbPools;
DEALLOCATE @vbPools;

INSERT INTO VB_POOL_AUD
SELECT * FROM @vbPoolAudits ORDER BY AUDIT_STAMP DESC;

GO

DROP INDEX [dbo].[DATAMIG_PSAUDIT].[DATAMIG_PSAUDIT_VB_POOL]

GO

CREATE NONCLUSTERED INDEX [DATAMIG_PSAUDIT_POOL] ON [dbo].[DATAMIG_PSAUDIT] ([AUDIT_ACTN],[FIELDNAME]) INCLUDE ([AUDIT_STAMP],[KEY1])
GO
CREATE NONCLUSTERED INDEX [DATAMIG_PSAUDIT_POOL2] ON [dbo].[DATAMIG_PSAUDIT] ([FIELDNAME]) INCLUDE ([AUDIT_OPRID],[AUDIT_STAMP],[AUDIT_ACTN],[RECNAME],[OLDVALUE],[NEWVALUE],[KEY1])

GO

DECLARE @psAudits TABLE (
                            [AUDIT_OPRID] [varchar](30) NOT NULL,
                            [AUDIT_STAMP] [datetime] NULL,
                            [AUDIT_ACTN] [varchar](1) NOT NULL,
                            [RECNAME] [varchar](15) NOT NULL,
                            [FIELDNAME] [varchar](18) NOT NULL,
                            [OLDVALUE] [varchar](65) NOT NULL,
                            [NEWVALUE] [varchar](65) NOT NULL,
                            [KEY1] [varchar](65) NOT NULL
                        );
DECLARE @versionAuditStamp datetime2 = NULL;
DECLARE @versionAuditStampPlusOne datetime2 = NULL;
DECLARE @poolVersions TABLE (VERSION int, AUDIT_STAMP datetime2);
DECLARE @poolAudits TABLE (
                              [ID] [bigint] NOT NULL,
                              [REV] [int] NULL,
                              [REVTYPE] [smallint] NULL,
                              [VERSION] [bigint] NULL,
                              [version_MOD] [bit] NULL,
                              [UNITS_TO_COVER] [int] NULL,
                              [unitsToCover_MOD] [bit] NULL,
                              [FH_UNITS_TO_COVER] [int] NULL,
                              [fhUnitsToCover_MOD] [bit] NULL,
                              [CE_UNITS_TO_COVER] [int] NULL,
                              [ceUnitsToCover_MOD] [bit] NULL,
                              [CREATE_DATE] [datetime2](7) NULL,
                              [createDate_MOD] [bit] NULL,
                              [CREATE_USER] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                              [createUser_MOD] [bit] NULL,
                              [UPDATE_DATE] [datetime2](7) NULL,
                              [updateDate_MOD] [bit] NULL,
                              [UPDATE_USER] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                              [updateUser_MOD] [bit] NULL,
                              [AUDIT_STAMP] [datetime2] NULL );

DECLARE @poolNum INT;
DECLARE @pools CURSOR;
DECLARE @version INT;
SET @pools = CURSOR FOR SELECT ID FROM POOL ORDER BY ID;
OPEN @pools;
FETCH NEXT
    FROM @pools INTO @poolNum
WHILE @@FETCH_STATUS = 0
BEGIN
    DELETE FROM @poolVersions;
    DELETE FROM @psAudits;

    WITH STAMPS (AUDIT_STAMP) AS (
        SELECT DISTINCT(AUDIT_STAMP) FROM DATAMIG_PSAUDIT WHERE FIELDNAME IN ('TWC_UNITS_TO_COVER') AND KEY1 = @poolNum AND AUDIT_ACTN = 'C'
    ), VERSIONS (VERSION, AUDIT_STAMP) AS (
        SELECT
            CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM STAMPS WHERE AUDIT_STAMP = (SELECT CREATE_DATE FROM POOL WHERE ID = @poolNum)) THEN
                     ROW_NUMBER() OVER (ORDER BY AUDIT_STAMP ASC) ELSE (ROW_NUMBER() OVER (ORDER BY AUDIT_STAMP ASC) + 1) END AS VERSION,
            AUDIT_STAMP
        FROM STAMPS
    )

    INSERT INTO @poolVersions SELECT * FROM VERSIONS;
    INSERT INTO @psAudits SELECT AUDIT_OPRID, AUDIT_STAMP, AUDIT_ACTN, RECNAME, FIELDNAME, OLDVALUE, NEWVALUE, KEY1 FROM DATAMIG_PSAUDIT WHERE FIELDNAME IN ('TWC_UNITS_TO_COVER') AND KEY1 = @poolNum;

    SET @version = COALESCE((SELECT MAX(VERSION) FROM @poolVersions), 1);
    WHILE @version > 0
    BEGIN
        SET @versionAuditStamp = (SELECT AUDIT_STAMP FROM @poolVersions WHERE VERSION = @version);
        IF EXISTS(SELECT ID FROM @poolAudits WHERE ID = @poolNum AND VERSION = (@version+1))
            BEGIN
                SET @versionAuditStampPlusOne = (SELECT AUDIT_STAMP FROM @poolVersions WHERE VERSION = (@version+1));

                INSERT INTO @poolAudits (
                    ID, REV, REVTYPE, VERSION, version_MOD,
                    UNITS_TO_COVER, unitsToCover_MOD, FH_UNITS_TO_COVER, fhUnitsToCover_MOD, CE_UNITS_TO_COVER, ceUnitsToCover_MOD,
                    CREATE_DATE, createDate_MOD, CREATE_USER, createUser_MOD, UPDATE_DATE, updateDate_MOD, UPDATE_USER, updateUser_MOD, AUDIT_STAMP)
                SELECT
                    P.ID,
                    0,
                    CASE WHEN @version = 1 THEN 0 ELSE 1 END,
                    @version,
                    1,
                    CASE WHEN @version = 1 AND EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_UNITS_TO_COVER' AND AUDIT_ACTN = 'A')
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_UNITS_TO_COVER' AND AUDIT_ACTN = 'A')
                         ELSE
                             CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_UNITS_TO_COVER' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                      THEN (SELECT OLDVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_UNITS_TO_COVER' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                  ELSE P.UNITS_TO_COVER END
                        END,
                    CASE WHEN @version = 1 THEN 1 ELSE
                        CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_UNITS_TO_COVER' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END
                        END,
                    P.FH_UNITS_TO_COVER,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    P.CE_UNITS_TO_COVER,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    P.CREATE_DATE,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    P.CREATE_USER,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    CASE WHEN @version = 1 THEN NULL ELSE @versionAuditStamp END,
                    1,
                    CASE WHEN @version = 1 THEN NULL ELSE (SELECT TOP 1 AUDIT_OPRID FROM @psAudits WHERE AUDIT_STAMP = @versionAuditStamp) END,
                    CASE
                        WHEN @version = 1 THEN NULL
                        WHEN ((SELECT TOP 1 AUDIT_OPRID FROM @psAudits WHERE AUDIT_STAMP = @versionAuditStamp) != (SELECT TOP 1 AUDIT_OPRID FROM @psAudits WHERE AUDIT_STAMP = (SELECT AUDIT_STAMP FROM @poolVersions WHERE VERSION = (@version-1)))) THEN 1
                        ELSE 0
                        END,
                    CASE WHEN @version = 1 THEN (SELECT CREATE_DATE FROM POOL WHERE ID = P.ID) ELSE	@versionAuditStamp END
                FROM @poolAudits P
                WHERE P.ID = @poolNum AND P.VERSION = (@version+1)
            END
        ELSE
            BEGIN
                INSERT INTO @poolAudits (
                    ID, REV, REVTYPE, VERSION, version_MOD,
                    UNITS_TO_COVER, unitsToCover_MOD, FH_UNITS_TO_COVER, fhUnitsToCover_MOD, CE_UNITS_TO_COVER, ceUnitsToCover_MOD,
                    CREATE_DATE, createDate_MOD, CREATE_USER, createUser_MOD, UPDATE_DATE, updateDate_MOD, UPDATE_USER, updateUser_MOD, AUDIT_STAMP)
                SELECT
                    P.ID,
                    0,
                    CASE WHEN @version = 1 THEN 0 ELSE 1 END, --REVTYPE
                    @version,
                    1, --version_MOD
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_UNITS_TO_COVER' AND AUDIT_STAMP = @versionAuditStamp)
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_UNITS_TO_COVER' AND AUDIT_STAMP = @versionAuditStamp)
                         ELSE P.UNITS_TO_COVER
                        END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_UNITS_TO_COVER' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END,
                    NULL,
                    0,
                    NULL,
                    0,
                    P.CREATE_DATE,
                    0,
                    P.CREATE_USER,
                    0,
                    P.UPDATE_DATE,
                    0,
                    P.UPDATE_USER,
                    0,
                    COALESCE(@versionAuditStamp, P.CREATE_DATE)
                FROM POOL P
                WHERE
                        P.ID = @poolNum
            END
        SET @version = @version-1;
    END;

    FETCH NEXT FROM @pools INTO @poolNum;
END;
CLOSE @pools;
DEALLOCATE @pools;

INSERT INTO POOL_AUD
SELECT * FROM @poolAudits ORDER BY AUDIT_STAMP;

GO

DROP INDEX [dbo].[DATAMIG_PSAUDIT].[DATAMIG_PSAUDIT_POOL]
GO
DROP INDEX [dbo].[DATAMIG_PSAUDIT].[DATAMIG_PSAUDIT_POOL2]

GO

CREATE NONCLUSTERED INDEX [DATAMIG_PSAUDIT_FIELDNAME_SEC] ON [dbo].[DATAMIG_PSAUDIT] ([FIELDNAME]) INCLUDE ([AUDIT_OPRID],[AUDIT_STAMP],[AUDIT_ACTN],[OLDVALUE],[NEWVALUE],[KEY1],[KEY15])

GO

DECLARE @psAudits TABLE (
                            [AUDIT_OPRID] [varchar](30) NOT NULL,
                            [AUDIT_STAMP] [datetime] NULL,
                            [AUDIT_ACTN] [varchar](1) NOT NULL,
                            [FIELDNAME] [varchar](18) NOT NULL,
                            [OLDVALUE] [varchar](65) NOT NULL,
                            [NEWVALUE] [varchar](65) NOT NULL,
                            [KEY1] [varchar](65) NOT NULL,
                            [KEY15] [varchar](65) NOT NULL
                        );

DECLARE @versionAuditStamp datetime2 = NULL;
DECLARE @versionAuditStampPlusOne datetime2 = NULL;
DECLARE @secVersions TABLE (VERSION int, AUDIT_STAMP datetime2);
DECLARE @secAudits TABLE (
                             [ID] [bigint] NULL,
                             [REV] [int] NULL,
                             [REVTYPE] [smallint] NULL,
                             [VERSION] [bigint] NULL,
                             [version_MOD] [bit] NULL,
                             [POOL_ID] [int] NULL,
                             [pool_MOD] [bit] NULL,
                             [STATUS_ID] [int] NULL,
                             [status_MOD] [bit] NULL,
                             [SECURITY_TYPE_ID] [int] NULL,
                             [securityType_MOD] [bit] NULL,
                             [SECURITY_PURPOSE_ID] [int] NULL,
                             [securityPurpose_MOD] [bit] NULL,
                             [BRANCH_ID] [int] NULL,
                             [branch_MOD] [bit] NULL,
                             [INSTRUMENT_NUMBER] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                             [instrumentNumber_MOD] [bit] NULL,
                             [ORIGINAL_AMOUNT] [money] NULL,
                             [originalAmount_MOD] [bit] NULL,
                             [CURRENT_AMOUNT] [money] NULL,
                             [currentAmount_MOD] [bit] NULL,
                             [ISSUED_DATE] [datetime2] NULL,
                             [issuedDate_MOD] [bit] NULL,
                             [RECEIVED_DATE] [datetime2] NULL,
                             [receivedDate_MOD] [bit] NULL,
                             [PPSA_NUMBER] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                             [ppsaNumber_MOD] [bit] NULL,
                             [PPSA_FILE_NUMBER] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                             [ppsaFileNumber_MOD] [bit] NULL,
                             [PPSA_EXPIRY_DATE] [datetime2] NULL,
                             [ppsaExpiryDate_MOD] [bit] NULL,
                             [JOINT_BOND] [bit] NULL,
                             [jointBond_MOD] [bit] NULL,
                             [JOINT_AUT] [bit] NULL,
                             [jointAut_MOD] [bit] NULL,
                             [RELEASE_FIRST] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                             [releaseFirst_MOD] [bit] NULL,
                             [DEMAND_FIRST] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                             [demandFirst_MOD] [bit] NULL,
                             [COMMENT] [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                             [comment_MOD] [bit] NULL,
                             [THIRD_PARTY] [bit] NULL,
                             [thirdParty_MOD] [bit] NULL,
                             [DELETED] [bit] NULL,
                             [deleted_MOD] [bit] NULL,
                             [ALLOCATED] [bit] NULL,
                             [allocated_MOD] [bit] NULL,
                             [WRITE_OFF] [bit] Null,
                             [writeOff_MOD] [bit] NULL,
                             [WRITE_OFF_REASON] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                             [writeOffReason_MOD] [bit] NULL,
                             [WRITE_OFF_DATE] [datetime2] NULL,
                             [writeOffDate_MOD] [bit] NULL,
                             [DTA_ACCOUNT_NUMBER] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                             [dtaAccountNumber_MOD] [bit] NULL,
                             [UNPOST_REASON] [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                             [unpostReason_MOD] [bit] NULL,
                             [UNPOST_USER] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                             [unpostUser_MOD] [bit] NULL,
                             [UNPOST_DATE] [datetime2] NULL,
                             [unpostDate_MOD] [bit] NULL,
                             [IDENTITY_TYPE_ID] [bigint] NULL,
                             [identityType_MOD] [bit] NULL,
                             [FINANCIAL_INSTITUTION_MAAPOA_ID] [int] NULL,
                             [financialInstitutionMaaPoa_MOD] [bit] NULL,
                             [PEF_ACCOUNT_NUMBER] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                             [pefAccountNumber_MOD] [bit] NULL,
                             [PRIMARY_VB_ID] [int] NULL,
                             [primaryVb_MOD] [bit] NULL,
                             [CREATE_DATE] [datetime2](7) NULL,
                             [createDate_MOD] [bit] NULL,
                             [CREATE_USER] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                             [createUser_MOD] [bit] NULL,
                             [UPDATE_DATE] [datetime2](7) NULL,
                             [updateDate_MOD] [bit] NULL,
                             [UPDATE_USER] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                             [updateUser_MOD] [bit] NULL,
                             [AUDIT_STAMP] [datetime2] NULL );


DECLARE @secNum INT;
DECLARE @securityNumbers CURSOR;
DECLARE @version INT;
SET @securityNumbers = CURSOR FOR SELECT ID FROM SECURITY ORDER BY ID ASC;
OPEN @securityNumbers;
FETCH NEXT
    FROM @securityNumbers INTO @secNum
WHILE @@FETCH_STATUS = 0
BEGIN
    DELETE FROM @secVersions;
    DELETE FROM @psAudits;

    INSERT INTO @psAudits
    SELECT AUDIT_OPRID, AUDIT_STAMP, AUDIT_ACTN, FIELDNAME, OLDVALUE, NEWVALUE, KEY1, KEY15 FROM DATAMIG_PSAUDIT WHERE FIELDNAME IN ('TWC_PPSA_NUMBER','TWC_ORIG_AMT','TWC_DTA_ACCT_NUM','TWC_JOINT_BOND_IND','TWC_PPSA_EXPIRY_DT','TWC_FIN_INST_CD','TWC_DEMAND_FIRST','TWC_RELEASE_FIRST','TWC_CURRENT_AMT','TWC_INSTRUMENT_NBR','TWC_SEC_PURPOSE','TWC_POOL_NBR') AND KEY1 = @secNum
    UNION
    SELECT AUDIT_OPRID, AUDIT_STAMP, AUDIT_ACTN, FIELDNAME, OLDVALUE, NEWVALUE, KEY1, KEY15 FROM DATAMIG_PSAUDIT WHERE FIELDNAME = 'PRIMARY_IND' AND KEY15 = @secNum;

    WITH STAMPS (AUDIT_STAMP) AS (
        SELECT DISTINCT(AUDIT_STAMP) FROM @psAudits WHERE AUDIT_ACTN != 'A'
    ), VERSIONS (VERSION, AUDIT_STAMP) AS (
        SELECT
            CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM STAMPS WHERE AUDIT_STAMP = (SELECT CREATE_DATE FROM SECURITY WHERE ID = @secNum)) THEN
                     ROW_NUMBER() OVER (ORDER BY AUDIT_STAMP ASC) ELSE (ROW_NUMBER() OVER (ORDER BY AUDIT_STAMP ASC) + 1) END AS VERSION,
            AUDIT_STAMP
        FROM STAMPS
    )
    INSERT INTO @secVersions SELECT * FROM VERSIONS;

    SET @version = COALESCE((SELECT MAX(VERSION) FROM @secVersions), 1);
    WHILE @version > 0
    BEGIN
        SET @versionAuditStamp = (SELECT AUDIT_STAMP FROM @secVersions WHERE VERSION = @version);
        IF EXISTS(SELECT ID FROM @secAudits WHERE ID = @secNum AND VERSION = (@version+1))
            BEGIN
                SET @versionAuditStampPlusOne = (SELECT AUDIT_STAMP FROM @secVersions WHERE VERSION = (@version+1));
                INSERT INTO @secAudits (ID, REV, REVTYPE, VERSION, version_MOD, POOL_ID, pool_MOD, status_MOD, SECURITY_TYPE_ID, securityType_MOD, SECURITY_PURPOSE_ID, securityPurpose_MOD, BRANCH_ID, branch_MOD,
                                        INSTRUMENT_NUMBER, instrumentNumber_MOD, ORIGINAL_AMOUNT, originalAmount_MOD, CURRENT_AMOUNT, currentAmount_MOD, ISSUED_DATE, issuedDate_MOD, RECEIVED_DATE, receivedDate_MOD,
                                        PPSA_NUMBER, ppsaNumber_MOD, PPSA_FILE_NUMBER, ppsaFileNumber_MOD, PPSA_EXPIRY_DATE, ppsaExpiryDate_MOD, JOINT_BOND, jointBond_MOD,JOINT_AUT, jointAut_MOD,
                                        RELEASE_FIRST, releaseFirst_MOD, DEMAND_FIRST, demandFirst_MOD,
                                        COMMENT, comment_MOD, THIRD_PARTY, thirdParty_MOD, DELETED, deleted_MOD, ALLOCATED, allocated_MOD, WRITE_OFF, writeOff_MOD, WRITE_OFF_REASON, writeOffReason_MOD,
                                        WRITE_OFF_DATE, writeOffDate_MOD, DTA_ACCOUNT_NUMBER, dtaAccountNumber_MOD, UNPOST_USER, unpostUser_MOD, UNPOST_REASON, unpostReason_MOD, UNPOST_DATE, unpostDate_MOD, IDENTITY_TYPE_ID, identityType_MOD,
                                        FINANCIAL_INSTITUTION_MAAPOA_ID, financialInstitutionMaaPoa_MOD, PEF_ACCOUNT_NUMBER, pefAccountNumber_MOD, PRIMARY_VB_ID, primaryVb_MOD, CREATE_DATE, createDate_MOD,
                                        CREATE_USER, createUser_MOD, UPDATE_DATE, updateDate_MOD, UPDATE_USER, updateUser_MOD, AUDIT_STAMP)
                SELECT
                    S.ID,
                    0,
                    CASE WHEN @version = 1 THEN 0 ELSE 1 END,
                    @version,
                    1,
                    CASE WHEN @version = 1 AND EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_POOL_NBR' AND AUDIT_ACTN = 'A')
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_POOL_NBR' AND AUDIT_ACTN = 'A')
                         ELSE
                             CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_POOL_NBR' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                      THEN (SELECT OLDVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_POOL_NBR' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                  ELSE S.POOL_ID END
                        END,
                    CASE WHEN @version = 1 THEN 1 ELSE
                        CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_POOL_NBR' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END
                        END,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    S.SECURITY_TYPE_ID,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    CASE WHEN @version = 1 AND EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_SEC_PURPOSE' AND AUDIT_ACTN = 'A')
                             THEN
                             CASE (SELECT OLDVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_SEC_PURPOSE' AND AUDIT_ACTN = 'A')
                                 WHEN 'DP' THEN 4
                                 WHEN 'CC' THEN 3
                                 WHEN 'WA' THEN 5
                                 WHEN 'DW' THEN 1
                                 WHEN 'Blanket - Freehold Units Only' THEN 2
                                 WHEN 'BL' THEN 2
                                 ELSE 1
                                 END
                         ELSE
                             CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_SEC_PURPOSE' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                      THEN
                                      CASE (SELECT OLDVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_SEC_PURPOSE' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                          WHEN 'DP' THEN 4
                                          WHEN 'CC' THEN 3
                                          WHEN 'WA' THEN 5
                                          WHEN 'DW' THEN 1
                                          WHEN 'Blanket - Freehold Units Only' THEN 2
                                          WHEN 'BL' THEN 2
                                          ELSE 1
                                          END
                                  ELSE S.SECURITY_PURPOSE_ID END
                        END,
                    CASE WHEN @version = 1 OR EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_SEC_PURPOSE' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END,
                    S.BRANCH_ID, --investigate
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    CASE WHEN @version = 1 AND EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_INSTRUMENT_NBR' AND AUDIT_ACTN = 'A')
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_INSTRUMENT_NBR' AND AUDIT_ACTN = 'A')
                         ELSE
                             CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_INSTRUMENT_NBR' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                      THEN (SELECT OLDVALUE COLLATE SQL_Latin1_General_CP1_CI_AS FROM @psAudits WHERE FIELDNAME = 'TWC_INSTRUMENT_NBR' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                  ELSE S.INSTRUMENT_NUMBER END
                        END,
                    CASE WHEN @version = 1 THEN 1 ELSE
                        CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_INSTRUMENT_NBR' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END
                        END,
                    CASE WHEN @version = 1 AND EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_ORIG_AMT' AND AUDIT_ACTN = 'A')
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_ORIG_AMT' AND AUDIT_ACTN = 'A')
                         ELSE
                             CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_ORIG_AMT' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                      THEN (SELECT OLDVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_ORIG_AMT' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                  ELSE S.ORIGINAL_AMOUNT END
                        END,
                    CASE WHEN @version = 1 THEN 1 ELSE
                        CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_ORIG_AMT' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END
                        END,
                    CASE WHEN @version = 1 AND EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_CURRENT_AMT' AND AUDIT_ACTN = 'A')
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_CURRENT_AMT' AND AUDIT_ACTN = 'A')
                         ELSE
                             CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_CURRENT_AMT' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                      THEN (SELECT OLDVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_CURRENT_AMT' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                  ELSE S.CURRENT_AMOUNT END
                        END,
                    CASE WHEN @version = 1 THEN 1 ELSE
                        CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_CURRENT_AMT' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END
                        END,
                    S.ISSUED_DATE,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    S.RECEIVED_DATE,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    CASE WHEN @version = 1 AND EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_PPSA_NUMBER' AND AUDIT_ACTN = 'A')
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_PPSA_NUMBER' AND AUDIT_ACTN = 'A')
                         ELSE
                             CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_PPSA_NUMBER' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                      THEN (SELECT OLDVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_PPSA_NUMBER' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                  ELSE S.PPSA_NUMBER END
                        END,
                    CASE WHEN @version = 1 THEN 1 ELSE
                        CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_PPSA_NUMBER' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END
                        END,
                    S.PPSA_FILE_NUMBER,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    CASE WHEN @version = 1 AND EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_PPSA_EXPIRY_DT' AND AUDIT_ACTN = 'A')
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_PPSA_EXPIRY_DT' AND AUDIT_ACTN = 'A')
                         ELSE
                             CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_PPSA_EXPIRY_DT' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                      THEN (SELECT OLDVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_PPSA_EXPIRY_DT' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                  ELSE S.PPSA_EXPIRY_DATE END
                        END,
                    CASE WHEN @version = 1 THEN 1 ELSE
                        CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_PPSA_EXPIRY_DT' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END
                        END,
                    CASE WHEN @version = 1 AND EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_JOINT_BOND_IND' AND AUDIT_ACTN = 'A')
                             THEN (SELECT CASE WHEN NEWVALUE = 'Y' THEN 1 ELSE 0 END FROM @psAudits WHERE FIELDNAME = 'TWC_JOINT_BOND_IND' AND AUDIT_ACTN = 'A')
                         ELSE
                             CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_JOINT_BOND_IND' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                      THEN (SELECT CASE WHEN OLDVALUE = 'Y' THEN 1 ELSE 0 END FROM @psAudits WHERE FIELDNAME = 'TWC_JOINT_BOND_IND' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                  ELSE S.JOINT_BOND END
                        END,
                    CASE WHEN @version = 1 THEN 1 ELSE
                        CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_JOINT_BOND_IND' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END
                        END,
                    S.JOINT_AUT,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    CASE WHEN @version = 1 AND EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_RELEASE_FIRST' AND LTRIM(RTRIM(NEWVALUE)) != '' AND AUDIT_ACTN = 'A')
                             THEN (SELECT CASE WHEN NEWVALUE = 'Y' THEN 'YES' WHEN NEWVALUE = 'N' THEN 'NO' ELSE NULL END COLLATE SQL_Latin1_General_CP1_CI_AS FROM @psAudits WHERE FIELDNAME = 'TWC_RELEASE_FIRST' AND AUDIT_ACTN = 'A')
                         ELSE
                             CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_RELEASE_FIRST' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                      THEN (SELECT CASE WHEN OLDVALUE = 'Y' THEN 'YES' WHEN OLDVALUE = 'N' THEN 'NO' ELSE NULL END COLLATE SQL_Latin1_General_CP1_CI_AS FROM @psAudits WHERE FIELDNAME = 'TWC_RELEASE_FIRST' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                  ELSE S.RELEASE_FIRST END
                        END,
                    CASE WHEN @version = 1 THEN 1 ELSE
                        CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_RELEASE_FIRST' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END
                        END,
                    CASE WHEN @version = 1 AND EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_DEMAND_FIRST' AND LTRIM(RTRIM(NEWVALUE)) != '' AND AUDIT_ACTN = 'A')
                             THEN (SELECT CASE WHEN NEWVALUE = 'Y' THEN 'YES' WHEN NEWVALUE = 'N' THEN 'NO' ELSE NULL END COLLATE SQL_Latin1_General_CP1_CI_AS FROM @psAudits WHERE FIELDNAME = 'TWC_DEMAND_FIRST' AND AUDIT_ACTN = 'A')
                         ELSE
                             CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_DEMAND_FIRST' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                      THEN (SELECT CASE WHEN OLDVALUE = 'Y' THEN 'YES' WHEN OLDVALUE = 'N' THEN 'NO' ELSE NULL END COLLATE SQL_Latin1_General_CP1_CI_AS FROM @psAudits WHERE FIELDNAME = 'TWC_DEMAND_FIRST' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                                  ELSE S.DEMAND_FIRST END
                        END,
                    CASE WHEN @version = 1 THEN 1 ELSE
                        CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_DEMAND_FIRST' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END
                        END,
                    S.COMMENT,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    S.THIRD_PARTY,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    S.DELETED,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    S.ALLOCATED,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    S.WRITE_OFF,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    S.WRITE_OFF_REASON,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    S.WRITE_OFF_DATE,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    S.DTA_ACCOUNT_NUMBER,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    S.UNPOST_USER,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    S.UNPOST_REASON,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    S.UNPOST_DATE,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    S.IDENTITY_TYPE_ID,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    S.FINANCIAL_INSTITUTION_MAAPOA_ID,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    S.PEF_ACCOUNT_NUMBER,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'PRIMARY_IND' AND AUDIT_STAMP = @versionAuditStampPlusOne)
                             THEN (SELECT ID FROM CONTACT WHERE CRM_CONTACT_ID = (
                            SELECT OLDVALUE COLLATE SQL_Latin1_General_CP1_CI_AS FROM @psAudits WHERE FIELDNAME = 'PRIMARY_IND' AND AUDIT_STAMP = @versionAuditStampPlusOne))
                         ELSE S.PRIMARY_VB_ID END,
                    CASE WHEN @version = 1 THEN 1 ELSE
                        CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'PRIMARY_IND' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END
                        END,
                    S.CREATE_DATE,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    S.CREATE_USER,
                    CASE WHEN @version = 1 THEN 1 ELSE 0 END,
                    CASE WHEN @version = 1 THEN NULL ELSE @versionAuditStamp END,
                    1,
                    CASE WHEN @version = 1 THEN NULL ELSE (SELECT TOP 1 AUDIT_OPRID FROM @psAudits WHERE AUDIT_STAMP = @versionAuditStamp) END,
                    CASE
                        WHEN @version = 1 THEN NULL
                        WHEN ((SELECT TOP 1 AUDIT_OPRID FROM @psAudits WHERE AUDIT_STAMP = @versionAuditStamp) != (SELECT TOP 1 AUDIT_OPRID FROM @psAudits WHERE AUDIT_STAMP = (SELECT AUDIT_STAMP FROM @secVersions WHERE VERSION = (@version-1)))) THEN 1
                        ELSE 0
                        END,
                    CASE WHEN @version = 1 THEN (SELECT CREATE_DATE FROM SECURITY WHERE ID = S.ID) ELSE	@versionAuditStamp END
                FROM @secAudits S
                WHERE S.ID = @secNum AND S.VERSION = (@version+1)
            END
        ELSE
            BEGIN
                --SELECT FROM THE CURRENT SECURITY. EDIT WITH ALL CHANGES.
                INSERT INTO @secAudits (ID, REV, REVTYPE, VERSION, version_MOD, POOL_ID, pool_MOD, status_MOD, SECURITY_TYPE_ID, securityType_MOD, SECURITY_PURPOSE_ID, securityPurpose_MOD, BRANCH_ID, branch_MOD,
                                        INSTRUMENT_NUMBER, instrumentNumber_MOD, ORIGINAL_AMOUNT, originalAmount_MOD, CURRENT_AMOUNT, currentAmount_MOD, ISSUED_DATE, issuedDate_MOD, RECEIVED_DATE, receivedDate_MOD,
                                        PPSA_NUMBER, ppsaNumber_MOD, PPSA_FILE_NUMBER, ppsaFileNumber_MOD, PPSA_EXPIRY_DATE, ppsaExpiryDate_MOD, JOINT_BOND, jointBond_MOD,JOINT_AUT, jointAut_MOD,
                                        RELEASE_FIRST, releaseFirst_MOD, DEMAND_FIRST, demandFirst_MOD,
                                        COMMENT, comment_MOD, THIRD_PARTY, thirdParty_MOD, DELETED, deleted_MOD, ALLOCATED, allocated_MOD, WRITE_OFF, writeOff_MOD, WRITE_OFF_REASON, writeOffReason_MOD,
                                        WRITE_OFF_DATE, writeOffDate_MOD, DTA_ACCOUNT_NUMBER, dtaAccountNumber_MOD, UNPOST_USER, unpostUser_MOD, UNPOST_REASON, unpostReason_MOD, UNPOST_DATE, unpostDate_MOD, IDENTITY_TYPE_ID, identityType_MOD,
                                        FINANCIAL_INSTITUTION_MAAPOA_ID, financialInstitutionMaaPoa_MOD, PEF_ACCOUNT_NUMBER, pefAccountNumber_MOD, PRIMARY_VB_ID, primaryVb_MOD, CREATE_DATE, createDate_MOD,
                                        CREATE_USER, createUser_MOD, UPDATE_DATE, updateDate_MOD, UPDATE_USER, updateUser_MOD, AUDIT_STAMP)
                SELECT
                    S.ID,
                    0,
                    CASE WHEN @version = 1 THEN 0 ELSE 1 END,
                    @version,
                    1,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_POOL_NBR' AND AUDIT_STAMP = @versionAuditStamp)
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_POOL_NBR' AND AUDIT_STAMP = @versionAuditStamp)
                         ELSE S.POOL_ID
                        END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_POOL_NBR' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END,
                    0,
                    S.SECURITY_TYPE_ID,
                    0,
                    S.SECURITY_PURPOSE_ID,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_SEC_PURPOSE' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END,
                    S.BRANCH_ID, --investigate
                    0,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_INSTRUMENT_NBR' AND AUDIT_STAMP = @versionAuditStamp)
                             THEN (SELECT NEWVALUE COLLATE SQL_Latin1_General_CP1_CI_AS FROM @psAudits WHERE FIELDNAME = 'TWC_INSTRUMENT_NBR' AND AUDIT_STAMP = @versionAuditStamp)
                         ELSE S.INSTRUMENT_NUMBER
                        END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_INSTRUMENT_NBR' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_ORIG_AMT' AND AUDIT_STAMP = @versionAuditStamp)
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_ORIG_AMT' AND AUDIT_STAMP = @versionAuditStamp)
                         ELSE S.ORIGINAL_AMOUNT
                        END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_ORIG_AMT' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_CURRENT_AMT' AND AUDIT_STAMP = @versionAuditStamp)
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_CURRENT_AMT' AND AUDIT_STAMP = @versionAuditStamp)
                         ELSE S.CURRENT_AMOUNT
                        END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_CURRENT_AMT' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END,
                    S.ISSUED_DATE,
                    0,
                    S.RECEIVED_DATE,
                    0,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_PPSA_NUMBER' AND AUDIT_STAMP = @versionAuditStamp)
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_PPSA_NUMBER' AND AUDIT_STAMP = @versionAuditStamp)
                         ELSE S.PPSA_NUMBER
                        END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_PPSA_NUMBER' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END,
                    NULL,
                    0,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_PPSA_EXPIRY_DT' AND AUDIT_STAMP = @versionAuditStamp)
                             THEN (SELECT NEWVALUE FROM @psAudits WHERE FIELDNAME = 'TWC_PPSA_EXPIRY_DT' AND AUDIT_STAMP = @versionAuditStamp)
                         ELSE S.PPSA_EXPIRY_DATE
                        END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_PPSA_EXPIRY_DT' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_JOINT_BOND_IND' AND AUDIT_STAMP = @versionAuditStamp)
                             THEN (SELECT CASE WHEN NEWVALUE = 'Y' THEN 1 ELSE 0 END FROM @psAudits WHERE FIELDNAME = 'TWC_JOINT_BOND_IND' AND AUDIT_STAMP = @versionAuditStamp)
                         ELSE S.JOINT_BOND
                        END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_JOINT_BOND_IND' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END,
                    S.JOINT_AUT,
                    0,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_RELEASE_FIRST' AND AUDIT_STAMP = @versionAuditStamp)
                             THEN (SELECT CASE NEWVALUE WHEN 'Y' THEN 'YES' WHEN 'N' THEN 'NO' ELSE NULL END FROM @psAudits WHERE FIELDNAME = 'TWC_RELEASE_FIRST' AND AUDIT_STAMP = @versionAuditStamp)
                         ELSE S.RELEASE_FIRST
                        END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_RELEASE_FIRST' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_DEMAND_FIRST' AND AUDIT_STAMP = @versionAuditStamp)
                             THEN (SELECT CASE NEWVALUE WHEN 'Y' THEN 'YES' WHEN 'N' THEN 'NO' ELSE NULL END FROM @psAudits WHERE FIELDNAME = 'TWC_DEMAND_FIRST' AND AUDIT_STAMP = @versionAuditStamp)
                         ELSE S.DEMAND_FIRST
                        END,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'TWC_DEMAND_FIRST' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END,
                    S.COMMENT,
                    0,
                    S.THIRD_PARTY,
                    0,
                    S.DELETED,
                    0,
                    S.ALLOCATED,
                    0,
                    NULL,
                    0,
                    NULL,
                    0,
                    NULL,
                    0,
                    S.DTA_ACCOUNT_NUMBER,
                    0,
                    S.UNPOST_USER,
                    0,
                    S.UNPOST_REASON,
                    0,
                    S.UNPOST_DATE,
                    0,
                    NULL,
                    0,
                    NULL,
                    0,
                    NULL,
                    0,
                    S.PRIMARY_VB_ID,
                    CASE WHEN EXISTS(SELECT AUDIT_STAMP FROM @psAudits WHERE FIELDNAME = 'PRIMARY_IND' AND AUDIT_STAMP = @versionAuditStamp) THEN 1 ELSE 0 END,
                    S.CREATE_DATE,
                    0,
                    S.CREATE_USER,
                    0,
                    S.UPDATE_DATE,
                    0,
                    S.UPDATE_USER,
                    0,
                    COALESCE(@versionAuditStamp, S.CREATE_DATE)
                FROM SECURITY S
                WHERE
                        S.ID = @secNum
            END
        SET @version = @version-1;
    END;
    FETCH NEXT FROM @securityNumbers INTO @secNum;
END;
CLOSE @securityNumbers;
DEALLOCATE @securityNumbers;

INSERT INTO SECURITY_AUD
SELECT * FROM @secAudits ORDER BY AUDIT_STAMP ASC;

GO

DROP INDEX [dbo].[DATAMIG_PSAUDIT].[DATAMIG_PSAUDIT_FIELDNAME_SEC]

GO

--RELEASE_AUD -> SECURITY_AUD
WITH R_SECURITY_IDS (SECURITY_ID, AUDIT_STAMP) AS (
    SELECT R.SECURITY_ID, MIN(R.AUDIT_STAMP)
    FROM RELEASE_AUD R
             LEFT OUTER JOIN SECURITY_AUD S ON R.SECURITY_ID = S.ID AND R.AUDIT_STAMP >= S.AUDIT_STAMP
    WHERE S.ID IS NULL
    GROUP BY R.SECURITY_ID
)

UPDATE S
SET S.AUDIT_STAMP = I.AUDIT_STAMP
FROM SECURITY_AUD S, R_SECURITY_IDS I
WHERE S.ID = I.SECURITY_ID AND S.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM SECURITY_AUD A WHERE A.ID = S.ID)

GO

--SECURITY_AUD -> POOL_AUD
WITH S_POOL_IDS (POOL_ID, AUDIT_STAMP) AS (
    SELECT S.POOL_ID, MIN(S.AUDIT_STAMP)
    FROM SECURITY_AUD S
             LEFT OUTER JOIN POOL_AUD P ON S.POOL_ID = P.ID AND S.AUDIT_STAMP >= P.AUDIT_STAMP
    WHERE P.ID IS NULL
    GROUP BY S.POOL_ID
)

UPDATE P
SET P.AUDIT_STAMP = I.AUDIT_STAMP
FROM POOL_AUD P, S_POOL_IDS I
WHERE P.ID = I.POOL_ID AND P.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM POOL_AUD A WHERE A.ID = P.ID)

GO

--SECURITY_AUD -> BRANCH_AUD
WITH S_BRANCH_IDS (BRANCH_ID, AUDIT_STAMP) AS (
    SELECT S.BRANCH_ID, MIN(S.AUDIT_STAMP)
    FROM SECURITY_AUD S
             LEFT OUTER JOIN BRANCH_AUD B ON S.BRANCH_ID = B.ID AND S.AUDIT_STAMP >= B.AUDIT_STAMP
    WHERE B.ID IS NULL AND S.BRANCH_ID IS NOT NULL
    GROUP BY S.BRANCH_ID
)

UPDATE B
SET B.AUDIT_STAMP = I.AUDIT_STAMP
FROM BRANCH_AUD B, S_BRANCH_IDS I
WHERE B.ID = I.BRANCH_ID AND B.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM BRANCH_AUD A WHERE A.ID = B.ID)

GO

--SECURITY_AUD -> CONTACT_AUD
WITH S_CONTACT_IDS (CONTACT_ID, AUDIT_STAMP) AS (
    SELECT S.PRIMARY_VB_ID, MIN(S.AUDIT_STAMP)
    FROM SECURITY_AUD S
             LEFT OUTER JOIN CONTACT_AUD C ON S.PRIMARY_VB_ID = C.ID AND S.AUDIT_STAMP >= C.AUDIT_STAMP
    WHERE C.ID IS NULL AND S.PRIMARY_VB_ID IS NOT NULL
    GROUP BY S.PRIMARY_VB_ID
)

UPDATE C
SET C.AUDIT_STAMP = I.AUDIT_STAMP
FROM CONTACT_AUD C, S_CONTACT_IDS I
WHERE C.ID = I.CONTACT_ID AND C.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM CONTACT_AUD A WHERE A.ID = C.ID)

GO

--VB_POOL_AUD -> POOL_AUD
WITH VBP_POOL_IDS (POOL_ID, AUDIT_STAMP) AS (
    SELECT VBP.POOL_ID, MIN(VBP.AUDIT_STAMP)
    FROM VB_POOL_AUD VBP
             LEFT OUTER JOIN POOL_AUD P ON VBP.POOL_ID = P.ID AND VBP.AUDIT_STAMP >= P.AUDIT_STAMP
    WHERE P.ID IS NULL
    GROUP BY VBP.POOL_ID
)

UPDATE P
SET P.AUDIT_STAMP = I.AUDIT_STAMP
FROM POOL_AUD P, VBP_POOL_IDS I
WHERE P.ID = I.POOL_ID AND P.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM POOL_AUD A WHERE A.ID = P.ID)

GO

--VB_POOL_AUD -> CONTACT_AUD
WITH VBP_CONTACT_IDS (CONTACT_ID, AUDIT_STAMP) AS (
    SELECT VBP.VB_CONTACT_ID, MIN(VBP.AUDIT_STAMP)
    FROM VB_POOL_AUD VBP
             LEFT OUTER JOIN CONTACT_AUD C ON VBP.VB_CONTACT_ID = C.ID AND VBP.AUDIT_STAMP >= C.AUDIT_STAMP
    WHERE C.ID IS NULL
    GROUP BY VBP.VB_CONTACT_ID
)

UPDATE C
SET C.AUDIT_STAMP = I.AUDIT_STAMP
FROM CONTACT_AUD C, VBP_CONTACT_IDS I
WHERE C.ID = I.CONTACT_ID AND C.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM CONTACT_AUD A WHERE A.ID = C.ID)

GO

--ENROLMENT_POOL_AUD -> ENROLMENT_AUD
WITH EP_ENROLMENT_NUMS (ENROLMENT_NUMBER, AUDIT_STAMP) AS (
    SELECT EP.ENROLMENT_NUMBER, MIN(EP.AUDIT_STAMP)
    FROM ENROLMENT_POOL_AUD EP
             LEFT OUTER JOIN ENROLMENT_AUD E ON EP.ENROLMENT_NUMBER = E.ENROLMENT_NUMBER AND EP.AUDIT_STAMP >= E.AUDIT_STAMP
    WHERE E.ENROLMENT_NUMBER IS NULL
    GROUP BY EP.ENROLMENT_NUMBER
)

UPDATE E
SET E.AUDIT_STAMP = I.AUDIT_STAMP
FROM ENROLMENT_AUD E, EP_ENROLMENT_NUMS I
WHERE E.ENROLMENT_NUMBER = I.ENROLMENT_NUMBER AND E.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM ENROLMENT_AUD A WHERE A.ENROLMENT_NUMBER = E.ENROLMENT_NUMBER)

GO

--ENROLMENT_POOL_AUD -> POOL_AUD
WITH EP_POOL_IDS (POOL_ID, AUDIT_STAMP) AS (
    SELECT EP.POOL_ID, MIN(EP.AUDIT_STAMP)
    FROM ENROLMENT_POOL_AUD EP
             LEFT OUTER JOIN POOL_AUD P ON EP.POOL_ID = P.ID AND EP.AUDIT_STAMP >= P.AUDIT_STAMP
    WHERE P.ID IS NULL
    GROUP BY EP.POOL_ID
)

UPDATE P
SET P.AUDIT_STAMP = I.AUDIT_STAMP
FROM POOL_AUD P, EP_POOL_IDS I
WHERE P.ID = I.POOL_ID AND P.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM POOL_AUD A WHERE A.ID = P.ID)

GO

--ENROLMENT_AUD -> ADDRESS_AUD
WITH E_ADDRESS_IDS (ADDRESS_ID, AUDIT_STAMP) AS (
    SELECT E.ADDRESS_ID, MIN(E.AUDIT_STAMP)
    FROM ENROLMENT_AUD E
             LEFT OUTER JOIN ADDRESS_AUD A ON E.ADDRESS_ID = A.ID AND E.AUDIT_STAMP >= A.AUDIT_STAMP
    WHERE A.ID IS NULL AND E.ADDRESS_ID IS NOT NULL
    GROUP BY E.ADDRESS_ID
)

UPDATE A
SET A.AUDIT_STAMP = I.AUDIT_STAMP
FROM ADDRESS_AUD A, E_ADDRESS_IDS I
WHERE A.ID = I.ADDRESS_ID AND A.AUDIT_STAMP = (SELECT MIN(B.AUDIT_STAMP) FROM ADDRESS_AUD B WHERE A.ID = B.ID)

GO

--ENROLMENT_AUD -> CONTACT_AUD
--ENROLLING_VB
WITH E_CONTACT_IDS (CONTACT_ID, AUDIT_STAMP) AS (
    SELECT E.ENROLLING_VB_CONTACT_ID, MIN(E.AUDIT_STAMP)
    FROM ENROLMENT_AUD E
             LEFT OUTER JOIN CONTACT_AUD C ON E.ENROLLING_VB_CONTACT_ID = C.ID AND E.AUDIT_STAMP >= C.AUDIT_STAMP
    WHERE C.ID IS NULL AND E.ENROLLING_VB_CONTACT_ID IS NOT NULL
    GROUP BY E.ENROLLING_VB_CONTACT_ID
)

UPDATE C
SET C.AUDIT_STAMP = I.AUDIT_STAMP
FROM CONTACT_AUD C, E_CONTACT_IDS I
WHERE C.ID = I.CONTACT_ID AND C.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM CONTACT_AUD A WHERE A.ID = C.ID)

GO

--VENDOR
WITH E_CONTACT_IDS (CONTACT_ID, AUDIT_STAMP) AS (
    SELECT E.VENDOR_CONTACT_ID, MIN(E.AUDIT_STAMP)
    FROM ENROLMENT_AUD E
             LEFT OUTER JOIN CONTACT_AUD C ON E.VENDOR_CONTACT_ID = C.ID AND E.AUDIT_STAMP >= C.AUDIT_STAMP
    WHERE C.ID IS NULL AND E.VENDOR_CONTACT_ID IS NOT NULL
    GROUP BY E.VENDOR_CONTACT_ID
)

UPDATE C
SET C.AUDIT_STAMP = I.AUDIT_STAMP
FROM CONTACT_AUD C, E_CONTACT_IDS I
WHERE C.ID = I.CONTACT_ID AND C.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM CONTACT_AUD A WHERE A.ID = C.ID)

GO

--BUILDER
WITH E_CONTACT_IDS (CONTACT_ID, AUDIT_STAMP) AS (
    SELECT E.BUILDER_CONTACT_ID, MIN(E.AUDIT_STAMP)
    FROM ENROLMENT_AUD E
             LEFT OUTER JOIN CONTACT_AUD C ON E.BUILDER_CONTACT_ID = C.ID AND E.AUDIT_STAMP >= C.AUDIT_STAMP
    WHERE C.ID IS NULL AND E.BUILDER_CONTACT_ID IS NOT NULL
    GROUP BY E.BUILDER_CONTACT_ID
)

UPDATE C
SET C.AUDIT_STAMP = I.AUDIT_STAMP
FROM CONTACT_AUD C, E_CONTACT_IDS I
WHERE C.ID = I.CONTACT_ID AND C.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM CONTACT_AUD A WHERE A.ID = C.ID)

GO

--CONTACT_AUD -> ADDRESS_AUD
--ADDRESS
WITH C_ADDRESS_IDS (ADDRESS_ID, AUDIT_STAMP) AS (
    SELECT C.ADDRESS_ID, MIN(C.AUDIT_STAMP)
    FROM CONTACT_AUD C
             LEFT OUTER JOIN ADDRESS_AUD A ON C.ADDRESS_ID = A.ID AND C.AUDIT_STAMP >= A.AUDIT_STAMP
    WHERE A.ID IS NULL AND C.ADDRESS_ID IS NOT NULL
    GROUP BY C.ADDRESS_ID
)

UPDATE A
SET A.AUDIT_STAMP = I.AUDIT_STAMP
FROM ADDRESS_AUD A, C_ADDRESS_IDS I
WHERE A.ID = I.ADDRESS_ID AND A.AUDIT_STAMP = (SELECT MIN(B.AUDIT_STAMP) FROM ADDRESS_AUD B WHERE A.ID = B.ID)

GO

--MAILING_ADDRESS
WITH C_MAILING_ADDRESS_IDS (MAILING_ADDRESS_ID, AUDIT_STAMP) AS (
    SELECT C.MAILING_ADDRESS_ID, MIN(C.AUDIT_STAMP)
    FROM CONTACT_AUD C
             LEFT OUTER JOIN ADDRESS_AUD A ON C.MAILING_ADDRESS_ID = A.ID AND C.AUDIT_STAMP >= A.AUDIT_STAMP
    WHERE A.ID IS NULL AND C.MAILING_ADDRESS_ID IS NOT NULL
    GROUP BY C.MAILING_ADDRESS_ID
)

UPDATE A
SET A.AUDIT_STAMP = I.AUDIT_STAMP
FROM ADDRESS_AUD A, C_MAILING_ADDRESS_IDS I
WHERE A.ID = I.MAILING_ADDRESS_ID AND A.AUDIT_STAMP = (SELECT MIN(B.AUDIT_STAMP) FROM ADDRESS_AUD B WHERE A.ID = B.ID)

GO

--CONTACT_RELATIONSHIP_AUD -> CONTACT_AUD
--FROM
WITH R_CONTACT_IDS (CONTACT_ID, AUDIT_STAMP) AS (
    SELECT R.FROM_CONTACT_ID, MIN(R.AUDIT_STAMP)
    FROM CONTACT_RELATIONSHIP_AUD R
             LEFT OUTER JOIN CONTACT_AUD C ON R.FROM_CONTACT_ID = C.ID AND R.AUDIT_STAMP >= C.AUDIT_STAMP
    WHERE C.ID IS NULL
    GROUP BY R.FROM_CONTACT_ID
)

UPDATE C
SET C.AUDIT_STAMP = I.AUDIT_STAMP
FROM CONTACT_AUD C, R_CONTACT_IDS I
WHERE C.ID = I.CONTACT_ID AND C.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM CONTACT_AUD A WHERE A.ID = C.ID)

GO

--TO
WITH R_CONTACT_IDS (CONTACT_ID, AUDIT_STAMP) AS (
    SELECT R.TO_CONTACT_ID, MIN(R.AUDIT_STAMP)
    FROM CONTACT_RELATIONSHIP_AUD R
             LEFT OUTER JOIN CONTACT_AUD C ON R.TO_CONTACT_ID = C.ID AND R.AUDIT_STAMP >= C.AUDIT_STAMP
    WHERE C.ID IS NULL
    GROUP BY R.TO_CONTACT_ID
)

UPDATE C
SET C.AUDIT_STAMP = I.AUDIT_STAMP
FROM CONTACT_AUD C, R_CONTACT_IDS I
WHERE C.ID = I.CONTACT_ID AND C.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM CONTACT_AUD A WHERE A.ID = C.ID)

GO

--CONTACT_SECURITY_AUD -> CONTACT_AUD
WITH S_CONTACT_IDS (CONTACT_ID, AUDIT_STAMP) AS (
    SELECT S.CONTACT_ID, MIN(S.AUDIT_STAMP)
    FROM CONTACT_SECURITY_AUD S
             LEFT OUTER JOIN CONTACT_AUD C ON S.CONTACT_ID = C.ID AND S.AUDIT_STAMP >= C.AUDIT_STAMP
    WHERE C.ID IS NULL
    GROUP BY S.CONTACT_ID
)

UPDATE C
SET C.AUDIT_STAMP = I.AUDIT_STAMP
FROM CONTACT_AUD C, S_CONTACT_IDS I
WHERE C.ID = I.CONTACT_ID AND C.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM CONTACT_AUD A WHERE A.ID = C.ID)

GO

--CONTACT_SECURITY_AUD -> SECURITY_AUD
WITH CS_SECURITY_IDS (SECURITY_ID, AUDIT_STAMP) AS (
    SELECT CS.SECURITY_ID, MIN(CS.AUDIT_STAMP)
    FROM CONTACT_SECURITY_AUD CS
             LEFT OUTER JOIN SECURITY_AUD S ON CS.SECURITY_ID = S.ID AND CS.AUDIT_STAMP >= S.AUDIT_STAMP
    WHERE S.ID IS NULL
    GROUP BY CS.SECURITY_ID
)

UPDATE S
SET S.AUDIT_STAMP = I.AUDIT_STAMP
FROM SECURITY_AUD S, CS_SECURITY_IDS I
WHERE S.ID = I.SECURITY_ID AND S.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM SECURITY_AUD A WHERE A.ID = S.ID)

GO

--BRANCH_AUD -> ADDRESS_AUD
WITH B_ADDRESS_IDS (ADDRESS_ID, AUDIT_STAMP) AS (
    SELECT B.ADDRESS_ID, MIN(B.AUDIT_STAMP)
    FROM BRANCH_AUD B
             LEFT OUTER JOIN ADDRESS_AUD A ON B.ADDRESS_ID = A.ID AND B.AUDIT_STAMP >= A.AUDIT_STAMP
    WHERE A.ID IS NULL AND B.ADDRESS_ID IS NOT NULL
    GROUP BY B.ADDRESS_ID
)

UPDATE A
SET A.AUDIT_STAMP = I.AUDIT_STAMP
FROM ADDRESS_AUD A, B_ADDRESS_IDS I
WHERE A.ID = I.ADDRESS_ID AND A.AUDIT_STAMP = (SELECT MIN(B.AUDIT_STAMP) FROM CONTACT_AUD B WHERE A.ID = B.ID)

GO

--BRANCH_AUD -> FINANCIAL_INSTITUTION_AUD
WITH B_FI_IDS (FI_ID, AUDIT_STAMP) AS (
    SELECT B.FINANCIAL_INSTITUTION_ID, MIN(B.AUDIT_STAMP)
    FROM BRANCH_AUD B
             LEFT OUTER JOIN FINANCIAL_INSTITUTION_AUD F ON B.FINANCIAL_INSTITUTION_ID = F.ID AND B.AUDIT_STAMP >= F.AUDIT_STAMP
    WHERE F.ID IS NULL
    GROUP BY B.FINANCIAL_INSTITUTION_ID
)

UPDATE F
SET F.AUDIT_STAMP = I.AUDIT_STAMP
FROM FINANCIAL_INSTITUTION_AUD F, B_FI_IDS I
WHERE F.ID = I.FI_ID AND F.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM FINANCIAL_INSTITUTION_AUD A WHERE A.ID = F.ID)

GO

--RELEASE_REASON_AUD --> RELEASE_AUD
WITH R_RELEASE_IDS (RELEASE_ID, AUDIT_STAMP) AS (
    SELECT R.RELEASE_ID, MIN(R.AUDIT_STAMP)
    FROM RELEASE_REASON_AUD R
             LEFT OUTER JOIN RELEASE_AUD R2 ON R.RELEASE_ID = R2.ID AND R.AUDIT_STAMP >= R2.AUDIT_STAMP
    WHERE R2.ID IS NULL
    GROUP BY R.RELEASE_ID
)

UPDATE R
SET R.AUDIT_STAMP = I.AUDIT_STAMP
FROM RELEASE_AUD R, R_RELEASE_IDS I
WHERE R.ID = I.RELEASE_ID AND R.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM RELEASE_AUD A WHERE A.ID = R.ID)

GO

--RELEASE_ENROLMENT_AUD  -> RELEASE_AUD
WITH R_RELEASE_IDS (RELEASE_ID, AUDIT_STAMP) AS (
    SELECT R.RELEASE_ID, MIN(R.AUDIT_STAMP)
    FROM RELEASE_ENROLMENT_AUD R
             LEFT OUTER JOIN RELEASE_AUD R2 ON R.RELEASE_ID = R2.ID AND R.AUDIT_STAMP >= R2.AUDIT_STAMP
    WHERE R2.ID IS NULL
    GROUP BY R.RELEASE_ID
)

UPDATE R
SET R.AUDIT_STAMP = I.AUDIT_STAMP
FROM RELEASE_AUD R, R_RELEASE_IDS I
WHERE R.ID = I.RELEASE_ID AND R.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM RELEASE_AUD A WHERE A.ID = R.ID)

GO

--AUTO_RELEASE_ELIMINATION_AUD -> AUTO_RELEASE_ENROLMENT_AUD
WITH A_RELEASE_ENROLMENT_IDS (RELEASE_ID, AUDIT_STAMP) AS (
    SELECT A.AUTO_RELEASE_ENROLMENT_ID, MIN(A.AUDIT_STAMP)
    FROM AUTO_RELEASE_ELIMINATION_AUD A
             LEFT OUTER JOIN AUTO_RELEASE_ENROLMENT_AUD A2 ON A.AUTO_RELEASE_ENROLMENT_ID = A2.ID AND A.AUDIT_STAMP >= A2.AUDIT_STAMP
    WHERE A2.ID IS NULL
    GROUP BY A.AUTO_RELEASE_ENROLMENT_ID
)

UPDATE A
SET A.AUDIT_STAMP = I.AUDIT_STAMP
FROM AUTO_RELEASE_ENROLMENT_AUD A, A_RELEASE_ENROLMENT_IDS I
WHERE A.ID = I.RELEASE_ID AND A.AUDIT_STAMP = (SELECT MIN(B.AUDIT_STAMP) FROM AUTO_RELEASE_ENROLMENT_AUD B WHERE A.ID = B.ID)

GO

--AUTO_RELEASE_ENROLMENT_DATA -> AUTO_RELEASE_RUN_AUD
WITH A_RELEASE_RUN_IDS (RUN_ID, AUDIT_STAMP) AS (
    SELECT A.AUTO_RELEASE_RUN_ID, MIN(A.AUDIT_STAMP)
    FROM AUTO_RELEASE_ENROLMENT_DATA_AUD A
             LEFT OUTER JOIN AUTO_RELEASE_RUN_AUD A2 ON A.AUTO_RELEASE_RUN_ID = A2.ID AND A.AUDIT_STAMP >= A2.AUDIT_STAMP
    WHERE A2.ID IS NULL
    GROUP BY A.AUTO_RELEASE_RUN_ID
)

UPDATE A
SET A.AUDIT_STAMP = I.AUDIT_STAMP
FROM AUTO_RELEASE_RUN_AUD A, A_RELEASE_RUN_IDS I
WHERE A.ID = I.RUN_ID AND A.AUDIT_STAMP = (SELECT MIN(B.AUDIT_STAMP) FROM AUTO_RELEASE_RUN_AUD B WHERE A.ID = B.ID)

GO

--AUTO_RELEASE_VB_APPLICATION_STATUS -> AUTO_RELEASE_RUN_AUD
WITH A_RELEASE_RUN_IDS (RUN_ID, AUDIT_STAMP) AS (
    SELECT A.AUTO_RELEASE_RUN_ID, MIN(A.AUDIT_STAMP)
    FROM AUTO_RELEASE_VB_APPLICATION_STATUS_AUD A
             LEFT OUTER JOIN AUTO_RELEASE_RUN_AUD A2 ON A.AUTO_RELEASE_RUN_ID = A2.ID AND A.AUDIT_STAMP >= A2.AUDIT_STAMP
    WHERE A2.ID IS NULL
    GROUP BY A.AUTO_RELEASE_RUN_ID
)

UPDATE A
SET A.AUDIT_STAMP = I.AUDIT_STAMP
FROM AUTO_RELEASE_RUN_AUD A, A_RELEASE_RUN_IDS I
WHERE A.ID = I.RUN_ID AND A.AUDIT_STAMP = (SELECT MIN(B.AUDIT_STAMP) FROM AUTO_RELEASE_RUN_AUD B WHERE A.ID = B.ID)

GO

--AUTO_RELEASE_VB_DATA -> AUTO_RELEASE_RUN_AUD
WITH A_RELEASE_RUN_IDS (RUN_ID, AUDIT_STAMP) AS (
    SELECT A.AUTO_RELEASE_RUN_ID, MIN(A.AUDIT_STAMP)
    FROM AUTO_RELEASE_VB_DATA_AUD A
             LEFT OUTER JOIN AUTO_RELEASE_RUN_AUD A2 ON A.AUTO_RELEASE_RUN_ID = A2.ID AND A.AUDIT_STAMP >= A2.AUDIT_STAMP
    WHERE A2.ID IS NULL
    GROUP BY A.AUTO_RELEASE_RUN_ID
)

UPDATE A
SET A.AUDIT_STAMP = I.AUDIT_STAMP
FROM AUTO_RELEASE_RUN_AUD A, A_RELEASE_RUN_IDS I
WHERE A.ID = I.RUN_ID AND A.AUDIT_STAMP = (SELECT MIN(B.AUDIT_STAMP) FROM AUTO_RELEASE_RUN_AUD B WHERE A.ID = B.ID)

GO

--AUTO_RELEASE_ENROLMENT -> RELEASE_AUD
WITH A_RELEASE_IDS (RELEASE_ID, AUDIT_STAMP) AS (
    SELECT A.RELEASE_ID, MIN(A.AUDIT_STAMP)
    FROM AUTO_RELEASE_ENROLMENT_AUD A
             LEFT OUTER JOIN RELEASE_AUD R ON A.RELEASE_ID = R.ID AND A.AUDIT_STAMP >= R.AUDIT_STAMP
    WHERE R.ID IS NULL AND A.RELEASE_ID IS NOT NULL
    GROUP BY A.RELEASE_ID
)

UPDATE R
SET R.AUDIT_STAMP = I.AUDIT_STAMP
FROM RELEASE_AUD R, A_RELEASE_IDS I
WHERE R.ID = I.RELEASE_ID AND R.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM RELEASE_AUD A WHERE A.ID = R.ID)

GO

--AUTO_RELEASE_ENROLMENT -> ENROLMENT_AUD
WITH A_ENROLMENT_NUMBERS (ENROLMENT_NUMBER, AUDIT_STAMP) AS (
    SELECT A.ENROLMENT_NUMBER, MIN(A.AUDIT_STAMP)
    FROM AUTO_RELEASE_ENROLMENT_AUD A
             LEFT OUTER JOIN ENROLMENT_AUD E ON A.ENROLMENT_NUMBER = E.ENROLMENT_NUMBER AND A.AUDIT_STAMP >= E.AUDIT_STAMP
    WHERE E.ENROLMENT_NUMBER IS NULL
    GROUP BY A.ENROLMENT_NUMBER
)

UPDATE E
SET E.AUDIT_STAMP = I.AUDIT_STAMP
FROM ENROLMENT_AUD E, A_ENROLMENT_NUMBERS I
WHERE E.ENROLMENT_NUMBER = I.ENROLMENT_NUMBER AND E.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM ENROLMENT_AUD A WHERE A.ENROLMENT_NUMBER = E.ENROLMENT_NUMBER)

GO

--AUTO_RELEASE_ENROLMENT -> SECURITY_AUD
WITH A_SECURITY_IDS (SECURITY_ID, AUDIT_STAMP) AS (
    SELECT A.SECURITY_ID, MIN(A.AUDIT_STAMP)
    FROM AUTO_RELEASE_ENROLMENT_AUD A
             LEFT OUTER JOIN SECURITY_AUD S ON A.SECURITY_ID = S.ID AND A.AUDIT_STAMP >= S.AUDIT_STAMP
    WHERE S.ID IS NULL
    GROUP BY A.SECURITY_ID
)

UPDATE S
SET S.AUDIT_STAMP = I.AUDIT_STAMP
FROM SECURITY_AUD S, A_SECURITY_IDS I
WHERE S.ID = I.SECURITY_ID AND S.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM SECURITY_AUD A WHERE A.ID = S.ID)

GO

--RELEASE_ENROLMENT -> ENROLMENT
WITH R_ENROLMENT_NUMBERS (ENROLMENT_NUMBER, AUDIT_STAMP) AS (
    SELECT R.ENROLMENT_NUMBER, MIN(R.AUDIT_STAMP)
    FROM RELEASE_ENROLMENT_AUD R
             LEFT OUTER JOIN ENROLMENT_AUD E ON R.ENROLMENT_NUMBER = E.ENROLMENT_NUMBER AND R.AUDIT_STAMP >= E.AUDIT_STAMP
    WHERE E.ENROLMENT_NUMBER IS NULL
    GROUP BY R.ENROLMENT_NUMBER
)

UPDATE E
SET E.AUDIT_STAMP = I.AUDIT_STAMP
FROM ENROLMENT_AUD E, R_ENROLMENT_NUMBERS I
WHERE E.ENROLMENT_NUMBER = I.ENROLMENT_NUMBER AND E.AUDIT_STAMP = (SELECT MIN(A.AUDIT_STAMP) FROM ENROLMENT_AUD A WHERE A.ENROLMENT_NUMBER = E.ENROLMENT_NUMBER)

GO

DECLARE @stamps TABLE (AUDIT_STAMP datetime2);
CREATE TABLE #revs (REV int, REVTSTMP bigint, AUDIT_STAMP datetime2);
DECLARE @auditTables CURSOR;
DECLARE @tableName VARCHAR(200);
DECLARE @sql VARCHAR(max) = '';
DECLARE @updateSql VARCHAR(max) = '';
SET @auditTables = CURSOR FOR SELECT name FROM sys.tables WHERE NAME LIKE '%_AUD%' AND NAME NOT LIKE '%DATAMIG%';

OPEN @auditTables;
FETCH NEXT
    FROM @auditTables INTO @tableName;
WHILE @@FETCH_STATUS = 0
BEGIN
    SET @sql = @sql + ' UNION SELECT AUDIT_STAMP FROM '+@tableName;
    SET @updateSql = 'UPDATE '+@tableName+' SET AUDIT_STAMP = ''1970-01-01 00:00:00.0000000'' WHERE AUDIT_STAMP < ''1970-01-01''';
    EXEC(@updateSql);
    FETCH NEXT FROM @auditTables INTO @tableName;
END;


SET @sql = (SELECT RIGHT(@sql, LEN(@sql)-7));
INSERT INTO @stamps
    EXEC(@sql);

INSERT INTO #revs
SELECT ROW_NUMBER() OVER(ORDER BY AUDIT_STAMP ASC) AS REV,
       (CAST(DATEDIFF(second, '1970-01-01', CAST(AUDIT_STAMP AS DATE)) AS DECIMAL(31, 0)) * 1000) + DATEDIFF(ms, CAST(AUDIT_STAMP AS DATE), AUDIT_STAMP) AS REVTSTMP,
       AUDIT_STAMP
FROM @stamps;

DELETE FROM @stamps;

SET IDENTITY_INSERT REVINFO ON
INSERT INTO REVINFO (REV, REVTSTMP)
SELECT REV, REVTSTMP FROM #revs;
SET IDENTITY_INSERT REVINFO OFF

SET @auditTables = CURSOR FOR SELECT name FROM sys.tables WHERE NAME LIKE '%_AUD%' AND NAME NOT LIKE '%DATAMIG%';

OPEN @auditTables;
FETCH NEXT
    FROM @auditTables INTO @tableName;
WHILE @@FETCH_STATUS = 0
BEGIN
    EXEC('UPDATE T SET T.REV = R.REV FROM '+@tableName+' T JOIN #revs R ON T.AUDIT_STAMP = R.AUDIT_STAMP');
    FETCH NEXT FROM @auditTables INTO @tableName;
END;

CLOSE @auditTables;
DEALLOCATE @auditTables;
DROP TABLE #revs;

GO

WITH CHANGES (REV, ENTITYNAME) AS (
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.security.VbPoolEntity' FROM VB_POOL_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionEntity' FROM FINANCIAL_INSTITUTION_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.security.PoolEntity' FROM POOL_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.financialinstitution.BranchEntity' FROM BRANCH_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.autorelease.AutoReleaseRunEntity' FROM AUTO_RELEASE_RUN_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.release.ReleaseEnrolmentEntity' FROM RELEASE_ENROLMENT_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.release.ReleaseReasonEntity' FROM RELEASE_REASON_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.autorelease.AutoReleaseEnrolmentEntity' FROM AUTO_RELEASE_ENROLMENT_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.enrolment.EnrolmentPoolEntity' FROM ENROLMENT_POOL_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.autorelease.AutoReleaseEliminationEntity' FROM AUTO_RELEASE_ELIMINATION_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.contact.ContactEntity' FROM CONTACT_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.contact.ContactRelationshipEntity' FROM CONTACT_RELATIONSHIP_AUD  UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.release.ReleaseEntity' FROM RELEASE_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.security.SecurityEntity' FROM SECURITY_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity' FROM ENROLMENT_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.AddressEntity' FROM ADDRESS_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.alert.AlertEntity' FROM ALERT_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.autorelease.AutoReleaseEnrolmentDataEntity' FROM AUTO_RELEASE_ENROLMENT_DATA_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.autorelease.AutoReleaseVbApplicationStatusEntity' FROM AUTO_RELEASE_VB_APPLICATION_STATUS_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.autorelease.AutoReleaseVbDataEntity' FROM AUTO_RELEASE_VB_DATA_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.contact.ContactSecurityEntity' FROM CONTACT_SECURITY_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.financialinstitution.ContactFinancialInstitutionEntity' FROM  CONTACT_FINANCIAL_INSTITUTION_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionAlertEntity' FROM FINANCIAL_INSTITUTION_ALERT_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionMaaPoaEntity' FROM FINANCIAL_INSTITUTION_MAAPOA_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionMaaPoaInstitutionEntity' FROM FINANCIAL_INSTITUTION_MAAPOA_INSTITUTION_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionSigningOfficerEntity' FROM FINANCIAL_INSTITUTION_SIGNING_OFFICER_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.interest.InterestRateEntity' FROM INTEREST_RATE_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.userprofile.RoleEntity' FROM ROLE_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.userprofile.RolePermissionEntity' FROM  ROLE_PERMISSION_AUD UNION
    SELECT DISTINCT(REV), 'com.tarion.vbs.orm.entity.userprofile.UserRoleEntity' FROM USER_ROLE_AUD
)

INSERT INTO REVCHANGES (REV, ENTITYNAME)
SELECT REV, ENTITYNAME FROM CHANGES ORDER BY REV ASC, ENTITYNAME ASC

GO

DELETE FROM REVINFO WHERE REV = 0

GO

DECLARE @auditTables CURSOR;
DECLARE @tableName VARCHAR(200);
DECLARE @sql VARCHAR(max);
SET @auditTables = CURSOR FOR SELECT name FROM sys.tables WHERE NAME LIKE '%_AUD%' AND NAME NOT LIKE '%DATAMIG%';
OPEN @auditTables;
FETCH NEXT
    FROM @auditTables INTO @tableName;
WHILE @@FETCH_STATUS = 0
BEGIN
    IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @tableName AND COLUMN_NAME = 'AUDIT_STAMP')
        BEGIN
            SET @sql = 'ALTER TABLE ' + @tableName + ' DROP COLUMN AUDIT_STAMP';
            EXEC(@sql);
        END;

    FETCH NEXT FROM @auditTables INTO @tableName;
END;
CLOSE @auditTables;
DEALLOCATE @auditTables;

GO
DROP TABLE DATAMIG_PSAUDIT
GO
ALTER TABLE SECURITY_AUD ADD CONSTRAINT PK_SECURITY_AUD PRIMARY KEY CLUSTERED ([ID] ASC, [REV] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE POOL_AUD ADD CONSTRAINT PK_POOL_AUD PRIMARY KEY CLUSTERED ([ID] ASC, [REV] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE VB_POOL_AUD ADD CONSTRAINT PK_VB_POOL_AUD PRIMARY KEY CLUSTERED ([ID] ASC, [REV] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE FINANCIAL_INSTITUTION_AUD ADD CONSTRAINT PK_FINANCIAL_INSTITUTION_AUD PRIMARY KEY CLUSTERED ([ID] ASC, [REV] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET NOCOUNT OFF