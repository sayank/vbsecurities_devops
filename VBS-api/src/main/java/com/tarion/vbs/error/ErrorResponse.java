package com.tarion.vbs.error;

import java.io.Serializable;
import java.util.List;

import com.tarion.vbs.common.dto.ErrorDTO;
import org.springframework.http.HttpStatus;

public class ErrorResponse implements Serializable {

	private static final long serialVersionUID = 2822351579602289253L;

	private String requestUrl;
	private String error;
	private Exception exception;
	private HttpStatus httpStatus;
	private Integer httpStatusCode;
	private List<ErrorDTO> validationErrorList;
	
	public ErrorResponse(String requestUrl, String error, Exception exception, HttpStatus httpStatus, List<ErrorDTO> validationErrorList){
		this.requestUrl = requestUrl;
		this.error = error;
		this.exception = exception;
		this.httpStatus = httpStatus;
		this.httpStatusCode = httpStatus.value();
		this.validationErrorList = validationErrorList;
	}
	
	public String getRequestUrl() {
		return requestUrl;
	}
	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public Exception getException() {
		return exception;
	}
	public void setException(Exception exception) {
		this.exception = exception;
	}
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
	public Integer getHttpStatusCode() {
		return httpStatusCode;
	}
	public void setHttpStatusCode(Integer httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}

	public List<ErrorDTO> getValidationErrorList() {
		return validationErrorList;
	}

	public void setValidationErrorList(List<ErrorDTO> validationErrorList) {
		this.validationErrorList = validationErrorList;
	}
}