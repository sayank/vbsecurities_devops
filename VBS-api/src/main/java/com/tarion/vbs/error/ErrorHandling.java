package com.tarion.vbs.error;

import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.exceptions.OptimisticLockException;
import com.tarion.vbs.common.exceptions.SaveValidationFailureException;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.exceptions.VbsValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.ejb.EJBException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestControllerAdvice("com.tarion.vbs.controller")
public class ErrorHandling {

	@Autowired
	private MessageSource messageSource;

	@ExceptionHandler(EJBException.class)		  
	public ResponseEntity<ErrorResponse> ejbException(final HttpServletRequest req, final EJBException exception) {
		if(getCause(exception) instanceof VbsValidationException) {
			VbsValidationException e = (VbsValidationException) getCause(exception);
			final String error = "Validation Errors exist.";
			return error(req, error, null, HttpStatus.UNPROCESSABLE_ENTITY, e.getErrors());
		}
		if(getCause(exception) instanceof OptimisticLockException) {
			final String error = messageSource.getMessage("optLock.exception.error", null, null);
			return error(req, error, exception, HttpStatus.CONFLICT, null);
		}

		if(getCause(exception) instanceof SaveValidationFailureException){
			final String error = "Validation Errors prevented this item from being saved.";
			return error(req, error, exception, HttpStatus.NOT_ACCEPTABLE,null);
		}
		
		if(getCause(exception) instanceof VbsCheckedException) {
			final String error = messageSource.getMessage("checked.exception.error", null, null);
			return error(req, error, exception, HttpStatus.INTERNAL_SERVER_ERROR, null);
		}

		final String error = messageSource.getMessage("ejb.exception.error", null, null);
		return error(req, error, exception, HttpStatus.BAD_GATEWAY, null);
	}		
	
	private ResponseEntity<ErrorResponse> error(final HttpServletRequest req, final String error, final Exception exception, final HttpStatus httpStatus, List<ErrorDTO> errorList) {
		return new ResponseEntity<>(new ErrorResponse(req.getRequestURL().toString(), error, exception, httpStatus, errorList), httpStatus);
	}
	
	private Throwable getCause(Throwable e) {
		Throwable cause;
		Throwable result = e;

		while(null != (cause = result.getCause())  && (result != cause) ) {
			result = cause;
		}
		return result;
	}
	
}