package com.tarion.vbs.session;

import java.io.Serializable;

import com.tarion.vbs.common.dto.security.SecuritySearchParametersDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchResultWrapper;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.tarion.vbs.common.dto.user.UserDTO;


@Component
@Scope(value="session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SessionStore implements Serializable {

	private static final long serialVersionUID = -4397909014593858380L;
	
	private UserDTO userDTO;
	private SecuritySearchResultWrapper lastSearchResult;
	private SecuritySearchParametersDTO lastSearchParams;

	public UserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public SecuritySearchResultWrapper getLastSearchResult() {
		return lastSearchResult;
	}

	public void setLastSearchResult(SecuritySearchResultWrapper lastSearchResult) {
		this.lastSearchResult = lastSearchResult;
	}

	public SecuritySearchParametersDTO getLastSearchParams() {
		return lastSearchParams;
	}

	public void setLastSearchParams(SecuritySearchParametersDTO lastSearchParams) {
		this.lastSearchParams = lastSearchParams;
	}
}
