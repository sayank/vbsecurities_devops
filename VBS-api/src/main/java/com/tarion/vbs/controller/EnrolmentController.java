package com.tarion.vbs.controller;

import com.tarion.vbs.common.dto.enrolment.CEEnrolmentDecisionDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentActivityDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/enrolment")
public class EnrolmentController extends Base {


	@GetMapping(value="/availableEnrolments")
	public List<EnrolmentDTO> getAvailableEnrolments(@RequestParam("poolId") Long poolId, @RequestParam("onlyCondoConv") boolean onlyCondoConv, @RequestParam("securityIdentityTypeId") Long securityIdentityTypeId) throws VbsCheckedException{
		return businessServiceLocator.getEnrolmentService().getAvailableEnrolmentListByPoolId(poolId, onlyCondoConv, securityIdentityTypeId);
	}

	@GetMapping(value="/enrolment")
	public EnrolmentDTO getEnrolment(@RequestParam("enrolmentNumber") String enrolmentNumber, @RequestParam("poolId") Long poolId, @RequestParam("securityIdentityTypeId") Long securityIdentityTypeId) {
		return businessServiceLocator.getEnrolmentService().getEnrolment(enrolmentNumber, poolId, securityIdentityTypeId);
	}

	@GetMapping(value="/enrolmentForSecurity")
	public EnrolmentDTO getEnrolmentForSecurity(@RequestParam("enrolmentNumber") String enrolmentNumber, @RequestParam("securityId") Long securityId) {
		return businessServiceLocator.getEnrolmentService().getEnrolmentForSecurity(enrolmentNumber, securityId);
	}
	@GetMapping(value="/getCRMEnrolmentList")
	public List<EnrolmentDTO> getCRMEnrolmentList(@RequestParam("poolId") Long poolId){
		return businessServiceLocator.getEnrolmentService().getCRMEnrolmentListByPoolId(poolId);
	}
	@GetMapping(value="/getCRMEnrolment")
	public EnrolmentDTO getCRMEnrolmentList(@RequestParam("enrolmentNumber") String enrolmentNumber){
		return businessServiceLocator.getEnrolmentService().getCRMEnrolment(enrolmentNumber);
	}
	@GetMapping(value="/getEnrolmentActivities")
	public List<EnrolmentActivityDTO> getEnrolmentActivities(@RequestParam("enrolmentNumber") String enrolmentNumber){
		return businessServiceLocator.getEnrolmentService().getEnrolmentActivitiesForEnrolmentNumber(enrolmentNumber);
	}

	@GetMapping(value="/decision")
	public CEEnrolmentDecisionDTO getDecisionDTOByEnrolmentNumber(@RequestParam("enrolmentNumber") String enrolmentNumber){
		return businessServiceLocator.getSecurityService().getOutstandingDecisionByEnrolmentNumber(enrolmentNumber);
	}

	@PostMapping(value="/decision")
	public void updateCEEnrolmentDecision(@RequestBody CEEnrolmentDecisionDTO ceEnrolmentDecisionDTO){
		businessServiceLocator.getSecurityService().updateCEEnrolmentDecision(ceEnrolmentDecisionDTO, getUserDTO());
	}
}
