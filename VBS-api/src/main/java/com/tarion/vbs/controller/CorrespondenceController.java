package com.tarion.vbs.controller;


import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tarion.vbs.common.constants.PermissionConstants;
import com.tarion.vbs.common.dto.correspondence.GenerateCorrespondenceDTO;

@RestController
@RequestMapping("/api/correspondence")
public class CorrespondenceController extends Base{

	@Secured({PermissionConstants.P_CORRESPONDENCE})
	@PostMapping(value="/generate")
	public GenerateCorrespondenceDTO generate(@RequestBody GenerateCorrespondenceDTO generateCorrespondenceDTO){
		return businessServiceLocator.getCorrespondenceService().generate(generateCorrespondenceDTO);
	}
	
}
