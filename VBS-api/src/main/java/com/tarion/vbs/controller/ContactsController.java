package com.tarion.vbs.controller;

import com.tarion.vbs.common.contact.EscrowAgentSearchParamsDTO;
import com.tarion.vbs.common.contact.EscrowAgnetSearchResultWrapper;
import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.common.enums.CRMSearchTypeEnum;
import com.tarion.vbs.web.service.BusinessServiceLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/contacts")
public class ContactsController extends Base{

    @GetMapping("/searchCrmContacts")
    public List<ContactDTO> searchCrmContacts(@RequestParam(value="search", required = false) String search, @RequestParam(value="type", required = false) CRMSearchTypeEnum type){
        if(search != null) {
            return businessServiceLocator.getContactService().findPersonOrCompanyInCrm(search, type);
        }
        return new ArrayList<>();
    }

    @GetMapping(value="/getLawyersByEscrowAgentCrmContactId")
    public List<ContactDTO> getLawyersByEscrowAgentCrmContactId(@RequestParam("crmContactId") String crmContactId){
        return businessServiceLocator.getContactService().getLawyersByEscrowAgentCrmContactId(crmContactId);
    }

    @GetMapping(value="/getAssistantsByEscrowAgentCrmContactId")
    public List<ContactDTO> getAssistantsByEscrowAgentId(@RequestParam("crmContactId") String crmContactId){
        return businessServiceLocator.getContactService().getAssistantsByEscrowAgentCrmContactId(crmContactId);
    }

    @GetMapping(value="/escrowAgentTypeahead")
    public List<ContactDTO> escrowAgentTypeahead(@RequestParam("partial") String partial){
        return businessServiceLocator.getContactService().getAllEscrowAgentsLike(partial.toUpperCase().trim());
    }

    @PostMapping(value="/escrowAgentSearch")
    public EscrowAgnetSearchResultWrapper escrowSearch(@RequestBody EscrowAgentSearchParamsDTO escrowAgentSearchParamsDTO){
        return businessServiceLocator.getContactService().escrowAgentSearch(escrowAgentSearchParamsDTO);
    }

    @GetMapping(value="/getContact")
    public ContactDTO getContactByCrmContactId(@RequestParam("crmContactId") String crmContactId){
         return businessServiceLocator.getContactService().findContactByCrmContactId(crmContactId, null);
    }

}
