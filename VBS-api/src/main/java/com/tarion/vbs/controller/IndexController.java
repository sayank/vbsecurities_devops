package com.tarion.vbs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

@RestController
public class IndexController extends Base {

	@Autowired
	private ServletContext context;

	@GetMapping("/monitor")
	public String monitor(){
		return "VBS STATUS OK";
	}

	@GetMapping("/manifest")
	public Attributes manifest() throws IOException {
		Manifest mf = new Manifest(context.getResourceAsStream("/META-INF/MANIFEST.MF"));
		return mf.getMainAttributes();
	}

	@GetMapping("/uiSetup")
	public Map<String, Object> getUiSetupMap(){
		return businessServiceLocator.getLookupService().getUiSetupMap();
	}
	
}