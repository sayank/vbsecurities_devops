 package com.tarion.vbs.controller;

import com.tarion.vbs.common.constants.PermissionConstants;
import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.dto.alert.AlertDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentPoolActivityDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionMaaPoaDTO;
import com.tarion.vbs.common.dto.security.*;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.util.VbsUtil;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/security")
public class SecurityController extends Base{

	@GetMapping(value="/getHistoryComment")
	public List<HistoryFieldDTO> getHistoryComment(@RequestParam("id") Long id){
		return businessServiceLocator.getSecurityService().getSecurityCommentHistory(id);
	}

	@PostMapping(value="/searchSecurity")
	public SecuritySearchResultWrapper searchSecurity(@RequestBody SecuritySearchParametersDTO securitySearchParametersDTO){
		SecuritySearchResultWrapper wrapper = businessServiceLocator.getSecurityService().searchSecurity(securitySearchParametersDTO, getUserDTO());
		sessionStore.setLastSearchParams(securitySearchParametersDTO);
		sessionStore.setLastSearchResult(wrapper);
		return wrapper;
	}
	
	@PostMapping(value="/searchResultsExcel/{showInterest}")
	public ResponseEntity<Object> searchResultsExcel(@RequestBody SecuritySearchParametersDTO securitySearchParametersDTO, @PathVariable(name="showInterest") Boolean showInterest, HttpServletResponse response) {
		try {
			byte[] file;
			if(sessionStore.getLastSearchResult() == null){
				file = businessServiceLocator.getExcelService().createSearchResultsExcel(securitySearchParametersDTO, showInterest, getUserDTO());
			}else {
				file = businessServiceLocator.getExcelService().createSearchResultsExcel(sessionStore.getLastSearchParams(), sessionStore.getLastSearchResult(), showInterest);
			}
			ByteArrayResource resource = new ByteArrayResource(file);
			
			HttpHeaders headers = new HttpHeaders(); 
			headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=security-search-results-"+LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()+".xslx");
			
		    return ResponseEntity.ok()
		            .headers(headers)
		            .contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
		            .body(resource);
		}catch(VbsCheckedException vbs) {
			return new ResponseEntity<>("fail", new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@GetMapping(value="/getSecurity")
	public SecurityDTO getSecurity(@RequestParam("id") Long id){
		return businessServiceLocator.getSecurityService().getSecurity(id);
	}


	@GetMapping(value="/getEnrolmentActivities")
	public List<EnrolmentPoolActivityDTO> getEnrolmentActivities(@RequestParam("securityId") Long securityId){
		return businessServiceLocator.getSecurityService().getDeletedEnrolmentsActivities(securityId);
	}

	@GetMapping(value="/getDeletedVbActivities")
	public List<EnrolmentPoolActivityDTO> getDeletedVbActivities(@RequestParam("poolId") Long poolId){
		return businessServiceLocator.getSecurityService().getDeletedBbActivities(poolId);
	}

	@Secured(PermissionConstants.P_CREATE_SECURITY)
	@PostMapping(value="/saveSecurity")
	public SecurityDTO saveSecurity(@RequestBody SecurityDTO securityDTO, @RequestParam(name="displayedAlertsConfirmations", required = false) String displayedAlertsConfirmations) throws VbsCheckedException {
		List<String> list = new ArrayList<>();
		if(!VbsUtil.isNullorEmpty(displayedAlertsConfirmations)) {
			list.addAll(Arrays.asList(displayedAlertsConfirmations.split("-")));
		}

		return this.getSecurity(businessServiceLocator.getSecurityService().saveSecurity(securityDTO, list, getUserDTO()));
	}

	@Secured({PermissionConstants.P_DELETE_SECURITY_CANCEL_RELEASE})
	@GetMapping(value="/validateDeleteSecurity")
	public ResponseEntity<List<ErrorDTO>> validateDeleteSecurity(@RequestParam("securityId") Long securityId){
		List<ErrorDTO> errors = businessServiceLocator.getSecurityService().validateDeleteSecurity(securityId);
		if(errors != null && !errors.isEmpty()){
			return new ResponseEntity<>(errors, new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
		return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.OK);
	}

	@Secured({PermissionConstants.P_DELETE_SECURITY_CANCEL_RELEASE})
	@GetMapping(value="/deleteSecurity")
	public void deleteSecurity(@RequestParam("securityId") Long securityId)
			throws VbsCheckedException {
		businessServiceLocator.getSecurityService().deleteSecurity(securityId, getUserDTO());
	}

	@PostMapping(value="/recalculateMaaPoaAmounts")
	public FinancialInstitutionMaaPoaDTO recalculateMaaPoaAmounts(@RequestBody SecurityDTO securityDTO) {
		return businessServiceLocator.getFinancialInstitutionService().getMaaPoa(securityDTO);
	}

	@GetMapping(value="/getMaaPoaDetails")
	public FinancialInstitutionMaaPoaDTO getMaaPoaDetails(@RequestParam Long securityId) {
		return businessServiceLocator.getFinancialInstitutionService().getMaaPoa(securityId);
	}

	@PostMapping(value="/poolSecurities")
	public ResponseEntity<List<ErrorDTO>> poolSecurities(@RequestBody PoolSecuritiesParametersDTO poolSecuritiesParametersDTO) throws VbsCheckedException {
		List<ErrorDTO> errors = businessServiceLocator.getSecurityService().poolSecurities(poolSecuritiesParametersDTO, getUserDTO());
		if (errors != null && !errors.isEmpty()) {
			return new ResponseEntity<>(errors, new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
		return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping(value="/validateAlert")
    public void validateAlert(@RequestBody AlertDTO alertDTO){
        businessServiceLocator.getSecurityService().validateAlert(alertDTO, null, 0);
	}
	
	@PostMapping(value="/searchDeletedSecurity")
	public List<SecuritySearchResultDTO> searchDeletedSecurity(@RequestBody DeletedSecuritySearchParametersDTO deletedSecuritySearchParametersDTO){
		List<SecuritySearchResultDTO> searchResults = businessServiceLocator.getSecurityService().searchDeletedSecurity(deletedSecuritySearchParametersDTO, getUserDTO());
		return searchResults;
	}
	
	@PostMapping(value="/searchDeletedSecurityExcel")
	public ResponseEntity<Object> searchDeletedSecurityExcel(@RequestBody DeletedSecuritySearchParametersDTO deletedSecuritySearchParametersDTO, HttpServletResponse response) {
		try {
			byte[] file = businessServiceLocator.getExcelService().searchDeletedSecurityExcel(deletedSecuritySearchParametersDTO, getUserDTO());
			ByteArrayResource resource = new ByteArrayResource(file);
			
			HttpHeaders headers = new HttpHeaders(); 
			headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=deleted-security-search-results-"+LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()+".xslx");
			
		    return ResponseEntity.ok()
		            .headers(headers)
		            .contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
		            .body(resource);
		}catch(VbsCheckedException vbs) {
			return new ResponseEntity<>("fail", new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

}
