package com.tarion.vbs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.session.SessionStore;
import com.tarion.vbs.web.service.BusinessServiceLocator;

@Component
public class Base {

	@Autowired
	protected BusinessServiceLocator businessServiceLocator;
	@Autowired
	protected MessageSource messageSource;
	@Autowired
	protected SessionStore sessionStore;
	
	public UserDTO getUserDTO() {
		return sessionStore.getUserDTO();
	}
	public void setUserDTO(UserDTO userDTO) {
		sessionStore.setUserDTO(userDTO);
	}
	
}