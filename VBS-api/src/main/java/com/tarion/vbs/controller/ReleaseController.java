package com.tarion.vbs.controller;

import com.tarion.vbs.common.constants.PermissionConstants;
import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.dto.autorelease.AutoReleaseResultDTO;
import com.tarion.vbs.common.dto.autorelease.AutoReleaseResultWrapper;
import com.tarion.vbs.common.dto.autorelease.AutoReleaseRunSearchParametersDTO;
import com.tarion.vbs.common.dto.autorelease.AutoReleaseRunSearchResultDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentDTO;
import com.tarion.vbs.common.dto.release.AutoReleaseDTO;
import com.tarion.vbs.common.dto.release.PendingReleaseDTO;
import com.tarion.vbs.common.dto.release.ReleaseDTO;
import com.tarion.vbs.common.dto.release.ReleaseSearchParametersDTO;
import com.tarion.vbs.common.enums.ReleaseStatus;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@RestController
@RequestMapping("/api/release")
public class ReleaseController extends Base{
	
	@GetMapping(value="/getReleases")
	public List<ReleaseDTO> getReleases(@RequestParam("securityId") Long securityId) throws VbsCheckedException{
		return businessServiceLocator.getReleaseService().getReleases(securityId);
	}
	
	@GetMapping(value="/getEnrolmentsForRelease")
	public List<EnrolmentDTO> getEnrolmentsForRelease(@RequestParam("securityId") Long securityId){
		return businessServiceLocator.getReleaseService().getUnreleasedEnrolmentsForSecurity(securityId);
	}
	
	@GetMapping(value="/getAutoReleaseResults")
	public AutoReleaseResultWrapper getAutoReleaseResults(@RequestParam("runId") Long runId) throws VbsCheckedException{
		return businessServiceLocator.getAutoReleaseService().getAutoReleaseResults(runId);
	}

	@PostMapping(value="/saveAutoReleaseEnrolments")
	public void saveAutoReleaseEnrolments(@RequestBody List<AutoReleaseResultDTO> autoReleaseEnrolments, @RequestParam("runId") Long runId) throws VbsCheckedException {
		businessServiceLocator.getAutoReleaseService().saveAutoReleaseEnrolments(runId, autoReleaseEnrolments, getUserDTO());
	}

	@Secured({PermissionConstants.P_AUTO_RELEASE})
	@PostMapping(value="/finalizeAutoRelease")
	public void finalizeAutoRelease(@RequestBody List<AutoReleaseResultDTO> autoReleaseEnrolments, @RequestParam("runId") Long runId) throws VbsCheckedException {
		businessServiceLocator.getAutoReleaseService().triggerFinalizeAutoRelease(runId, autoReleaseEnrolments, getUserDTO());
	}

	@Secured({PermissionConstants.P_CREATE_DEMAND,PermissionConstants.P_CREATE_SECURITY})
	@PostMapping(value="/saveRelease")
	public ReleaseDTO saveRelease(@RequestBody ReleaseDTO releaseDTO) throws VbsCheckedException {
		Long releaseId = businessServiceLocator.getReleaseService().saveRelease(releaseDTO, getUserDTO());
		return businessServiceLocator.getReleaseService().getReleaseDTO(releaseId);
	}

	@Secured({PermissionConstants.P_PENDING_RELEASE})
	@PostMapping(value="/sendReleaseListToFMS")
	public List<PendingReleaseDTO> sendReleaseListToFMS(@RequestBody List<PendingReleaseDTO> pendingReleaseDTO) throws VbsCheckedException {
		return businessServiceLocator.getReleaseService().sendReleaseListToFms(pendingReleaseDTO, getUserDTO());
	}

	@Secured({PermissionConstants.P_PENDING_RELEASE})
	@GetMapping(value="/getPendingReleases")
	public List<PendingReleaseDTO> getPendingReleases() throws VbsCheckedException {
		return businessServiceLocator.getReleaseService().getReleaseListByStatus(ReleaseStatus.PENDING);
	}

	@Secured({PermissionConstants.P_PENDING_RELEASE})
	@GetMapping(value="/getPendingAnsSentReleases")
	public List<PendingReleaseDTO> getPendingAndSentReleases() throws VbsCheckedException {
		return businessServiceLocator.getReleaseService().getReleaseListByStatuses(ReleaseStatus.PENDING, ReleaseStatus.SENT);
	}

	@PostMapping(value="/searchReleases")
	public List<PendingReleaseDTO> searchReleases(@RequestBody ReleaseSearchParametersDTO releaseSearchParametersDTO) throws VbsCheckedException {
		return businessServiceLocator.getReleaseService().searchReleases(releaseSearchParametersDTO);
	}

	@PostMapping(value="/validateRelease")
	public ResponseEntity<List<ErrorDTO>> validateRelease(@RequestBody ReleaseDTO releaseDTO) throws VbsCheckedException{
		List<ErrorDTO> errors = businessServiceLocator.getReleaseService().validateRelease(releaseDTO, getUserDTO());
		if(errors != null && !errors.isEmpty()){
			return new ResponseEntity<>(errors, new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
		return null;
	}

	@PostMapping(value="/searchAutoReleaseResults")
	public List<AutoReleaseRunSearchResultDTO> searchAutoReleaseResults(@RequestBody AutoReleaseRunSearchParametersDTO autoReleaseRunSearchParametersDTO){
		return businessServiceLocator.getAutoReleaseService().searchAutoReleaseRuns(autoReleaseRunSearchParametersDTO);
	}

	@Secured({PermissionConstants.P_AUTO_RELEASE})
	@PostMapping(value="/startAutoReleaseProcess")
	public void startAutoReleaseProcess() throws VbsCheckedException {
		businessServiceLocator.getAutoReleaseService().startAutoReleaseProcess(getUserDTO());
	}

	@GetMapping(value="/existsRunInProgress")
	public boolean existsRunInProgress() {
		return businessServiceLocator.getAutoReleaseService().existsRunInProgress();
	}

	@GetMapping(value="/setupRelease")
	public ReleaseDTO setupRelease(@RequestParam("releaseTypeId") Long releaseTypeId, @RequestParam("securityId") Long securityId){
		return businessServiceLocator.getReleaseService().setupRelease(releaseTypeId, securityId, getUserDTO());
	}
	
	@PostMapping(value="/getAutoReleasePassedResultsExcel/{runId}")
	public ResponseEntity<Object> getAutoReleasePassedResultsExcel(@PathVariable(name="runId") Long runId, HttpServletResponse response) {
		try {
			byte[] file = businessServiceLocator.getExcelService().getAutoReleasePassedResultsExcel(runId);
			ByteArrayResource resource = new ByteArrayResource(file);
			
			HttpHeaders headers = new HttpHeaders(); 
			headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=auto-release-passed-results-"+LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()+VbsConstants.EXCEL_FILE_EXTENSION);
			
		    return ResponseEntity.ok()
		            .headers(headers)
		            .contentType(MediaType.parseMediaType(VbsConstants.EXCEL_MIME_TYPE))
		            .body(resource);
		}catch(VbsCheckedException vbs) {
			return new ResponseEntity<>("fail", new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}
	
	@PostMapping(value="/getAutoReleaseFailedResultsExcel/{runId}")
	public ResponseEntity<Object> getAutoReleaseFailedResultsExcel(@PathVariable(name="runId") Long runId, HttpServletResponse response) {
		try {
			byte[] file = businessServiceLocator.getExcelService().getAutoReleaseFailedResultsExcel(runId);
			ByteArrayResource resource = new ByteArrayResource(file);
			
			HttpHeaders headers = new HttpHeaders(); 
			headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=auto-release-failed-results-"+LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()+VbsConstants.EXCEL_FILE_EXTENSION);
			
		    return ResponseEntity.ok()
		            .headers(headers)
		            .contentType(MediaType.parseMediaType(VbsConstants.EXCEL_MIME_TYPE))
		            .body(resource);
		}catch(VbsCheckedException vbs) {
			return new ResponseEntity<>("fail", new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}
	
	@PostMapping(value="/getSearchedReleasesExcel")
	public ResponseEntity<Object> getSearchedReleasesExcel(@RequestBody ReleaseSearchParametersDTO releaseSearchParametersDTO, HttpServletResponse response) {
		try {
			byte[] file = businessServiceLocator.getExcelService().getSearchedReleasesExcel(releaseSearchParametersDTO);
			ByteArrayResource resource = new ByteArrayResource(file);
			
			HttpHeaders headers = new HttpHeaders(); 
			headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=pending-sent-releases-"+LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()+VbsConstants.EXCEL_FILE_EXTENSION);
			
		    return ResponseEntity.ok()
		            .headers(headers)
		            .contentType(MediaType.parseMediaType(VbsConstants.EXCEL_MIME_TYPE))
		            .body(resource);
		}catch(VbsCheckedException vbs) {
			return new ResponseEntity<>("fail", new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Secured({PermissionConstants.P_DELETE_SECURITY_CANCEL_RELEASE, PermissionConstants.P_CREATE_DEMAND_FINANCE})
	@PostMapping(value="/cancelRelease")
	public ReleaseDTO cancelRelease(@RequestBody ReleaseDTO releaseDTO) throws VbsCheckedException {
		Long releaseId = businessServiceLocator.getReleaseService().cancelRelease(releaseDTO, getUserDTO());
		return businessServiceLocator.getReleaseService().getReleaseDTO(releaseId);
	}
	
	@GetMapping(value="/getAutoReleaseForSecurityId")
	public List<AutoReleaseDTO> getAutoReleaseForSecurityId(@RequestParam("securityId") Long securityId) throws VbsCheckedException{
		return businessServiceLocator.getAutoReleaseService().getAutoReleaseForSecurityId(securityId);
	}

	@PostMapping(value="/getAutoReleaseForSecurityIdExcel")
	public ResponseEntity<Object> getAutoReleaseForSecurityIdExcel(@RequestParam("securityId") Long securityId, HttpServletResponse response) {
		try {
			byte[] file = businessServiceLocator.getExcelService().getAutoReleaseForSecurityIdExcel(securityId);
			ByteArrayResource resource = new ByteArrayResource(file);
			
			HttpHeaders headers = new HttpHeaders(); 
			headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=auto-releases-"+LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()+".xslx");
	
			return ResponseEntity.ok()
				.headers(headers)
				.contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
				.body(resource);
		}catch(VbsCheckedException vbs) {
			return new ResponseEntity<>("fail", new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}
	
	@PostMapping(value="/getReleasesExcel")
	public ResponseEntity<Object> getReleasesExcel(@RequestParam("securityId") Long securityId, HttpServletResponse response) {
		try {
			byte[] file = businessServiceLocator.getExcelService().getReleasesExcel(securityId, getUserDTO());
			ByteArrayResource resource = new ByteArrayResource(file);
			
			HttpHeaders headers = new HttpHeaders(); 
			headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=releases-summary-"+LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()+VbsConstants.EXCEL_FILE_EXTENSION);
			
		    return ResponseEntity.ok()
		            .headers(headers)
		            .contentType(MediaType.parseMediaType(VbsConstants.EXCEL_MIME_TYPE))
		            .body(resource);
		}catch(VbsCheckedException vbs) {
			return new ResponseEntity<>("fail", new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}
}
