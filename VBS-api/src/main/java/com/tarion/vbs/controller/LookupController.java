package com.tarion.vbs.controller;

import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.common.dto.security.SecurityDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/lookup")
public class LookupController extends Base {

    @GetMapping(value="/vbTypeahead")
    public List<ContactDTO> vbTypeahead(@RequestParam("partial") String partial){
        return businessServiceLocator.getContactService().getAllVbsForVbNumberLike(partial.toUpperCase().trim());
    }

    @GetMapping(value="/instrumentNumberTypeahead")
    public List<String> instrumentNumberTypeahead(@RequestParam("partial") String partial){
        return businessServiceLocator.getSecurityService().getAllInstrumentNumbersLike(partial);
    }

    @GetMapping(value="/allSecuritiesByInstrumentNumbersLike")
    public List<SecurityDTO> replaceByInstrumentNumberTypeahead(@RequestParam("partial") String partial){
        return businessServiceLocator.getSecurityService().getAllSecuritiesByInstrumentNumbersLike(partial);
    }

}
