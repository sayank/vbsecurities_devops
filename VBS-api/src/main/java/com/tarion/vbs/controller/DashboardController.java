package com.tarion.vbs.controller;

import java.util.List;

import com.tarion.vbs.common.dto.dashboard.NewBlanketCEDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tarion.vbs.common.dto.dashboard.ManagerReleaseDTO;
import com.tarion.vbs.web.service.BusinessServiceLocator;

@CrossOrigin
@RestController
@RequestMapping("/dashboard")
public class DashboardController {

	@Autowired
	private BusinessServiceLocator businessServiceLocator;
	
	@GetMapping("/managerReleases/{userId}")
	public List<ManagerReleaseDTO> getReleasesForManagerUserId(@PathVariable("userId") String userId){
		return this.businessServiceLocator.getDashboardService().getReleasesForManagerUserId(userId);
	}

	@GetMapping("/newBlanketCEs/{userId}")
	public List<NewBlanketCEDTO> getNewBlanketCEs(@PathVariable("userId") String userId){
		//swallowing userid for compatability.
		return this.businessServiceLocator.getDashboardService().getNewBlanketCEs();
	}

}
