package com.tarion.vbs.controller;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.dto.interest.InterestRateDTO;
import com.tarion.vbs.common.dto.interest.InterestSearchResultDTO;
import com.tarion.vbs.common.dto.interest.SecurityInterestSearchParamsDTO;
import com.tarion.vbs.common.dto.security.CashSecurityNoInterestDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@RestController
@RequestMapping("/api/interest")
public class InterestController extends Base{
	
	@GetMapping(value="/interestRates")
	public List<InterestRateDTO> getAllInterestRates(){
		return businessServiceLocator.getInterestService().getAllInterestRates();
	}
	
	@GetMapping(value="/getNextValidStartDate")
	public LocalDate getNextValidStartDate(){
		return businessServiceLocator.getInterestService().getNextValidStartDate();
	}
	
	@PostMapping(value="/searchInterestBySecurityId")
	public List<InterestSearchResultDTO> searchInterestBySecurityId(@RequestBody SecurityInterestSearchParamsDTO securityInterestSearchParams){
		return businessServiceLocator.getInterestService().searchInterest(securityInterestSearchParams);
	}

	@PostMapping(value="/saveInterestRate")
	public List<InterestRateDTO> saveInterestRate(@RequestBody InterestRateDTO interestRateDTO) throws VbsCheckedException {
		return businessServiceLocator.getInterestService().saveInterestRate(interestRateDTO, getUserDTO());
	}

	@PostMapping(value="/validateInterestRate")
	public ResponseEntity<List<ErrorDTO>> validateInterestRate(@RequestBody InterestRateDTO interestRateDTO){
		List<ErrorDTO> errors = businessServiceLocator.getInterestService().validateInterestRate(interestRateDTO);
		if(errors != null && !errors.isEmpty()){
			return new ResponseEntity<>(errors, new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
		return null;
	}

	@PostMapping(value="/deleteInterestRate")
	public ResponseEntity<T> deleteInterestRate(@RequestBody InterestRateDTO interestRateDTO){
		try{
			businessServiceLocator.getInterestService().deleteInterestRate(interestRateDTO, getUserDTO());
			return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.OK);
		}catch(VbsCheckedException e){
			return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@GetMapping(value="/startCalc")
	public void startInterestCalculation(@RequestParam(name="date", required = false) LocalDate date, @RequestParam(name="days", required = false) Integer days){
		if(days != null){
			businessServiceLocator.getInterestService().findDatesRequiringInterestCalculation(days);
		}else if(date != null){
			businessServiceLocator.getInterestService().performInterestCalcForDate(date);
		}else{
			businessServiceLocator.getInterestService().performInterestCalcForDate(LocalDate.parse("2019-05-01"));

		}
	}

	@GetMapping(value="/getCashSecuritiesWithoutInterest")
	public List<CashSecurityNoInterestDTO> getCashSecuritiesWithoutInterest(){
		return businessServiceLocator.getInterestService().getCashSecuritiesWithoutInterest();
	}

	@PostMapping(value="/searchResultsExcel")
	public ResponseEntity<Object> searchResultsExcel(@RequestBody SecurityInterestSearchParamsDTO securityInterestSearchParams, HttpServletResponse response) {
		try {
			byte[] file = businessServiceLocator.getExcelService().createInterestSearchResultsExcel(securityInterestSearchParams);
			ByteArrayResource resource = new ByteArrayResource(file);

			HttpHeaders headers = new HttpHeaders();
			headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=interest-search-results-"+LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()+".xslx");

			return ResponseEntity.ok()
					.headers(headers)
					.contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
					.body(resource);
		}catch(VbsCheckedException vbs) {
			return new ResponseEntity<>("fail", new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@GetMapping(value="/searchNoInterestResultsExcel")
	public ResponseEntity<Object> searchNoInterestResultsExcel(HttpServletResponse response) {
		try {
			byte[] file = businessServiceLocator.getExcelService().createNoInterestSearchResultsExcel();			
			ByteArrayResource resource = new ByteArrayResource(file);

			HttpHeaders headers = new HttpHeaders();
			headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=no-interest-search-results-"+LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()+VbsConstants.EXCEL_FILE_EXTENSION);

			return ResponseEntity.ok()
					.headers(headers)
					.contentType(MediaType.parseMediaType(VbsConstants.EXCEL_MIME_TYPE))
					.body(resource);
		}catch(VbsCheckedException vbs) {
			return new ResponseEntity<>("fail", new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}
}