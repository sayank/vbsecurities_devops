package com.tarion.vbs.controller;

import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.dto.financialinstitution.*;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/financialInstitution")
public class FinancialInstitutionController extends Base{

	@GetMapping(value="/getBranches")
	public List<BranchDTO> getbranches(@RequestParam("financialInstitutionId") Long financialInstitutionId){
		return businessServiceLocator.getFinancialInstitutionService().getBranchesForFinancialInstitution(financialInstitutionId);
	}

	@GetMapping(value="/getMaaPoas")
	public List<FinancialInstitutionMaaPoaDTO> getMaaPoas(@RequestParam("financialInstitutionId") Long financialInstitutionId){
		return businessServiceLocator.getFinancialInstitutionService().getFinancialInstitutionMaaPoas(financialInstitutionId);
	}

	@GetMapping(value="/getFinancialInstitutionList")
	public List<FinancialInstitutionDTO> getFinancialInstitutions(@RequestParam("financialInstitutionId") Long financialInstitutionId){
		return businessServiceLocator.getFinancialInstitutionService().getAllFinancialInstitutions();
	}

	@GetMapping(value="/financialInstitutionTypeahead")
	public List<FinancialInstitutionDTO> financialTypeahead(@RequestParam("partial") String partial){
		return businessServiceLocator.getFinancialInstitutionService().getFinancialInstitutionsForNameOrIdLike(partial, 20);
	}
	
	@GetMapping(value="/financialInstitutionSearch")
	public List<FinancialInstitutionDTO> financialSearch(@RequestParam("partial") String partial){
		return businessServiceLocator.getFinancialInstitutionService().getFinancialInstitutionsForNameOrIdLike(partial, 0);
	}
	
	@GetMapping(value="/financialSearch")
	public List<FinancialInstitutionDTO> financialSearch(@RequestParam(value="code", required=false) String code, @RequestParam("codeType") String codeType,
			@RequestParam(value="name", required=false) String name, @RequestParam(value="nameType") String nameType,
			@RequestParam(value="nameTo", required=false) String nameTo, @RequestParam(value="codeTo", required=false) Long codeTo, 
			@RequestParam(value="financialInstitutionTypeId", required=false) Long financialInstitutionTypeId){
		return businessServiceLocator.getFinancialInstitutionService().getFinancialInstitutionsByCodeOrName(code, codeTo, codeType, name, nameTo, nameType, financialInstitutionTypeId);
	}

	@GetMapping(value="/getFinancialInstitutionAndBranch")
	public FinancialInstitutionBranchCompositeDTO getFinancialInstitutionAndBranch(@RequestParam("id") Long id){
		return businessServiceLocator.getFinancialInstitutionService().getFullFinancialInstitutionForSecurity(id);
	}

	@PostMapping(value="/validateFinancialInstitution")
	public ResponseEntity<List<ErrorDTO>> validateFinancialInstitution(@RequestBody FinancialInstitutionDTO dto){
			List<ErrorDTO> errors = businessServiceLocator.getFinancialInstitutionService().validateFinancialInstitution(dto);
			if(errors != null && !errors.isEmpty()){
				return new ResponseEntity<>(errors, new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
			}
			return null;
	}

    @GetMapping(value="/getFinancialInstitution")
	public FinancialInstitutionDTO getFinancialInstitution(@RequestParam("financialInstitutionId") Long financialInstitutionId){
		return businessServiceLocator.getFinancialInstitutionService().getFinancialInstitution(financialInstitutionId);
	}

	@PostMapping(value="/saveFinancialInstitution")
	public FinancialInstitutionDTO saveFinancialInstitution(@RequestBody FinancialInstitutionDTO financialInstitutionDTO) throws VbsCheckedException {
		return businessServiceLocator.getFinancialInstitutionService().saveFinancialInstitution(financialInstitutionDTO, getUserDTO());
	}

	@GetMapping(value="/getContactsForFinancialInstitution")
	public List<ContactFinancialInstitutionDTO> getContactsForFinancialInstitution(@RequestParam("financialInstitutionId") Long financialInstitutionId){
		return businessServiceLocator.getFinancialInstitutionService().getContactsForFinancialInstitution(financialInstitutionId);
	}

	@PostMapping(value="/saveFinancialInstitutionContacts")
	public void saveFinancialInstitutionContacts(@RequestBody List<ContactFinancialInstitutionDTO> financialInstitutionContactsDTO){
		businessServiceLocator.getFinancialInstitutionService().saveFinancialInstitutionContacts(financialInstitutionContactsDTO, getUserDTO());
	}

	@GetMapping(value="/getActivities")
	public List<FIActivityDTO> getActivities(@RequestParam("financialInstitutionId") Long financialInstitutionId){
		return businessServiceLocator.getFinancialInstitutionActivitiesService().getActivitiesForFinancialInstitution(financialInstitutionId);
	}

}
