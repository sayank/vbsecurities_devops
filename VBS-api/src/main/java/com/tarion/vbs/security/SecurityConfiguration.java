package com.tarion.vbs.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.allanditzel.springframework.security.web.csrf.CsrfTokenResponseHeaderBindingFilter;

@Configuration
@EnableWebSecurity
@Order(1)
public class SecurityConfiguration  extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
	@Autowired
	private RestAuthenticationFailureHandler restAuthenticationFailureHandler;
	@Autowired
	private RestAuthenticationSuccessHandler restAuthenticationSuccessHandler;
	@Autowired
	private RestLogoutSuccessHandler restLogoutSuccessHandler;
	@Autowired
	private VbsAuthenticationProvider vbsAuthenticationProvider;
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth){
		auth.eraseCredentials(true);
		auth.authenticationProvider(vbsAuthenticationProvider);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {	
		http.authorizeRequests().antMatchers("/*").permitAll().and()
			.authorizeRequests().antMatchers("/api/uiSetup").permitAll().and()
			.authorizeRequests().antMatchers("/api/**").authenticated().and()
		    .formLogin().loginProcessingUrl("/login").and()
			.formLogin().failureHandler(restAuthenticationFailureHandler).and()
			.formLogin().successHandler(restAuthenticationSuccessHandler).and()
			.exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint).and()
			.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessHandler(restLogoutSuccessHandler).and()
			.csrf().disable();
		
		http.addFilterAfter(new CsrfTokenResponseHeaderBindingFilter(), CsrfFilter.class);
    }	
}