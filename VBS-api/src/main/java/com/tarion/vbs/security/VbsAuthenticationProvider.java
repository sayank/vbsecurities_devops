package com.tarion.vbs.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.tarion.vbs.web.service.AuthService;

@Service("vbsAuthenticationProvider")
public class VbsAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
	
	@Autowired
	private AuthService authService;
	
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication){
		//must implement.
	}

	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication){
		return authService.authenticate(username, authentication.getCredentials().toString());
	}

}
