package com.tarion.vbs.security;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tarion.vbs.controller.Base;

@Component
public class RestAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	@Autowired
	private Base base;
	
	@Override
    public void onAuthenticationSuccess(HttpServletRequest request, 
      HttpServletResponse response, Authentication authentication) throws IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		
		String json = mapper.writeValueAsString(base.getUserDTO());
		
		response.getWriter().print(json);
	    response.getWriter().flush();
	}

}