package com.tarion.vbs.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

@Component
public class RestAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
	                                    org.springframework.security.core.AuthenticationException exception) throws IOException, ServletException {
		if(exception instanceof BadCredentialsException) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Bad Credentials");		
		}else if(exception instanceof InternalAuthenticationServiceException) {
			response.sendError(HttpServletResponse.SC_BAD_GATEWAY, "Internal System Error");
		}else {
			super.onAuthenticationFailure(request, response, exception);
		}
	}	
	
}