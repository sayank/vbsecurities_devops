package com.tarion.vbs.web.service;

import com.tarion.vbs.service.contact.ContactServiceRemote;
import com.tarion.vbs.service.correspondence.CorrespondenceServiceRemote;
import com.tarion.vbs.service.dashboard.DashboardServiceRemote;
import com.tarion.vbs.service.enrolment.EnrolmentServiceRemote;
import com.tarion.vbs.service.financialinstitution.FinancialInstitutionActivitiesServiceRemote;
import com.tarion.vbs.service.financialinstitution.FinancialInstitutionServiceRemote;
import com.tarion.vbs.service.interest.InterestServiceRemote;
import com.tarion.vbs.service.release.AutoReleaseServiceRemote;
import com.tarion.vbs.service.release.ReleaseServiceRemote;
import com.tarion.vbs.service.security.SecurityServiceRemote;
import com.tarion.vbs.service.userprofile.AdSecurityServiceRemote;
import com.tarion.vbs.service.userprofile.UserServiceRemote;
import com.tarion.vbs.service.util.ExcelServiceRemote;
import com.tarion.vbs.service.util.LookupServiceRemote;
import com.tarion.vbs.web.util.JNDIServicesLocator;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;

@Service
public class BusinessServiceLocatorImpl extends JNDIServicesLocator implements BusinessServiceLocator {

	public BusinessServiceLocatorImpl() throws NamingException {
		super();
	}

	@Override
	public AdSecurityServiceRemote getAdSecurityService() {
		return getBean(AdSecurityServiceRemote.class);
	}

	@Override
	public LookupServiceRemote getLookupService() {
		return getBean(LookupServiceRemote.class);
	}
	
	@Override
	public SecurityServiceRemote getSecurityService() {
		return getBean(SecurityServiceRemote.class);
	}
	
	@Override
	public FinancialInstitutionServiceRemote getFinancialInstitutionService() { return getBean(FinancialInstitutionServiceRemote.class); }

	@Override
	public FinancialInstitutionActivitiesServiceRemote getFinancialInstitutionActivitiesService() { return getBean(FinancialInstitutionActivitiesServiceRemote.class); }

	@Override
	public EnrolmentServiceRemote getEnrolmentService() {
		return getBean(EnrolmentServiceRemote.class);
	}

	@Override
	public ReleaseServiceRemote getReleaseService() {
		return getBean(ReleaseServiceRemote.class);
	}
	
	@Override
	public ExcelServiceRemote getExcelService() {
		return getBean(ExcelServiceRemote.class);
	}

	@Override
	public InterestServiceRemote getInterestService() {
		return getBean(InterestServiceRemote.class);
	}

	@Override
	public DashboardServiceRemote getDashboardService() {
		return getBean(DashboardServiceRemote.class);
	}	

	@Override
	public UserServiceRemote getUserService() {
		return getBean(UserServiceRemote.class);
	}
	
	@Override
	public ContactServiceRemote getContactService() {
		return getBean(ContactServiceRemote.class);
	}

	@Override
	public CorrespondenceServiceRemote getCorrespondenceService() {
		return getBean(CorrespondenceServiceRemote.class);
	}
	
	@Override
	public AutoReleaseServiceRemote getAutoReleaseService() {
		return getBean(AutoReleaseServiceRemote.class);
	}

}