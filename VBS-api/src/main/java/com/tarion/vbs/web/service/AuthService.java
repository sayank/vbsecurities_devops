package com.tarion.vbs.web.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.tarion.vbs.common.exceptions.IncorrectCredentialsException;
import com.tarion.vbs.common.exceptions.VbsDirectoryEnvironmentException;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.session.SessionStore;

@Service
public class AuthService {

	@Autowired
	private BusinessServiceLocator businessServiceLocator;
	@Autowired
	private SessionStore sessionStore;
	
	public UserDetails authenticate(String username, String password) {
		try {
			UserDTO dto = businessServiceLocator.getAdSecurityService().authenticate(username, password);
			sessionStore.setUserDTO(dto);
			List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
			dto.getPermissions().stream().forEach(p -> grantedAuthorities.add(new SimpleGrantedAuthority(p.getPermissionName())));
			return new User(username, password, grantedAuthorities);
		}catch(IncorrectCredentialsException ice) {
			throw new BadCredentialsException("Bad Credentials");
		}catch(VbsDirectoryEnvironmentException dee) {
			throw new InternalAuthenticationServiceException("Service fail");
		}catch(Exception e) {
			throw new AuthenticationServiceException("Unknown Error");
		}
	}
	
}