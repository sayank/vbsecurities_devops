package com.tarion.vbs.web.service;

import com.tarion.vbs.service.contact.ContactServiceRemote;
import com.tarion.vbs.service.correspondence.CorrespondenceServiceRemote;
import com.tarion.vbs.service.dashboard.DashboardServiceRemote;
import com.tarion.vbs.service.enrolment.EnrolmentServiceRemote;
import com.tarion.vbs.service.financialinstitution.FinancialInstitutionActivitiesServiceRemote;
import com.tarion.vbs.service.financialinstitution.FinancialInstitutionServiceRemote;
import com.tarion.vbs.service.interest.InterestServiceRemote;
import com.tarion.vbs.service.release.AutoReleaseServiceRemote;
import com.tarion.vbs.service.release.ReleaseServiceRemote;
import com.tarion.vbs.service.security.SecurityServiceRemote;
import com.tarion.vbs.service.userprofile.AdSecurityServiceRemote;
import com.tarion.vbs.service.userprofile.UserServiceRemote;
import com.tarion.vbs.service.util.ExcelServiceRemote;
import com.tarion.vbs.service.util.LookupServiceRemote;

public interface BusinessServiceLocator {

	AdSecurityServiceRemote getAdSecurityService();

	LookupServiceRemote getLookupService();

	SecurityServiceRemote getSecurityService();

	FinancialInstitutionServiceRemote getFinancialInstitutionService();

	FinancialInstitutionActivitiesServiceRemote getFinancialInstitutionActivitiesService();

	EnrolmentServiceRemote getEnrolmentService();
	
	ReleaseServiceRemote getReleaseService();
	
	ExcelServiceRemote getExcelService();
	
	InterestServiceRemote getInterestService();
	UserServiceRemote getUserService();
	DashboardServiceRemote getDashboardService();
	ContactServiceRemote getContactService();

	CorrespondenceServiceRemote getCorrespondenceService();
	AutoReleaseServiceRemote getAutoReleaseService();

}