package com.tarion.vbs.web.util;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.springframework.stereotype.Service;

import com.tarion.vbs.common.exceptions.VbsRuntimeException;

@Service
public class JNDIServicesLocator {
	
	/**
	 * Generic method that looks up the remote EJB based on the package and class name of given EJB.
	 * 
	 * 
	 */
	@SuppressWarnings("unchecked")
	public <T> T getBean(Class<T> clazz) {
		String simpleName = clazz.getSimpleName();
		String packageName = clazz.getPackage().getName();
		String jndiName = "ejb." + simpleName.replace("Remote", "") + "#" + packageName + "." + simpleName;
		Object ret = getJndiService(jndiName);
		return (T) ret;
	}
	
	/**
	 * Returns remote reference for the EJB with given jndiName
	 * @throws RuntimeException - to be caught and logged at top level error handller
	 */
	private Object getJndiService(String jndiName) {
		try {
			Object businessService = getInitialContext().lookup(jndiName);
			if (businessService == null) {
				throw new VbsRuntimeException("The JNDI lookup failed for '" + jndiName + "'");
			}
			return businessService;
		} catch (Exception e) {
			throw new VbsRuntimeException(e);
		}
	}
	
	/**
	 * Get the Initial Context
	 * 
	 * @return The Intitial Context
	 */
	private synchronized InitialContext getInitialContext() {
		try {
			return new InitialContext();
		}catch(NamingException ne){
			throw new VbsRuntimeException(ne);
	    }
	}
	
}