package com.tarion.vbs.web.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.RequestContextFilter;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.MultipartFilter;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebMVCConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("forward:/index.html");
		registry.addViewController("/{path:[^\\.]*}").setViewName("/");
	}
	
	@Bean
	@Order(1)
	public RequestContextFilter requestContextFilter(){
		return new RequestContextFilter();
	}
	
	@Bean
	public CommonsMultipartResolver filterMultipartResolver() {
	    return new CommonsMultipartResolver();
	}

	@Bean
	@Order(0)
	public MultipartFilter multipartFilter() {
	    return new MultipartFilter();
	}
}