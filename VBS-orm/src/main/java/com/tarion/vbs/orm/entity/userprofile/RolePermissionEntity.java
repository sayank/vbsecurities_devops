package com.tarion.vbs.orm.entity.userprofile;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.envers.Audited;

import com.tarion.vbs.orm.entity.AuditedEntity;

@NamedQuery(name = RolePermissionEntity.SELECT_ROLE_PERMISSION, query = RolePermissionEntity.JPQL_ROLE_PERMISSION)

@Entity
@Table(name="ROLE_PERMISSION")
@Audited(withModifiedFlag=true)
public class RolePermissionEntity implements AuditedEntity {
	private static final long serialVersionUID = 1L;
	
	public static final String SELECT_ROLE_PERMISSION = "RolePermissionEntity.SELECT_ROLE_PERMISSION";
	protected static final String JPQL_ROLE_PERMISSION = "SELECT o FROM RolePermissionEntity o WHERE o.roleId = ?1 AND o.permissionId = ?2";
	
	@Id
	@Column(name = "ROLE_ID")
	private Long roleId;
	@Id
	@Column(name = "PERMISSION_ID")
	private Long permissionId;
	@Column(name = "VERSION")
	@Version
	private Long version;
	@Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;
	
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getPermissionId() {
		return permissionId;
	}
	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
}