package com.tarion.vbs.orm.entity.enrolment;

import com.tarion.vbs.common.enums.YesNoEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@NamedQueries({
    @NamedQuery(name = CEEnrolmentDecisionEntity.SELECT_OUTSTANDING_NEW_CES, query = CEEnrolmentDecisionEntity.JPQL_OUTSTANDING_NEW_CES),
    @NamedQuery(name = CEEnrolmentDecisionEntity.SELECT_OUTSTANDING_DECISION_BY_ENROLMENT_NUMBER, query = CEEnrolmentDecisionEntity.JPQL_OUTSTANDING_DECISION_BY_ENROLMENT_NUMBER)
})

@Entity
@Table(name="CE_ENROLMENT_DECISION")
public class CEEnrolmentDecisionEntity implements Serializable {

    public static final String SELECT_OUTSTANDING_NEW_CES = "CEEnrolmentDecisionEntity.SELECT_OUTSTANDING_NEW_CES";
    static final String JPQL_OUTSTANDING_NEW_CES = "SELECT d FROM CEEnrolmentDecisionEntity d WHERE d.approvalDecision IS NULL ORDER BY enrolment.enrolmentNumber ASC";

    public static final String SELECT_OUTSTANDING_DECISION_BY_ENROLMENT_NUMBER = "CEEnrolmentDecisionEntity.SELECT_OUTSTANDING_DECISION_BY_ENROLMENT_NUMBER";
    static final String JPQL_OUTSTANDING_DECISION_BY_ENROLMENT_NUMBER= "SELECT d FROM CEEnrolmentDecisionEntity d WHERE d.approvalDecision IS NULL AND d.enrolment.enrolmentNumber = ?1";

    private static final long serialVersionUID = 386808114743493348L;

    @Id
    @JoinColumn(name="ENROLMENT_NUMBER")
    @OneToOne
    private EnrolmentEntity enrolment;
    @Enumerated(EnumType.STRING)
    @Column(name="APPROVAL_DECISION")
    private YesNoEnum approvalDecision;
    @Column(name="APPROVAL_ANALYST")
    private String approvalAnalyst;
    @Column(name="APPROVAL_DATE")
    private LocalDateTime approvalDate;
    @Column(name="APPROVAL_REASON")
    private String approvalReason;

    public CEEnrolmentDecisionEntity(EnrolmentEntity enrolment){
        this.enrolment = enrolment;
    }
    public CEEnrolmentDecisionEntity(){}

    public EnrolmentEntity getEnrolment() {
        return enrolment;
    }

    public void setEnrolment(EnrolmentEntity enrolment) {
        this.enrolment = enrolment;
    }

    public YesNoEnum getApprovalDecision() {
        return approvalDecision;
    }

    public void setApprovalDecision(YesNoEnum approvalDecision) {
        this.approvalDecision = approvalDecision;
    }

    public String getApprovalAnalyst() {
        return approvalAnalyst;
    }

    public void setApprovalAnalyst(String approvalAnalyst) {
        this.approvalAnalyst = approvalAnalyst;
    }

    public LocalDateTime getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(LocalDateTime approvalDate) {
        this.approvalDate = approvalDate;
    }

    public String getApprovalReason() {
        return approvalReason;
    }

    public void setApprovalReason(String approvalReason) {
        this.approvalReason = approvalReason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CEEnrolmentDecisionEntity that = (CEEnrolmentDecisionEntity) o;
        return enrolment.equals(that.enrolment) &&
                approvalDecision == that.approvalDecision &&
                Objects.equals(approvalAnalyst, that.approvalAnalyst) &&
                Objects.equals(approvalDate, that.approvalDate) &&
                Objects.equals(approvalReason, that.approvalReason);
    }

    @Override
    public int hashCode() {
        return Objects.hash(enrolment, approvalDecision, approvalAnalyst, approvalDate, approvalReason);
    }

    @Override
    public String toString() {
        return "CEEnrolmentDecisionEntity{" +
                "enrolment=" + enrolment +
                ", approvalDecision=" + approvalDecision +
                ", approvalAnalyst='" + approvalAnalyst + '\'' +
                ", approvalDate=" + approvalDate +
                ", approvalReason='" + approvalReason + '\'' +
                '}';
    }
}
