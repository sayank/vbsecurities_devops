package com.tarion.vbs.orm.entity.interest;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQuery(name= InterestEntity.SELECT_INTEREST_ENTITY_BY_CALCULATION_ID, query = InterestEntity.JPQL_INTEREST_ENTITY_BY_CALCULATION_ID)
@NamedQuery(name= InterestEntity.SELECT_INTEREST_ENTITY_BY_SECURITY_ID, query = InterestEntity.JPQL_INTEREST_ENTITY_BY_SECURITY_ID)

@Entity
@Table(name = "INTEREST")
public class InterestEntity implements Serializable {

    public static final String SELECT_INTEREST_ENTITY_BY_CALCULATION_ID = "InterestEntity.SELECT_INTEREST_ENTITY_BY_CALCULATION_ID";
    static final String JPQL_INTEREST_ENTITY_BY_CALCULATION_ID = "SELECT o FROM InterestEntity o WHERE o.interestCalculation.id = ?1";

    public static final String SELECT_INTEREST_ENTITY_BY_SECURITY_ID = "InterestEntity.SELECT_INTEREST_ENTITY_BY_SECURITY_ID";
    static final String JPQL_INTEREST_ENTITY_BY_SECURITY_ID = "SELECT o FROM InterestEntity o WHERE o.securityId = ?1 ORDER BY o.interestCalculation.interestDate";

    public static final String SELECT_DAILY_INTERESTS_FOR_SECURITY_BETWEEN_DATES = "SELECT I.INTEREST_AMOUNT as interestAmount, IC.INTEREST_DATE as interestDate, " +
            "0 as discountAmount " +
            "FROM INTEREST I JOIN INTEREST_CALCULATION IC ON IC.ID = I.INTEREST_CALCULATION_ID " +
            "WHERE I.SECURITY_ID = ?1 AND IC.INTEREST_DATE BETWEEN ?2 AND ?3 " +
            "ORDER BY IC.INTEREST_DATE DESC";

    private static final long serialVersionUID = -7991812148108058011L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;

    @Column(name="SECURITY_ID")
    private Long securityId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="INTEREST_CALCULATION_ID")
    private InterestCalculationEntity interestCalculation;
    @Column(name="INTEREST_AMOUNT")
    private BigDecimal interestAmount;

    public InterestEntity(Long securityId, InterestCalculationEntity interestCalculation){
        this.securityId = securityId;
        this.interestCalculation = interestCalculation;
    }

    public InterestEntity() {
    }

    public Long getSecurityId() {
        return securityId;
    }

    public void setSecurityId(Long securityId) {
        this.securityId = securityId;
    }

    public InterestCalculationEntity getInterestCalculation() {
        return interestCalculation;
    }

    public void setInterestCalculation(InterestCalculationEntity interestCalculation) {
        this.interestCalculation = interestCalculation;
    }

    public BigDecimal getInterestAmount() {
        return interestAmount;
    }

    public void setInterestAmount(BigDecimal interestAmount) {
        this.interestAmount = interestAmount;
    }
}