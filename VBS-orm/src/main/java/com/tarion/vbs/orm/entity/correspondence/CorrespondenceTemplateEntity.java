package com.tarion.vbs.orm.entity.correspondence;

import com.tarion.vbs.orm.entity.security.SecurityTypeEntity;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Correspondence Template Entity, for mapping templates to tokens
 *
 * @author <A href="joni.paananen@tarion.com">Joni Paananen</A>
 * @since 2019-02-19
 * @version 1.0
 */

@NamedQuery(name=CorrespondenceTemplateEntity.SELECT_ALL_TEMPLATES, query=CorrespondenceTemplateEntity.JPQL_ALL_TEMPLATES)
@NamedQuery(name=CorrespondenceTemplateEntity.SELECT_TEMPLATE_BY_NAME, query=CorrespondenceTemplateEntity.JPQL_TEMPLATE_BY_NAME)

@Entity
@Table(name = "CORRESPONDENCE_TEMPLATE")
public class CorrespondenceTemplateEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String SELECT_ALL_TEMPLATES = "CorrespondenceTemplateEntity.SELECT_ALL_TEMPLATES";
	static final String JPQL_ALL_TEMPLATES = "SELECT t FROM CorrespondenceTemplateEntity t ORDER BY t.name ";

	public static final String SELECT_TEMPLATE_BY_NAME = "CorrespondenceTemplateEntity.SELECT_TEMPLATE_BY_NAME";
	static final String JPQL_TEMPLATE_BY_NAME = "SELECT t FROM CorrespondenceTemplateEntity t WHERE t.name = ?1";


	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
    @Column(name = "NAME")
    private String name;

    @ManyToOne
	@JoinColumn(name = "SECURITY_TYPE_ID")
	private SecurityTypeEntity securityType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SecurityTypeEntity getSecurityType() {
		return securityType;
	}

	public void setSecurityType(SecurityTypeEntity securityType) {
		this.securityType = securityType;
	}

	@Override
	public String toString() {
		return "CorrespondenceTemplateEntity [id=" + id + ", name=" + name + "]";
	}
}
