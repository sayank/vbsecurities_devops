/* 
 * 
 * FinancialInstitutionMaaPoaEntity.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.financialinstitution;

import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.envers.Audited;

import com.tarion.vbs.orm.entity.AuditedEntity;

/**
 * Financial institution MAAPOA Entity JPA class holds information about financial institutions MAA or POA
 *
 * @author <A href="sbogicevic@tarion.com">Srdjan Bogicevic</A>
 * @since 2019-05-16
 * @version 1.0
 */
@NamedQuery(name=FinancialInstitutionMaaPoaEntity.SELECT_ALL_MAAPOA_FOR_FINANCIAL_INSTITUTION_ID, query = FinancialInstitutionMaaPoaEntity.JPQL_ALL_MAAPOA_FOR_FINANCIAL_INSTITUTION_ID)

@Entity
@Table(name = "FINANCIAL_INSTITUTION_MAAPOA")
@Audited(withModifiedFlag=true)
public class FinancialInstitutionMaaPoaEntity implements AuditedEntity {
	private static final long serialVersionUID = 1L;
	
	public static final String SELECT_ALL_MAAPOA_FOR_FINANCIAL_INSTITUTION_ID = "FinancialInstitutionMaaPoaEntity.SELECT_ALL_MAAPOA_FOR_FINANCIAL_INSTITUTION_ID";
	protected static final String JPQL_ALL_MAAPOA_FOR_FINANCIAL_INSTITUTION_ID = "SELECT o FROM FinancialInstitutionMaaPoaEntity o WHERE o.financialInstitution.id = ?1 ORDER BY o.id ";

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FINANCIAL_INSTITUTION_ID")
	private FinancialInstitutionEntity financialInstitution;
   
	@Column(name = "MAA_POA_SEQUENCE")
	private Long maaPoaSequence;
	
	@Column(name = "EXPIRATION_DATE")
	private LocalDateTime expirationDate;

    @Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
	
	public FinancialInstitutionEntity getFinancialInstitution() {
		return financialInstitution;
	}

	public void setFinancialInstitution(FinancialInstitutionEntity financialInstitution) {
		this.financialInstitution = financialInstitution;
	}

	public LocalDateTime getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDateTime expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Long getMaaPoaSequence() {
		return maaPoaSequence;
	}

	public void setMaaPoaSequence(Long maaPoaSequence) {
		this.maaPoaSequence = maaPoaSequence;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinancialInstitutionMaaPoaEntity other = (FinancialInstitutionMaaPoaEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "FinancialInstitutionMaaPoaEntity [id=" + id + ", version=" + version +
				", expirationDate=" + expirationDate +
				"]";
	}

}
