package com.tarion.vbs.orm.entity.userprofile;


import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.envers.Audited;

import com.tarion.vbs.orm.entity.AuditedEntity;

@NamedQuery(name=UserRoleEntity.SELECT_USERS_WITH_ROLE, query=UserRoleEntity.JQPL_USERS_WITH_ROLE)
@NamedQuery(name=UserRoleEntity.SELECT_USERS_WITH_ROLE_NAME, query=UserRoleEntity.JQPL_USERS_WITH_ROLE_NAME)
@NamedQuery(name=UserRoleEntity.SELECT_USERS_WITH_ANY_ROLE, query=UserRoleEntity.JQPL_USERS_WITH_ANY_ROLE)

@Entity
@Table(name="USER_ROLE")
@Audited(withModifiedFlag=true)
public class UserRoleEntity implements AuditedEntity {
	private static final long serialVersionUID = 1L;
	
	public static final String SELECT_USERS_WITH_ROLE = "UserRoleEntity.SELECT_USERS_WITH_ROLE";
	public static final String JQPL_USERS_WITH_ROLE = "SELECT o.userId FROM UserRoleEntity o WHERE o.roleId = ?1";
	
	public static final String SELECT_USERS_WITH_ROLE_NAME = "UserRoleEntity.SELECT_USERS_WITH_ROLE_NAME";
	public static final String JQPL_USERS_WITH_ROLE_NAME = "SELECT o.userId FROM UserRoleEntity o "
			+ "JOIN RoleEntity r ON o.roleId = r.id "
			+ "WHERE r.name = ?1";
	
	public static final String SELECT_USERS_WITH_ANY_ROLE = "UserRoleEntity.SELECT_USERS_WITH_ANY_ROLE";
	public static final String JQPL_USERS_WITH_ANY_ROLE = "SELECT DISTINCT o.userId FROM UserRoleEntity o";
	
	
	@Id
	@Column(name = "ROLE_ID")
	private Long roleId;
	@Id
	@Column(name = "USER_ID")
	private String userId;
	@Column(name = "VERSION")
	@Version
	private Long version;
	@Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;
	
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
}