/* 
 * 
 * ContactFinancialInstitutionEntity.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.financialinstitution;

import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.envers.Audited;

import com.tarion.vbs.orm.entity.AuditedEntity;
import com.tarion.vbs.orm.entity.contact.ContactEntity;

/**
 * Financial institution contacts Entity JPA class holds relationship information between financial institutions and contacts
 *
 * @author <A href="sbogicevic@tarion.com">Srdjan Bogicevic</A>
 * @since 2019-05-16
 * @version 1.0
 */
@NamedQuery(name=ContactFinancialInstitutionEntity.SELECT_ALL_CONTACTS_FOR_FINANCIAL_INSTITUTION_ID, query = ContactFinancialInstitutionEntity.JPQL_ALL_CONTACTS_FOR_FINANCIAL_INSTITUTION_ID)

@Entity
@Table(name = "CONTACT_FINANCIAL_INSTITUTION")
@Audited(withModifiedFlag=true)
public class ContactFinancialInstitutionEntity implements AuditedEntity {
	private static final long serialVersionUID = 1L;
	
	public static final String SELECT_ALL_CONTACTS_FOR_FINANCIAL_INSTITUTION_ID = "ContactFinancialInstitutionEntity.SELECT_ALL_CONTACTS_FOR_FINANCIAL_INSTITUTION_ID";
	protected static final String JPQL_ALL_CONTACTS_FOR_FINANCIAL_INSTITUTION_ID = "SELECT o FROM ContactFinancialInstitutionEntity o WHERE o.financialInstitution.id = ?1 ORDER BY o.id ";

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FINANCIAL_INSTITUTION_ID")
	private FinancialInstitutionEntity financialInstitution;
   
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTACT_ID")
	private ContactEntity contact;

    @Column(name = "START_DATE")
	private LocalDateTime startDate;
    
    @Column(name = "END_DATE")
	private LocalDateTime endDate;

    @Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
	
	public FinancialInstitutionEntity getFinancialInstitution() {
		return financialInstitution;
	}

	public void setFinancialInstitution(FinancialInstitutionEntity financialInstitution) {
		this.financialInstitution = financialInstitution;
	}

	public ContactEntity getContact() {
		return contact;
	}

	public void setContact(ContactEntity contact) {
		this.contact = contact;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContactFinancialInstitutionEntity other = (ContactFinancialInstitutionEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ContactFinancialInstitutionEntity [id=" + id + ", version=" + version +
				", startDate=" + startDate +
				", endDate=" + endDate +
				"]";
	}

}
