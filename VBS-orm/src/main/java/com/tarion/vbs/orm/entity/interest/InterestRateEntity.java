/* 
 * 
 * InterestRateEntity.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.interest;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.envers.Audited;

import com.tarion.vbs.orm.entity.AuditedEntity;

/**
 * Interest Rate Entity JPA class holds information about Interest rates
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-12-10
 * @version 1.0
 */

@NamedQuery(name=InterestRateEntity.SELECT_ALL_INTEREST_RATES, query = InterestRateEntity.JPQL_ALL_INTEREST_RATES)
@NamedQuery(name=InterestRateEntity.SELECT_INTEREST_RATES_FOR_YEAR, query = InterestRateEntity.JPQL_INTEREST_RATES_FOR_YEAR)
@NamedQuery(name=InterestRateEntity.SELECT_INTEREST_RATE_FOR_DATE, query = InterestRateEntity.JPQL_INTEREST_RATE_FOR_DATE)

@Entity
@Table(name = "INTEREST_RATE")
@Audited(withModifiedFlag=true)
public class InterestRateEntity implements AuditedEntity {
	private static final long serialVersionUID = 1L;

	public static final String SELECT_ALL_INTEREST_RATES = "InterestRateEntity.SELECT_ALL_INTEREST_RATES";	
	protected static final String JPQL_ALL_INTEREST_RATES = "SELECT o FROM InterestRateEntity o ORDER BY o.interestStartDate DESC ";

	// This query should return 1 or for 1st of the month 2 rates the most
	// so it has to be order descending
	public static final String SELECT_INTEREST_RATE_FOR_DATE = "InterestRateEntity.SELECT_INTEREST_RATE_FOR_DATE";	
	protected static final String JPQL_INTEREST_RATE_FOR_DATE = "SELECT o FROM InterestRateEntity o WHERE o.interestStartDate = (SELECT MAX(r2.interestStartDate) FROM InterestRateEntity r2 WHERE r2.interestStartDate <= ?1) ";
	
	public static final String SELECT_INTEREST_RATES_FOR_YEAR = "InterestRateEntity.SELECT_INTEREST_RATES_FOR_YEAR";	
	protected static final String JPQL_INTEREST_RATES_FOR_YEAR = "SELECT o FROM InterestRateEntity o WHERE o.interestStartDate >= ?1 AND o.interestStartDate <= ?2 ORDER BY o.interestStartDate DESC ";
	
	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;
   
	@Column(name = "INTEREST_START_DATE")
    private LocalDateTime interestStartDate;

    @Column(name = "ANNUAL_RATE")
    private BigDecimal annualRate;
    
    @Column(name = "CREATE_DATE")
    private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public LocalDateTime getInterestStartDate() {
		return interestStartDate;
	}
	public void setInterestStartDate(LocalDateTime interestStartDate) {
		this.interestStartDate = interestStartDate;
	}
	public BigDecimal getAnnualRate() {
		return annualRate;
	}
	public void setAnnualRate(BigDecimal annualRate) {
		this.annualRate = annualRate;
	}
	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InterestRateEntity other = (InterestRateEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "InterestRateEntity [id=" + id +
				", version=" + version +
				", interestStartDate=" + interestStartDate + 
				", annualRate=" + annualRate + "]";
	}	

}
