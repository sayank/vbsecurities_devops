/* 
 * 
 * PoolEntity.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.security;

import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.envers.Audited;

import com.tarion.vbs.orm.entity.AuditedEntity;

/**
 * Pool Entity JPA class holds information about Securities Pool
 * Every security belong to the pool even if it is only one security
 * Pool can be even without any security
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-15
 * @version 1.0
 */

@Entity
@Table(name = "POOL")
@Audited(withModifiedFlag=true)
public class PoolEntity implements AuditedEntity {
	private static final long serialVersionUID = 1L;



	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;
    
    @Column(name = "UNITS_TO_COVER")
    private int unitsToCover;
    @Column(name = "FH_UNITS_TO_COVER")
    private Integer fhUnitsToCover;
    @Column(name = "CE_UNITS_TO_COVER")
    private Integer ceUnitsToCover;

    @Column(name = "CREATE_DATE")
    private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	public PoolEntity(){}
	public PoolEntity(int unitsToCover){
		this.unitsToCover = unitsToCover;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public int getUnitsToCover() {
		return unitsToCover;
	}
	public void setUnitsToCover(int unitsToCover) {
		this.unitsToCover = unitsToCover;
	}
	public Integer getFHUnitsToCover() {
		return fhUnitsToCover;
	}
	public void setFHUnitsToCover(Integer fhUnitsToCover) {
		this.fhUnitsToCover = fhUnitsToCover;
	}
	public Integer getCEUnitsToCover() {
		return ceUnitsToCover;
	}
	public void setCEUnitsToCover(Integer ceUnitsToCover) {
		this.ceUnitsToCover = ceUnitsToCover;
	}
	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PoolEntity other = (PoolEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "PoolEntity [id=" + id + ", version=" + version + ", unitsToCover=" + unitsToCover + ", fhUnitsToCover=" + fhUnitsToCover + ", ceUnitsToCover=" + ceUnitsToCover + "]";
	}	

}
