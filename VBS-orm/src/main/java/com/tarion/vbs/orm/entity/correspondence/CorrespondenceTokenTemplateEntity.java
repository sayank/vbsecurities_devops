package com.tarion.vbs.orm.entity.correspondence;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="CORRESPONDENCE_TOKEN_TEMPLATE")
public class CorrespondenceTokenTemplateEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "TEMPLATE_ID")
	private Long templateId;
	@Id
	@Column(name = "TOKEN_ID")
	private Long tokenId;
	
	public Long getTemplateId() {
		return templateId;
	}
	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}
	public Long getTokenId() {
		return tokenId;
	}
	public void setTokenId(Long tokenId) {
		this.tokenId = tokenId;
	}
}