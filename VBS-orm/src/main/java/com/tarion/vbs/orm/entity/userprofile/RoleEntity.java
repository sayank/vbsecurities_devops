package com.tarion.vbs.orm.entity.userprofile;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.envers.Audited;

import com.tarion.vbs.orm.entity.AuditedEntity;

@NamedQuery(name = RoleEntity.SELECT_ALL_ROLES, query = RoleEntity.JPQL_ALL_ROLES)
@NamedQuery(name = RoleEntity.SELECT_ROLES_FOR_USER, query = RoleEntity.JPQL_ROLES_FOR_USER)

@Entity
@Table(name="ROLE")
@Audited(withModifiedFlag=true)
public class RoleEntity implements AuditedEntity {

	private static final long serialVersionUID = 1l;
	
	public static final String SELECT_ALL_ROLES = "RoleEntity.SELECT_ALL_ROLES";
	protected static final String JPQL_ALL_ROLES = "SELECT o FROM RoleEntity o";
	
	public static final String SELECT_ROLES_FOR_USER = "RoleEntity.SELECT_ROLES_FOR_USER";
	protected static final String JPQL_ROLES_FOR_USER = "SELECT o FROM RoleEntity o " + 
			"JOIN UserRoleEntity ure ON o.id = ure.roleId " + 
			"WHERE ure.userId = ?1";
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "VERSION")
	@Version
	private Long version;
	@Column(name = "NAME")
	private String name;
	@Column(name = "DESCRIPTION")
	private String description;
	@Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoleEntity other = (RoleEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "RoleEntity [id=" + id + ", version=" + version + ", name=" + name + ", description=" + description
				+ ", createDate=" + createDate + ", createUser=" + createUser + ", updateDate=" + updateDate
				+ ", updateUser=" + updateUser + "]";
	}
	
}