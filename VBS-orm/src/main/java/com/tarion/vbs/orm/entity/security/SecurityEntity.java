/* 
 * 
 * SecurityEntity.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.security;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.*;

import com.tarion.vbs.orm.entity.contact.ContactEntity;
import org.hibernate.annotations.Formula;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.enums.YesNoEnum;
import com.tarion.vbs.orm.entity.AuditedEntity;
import com.tarion.vbs.orm.entity.financialinstitution.BranchEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionMaaPoaEntity;

/**
 * Security Entity JPA class holds information about Security
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-15
 * @version 1.0
 */
@NamedQuery(name=SecurityEntity.SELECT_ALL_INSTRUMENT_NUMBERS_LIKE, query = SecurityEntity.JPQL_ALL_INSTRUMENT_NUMBERS_LIKE)
@NamedQuery(name=SecurityEntity.SELECT_SECURITY_BY_INSTRUMENT_NUMBER, query = SecurityEntity.JPQL_SELECT_SECURITY_BY_INSTRUMENT_NUMBER)
@NamedQuery(name=SecurityEntity.COUNT_SECURITY_BY_SAME_INSTRUMENT_NUMBER, query = SecurityEntity.JPQL_COUNT_SECURITY_BY_SAME_INSTRUMENT_NUMBER)
@NamedQuery(name=SecurityEntity.SELECT_SECURITY_BY_VB_NUMBER, query = SecurityEntity.JPQL_SELECT_SECURITY_BY_VB_NUMBER)
@NamedNativeQuery(name=SecurityEntity.SELECT_SECURITY_BY_VB_NUMBER_BLANKET, query = SecurityEntity.JPQL_SELECT_SECURITY_BY_VB_NUMBER_BLANKET)
@NamedNativeQuery(name=SecurityEntity.SELECT_NUMBER_OF_SECURITY_AUD_BY_SECURITY_ID_STATUS, query = SecurityEntity.JPQL_SELECT_NUMBER_OF_SECURITY_AUD_BY_SECURITY_ID_STATUS)
@NamedQuery(name=SecurityEntity.SELECT_POOL_ID, query = SecurityEntity.JPQL_SELECT_POOL_ID)
@NamedQuery(name=SecurityEntity.SELECT_ALL_SECURITIES_BY_INSTRUMENT_NUMBER_LIKE, query = SecurityEntity.JPQL_SELECT_ALL_SECURITIES_BY_INSTRUMENT_NUMBER_LIKE)
@NamedQuery(name=SecurityEntity.SELECT_NUMBER_OF_CASH_SECURITIES_BY_INSTR_NUM_VB, query = SecurityEntity.JPQL_NUMBER_OF_CASH_SECURITIES_BY_INSTR_NUM_VB)
@NamedQuery(name=SecurityEntity.SELECT_DIRECT_FINANCIAL_INSTITUTION_AMOUNTS, query = SecurityEntity.JPQL_DIRECT_FINANCIAL_INSTITUTION_AMOUNTS)
@NamedQuery(name=SecurityEntity.SELECT_MAA_POA_FINANCIAL_INSTITUTION_AMOUNTS, query = SecurityEntity.JPQL_MAA_POA_FINANCIAL_INSTITUTION_AMOUNTS)
@NamedQuery(name=SecurityEntity.SELECT_SECURITY_ID_FOR_INSTR_NUM_FINANCIAL_INST, query = SecurityEntity.JPQL_SECURITY_ID_FOR_INSTR_NUM_FINANCIAL_INST)
@NamedQuery(name=SecurityEntity.SELECT_SECURITIES_BY_POOL_ID, query = SecurityEntity.JPQL_SELECT_SECURITIES_BY_POOL_ID)
@NamedQuery(name=SecurityEntity.SELECT_BOND_SUM_ORIGINAL_AMOUNT_BY_POOL_ID, query = SecurityEntity.JPQL_SELECT_BOND_SUM_ORIGINAL_AMOUNT_BY_POOL_ID)
@NamedQuery(name=SecurityEntity.SELECT_BOND_SUM_CURRENT_AMOUNT_BY_POOL_ID, query = SecurityEntity.JPQL_SELECT_BOND_SUM_CURRENT_AMOUNT_BY_POOL_ID)
@NamedQuery(name=SecurityEntity.SELECT_SECURITY_FOR_MAA_POA, query = SecurityEntity.JPQL_SECURITY_FOR_MAA_POA)
@NamedQuery(name=SecurityEntity.SELECT_BLANKET_SECURITY_BY_ENROLMENT_ENROLLING_VB, query = SecurityEntity.JPQL_BLANKET_SECURITY_BY_ENROLMENT_ENROLLING_VB)

@Entity
@Table(name = "SECURITY")
@Audited(withModifiedFlag=true)
public class SecurityEntity implements AuditedEntity {

	private static final long serialVersionUID = -1956463824404524329L;

	public static final String SELECT_ALL_INSTRUMENT_NUMBERS_LIKE = "SecurityEntity.SELECT_ALL_INSTRUMENT_NUMBERS_LIKE";	
	static final String JPQL_ALL_INSTRUMENT_NUMBERS_LIKE = "SELECT o.instrumentNumber FROM SecurityEntity o where o.instrumentNumber like ?1 AND o.deleted = FALSE ORDER BY o.instrumentNumber ASC ";

	public static final String SELECT_SECURITIES_BY_POOL_ID = "SecurityEntity.SELECT_SECURITIES_BY_POOL_ID";	
	static final String JPQL_SELECT_SECURITIES_BY_POOL_ID = "SELECT o FROM SecurityEntity o where o.pool.id = ?1 AND o.deleted = FALSE";

	public static final String SELECT_BOND_SUM_ORIGINAL_AMOUNT_BY_POOL_ID = "SecurityEntity.SELECT_BOND_SUM ORIGINAL_AMOUNT_BY_POOL_ID";
	static final String JPQL_SELECT_BOND_SUM_ORIGINAL_AMOUNT_BY_POOL_ID = "SELECT sum(s.originalAmount) FROM SecurityEntity s where s.pool.id = ?1 AND s.deleted = FALSE AND s.securityType.id = " + VbsConstants.SECURITY_TYPE_SURETY_BOND;

	public static final String SELECT_BOND_SUM_CURRENT_AMOUNT_BY_POOL_ID = "SecurityEntity.SELECT_BOND_SUM_CURRENT_AMOUNT_BY_POOL_ID";
	static final String JPQL_SELECT_BOND_SUM_CURRENT_AMOUNT_BY_POOL_ID = "SELECT sum(s.currentAmount) FROM SecurityEntity s where s.pool.id = ?1 AND s.deleted = FALSE AND s.securityType.id = " + VbsConstants.SECURITY_TYPE_SURETY_BOND;

	public static final String SELECT_SECURITY_BY_INSTRUMENT_NUMBER = "SecurityEntity.SECURITY_BY_INSTRUMENT_NUMBER";	
	static final String JPQL_SELECT_SECURITY_BY_INSTRUMENT_NUMBER = "SELECT o FROM SecurityEntity o where o.instrumentNumber = ?1 AND o.deleted = FALSE ORDER BY o.instrumentNumber ASC ";
	
	public static final String COUNT_SECURITY_BY_SAME_INSTRUMENT_NUMBER = "SecurityEntity.COUNT_SECURITY_BY_SAME_INSTRUMENT_NUMBER";	
	static final String JPQL_COUNT_SECURITY_BY_SAME_INSTRUMENT_NUMBER = "SELECT COUNT(o.id) FROM SecurityEntity o where upper(o.instrumentNumber) = ?1 AND o.deleted = FALSE";

	public static final String SELECT_SECURITY_BY_VB_NUMBER = "SecurityEntity.SECURITY_BY_VB_NUMBER";	
	static final String JPQL_SELECT_SECURITY_BY_VB_NUMBER = "SELECT o FROM SecurityEntity o INNER JOIN VbPoolEntity vbe " + 
			"			ON o.pool.id = vbe.pool.id " + 
			"			where vbe.vb.crmContactId =  ?1 ORDER BY o.id ASC ";

	public static final String SELECT_SECURITY_BY_VB_NUMBER_BLANKET = "SecurityEntity.SELECT_SECURITY_BY_VB_NUMBER_BLANKET";
	static final String JPQL_SELECT_SECURITY_BY_VB_NUMBER_BLANKET =
			"SELECT vb.CRM_CONTACT_ID as vbNumber, s.ID as securityNumber, s.INSTRUMENT_NUMBER as instrumentNumber, " +
					"(SELECT CURRENT_AMOUNT - COALESCE(SUM(R.PRINCIPAL_AMOUNT), 0) FROM RELEASE R WHERE R.SECURITY_ID = s.ID AND R.STATUS = 'PENDING') as currentAmount, " +
					"(select top 1 e.ENROLMENT_NUMBER from ENROLMENT e inner join ENROLMENT_POOL ep on e.ENROLMENT_NUMBER = ep.ENROLMENT_NUMBER where ep.POOL_ID = s.POOL_ID) as enrolmentNumber, " +
					"type.DESCRIPTION as securityType, " +
					"CASE WHEN (SELECT count(*) FROM VB_POOL vbPool WHERE  vbPool.POOL_ID = s.POOL_ID and vbPool.VB_DELETE_REASON_ID is null ) > 1 THEN 1 ELSE 0 END AS multiVB " +
			"FROM SECURITY s inner join CONTACT vb on s.PRIMARY_VB_ID = vb.ID inner join SECURITY_TYPE type on type.ID = s.SECURITY_TYPE_ID " +
			"WHERE vb.CRM_CONTACT_ID =  ?1 AND " +
					"s.SECURITY_TYPE_ID <> " + VbsConstants.SECURITY_TYPE_PRE_EXISTING_ELEMENTS_FUND + " AND " +
					"s.SECURITY_PURPOSE_ID = "+ VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY + " AND " +
					"(SELECT CURRENT_AMOUNT - COALESCE(SUM(R.PRINCIPAL_AMOUNT), 0) FROM RELEASE R WHERE R.SECURITY_ID = s.ID AND R.STATUS = 'PENDING') > 0 ";



	public static final String SELECT_ALL_SECURITIES_BY_INSTRUMENT_NUMBER_LIKE = "SecurityEntity.SELECT_ALL_SECURITIES_BY_INSTRUMENT_NUMBER_LIKE";
	static final String JPQL_SELECT_ALL_SECURITIES_BY_INSTRUMENT_NUMBER_LIKE = "SELECT o FROM SecurityEntity o where o.instrumentNumber like ?1 AND o.deleted = FALSE ORDER BY o.instrumentNumber ASC ";

	
	public static final String SELECT_POOL_ID = "SecurityEntity.SELECT_POOL_ID";	
	static final String JPQL_SELECT_POOL_ID = "SELECT o.pool.id FROM SecurityEntity o where o.id = ?1 AND o.deleted = FALSE";

	public static final String SELECT_NUMBER_OF_CASH_SECURITIES_BY_INSTR_NUM_VB = "SecurityEntity.SELECT_NUMBER_OF_SECURITIES_BY_INSTR_NUM_VB";
	static final String JPQL_NUMBER_OF_CASH_SECURITIES_BY_INSTR_NUM_VB = "SELECT count(distinct s) FROM VbPoolEntity vb, SecurityEntity s where s.instrumentNumber = ?1 AND vb.pool.id = s.pool.id AND vb.vbDeleteReason IS NULL AND vb.vb.crmContactId in ?2 AND s.securityType.id IN (" + VbsConstants.SECURITY_TYPE_CASH + ", " + VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT + ")";
	
	
	public static final String SELECT_DIRECT_FINANCIAL_INSTITUTION_AMOUNTS = "SecurityEntity.SELECT_DIRECT_FINANCIAL_INSTITUTION_AMOUNTS";	
	static final String JPQL_DIRECT_FINANCIAL_INSTITUTION_AMOUNTS = "SELECT sum(s.originalAmount) as totalOriginalAmount, sum(s.currentAmount) as totalCurrentAmount "
			+ " FROM SecurityEntity s where s.branch.financialInstitution.id = ?1  AND s.deleted = FALSE "
			+ " AND (?2 = true OR s.securityType.id not in (" + VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT + "," + VbsConstants.SECURITY_TYPE_PRE_EXISTING_ELEMENTS_FUND + "))" ;

	public static final String SELECT_MAA_POA_FINANCIAL_INSTITUTION_AMOUNTS = "SecurityEntity.SELECT_MAA_POA_FINANCIAL_INSTITUTION_AMOUNTS";	
	static final String JPQL_MAA_POA_FINANCIAL_INSTITUTION_AMOUNTS = 
			" SELECT sum(s.originalAmount * mpi.percentage / 100) as totalOriginalAmount, sum(s.currentAmount * mpi.percentage / 100) as totalCurrentAmount "
			+ " FROM SecurityEntity s "
			+ " INNER JOIN FinancialInstitutionMaaPoaInstitutionEntity mpi "
			+ " ON s.financialInstitutionMaaPoa.id = mpi.financialInstitutionMaaPoa.id"
			+ " where mpi.financialInstitution.id = ?1 AND s.deleted = FALSE  "
			+ " AND (?2 = true OR s.securityType.id not in (" + VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT + "," + VbsConstants.SECURITY_TYPE_PRE_EXISTING_ELEMENTS_FUND + "))" ;

	public static final String SELECT_SECURITY_ID_FOR_INSTR_NUM_FINANCIAL_INST = "SecurityEntity.SELECT_SECURITY_ID_FOR_INSTR_NUM_FINANCIAL_INST";	
	static final String JPQL_SECURITY_ID_FOR_INSTR_NUM_FINANCIAL_INST = "SELECT o.id FROM SecurityEntity o where UPPER(o.instrumentNumber) = UPPER(?1) AND o.deleted = FALSE AND o.branch.financialInstitution.id = ?2";

	public static final String SELECT_SECURITY_FOR_MAA_POA = "SecurityEntity.SELECT_SECURITY_FOR_MAA_POA";	
	static final String JPQL_SECURITY_FOR_MAA_POA = "SELECT o FROM SecurityEntity o "
			+ " where o.financialInstitutionMaaPoa.id = ?1 ";

	public static final String SELECT_BLANKET_SECURITY_BY_ENROLMENT_ENROLLING_VB = "SecurityEntity.SELECT_BLANKET_SECURITY_BY_ENROLMENT_ENROLLING_VB";
	static final String JPQL_BLANKET_SECURITY_BY_ENROLMENT_ENROLLING_VB = "SELECT s FROM SecurityEntity s JOIN VbPoolEntity vbp ON s.pool.id = vbp.pool.id AND vbp.vbDeleteReason IS NULL " +
			"JOIN EnrolmentEntity e ON e.enrollingVb.id = vbp.vb.id WHERE s.securityPurpose.id = 2 AND s.currentAmount > 0 AND e.enrolmentNumber = ?1 AND (s.identityType.id is null OR s.identityType.id <> " + VbsConstants.IDENTITY_TYPE_FREEHOLD + ")";

	public static final String SELECT_NUMBER_OF_SECURITY_AUD_BY_SECURITY_ID_STATUS = "SecurityEntity.SELECT_NUMBER_OF_SECURITY_AUD_BY_SECURITY_ID_STATUS";
	protected static final String JPQL_SELECT_NUMBER_OF_SECURITY_AUD_BY_SECURITY_ID_STATUS = "SELECT COUNT(ID) FROM SECURITY_AUD WHERE ID = ?1 AND STATUS_ID = ?2";
	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;
   
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POOL_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private PoolEntity pool;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private SecurityStatusEntity status;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SECURITY_TYPE_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private SecurityTypeEntity securityType;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SECURITY_PURPOSE_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private SecurityPurposeEntity securityPurpose;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDENTITY_TYPE_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private IdentityTypeEntity identityType;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BRANCH_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private BranchEntity branch;

	@Column(name = "INSTRUMENT_NUMBER")
	private String instrumentNumber;
	
	@Column(name = "ORIGINAL_AMOUNT")
	private BigDecimal originalAmount;
	
	@Column(name = "CURRENT_AMOUNT")
	private BigDecimal currentAmount;

	@Column(name = "ISSUED_DATE")
	private LocalDateTime issuedDate;
	
	@Column(name = "RECEIVED_DATE")
	private LocalDateTime receivedDate;

	@Column(name = "PPSA_NUMBER")
	private String ppsaNumber;
	
	@Column(name = "PPSA_FILE_NUMBER")
	private String ppsaFileNumber;
	
	@Column(name = "PPSA_EXPIRY_DATE")
	private LocalDateTime ppsaExpiryDate;
	
	@Column(name = "JOINT_BOND")
	private boolean jointBond;
	
	@Column(name = "JOINT_AUT")
	private boolean jointAut;
	
	@Column(name = "RELEASE_FIRST")
	@Enumerated(EnumType.STRING)
	private YesNoEnum releaseFirst;
	
	@Column(name = "DEMAND_FIRST")
	@Enumerated(EnumType.STRING)
	private YesNoEnum demandFirst;
	
	@Column(name = "COMMENT")
	private String comment;
	
	@Column(name = "THIRD_PARTY")
	private boolean thirdParty;

	@Column(name = "ALLOCATED")
	private boolean allocated;

	@Column(name = "WRITE_OFF")
	private boolean writeOff;

	@Column(name = "WRITE_OFF_REASON")
	private String writeOffReason;

	@Column(name = "WRITE_OFF_DATE")
	private LocalDateTime writeOffDate;

	@Column(name = "DELETED")
	private boolean deleted;

	@Column(name = "DTA_ACCOUNT_NUMBER")
	private String dtaAccountNumber;

	@Column(name = "UNPOST_REASON")
	private String unpostReason;

	@Column(name = "UNPOST_DATE")
	private LocalDateTime unpostDate;

	@Column(name = "UNPOST_USER")
	private String unpostUser;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FINANCIAL_INSTITUTION_MAAPOA_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private FinancialInstitutionMaaPoaEntity financialInstitutionMaaPoa;

	@Column(name = "CREATE_DATE")
    private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	@Column(name = "PEF_ACCOUNT_NUMBER")
	private String pefAccountNumber;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRIMARY_VB_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private ContactEntity primaryVb;

    @NotAudited
    @Formula("(SELECT CURRENT_AMOUNT - COALESCE(SUM(R.PRINCIPAL_AMOUNT), 0) FROM RELEASE R WHERE R.SECURITY_ID = ID AND R.STATUS = 'PENDING')")
    private BigDecimal availableAmount;

    //releasable for purposes of validation - newly created release cannot have a value greater than this.
    @NotAudited
    @Formula("(SELECT CURRENT_AMOUNT - COALESCE(SUM(R.PRINCIPAL_AMOUNT), 0) FROM RELEASE R WHERE R.SECURITY_ID = ID AND R.STATUS IN ('INITIATED', 'PENDING'))")
    private BigDecimal releasableAmount;

    @NotAudited
    @Formula("(SELECT " +
            " CASE WHEN SECURITY_TYPE_ID != 1 OR UNPOST_DATE IS NOT NULL THEN 0 " +
            " ELSE (SELECT COALESCE(SUM(I.INTEREST_AMOUNT), 0) FROM INTEREST I WHERE I.SECURITY_ID = ID) - " +
            " (SELECT COALESCE(SUM(R.INTEREST_AMOUNT),0) FROM RELEASE R WHERE R.SECURITY_ID = ID AND R.STATUS IN ('VOUCHER_CREATED', 'SENT', 'COMPLETED') )" +
            "END)")
    private BigDecimal currentInterest;

    @NotAudited
    @Formula("(SELECT COALESCE(SUM(R.INTEREST_AMOUNT), 0) FROM RELEASE R WHERE R.SECURITY_ID = ID AND R.STATUS = 'PENDING')")
    private BigDecimal forAvailableInterest;

    @NotAudited
    @Formula("(SELECT COALESCE(SUM(R.INTEREST_AMOUNT), 0) FROM RELEASE R WHERE R.SECURITY_ID = ID AND R.STATUS IN ('INITIATED', 'PENDING'))")
    private BigDecimal forReleasableInterest;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public PoolEntity getPool() {
		return pool;
	}
	public void setPool(PoolEntity pool) {
		this.pool = pool;
	}
	public SecurityStatusEntity getStatus() {
		return status;
	}
	public void setStatus(SecurityStatusEntity status) {
		this.status = status;
	}
	public SecurityTypeEntity getSecurityType() {
		return securityType;
	}
	public void setSecurityType(SecurityTypeEntity securityType) {
		this.securityType = securityType;
	}
	public SecurityPurposeEntity getSecurityPurpose() {
		return securityPurpose;
	}
	public void setSecurityPurpose(SecurityPurposeEntity securityPurpose) {
		this.securityPurpose = securityPurpose;
	}
	

	public IdentityTypeEntity getIdentityType() {
		return identityType;
	}
	
	public void setIdentityType(IdentityTypeEntity identityType) {
		this.identityType = identityType;
	}

	public BranchEntity getBranch() {
		return branch;
	}
	public void setBranch(BranchEntity branch) {
		this.branch = branch;
	}

	public String getInstrumentNumber() {
		return instrumentNumber;
	}
	public void setInstrumentNumber(String instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}
	public BigDecimal getOriginalAmount() {
		return originalAmount;
	}
	public void setOriginalAmount(BigDecimal originalAmount) {
		this.originalAmount = originalAmount;
	}
	public BigDecimal getCurrentAmount() {
		return currentAmount;
	}
	public void setCurrentAmount(BigDecimal currentAmount) {
		this.currentAmount = currentAmount;
	}

	public LocalDateTime getIssuedDate() {
		return issuedDate;
	}
	public void setIssuedDate(LocalDateTime issuedDate) {
		this.issuedDate = issuedDate;
	}
	public LocalDateTime getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(LocalDateTime receivedDate) {
		this.receivedDate = receivedDate;
	}

	public String getPpsaNumber() {
		return ppsaNumber;
	}
	public void setPpsaNumber(String ppsaNumber) {
		this.ppsaNumber = ppsaNumber;
	}
	public String getPpsaFileNumber() {
		return ppsaFileNumber;
	}
	public void setPpsaFileNumber(String ppsaFileNumber) {
		this.ppsaFileNumber = ppsaFileNumber;
	}
	public LocalDateTime getPpsaExpiryDate() {
		return ppsaExpiryDate;
	}
	public void setPpsaExpiryDate(LocalDateTime ppsaExpiryDate) {
		this.ppsaExpiryDate = ppsaExpiryDate;
	}
	public boolean isJointBond() {
		return jointBond;
	}
	public void setJointBond(boolean jointBond) {
		this.jointBond = jointBond;
	}
	public boolean isJointAut() {
		return jointAut;
	}
	public void setJointAut(boolean jointAut) {
		this.jointAut = jointAut;
	}
	public YesNoEnum getReleaseFirst() {
		return releaseFirst;
	}
	public void setReleaseFirst(YesNoEnum releaseFirst) {
		this.releaseFirst = releaseFirst;
	}
	public YesNoEnum getDemandFirst() {
		return demandFirst;
	}
	public void setDemandFirst(YesNoEnum demandFirst) {
		this.demandFirst = demandFirst;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public boolean isThirdParty() {
		return thirdParty;
	}
	public void setThirdParty(boolean thirdParty) {
		this.thirdParty = thirdParty;
	}
	public String getDtaAccountNumber() {
		return dtaAccountNumber;
	}
	public void setDtaAccountNumber(String dtaAccountNumber) {
		this.dtaAccountNumber = dtaAccountNumber;
	}

	public String getUnpostUser() {
		return unpostUser;
	}

	public void setUnpostUser(String unpostUser) {
		this.unpostUser = unpostUser;
	}

	public boolean isWriteOff() {
		return writeOff;
	}

	public void setWriteOff(boolean writeOff) {
		this.writeOff = writeOff;
	}

	public String getWriteOffReason() {
		return writeOffReason;
	}

	public void setWriteOffReason(String writeOffReason) {
		this.writeOffReason = writeOffReason;
	}

	public LocalDateTime getWriteOffDate() {
		return writeOffDate;
	}

	public void setWriteOffDate(LocalDateTime writeOffDate) {
		this.writeOffDate = writeOffDate;
	}


	public boolean isAllocated() {
		return allocated;
	}

	public void setAllocated(boolean allocated) {
		this.allocated = allocated;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getUnpostReason() {
		return unpostReason;
	}

	public void setUnpostReason(String unpostReason) {
		this.unpostReason = unpostReason;
	}

	public LocalDateTime getUnpostDate() {
		return unpostDate;
	}

	public void setUnpostDate(LocalDateTime unpostDate) {
		this.unpostDate = unpostDate;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public BigDecimal getAvailableAmount() {
		return availableAmount;
	}

	public BigDecimal getAvailableInterest() { return (this.currentInterest != null) ? this.currentInterest.subtract(this.forAvailableInterest) : null; }

	public BigDecimal getReleasableAmount() {
		return releasableAmount;
	}

	public BigDecimal getReleasableInterest() { return (this.currentInterest != null) ? this.currentInterest.subtract(this.forReleasableInterest) : null; }

	public BigDecimal getCurrentInterest() { return currentInterest; }

	public FinancialInstitutionMaaPoaEntity getFinancialInstitutionMaaPoa() {
		return financialInstitutionMaaPoa;
	}
	public void setFinancialInstitutionMaaPoa(FinancialInstitutionMaaPoaEntity financialInstitutionMaaPoa) {
		this.financialInstitutionMaaPoa = financialInstitutionMaaPoa;
	}
	public String getPefAccountNumber() {
		return pefAccountNumber;
	}

	public void setPefAccountNumber(String pefAccountNumber) {
		this.pefAccountNumber = pefAccountNumber;
	}

	public ContactEntity getPrimaryVb() {
		return primaryVb;
	}

	public void setPrimaryVb(ContactEntity primaryVb) {
		this.primaryVb = primaryVb;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		SecurityEntity that = (SecurityEntity) o;
		return 	jointBond == that.jointBond &&
				jointAut == that.jointAut &&
				thirdParty == that.thirdParty &&
				allocated == that.allocated &&
				writeOff == that.writeOff &&
				deleted == that.deleted &&
				id.equals(that.id) &&
				pool.equals(that.pool) &&
				Objects.equals(status, that.status) &&
				securityType.equals(that.securityType) &&
				securityPurpose.equals(that.securityPurpose) &&
				Objects.equals(identityType, that.identityType) &&
				Objects.equals(branch, that.branch) &&
				instrumentNumber.equals(that.instrumentNumber) &&
				originalAmount.equals(that.originalAmount) &&
				currentAmount.equals(that.currentAmount) &&
				issuedDate.equals(that.issuedDate) &&
				receivedDate.equals(that.receivedDate) &&
				Objects.equals(ppsaNumber, that.ppsaNumber) &&
				Objects.equals(ppsaFileNumber, that.ppsaFileNumber) &&
				Objects.equals(ppsaExpiryDate, that.ppsaExpiryDate) &&
				releaseFirst == that.releaseFirst &&
				demandFirst == that.demandFirst &&
				Objects.equals(comment, that.comment) &&
				Objects.equals(writeOffReason, that.writeOffReason) &&
				Objects.equals(writeOffDate, that.writeOffDate) &&
				Objects.equals(dtaAccountNumber, that.dtaAccountNumber) &&
				Objects.equals(unpostReason, that.unpostReason) &&
				Objects.equals(unpostDate, that.unpostDate) &&
				Objects.equals(unpostUser, that.unpostUser) &&
				Objects.equals(financialInstitutionMaaPoa, that.financialInstitutionMaaPoa) &&
				createDate.equals(that.createDate) &&
				createUser.equals(that.createUser) &&
				Objects.equals(pefAccountNumber, that.pefAccountNumber) &&
				primaryVb.equals(that.primaryVb);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, pool, status, securityType, securityPurpose, identityType, branch, instrumentNumber, originalAmount, currentAmount, issuedDate, receivedDate, ppsaNumber, ppsaFileNumber, ppsaExpiryDate, jointBond, jointAut, releaseFirst, demandFirst, comment, thirdParty, allocated, writeOff, writeOffReason, writeOffDate, deleted, dtaAccountNumber, unpostReason, unpostDate, unpostUser, financialInstitutionMaaPoa, createDate, createUser, pefAccountNumber, primaryVb);
	}

	@Override
	public String toString() {
		return "SecurityEntity [id=" + id + ", version=" + version + "]";
	}	

}
