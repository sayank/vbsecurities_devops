/* 
 * 
 * FinancialInstitutionEntity.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.financialinstitution;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.tarion.vbs.orm.entity.AuditedEntity;

/**
 * Financial Institution Entity JPA class holds information about Financial Institution
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-11-01
 * @version 1.0
 */

@NamedQuery(name=FinancialInstitutionEntity.SELECT_ALL_FINANCIAL_INSTITUTIONS_NAME_LIKE, query = FinancialInstitutionEntity.JPQL_ALL_FINANCIAL_INSTITUTIONS_NAME_LIKE)
@NamedQuery(name=FinancialInstitutionEntity.SELECT_ALL_FINANCIAL_INSTITUTIONS_ID_LIKE, query = FinancialInstitutionEntity.JPQL_ALL_FINANCIAL_INSTITUTIONS_ID_LIKE)
@NamedQuery(name=FinancialInstitutionEntity.SELECT_ALL_FINANCIAL_INSTITUTIONS, query = FinancialInstitutionEntity.JPQL_ALL_FINANCIAL_INSTITUTIONS)
@NamedQuery(name=FinancialInstitutionEntity.SELECT_TYPE_FOR_FINANCIAL_INSTITUTION_ID, query = FinancialInstitutionEntity.JPQL_TYPE_FOR_FINANCIAL_INSTITUTION_ID)

@Entity
@Table(name = "FINANCIAL_INSTITUTION")
@Audited(withModifiedFlag=true)
public class FinancialInstitutionEntity implements AuditedEntity {
	private static final long serialVersionUID = 1L;

	public static final String SELECT_ALL_FINANCIAL_INSTITUTIONS_NAME_LIKE = "FinancialInstitutionEntity.SELECT_ALL_FINANCIAL_INSTITUTIONS_NAME_LIKE";	
	protected static final String JPQL_ALL_FINANCIAL_INSTITUTIONS_NAME_LIKE = "SELECT o FROM FinancialInstitutionEntity o where UPPER(o.name) like ?1 ORDER BY o.name ASC ";

	public static final String SELECT_ALL_FINANCIAL_INSTITUTIONS_ID_LIKE = "FinancialInstitutionEntity.SELECT_ALL_FINANCIAL_INSTITUTIONS_ID_LIKE";	
	protected static final String JPQL_ALL_FINANCIAL_INSTITUTIONS_ID_LIKE = "SELECT o FROM FinancialInstitutionEntity o where CAST(o.id as string) LIKE ?1 ORDER BY o.id ASC ";
	
	public static final String SELECT_ALL_FINANCIAL_INSTITUTIONS = "FinancialInstitutionEntity.SELECT_ALL_FINANCIAL_INSTITUTIONS";	
	protected static final String JPQL_ALL_FINANCIAL_INSTITUTIONS = "SELECT o FROM FinancialInstitutionEntity o ORDER BY o.name ASC ";

	public static final String SELECT_TYPE_FOR_FINANCIAL_INSTITUTION_ID = "FinancialInstitutionEntity.SELECT_TYPE_FOR_FINANCIAL_INSTITUTION_ID";
	protected static final String JPQL_TYPE_FOR_FINANCIAL_INSTITUTION_ID = "SELECT o.financialInstitutionType.id FROM FinancialInstitutionEntity o WHERE o.id = ?1";

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;
   
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "FINANCIAL_INSTITUTION_TYPE_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private FinancialInstitutionTypeEntity financialInstitutionType;

	@Column(name = "NAME")
	private String name;
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "EXPIRY_DATE")
    private LocalDateTime expiryDate;
	
	@Column(name = "MAX_SECURITY_AMOUNT")
	private BigDecimal maxSecurityAmount;
	
	@Column(name = "WN_SECURITY_MAX_AMOUNT")
	private BigDecimal wnSecurityMaxAmount;
	
	@Column(name = "CREATE_DATE")
    private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	
	public FinancialInstitutionTypeEntity getFinancialInstitutionType() {
		return financialInstitutionType;
	}
	public void setFinancialInstitutionType(FinancialInstitutionTypeEntity financialInstitutionType) {
		this.financialInstitutionType = financialInstitutionType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public LocalDateTime getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(LocalDateTime expiryDate) {
		this.expiryDate = expiryDate;
	}
	public BigDecimal getMaxSecurityAmount() {
		return maxSecurityAmount;
	}
	public void setMaxSecurityAmount(BigDecimal maxSecurityAmount) {
		this.maxSecurityAmount = maxSecurityAmount;
	}
	public BigDecimal getWnSecurityMaxAmount() {
		return wnSecurityMaxAmount;
	}
	public void setWnSecurityMaxAmount(BigDecimal wnSecurityMaxAmount) {
		this.wnSecurityMaxAmount = wnSecurityMaxAmount;
	}
	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinancialInstitutionEntity other = (FinancialInstitutionEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "FinancialInstitutionEntity [id=" + id + ", version=" + version +
				", name=" + name +
				", description=" + description +
				", expiryDate=" + expiryDate +
				", maxSecurityAmount=" + maxSecurityAmount +
				", wsSecurityMaxAmount=" + wnSecurityMaxAmount +
				"]";
	}	

}
