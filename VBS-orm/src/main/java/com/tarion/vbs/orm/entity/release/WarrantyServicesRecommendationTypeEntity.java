package com.tarion.vbs.orm.entity.release;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Release Type Entity JPA class holds diferent types of Release
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-03-15
 * @version 1.0
 */
@NamedQuery(name=WarrantyServicesRecommendationTypeEntity.SELECT_ALL_WARRANTY_SERVICES_RECOMMENDATIONS, query = WarrantyServicesRecommendationTypeEntity.JPQL_ALL_WARRANTY_SERVICES_RECOMMENDATIONS)
@NamedQuery(name=WarrantyServicesRecommendationTypeEntity.SELECT_WARRANTY_SERVICES_RECOMMENDATIONS_FOR_NAME, query = WarrantyServicesRecommendationTypeEntity.JPQL_WARRANTY_SERVICES_RECOMMENDATIONS_FOR_NAME)
@Entity
@Table(name = "WARRANTY_SERVICES_RECOMMENDATION_TYPE")
public class WarrantyServicesRecommendationTypeEntity implements Serializable{

	private static final long serialVersionUID = 1l;

	public static final String SELECT_ALL_WARRANTY_SERVICES_RECOMMENDATIONS = "WarrantyServicesRecommendationTypeEntity.SELECT_ALL_WARRANTY_SERVICES_RECOMMENDATIONS";	
	protected static final String JPQL_ALL_WARRANTY_SERVICES_RECOMMENDATIONS = "SELECT o FROM WarrantyServicesRecommendationTypeEntity o ORDER BY o.id ASC ";

	public static final String SELECT_WARRANTY_SERVICES_RECOMMENDATIONS_FOR_NAME = "WarrantyServicesRecommendationTypeEntity.SELECT_WARRANTY_SERVICES_RECOMMENDATIONS_FOR_NAME";	
	protected static final String JPQL_WARRANTY_SERVICES_RECOMMENDATIONS_FOR_NAME = "SELECT o FROM WarrantyServicesRecommendationTypeEntity o WHERE o.name = ?1 ORDER BY o.id ASC ";

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "NAME")
	private String name;
	@Column(name = "DESCRIPTION")
	private String description;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WarrantyServicesRecommendationTypeEntity other = (WarrantyServicesRecommendationTypeEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "ReleaseTypeEntity [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
	
	
}
