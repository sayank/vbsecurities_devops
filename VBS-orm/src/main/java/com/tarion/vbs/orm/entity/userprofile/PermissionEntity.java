/* 
 * 
 * PermissionEntity.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.userprofile;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Permission Entity JPA class holds permission information
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-01-05
 * @version 1.0
 */

@NamedQuery(name=PermissionEntity.SELECT_ALL_PERMISSION, query = PermissionEntity.JPQL_ALL_PERMISSION)
@NamedQuery(name=PermissionEntity.SELECT_PERMISSIONS_BY_ROLE, query = PermissionEntity.JPQL_PERMISSIONS_BY_ROLE)
@NamedQuery(name=PermissionEntity.SELECT_PERMISSIONS_FOR_USER, query = PermissionEntity.JPQL_PERMISSIONS_FOR_USER)

@Entity
@Table(name = "PERMISSION")
public class PermissionEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String SELECT_ALL_PERMISSION = "PermissionEntity.SELECT_ALL_PERMISSION";	
	protected static final String JPQL_ALL_PERMISSION = "SELECT o FROM PermissionEntity o ORDER BY o.id ASC";
	
	public static final String SELECT_PERMISSIONS_BY_ROLE = "PermissionEntity.SELECT_PERMISSIONS_BY_ROLE";	
	protected static final String JPQL_PERMISSIONS_BY_ROLE = "SELECT o FROM PermissionEntity o JOIN RolePermissionEntity rp ON o.id = rp.permissionId AND rp.roleId = ?1 ORDER BY o.id ASC";

	public static final String SELECT_PERMISSIONS_FOR_USER = "PermissionEntity.SELECT_PERMISSIONS_FOR_USER";	
	protected static final String JPQL_PERMISSIONS_FOR_USER = "SELECT DISTINCT o FROM PermissionEntity o "
			+ "JOIN RolePermissionEntity rp ON o.id = rp.permissionId "
			+ "JOIN UserRoleEntity ur ON rp.roleId = ur.roleId AND ur.userId = ?1 ORDER BY o.id ASC";
	
	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "NAME")
	private String name;
	@Column(name = "DESCRIPTION")
	private String description;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PermissionEntity other = (PermissionEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "PermissionEntity [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
	
}
