package com.tarion.vbs.orm.entity.autorelease;

import java.time.LocalDateTime;

import javax.persistence.*;

import org.hibernate.envers.Audited;

import com.tarion.vbs.orm.entity.AuditedEntity;

@NamedQuery(name=AutoReleaseVbDataEntity.SELECT_WITH_VB_NUMBER_NUMBER_RUN_ID, query=AutoReleaseVbDataEntity.JPQL_SELECT_WITH_VB_NUMBER_NUMBER_RUN_ID)

@Entity
@Table(name = "AUTO_RELEASE_VB_DATA")
@Audited(withModifiedFlag=true)
public class AutoReleaseVbDataEntity implements AuditedEntity {

	public static final String SELECT_WITH_VB_NUMBER_NUMBER_RUN_ID = "AutoReleaseVbDataEntity.SELECT_WITH_ENROLMENT_NUMBER_RUN_ID";
	protected static final String JPQL_SELECT_WITH_VB_NUMBER_NUMBER_RUN_ID = "SELECT e FROM AutoReleaseVbDataEntity e WHERE e.vbNumber = ?1 AND e.autoReleaseRun.id = ?2";

	private static final long serialVersionUID = 1594327867691398395L;

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;	
	@Version
    @Column(name = "VERSION")
	private Long version;
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "AUTO_RELEASE_RUN_ID")
	private AutoReleaseRunEntity autoReleaseRun;	
	@Column(name = "VB_NUMBER")
	private String vbNumber;
	@Column(name = "UNDER_INVESTIGATION")
	private boolean underInvestigation;
	@Column(name = "CREDIT_RATING")
	private String creditRating;
	@Column(name = "LICENSE_STATUS")
	private Long licenseStatus;
	@Column(name = "ACTIVE_ALERT")
	private boolean activeAlert;
	@Column(name = "OS_CCPS")
	private Long osCCPS;	
	@Column(name = "AVG_GUARANTOR_CREDIT_RATING")
	private String avgGuarantorCreditRating;
	@Column(name = "NUM_GUARANTORS")
	private Long numGuarantors;
	@Column(name = "UNWILLING_UNABLE")
	private boolean unwillingUnable;	
	@Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}	
	public AutoReleaseRunEntity getAutoReleaseRun() {
		return autoReleaseRun;
	}
	public void setAutoReleaseRun(AutoReleaseRunEntity autoReleaseRun) {
		this.autoReleaseRun = autoReleaseRun;
	}	
	public String getVbNumber() {
		return vbNumber;
	}
	public void setVbNumber(String vbNumber) {
		this.vbNumber = vbNumber;
	}
	public boolean isUnderInvestigation() {
		return underInvestigation;
	}
	public void setUnderInvestigation(boolean underInvestigation) {
		this.underInvestigation = underInvestigation;
	}
	public String getCreditRating() {
		return creditRating;
	}
	public void setCreditRating(String creditRating) {
		this.creditRating = creditRating;
	}
	public Long getLicenseStatus() {
		return licenseStatus;
	}
	public void setLicenseStatus(Long licenseStatus) {
		this.licenseStatus = licenseStatus;
	}
	public boolean isActiveAlert() {
		return activeAlert;
	}
	public void setActiveAlert(boolean activeAlert) {
		this.activeAlert = activeAlert;
	}
	public Long getOsCCPS() {
		return osCCPS;
	}
	public void setOsCCPS(Long osCCPS) {
		this.osCCPS = osCCPS;
	}
	public String getAvgGuarantorCreditRating() {
		return avgGuarantorCreditRating;
	}
	public void setAvgGuarantorCreditRating(String avgGuarantorCreditRating) {
		this.avgGuarantorCreditRating = avgGuarantorCreditRating;
	}
	public Long getNumGuarantors() {
		return numGuarantors;
	}
	public void setNumGuarantors(Long numGuarantors) {
		this.numGuarantors = numGuarantors;
	}
	public boolean isUnwillingUnable() {
		return unwillingUnable;
	}
	public void setUnwillingUnable(boolean unwillingUnable) {
		this.unwillingUnable = unwillingUnable;
	}
	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	@Override
	public String toString() {
		return "AutoReleaseVbData [id=" + id + ", version=" + version
				+ ", vbNumber=" + vbNumber + ", underInvestigation=" + underInvestigation + ", creditRating="
				+ creditRating + ", licenseStatus=" + licenseStatus + ", activeAlert=" + activeAlert + ", osCCPS="
				+ osCCPS + ", avgGuarantorCreditRating=" + avgGuarantorCreditRating + ", numGuarantors=" + numGuarantors
				+ ", unwillingUnable=" + unwillingUnable + ", createDate=" + createDate + ", createUser=" + createUser
				+ ", updateDate=" + updateDate + ", updateUser=" + updateUser + "]";
	}	
}
