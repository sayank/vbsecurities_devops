package com.tarion.vbs.orm.entity.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * This class represents the VBS Property Entity, that holds all VB database Configuartion properties
 *
 */

@NamedQuery(name=PropertiesEntity.SELECT_ALL_PROPERTIES, query = PropertiesEntity.JPQL_ALL_PROPERTIES)

@Entity
@Table(name = "VBS_CONFIG")
public class PropertiesEntity implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
	public static final String SELECT_ALL_PROPERTIES = "PropertiesEntity.SELECT_ALL_PROPERTIES";
	protected static final String JPQL_ALL_PROPERTIES = "SELECT o FROM PropertiesEntity o ";

	@Id
	@Column(name = "NAME")
    private String name;

	@Column(name = "DESCRIPTION")
	private String description;
	@Column(name = "VALUE")
    private String value;


    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(String value) {
        this.value = value;
    }
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PropertiesEntity other = (PropertiesEntity) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "PropertiesEntity [name=" + name + ", description=" + description + ", value=" + value + "]";
	}

}
