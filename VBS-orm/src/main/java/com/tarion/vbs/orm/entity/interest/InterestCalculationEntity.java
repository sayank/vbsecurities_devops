package com.tarion.vbs.orm.entity.interest;

import com.tarion.vbs.common.enums.InterestStatusEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@NamedQuery(name = InterestCalculationEntity.SELECT_LAST_CALCULATED_INTEREST_DATE, query = InterestCalculationEntity.JPQL_LAST_CALCULATED_INTEREST_DATE)

@Entity
@Table(name = "INTEREST_CALCULATION")
public class InterestCalculationEntity implements Serializable {

    /*
    public static final String SELECT_SECURITY_IDS_FOR_INTEREST_CALC = "InterestCalculationEntity.SELECT_SECURITIES_FOR_INTEREST_CALC";
    static final String JPQL_SECURITY_IDS_FOR_INTEREST_CALC = "SELECT o.id FROM SecurityEntity o" +
            " WHERE o.securityType.id = 1 AND o.currentAmount > 0 AND o.allocated = TRUE AND o.deleted = FALSE AND o.receivedDate <=  ?1" +
            " AND NOT EXISTS (SELECT i FROM InterestEntity i WHERE i.interestCalculation.interestDate = ?2)";

    public static final String SELECT_SECURITY_IDS_AMOUNTS_FOR_INTEREST_CALC = "InterestCalculationEntity.SELECT_SECURITY_IDS_AMOUNTS_FOR_INTEREST_CALC";
    static final String JPQL_SECURITY_IDS_FOR__AMOUNTS_INTEREST_CALC = "SELECT o.id, o.currentAmount FROM SecurityEntity o" +
            " WHERE o.securityType.id = 1 AND o.currentAmount > 0 AND o.allocated = TRUE AND o.deleted = FALSE AND o.receivedDate <=  ?1" +
            " AND NOT EXISTS (SELECT i FROM InterestEntity i WHERE i.interestCalculation.interestDate = ?2)";

     */

    public static final String SELECT_LAST_CALCULATED_INTEREST_DATE = "InterestCalculationEntity.SELECT_LAST_CALCULATED_INTEREST_DATE";
    static final String JPQL_LAST_CALCULATED_INTEREST_DATE = "SELECT MAX(o.interestDate) FROM InterestCalculationEntity o";

    private static final long serialVersionUID = -1754431154779990123L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="INTEREST_RATE_ID")
    private InterestRateEntity interestRate;

    @Column(name="INTEREST_DATE")
    private LocalDate interestDate;

    @Column(name="CALCULATION_DATE")
    private LocalDateTime calculationDate;

    @Enumerated(EnumType.STRING)
    @Column(name="STATUS")
    private InterestStatusEnum status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InterestRateEntity getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(InterestRateEntity interestRate) {
        this.interestRate = interestRate;
    }

    public LocalDate getInterestDate() {
        return interestDate;
    }

    public void setInterestDate(LocalDate interestDate) {
        this.interestDate = interestDate;
    }

    public LocalDateTime getCalculationDate() {
        return calculationDate;
    }

    public void setCalculationDate(LocalDateTime calculationDate) {
        this.calculationDate = calculationDate;
    }

    public InterestStatusEnum getStatus() {
        return status;
    }

    public void setStatus(InterestStatusEnum status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "InterestCalculationEntity{" +
                "id=" + id +
                ", interestRate=" + interestRate +
                ", interestDate=" + interestDate +
                ", calculationDate=" + calculationDate +
                ", status=" + status +
                '}';
    }
}
