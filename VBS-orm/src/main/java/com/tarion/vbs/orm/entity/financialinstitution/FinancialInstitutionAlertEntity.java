package com.tarion.vbs.orm.entity.financialinstitution;

import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.envers.Audited;

import com.tarion.vbs.common.enums.AlertStatus;
import com.tarion.vbs.orm.entity.AuditedEntity;
import com.tarion.vbs.orm.entity.alert.AlertTypeEntity;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * Alert Entity JPA class holds alert informations for security
 *
 * @author <A href="djordje.przulj@tarion.com">Djordje Przulj</A>
 * @since 2019-04-16
 * @version 1.0
 */
@NamedQuery(name=FinancialInstitutionAlertEntity.SELECT_ALERT_FOR_FINANCIAL_INSTITUTION_ID, query = FinancialInstitutionAlertEntity.JPQL_ALERT_FOR_FINANCIAL_INSTITUTION_ID)
@Entity
@Table(name="FINANCIAL_INSTITUTION_ALERT")
@Audited(withModifiedFlag=true)
public class FinancialInstitutionAlertEntity implements AuditedEntity {

	private static final long serialVersionUID = 1L;

	public static final String SELECT_ALERT_FOR_FINANCIAL_INSTITUTION_ID = "FinancialInstitutionAlertEntity.SELECT_ALERT_FOR_FINANCIAL_INSTITUTION_ID";
	static final String JPQL_ALERT_FOR_FINANCIAL_INSTITUTION_ID = "SELECT o FROM FinancialInstitutionAlertEntity o WHERE o.financialInstitution.id = ?1  ORDER BY o.startDate ";
	
	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;

	@Version
    @Column(name = "VERSION")
	private Long version;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FINANCIAL_INSTITUTION_ID")
	private FinancialInstitutionEntity financialInstitution;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ALERT_TYPE_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private AlertTypeEntity alertType;
	
	@Column(name = "NAME")
	private String name;

	@Column(name = "DESCRIPTION")
	private String description;
	
    @Column(name = "START_DATE")
	private LocalDateTime startDate;
    
    @Column(name = "END_DATE")
	private LocalDateTime endDate;

    @Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
    
	@Column(name = "CREATE_USER")
	private String createUser;
	
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	
	@Column(name = "UPDATE_USER")
	private String updateUser;
	
	@Column(name = "END_USER")
	private String endUser;
	
	@Transient
	public AlertStatus getStatus() {
		if (this.endDate == null) {
			return AlertStatus.ACTIVE;
		} else
			return AlertStatus.INACTIVE;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public FinancialInstitutionEntity getFinancialInstitution() {
		return financialInstitution;
	}

	public void setFinancialInstitution(FinancialInstitutionEntity financialInstitution) {
		this.financialInstitution = financialInstitution;
	}
	
	public AlertTypeEntity getAlertType() {
		return alertType;
	}

	public void setAlertType(AlertTypeEntity alertType) {
		this.alertType = alertType;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getEndUser() {
		return endUser;
	}

	public void setEndUser(String endUser) {
		this.endUser = endUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinancialInstitutionAlertEntity other = (FinancialInstitutionAlertEntity) obj;
		if (id == null) {
			return other.id == null;
		} else {
			if ((this.name != null && (other.name == null || !this.name.equals(other.name))) 
					|| (this.name == null && other.name != null)) {
				return false;
			} else if ((this.description != null && (other.description == null || !this.description.equals(other.description)))
					|| (this.description == null && other.description != null)) {
				return false;
			} else if ((this.alertType != null && (other.alertType == null || !this.alertType.equals(other.alertType)))
					|| (this.alertType == null && other.alertType != null)) {
				return false;				
			} else if ((this.createDate != null && (other.createDate == null || !this.createDate.equals(other.createDate)))
					|| (this.createDate == null && other.createDate != null)) {
				return false;
			} else if ((this.startDate != null && (other.startDate == null || !this.startDate.equals(other.startDate)))
					|| (this.startDate== null && other.startDate!= null)) {
				return false;
			} else if ((this.endDate != null && (other.endDate == null || !this.endDate.equals(other.endDate)))
					|| (this.endDate == null && other.endDate != null)) {
				return false;
			} else if ((this.endUser != null && (other.endUser == null || !this.endUser.equals(other.endUser)))
					|| (this.endUser == null && other.endUser != null)) {
				return false;				
			} else {
				return true;
			}
		}
	}

	@Override
	public String toString() {
		return "AlertEntity [id=" + id + ", version=" + version  
				+ ", name=" + name + ", description=" + description + ", startDate=" + startDate  
				+ ", endDate=" + endDate + ", endUser=" + endUser
				+ ", createDate=" + createDate + ", createUser=" + createUser 
				+ ", updateDate=" + updateDate + ", updateUser=" + updateUser + "]";
	}
}
