package com.tarion.vbs.orm.entity.autorelease;

import com.tarion.vbs.orm.entity.AuditedEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDateTime;

@NamedQuery(name=AutoReleaseVbApplicationStatusEntity.SELECT_WITH_VB_NUMBER_NUMBER_RUN_ID, query=AutoReleaseVbApplicationStatusEntity.JPQL_SELECT_WITH_VB_NUMBER_NUMBER_RUN_ID)

@Entity
@Table(name = "AUTO_RELEASE_VB_APPLICATION_STATUS")
@Audited(withModifiedFlag=true)
public class AutoReleaseVbApplicationStatusEntity implements AuditedEntity {

	public static final String SELECT_WITH_VB_NUMBER_NUMBER_RUN_ID = "AutoReleaseVbApplicationStatusEntity.SELECT_WITH_ENROLMENT_NUMBER_RUN_ID";
	protected static final String JPQL_SELECT_WITH_VB_NUMBER_NUMBER_RUN_ID = "SELECT e FROM AutoReleaseVbApplicationStatusEntity e WHERE e.vbNumber = ?1 AND e.autoReleaseRun.id = ?2";

	private static final long serialVersionUID = 1594327867691398395L;

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;	
	@Version
    @Column(name = "VERSION")
	private Long version;
	
	@ManyToOne(optional=false, fetch = FetchType.LAZY)
    @JoinColumn(name = "AUTO_RELEASE_RUN_ID")
	private AutoReleaseRunEntity autoReleaseRun;	
	@Column(name = "VB_NUMBER")
	private String vbNumber;
	@Column(name = "APPLICATION_STATUS")
	private Long applicationStatus;
	@Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Long getVersion() {
		return version;
	}

	@Override
	public void setVersion(Long version) {
		this.version = version;
	}

	public AutoReleaseRunEntity getAutoReleaseRun() {
		return autoReleaseRun;
	}

	public void setAutoReleaseRun(AutoReleaseRunEntity autoReleaseRun) {
		this.autoReleaseRun = autoReleaseRun;
	}

	public String getVbNumber() {
		return vbNumber;
	}

	public void setVbNumber(String vbNumber) {
		this.vbNumber = vbNumber;
	}

	public Long getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(Long applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	@Override
	public LocalDateTime getCreateDate() {
		return createDate;
	}

	@Override
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	@Override
	public String getCreateUser() {
		return createUser;
	}

	@Override
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Override
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	@Override
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String getUpdateUser() {
		return updateUser;
	}

	@Override
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public String toString() {
		return "AutoReleaseVbApplicationStatusEntity{" +
				"id=" + id +
				", version=" + version +
				", vbNumber='" + vbNumber + '\'' +
				", applicationStatus=" + applicationStatus +
				", createDate=" + createDate +
				", createUser='" + createUser + '\'' +
				", updateDate=" + updateDate +
				", updateUser='" + updateUser + '\'' +
				'}';
	}
}
