package com.tarion.vbs.orm.entity.enrolment;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.enums.EnrolmentStatus;
import com.tarion.vbs.orm.entity.AddressEntity;
import com.tarion.vbs.orm.entity.AuditedEntity;
import com.tarion.vbs.orm.entity.contact.ContactEntity;
import org.hibernate.annotations.Formula;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Enrolment Entity JPA class holds enrolment information
 *
 * @author <A href="shadi.kajevand@tarion.com">Shadi Kajevand</A>
 * @since 2018-11-26
 * @version 1.0
 */

@NamedNativeQuery(name=EnrolmentEntity.SELECT_AUTO_RELEASE_SHORTLIST, query = EnrolmentEntity.JPQL_AUTO_RELEASE_SHORTLIST)

@NamedQuery(name=EnrolmentEntity.SELECT_ALL_ENROLMENT_NUMBERS_LIKE, query = EnrolmentEntity.JPQL_SELECT_ALL_ENROLMENT_NUMBERS_LIKE)
@NamedQuery(name=EnrolmentEntity.SELECT_AVAILABLE_ENROLMENTS_BY_POOLID, query = EnrolmentEntity.JPQL_SELECT_AVAILABLE_ENROLMENTS_BY_POOLID)
@NamedQuery(name=EnrolmentEntity.SELECT_ENROLMENT_BY_ENROLMENT_NUMBER_POOLID, query = EnrolmentEntity.JPQL_SELECT_ENROLMENT_BY_ENROLMENT_NUMBER_POOLID)
@NamedQuery(name=EnrolmentEntity.SELECT_UNRELEASED_ENROLMENTS_FOR_SECURITY, query = EnrolmentEntity.JPQL_UNRELEASED_ENROLMENTS_FOR_SECURITY)
@NamedQuery(name=EnrolmentEntity.SELECT_ENROLMENT_FOR_SECURITY, query = EnrolmentEntity.JPQL_ENROLMENT_FOR_SECURITY)
@NamedQuery(name=EnrolmentEntity.SELECT_ENROLMENTS_BY_ENROLMENT_NUMBER, query = EnrolmentEntity.JPQL_SELECT_ENROLMENTS_BY_ENROLMENT_NUMBER)
@NamedQuery(name=EnrolmentEntity.SELECT_CURRENT_ENROLMENT_NUMBERS_IN, query = EnrolmentEntity.JPQL_SELECT_CURRENT_ENROLMENT_NUMBERS_IN)
@NamedQuery(name=EnrolmentEntity.SELECT_COUNT_CE_ENROLMENTS_FOR_POOL_ID, query = EnrolmentEntity.JQPL_COUNT_CE_ENROLMENTS_FOR_POOL_ID)

@Entity
@Table(name = "ENROLMENT")
@Audited(withModifiedFlag=true)
public class EnrolmentEntity  implements AuditedEntity{
	
	private static final long serialVersionUID = 9219495977520809883L;
	
	public static final String SELECT_ALL_ENROLMENT_NUMBERS_LIKE = "EnrolmentEntity.SELECT_ALL_ENROLMENT_NUMBERS_LIKE";	
	protected static final String JPQL_SELECT_ALL_ENROLMENT_NUMBERS_LIKE = "SELECT o.enrolmentNumber FROM EnrolmentEntity o where o.enrolmentNumber like ?1 ORDER BY o.enrolmentNumber ASC ";

	public static final String SELECT_CURRENT_ENROLMENT_NUMBERS_IN = "EnrolmentEntity.SELECT_CURRENT_ENROLMENT_NUMBERS_IN";
	protected static final String JPQL_SELECT_CURRENT_ENROLMENT_NUMBERS_IN = 
            "   SELECT DISTINCT(en.enrolmentNumber) FROM EnrolmentEntity en "
            + " INNER JOIN EnrolmentPoolEntity enPool ON enPool.enrolment.enrolmentNumber = en.enrolmentNumber"
            + " INNER JOIN PoolEntity pool ON pool.id = enPool.pool.id "
            + " INNER JOIN SecurityEntity o ON pool.id = o.pool.id and o.currentAmount > 0 "
            + " WHERE en.enrolmentNumber in (?1) ";


	public static final String SELECT_AVAILABLE_ENROLMENTS_BY_POOLID = "EnrolmentEntity.SELECT_AVAILABLE_ENROLMENTS_BY_POOLID";
	protected static final String JPQL_SELECT_AVAILABLE_ENROLMENTS_BY_POOLID =
			"SELECT e.enrolmentNumber as enrolmentNumber, e.enrollingVb.crmContactId as crmContactId " +
			"FROM EnrolmentEntity e INNER JOIN " +
				"VbPoolEntity vbPool ON (e.vendor = vbPool.vb OR e.builder = vbPool.vb) AND vbPool.vbDeleteReason is null " +
				"WHERE e.createDate > ?1 AND " +
					"e.enrolmentNumber NOT in (select ep.enrolment from EnrolmentPoolEntity ep) AND " +
					"vbPool.pool.id = ?2 AND " +
					"(e.condoConversion = true OR ?3 = false) AND " +
					"e.unitType in ?4 " +
			"ORDER BY e.enrolmentNumber DESC";

	public static final String SELECT_ENROLMENTS_BY_ENROLMENT_NUMBER = "EnrolmentEntity.SELECT_ENROLMENTS_BY_ENROLMENT_NUMBER";
	protected static final String JPQL_SELECT_ENROLMENTS_BY_ENROLMENT_NUMBER = "SELECT e FROM EnrolmentEntity e WHERE e.enrolmentNumber in ?1";

	public static final String SELECT_ENROLMENT_BY_ENROLMENT_NUMBER_POOLID = "SELECT_ENROLMENT_BY_ENROLMENT_NUMBER_POOLID";
	protected static final String JPQL_SELECT_ENROLMENT_BY_ENROLMENT_NUMBER_POOLID = "SELECT e FROM EnrolmentEntity e INNER JOIN VbPoolEntity vbPool ON e.vendor = vbPool.vb OR e.builder = vbPool.vb where e.enrolmentNumber = ?1 AND vbPool.pool.id = ?2 and e.unitType in ?3 ";

	public static final String SELECT_UNRELEASED_ENROLMENTS_FOR_SECURITY = "EnrolmentEntity.SELECT_UNRELEASED_ENROLMENTS_FOR_SECURITY";	
    protected static final String JPQL_UNRELEASED_ENROLMENTS_FOR_SECURITY = 
            "   SELECT enPool.enrolment FROM SecurityEntity o "
            + " INNER JOIN PoolEntity pool ON o.pool.id = pool.id "
            + " INNER JOIN EnrolmentPoolEntity enPool ON enPool.pool.id = pool.id "
            + " WHERE o.id = ?1 and enPool.enrolment NOT IN ( "
            + "  SELECT relEn.enrolment FROM ReleaseEnrolmentEntity relEn WHERE o = relEn.release.security AND relEn.fullRelease = TRUE AND (relEn.analystApprovalStatus = null OR relEn.analystApprovalStatus = com.tarion.vbs.common.enums.ReleaseApprovalStatusEnum.Authorized) AND (relEn.release.finalApprovalStatus IS NULL OR relEn.release.finalApprovalStatus = com.tarion.vbs.common.enums.ReleaseApprovalStatusEnum.Authorized)"
            + "    )";
    
    public static final String SELECT_ENROLMENT_FOR_SECURITY = "EnrolmentEntity.SELECT_ENROLMENT_FOR_SECURITY";	
    protected static final String JPQL_ENROLMENT_FOR_SECURITY = 
            "   SELECT enPool.enrolment FROM SecurityEntity o "
            + " INNER JOIN PoolEntity pool ON o.pool.id = pool.id "
            + " INNER JOIN EnrolmentPoolEntity enPool ON enPool.pool.id = pool.id "
            + " WHERE o.id = ?1 and enPool.enrolment.enrolmentNumber = ?2 ";

	public static final String SELECT_AUTO_RELEASE_SHORTLIST = "EnrolmentEntity.SELECT_AUTO_RELEASE_SHORTLIST";
	protected static final String JPQL_AUTO_RELEASE_SHORTLIST =
			"SELECT DISTINCT sec.ID as securityId, enrolment.ENROLMENT_NUMBER as enrolmentNumber, c.CRM_CONTACT_ID as vbNumber, timingType.ID as timingTypeId " +
			"FROM " +
				"SECURITY sec INNER JOIN " +
				"ENROLMENT_POOL ep ON sec.POOL_ID = ep.POOL_ID INNER JOIN " +
				"ENROLMENT enrolment ON ep.ENROLMENT_NUMBER = enrolment.ENROLMENT_NUMBER INNER JOIN " +
				"VB_POOL vbPool ON ep.POOL_ID = vbPool.POOL_ID AND vbPool.VB_DELETE_REASON_ID IS NULL INNER JOIN " +
				"CONTACT c ON c.ID = vbPool.VB_CONTACT_ID CROSS JOIN " +
				"AUTO_RELEASE_TIMING_TYPE timingType LEFT OUTER JOIN " +
				"AUTO_RELEASE_ENROLMENT are ON are.ENROLMENT_NUMBER = enrolment.ENROLMENT_NUMBER AND are.RELEASE_ID IS NOT NULL " +
			"WHERE " +
				"sec.SECURITY_PURPOSE_ID <> " + VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY  + " " +
				"AND (SELECT sec.CURRENT_AMOUNT - COALESCE(SUM(R.AUTHORIZED_AMOUNT), 0) FROM RELEASE R WHERE R.SECURITY_ID = sec.ID AND R.STATUS NOT IN ('INITIATED',  'COMPLETED', 'REJECTED', 'CANCELLED')) > 0 " +
				"AND (enrolment.EXCESS_DEPOSIT = 1 AND enrolment.EXCESS_DEPOSIT_RELEASE = 1 OR timingType.ID <> " + VbsConstants.AUTO_RELEASE_TIMING_TYPE_EXCESS_DEPOSIT + ")" +
				"AND (enrolment.AUTO_ONE_YEAR = 1 OR timingType.ID <> " + VbsConstants.AUTO_RELEASE_TIMING_TYPE_ONE_YEAR + ")" +
				"AND (enrolment.AUTO_ONE_YEAR_RFC = 1 AND are.ID IS NULL OR timingType.ID <> " + VbsConstants.AUTO_RELEASE_TIMING_TYPE_RFC + ")" +
				"AND (enrolment.AUTO_TWO_YEAR = 1 AND are.ID IS NULL OR timingType.ID <> " + VbsConstants.AUTO_RELEASE_TIMING_TYPE_TWO_YEAR + ")" +
				"AND datediff(dd,convert(smalldatetime,convert(varchar(10),enrolment.WARRANTY_START_DATE,121),121),getdate()) > timingType.NUMBER_OF_DAYS " +
				"AND datediff(dd,convert(smalldatetime,convert(varchar(10),sec.RECEIVED_DATE,121),121),getdate()) > timingType.NUMBER_OF_DAYS " +
				"AND datediff(dd,convert(smalldatetime,convert(varchar(10),enrolment.ENTRY_DATE,121),121),getdate()) > timingType.NUMBER_OF_DAYS " +
				"order by timingType.ID, sec.ID, enrolment.ENROLMENT_NUMBER";

	public static final String SELECT_COUNT_CE_ENROLMENTS_FOR_POOL_ID = "EnrolmentEntity.SELECT_COUNT_CE_ENROLMENTS_FOR_POOL_ID";
	protected static final String JQPL_COUNT_CE_ENROLMENTS_FOR_POOL_ID = "SELECT COUNT(ep) FROM EnrolmentPoolEntity ep WHERE ep.pool.id = ?1 AND ep.enrolment.homeCategory.id != 1";

	@Id
    @Column(name = "ENROLMENT_NUMBER")
	@Access(AccessType.PROPERTY)
	private String enrolmentNumber;

	@Version
    @Column(name = "VERSION")
	private Long version;
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "ADDRESS_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag=true)
	private AddressEntity address;
	
	@Column(name = "WS_FCR")
	private String wsFcr;
	
	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private EnrolmentStatus status;
	
	@Column(name = "CONDO_CORP_HO_NAME")
	private String condoCorpHoName;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "ENROLLING_VB_CONTACT_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag=true)
	private ContactEntity enrollingVb;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "VENDOR_CONTACT_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag=true)
	private ContactEntity vendor;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BUILDER_CONTACT_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag=true)
	private ContactEntity builder;
	
	@Column(name = "UNIT_TYPE")
	private String unitType;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "HOME_CATEGORY_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private HomeCategoryEntity homeCategory;
	
	@Column(name = "CONDO_CONVERSION")
	private boolean condoConversion;
	
	@Column(name = "WARRANTY_START_DATE")
	private LocalDateTime warrantyStartDate;	
	
	@Column(name = "ENTRY_DATE")
	private LocalDateTime entryDate;

	@Column(name = "EXCESS_DEPOSIT")
	private boolean excessDeposit;

	@Column(name = "EXCESS_DEPOSIT_RELEASE")
	private boolean excessDepositRelease;

	@Column(name = "AUTO_ONE_YEAR")
	private boolean autoOneYear;

	@Column(name = "AUTO_ONE_YEAR_RFC")
	private boolean autoOneYearRfc;

	@Column(name = "AUTO_TWO_YEAR")
	private boolean autoTwoYear;
	
	@Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	
	@Column(name = "CREATE_USER")
	private String createUser;
	
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;

	@Column(name = "UPDATE_USER")
	private String updateUser;

	@NotAudited
	@Formula("(CASE WHEN (SELECT COUNT(*) FROM ENROLMENT_POOL EP WHERE EP.ENROLMENT_NUMBER = ENROLMENT_NUMBER) = 1 THEN 0 ELSE 1 END)")
	private boolean multiSecurity;

	public EnrolmentEntity(){
		this.autoOneYear = true;
		this.autoOneYearRfc = true;
		this.autoTwoYear = true;
		this.excessDeposit = false;
		this.excessDepositRelease = false;
	}

	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}

	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}

	@Override
	public Long getVersion() {
		return version;
	}

	@Override
	public void setVersion(Long version) {
		this.version = version;
	}

	public AddressEntity getAddress() {
		return address;
	}

	public void setAddress(AddressEntity address) {
		this.address = address;
	}

	public String getWsFcr() {
		return wsFcr;
	}

	public void setWsFcr(String wsFcr) {
		this.wsFcr = wsFcr;
	}
	
	public EnrolmentStatus getStatus() {
		return status;
	}

	public void setStatus(EnrolmentStatus status) {
		this.status = status;
	}

	public String getCondoCorpHoName() {
		return condoCorpHoName;
	}

	public void setCondoCorpHoName(String condoCorpHoName) {
		this.condoCorpHoName = condoCorpHoName;
	}

	public ContactEntity getEnrollingVb() {
		return enrollingVb;
	}

	public void setEnrollingVb(ContactEntity enrollingVb) {
		this.enrollingVb = enrollingVb;
	}

	public ContactEntity getVendor() {
		return vendor;
	}

	public void setVendor(ContactEntity vendor) {
		this.vendor = vendor;
	}

	public ContactEntity getBuilder() {
		return builder;
	}

	public void setBuilder(ContactEntity builder) {
		this.builder = builder;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public HomeCategoryEntity getHomeCategory() {
		return homeCategory;
	}

	public void setHomeCategory(HomeCategoryEntity homeCategory) {
		this.homeCategory = homeCategory;
	}

	public boolean isCondoConversion() {
		return condoConversion;
	}

	public void setCondoConversion(boolean condoConversion) {
		this.condoConversion = condoConversion;
	}

	public LocalDateTime getWarrantyStartDate() {
		return warrantyStartDate;
	}

	public void setWarrantyStartDate(LocalDateTime warrantyStartDate) {
		this.warrantyStartDate = warrantyStartDate;
	}

	public LocalDateTime getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(LocalDateTime entryDate) {
		this.entryDate = entryDate;
	}

	public boolean isExcessDeposit() {
		return excessDeposit;
	}

	public void setExcessDeposit(boolean excessDeposit) {
		this.excessDeposit = excessDeposit;
	}

	public boolean isExcessDepositRelease() {
		return excessDepositRelease;
	}

	public void setExcessDepositRelease(boolean excessDepositRelease) {
		this.excessDepositRelease = excessDepositRelease;
	}

	public boolean isAutoOneYear() {
		return autoOneYear;
	}

	public void setAutoOneYear(boolean autoOneYear) {
		this.autoOneYear = autoOneYear;
	}

	public boolean isAutoOneYearRfc() {
		return autoOneYearRfc;
	}

	public void setAutoOneYearRfc(boolean autoOneYearRfc) {
		this.autoOneYearRfc = autoOneYearRfc;
	}

	public boolean isAutoTwoYear() {
		return autoTwoYear;
	}

	public void setAutoTwoYear(boolean autoTwoYear) {
		this.autoTwoYear = autoTwoYear;
	}

	@Override
	public LocalDateTime getCreateDate() {
		return createDate;
	}

	@Override
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	@Override
	public String getCreateUser() {
		return createUser;
	}

	@Override
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Override
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	@Override
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String getUpdateUser() {
		return updateUser;
	}

	@Override
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public boolean isMultiSecurity() {
		return multiSecurity;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		EnrolmentEntity that = (EnrolmentEntity) o;
		return condoConversion == that.condoConversion &&
				excessDeposit == that.excessDeposit &&
				excessDepositRelease == that.excessDepositRelease &&
				autoOneYear == that.autoOneYear &&
				autoOneYearRfc == that.autoOneYearRfc &&
				autoTwoYear == that.autoTwoYear &&
				enrolmentNumber.equals(that.enrolmentNumber) &&
				version.equals(that.version) &&
				address.equals(that.address) &&
				Objects.equals(wsFcr, that.wsFcr) &&
				status == that.status &&
				Objects.equals(condoCorpHoName, that.condoCorpHoName) &&
				Objects.equals(enrollingVb, that.enrollingVb) &&
				Objects.equals(vendor, that.vendor) &&
				Objects.equals(builder, that.builder) &&
				Objects.equals(unitType, that.unitType) &&
				Objects.equals(homeCategory, that.homeCategory) &&
				Objects.equals(warrantyStartDate, that.warrantyStartDate) &&
				Objects.equals(entryDate, that.entryDate) &&
				Objects.equals(createDate, that.createDate) &&
				Objects.equals(createUser, that.createUser) &&
				Objects.equals(updateDate, that.updateDate) &&
				Objects.equals(updateUser, that.updateUser);
	}

	@Override
	public int hashCode() {
		return Objects.hash(enrolmentNumber, version, address, wsFcr, condoCorpHoName, enrollingVb, vendor, builder, unitType, homeCategory, condoConversion, warrantyStartDate, entryDate, excessDeposit, excessDepositRelease, autoOneYear, autoOneYearRfc, autoTwoYear, createDate, createUser, updateDate, updateUser);
	}

	@Override
	public String toString() {
		return "EnrolmentEntity{" +
				"enrolmentNumber='" + enrolmentNumber + '\'' +
				", version=" + version +
				", address=" + address +
				", wsFcr='" + wsFcr + '\'' +
				", status='" + status + '\'' +
				", condoCorpHoName='" + condoCorpHoName + '\'' +
				", enrollingVb=" + enrollingVb +
				", vendor=" + vendor +
				", builder=" + builder +
				", unitType='" + unitType + '\'' +
				", homeCategory=" + homeCategory +
				", condoConversion=" + condoConversion +
				", warrantyStartDate=" + warrantyStartDate +
				", entryDate=" + entryDate +
				", excessDeposit=" + excessDeposit +
				", excessDepositRelease=" + excessDepositRelease +
				", autoOneYear=" + autoOneYear +
				", autoOneYearRfc=" + autoOneYearRfc +
				", autoTwoYear=" + autoTwoYear +
				", createDate=" + createDate +
				", createUser='" + createUser + '\'' +
				", updateDate=" + updateDate +
				", updateUser='" + updateUser + '\'' +
				", multiSecurity=" + multiSecurity +
				'}';
	}
}
