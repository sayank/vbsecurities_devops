package com.tarion.vbs.orm.entity.converters;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class LocalDateTimeConverter implements AttributeConverter<LocalDateTime, Date> {

    @Override
    public Date convertToDatabaseColumn(LocalDateTime date) {
        Instant instant = Instant.from(date);
        return Date.from(instant);
    }

    @Override
    public LocalDateTime convertToEntityAttribute(Date value) {
    	ZoneId zone = ZoneId.of("America/Toronto");
		ZonedDateTime i = value.toInstant().atZone(zone).withZoneSameInstant(ZoneId.systemDefault());
		return LocalDateTime.from(i);
    }
}
