package com.tarion.vbs.orm.entity.enrolment;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.envers.Audited;

import com.tarion.vbs.orm.entity.AuditedEntity;
import com.tarion.vbs.orm.entity.security.PoolEntity;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * Enrolment Pool Entity JPA class holds Enrolment Pool information
 *
 * @author <A href="shadi.kajevand@tarion.com">Shadi Kajevand</A>
 * @since 2018-11-26
 * @version 1.0
 */
@NamedQuery(name=EnrolmentPoolEntity.SELECT_ENROLMENT_POOL_BY_ENROLMENT_NUMBER, query = EnrolmentPoolEntity.JPQL_SELECT_ENROLMENT_POOL_BY_ENROLMENT_NUMBER)
@NamedQuery(name=EnrolmentPoolEntity.SELECT_MULTI_SECURITY, query = EnrolmentPoolEntity.JPQL_MULTI_SECURITY)
@NamedQuery(name=EnrolmentPoolEntity.SELECT_NUMBER_OF_SECURITY_BY_ENROLMENT_NUMBER, query = EnrolmentPoolEntity.JPQL_SELECT_NUMBER_OF_SECURITY_BY_ENROLMENT_NUMBER)
@NamedQuery(name=EnrolmentPoolEntity.SELECT_SECURITY_BY_ENROLMENT_NUMBER, query = EnrolmentPoolEntity.JPQL_SELECT_SECURITY_BY_ENROLMENT_NUMBER)
@NamedQuery(name=EnrolmentPoolEntity.SELECT_POOL_UNITS_TO_COVER_BY_ENROLMENT_NUMBER, query = EnrolmentPoolEntity.JPQL_SELECT_POOL_UNITS_TO_COVER_BY_ENROLMENT_NUMBER)
@NamedNativeQuery(name=EnrolmentPoolEntity.SELECT_ENROLMENT_SECURITY_DATA, query = EnrolmentPoolEntity.JPQL_SELECT_ENROLMENT_SECURITY_DATA)
@NamedNativeQuery(name=EnrolmentPoolEntity.SELECT_CE_ENROLMENTS_SAME_POOL, query = EnrolmentPoolEntity.JPQL_SELECT_CE_ENROLMENTS_SAME_POOL)
@NamedQuery(name=EnrolmentPoolEntity.SELECT_ENROLMENT_NUMBERS_BY_POOL_ID, query = EnrolmentPoolEntity.JPQL_SELECT_ENROLMENT_NUMBERS_BY_POOL_ID)
@NamedQuery(name=EnrolmentPoolEntity.SELECT_ENROLMENT_POOL_BY_POOL_ID_AND_VB_NUMBER, query = EnrolmentPoolEntity.JPQL_SELECT_ENROLMENT_POOL_BY_POOL_ID_AND_VB_NUMBER)

@Entity
@Table(name = "ENROLMENT_POOL")
@Audited(withModifiedFlag=true)
public class EnrolmentPoolEntity implements AuditedEntity {

	private static final long serialVersionUID = -5856549398707715169L;
	public static final String SELECT_ENROLMENT_BY_POOL_ID = "SELECT EP.* FROM ENROLMENT_POOL EP WHERE EP.POOL_ID = ?1 ORDER BY CAST(REPLACE(EP.ENROLMENT_NUMBER, 'H','') as integer) DESC";


	public static final String SELECT_ENROLMENT_NUMBERS_BY_POOL_ID = "EnrolmentPoolEntity.SELECT_ENROLMENT_NUMBERS_BY_POOL_ID";
	protected static final String JPQL_SELECT_ENROLMENT_NUMBERS_BY_POOL_ID = "SELECT o.enrolment.enrolmentNumber FROM EnrolmentPoolEntity o WHERE o.pool.id = ?1";

	public static final String SELECT_ENROLMENT_POOL_BY_POOL_ID_AND_VB_NUMBER = "EnrolmentPoolEntity.SELECT_ENROLMENT_POOL_BY_POOL_ID_AND_VB_NUMBER";
	protected static final String JPQL_SELECT_ENROLMENT_POOL_BY_POOL_ID_AND_VB_NUMBER = "SELECT o FROM EnrolmentPoolEntity o WHERE o.pool.id = ?1 AND o.enrolment.enrollingVb.crmContactId = ?2";

	public static final String SELECT_ENROLMENT_POOL_BY_ENROLMENT_NUMBER = "EnrolmentPoolEntity.SELECT_ENROLMENT_POOL_ID_BY_ENROLMENT_NUMBER";	
	protected static final String JPQL_SELECT_ENROLMENT_POOL_BY_ENROLMENT_NUMBER = "SELECT o FROM EnrolmentPoolEntity o where o.enrolment.enrolmentNumber in ?1 AND o.pool.id = ?2 ";
	
	public static final String SELECT_MULTI_SECURITY= "EnrolmentPoolEntity.SELECT_MULTI_SECURITY";
	protected static final String JPQL_MULTI_SECURITY = "SELECT count(o) FROM EnrolmentPoolEntity o where o.enrolment.enrolmentNumber = ?1 ";

	public static final String SELECT_NUMBER_OF_SECURITY_BY_ENROLMENT_NUMBER = "EnrolmentPoolEntity.SELECT_NUMBER_OF_SECURITY_BY_ENROLMENT_NUMBER";	
	protected static final String JPQL_SELECT_NUMBER_OF_SECURITY_BY_ENROLMENT_NUMBER = "SELECT count(s) FROM EnrolmentPoolEntity o, SecurityEntity s where o.enrolment.enrolmentNumber = ?1 AND o.pool.id = s.pool.id";

	public static final String SELECT_SECURITY_BY_ENROLMENT_NUMBER = "EnrolmentPoolEntity.SELECT_SECURITY_BY_ENROLMENT_NUMBER";	
	protected static final String JPQL_SELECT_SECURITY_BY_ENROLMENT_NUMBER = "SELECT s FROM EnrolmentPoolEntity o, SecurityEntity s where o.enrolment.enrolmentNumber = ?1 AND o.pool.id = s.pool.id";

	public static final String SELECT_POOL_UNITS_TO_COVER_BY_ENROLMENT_NUMBER = "EnrolmentPoolEntity.SELECT_POOL_UNITS_TO_COVER_BY_ENROLMENT_NUMBER";	
	protected static final String JPQL_SELECT_POOL_UNITS_TO_COVER_BY_ENROLMENT_NUMBER = "SELECT sum(o.pool.unitsToCover) FROM EnrolmentPoolEntity o, SecurityEntity s where o.enrolment.enrolmentNumber = ?1 "
			+ " and s.pool.id = o.pool.id and s.currentAmount > 0 ";
	
	public static final String SELECT_ENROLMENT_SECURITY_DATA = "EnrolmentPoolEntity.SELECT_ENROLMENT_SECURITY_DATA";
	protected static final String JPQL_SELECT_ENROLMENT_SECURITY_DATA =
			  "SELECT DISTINCT e.ENROLMENT_NUMBER as enrolmentNumber, "
			+ "(SELECT COUNT(*) FROM ENROLMENT_POOL ep WHERE ep.ENROLMENT_NUMBER = e.ENROLMENT_NUMBER) as poolCount, "
			+ "(SELECT ISNULL(SUM(pool.UNITS_TO_COVER),0) FROM ENROLMENT_POOL ep INNER JOIN SECURITY s ON ep.POOL_ID = s.POOL_ID INNER JOIN POOL pool ON ep.POOL_ID = pool.ID WHERE ep.ENROLMENT_NUMBER = e.ENROLMENT_NUMBER) as unitsToCover, "
			+ "(SELECT COUNT(1) FROM ENROLMENT_POOL ep INNER JOIN SECURITY s ON ep.POOL_ID = s.POOL_ID INNER JOIN POOL p ON ep.POOL_ID = p.ID WHERE ep.ENROLMENT_NUMBER = e.ENROLMENT_NUMBER AND s.CURRENT_AMOUNT > 0) as enrolmentCountWithCurrentAmount, "
			+ "ISNULL((SELECT TOP 1 p.UNITS_TO_COVER FROM ENROLMENT_POOL ep INNER JOIN SECURITY s ON ep.POOL_ID = s.POOL_ID INNER JOIN POOL p ON ep.POOL_ID = p.ID WHERE ep.ENROLMENT_NUMBER = e.ENROLMENT_NUMBER AND s.CURRENT_AMOUNT > 0 ORDER BY p.UNITS_TO_COVER), 0) as unitsToCoverWithCurrentAmount "
			+ "FROM ENROLMENT e "
			+ "WHERE e.ENROLMENT_NUMBER in ?1 ";

	public static final String SELECT_CE_ENROLMENTS_SAME_POOL= "EnrolmentPoolEntity.SELECT_CE_ENROLMENTS_SAME_POOL";
	protected static final String JPQL_SELECT_CE_ENROLMENTS_SAME_POOL =
			  "SELECT DISTINCT e.ENROLMENT_NUMBER as enrolmentNumber, ep.ENROLMENT_NUMBER as poolEnrolmentNumber "
			+ "FROM ENROLMENT e CROSS JOIN ENROLMENT_POOL ep "
			+ "WHERE ep.POOL_ID IN (SELECT ep1.POOL_ID FROM ENROLMENT_POOL ep1 INNER JOIN ENROLMENT ce1 ON ce1.ENROLMENT_NUMBER = ep1.ENROLMENT_NUMBER AND ce1.UNIT_TYPE = '001' WHERE ep1.ENROLMENT_NUMBER = e.ENROLMENT_NUMBER) "
			+ "AND e.ENROLMENT_NUMBER in ?1 ";
		
	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;
    
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "POOL_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
    private PoolEntity pool; 

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "ENROLMENT_NUMBER")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
    private EnrolmentEntity enrolment; 
	
	@Column(name = "RA_AMOUNT")
	private BigDecimal raAmount;
	
	@Column(name = "MAX_UNIT_DEPOSIT")
	private BigDecimal maxUnitDeposit;

	@Column(name = "INVENTORY_AMOUNT")
	private BigDecimal inventoryAmount;
	
	@Column(name = "EXCESS_AMOUNT")
	private BigDecimal excessAmount;
	
	@Column(name = "ALLOCATED_AMOUNT")
	private BigDecimal allocatedAmount;

	@Column(name = "CURRENT_ALLOCATED_AMOUNT")
	private BigDecimal currentAllocatedAmount;

	@Column(name = "PEF_AMOUNT")
	private BigDecimal pefAmount;
	
	@Column(name = "CLEARANCE_LETTER_ISSUED")
	private boolean clearanceLetterIssued;
	
	@Column(name = "CLEARANCE_DATE")
    private LocalDateTime clearanceDate;

	@Column(name = "CREATE_DATE")
    private LocalDateTime createDate;
	
	@Column(name = "CREATE_USER")
	private String createUser;
	
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	
	@Column(name = "UPDATE_USER")
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public PoolEntity getPool() {
		return pool;
	}

	public void setPool(PoolEntity pool) {
		this.pool = pool;
	}

	public EnrolmentEntity getEnrolment() {
		return enrolment;
	}

	public void setEnrolment(EnrolmentEntity enrolment) {
		this.enrolment = enrolment;
	}

	public BigDecimal getRaAmount() {
		return raAmount;
	}

	public void setRaAmount(BigDecimal raAmount) {
		this.raAmount = raAmount;
	}

	public BigDecimal getMaxUnitDeposit() {
		return maxUnitDeposit;
	}

	public void setMaxUnitDeposit(BigDecimal maxUnitDeposit) {
		this.maxUnitDeposit = maxUnitDeposit;
	}


	public BigDecimal getInventoryAmount() {
		return inventoryAmount;
	}

	public void setInventoryAmount(BigDecimal inventoryAmount) {
		this.inventoryAmount = inventoryAmount;
	}

	public BigDecimal getExcessAmount() {
		return excessAmount;
	}

	public void setExcessAmount(BigDecimal excessAmount) {
		this.excessAmount = excessAmount;
	}

	public BigDecimal getAllocatedAmount() {
		return allocatedAmount;
	}

	public void setAllocatedAmount(BigDecimal allocatedAmount) {
		this.allocatedAmount = allocatedAmount;
	}
	
	public BigDecimal getCurrentAllocatedAmount() {
		return currentAllocatedAmount;
	}

	public void setCurrentAllocatedAmount(BigDecimal currentAllocatedAmount) {
		this.currentAllocatedAmount = currentAllocatedAmount;
	}

	public BigDecimal getPefAmount() {
		return pefAmount;
	}

	public void setPefAmount(BigDecimal pefAmount) {
		this.pefAmount = pefAmount;
	}

	public boolean isClearanceLetterIssued() {
		return clearanceLetterIssued;
	}

	public void setClearanceLetterIssued(boolean clearanceLetterIssued) {
		this.clearanceLetterIssued = clearanceLetterIssued;
	}

	public LocalDateTime getClearanceDate() {
		return clearanceDate;
	}

	public void setClearanceDate(LocalDateTime clearanceDate) {
		this.clearanceDate = clearanceDate;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
}
