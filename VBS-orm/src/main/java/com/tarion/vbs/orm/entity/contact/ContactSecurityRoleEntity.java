package com.tarion.vbs.orm.entity.contact;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Home Category Entity JPA class holds Home Category
 *
 * @author <A href="shadi.kajevand@tarion.com">Shadi Kajevand</A>
 * @since 2018-02-20
 * @version 1.0
 */

@NamedQuery(name=ContactSecurityRoleEntity.SELECT_ALL_CONTACT_SECURITY_ROLES, query = ContactSecurityRoleEntity.JPQL_ALL_CONTACT_SECURITY_ROLES)

@Entity
@Table(name = "CONTACT_SECURITY_ROLE")
public class ContactSecurityRoleEntity implements Serializable{

	private static final long serialVersionUID = -3309585306341730600L;
	
	public static final String SELECT_ALL_CONTACT_SECURITY_ROLES = "HomeCategoryEntity.SELECT_ALL_CONTACT_SECURITY_ROLES";
	static final String JPQL_ALL_CONTACT_SECURITY_ROLES = "SELECT o FROM ContactSecurityRoleEntity o";

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "NAME")
	private String name;
	@Column(name = "DESCRIPTION")
	private String description;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ContactSecurityRoleEntity that = (ContactSecurityRoleEntity) o;
		return Objects.equals(id, that.id) &&
				Objects.equals(name, that.name) &&
				Objects.equals(description, that.description);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, description);
	}

	@Override
	public String toString() {
		return "ContactSecurityRoleEntity{" +
				"id=" + id +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				'}';
	}

}
