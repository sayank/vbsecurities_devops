/* 
 * 
 * ReleaseReasonEntity.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.release;

import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.tarion.vbs.orm.entity.AuditedEntity;

/**
 * Release Reason Entity JPA class holds data about reasons for release
 * (M:N) table between Release and Reason Lookup
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-01-08
 * @version 1.0
 */
@NamedQuery(name=ReleaseReasonEntity.SELECT_REASONS_BY_RELEASE_ID, query = ReleaseReasonEntity.JPQL_REASONS_BY_RELEASE_ID)

@Entity
@Table(name = "RELEASE_REASON")
@Audited(withModifiedFlag=true)
public class ReleaseReasonEntity implements AuditedEntity {
	private static final long serialVersionUID = 1L;

	public static final String SELECT_REASONS_BY_RELEASE_ID = "ReleaseReasonEntity.SELECT_REASONS_BY_RELEASE_ID";	
	protected static final String JPQL_REASONS_BY_RELEASE_ID = "SELECT o FROM ReleaseReasonEntity o where o.release.id = ?1 order by o.id asc ";

	

	
	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;
   
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "RELEASE_ID")
	private ReleaseEntity release;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "REASON_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private ReasonEntity reason;
	
    @Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public ReleaseEntity getRelease() {
		return release;
	}

	public void setRelease(ReleaseEntity release) {
		this.release = release;
	}

	public ReasonEntity getReason() {
		return reason;
	}

	public void setReason(ReasonEntity reason) {
		this.reason = reason;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReleaseReasonEntity other = (ReleaseReasonEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ReleaseReasonEntity [id=" + id + ", version=" + version + ", createDate=" + createDate + ", createUser=" + createUser + ", updateDate=" + updateDate + ", updateUser="
				+ updateUser + "]";
	}
}
