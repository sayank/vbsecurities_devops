/* 
 * 
 * EscrowLicenseApplicationStatusEntity.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.contact;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Escrow License Application Status Lookup Entity JPA class holds all values of License Application Statuses of Escrow Agents
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-01-08
 * @version 1.0
 */

@NamedQuery(name=EscrowLicenseApplicationStatusEntity.SELECT_ALL_ESCROW_LICENSE_APPLICATION_STATUS, query = EscrowLicenseApplicationStatusEntity.JPQL_ALL_ESCROW_LICENSE_APPLICATION_STATUS)
@NamedQuery(name=EscrowLicenseApplicationStatusEntity.SELECT_ESCROW_LICENSE_APPLICATION_STATUS_FOR_NAME, query = EscrowLicenseApplicationStatusEntity.JPQL_ESCROW_LICENSE_APPLICATION_STATUS_FOR_NAME)
@Entity
@Table(name = "ESCROW_LICENSE_APPLICATION_STATUS")
public class EscrowLicenseApplicationStatusEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String SELECT_ALL_ESCROW_LICENSE_APPLICATION_STATUS = "EscrowLicenseApplicationStatusEntity.SELECT_ALL_ESCROW_LICENSE_APPLICATION_STATUS";	
	protected static final String JPQL_ALL_ESCROW_LICENSE_APPLICATION_STATUS = "SELECT o FROM EscrowLicenseApplicationStatusEntity o ORDER BY o.id ASC ";

	public static final String SELECT_ESCROW_LICENSE_APPLICATION_STATUS_FOR_NAME = "EscrowLicenseApplicationStatusEntity.SELECT_ALL_ESCROW_LICENSE_APPLICATION_STATUS_FOR_NAME";	
	protected static final String JPQL_ESCROW_LICENSE_APPLICATION_STATUS_FOR_NAME = "SELECT o FROM EscrowLicenseApplicationStatusEntity o WHERE o.name = ?1 ORDER BY o.id ASC ";

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
    @Column(name = "NAME")
    private String name; 
    @Column(name = "DESCRIPTION")
    private String description;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	} 
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EscrowLicenseApplicationStatusEntity other = (EscrowLicenseApplicationStatusEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "EscrowLicenseApplicationStatusEntity [id=" + id + ", name=" + name + ", description=" + description + "]";
	}

}
