/* 
 * 
 * VbEntity.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.security;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.orm.entity.AuditedEntity;
import com.tarion.vbs.orm.entity.contact.ContactEntity;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Vb Pool Entity JPA class holds Vb Pool information
 *
 * @author <A href="shadi.kajevand@tarion.com">Shadi Kajevand</A>
 * @since 2018-11-25
 * @version 1.0
 */

@NamedQuery(name=VbPoolEntity.SELECT_ALL_EVEN_DELETED_VBS, query = VbPoolEntity.JPQL_SELECT_ALL_EVEN_DELETED_VBS)
@NamedQuery(name=VbPoolEntity.SELECT_NOT_DELETED_VB, query = VbPoolEntity.JPQL_SELECT_NOT_DELETED_VB)
@NamedQuery(name=VbPoolEntity.SELECT_ALL_VBS_BY_POOLID, query = VbPoolEntity.JPQL_SELECT_ALL_VBS_BY_POOLID)
@NamedQuery(name=VbPoolEntity.SELECT_ALL_VB_NUMBERS_BY_POOLID, query = VbPoolEntity.JPQL_SELECT_ALL_VB_NUMBERS_BY_POOLID)
@NamedQuery(name=VbPoolEntity.SELECT_NUMBER_OF_VBS_BY_SECURITY_ID, query = VbPoolEntity.JPQL_SELECT_NUMBER_OF_VBS_BY_SECURITY_ID)
@NamedQuery(name=VbPoolEntity.SELECT_ALL_DELETED_VBS_BY_POOLID, query = VbPoolEntity.JPQL_SELECT_ALL_DELETED_VBS_BY_POOLID)
@NamedQuery(name=VbPoolEntity.SELECT_POOL_BY_BLANKET_VB_ID, query= VbPoolEntity.JPQL_POOL_BY_BLANKET_VB_ID)

@Entity
@Table(name = "VB_POOL")
@Audited(withModifiedFlag=true)
public class VbPoolEntity implements AuditedEntity {
	private static final long serialVersionUID = 1L;

	public static final String SELECT_ALL_EVEN_DELETED_VBS = "VbPoolEntity.SELECT_ALL_EVEN_DELETED_VBS";	
	static final String JPQL_SELECT_ALL_EVEN_DELETED_VBS = "SELECT o FROM VbPoolEntity o where o.pool.id = ?1 ";

	public static final String SELECT_NOT_DELETED_VB = "VbPoolEntity.SELECT_NOT_DELETED_VB";
	static final String JPQL_SELECT_NOT_DELETED_VB = "SELECT o FROM VbPoolEntity o where o.pool.id = ?1 and o.vb.crmContactId = ?2  and o.vbDeleteReason is null ";

	public static final String SELECT_ALL_VBS_BY_POOLID = "VbPoolEntity.SELECT_ALL_VBS_BY_POOLID";	
	static final String JPQL_SELECT_ALL_VBS_BY_POOLID = "SELECT o FROM VbPoolEntity o where o.pool.id = ?1 and o.vbDeleteReason is null";

	public static final String SELECT_ALL_DELETED_VBS_BY_POOLID = "VbPoolEntity.SELECT_ALL_DELETED_VBS_BY_POOLID";
	static final String JPQL_SELECT_ALL_DELETED_VBS_BY_POOLID = "SELECT o FROM VbPoolEntity o where o.pool.id = ?1 and o.vbDeleteReason is not null ORDER BY o.updateDate DESC";

	public static final String SELECT_ALL_VB_NUMBERS_BY_POOLID = "VbPoolEntity.SELECT_ALL_VB_NUMBERS_BY_POOLID";
	static final String JPQL_SELECT_ALL_VB_NUMBERS_BY_POOLID = "SELECT o.vb.crmContactId FROM VbPoolEntity o where o.pool.id = ?1 and o.vbDeleteReason is null";

	public static final String SELECT_NUMBER_OF_VBS_BY_SECURITY_ID = "VbPoolEntity.SELECT_NUMBER_OF_VBS_BY_SECURITY_ID";	
	static final String JPQL_SELECT_NUMBER_OF_VBS_BY_SECURITY_ID = "SELECT count(vb) FROM VbPoolEntity vb, SecurityEntity s where s.id = ?1 AND s.deleted = FALSE AND vb.pool.id = s.pool.id and vb.vbDeleteReason is null";

	public static final String SELECT_POOL_BY_BLANKET_VB_ID = "VbPoolEntity.SELECT_POOL_BY_BLANKET_VB_ID";
	static final String JPQL_POOL_BY_BLANKET_VB_ID = "SELECT vbp.pool.id FROM VbPoolEntity vbp JOIN SecurityEntity s ON s.pool.id = vbp.pool.id AND s.securityPurpose.id = 2 AND s.currentAmount > 0 WHERE vbp.vb.id = ?1 AND vbp.vbDeleteReason IS NULL AND (s.identityType.id is null OR s.identityType.id <> " + VbsConstants.IDENTITY_TYPE_FREEHOLD + ")" ;

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;
    
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VB_CONTACT_ID")
	private ContactEntity vb;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "POOL_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
    private PoolEntity pool; 

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VB_DELETE_REASON_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
    private VbDeleteReasonEntity vbDeleteReason; 

	@Column(name = "CREATE_DATE")
    private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ContactEntity getVb() {
		return vb;
	}
	public void setVb(ContactEntity vb) {
		this.vb = vb;
	}
	public PoolEntity getPool() {
		return pool;
	}
	public void setPool(PoolEntity pool) {
		this.pool = pool;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public VbDeleteReasonEntity getVbDeleteReason() {
		return vbDeleteReason;
	}
	public void setVbDeleteReason(VbDeleteReasonEntity vbDeleteReason) {
		this.vbDeleteReason = vbDeleteReason;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VbPoolEntity other = (VbPoolEntity) obj;
		if (id == null) {
			return other.id == null;
		} else return id.equals(other.id);
	}
	
	@Override
	public String toString() {
		return "VbEntity [id=" + id +
				"VbDeleteReason=" + vbDeleteReason + "]";
	}	

}
