package com.tarion.vbs.orm.entity.enrolment;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Home Category Entity JPA class holds Home Category
 *
 * @author <A href="shadi.kajevand@tarion.com">Shadi Kajevand</A>
 * @since 2018-02-20
 * @version 1.0
 */

@NamedQuery(name=HomeCategoryEntity.SELECT_ALL_HOME_CATEGORYS, query = HomeCategoryEntity.JPQL_HOME_CATEGORYS)
@Entity
@Table(name = "HOME_CATEGORY")
public class HomeCategoryEntity implements Serializable{

	private static final long serialVersionUID = 546026582596634115L;
	
	public static final String SELECT_ALL_HOME_CATEGORYS = "HomeCategoryEntity.SELECT_ALL_HOME_CATEGORYS";
	protected static final String JPQL_HOME_CATEGORYS = "SELECT o FROM HomeCategoryEntity o";
	

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "NAME")
	private String name;
	@Column(name = "DESCRIPTION")
	private String description;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HomeCategoryEntity other = (HomeCategoryEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "HomeCategoryEntity [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
	
	
}
