package com.tarion.vbs.orm.entity.alert;

import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.envers.Audited;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.enums.AlertStatus;
import com.tarion.vbs.orm.entity.AuditedEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * Alert Entity JPA class holds alert informations for security
 *
 * @author <A href="djordje.przulj@tarion.com">Djordje Przulj</A>
 * @since 2019-04-16
 * @version 1.0
 */
@NamedQuery(name=AlertEntity.SELECT_ALERT_FOR_SECURITY_ID, query = AlertEntity.JPQL_ALERT_FOR_SECURITY_ID)
@NamedQuery(name=AlertEntity.SELECT_ALERTS_FOR_SECURITY_ID_EXCEPT_NOTE, query = AlertEntity.JPQL_ALERTS_FOR_SECURITY_ID_EXCEPT_NOTE)
@Entity
@Table(name="ALERT")
@Audited(withModifiedFlag=true)
public class AlertEntity implements AuditedEntity {

	private static final long serialVersionUID = 1L;

	public static final String SELECT_ALERT_FOR_SECURITY_ID = "AlertEntity.SELECT_ALERT_FOR_SECURITY_ID";
	static final String JPQL_ALERT_FOR_SECURITY_ID = "SELECT o FROM AlertEntity o WHERE o.security.id = ?1  ORDER BY o.startDate ";
	
	public static final String SELECT_ALERTS_FOR_SECURITY_ID_EXCEPT_NOTE = "AlertEntity.SELECT_ALERTS_FOR_SECURITY_ID_EXCEPT_NOTE";
	static final String JPQL_ALERTS_FOR_SECURITY_ID_EXCEPT_NOTE = "SELECT o FROM AlertEntity o WHERE o.security.id = ?1 AND o.alertType.id !=" + VbsConstants.ALERT_TYPE_NOTE + " ORDER BY o.startDate ";

	
	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;

	@Version
    @Column(name = "VERSION")
	private Long version;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SECURITY_ID")
	private SecurityEntity security;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ALERT_TYPE_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private AlertTypeEntity alertType;
	
	@Column(name = "NAME")
	private String name;

	@Column(name = "DESCRIPTION")
	private String description;
	
    @Column(name = "START_DATE")
	private LocalDateTime startDate;
    
    @Column(name = "END_DATE")
	private LocalDateTime endDate;

    @Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
    
	@Column(name = "CREATE_USER")
	private String createUser;
	
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	
	@Column(name = "UPDATE_USER")
	private String updateUser;

	@Transient
	public AlertStatus getStatus() {
		if (this.endDate == null) {
			return AlertStatus.ACTIVE;
		} else
			return AlertStatus.INACTIVE;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public SecurityEntity getSecurity() {
		return security;
	}

	public void setSecurity(SecurityEntity security) {
		this.security = security;
	}
	
	public AlertTypeEntity getAlertType() {
		return alertType;
	}

	public void setAlertType(AlertTypeEntity alertType) {
		this.alertType = alertType;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlertEntity other = (AlertEntity) obj;
		if (id == null) {
			return other.id == null;
		} else {
			if ((this.name != null && (other.name == null || !this.name.equals(other.name))) 
					|| (this.name == null && other.name != null)) {
				return false;
			} else if ((this.description != null && (other.description == null || !this.description.equals(other.description)))
					|| (this.description == null && other.description != null)) {
				return false;
			} else if ((this.alertType != null && (other.alertType == null || !this.alertType.equals(other.alertType)))
					|| (this.alertType == null && other.alertType != null)) {
				return false;				
			} else if ((this.createDate != null && (other.createDate == null || !this.createDate.equals(other.createDate)))
					|| (this.createDate == null && other.createDate != null)) {
				return false;
			} else if ((this.endDate != null && (other.endDate == null || !this.endDate.equals(other.endDate)))
					|| (this.endDate == null && other.endDate != null)) {
				return false;
			} else {
				return true;
			}
		}
	}

	@Override
	public String toString() {
		return "AlertEntity [id=" + id + ", version=" + version  
				+ ", name=" + name + ", description=" + description + ", startDate=" + startDate 
				+ ", endDate=" + endDate
				+ ", createDate=" + createDate + ", createUser=" + createUser 
				+ ", updateDate=" + updateDate + ", updateUser=" + updateUser + "]";
	}

}
