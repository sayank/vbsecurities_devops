package com.tarion.vbs.orm.entity.release;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Release Type Entity JPA class holds diferent types of Release
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-03-15
 * @version 1.0
 */
@NamedQuery(name=ReleaseTypeEntity.SELECT_ALL_RELEASE_TYPES, query = ReleaseTypeEntity.JPQL_ALL_RELEASE_TYPES)
@Entity
@Table(name = "RELEASE_TYPE")
public class ReleaseTypeEntity implements Serializable{

	private static final long serialVersionUID = 1l;

	public static final String SELECT_ALL_RELEASE_TYPES = "ReleaseTypeEntity.SELECT_ALL_RELEASE_TYPES";	
	protected static final String JPQL_ALL_RELEASE_TYPES = "SELECT o FROM ReleaseTypeEntity o ORDER BY o.id ASC ";

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "NAME")
	private String name;
	@Column(name = "DESCRIPTION")
	private String description;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReleaseTypeEntity other = (ReleaseTypeEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "ReleaseTypeEntity [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
	
	
}
