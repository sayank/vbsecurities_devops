/* 
 * 
 * ReleaseEnrolmentEntity.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.release;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.*;

import com.tarion.vbs.common.enums.ReleaseApprovalStatusEnum;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.tarion.vbs.orm.entity.AuditedEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;

/**
 * Release Enorlment Entity JPA class holds data about releases per enrolment
 * (M:N) table between Release and Enrolment tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-01-08
 * @version 1.0
 */

@NamedQuery(name=ReleaseEnrolmentEntity.SELECT_ENROLMENTS_BY_RELEASE_ID, query = ReleaseEnrolmentEntity.JPQL_SELECT_ENROLMENTS_BY_RELEASE_ID)
@NamedNativeQuery(name=ReleaseEnrolmentEntity.SELECT_ENROLMENT_NUMBERS_BY_RELEASE_ID, query = ReleaseEnrolmentEntity.JPQL_SELECT_ENROLMENT_NUMBERS_BY_RELEASE_ID)
@NamedQuery(name=ReleaseEnrolmentEntity.SELECT_RELEASES_BY_ENROLMENT_NUMBER, query = ReleaseEnrolmentEntity.JPQL_RELEASES_BY_ENROLMENT_NUMBER)
@NamedQuery(name=ReleaseEnrolmentEntity.SELECT_ENROLMENTS_BY_SECURITY_ID, query = ReleaseEnrolmentEntity.JPQL_SELECT_ENROLMENTS_BY_SECURITY_ID)

@Entity
@Table(name = "RELEASE_ENROLMENT")
@Audited(withModifiedFlag=true)
public class ReleaseEnrolmentEntity implements AuditedEntity {
	private static final long serialVersionUID = 1L;

	public static final String SELECT_ENROLMENTS_BY_RELEASE_ID = "ReleaseEnrolmentEntity.SELECT_ENROLMENTS_BY_RELEASE_ID";	
	protected static final String JPQL_SELECT_ENROLMENTS_BY_RELEASE_ID = "SELECT o FROM ReleaseEnrolmentEntity o where o.release.id = ?1";

	public static final String SELECT_ENROLMENT_NUMBERS_BY_RELEASE_ID = "ReleaseEnrolmentEntity.SELECT_ENROLMENT_NUMBERS_BY_RELEASE_ID";
	protected static final String JPQL_SELECT_ENROLMENT_NUMBERS_BY_RELEASE_ID = "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED SELECT e.ENROLMENT_NUMBER FROM RELEASE_ENROLMENT e where e.RELEASE_ID = ?1";

	public static final String SELECT_ENROLMENTS_BY_SECURITY_ID = "ReleaseEnrolmentEntity.SELECT_ENROLMENTS_BY_SECURITY_ID";
	protected static final String JPQL_SELECT_ENROLMENTS_BY_SECURITY_ID = "SELECT o FROM ReleaseEnrolmentEntity o where o.release.security.id = ?1";

	public static final String SELECT_RELEASES_BY_ENROLMENT_NUMBER = "ReleaseEnrolmentEntity.SELECT_RELEASES_BY_ENROLMENT_NUMBER";	
	protected static final String JPQL_RELEASES_BY_ENROLMENT_NUMBER = "SELECT o FROM ReleaseEnrolmentEntity o where o.enrolment.enrolmentNumber = ?1";


	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;
   
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "RELEASE_ID")
	private ReleaseEntity release;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "ENROLMENT_NUMBER")
	private EnrolmentEntity enrolment;
	
    @Column(name = "REQUESTED_AMOUNT")
	private BigDecimal requestedAmount;

    @Column(name = "AUTHORIZED_AMOUNT")
	private BigDecimal authorizedAmount;
    
    @Column(name = "ANALYST_APPR_BY")
    private String analystApprovalBy;
    @Column(name = "ANALYST_APPR_DATE")
    private LocalDateTime analystApprovalDate;
    @Column(name = "ANALYST_APPR_STATUS")
	@Enumerated(EnumType.STRING)
    private ReleaseApprovalStatusEnum analystApprovalStatus;
    //TODO remove
//    @Column(name = "ANALYST_APPR_REQUESTOR")
//    private String analystApprovalRequestor;

    @Column(name = "FULL_RELEASE")
    private Boolean fullRelease;
    
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "REJECT_REASON_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
    private RejectReasonEntity rejectReason;


    @Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getAnalystApprovalBy() {
		return analystApprovalBy;
	}

	public void setAnalystApprovalBy(String analystApprovalBy) {
		this.analystApprovalBy = analystApprovalBy;
	}

	public LocalDateTime getAnalystApprovalDate() {
		return analystApprovalDate;
	}

	public void setAnalystApprovalDate(LocalDateTime analystApprovalDate) {
		this.analystApprovalDate = analystApprovalDate;
	}

	public ReleaseApprovalStatusEnum getAnalystApprovalStatus() {
		return analystApprovalStatus;
	}

	public void setAnalystApprovalStatus(ReleaseApprovalStatusEnum analystApprovalStatus) {
		this.analystApprovalStatus = analystApprovalStatus;
	}

	//
//	public String getAnalystApprovalRequestor() {
//		return analystApprovalRequestor;
//	}
//
//	public void setAnalystApprovalRequestor(String analystApprovalRequestor) {
//		this.analystApprovalRequestor = analystApprovalRequestor;
//	}

	public RejectReasonEntity getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(RejectReasonEntity rejectReason) {
		this.rejectReason = rejectReason;
	}
	
	public ReleaseEntity getRelease() {
		return release;
	}

	public void setRelease(ReleaseEntity release) {
		this.release = release;
	}

	public EnrolmentEntity getEnrolment() {
		return enrolment;
	}

	public void setEnrolment(EnrolmentEntity enrolment) {
		this.enrolment = enrolment;
	}

	public BigDecimal getRequestedAmount() {
		return requestedAmount;
	}

	public void setRequestedAmount(BigDecimal requestedAmount) {
		this.requestedAmount = requestedAmount;
	}

	public BigDecimal getAuthorizedAmount() {
		return authorizedAmount;
	}

	public void setAuthorizedAmount(BigDecimal authorizedAmount) {
		this.authorizedAmount = authorizedAmount;
	}

	public Boolean getFullRelease() {
		return fullRelease;
	}

	public void setFullRelease(Boolean fullRelease) {
		this.fullRelease = fullRelease;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReleaseEnrolmentEntity other = (ReleaseEnrolmentEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ReleaseEnrolmentEntity [id=" + id + ", version=" + version + ", requestedAmount=" + requestedAmount + ", authorizedAmount=" + authorizedAmount + ", analystApprovalBy=" + analystApprovalBy + ", analystApprovalDate=" + analystApprovalDate + ", analystApprovalStatus=" + analystApprovalStatus
				+ ", fullRelease=" + fullRelease + ", createDate=" + createDate + ", createUser=" + createUser + ", updateDate=" + updateDate
				+ ", updateUser=" + updateUser + "]";
	}

}
