package com.tarion.vbs.orm.entity.autorelease;

import com.tarion.vbs.orm.entity.AuditedEntity;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.time.LocalDateTime;

@NamedQuery(name=AutoReleaseEliminationEntity.SELECT_ALL_BY_AUTO_RELEASE_ENROLMENT_ID, query = AutoReleaseEliminationEntity.JPQL_SELECT_ALL_BY_AUTO_RELEASE_ENROLMENT_ID)

@Entity
@Table(name = "AUTO_RELEASE_ELIMINATION")
@Audited(withModifiedFlag=true)
public class AutoReleaseEliminationEntity implements AuditedEntity {

	public static final String SELECT_ALL_BY_AUTO_RELEASE_ENROLMENT_ID = "AutoReleaseEliminationEntity.SELECT_ALL_BY_AUTO_RELEASE_ENROLMENT_ID";
	protected static final String JPQL_SELECT_ALL_BY_AUTO_RELEASE_ENROLMENT_ID = "SELECT e.autoReleaseCriteria.id FROM AutoReleaseEliminationEntity e where e.autoReleaseEnrolment.id = ?1";

	private static final long serialVersionUID = 1;

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;	
	
	@Version
    @Column(name = "VERSION")
	private Long version;
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "AUTO_RELEASE_ENROLMENT_ID")
	private AutoReleaseEnrolmentEntity autoReleaseEnrolment;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "AUTO_RELEASE_CRITERIA_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private AutoReleaseCriteriaEntity autoReleaseCriteria;
	
	@Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public AutoReleaseEnrolmentEntity getAutoReleaseEnrolment() { return autoReleaseEnrolment; }
	public void setAutoReleaseEnrolment(AutoReleaseEnrolmentEntity autoReleaseEnrolment) { this.autoReleaseEnrolment = autoReleaseEnrolment; }
	public AutoReleaseCriteriaEntity getAutoReleaseCriteria() { return autoReleaseCriteria; }
	public void setAutoReleaseCriteria(AutoReleaseCriteriaEntity autoReleaseCriteria) { this.autoReleaseCriteria = autoReleaseCriteria;}
	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
	@Override
	public String toString() {
		return "AutoReleaseEliminationEntity [id=" + id + ", version=" + version 
				+ ", createDate=" + createDate + ", createUser=" + createUser + ", updateDate=" + updateDate
				+ ", updateUser=" + updateUser + "]";
	}
}
