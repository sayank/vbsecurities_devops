/* 
 * 
 * InterestTransactionLogEntity.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Transaction Log Entity JPA class holds daily interest data sent to FMS
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-11-20
 * @version 1.0
 */

@NamedQuery(name=InterestTransactionLogEntity.SELECT_TRANSACTION_BY_TRACKING_NUMBER, query = InterestTransactionLogEntity.JPQL_TRANSACTION_BY_TRACKING_NUMBER)

@Entity
@Table(name = "INTEREST_TRANSACTION_LOG")
public class InterestTransactionLogEntity implements AuditedEntity {
	private static final long serialVersionUID = 1L;
	
	public static final String SELECT_TRANSACTION_BY_TRACKING_NUMBER = "InterestTransactionLogEntity.SELECT_TRANSACTION_BY_TRACKING_NUMBER";
	protected static final String JPQL_TRANSACTION_BY_TRACKING_NUMBER = "SELECT o FROM InterestTransactionLogEntity o WHERE o.trackingNumber = ?1 ORDER BY o.id DESC ";

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;

	@Column(name = "TRACKING_NUMBER")
	private String trackingNumber;

	@Column(name = "MESSAGE_NAME")
	private String messageName;
	
    @Column(name = "REQUEST")
	private String request;
    
    @Column(name = "RESPONSE")
	private String response;
    
    @Column(name = "STATUS")
	private String status;
    
    @Column(name = "ERROR_MESSAGE")
	private String errorMessage;
    
    @Column(name = "REQUEST_SENT_DATE")
    private LocalDateTime requestSendDate;

    @Column(name = "RESPONSE_RECEIVED_DATE")
    private LocalDateTime responseReceivedDate;

	@Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public String getMessageName() {
		return messageName;
	}

	public void setMessageName(String messageName) {
		this.messageName = messageName;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public LocalDateTime getRequestSendDate() {
		return requestSendDate;
	}

	public void setRequestSendDate(LocalDateTime requestSendDate) {
		this.requestSendDate = requestSendDate;
	}

	public LocalDateTime getResponseReceivedDate() {
		return responseReceivedDate;
	}

	public void setResponseReceivedDate(LocalDateTime responseReceivedDate) {
		this.responseReceivedDate = responseReceivedDate;
	}
	
	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	
}
