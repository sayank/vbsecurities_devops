package com.tarion.vbs.orm.entity.autorelease;

import java.time.LocalDateTime;

import javax.persistence.*;

import com.tarion.vbs.common.constants.VbsConstants;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.tarion.vbs.orm.entity.AuditedEntity;


@NamedQuery(name = AutoReleaseRunEntity.SELECT_RUN_IN_PROGRESS, query = AutoReleaseRunEntity.JPQL_SELECT_RUN_IN_PROGRESS)

@Entity
@Table(name = "AUTO_RELEASE_RUN")
@Audited(withModifiedFlag=true)
public class AutoReleaseRunEntity implements AuditedEntity {

	public static final String SELECT_RUN_IN_PROGRESS= "AutoReleaseRunEntity.SELECT_RUN_IN_PROGRESS";
	protected static final String JPQL_SELECT_RUN_IN_PROGRESS = "SELECT o FROM AutoReleaseRunEntity o where o.runStatus.id <> " + VbsConstants.AUTO_RELEASE_RUN_STATUS_FINAL_COMPLETE;

	private static final long serialVersionUID = 1594327867691398395L;

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;	
	@Version
    @Column(name = "VERSION")
	private Long version;
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "RUN_STATUS_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private AutoReleaseRunStatusEntity runStatus;	
	@Column(name = "ENROLMENTS_RECEIVED")
	private boolean enrolmentsReceived;
	@Column(name = "VBS_RECEIVED")
	private boolean vbsReceived;
	@Column(name = "VBA_RECEIVED")
	private boolean vbaReceived;	
	@Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public AutoReleaseRunStatusEntity getRunStatus() {
		return runStatus;
	}
	public void setRunStatus(AutoReleaseRunStatusEntity runStatus) {
		this.runStatus = runStatus;
	}
	public boolean isEnrolmentsReceived() {
		return enrolmentsReceived;
	}
	public void setEnrolmentsReceived(boolean enrolmentsReceived) {
		this.enrolmentsReceived = enrolmentsReceived;
	}
	public boolean isVbsReceived() {
		return vbsReceived;
	}
	public void setVbsReceived(boolean vbsReceived) {
		this.vbsReceived = vbsReceived;
	}
	public boolean isVbaReceived() {
		return vbaReceived;
	}
	public void setVbaReceived(boolean vbaReceived) {
		this.vbaReceived = vbaReceived;
	}
	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
	@Override
	public String toString() {
		return "AutoReleaseRunEntity [id=" + id + ", version=" + version
				+ ", enrolmentsReceived=" + enrolmentsReceived + ", vbsReceived=" + vbsReceived + ", vbaReceived="
				+ vbaReceived + ", createDate=" + createDate + ", createUser=" + createUser + ", updateDate="
				+ updateDate + ", updateUser=" + updateUser + "]";
	}
}
