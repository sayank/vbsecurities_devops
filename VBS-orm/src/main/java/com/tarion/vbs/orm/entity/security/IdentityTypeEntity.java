/* 
 * 
 * ApplicationEntity.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.security;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Identity Type Lookup Entity JPA class holds all Identity Types
 *
 * @author <A href="djordje.przulj@tarion.com">Djordje Przulj</A>
 * @since 2019-05-07
 * @version 1.0
 */

@NamedQuery(name=IdentityTypeEntity.SELECT_ALL_IDENTITY_TYPE, query = IdentityTypeEntity.JPQL_ALL_IDENTITY_TYPE)

@Entity
@Table(name = "IDENTITY_TYPE")
public class IdentityTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String SELECT_ALL_IDENTITY_TYPE = "IdentityTypeEntity.SELECT_ALL_IDENTITY_TYPE";	
	protected static final String JPQL_ALL_IDENTITY_TYPE = "SELECT o FROM IdentityTypeEntity o ORDER BY o.id ASC ";

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
    @Column(name = "NAME")
    private String name; 
    @Column(name = "DESCRIPTION")
    private String description;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	} 
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdentityTypeEntity other = (IdentityTypeEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "IdentityTypeEntity [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
    
}
