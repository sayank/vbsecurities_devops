/* 
 * 
 * ReleaseEntity.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.release;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.enums.ReleaseApprovalStatusEnum;
import org.hibernate.annotations.Formula;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.tarion.vbs.common.enums.ReleaseStatus;
import com.tarion.vbs.orm.entity.AuditedEntity;
import com.tarion.vbs.orm.entity.contact.ContactEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;

/**
 * Release Entity JPA class holds data about releases
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-01-08
 * @version 1.0
 */
@NamedQuery(name=ReleaseEntity.SELECT_RELEASES_FOR_SECURITY, query = ReleaseEntity.JPQL_RELEASES_FOR_SECURITY)
@NamedQuery(name=ReleaseEntity.SELECT_NUMBER_OF_RELEASES, query = ReleaseEntity.JPQL_SELECT_NUMBER_OF_RELEASES)
@NamedQuery(name=ReleaseEntity.SELECT_RELEASES_BY_STATUS, query = ReleaseEntity.JPQL_RELEASES_BY_STATUS)
@NamedQuery(name=ReleaseEntity.SELECT_RELEASES_BY_STATUSES, query = ReleaseEntity.JPQL_SELECT_RELEASES_BY_STATUSES)
@NamedQuery(name=ReleaseEntity.SELECT_RELEASES_BY_STATUSES_FROM_TO_DATE, query = ReleaseEntity.JPQL_RELEASES_BY_STATUSES_FROM_TO_DATE)
@NamedQuery(name=ReleaseEntity.SELECT_WARRANTY_FIELDS_BY_ENROLMENT_NUMBER, query = ReleaseEntity.JPQL_WARRANTY_FIELDS_BY_ENROLMENT_NUMBER)
@NamedQuery(name=ReleaseEntity.SELECT_PENDING_APPROVAL_RELEASES_BY_STATUS, query = ReleaseEntity.JPQL_SELECT_PENDING_APPROVAL_RELEASES_BY_STATUS)
@NamedQuery(name=ReleaseEntity.SELECT_RELEASE_BY_SECURITY_SEQUENCE, query = ReleaseEntity.JPQL_RELEASE_BY_SECURITY_SEQUENCE)
@NamedQuery(name=ReleaseEntity.SELECT_NUMBER_OF_COMPLETED_PRINCIPAL_RELEASES, query = ReleaseEntity.JPQL_SELECT_NUMBER_OF_COMPLETED_PRINCIPAL_RELEASES)
@NamedQuery(name=ReleaseEntity.SELECT_DEMAND_COLLECTOR_RELEASES_BY_STATUS_SECURITY_ID, query = ReleaseEntity.JPQL_SELECT_DEMAND_COLLECTOR_RELEASES_BY_STATUS_SECURITY_ID)
@NamedQuery(name=ReleaseEntity.SELECT_SECURITY_ID_RELEASE_ID_BY_STATUS, query = ReleaseEntity.JPQL_SELECT_SECURITY_ID_RELEASE_ID_BY_STATUS)

@Entity
@Table(name = "RELEASE")
@Audited(withModifiedFlag=true)
public class ReleaseEntity implements AuditedEntity {
	private static final long serialVersionUID = 2L;

	
	public static final String SELECT_RELEASES_FOR_SECURITY = "ReleaseEntity.SELECT_RELEASES_FOR_SECURITY";	
	protected static final String JPQL_RELEASES_FOR_SECURITY = "SELECT o FROM ReleaseEntity o where o.security.id = ?1 ORDER BY o.id desc ";

	public static final String SELECT_PENDING_APPROVAL_RELEASES_BY_STATUS = "ReleaseEntity.SELECT_NUMBER_OF_RELEASES_BY_STATUS";
	protected static final String JPQL_SELECT_PENDING_APPROVAL_RELEASES_BY_STATUS = "SELECT count(o.id) FROM ReleaseEntity o where o.security.id = ?1 AND o.status = ?2 AND o.finalApprovalSentTo is not null";

	public static final String SELECT_NUMBER_OF_RELEASES = "ReleaseEntity.SELECT_NUMBER_OF_RELEASES";	
	protected static final String JPQL_SELECT_NUMBER_OF_RELEASES = "SELECT count(o.id) FROM ReleaseEntity o where o.security.id = ?1";

	public static final String SELECT_NUMBER_OF_COMPLETED_PRINCIPAL_RELEASES = "ReleaseEntity.SELECT_NUMBER_OF_COMPLETED_PRINCIPAL_RELEASES";
	protected static final String JPQL_SELECT_NUMBER_OF_COMPLETED_PRINCIPAL_RELEASES = "SELECT count(o.id) FROM ReleaseEntity o WHERE o.security.id = ?1 AND o.principalAmount > 0 AND o.status = com.tarion.vbs.common.enums.ReleaseStatus.COMPLETED";
	
	public static final String SELECT_RELEASES_BY_STATUS = "ReleaseEntity.SELECT_RELEASES_BY_STATUS";	
	protected static final String JPQL_RELEASES_BY_STATUS = "SELECT o FROM ReleaseEntity o where o.status = ?1 AND o.releaseType.id IN (" + VbsConstants.RELEASE_TYPE_RELEASE + ", "+ VbsConstants.RELEASE_TYPE_REPLACE + ", " + VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR + ")";

	public static final String SELECT_SECURITY_ID_RELEASE_ID_BY_STATUS = "ReleaseEntity.SELECT_SECURITY_ID_RELEASE_ID_BY_STATUS";
	protected static final String JPQL_SELECT_SECURITY_ID_RELEASE_ID_BY_STATUS = "SELECT o.security.id as securityId, o.id as releaseId FROM ReleaseEntity o where o.status = ?1 AND o.releaseType.id IN ?2 AND o.security.id IN ?3";

	public static final String SELECT_DEMAND_COLLECTOR_RELEASES_BY_STATUS_SECURITY_ID = "ReleaseEntity.SELECT_DEMAND_COLLECTOR_RELEASES_BY_STATUS_SECURITY_ID";
	protected static final String JPQL_SELECT_DEMAND_COLLECTOR_RELEASES_BY_STATUS_SECURITY_ID = "SELECT count(o.id) FROM ReleaseEntity o WHERE o.status = ?1 AND o.security.id = ?2 AND o.releaseType.id IN (" + VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR + ")";

	public static final String SELECT_RELEASES_BY_STATUSES = "ReleaseEntity.SELECT_RELEASES_BY_STATUSES";
	protected static final String JPQL_SELECT_RELEASES_BY_STATUSES = "SELECT o FROM ReleaseEntity o where (o.status = ?1 OR o.status = ?2) AND o.releaseType.id IN (" + VbsConstants.RELEASE_TYPE_RELEASE + ", "+ VbsConstants.RELEASE_TYPE_REPLACE + ")";

	public static final String SELECT_RELEASES_BY_STATUSES_FROM_TO_DATE = "ReleaseEntity.SELECT_RELEASES_BY_STATUSES_FROM_TO_DATE";	
	protected static final String JPQL_RELEASES_BY_STATUSES_FROM_TO_DATE = "SELECT o FROM ReleaseEntity o where o.status IN (?1) AND o.createDate >= ?2 AND o.createDate <= ?3 ORDER BY o.id " ;

	public static final String SELECT_WARRANTY_FIELDS_BY_ENROLMENT_NUMBER = "ReleaseEntity.SELECT_WARRANTY_FIELDS_BY_ENROLMENT_NUMBER";	
	protected static final String JPQL_WARRANTY_FIELDS_BY_ENROLMENT_NUMBER = "SELECT re.enrolment.enrolmentNumber as enrolmentNumber, r2.wsInputProvidedBy as wsInputProvidedBy, r.wsInputRequested as wsInputRequested, r2.wsReceivedDate as wsReceivedDate, " +
			"t.description as wsRecommendation, r.wsRequestedDate as wsRequestedDate, r.wsRequestor as wsRequestor, r2.fcmAmountRetained as fcmAmountRetained " +
			"FROM ReleaseEntity r " +
			"JOIN ReleaseEnrolmentEntity re ON r.id = re.release.id " +
			"LEFT OUTER JOIN ReleaseEntity r2 ON r2.id = (SELECT MAX(r2.id) FROM ReleaseEntity r2 JOIN ReleaseEnrolmentEntity r2e ON r2e.release.id = r2.id WHERE r2e.enrolment.enrolmentNumber = re.enrolment.enrolmentNumber AND r2.wsReceivedDate IS NOT NULL) " +
			"LEFT OUTER JOIN WarrantyServicesRecommendationTypeEntity t ON t.id = r2.wsRecommendation.id " +
			"WHERE re.enrolment.enrolmentNumber IN (?1) " +
			"AND r.id = (SELECT MAX(r3.id) FROM ReleaseEntity r3 JOIN ReleaseEnrolmentEntity r3e ON r3e.release.id = r3.id WHERE r3e.enrolment.enrolmentNumber = re.enrolment.enrolmentNumber AND r3.wsRequestedDate IS NOT NULL) ";

	public static final String SELECT_RELEASE_BY_SECURITY_SEQUENCE = "ReleaseEntity.SELECT_RELEASE_BY_SECURITY_SEQUENCE";
	protected static final String JPQL_RELEASE_BY_SECURITY_SEQUENCE = "SELECT r FROM ReleaseEntity r WHERE r.security.id = ?1 AND r.sequenceNumber = ?2";
	
	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;
   
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "SECURITY_ID")
	private SecurityEntity security;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTACT_ID")
	private ContactEntity contact;
	
    @Column(name = "REGULAR_RELEASE")
	private boolean regularRelease;

    @Column(name = "REQUEST_DATE")
	private LocalDateTime requestDate;
    
    @Column(name = "REQUEST_AMOUNT")
	private BigDecimal requestAmount;
    
    @Column(name = "AUTHORIZED_AMOUNT")
	private BigDecimal authorizedAmount;
    
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "REPLACED_BY_SECURITY_ID")
	private SecurityEntity replacedBySecurity;
	
	@ManyToOne(optional = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "RELEASE_TYPE_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private ReleaseTypeEntity releaseType;

    @Column(name = "ANALYST_APPR_BY")
    private String analystApprovalBy;
    @Column(name = "ANALYST_APPR_DATE")
    private LocalDateTime analystApprovalDate;
    @Column(name = "ANALYST_APPR_STATUS")
	@Enumerated(EnumType.STRING)
    private ReleaseApprovalStatusEnum analystApprovalStatus;

    //TODO is this needed
//    @Column(name = "ANALYST_APPR_REQUESTOR")
//    private String analystApprovalRequestor;

    @Column(name = "COMMENT")
    private String comment;
    @Column(name = "FINAL_APPR_DATE")
    private LocalDateTime finalApprovalDate;
    @Column(name = "FINAL_APPR_BY")
    private String finalApprovalBy;

    @Column(name = "FINAL_APPR_STATUS")
	@Enumerated(EnumType.STRING)
    private ReleaseApprovalStatusEnum finalApprovalStatus;
    
    @Column(name = "FINAL_APPR_SENT_TO")
    private String finalApprovalSentTo;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "MANAGER_REJECT_REASON_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
    private RejectReasonEntity managerRejectReason;
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "ANALYST_REJECT_REASON_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
    private RejectReasonEntity analystRejectReason;
	
	
    @Column(name = "INTEREST_AMOUNT")
    private BigDecimal interestAmount;
    @Column(name = "PRINCIPAL_AMOUNT")
    private BigDecimal principalAmount;

	@NotAudited
	@Formula(value = " CONCAT(SECURITY_ID, '-', SEQUENCE_NUMBER) ")
	private String referenceNumber;
    @Column(name = "SEQUENCE_NUMBER")
    private Long sequenceNumber;
    @Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
    private ReleaseStatus status;
    @Column(name = "VOUCHER_ID")
    private String voucherId;

    @Column(name = "WS_INP_REQ")
    private boolean wsInputRequested;
    @Column(name = "WS_REQUESTOR")
    private String wsRequestor;
    @Column(name = "WS_REQUESTED_DATE")
    private LocalDateTime wsRequestedDate;
    @Column(name = "WS_RECEIVED_DATE")
    private LocalDateTime wsReceivedDate;

	@ManyToOne(optional = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "WS_RECOMMENDATION_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private WarrantyServicesRecommendationTypeEntity wsRecommendation;

    @Column(name = "WS_INPUT_PROVIDED_BY")
    private String wsInputProvidedBy;
    @Column(name = "FCM_AMOUNT_RETAINED")
    private BigDecimal fcmAmountRetained;
    @Column(name = "UNWIND_PRO_RATA")
    private BigDecimal unwindProRata;
    @Column(name = "SETTLEMENT_INSTRUCTIONS")
    private String settlementInstructions;
    @Column(name = "CHEQUE_NUMBER")
    private String chequeNumber;
    @Column(name = "CHEQUE_DATE")
    private LocalDateTime chequeDate;
    @Column(name = "CHEQUE_STATUS")
    private String chequeStatus;
    @Column(name = "REPLACEMENT_CHEQUE_NUMBER")
    private String replacementChequeNumber;
    @Column(name = "REPLACEMENT_CHEQUE_DATE")
    private LocalDateTime replacementChequeDate;
    @Column(name = "REPLACEMENT_CHEQUE_STATUS")
    private String replacementChequeStatus;
    @Column(name = "SERVICE_ORDER_ID")
    private String serviceOrderId;
    @Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "REPLACEMENT_PAYEE_CONTACT_ID")
	private ContactEntity replacementPayeeContact;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public SecurityEntity getSecurity() {
		return security;
	}

	public void setSecurity(SecurityEntity security) {
		this.security = security;
	}

	public ContactEntity getContact() {
		return contact;
	}

	public void setContact(ContactEntity contact) {
		this.contact = contact;
	}

	public boolean isRegularRelease() {
		return regularRelease;
	}

	public void setRegularRelease(boolean regularRelease) {
		this.regularRelease = regularRelease;
	}

	public LocalDateTime getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(LocalDateTime requestDate) {
		this.requestDate = requestDate;
	}

	public BigDecimal getRequestAmount() {
		return requestAmount;
	}

	public void setRequestAmount(BigDecimal requestAmount) {
		this.requestAmount = requestAmount;
	}

	public BigDecimal getAuthorizedAmount() {
		return authorizedAmount;
	}

	public void setAuthorizedAmount(BigDecimal authorizedAmount) {
		this.authorizedAmount = authorizedAmount;
	}

	public SecurityEntity getReplacedBySecurity() {
		return replacedBySecurity;
	}

	public void setReplacedBySecurity(SecurityEntity replacedBySecurity) {
		this.replacedBySecurity = replacedBySecurity;
	}

	public String getAnalystApprovalBy() {
		return analystApprovalBy;
	}

	public void setAnalystApprovalBy(String analystApprovalBy) {
		this.analystApprovalBy = analystApprovalBy;
	}

	public LocalDateTime getAnalystApprovalDate() {
		return analystApprovalDate;
	}

	public void setAnalystApprovalDate(LocalDateTime analystApprovalDate) {
		this.analystApprovalDate = analystApprovalDate;
	}

	public ReleaseApprovalStatusEnum getAnalystApprovalStatus() {
		return analystApprovalStatus;
	}

	public void setAnalystApprovalStatus(ReleaseApprovalStatusEnum analystApprovalStatus) {
		this.analystApprovalStatus = analystApprovalStatus;
	}

	public ReleaseApprovalStatusEnum getFinalApprovalStatus() {
		return finalApprovalStatus;
	}

	public void setFinalApprovalStatus(ReleaseApprovalStatusEnum finalApprovalStatus) {
		this.finalApprovalStatus = finalApprovalStatus;
	}

	//TODO is this needed
//	public String getAnalystApprovalRequestor() {
//		return analystApprovalRequestor;
//	}
//
//	public void setAnalystApprovalRequestor(String analystApprovalRequestor) {
//		this.analystApprovalRequestor = analystApprovalRequestor;
//	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public LocalDateTime getFinalApprovalDate() {
		return finalApprovalDate;
	}

	public void setFinalApprovalDate(LocalDateTime finalApprovalDate) {
		this.finalApprovalDate = finalApprovalDate;
	}

	public String getFinalApprovalBy() {
		return finalApprovalBy;
	}

	public void setFinalApprovalBy(String finalApprovalBy) {
		this.finalApprovalBy = finalApprovalBy;
	}

	public String getFinalApprovalSentTo() {
		return finalApprovalSentTo;
	}

	public void setFinalApprovalSentTo(String finalApprovalSentTo) {
		this.finalApprovalSentTo = finalApprovalSentTo;
	}

	public RejectReasonEntity getManagerRejectReason() {
		return managerRejectReason;
	}

	public void setManagerRejectReason(RejectReasonEntity managerRejectReason) {
		this.managerRejectReason = managerRejectReason;
	}

	public RejectReasonEntity getAnalystRejectReason() {
		return analystRejectReason;
	}

	public void setAnalystRejectReason(RejectReasonEntity analystRejectReason) {
		this.analystRejectReason = analystRejectReason;
	}

	public BigDecimal getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(BigDecimal interestAmount) {
		this.interestAmount = interestAmount;
	}

	public BigDecimal getPrincipalAmount() {
		return principalAmount;
	}

	public void setPrincipalAmount(BigDecimal principalAmount) {
		this.principalAmount = principalAmount;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public Long getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Long sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public ReleaseStatus getStatus() {
		return status;
	}

	public void setStatus(ReleaseStatus status) {
		this.status = status;
	}

	public String getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	public boolean isWsInputRequested() {
		return wsInputRequested;
	}

	public void setWsInputRequested(boolean wsInputRequested) {
		this.wsInputRequested = wsInputRequested;
	}

	public String getWsRequestor() {
		return wsRequestor;
	}

	public void setWsRequestor(String wsRequestor) {
		this.wsRequestor = wsRequestor;
	}

	public LocalDateTime getWsRequestedDate() {
		return wsRequestedDate;
	}

	public void setWsRequestedDate(LocalDateTime wsRequestedDate) {
		this.wsRequestedDate = wsRequestedDate;
	}

	public LocalDateTime getWsReceivedDate() {
		return wsReceivedDate;
	}

	public void setWsReceivedDate(LocalDateTime wsReceivedDate) {
		this.wsReceivedDate = wsReceivedDate;
	}

	public WarrantyServicesRecommendationTypeEntity getWsRecommendation() {
		return wsRecommendation;
	}

	public void setWsRecommendation(WarrantyServicesRecommendationTypeEntity wsRecommendation) {
		this.wsRecommendation = wsRecommendation;
	}

	public String getWsInputProvidedBy() {
		return wsInputProvidedBy;
	}

	public void setWsInputProvidedBy(String wsInputProvidedBy) {
		this.wsInputProvidedBy = wsInputProvidedBy;
	}

	public BigDecimal getFcmAmountRetained() {
		return fcmAmountRetained;
	}

	public void setFcmAmountRetained(BigDecimal fcmAmountRetained) {
		this.fcmAmountRetained = fcmAmountRetained;
	}

	public BigDecimal getUnwindProRata() {
		return unwindProRata;
	}

	public void setUnwindProRata(BigDecimal unwindProRata) {
		this.unwindProRata = unwindProRata;
	}

	public String getSettlementInstructions() {
		return settlementInstructions;
	}

	public void setSettlementInstructions(String settlementInstructions) {
		this.settlementInstructions = settlementInstructions;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public LocalDateTime getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(LocalDateTime chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getChequeStatus() {
		return chequeStatus;
	}

	public void setChequeStatus(String chequeStatus) {
		this.chequeStatus = chequeStatus;
	}

	public String getReplacementChequeNumber() {
		return replacementChequeNumber;
	}

	public void setReplacementChequeNumber(String replacementChequeNumber) {
		this.replacementChequeNumber = replacementChequeNumber;
	}

	public LocalDateTime getReplacementChequeDate() {
		return replacementChequeDate;
	}

	public void setReplacementChequeDate(LocalDateTime replacementChequeDate) {
		this.replacementChequeDate = replacementChequeDate;
	}

	public String getReplacementChequeStatus() {
		return replacementChequeStatus;
	}

	public void setReplacementChequeStatus(String replacementChequeStatus) {
		this.replacementChequeStatus = replacementChequeStatus;
	}
	
	public ReleaseTypeEntity getReleaseType() {
		return releaseType;
	}

	public void setReleaseType(ReleaseTypeEntity releaseType) {
		this.releaseType = releaseType;
	}

	public String getServiceOrderId() {
		return serviceOrderId;
	}

	public void setServiceOrderId(String serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public ContactEntity getReplacementPayeeContact() {
		return replacementPayeeContact;
	}

	public void setReplacementPayeeContact(ContactEntity replacementPayeeContact) {
		this.replacementPayeeContact = replacementPayeeContact;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReleaseEntity other = (ReleaseEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ReleaseEntity [id=" + id + ", version=" + version
				+ ", regularRelease=" + regularRelease + ", requestDate=" + requestDate + ", requestAmount="
				+ requestAmount + ", authorizedAmount=" + authorizedAmount
				+ ", analystApprovalBy=" + analystApprovalBy
				+ ", analystApprovalDate=" + analystApprovalDate + ", analystApprovalStatus=" + analystApprovalStatus
				+ ", comment=" + comment + ", finalApprovalDate=" + finalApprovalDate + ", finalApprovalBy=" + finalApprovalBy + ", finalApprovalStatus=" + finalApprovalStatus
				+ ", finalApprovalSentTo=" + finalApprovalSentTo
				+ ", interestAmount=" + interestAmount
				+ ", principalAmount=" + principalAmount + ", referenceNumber=" + referenceNumber
				+ ", sequenceNumber=" + sequenceNumber + ", status=" + status + ", voucherId=" + voucherId
				+ ", wsInputRequested=" + wsInputRequested + ", wsRequestor=" + wsRequestor
				+ ", wsRequestedDate=" + wsRequestedDate + ", wsReceivedDate=" + wsReceivedDate + ", wsRecommendation="
				+ wsRecommendation + ", wsInputProvidedBy=" + wsInputProvidedBy + ", fcmAmountRetained="
				+ fcmAmountRetained + ", unwindProRata=" + unwindProRata + ", settlementInstructions="
				+ settlementInstructions + ", chequeNumber=" + chequeNumber + ", chequeDate=" + chequeDate
				+ ", chequeStatus=" + chequeStatus + ", serviceOrderId=" + serviceOrderId + ", replacementChequeNumber=" + replacementChequeNumber
				+ ", replacementChequeDate=" + replacementChequeDate + ", replacementChequeStatus="
				+ replacementChequeStatus + ", createDate=" + createDate + ", createUser=" + createUser
				+ ", updateDate=" + updateDate + ", updateUser=" + updateUser + "]";
	}
	
	
	
}
