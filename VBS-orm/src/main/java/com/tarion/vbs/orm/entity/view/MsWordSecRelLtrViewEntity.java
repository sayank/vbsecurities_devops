package com.tarion.vbs.orm.entity.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * It represents MSWORD_SEC_REL_LTR_VW view in VBS database
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-06-11
 * @version 1.0
 *
 */
@NamedQuery(name=MsWordSecRelLtrViewEntity.SELECT_MSWORD_SEC_REL_LTR_VW_BY_REFERENCE_FIELD_NAME, query = MsWordSecRelLtrViewEntity.JPQL_MSWORD_SEC_REL_LTR_VW_BY_REFERENCE_FIELD_NAME)
@NamedQuery(name=MsWordSecRelLtrViewEntity.SELECT_ALL_MSWORD_SEC_REL_LTR_VW, query = MsWordSecRelLtrViewEntity.JPQL_ALL_MSWORD_SEC_REL_LTR_VW)

@Entity
@Table(name = "MSWORD_SEC_REL_LTR_VW")
public class MsWordSecRelLtrViewEntity implements Serializable {

	public static final String SELECT_ALL_MSWORD_SEC_REL_LTR_VW = "MsWordSecRelLtrViewEntity.SELECT_ALL_MSWORD_SEC_REL_LTR_VW";
	protected static final String JPQL_ALL_MSWORD_SEC_REL_LTR_VW = "SELECT o FROM MsWordSecRelLtrViewEntity o ";

	public static final String SELECT_MSWORD_SEC_REL_LTR_VW_BY_REFERENCE_FIELD_NAME = "MsWordSecRelLtrViewEntity.SELECT_MSWORD_SEC_REL_LTR_VW_BY_REFERENCE_FIELD_NAME";
	protected static final String JPQL_MSWORD_SEC_REL_LTR_VW_BY_REFERENCE_FIELD_NAME = "SELECT o FROM MsWordSecRelLtrViewEntity o where o.refFieldName = ?1 ";

	private static final long serialVersionUID = 1l;
	
	@Id
	@Column(name = "REF_FIELDNAME")
	private String refFieldName;
	
	@Column(name = "COMPANY")
	private String company;
	
	@Column(name = "COMPANY_NAME")
	private String companyName;
	
	@Column(name = "COMPANY_ADDRESS1")
	private String companyAddress1;
	
	@Column(name = "COMPANY_ADDRESS2")
	private String companyAddress2;
	
	@Column(name = "COMPANY_ADDRESS3")
	private String companyAddress3;
	
	@Column(name = "COMPANY_ADDERSS4")
	private String companyAddress4;

	@Column(name = "COMPANY_CITY")
	private String companyCity;
	
	@Column(name = "COMPANY_STATE")
	private String companyState;
	
	@Column(name = "COMPANY_POSTAL")
	private String companyPostal;
	
	@Column(name = "COMPANY_COUNTRY")
	private String companyCountry;
	
	@Column(name = "COMPANY_CONTACT_NAME")
	private String companyContactName;
	
	@Column(name = "TWC_AMOUNT_TYPE")
	private String twcAmountType;
	
	@Column(name = "TOT_REQUEST_AMT")
	private String totRequestAmt;
	
	@Column(name = "TOTAL_AUTHORIZED_AMT")
	private String totalAuthorizedAmt;
	
	@Column(name = "CURRENT_AMT")
	private String currentAmt;
	
	@Column(name = "ENROLMENTNO")
	private String enrolmentNo;
	
	@Column(name = "TWC_SECURITY_NBR")
	private int twcSecurityNumber;
	
	@Column(name = "TWC_INSTRUMENT_NBR")
	private String twcInstrumentNbr;
	
	@Column(name = "FIN_INSTITUTION_NAME")
	private String finInstitutionName;
	
	@Column(name = "FIN_ADDRESS1")
	private String finAddress1;
	
	@Column(name = "FIN_ADDRESS2")
	private String finAddress2;
	
	@Column(name = "FIN_CITY")
	private String finCity;
	
	@Column(name = "FIN_STATE")
	private String finState;
	
	@Column(name = "FIN_POSTL")
	private String finPostl;
	
	@Column(name = "REQUEST_DT")
	private String requestDt;
	
	@Column(name = "HOME")
	private String home;
	
	@Column(name = "ADDRESS1_HOME")
	private String address1Home;
	
	@Column(name = "SECURITY_TYPE")
	private String securityType;
	
	@Column(name = "WS_FCR")
	private String wsFcr;
	
	@Column(name = "CONDO_CORP")
	private String condoCorp;
	
	@Column(name = "REL_ROW_ADDED_OPRID")
	private String relRowAddedOPRID;
	
	/**
	 * @return the relRowAddedOPRID
	 */
	public String getRelRowAddedOPRID() {
		return relRowAddedOPRID;
	}

	/**
	 * @param relRowAddedOPRID the relRowAddedOPRID to set
	 */
	public void setRelRowAddedOPRID(String relRowAddedOPRID) {
		this.relRowAddedOPRID = relRowAddedOPRID;
	}

	/**
	 * @return the refFieldName
	 */
	public String getRefFieldName() {
		return refFieldName;
	}

	/**
	 * @param refFieldName the refFieldName to set
	 */
	public void setRefFieldName(String refFieldName) {
		this.refFieldName = refFieldName;
	}

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return the companyAddress1
	 */
	public String getCompanyAddress1() {
		return companyAddress1;
	}

	/**
	 * @param companyAddress1 the companyAddress1 to set
	 */
	public void setCompanyAddress1(String companyAddress1) {
		this.companyAddress1 = companyAddress1;
	}

	/**
	 * @return the companyAddress2
	 */
	public String getCompanyAddress2() {
		return companyAddress2;
	}

	/**
	 * @param companyAddress2 the companyAddress2 to set
	 */
	public void setCompanyAddress2(String companyAddress2) {
		this.companyAddress2 = companyAddress2;
	}

	/**
	 * @return the companyAddress3
	 */
	public String getCompanyAddress3() {
		return companyAddress3;
	}

	/**
	 * @param companyAddress3 the companyAddress3 to set
	 */
	public void setCompanyAddress3(String companyAddress3) {
		this.companyAddress3 = companyAddress3;
	}

	/**
	 * @return the companyAddress4
	 */
	public String getCompanyAddress4() {
		return companyAddress4;
	}

	/**
	 * @param companyAddress4 the companyAddress4 to set
	 */
	public void setCompanyAddress4(String companyAddress4) {
		this.companyAddress4 = companyAddress4;
	}

	/**
	 * @return the companyCity
	 */
	public String getCompanyCity() {
		return companyCity;
	}

	/**
	 * @param companyCity the companyCity to set
	 */
	public void setCompanyCity(String companyCity) {
		this.companyCity = companyCity;
	}

	/**
	 * @return the companyState
	 */
	public String getCompanyState() {
		return companyState;
	}

	/**
	 * @param companyState the companyState to set
	 */
	public void setCompanyState(String companyState) {
		this.companyState = companyState;
	}

	/**
	 * @return the companyPostal
	 */
	public String getCompanyPostal() {
		return companyPostal;
	}

	/**
	 * @param companyPostal the companyPostal to set
	 */
	public void setCompanyPostal(String companyPostal) {
		this.companyPostal = companyPostal;
	}

	/**
	 * @return the companyCountry
	 */
	public String getCompanyCountry() {
		return companyCountry;
	}

	/**
	 * @param companyCountry the companyCountry to set
	 */
	public void setCompanyCountry(String companyCountry) {
		this.companyCountry = companyCountry;
	}

	/**
	 * @return the companyContactName
	 */
	public String getCompanyContactName() {
		return companyContactName;
	}

	/**
	 * @param companyContactName the companyContactName to set
	 */
	public void setCompanyContactName(String companyContactName) {
		this.companyContactName = companyContactName;
	}

	/**
	 * @return the twcAmountType
	 */
	public String getTwcAmountType() {
		return twcAmountType;
	}

	/**
	 * @param twcAmountType the twcAmountType to set
	 */
	public void setTwcAmountType(String twcAmountType) {
		this.twcAmountType = twcAmountType;
	}

	/**
	 * @return the totRequestAmt
	 */
	public String getTotRequestAmt() {
		return totRequestAmt;
	}

	/**
	 * @param totRequestAmt the totRequestAmt to set
	 */
	public void setTotRequestAmt(String totRequestAmt) {
		this.totRequestAmt = totRequestAmt;
	}

	/**
	 * @return the totalAuthorizedAmt
	 */
	public String getTotalAuthorizedAmt() {
		return totalAuthorizedAmt;
	}

	/**
	 * @param totalAuthorizedAmt the totalAuthorizedAmt to set
	 */
	public void setTotalAuthorizedAmt(String totalAuthorizedAmt) {
		this.totalAuthorizedAmt = totalAuthorizedAmt;
	}

	/**
	 * @return the currentAmt
	 */
	public String getCurrentAmt() {
		return currentAmt;
	}

	/**
	 * @param currentAmt the currentAmt to set
	 */
	public void setCurrentAmt(String currentAmt) {
		this.currentAmt = currentAmt;
	}

	/**
	 * @return the enrolmentNo
	 */
	public String getEnrolmentNo() {
		return enrolmentNo;
	}

	/**
	 * @param enrolmentNo the enrolmentNo to set
	 */
	public void setEnrolmentNo(String enrolmentNo) {
		this.enrolmentNo = enrolmentNo;
	}

	/**
	 * @return the twcSecurityNumber
	 */
	public int getTwcSecurityNumber() {
		return twcSecurityNumber;
	}

	/**
	 * @param twcSecurityNumber the twcSecurityNumber to set
	 */
	public void setTwcSecurityNumber(int twcSecurityNumber) {
		this.twcSecurityNumber = twcSecurityNumber;
	}

	/**
	 * @return the twcInstrumentNbr
	 */
	public String getTwcInstrumentNbr() {
		return twcInstrumentNbr;
	}

	/**
	 * @param twcInstrumentNbr the twcInstrumentNbr to set
	 */
	public void setTwcInstrumentNbr(String twcInstrumentNbr) {
		this.twcInstrumentNbr = twcInstrumentNbr;
	}

	/**
	 * @return the finInstitutionName
	 */
	public String getFinInstitutionName() {
		return finInstitutionName;
	}

	/**
	 * @param finInstitutionName the finInstitutionName to set
	 */
	public void setFinInstitutionName(String finInstitutionName) {
		this.finInstitutionName = finInstitutionName;
	}

	/**
	 * @return the finAddress1
	 */
	public String getFinAddress1() {
		return finAddress1;
	}

	/**
	 * @param finAddress1 the finAddress1 to set
	 */
	public void setFinAddress1(String finAddress1) {
		this.finAddress1 = finAddress1;
	}

	/**
	 * @return the finAddress2
	 */
	public String getFinAddress2() {
		return finAddress2;
	}

	/**
	 * @param finAddress2 the finAddress2 to set
	 */
	public void setFinAddress2(String finAddress2) {
		this.finAddress2 = finAddress2;
	}

	/**
	 * @return the finCity
	 */
	public String getFinCity() {
		return finCity;
	}

	/**
	 * @param finCity the finCity to set
	 */
	public void setFinCity(String finCity) {
		this.finCity = finCity;
	}

	/**
	 * @return the finState
	 */
	public String getFinState() {
		return finState;
	}

	/**
	 * @param finState the finState to set
	 */
	public void setFinState(String finState) {
		this.finState = finState;
	}

	/**
	 * @return the finPostl
	 */
	public String getFinPostl() {
		return finPostl;
	}

	/**
	 * @param finPostl the finPostl to set
	 */
	public void setFinPostl(String finPostl) {
		this.finPostl = finPostl;
	}

	/**
	 * @return the requestDt
	 */
	public String getRequestDt() {
		return requestDt;
	}

	/**
	 * @param requestDt the requestDt to set
	 */
	public void setRequestDt(String requestDt) {
		this.requestDt = requestDt;
	}

	/**
	 * @return the home
	 */
	public String getHome() {
		return home;
	}

	/**
	 * @param home the home to set
	 */
	public void setHome(String home) {
		this.home = home;
	}

	/**
	 * @return the address1Home
	 */
	public String getAddress1Home() {
		return address1Home;
	}

	/**
	 * @param address1Home the address1Home to set
	 */
	public void setAddress1Home(String address1Home) {
		this.address1Home = address1Home;
	}

	/**
	 * @return the securityType
	 */
	public String getSecurityType() {
		return securityType;
	}

	/**
	 * @param securityType the securityType to set
	 */
	public void setSecurityType(String securityType) {
		this.securityType = securityType;
	}

	/**
	 * @return the wsFcr
	 */
	public String getWsFcr() {
		return wsFcr;
	}

	/**
	 * @param wsFcr the wsFcr to set
	 */
	public void setWsFcr(String wsFcr) {
		this.wsFcr = wsFcr;
	}

	/**
	 * @return the condoCorp
	 */
	public String getCondoCorp() {
		return condoCorp;
	}

	/**
	 * @param condoCorp the condoCorp to set
	 */
	public void setCondoCorp(String condoCorp) {
		this.condoCorp = condoCorp;
	}

}
