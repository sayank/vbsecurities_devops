package com.tarion.vbs.orm.entity.autorelease;

import java.time.LocalDateTime;

import javax.persistence.*;

import com.tarion.vbs.common.constants.VbsConstants;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.tarion.vbs.orm.entity.AuditedEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
import com.tarion.vbs.orm.entity.release.ReleaseEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;


@NamedQuery(name = AutoReleaseEnrolmentEntity.SELECT_ALL_ENROLMENT_FOR_RUN, query = AutoReleaseEnrolmentEntity.JPQL_SELECT_ALL_ENROLMENT_FOR_RUN)
@NamedQuery(name = AutoReleaseEnrolmentEntity.SELECT_COUNT_PROCESSING_FOR_RUN, query = AutoReleaseEnrolmentEntity.JPQL_SELECT_COUNT_PROCESSING_FOR_RUN)
@NamedQuery(name = AutoReleaseEnrolmentEntity.SELECT_COUNT_RELEASES_IN_RUN, query = AutoReleaseEnrolmentEntity.JPQL_SELECT_COUNT_RELEASES_IN_RUN)
@NamedQuery(name = AutoReleaseEnrolmentEntity.SELECT_SEARCH_ENROLMENT_FOR_RUN_ELIMINATED, query = AutoReleaseEnrolmentEntity.JPQL_SELECT_SEARCH_ENROLMENT_FOR_RUN_ELIMINATED)
@NamedQuery(name = AutoReleaseEnrolmentEntity.SELECT_SEARCH_ENROLMENT_FOR_RUN_SUCCESS, query = AutoReleaseEnrolmentEntity.JPQL_SELECT_SEARCH_ENROLMENT_FOR_RUN_SUCCESS)
@NamedQuery(name = AutoReleaseEnrolmentEntity.SELECT_ALL_ENROLMENT_FOR_RUN_SUCCESS, query = AutoReleaseEnrolmentEntity.JPQL_SELECT_ALL_ENROLMENT_FOR_RUN_SUCCESS)
@NamedQuery(name = AutoReleaseEnrolmentEntity.SELECT_AUTO_RELEASE_FOR_SECURITY, query = AutoReleaseEnrolmentEntity.JPQL_SELECT_AUTO_RELEASE_FOR_SECURITY)

@Entity
@Table(name = "AUTO_RELEASE_ENROLMENT")
@Audited(withModifiedFlag=true)
public class AutoReleaseEnrolmentEntity implements AuditedEntity {

	public static final String SELECT_ALL_ENROLMENT_FOR_RUN = "AutoReleaseEnrolmentEntity.SELECT_ALL_ENROLMENT_FOR_RUN";
	static final String JPQL_SELECT_ALL_ENROLMENT_FOR_RUN = "SELECT o FROM AutoReleaseEnrolmentEntity o where o.autoReleaseRun.id = ?1 ";

	public static final String SELECT_ALL_ENROLMENT_FOR_RUN_SUCCESS = "SELECT_ALL_ENROLMENT_FOR_RUN_SUCCESS.SELECT_SEARCH_ENROLMENT_FOR_RUN_SUCCESS";
	static final String JPQL_SELECT_ALL_ENROLMENT_FOR_RUN_SUCCESS = "SELECT o"
			+ " FROM AutoReleaseEnrolmentEntity o "
			+ " WHERE o.autoReleaseRun.id = ?1 AND "
			+ " NOT EXISTS (select k FROM AutoReleaseEliminationEntity k where k.autoReleaseEnrolment.id = o.id ) ";

	public static final String SELECT_SEARCH_ENROLMENT_FOR_RUN_SUCCESS = "AutoReleaseEnrolmentEntity.SELECT_SEARCH_ENROLMENT_FOR_RUN_SUCCESS";
	static final String JPQL_SELECT_SEARCH_ENROLMENT_FOR_RUN_SUCCESS = "SELECT o.id as id, vb.crmContactId AS vbNumber, vb.companyName AS vbName, o.security.receivedDate as securityReceivedDate, "
			+ " o.security.instrumentNumber as instrumentNumber, o.enrolment.enrolmentNumber as enrolmentNumber, a as address, "
			+ " o.enrolment.warrantyStartDate as warrantyStartDate, o.security.securityType.description as securityType, o.timingType.description as releaseTimingType, "
			+ " o.deletedBy as deletedBy, o.autoReleaseRun.id as runId, o.security.id as securityId, '' as eliminationCriteria, licStatus.description as licenseStatus "
			+ " FROM AutoReleaseEnrolmentEntity o "
            + " LEFT OUTER JOIN ContactEntity vb on o.security.primaryVb = vb "
            + " LEFT OUTER JOIN LicenseStatusEntity licStatus on vb.licenseStatus = licStatus "
			+ " LEFT OUTER JOIN AddressEntity a on o.enrolment.address.id = a.id "
			+ " where o.autoReleaseRun.id = ?1 and "
			+ " NOT EXISTS (select k FROM AutoReleaseEliminationEntity k where k.autoReleaseEnrolment.id = o.id ) ";

	public static final String SELECT_SEARCH_ENROLMENT_FOR_RUN_ELIMINATED = "AutoReleaseEnrolmentEntity.SELECT_SEARCH_ENROLMENT_FOR_RUN_ELIMINATED";
	static final String JPQL_SELECT_SEARCH_ENROLMENT_FOR_RUN_ELIMINATED = "SELECT o.id as id, vb.crmContactId AS vbNumber, vb.companyName AS vbName, o.security.receivedDate as securityReceivedDate, "
			+ " o.security.instrumentNumber as instrumentNumber, o.enrolment.enrolmentNumber as enrolmentNumber, a as address, "
			+ " o.enrolment.warrantyStartDate as warrantyStartDate, o.security.securityType.description as securityType, o.timingType.description as releaseTimingType, "
			+ " o.deletedBy as deletedBy, o.autoReleaseRun.id as runId, o.security.id as securityId, el.autoReleaseCriteria.name as eliminationCriteria, licStatus.description as licenseStatus "
			+ " FROM AutoReleaseEnrolmentEntity o " 
			+ " JOIN AutoReleaseEliminationEntity el on el.autoReleaseEnrolment = o "
			+ " LEFT OUTER JOIN ContactEntity vb on o.security.primaryVb = vb "
			+ " LEFT OUTER JOIN LicenseStatusEntity licStatus on vb.licenseStatus = licStatus "
			+ " LEFT OUTER JOIN AddressEntity a on o.enrolment.address.id = a.id "
			+ " where o.autoReleaseRun.id = ?1 and "
			+ " EXISTS (select k FROM AutoReleaseEliminationEntity k where k.autoReleaseEnrolment.id = o.id ) ";

	public static final String SELECT_COUNT_PROCESSING_FOR_RUN = "AutoReleaseEnrolmentEntity.SELECT_COUNT_PROCESSING_FOR_RUN";
	static final String JPQL_SELECT_COUNT_PROCESSING_FOR_RUN = "SELECT count(o) FROM AutoReleaseEnrolmentEntity o where o.autoReleaseRun.id = ?1 AND o.status.id = " + VbsConstants.AUTO_RELEASE_ENROLMENT_STATUS_PROCESSING;

	public static final String SELECT_COUNT_RELEASES_IN_RUN = "AutoReleaseEnrolmentEntity.SELECT_NUMBER_OF_RELEASES_IN_RUN";
	protected static final String JPQL_SELECT_COUNT_RELEASES_IN_RUN = "SELECT count(e) FROM AutoReleaseEnrolmentEntity e WHERE e.release is not null and e.autoReleaseRun.id = ?1 ";

	public static final String SELECT_AUTO_RELEASE_FOR_SECURITY = "AutoReleaseEnrolmentEntity.SELECT_AUTO_RELEASE_FOR_SECURITY";
	static final String JPQL_SELECT_AUTO_RELEASE_FOR_SECURITY =
			  " SELECT o.autoReleaseRun.id as runId, o.autoReleaseRun.createDate as runDate, o.enrolment.enrolmentNumber as enrolmentNumber, a as address, "
			+ " o.timingType.description as timingType, crit.description as elimination, rel.sequenceNumber as releaseSeqNum, rel.createDate as releaseDate, o.deletedBy as deletedBy "
			+ " FROM AutoReleaseEnrolmentEntity o "
		    + " LEFT OUTER JOIN ReleaseEntity rel on o.release = rel "
			+ " LEFT OUTER JOIN AutoReleaseEliminationEntity el on el.autoReleaseEnrolment = o "
		    + " LEFT OUTER JOIN AutoReleaseCriteriaEntity crit on crit = el.autoReleaseCriteria "
		    + " LEFT OUTER JOIN AddressEntity a on o.enrolment.address.id = a.id "
			+ " WHERE o.security.id = ?1 "
			+ " ORDER BY o.autoReleaseRun.id DESC, rel.createDate DESC, o.enrolment.enrolmentNumber";


	private static final long serialVersionUID = 1594327867691398395L;


	
	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;	
	
	@Version
    @Column(name = "VERSION")
	private Long version;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AUTO_RELEASE_RUN_ID")
	private AutoReleaseRunEntity autoReleaseRun;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "SECURITY_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private SecurityEntity security;

	@Column(name = "VB_NUMBER")
	private String vbNumber;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "ENROLMENT_NUMBER")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private EnrolmentEntity enrolment;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "TIMING_TYPE_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private AutoReleaseTimingTypeEntity timingType;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RELEASE_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private ReleaseEntity release;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private AutoReleaseEnrolmentStatusEntity status;
	
    @Column(name = "DELETED_BY")
	private String deletedBy;

	@Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}	
	public AutoReleaseRunEntity getAutoReleaseRun() {
		return autoReleaseRun;
	}
	public void setAutoReleaseRun(AutoReleaseRunEntity autoReleaseRun) {
		this.autoReleaseRun = autoReleaseRun;
	}	
	public SecurityEntity getSecurity() {
		return security;
	}
	public void setSecurity(SecurityEntity security) {
		this.security = security;
	}
	public String getVbNumber() {
		return vbNumber;
	}
	public void setVbNumber(String vbNumber) {
		this.vbNumber = vbNumber;
	}
	public EnrolmentEntity getEnrolment() {
		return enrolment;
	}
	public void setEnrolment(EnrolmentEntity enrolment) {
		this.enrolment = enrolment;
	}
	public AutoReleaseTimingTypeEntity getTimingType() {
		return timingType;
	}
	public void setTimingType(AutoReleaseTimingTypeEntity timingType) {
		this.timingType = timingType;
	}
	public ReleaseEntity getRelease() {
		return release;
	}
	public void setRelease(ReleaseEntity release) {
		this.release = release;
	}
	public AutoReleaseEnrolmentStatusEntity getStatus() {
		return status;
	}
	public void setStatus(AutoReleaseEnrolmentStatusEntity status) {
		this.status = status;
	}
	public String getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}
	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	

}
