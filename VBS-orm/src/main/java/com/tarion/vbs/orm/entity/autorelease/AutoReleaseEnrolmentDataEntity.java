package com.tarion.vbs.orm.entity.autorelease;

import com.tarion.vbs.orm.entity.AuditedEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDateTime;


@NamedQuery(name=AutoReleaseEnrolmentDataEntity.SELECT_WITH_ENROLMENT_NUMBER_RUN_ID, query=AutoReleaseEnrolmentDataEntity.JPQL_SELECT_WITH_ENROLMENT_NUMBER_RUN_ID)

@Entity
@Table(name = "AUTO_RELEASE_ENROLMENT_DATA")
@Audited(withModifiedFlag=true)
public class AutoReleaseEnrolmentDataEntity implements AuditedEntity {

	public static final String SELECT_WITH_ENROLMENT_NUMBER_RUN_ID = "AutoReleaseEnrolmentDataEntity.SELECT_WITH_ENROLMENT_NUMBER_RUN_ID";
	protected static final String JPQL_SELECT_WITH_ENROLMENT_NUMBER_RUN_ID = "SELECT e FROM AutoReleaseEnrolmentDataEntity e WHERE e.enrolmentNumber = ?1 AND e.autoReleaseRun.id = ?2";

	private static final long serialVersionUID = 1594327867691398395L;

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;	
	@Version
    @Column(name = "VERSION")
	private Long version;
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "AUTO_RELEASE_RUN_ID")
	private AutoReleaseRunEntity autoReleaseRun;	
	@Column(name = "ENROLMENT_NUMBER")
	private String enrolmentNumber;
	@Column(name = "DATE_OF_POSSESSION")
	private LocalDateTime dateOfPossession;
	@Column(name = "UNRESOLVED_CASES")
	private long unresolvedCases;	
	@Column(name = "INVENTORY_ITEM")
	private boolean inventoryItem;	
	@Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}	
	public AutoReleaseRunEntity getAutoReleaseRun() {
		return autoReleaseRun;
	}
	public void setAutoReleaseRun(AutoReleaseRunEntity autoReleaseRun) {
		this.autoReleaseRun = autoReleaseRun;
	}
	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}
	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}
	public LocalDateTime getDateOfPossession() {
		return dateOfPossession;
	}
	public void setDateOfPossession(LocalDateTime dateOfPossession) {
		this.dateOfPossession = dateOfPossession;
	}
	public long getUnresolvedCases() {
		return unresolvedCases;
	}
	public void setUnresolvedCases(long unresolvedCases) {
		this.unresolvedCases = unresolvedCases;
	}
	public boolean isInventoryItem() {
		return inventoryItem;
	}
	public void setInventoryItem(boolean inventoryItem) {
		this.inventoryItem = inventoryItem;
	}
	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	@Override
	public String toString() {
		return "AutoReleaseEnrolmentData [id=" + id + ", version=" + version
				+ ", enrolmentNumber=" + enrolmentNumber + ", dateOfPossession=" + dateOfPossession
				+ ", unresolvedCases=" + unresolvedCases + ", inventoryItem=" + inventoryItem + ", createDate="
				+ createDate + ", createUser=" + createUser + ", updateDate=" + updateDate + ", updateUser="
				+ updateUser + "]";
	}
}
