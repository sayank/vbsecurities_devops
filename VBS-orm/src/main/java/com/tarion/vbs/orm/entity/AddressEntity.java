/* 
 * 
 * AddressEntity.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.envers.Audited;

import com.tarion.vbs.common.util.VbsUtil;

/**
 * Address Entity JPA class holds address information
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-11-18
 * @version 1.0
 */
@Entity
@Table(name = "ADDRESS")
@Audited(withModifiedFlag=true)
public class AddressEntity implements AuditedEntity {
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;
	@Column(name = "ADDRESS_LINE1")
	private String addressLine1;
    @Column(name = "ADDRESS_LINE2")
	private String addressLine2;
    @Column(name = "ADDRESS_LINE3")
	private String addressLine3;
    @Column(name = "ADDRESS_LINE4")
	private String addressLine4;
    @Column(name = "CITY")
	private String city;
    @Column(name = "POSTAL_CODE")
	private String postalCode;
    @Column(name = "PROVINCE")
	private String province;
    @Column(name = "COUNTRY")
	private String country;

	@Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getAddressLine3() {
		return addressLine3;
	}
	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}
	public String getAddressLine4() {
		return addressLine4;
	}
	public void setAddressLine4(String addressLine4) {
		this.addressLine4 = addressLine4;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addressLine1 == null) ? 0 : addressLine1.hashCode());
		result = prime * result + ((addressLine2 == null) ? 0 : addressLine2.hashCode());
		result = prime * result + ((addressLine3 == null) ? 0 : addressLine3.hashCode());
		result = prime * result + ((addressLine4 == null) ? 0 : addressLine4.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result + ((createUser == null) ? 0 : createUser.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((postalCode == null) ? 0 : postalCode.hashCode());
		result = prime * result + ((province == null) ? 0 : province.hashCode());
		result = prime * result + ((updateDate == null) ? 0 : updateDate.hashCode());
		result = prime * result + ((updateUser == null) ? 0 : updateUser.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AddressEntity other = (AddressEntity) obj;
		if (addressLine1 == null) {
			if (other.addressLine1 != null) { return false; }
		} else if (!addressLine1.equals(other.addressLine1)) {
			return false;
		}
		if (addressLine2 == null) {
			if (other.addressLine2 != null) { return false; }
		} else if (!addressLine2.equals(other.addressLine2)) {
			return false;
		}
		if (addressLine3 == null) {
			if (other.addressLine3 != null) {
				return false;
			}
		} else if (!addressLine3.equals(other.addressLine3)) {
			return false;
		}
		if (addressLine4 == null) {
			if (other.addressLine4 != null) {
				return false;
			}
		} else if (!addressLine4.equals(other.addressLine4)) {
			return false;
		}
		if (city == null) {
			if (other.city != null) {
				return false;
			}
		} else if (!city.equals(other.city)) {
			return false;
		}
		if (country == null) {
			if (other.country != null) {
				return false;
			}
		} else if (!country.equals(other.country)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (postalCode == null) {
			if (other.postalCode != null) {
				return false;
			}
		} else if (!postalCode.equals(other.postalCode)) {
			return false;
		}
		if (province == null) {
			if (other.province != null) {
				return false;
			}
		} else if (!province.equals(other.province)) {
			return false;
		}
		if (version == null) {
			if (other.version != null) {
				return false;
			}
		} else if (!version.equals(other.version)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "AddressEntity [id=" + id + ", version=" + version + 
				", addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", addressLine3=" + addressLine3 + ", addressLine4=" + addressLine4 + 
				", city=" + city +
				", postalCode=" + postalCode + 
				", province=" + province + 
				", country=" + country + 
				", createDate=" + createDate + ", createUser=" + createUser + 
				", updateDate=" + updateDate + ", updateUser=" + updateUser + "]";
	}
	
	public String concatAddress() {
		List<String> list = new LinkedList<>();
		if(!VbsUtil.isNullorEmpty(this.addressLine1)) {list.add(this.addressLine1);}
		if(!VbsUtil.isNullorEmpty(this.addressLine2)) {list.add(this.addressLine2);}
		if(!VbsUtil.isNullorEmpty(this.addressLine3)) {list.add(this.addressLine3);}
		if(!VbsUtil.isNullorEmpty(this.addressLine4)) {list.add(this.addressLine4);}
		if(!VbsUtil.isNullorEmpty(this.city)) {list.add(this.city);}
		if(!VbsUtil.isNullorEmpty(this.province)) {list.add(this.province);}
		if(!VbsUtil.isNullorEmpty(this.postalCode)) {list.add(this.postalCode);}
		return String.join(", ", list);
	}
}
