package com.tarion.vbs.orm.entity;

import java.io.Serializable;
import java.time.LocalDateTime;


public interface AuditedEntity extends Serializable {
	public LocalDateTime getCreateDate();
	public void setCreateDate(LocalDateTime createDate);
	public String getCreateUser();
	public void setCreateUser(String createUser);
	public LocalDateTime getUpdateDate();
	public void setUpdateDate(LocalDateTime updateDate);
	public String getUpdateUser();
	public void setUpdateUser(String updateUser);
	public Long getVersion();
	public void setVersion(Long version);
}
