/* 
 * 
 * EmailTransactionLogEntity.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Transaction Log Entity JPA class holds data about all internal emails sent via ESP
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-11-20
 * @version 1.0
 */

@Entity
@Table(name = "EMAIL_TRANSACTION_LOG")
public class EmailTransactionLogEntity implements AuditedEntity {
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;

	@Column(name = "TRACKING_NUMBER")
	private String trackingNumber;

	@Column(name = "SUBJECT")
	private String subject;
	
    @Column(name = "BODY")
	private String body;
    
    @Column(name = "FROM_ADDRESS")
	private String fromAddress;
    
    @Column(name = "TO_ADDRESSES")
	private String toAddresses;
    
    @Column(name = "EMAIL_ATTACHMENTS_XML")
	private String emailAttachmentsXml;
    
    @Column(name = "STATUS")
	private String status;
    
    @Column(name = "ERROR_MESSAGE")
	private String errorMessage;
    
    @Column(name = "EMAIL_SENT_DATE")
    private LocalDateTime emailSentDate;

	@Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getToAddresses() {
		return toAddresses;
	}

	public void setToAddresses(String toAddresses) {
		this.toAddresses = toAddresses;
	}

	public String getEmailAttachmentsXml() {
		return emailAttachmentsXml;
	}

	public void setEmailAttachmentsXml(String emailAttachmentsXml) {
		this.emailAttachmentsXml = emailAttachmentsXml;
	}

	public LocalDateTime getEmailSentDate() {
		return emailSentDate;
	}

	public void setEmailSentDate(LocalDateTime emailSentDate) {
		this.emailSentDate = emailSentDate;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	
}
