/* 
 * 
 * ContactEntity.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.contact;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.orm.entity.AddressEntity;
import com.tarion.vbs.orm.entity.AuditedEntity;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Contact Entity JPA class holds information about Contacts as 3rd party relation to securty
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-01-08
 * @version 1.0
 */

@NamedQuery(name=ContactEntity.SELECT_ALL_ESCROW_AGENTS, query = ContactEntity.JPQL_ALL_ESCROW_AGENTS)
@NamedQuery(name=ContactEntity.SELECT_ALL_ESCROW_AGENTS_LIKE, query = ContactEntity.JPQL_ALL_ESCROW_AGENTS_LIKE)
@NamedQuery(name=ContactEntity.SELECT_ESCROW_AGENTS_CRM_CONTACT_ID_BEGINS_WITH, query = ContactEntity.JPQL_ESCROW_AGENTS_CRM_CONTACT_ID_BEGINS_WITH)
@NamedQuery(name=ContactEntity.SELECT_LAWYERS_BY_ESCROW_AGENT_ID, query = ContactEntity.JPQL_LAWYERS_BY_ESCROW_AGENT_ID)
@NamedQuery(name=ContactEntity.SELECT_ASSISTANTS_BY_ESCROW_AGENT_ID, query = ContactEntity.JPQL_ASSISTANTS_BY_ESCROW_AGENT_ID)
@NamedQuery(name=ContactEntity.SELECT_CONTACT_ID_BY_CRM_CONTACT_ID, query = ContactEntity.JPQL_CONTACT_ID_BY_CRM_CONTACT_ID)
@NamedQuery(name=ContactEntity.SELECT_CONTACT_BY_CRM_CONTACT_ID, query = ContactEntity.JPQL_CONTACT_BY_CRM_CONTACT_ID)
@NamedQuery(name=ContactEntity.SELECT_ALL_VBS_VB_NUMBER_LIKE, query = ContactEntity.JPQL_ALL_VBS_VB_NUMBER_LIKE)
@NamedQuery(name=ContactEntity.SELECT_ALL_VB_NUMBERS_LIKE, query = ContactEntity.JPQL_ALL_VB_NUMBERS_LIKE)
@NamedQuery(name=ContactEntity.SELECT_LICENSE_STATUS_BY_CRM_CONTACT_ID, query = ContactEntity.JPQL_SELECT_LICENSE_STATUS_CRM_CONTACT_ID)

@Entity
@Table(name = "CONTACT")
@Audited(withModifiedFlag=true)
public class ContactEntity implements AuditedEntity {
	private static final long serialVersionUID = 2L;
	
	public static final String SELECT_ALL_ESCROW_AGENTS_LIKE = "ContactEntity.SELECT_ALL_ESCROW_AGENTS_LIKE";	
	protected static final String JPQL_ALL_ESCROW_AGENTS_LIKE = "SELECT o FROM ContactEntity o where o.contactType.id = " + VbsConstants.CONTACT_TYPE_ESCROW_AGENT + " AND o.companyName like ?1 ORDER BY o.companyName ASC ";
	
	public static final String SELECT_ALL_ESCROW_AGENTS = "ContactEntity.SELECT_ALL_ESCROW_AGENTS";
	protected static final String JPQL_ALL_ESCROW_AGENTS  = "SELECT o FROM ContactEntity o WHERE o.contactType.id = " + VbsConstants.CONTACT_TYPE_ESCROW_AGENT + " ORDER BY createDate DESC";

	public static final String SELECT_ESCROW_AGENTS_CRM_CONTACT_ID_BEGINS_WITH = "ContactEntity.SELECT_ESCROW_AGENTS_CRM_CONTACT_ID_BEGINS_WITH";
	protected static final String JPQL_ESCROW_AGENTS_CRM_CONTACT_ID_BEGINS_WITH = "SELECT o FROM ContactEntity o WHERE o.contactType.id = " + VbsConstants.CONTACT_TYPE_ESCROW_AGENT + " AND o.crmContactId LIKE ?1 ORDER BY o.crmContactId ASC";

	public static final String SELECT_LICENSE_STATUS_BY_CRM_CONTACT_ID = "ContactEntity.SELECT_LICENSE_STATUS_BY_CRM_CONTACT_ID";
	protected static final String JPQL_SELECT_LICENSE_STATUS_CRM_CONTACT_ID = "SELECT o.licenseStatus FROM ContactEntity o WHERE o.crmContactId = ?1";

	public static final String SELECT_CONTACT_ID_BY_CRM_CONTACT_ID = "ContactEntity.SELECT_CONTACT_ID_BY_CRM_CONTACT_ID";
	protected static final String JPQL_CONTACT_ID_BY_CRM_CONTACT_ID = "SELECT o.id FROM ContactEntity o WHERE o.crmContactId = ?1";
	
	public static final String SELECT_CONTACT_BY_CRM_CONTACT_ID = "ContactEntity.SELECT_CONTACT_BY_CRM_CONTACT_ID";
	protected static final String JPQL_CONTACT_BY_CRM_CONTACT_ID = "SELECT o FROM ContactEntity o WHERE o.crmContactId = ?1";
	
	public static final String SELECT_LAWYERS_BY_ESCROW_AGENT_ID = "ContactEntity.SELECT_LAWYERS_BY_ESCROW_AGENT_ID";
	protected static final String JPQL_LAWYERS_BY_ESCROW_AGENT_ID = "SELECT o.toContact FROM ContactRelationshipEntity o WHERE o.fromContact.crmContactId = ?1 AND (o.endDate is null OR o.endDate > ?2) AND (o.endDate is null OR o.startDate != o.endDate) AND o.relationshipType.id = " + VbsConstants.RELATIONSHIP_TYPE_LAWYER +" ORDER BY o.toContact.crmContactId ASC";

	public static final String SELECT_ASSISTANTS_BY_ESCROW_AGENT_ID = "ContactEntity.SELECT_ASSISTANTS_BY_ESCROW_AGENT_ID";
	protected static final String JPQL_ASSISTANTS_BY_ESCROW_AGENT_ID = "SELECT o.toContact FROM ContactRelationshipEntity o WHERE o.fromContact.crmContactId = ?1 AND (o.endDate is null OR o.endDate > ?2) AND (o.endDate is null OR o.startDate != o.endDate) AND o.relationshipType.id = " + VbsConstants.RELATIONSHIP_TYPE_ASSISTANT +" ORDER BY o.toContact.crmContactId ASC";

	public static final String SELECT_ALL_VBS_VB_NUMBER_LIKE = "ContactEntity.SELECT_ALL_VBS_VB_NUMBER_LIKE";
	protected static final String JPQL_ALL_VBS_VB_NUMBER_LIKE = "SELECT o FROM ContactEntity o where o.crmContactId like ?1 AND o.contactType.id = " + VbsConstants.CONTACT_TYPE_VB + " ORDER BY o.crmContactId ASC ";

	public static final String SELECT_ALL_VB_NUMBERS_LIKE = "ContactEntity.SELECT_ALL_VB_NUMBERS_LIKE";
	protected static final String JPQL_ALL_VB_NUMBERS_LIKE = "SELECT o.crmContactId FROM ContactEntity o where UPPER(o.crmContactId) like ?1 AND o.contactType = " + VbsConstants.CONTACT_TYPE_VB + " ORDER BY o.crmContactId ASC ";


	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;
   
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTACT_TYPE_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private ContactTypeEntity contactType;	

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "LICENSE_STATUS_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private LicenseStatusEntity licenseStatus;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "ESCROW_LICENSE_APPLICATION_STATUS_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private EscrowLicenseApplicationStatusEntity escrowLicenseApplicationStatus;

	@Column(name = "CRM_CONTACT_ID")
	private String crmContactId;
    @Column(name = "FIRST_NAME")
	private String firstName;
    @Column(name = "LAST_NAME")
	private String lastName;
    @Column(name = "COMPANY_NAME")
	private String companyName;
    @Column(name = "EMAIL_ADDRESS")
	private String emailAddress;
    @Column(name = "PHONE")
	private String phone;
    @Column(name = "PHONE_EXTENSION")
	private String phoneExtension;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "ADDRESS_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private AddressEntity address;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "MAILING_ADDRESS_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private AddressEntity mailingAddress;
	

    @Column(name = "DATE_OF_BIRTH")
	private LocalDateTime dateOfBirth;
    
    @Column(name = "DRIVERS_LICENCE")
    private String driversLicence;
    @Column(name = "SIN")
    private String sin;

	@Column(name = "UNWILLING")
	private String unwilling;
	@Column(name = "ACCESSIBILITY")
	private boolean accessibility;
	@Column(name = "YELLOW_STICKY")
	private boolean yellowSticky;
	@Column(name = "ALERT")
	private String alert;
	@Column(name = "AP_VENDOR_FLAG")
	private boolean apVendorFlag;

    @Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Long getVersion() {
		return version;
	}

	@Override
	public void setVersion(Long version) {
		this.version = version;
	}

	public ContactTypeEntity getContactType() {
		return contactType;
	}

	public void setContactType(ContactTypeEntity contactType) {
		this.contactType = contactType;
	}

	public LicenseStatusEntity getLicenseStatus() {
		return licenseStatus;
	}

	public void setLicenseStatus(LicenseStatusEntity licenseStatus) {
		this.licenseStatus = licenseStatus;
	}

	public EscrowLicenseApplicationStatusEntity getEscrowLicenseApplicationStatus() {
		return escrowLicenseApplicationStatus;
	}

	public void setEscrowLicenseApplicationStatus(EscrowLicenseApplicationStatusEntity escrowLicenseApplicationStatus) {
		this.escrowLicenseApplicationStatus = escrowLicenseApplicationStatus;
	}

	public String getCrmContactId() {
		return crmContactId;
	}

	public void setCrmContactId(String crmContactId) {
		this.crmContactId = crmContactId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhoneExtension() {
		return phoneExtension;
	}

	public void setPhoneExtension(String phoneExtension) {
		this.phoneExtension = phoneExtension;
	}

	public AddressEntity getAddress() {
		return address;
	}

	public void setAddress(AddressEntity address) {
		this.address = address;
	}

	public AddressEntity getMailingAddress() {
		return mailingAddress;
	}

	public void setMailingAddress(AddressEntity mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public LocalDateTime getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDateTime dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getDriversLicence() {
		return driversLicence;
	}

	public void setDriversLicence(String driversLicence) {
		this.driversLicence = driversLicence;
	}

	public String getSin() {
		return sin;
	}

	public void setSin(String sin) {
		this.sin = sin;
	}

	public String getUnwilling() {
		return unwilling;
	}

	public void setUnwilling(String unwilling) {
		this.unwilling = unwilling;
	}

	public boolean isAccessibility() {
		return accessibility;
	}

	public void setAccessibility(boolean accessibility) {
		this.accessibility = accessibility;
	}

	public boolean isYellowSticky() {
		return yellowSticky;
	}

	public void setYellowSticky(boolean yellowSticky) {
		this.yellowSticky = yellowSticky;
	}

	public String getAlert() {
		return alert;
	}

	public void setAlert(String alert) {
		this.alert = alert;
	}

	public boolean getApVendorFlag() {
		return apVendorFlag;
	}

	public void setApVendorFlag(boolean apVendorFlag) {
		this.apVendorFlag = apVendorFlag;
	}

	@Override
	public LocalDateTime getCreateDate() {
		return createDate;
	}

	@Override
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	@Override
	public String getCreateUser() {
		return createUser;
	}

	@Override
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Override
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	@Override
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String getUpdateUser() {
		return updateUser;
	}

	@Override
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ContactEntity that = (ContactEntity) o;
		return accessibility == that.accessibility &&
				yellowSticky == that.yellowSticky &&
				apVendorFlag == that.apVendorFlag &&
				Objects.equals(id, that.id) &&
				Objects.equals(version, that.version) &&
				Objects.equals(contactType, that.contactType) &&
				Objects.equals(licenseStatus, that.licenseStatus) &&
				Objects.equals(escrowLicenseApplicationStatus, that.escrowLicenseApplicationStatus) &&
				Objects.equals(crmContactId, that.crmContactId) &&
				Objects.equals(firstName, that.firstName) &&
				Objects.equals(lastName, that.lastName) &&
				Objects.equals(companyName, that.companyName) &&
				Objects.equals(emailAddress, that.emailAddress) &&
				Objects.equals(phone, that.phone) &&
				Objects.equals(phoneExtension, that.phoneExtension) &&
				Objects.equals(address, that.address) &&
				Objects.equals(mailingAddress, that.mailingAddress) &&
				Objects.equals(dateOfBirth, that.dateOfBirth) &&
				Objects.equals(driversLicence, that.driversLicence) &&
				Objects.equals(sin, that.sin) &&
				Objects.equals(unwilling, that.unwilling) &&
				Objects.equals(alert, that.alert) &&
				Objects.equals(createDate, that.createDate) &&
				Objects.equals(createUser, that.createUser) &&
				Objects.equals(updateDate, that.updateDate) &&
				Objects.equals(updateUser, that.updateUser);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, version, contactType, licenseStatus, escrowLicenseApplicationStatus, crmContactId, firstName, lastName, companyName, emailAddress, phone, phoneExtension, address, mailingAddress, dateOfBirth, driversLicence, sin, unwilling, accessibility, yellowSticky, alert, apVendorFlag, createDate, createUser, updateDate, updateUser);
	}

	@Override
	public String toString() {
		return "ContactEntity{" +
				"id=" + id +
				", version=" + version +
				", crmContactId='" + crmContactId + '\'' +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", companyName='" + companyName + '\'' +
				", emailAddress='" + emailAddress + '\'' +
				", phone='" + phone + '\'' +
				", phoneExtension='" + phoneExtension + '\'' +
				", dateOfBirth=" + dateOfBirth +
				", driversLicence='" + driversLicence + '\'' +
				", sin='" + sin + '\'' +
				", unwilling='" + unwilling + '\'' +
				", accessibility=" + accessibility +
				", yellowSticky=" + yellowSticky +
				", alert='" + alert + '\'' +
				", createDate=" + createDate +
				", createUser='" + createUser + '\'' +
				", updateDate=" + updateDate +
				", updateUser='" + updateUser + '\'' +
				'}';
	}
}
