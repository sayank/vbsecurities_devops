package com.tarion.vbs.orm.entity.security;

import com.tarion.vbs.orm.entity.AuditedEntity;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
@NamedQuery(name=MonthlyReportEntity.SELECT_MONTHLY_REPORT_BY_SECURITY_YEAR_MONTH, query = MonthlyReportEntity.JPQL_SELECT_MONTHLY_REPORT_BY_SECURITY_YEAR_MONTH)
@NamedQuery(name=MonthlyReportEntity.SELECT_MONTHLY_REPORT_BY_SECURITY_ID, query = MonthlyReportEntity.JPQL_SELECT_MONTHLY_REPORT_BY_SECURITY_ID)

@Entity
@Table(name = "MONTHLY_REPORT")
@Audited(withModifiedFlag=true)
public class MonthlyReportEntity implements AuditedEntity {

    public static final String SELECT_MONTHLY_REPORT_BY_SECURITY_YEAR_MONTH = "MonthlyReportEntity.SELECT_MONTHLY_REPORT_BY_SECURITY_YEAR_MONTH";
    static final String JPQL_SELECT_MONTHLY_REPORT_BY_SECURITY_YEAR_MONTH = "SELECT o FROM MonthlyReportEntity o where o.securityId = ?1 AND o.reportYear = ?2 AND o.reportMonth = ?3";

    public static final String SELECT_MONTHLY_REPORT_BY_SECURITY_ID = "MonthlyReportEntity.SELECT_MONTHLY_REPORT_BY_SECURITY_ID";
    static final String JPQL_SELECT_MONTHLY_REPORT_BY_SECURITY_ID = "SELECT o FROM MonthlyReportEntity o where o.securityId = ?1 ORDER BY o.reportYear DESC, o.reportMonth DESC";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    private Long id;

    @Version
    @Column(name = "VERSION")
    private Long version;

    @Column(name = "SECURITY_ID", updatable = false)
    private int securityId;

    @Column(name = "REPORT_YEAR", updatable = false)
    private int reportYear;

    @Column(name = "REPORT_MONTH", updatable = false)
    private int reportMonth;

    @JoinColumn(name = "SECURITY_DEPOSIT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
    private SecurityDepositEntity securityDeposit;

    @Column(name = "CREATE_DATE")
    private LocalDateTime createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "UPDATE_DATE")
    private LocalDateTime updateDate;

    @Column(name = "UPDATE_USER")
    private String updateUser;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Long getVersion() { return version; }

    public void setVersion(Long version) { this.version = version; }

    public int getSecurityId() {
        return securityId;
    }

    public void setSecurityEntity(int securityId) {
        this.securityId = securityId;
    }

    public int getReportYear() {
        return reportYear;
    }

    public void setReportYear(int reportYear) {
        this.reportYear = reportYear;
    }

    public int getReportMonth() {
        return reportMonth;
    }

    public void setReportMonth(int reportMonth) {
        this.reportMonth = reportMonth;
    }

    public SecurityDepositEntity getSecurityDeposit() {
        return securityDeposit;
    }

    public void setSecurityDeposit(SecurityDepositEntity securityDeposit) {
        this.securityDeposit = securityDeposit;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public LocalDateTime getUpdateDate() { return updateDate; }

    public void setUpdateDate(LocalDateTime updateDate) { this.updateDate = updateDate; }

    public String getUpdateUser() { return updateUser; }

    public void setUpdateUser(String updateUser) { this.updateUser = updateUser; }
}
