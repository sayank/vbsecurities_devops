package com.tarion.vbs.orm.entity.correspondence;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Correspondence Token Entity, for mapping templates to tokens
 *
 * @author <A href="joni.paananen@tarion.com">Joni Paananen</A>
 * @since 2019-02-19
 * @version 1.0
 */

@NamedQuery(name=CorrespondenceTokenEntity.SELECT_TOKENS_WITH_TEMPLATE, query=CorrespondenceTokenEntity.JPQL_TOKENS_WITH_TEMPLATE)

@Entity
@Table(name = "CORRESPONDENCE_TOKEN")
public class CorrespondenceTokenEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String SELECT_TOKENS_WITH_TEMPLATE = "CorrespondenceTokenEntity.SELECT_TOKENS_WITH_TEMPLATE";
	protected static final String JPQL_TOKENS_WITH_TEMPLATE = "SELECT token "
			+ " FROM CorrespondenceTokenEntity token, CorrespondenceTokenTemplateEntity tokenTemplate, CorrespondenceTemplateEntity template "
			+ " WHERE token.id = tokenTemplate.tokenId AND template.id = tokenTemplate.templateId AND template.name = ?1 ";

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
    @Column(name = "NAME")
    private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CorrespondenceTokenEntity [id=" + id + ", name=" + name + "]";
	}
}
