/* 
 * 
 * ContactSecurityEntity.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.contact;

import com.tarion.vbs.orm.entity.AuditedEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Contact Security Entity JPA class is M:N relationship table between Contacts and Security
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-01-09
 * @version 1.0
 */

@NamedQuery(name=ContactSecurityEntity.SELECT_ESCROW_AGENT_FOR_SECURITY_ID, query = ContactSecurityEntity.JQPL_ESCROW_AGENT_FOR_SECURITY_ID)
@NamedQuery(name=ContactSecurityEntity.SELECT_CONTACT_BY_SECURITY_ID_CONTACT_SECURITY_ROLE_ID, query = ContactSecurityEntity.JQPL_CONTACT_BY_SECURITY_ID_CONTACT_SECURITY_ROLE_ID)
@NamedQuery(name=ContactSecurityEntity.SELECT_CONTACT_SECURITY_BY_SECURITY_ID_CONTACT_SECURITY_ROLE_ID, query = ContactSecurityEntity.JQPL_CONTACT_SECURITY_BY_SECURITY_ID_CONTACT_SECURITY_ROLE_ID)
@NamedQuery(name=ContactSecurityEntity.SELECT_CONTACT_BY_CRM_CONTACT_ID_AND_ROLE, query = ContactSecurityEntity.JQPL_SELECT_CONTACT_BY_CRM_CONTACT_ID_AND_ROLE)
@Entity
@Table(name = "CONTACT_SECURITY")
@Audited(withModifiedFlag=true)
public class ContactSecurityEntity implements AuditedEntity {

	private static final long serialVersionUID = 1L;

	public static final String SELECT_ESCROW_AGENT_FOR_SECURITY_ID = "ContactSecurityEntity.SELECT_ESCROW_AGENT_FOR_SECURITY_ID";
	static final String JQPL_ESCROW_AGENT_FOR_SECURITY_ID = "SELECT o.contact FROM ContactSecurityEntity o WHERE o.security.id = ?1 AND o.contact.contactType.id = ?2";

    public static final String SELECT_CONTACT_BY_SECURITY_ID_CONTACT_SECURITY_ROLE_ID = "ContactSecurityEntity.SELECT_CONTACT_BY_SECURITY_ID_CONTACT_SECURITY_ROLE_ID";
    static final String JQPL_CONTACT_BY_SECURITY_ID_CONTACT_SECURITY_ROLE_ID = "SELECT o.contact FROM ContactSecurityEntity o WHERE o.security.id = ?1 AND o.contactSecurityRole.id = ?2";

    public static final String SELECT_CONTACT_SECURITY_BY_SECURITY_ID_CONTACT_SECURITY_ROLE_ID = "ContactSecurityEntity.SELECT_CONTACT_SECURITY_BY_SECURITY_ID_CONTACT_SECURITY_ROLE_ID";
    static final String JQPL_CONTACT_SECURITY_BY_SECURITY_ID_CONTACT_SECURITY_ROLE_ID = "SELECT o FROM ContactSecurityEntity o WHERE o.security.id = ?1 AND o.contactSecurityRole.id = ?2";

    public static final String SELECT_CONTACT_BY_CRM_CONTACT_ID_AND_ROLE = "ContactSecurityEntity.SELECT_CONTACT_BY_CRM_CONTACT_ID";
    static final String JQPL_SELECT_CONTACT_BY_CRM_CONTACT_ID_AND_ROLE = "SELECT o.contact FROM ContactSecurityEntity o WHERE o.contact.crmContactId = ?1 AND o.contactSecurityRole.id = ?2";

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;

	@Version
    @Column(name = "VERSION")
	private Long version;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SECURITY_ID")
	private SecurityEntity security;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTACT_ID")
	private ContactEntity contact;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTACT_SECURITY_ROLE_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private ContactSecurityRoleEntity contactSecurityRole;

    @Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public SecurityEntity getSecurity() {
		return security;
	}

	public void setSecurity(SecurityEntity security) {
		this.security = security;
	}

	public ContactEntity getContact() {
		return contact;
	}

	public void setContact(ContactEntity contact) {
		this.contact = contact;
	}

	public ContactSecurityRoleEntity getContactSecurityRole() {
		return contactSecurityRole;
	}

	public void setContactSecurityRole(ContactSecurityRoleEntity contactSecurityRole) {
		this.contactSecurityRole = contactSecurityRole;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContactSecurityEntity other = (ContactSecurityEntity) obj;
		if (id == null) {
			return other.id == null;
		} else return id.equals(other.id);
	}

	@Override
	public String toString() {
		return "ContactSecurityEntity [id=" + id + ", version=" + version + " createDate=" + createDate + ", createUser=" + createUser
				+ ", updateDate=" + updateDate + ", updateUser=" + updateUser + "]";
	}

}
