/* 
 * 

 * FinancialInstitutionMaaPoaInstitutionEntity.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.financialinstitution;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.envers.Audited;

import com.tarion.vbs.orm.entity.AuditedEntity;

/**
 * Financial institution MAAPOA institution Entity JPA class holds information about financial institutions MAA or POA institutions
 *
 * @author <A href="sbogicevic@tarion.com">Srdjan Bogicevic</A>
 * @since 2019-05-16
 * @version 1.0
 */
@NamedQuery(name=FinancialInstitutionMaaPoaInstitutionEntity.SELECT_ALL_MAAPOA_INSTITUTIONS_FOR_MAAPOA_ID, query = FinancialInstitutionMaaPoaInstitutionEntity.JPQL_ALL_MAAPOA_INSTITUTIONS_FOR_MAAPOA_ID)

@Entity
@Table(name = "FINANCIAL_INSTITUTION_MAAPOA_INSTITUTION")
@Audited(withModifiedFlag=true)
public class FinancialInstitutionMaaPoaInstitutionEntity implements AuditedEntity {
	private static final long serialVersionUID = 1L;
	
	public static final String SELECT_ALL_MAAPOA_INSTITUTIONS_FOR_MAAPOA_ID = "FinancialInstitutionMaaPoaInstitutionEntity.SELECT_ALL_MAAPOA_INSTITUTIONS_FOR_MAAPOA_ID";
	protected static final String JPQL_ALL_MAAPOA_INSTITUTIONS_FOR_MAAPOA_ID = "SELECT o FROM FinancialInstitutionMaaPoaInstitutionEntity o WHERE o.financialInstitutionMaaPoa.id = ?1 ORDER BY o.id ";

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FINANCIAL_INSTITUTION_ID")
	private FinancialInstitutionEntity financialInstitution;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BRANCH_ID")
	private BranchEntity branch;
   
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FINANCIAL_INSTITUTION_MAAPOA_ID")
    private FinancialInstitutionMaaPoaEntity financialInstitutionMaaPoa;
    
    @Column(name = "PERCENTAGE")
	private BigDecimal percentage;

    @Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
	
	public FinancialInstitutionEntity getFinancialInstitution() {
		return financialInstitution;
	}

	public void setFinancialInstitution(FinancialInstitutionEntity financialInstitution) {
		this.financialInstitution = financialInstitution;
	}
	
	public BranchEntity getBranch() {
		return branch;
	}

	public void setBranch(BranchEntity branch) {
		this.branch = branch;
    }
    
    public FinancialInstitutionMaaPoaEntity getFinancialInstitutionMaaPoa() {
		return financialInstitutionMaaPoa;
	}

	public void setFinancialInstitutionMaaPoa(FinancialInstitutionMaaPoaEntity financialInstitutionMaaPoa) {
		this.financialInstitutionMaaPoa = financialInstitutionMaaPoa;
	}

	public BigDecimal getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinancialInstitutionMaaPoaInstitutionEntity other = (FinancialInstitutionMaaPoaInstitutionEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "FinancialInstitutionMaaPoaInstitutionEntity [id=" + id + ", version=" + version +
				", percentage=" + percentage +
				"]";
	}

}
