/* 
 * 
 * ContactRelationshipEntity.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.contact;

import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.tarion.vbs.orm.entity.AuditedEntity;

/**
 * Contact Relationship Entity JPA class holds relationship information between escrow agent lawyer, assistant
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-01-09
 * @version 1.0
 */
@NamedQuery(name=ContactRelationshipEntity.SELECT_ALL_RELATIONSHIP_FOR_CONTACT_ID, query = ContactRelationshipEntity.JPQL_ALL_RELATIONSHIP_FOR_CONTACT_ID)
@NamedQuery(name=ContactRelationshipEntity.SELECT_RELATIONSHIP_FOR_CRM_REL_ID, query = ContactRelationshipEntity.JPQL_RELATIONSHIP_FOR_CRM_REL_ID)
@Entity
@Table(name = "CONTACT_RELATIONSHIP")
@Audited(withModifiedFlag=true)
public class ContactRelationshipEntity implements AuditedEntity {
	private static final long serialVersionUID = 1L;
	
	public static final String SELECT_ALL_RELATIONSHIP_FOR_CONTACT_ID = "ContactRelationshipEntity.SELECT_ALL_RELATIONSHIP_FOR_CONTACT_ID";
	protected static final String JPQL_ALL_RELATIONSHIP_FOR_CONTACT_ID = "SELECT o FROM ContactRelationshipEntity o WHERE o.fromContact.id = ?1  ORDER BY o.startDate ";

	public static final String SELECT_RELATIONSHIP_FOR_CRM_REL_ID = "ContactRelationshipEntity.SELECT_RELATIONSHIP_FOR_CRM_REL_ID";
	protected static final String JPQL_RELATIONSHIP_FOR_CRM_REL_ID = "SELECT o FROM ContactRelationshipEntity o WHERE o.crmRelId = ?1 ";

	@Id
    @Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "FROM_CONTACT_ID")
	private ContactEntity fromContact;
   
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "TO_CONTACT_ID")
	private ContactEntity toContact;

	@ManyToOne(optional = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "RELATIONSHIP_TYPE_ID")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private RelationshipTypeEntity relationshipType;

    @Column(name = "START_DATE")
	private LocalDateTime startDate;
    
    @Column(name = "END_DATE")
	private LocalDateTime endDate;

	@Column(name = "CRM_REL_ID")
	private String crmRelId;

    @Column(name = "CREATE_DATE")
	private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
	
	public ContactEntity getFromContact() {
		return fromContact;
	}

	public void setFromContact(ContactEntity fromContact) {
		this.fromContact = fromContact;
	}

	public ContactEntity getToContact() {
		return toContact;
	}

	public void setToContact(ContactEntity toContact) {
		this.toContact = toContact;
	}

	public RelationshipTypeEntity getRelationshipType() {
		return relationshipType;
	}

	public void setRelationshipType(RelationshipTypeEntity relationshipType) {
		this.relationshipType = relationshipType;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public String getCrmRelId() {
		return crmRelId;
	}

	public void setCrmRelId(String crmRelId) {
		this.crmRelId = crmRelId;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContactRelationshipEntity other = (ContactRelationshipEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ContactRelationshipEntity [id=" + id + ", version=" + version + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", createDate=" + createDate + ", createUser=" + createUser + ", updateDate=" + updateDate + ", updateUser=" + updateUser + "]";
	}

}
