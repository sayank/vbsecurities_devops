/* 
 * 
 * BranchEntity.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.orm.entity.financialinstitution;

import com.tarion.vbs.orm.entity.AddressEntity;
import com.tarion.vbs.orm.entity.AuditedEntity;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Branch Entity JPA class holds information about Financial Institution's Branch
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-11-01
 * @version 1.0
 */
@NamedQuery(name=BranchEntity.SELECT_ALL_BRANCHES_FOR_FINANCIAL_INSTITUTION, query = BranchEntity.JPQL_ALL_BRANCHES_FOR_FINANCIAL_INSTITUTION)
@NamedQuery(name=BranchEntity.SELECT_ALL_BRANCHES, query = BranchEntity.JPQL_ALL_BRANCHES)

@Entity
@Table(name = "BRANCH")
@Audited(withModifiedFlag=true)
public class BranchEntity implements AuditedEntity {
	private static final long serialVersionUID = 1L;

	public static final String SELECT_ALL_BRANCHES_FOR_FINANCIAL_INSTITUTION = "BranchEntity.SELECT_ALL_BRANCHES_FOR_FINANCIAL_INSTITUTION";	
	protected static final String JPQL_ALL_BRANCHES_FOR_FINANCIAL_INSTITUTION = "SELECT o FROM BranchEntity o where o.financialInstitution.id =  ?1 ORDER BY o.id ASC ";

	public static final String SELECT_ALL_BRANCHES = "BranchEntity.SELECT_ALL_BRANCHES";	
	protected static final String JPQL_ALL_BRANCHES = "SELECT o FROM BranchEntity o ORDER BY o.id ASC ";

	@Id
    @Column(name = "ID")
	// PK is CRM_CONTACT_ID and since it cannot be VB, it is Long
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
    @Column(name = "VERSION")
	private Long version;
   
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "FINANCIAL_INSTITUTION_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private FinancialInstitutionEntity financialInstitution;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "ADDRESS_ID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = true)
	private AddressEntity address;

	@Column(name = "NAME")
	private String name;
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "CREATE_DATE")
    private LocalDateTime createDate;
	@Column(name = "CREATE_USER")
	private String createUser;
	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;
	@Column(name = "UPDATE_USER")
	private String updateUser;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	
	public FinancialInstitutionEntity getFinancialInstitution() {
		return financialInstitution;
	}
	public void setFinancialInstitution(FinancialInstitutionEntity financialInstitution) {
		this.financialInstitution = financialInstitution;
	}
	public AddressEntity getAddress() {
		return address;
	}
	public void setAddress(AddressEntity address) {
		this.address = address;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BranchEntity other = (BranchEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "BranchEntity [id=" + id + ", version=" + version +
				", name=" + name +
				", description=" + description +
				"]";
	}	

}
