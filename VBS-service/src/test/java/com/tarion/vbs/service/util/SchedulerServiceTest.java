/* 
 * 
 * SchedulerServiceTest.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import static org.junit.Assert.assertNotNull;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.common.dto.interest.InterestRateDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.service.interest.InterestServiceImpl;
import com.tarion.vbs.service.interest.InterestServiceRemote;
import com.tarion.vbs.service.test.Container;

public class SchedulerServiceTest {

	private SchedulerService schedulerService;
	private UserTransaction userTransaction;
	private InterestServiceRemote interestService;
//	private InterestDAO interestDAO;
	
	@Before
	public void lookupABean() throws NamingException {
//		InitialContext ctx = new InitialContext();
//		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
//		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		schedulerService = Container.getBean(SchedulerService.class);
		interestService = Container.getBean(InterestServiceImpl.class);
//		interestDAO = Container.getBean(InterestDAOImpl.class);
	}
	
	@After
	public void cleanUp() {
//		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}
	
	
	@Test
	public void test() {
		assertNotNull(schedulerService);
		assertNotNull(interestService);
	}
	
//	@Test
	public void testTriggerCalculateDailyInterest() throws VbsCheckedException {
		interestService.findDatesRequiringInterestCalculation(5);
	}

}
