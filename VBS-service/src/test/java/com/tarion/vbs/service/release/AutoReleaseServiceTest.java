package com.tarion.vbs.service.release;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.autorelease.AutoReleaseResultWrapper;
import com.tarion.vbs.common.dto.autorelease.AutoReleaseRunSearchParametersDTO;
import com.tarion.vbs.common.dto.autorelease.AutoReleaseRunSearchResultDTO;
import com.tarion.vbs.common.enums.SearchTypeEnum;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.dao.util.GenericDAOImpl;
import com.tarion.vbs.orm.entity.autorelease.AutoReleaseEliminationEntity;
import com.tarion.vbs.orm.entity.autorelease.AutoReleaseEnrolmentEntity;
import com.tarion.vbs.orm.entity.autorelease.AutoReleaseTimingTypeEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import com.tarion.vbs.service.jaxb.crm.autorelease.request.AutoReleaseEnrolmentsRequest;
import com.tarion.vbs.service.test.Container;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.junit.Assert.assertNotNull;

public class AutoReleaseServiceTest {

	private AutoReleaseService autoReleaseService;
	private AutoReleaseCriteriaService autoReleaseCriteriaService;
	private UserTransaction userTransaction;
	private GenericDAO genericDAO;

	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		autoReleaseService = Container.getBean(AutoReleaseServiceImpl.class);
		autoReleaseCriteriaService = Container.getBean(AutoReleaseCriteriaServiceImpl.class);
		genericDAO = Container.getDAO(GenericDAOImpl.class);
	}
	
	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}
	
//	@Test
//	public void testJaxbConversion() {
//		AutoReleaseEnrolmentsRequest jaxb = new AutoReleaseEnrolmentsRequest();
//		jaxb.setAutoReleaseRunId(10l);
//		jaxb.setEnrolments(new AutoReleaseEnrolmentsRequest.Enrolments());
//		jaxb.getEnrolments().getEnrolmentNumber().add("H12345");
//
//		String xml = VbsUtil.convertJaxbToXml(jaxb, AutoReleaseEnrolmentsRequest.class);
//		assertNotNull(xml);
//	}
	
//	@Test
//	public void testGetRelease() throws VbsCheckedException {
//		AutoReleaseRunSearchParametersDTO params = new AutoReleaseRunSearchParametersDTO();
//		params.setDateSearchType(SearchTypeEnum.BETWEEN);
//		params.setStartDate(DateUtil.getLocalDateTimeStartOfYear(2018));
//		params.setEndDate(DateUtil.getLocalDateTimeNextYear());
//		List<AutoReleaseRunSearchResultDTO> runs = autoReleaseService.searchAutoReleaseRuns(params);
//		assertNotNull(runs);
//	}
//
//	@Test
//	public void testGetAutoReleaseResults() throws VbsCheckedException {
//		AutoReleaseResultWrapper runResult = autoReleaseService.getAutoReleaseResults(1l);
//		assertNotNull(runResult);
//		assertNotNull(runResult.getAutoReleaseFailedResults());
//		assertNotNull(runResult.getAutoReleasePassedResults());
//	}

//	@Test
//	public void testCriteria() {
//		AutoReleaseEnrolmentEntity entity = genericDAO.findEntityById(AutoReleaseEnrolmentEntity.class, 41086L);
//		autoReleaseCriteriaService.evaluateCriteria(entity);
//	}


//	@Test
//	public void testSaveAutoReleaseEnrolments() throws VbsCheckedException {
//		List<AutoReleaseResultDTO> list = new ArrayList<>();
//		AutoReleaseResultDTO dto = new AutoReleaseResultDTO();
//		dto.setId(1l);
//		dto.setDeletedBy("UnitTest");
//		list.add(dto);
//		autoReleaseService.saveAutoReleaseEnrolments(list, VbsUtil.getSystemUserDTO());
//	}

	@Test
	public void testSaveAutoReleaseEnrolments() throws VbsCheckedException {
			SecurityEntity security = genericDAO.findEntityById(SecurityEntity.class, 35966l);
			EnrolmentEntity enrolment = genericDAO.findEntityById(EnrolmentEntity.class, "H2118381");
			long daysTwoYear = genericDAO.findEntityById(AutoReleaseTimingTypeEntity.class, VbsConstants.AUTO_RELEASE_TIMING_TYPE_TWO_YEAR).getNumberOfDays();
			LocalDateTime date = security.getReceivedDate();
			LocalDateTime wsd = enrolment.getWarrantyStartDate();
			if(date == null && wsd == null) {

			}
			if(date == null || wsd.isAfter(date)) {
				date = wsd;
			}

			date = date.truncatedTo(ChronoUnit.DAYS);

			long daysPossessed = Duration.between(date, DateUtil.getLocalDateTimeStartOfDay()).toDays();

			boolean possessed = daysPossessed > daysTwoYear;
	}

	// TODO fix this
	//@Test
	public void testStartAutoRelease() throws VbsCheckedException {

		if(!autoReleaseService.existsRunInProgress()) {
			long startTime = System.nanoTime();

			autoReleaseService.startAutoReleaseProcess(VbsUtil.getSystemUserDTO());

			long endTime = System.nanoTime();
			long timeElapsed = (endTime - startTime)/1000;
			System.out.println("Execution time in ms  : " + timeElapsed);

		}
	}
}