/* 
 * 
 * BsaQueueResponseProcessorTest.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.common.dto.release.ReleaseDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.service.test.Container;

public class BsaQueueResponseProcessorTest {

	private UserTransaction userTransaction;
	private BsaQueueRequestProcessor bsaQueueResponseProcessor;
	
//	private String xmlFmsResponse = "<?xml version=\"1.0\"?><TWC_SEC_STAT_V2><FieldTypes><PSCAMA class=\"R\"><LANGUAGE_CD type=\"CHAR\"/><AUDIT_ACTN type=\"CHAR\"/><BASE_LANGUAGE_CD type=\"CHAR\"/><MSG_SEQ_FLG type=\"CHAR\"/><PROCESS_INSTANCE type=\"NUMBER\"/><PUBLISH_RULE_ID type=\"CHAR\"/><MSGNODENAME type=\"CHAR\"/></PSCAMA></FieldTypes><MsgData><Transaction><TWC_SEC_STAT class=\"R\"><TWC_SECURITY_NBR>12345</TWC_SECURITY_NBR><COMPANYID>B12345</COMPANYID><TWC_UNPOST_REASON>STALE_DATE</TWC_UNPOST_REASON><DESCR>Stale Dated Cheques AAA</DESCR><ROW_ADDED_DTTM>2018-12-14-10.59.40.000000</ROW_ADDED_DTTM></TWC_SEC_STAT></Transaction></MsgData></TWC_SEC_STAT_V2>";
	private String xmlCashSecurityCreateRequest = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
			"<createCashSecurityRequest>\n" + 
			"   <requestId>9019</requestId>\n" + 
			"   <vbNumber>B27794</vbNumber>\n" + 
			"   <instrumentNumber>FIT_B27794_DD99679</instrumentNumber>\n" + 
			"   <enrolments>\n" + 
			"      <enrolmentNumber>H2264371</enrolmentNumber>\n" + 
			"   </enrolments>\n" + 
			"   <amount>5000.0</amount>\n" + 
			"   <createDate>2019-04-29</createDate>\n" + 
			"   <createUser>bojan</createUser>\n" + 
			"</createCashSecurityRequest>\n" + 
			"";
	
	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		bsaQueueResponseProcessor = Container.getBean(BsaQueueRequestProcessorImpl.class);
	}
	
	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}


	@Test
	public void test() {
		assertNotNull(bsaQueueResponseProcessor);
	}

	@Test
	public void testResponseCreateCashSecurity() {
		bsaQueueResponseProcessor.processReceivedBsaXml(xmlCashSecurityCreateRequest);
	}

}
