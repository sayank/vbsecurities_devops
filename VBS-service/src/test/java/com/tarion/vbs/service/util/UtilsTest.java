/* 
 * 
 * UtilsTest.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.Date;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;
import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.common.util.DateUtil;

public class UtilsTest {

	private UserTransaction userTransaction;
	
	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
	}
	
	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}
	
	
	@Test
	public void testDateUtilGetLocalDateTimeStartOfCurrentMonth() {
		LocalDateTime startOfCurrentMonth = DateUtil.getLocalDateTimeStartOfCurrentMonth();
		assertTrue(DateUtil.getLocalDateTimeNow().isAfter(startOfCurrentMonth));
		assertTrue(DateUtil.getLocalDateTimeNow().minusMonths(1l).isBefore(startOfCurrentMonth));
	}
	
	@Test
	public void testDateUtilGetLocalDateTimeStartOfYear() {
		LocalDateTime startOf2019 = DateUtil.getLocalDateTimeStartOfYear(2019);
		assertTrue(startOf2019.isBefore(DateUtil.getLocalDateTimeStartOfYear(2020)));
		assertTrue(startOf2019.isAfter(DateUtil.getLocalDateTimeStartOfYear(2018)));
	}
	
	@Test
	public void testDateUtil() {
		Date javaUtilDate = DateUtil.getDateFromLocalDateTime(DateUtil.getLocalDateTimeNow());
		assertNotNull(javaUtilDate);
		XMLGregorianCalendar xmlGregorianCalendar = DateUtil.getXMLGregorianCalendarFromLocalDateTime(DateUtil.getLocalDateTimeNow());
		assertNotNull(xmlGregorianCalendar);
		
//		long timestamp = 1471432246347l;
//		Date testDate = DateUtil.convertEnversRevisionDateToEasternTime(new Date(timestamp));
//		assertNotNull(testDate);
	}
	
	
}
