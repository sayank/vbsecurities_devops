/* 
 * 
 * SecurityServiceTest.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.security;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.common.dto.security.HistoryFieldDTO;
import com.tarion.vbs.common.dto.security.SecurityDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.service.test.Container;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.naming.NamingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class HistoryTest {

	private SecurityService securityService;
	SecurityDTO testSecurity;
	
	@Before
	public void lookupABean() throws NamingException {
		securityService = Container.getBean(SecurityServiceImpl.class);
		testSecurity = createNewSecurityDTO();
		Long securityId = securityService.saveSecurity(testSecurity, new ArrayList<>(), VbsUtil.getSystemUserDTO());
		testSecurity = securityService.getSecurity(securityId);
	}
	
	@After
	public void cleanUp() {
		try {
			securityService.deleteSecurity(testSecurity.getId(), VbsUtil.getSystemUserDTO());
		} catch (VbsCheckedException e) {
			//TODO
			fail();
		}
	}
	
	
	@Test
	public void testCommentHistory() {
		int counter = 1;

		String testComment = "Test" + counter++;
		testSecurity.setComment(testComment);
		securityService.saveSecurity(testSecurity, new ArrayList<>(), VbsUtil.getSystemUserDTO());
		List<HistoryFieldDTO> comments = securityService.getSecurityCommentHistory(testSecurity.getId());
		assertTrue(comments.size() == counter);
		assertTrue(testComment.equalsIgnoreCase(comments.get(0).getFieldValue()));

		comments = securityService.getSecurityCommentHistory(testSecurity.getId());
		assertTrue(comments.size() == counter);
		assertTrue(testComment.equalsIgnoreCase(comments.get(0).getFieldValue()));

		testComment = "Test" + counter++;
		testSecurity.setComment(testComment);
		securityService.saveSecurity(testSecurity, new ArrayList<>(), VbsUtil.getSystemUserDTO());
		comments = securityService.getSecurityCommentHistory(testSecurity.getId());
		assertTrue(comments.size() == counter);
		assertTrue(testComment.equalsIgnoreCase(comments.get(0).getFieldValue()));

		comments = securityService.getSecurityCommentHistory(testSecurity.getId());
		assertTrue(comments.size() == counter);
		assertTrue(testComment.equalsIgnoreCase(comments.get(0).getFieldValue()));

		// now let's not modify comment, and counter remains the same
		testSecurity.setInstrumentNumber("RA 33333");
		securityService.saveSecurity(testSecurity, new ArrayList<>(), VbsUtil.getSystemUserDTO());
		comments = securityService.getSecurityCommentHistory(testSecurity.getId());
		assertTrue(comments.size() == counter);
		assertTrue(testComment.equalsIgnoreCase(comments.get(0).getFieldValue()));

		testSecurity.setInstrumentNumber("RA 55555");
		securityService.saveSecurity(testSecurity, new ArrayList<>(), VbsUtil.getSystemUserDTO());
		comments = securityService.getSecurityCommentHistory(testSecurity.getId());
		assertTrue(comments.size() == counter);
		assertTrue(testComment.equalsIgnoreCase(comments.get(0).getFieldValue()));

		testComment = "Test" + counter++;
		testSecurity.setComment(testComment);
		securityService.saveSecurity(testSecurity, new ArrayList<>(), VbsUtil.getSystemUserDTO());
		comments = securityService.getSecurityCommentHistory(testSecurity.getId());
		assertTrue(comments.size() == counter);
		assertTrue(testComment.equalsIgnoreCase(comments.get(0).getFieldValue()));
	}
	
	private SecurityDTO createNewSecurityDTO() {
		SecurityDTO securityDTO = new SecurityDTO();
		securityDTO.setReceivedDate(DateUtil.getLocalDateTimeNow());
		securityDTO.setIssuedDate(DateUtil.getLocalDateTimeNow());
		securityDTO.setOriginalAmount(BigDecimal.valueOf(10L));
		securityDTO.setSecurityTypeId(1L);
		securityDTO.setSecurityPurposeId(1L);
		securityDTO.setInstrumentNumber("RA 12345");
		securityDTO.setUnitsToCover(1);
		securityDTO.setPoolId(368L);
		securityDTO.setCreateUser(VbsConstants.VBS_SYSTEM_USER_ID);
		securityDTO.setCreateDate(DateUtil.getLocalDateTimeNow());
		List<ContactDTO> vbList = new ArrayList<>();
		vbList.add(createPrimaryVb());
		securityDTO.setVbList(vbList);
		
		return securityDTO;
	}
	
	private ContactDTO createPrimaryVb() {
		ContactDTO vbDTO = new ContactDTO();
		vbDTO.setPrimaryVb(true);
		vbDTO.setCrmContactId("B24332");
		vbDTO.setDeleteReason(null);
		return vbDTO;
	}
	
}
