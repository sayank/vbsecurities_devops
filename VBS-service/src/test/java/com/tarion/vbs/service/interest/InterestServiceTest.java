/* 
 * 
 * InterestServiceTest.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.interest;

import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.dto.interest.InterestRateDTO;
import com.tarion.vbs.common.dto.interest.InterestSearchResultDTO;
import com.tarion.vbs.common.dto.interest.SecurityInterestSearchParamsDTO;
import com.tarion.vbs.common.dto.security.CashSecurityNoInterestDTO;
import com.tarion.vbs.common.enums.InterestPeriodTypeEnum;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.service.test.Container;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

public class InterestServiceTest {

	private InterestService interestService;
	private UserTransaction userTransaction;

	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {
			userTransaction.begin();
		} catch (Exception e) {
			e.printStackTrace();
		}
		interestService = Container.getBean(InterestServiceImpl.class);
	}

	@After
	public void cleanUp() {
		try {
			userTransaction.rollback();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test() {
		assertNotNull(interestService);
	}

	@Test
	public void testGetAllRates() {
		List<InterestRateDTO> rates = interestService.getAllInterestRates();
		assertNotNull(rates);
	}

	@Test
	public void testValidateRate() {
		InterestRateDTO rate = createInterestRateDTO();
		List<ErrorDTO> errors = interestService.validateInterestRate(rate);
		assertNotNull(errors);
		assertTrue(errors.isEmpty());
	}

	@Test
	public void testAddRate() {
		InterestRateDTO rate = createInterestRateDTO();
		List<InterestRateDTO> rateList = interestService.saveInterestRate(rate, VbsUtil.getSystemUserDTO());
		rate = rateList.get(0);
		assertNotNull(rate);
		assertNotNull(rate.getId());
	}

	@Test
	public void testUpdateRate() {
		InterestRateDTO rate = createInterestRateDTO();
		List<InterestRateDTO> rateList = interestService.saveInterestRate(rate, VbsUtil.getSystemUserDTO());
		rate = rateList.get(0);

		//TODO re-write this test to not test update with end date.
		//rate.setInterestEndDate(DateUtil.getLocalDateTimeNow().plusDays(5l));
		rateList = interestService.saveInterestRate(rate, VbsUtil.getSystemUserDTO());
		assertNotNull(rateList);
	}

	/*
	@Test
	public void testGetInterestRatesForYear() {
		InterestRateDTO rate = createInterestRateDTO();
		try {
			interestService.saveInterestRate(rate, VbsUtil.getSystemUserDTO());
 			List<InterestRateDTO> rates = interestService.getInterestRatesForYear(DateUtil.getLocalDateTimeNow().getYear());
			assertTrue(!rates.isEmpty());
 			rates = interestService.getInterestRatesForYear(2017);
			assertTrue(rates.size() <= 12);
 			rates = interestService.getInterestRatesForYear(2016);
			assertTrue(rates.size() <= 12);
		} catch (VbsCheckedException e) {
			e.printStackTrace();
			fail();
		}
	}
	*/

	@Test
	public void testSearchInterestDaily() {
		Long securityId = 37575L;
		LocalDateTime fromDate = DateUtil.getLocalDateTimeStartOfCurrentMonth().minusMonths(15L);
		LocalDateTime toDate = DateUtil.getLocalDateTimeNow().plusMonths(1L);
		List<InterestSearchResultDTO> list = interestService.searchInterest(new SecurityInterestSearchParamsDTO(securityId, fromDate, toDate, InterestPeriodTypeEnum.DAILY));
		assertFalse(list.isEmpty());
	}

	@Test
	public void testSearchInterestWeekly() {
		Long securityId = 37575L;
		LocalDateTime fromDate = DateUtil.getLocalDateTimeStartOfCurrentMonth().minusMonths(15L);
		LocalDateTime toDate = DateUtil.getLocalDateTimeNow().plusMonths(1L);
		List<InterestSearchResultDTO> list = interestService.searchInterest(new SecurityInterestSearchParamsDTO(securityId, fromDate, toDate, InterestPeriodTypeEnum.WEEKLY));
		assertFalse(list.isEmpty());
	}
	
	@Test
	public void testSearchInterestMonthly() {
		Long securityId = 37575L;
		LocalDateTime fromDate = DateUtil.getLocalDateTimeStartOfCurrentMonth().minusMonths(15L);
		LocalDateTime toDate = DateUtil.getLocalDateTimeNow().plusMonths(1L);
		List<InterestSearchResultDTO> list = interestService.searchInterest(new SecurityInterestSearchParamsDTO(securityId, fromDate, toDate, InterestPeriodTypeEnum.MONTHLY));
		assertFalse(list.isEmpty());
	}
	
	@Test
	public void testSearchInterestQuarterly() {
		Long securityId = 37575L;
		LocalDateTime fromDate = DateUtil.getLocalDateTimeStartOfCurrentMonth().minusMonths(15L);
		LocalDateTime toDate = DateUtil.getLocalDateTimeNow().plusMonths(1L);
		List<InterestSearchResultDTO> list = interestService.searchInterest(new SecurityInterestSearchParamsDTO(securityId, fromDate, toDate, InterestPeriodTypeEnum.QUARTERLY));
		assertFalse(list.isEmpty());
	}



	private InterestRateDTO createInterestRateDTO() {
		InterestRateDTO currentRate = null;
		List<InterestRateDTO> rates = interestService.getAllInterestRates();
		if (!rates.isEmpty()) {
			currentRate = rates.get(0);
		}
		InterestRateDTO rate = new InterestRateDTO();
		if (currentRate != null) {

				if (currentRate.getInterestStartDate().isAfter(DateUtil.getLocalDateTimeNow())) {
					rate.setInterestStartDate(currentRate.getInterestStartDate().plusMonths(1L));
				} else {
					rate.setInterestStartDate(DateUtil.getLocalDateTimeNow());
				}

		} else {
			rate.setInterestStartDate(DateUtil.getLocalDateTimeNow().minusDays(2L));
		}
		rate.setAnnualRate(new BigDecimal("1.25"));
		return rate;
	}
	
	@Test void testGetCashSecuritiesWithoutInterest() {
		List<CashSecurityNoInterestDTO> dtos = interestService.getCashSecuritiesWithoutInterest();
		assertNotNull(dtos);
		assertFalse(dtos.isEmpty());
	}

}
