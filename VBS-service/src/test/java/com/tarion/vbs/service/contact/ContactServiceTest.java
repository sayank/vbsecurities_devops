package com.tarion.vbs.service.contact;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import com.tarion.vbs.common.enums.CRMSearchTypeEnum;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.service.test.Container;

public class ContactServiceTest {

	private ContactService contactService;
	private UserTransaction userTransaction;

	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {
			userTransaction.begin();
		} catch (Exception e) {
			e.printStackTrace();
		}
		contactService = Container.getBean(ContactServiceImpl.class);
	}

	@After
	public void cleanUp() {
		try {
			userTransaction.rollback();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test() {
		assertNotNull(contactService);
	}
	
	// For some reason Mattamy is not present in dev3!!!
//	@Test
	public void testFindPersonOrCompanyInCrm() {
		assertNotNull(contactService);
		List<ContactDTO> contacts = contactService.findPersonOrCompanyInCrm("Mattamy", CRMSearchTypeEnum.CONTAINS);
		assertTrue(!contacts.isEmpty());
	}
}


