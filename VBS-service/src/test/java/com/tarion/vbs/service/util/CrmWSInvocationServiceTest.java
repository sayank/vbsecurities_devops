/* 
 * 
 * SchedulerServiceTest.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import static org.junit.Assert.assertNotNull;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import com.tarion.crm.vbsws.GetSecHomeInfoResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.service.test.Container;

public class CrmWSInvocationServiceTest {

	private UserTransaction userTransaction;
	private CrmWSInvocationService crmWSInvocationService;
	
	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		crmWSInvocationService = Container.getBean(CrmWSInvocationServiceImpl.class);
	}
	
	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}
	
	
	@Test
	public void test() {
		assertNotNull(crmWSInvocationService);
	}
	
	@Test
	public void testGetEnrolment() {
		GetSecHomeInfoResponse.SECHOMEINFO response = crmWSInvocationService.getEnrolment("H2085807");
		assertNotNull(response);
		assertNotNull(response.getENROLMENTNUMBER());
	}
	
//	@Test
//	public void testViewWorklistItem() {
//		ViewWLRequest item = new ViewWLRequest();
//		item.setAPPLICATIONID("");
//		item.setCOMPANYID("");
//		item.setTWCVBAPPWLID("");
//		crmWSInvocationService.viewWorklistItem(item);
//	}

}
