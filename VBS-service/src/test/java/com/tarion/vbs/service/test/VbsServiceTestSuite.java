package com.tarion.vbs.service.test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.tarion.vbs.service.contact.ContactServiceTest;
import com.tarion.vbs.service.enrolment.CrmQueueResponseProcessorTest;
import com.tarion.vbs.service.financialinstitution.FinancialInstitutionServiceTest;
import com.tarion.vbs.service.interest.InterestServiceTest;
import com.tarion.vbs.service.release.AutoReleaseServiceTest;
import com.tarion.vbs.service.release.ReleaseServiceTest;
import com.tarion.vbs.service.security.SecurityServiceTest;
import com.tarion.vbs.service.util.BsaQueueResponseProcessorTest;
import com.tarion.vbs.service.util.CrmWSInvocationServiceTest;
import com.tarion.vbs.service.util.EmailWSInvocationServiceTest;
import com.tarion.vbs.service.util.FmsQueueResponseProcessorTest;
import com.tarion.vbs.service.util.JmsMessageSenderServiceTest;
import com.tarion.vbs.service.util.SchedulerServiceTest;
import com.tarion.vbs.service.util.UtilsTest;
import com.tarion.vbs.service.webservice.VBSWSTest;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	CrmQueueResponseProcessorTest.class,
//	, VBSWSTest.class
//	, BsaQueueResponseProcessorTest.class
//	, InterestServiceTest.class
//	// TODO fix this
////	, HistoryTest.class
//
//	, ReleaseServiceTest.class
//	, FinancialInstitutionServiceTest.class
//	, SecurityServiceTest.class
//	, JmsMessageSenderServiceTest.class
//	, SchedulerServiceTest.class
//	, 
	UtilsTest.class
//	, EmailWSInvocationServiceTest.class
//	, CrmWSInvocationServiceTest.class
//	, ContactServiceTest.class
//	, FmsQueueResponseProcessorTest.class
//	, AutoReleaseServiceTest.class
})
public class VbsServiceTestSuite {

	@BeforeClass
	public static void setUp() {
		Container.getInstance().startTheContainer();
	}

	@AfterClass
	public static void tearDown() {
		Container.getInstance().stopTheContainer();
	}
}
