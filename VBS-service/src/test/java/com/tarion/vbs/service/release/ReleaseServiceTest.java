package com.tarion.vbs.service.release;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.common.constants.JMSConstants;
import com.tarion.vbs.common.dto.release.PendingReleaseDTO;
import com.tarion.vbs.common.dto.release.ReleaseDTO;
import com.tarion.vbs.common.dto.release.ReleaseSearchParametersDTO;
import com.tarion.vbs.common.enums.ReleaseStatus;
import com.tarion.vbs.common.enums.SearchTypeEnum;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.service.test.Container;
import com.tarion.vbs.service.util.PropertyService;
import com.tarion.vbs.service.util.PropertyServiceImpl;

public class ReleaseServiceTest {

	private ReleaseService releaseService;
	private UserTransaction userTransaction;
	private PropertyService propertyService;
	
	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		releaseService = Container.getBean(ReleaseServiceImpl.class);
		propertyService = Container.getBean(PropertyServiceImpl.class);
		propertyService.setProperty(JMSConstants.TEST_JMS_PREFIX, JMSConstants.TEST_JMS_PREFIX_VALUE);
	}
	
	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}
	
	@Test
	public void testGetRelease() throws VbsCheckedException {
		List<ReleaseDTO> releases = releaseService.getReleases(123L);
		assertTrue(!releases.isEmpty());
	}
	
	@Test
	public void testAddRelease() throws VbsCheckedException {
		List<ReleaseDTO> releases = releaseService.getReleases(34681L);
		if (!releases.isEmpty()) {
			ReleaseDTO release = releases.get(0);
			release.setWsInputRequested(true);
			Long releaseId = releaseService.saveRelease(release, VbsUtil.getSystemUserDTO());
			assertNotNull(releaseId);
		}
	}
	
	@Test
	public void testCancelRelease() throws VbsCheckedException {
		List<ReleaseDTO> releases = releaseService.getReleases(34681L);
		for (ReleaseDTO release : releases) {
			if (release.getStatus() == ReleaseStatus.PENDING) {
				Long releaseId = releaseService.cancelRelease(release, VbsUtil.getSystemUserDTO());
				assertNotNull(releaseId);
			} else {
				try {
					Long releaseId = releaseService.cancelRelease(release, VbsUtil.getSystemUserDTO());
				} catch (VbsCheckedException e) {
					// good we cannot cancel non pending release
				}
			}
		}
	}
	
	@Test
	public void testGetReleasesByStatusesFromToDate() throws VbsCheckedException {
		List<ReleaseStatus> releaseStatuses = new ArrayList<>();
		releaseStatuses.add(ReleaseStatus.PENDING);
		releaseStatuses.add(ReleaseStatus.SENT);
		LocalDateTime startDate = DateUtil.getLocalDateTimeNow().minusMonths(5l);
		LocalDateTime endDate = DateUtil.getLocalDateTimeNow();
		List<PendingReleaseDTO> releases = releaseService.getReleasesByStatusesFromToDate(releaseStatuses, startDate, endDate);
		assertNotNull(releases);
		releases = releaseService.getReleasesByStatusesFromToDate(releaseStatuses, null, null);
		assertNotNull(releases);
		releases = releaseService.getReleasesByStatusesFromToDate(releaseStatuses, startDate, null);
		assertNotNull(releases);
		releases = releaseService.getReleasesByStatusesFromToDate(releaseStatuses, null, endDate);
		assertNotNull(releases);
	}

	@Test
	public void testSearchReleases() throws VbsCheckedException {
		ReleaseSearchParametersDTO params = new ReleaseSearchParametersDTO();
		params.setDateSearchType(SearchTypeEnum.BETWEEN);
		params.setReleaseUpdateStartDate(DateUtil.getLocalDateTimeStartOfYear(2019));
		params.setReleaseUpdateEndDate(DateUtil.getLocalDateTimeNextYear());

		List<PendingReleaseDTO> releases = releaseService.searchReleases(params);
		assertNotNull(releases);

		params.setDateSearchType(SearchTypeEnum.GREATER_THAN);
		params.setReleaseUpdateStartDate(DateUtil.getLocalDateTimeStartOfYear(2019));
		params.setReleaseUpdateEndDate(null);

		releases = releaseService.searchReleases(params);
		assertNotNull(releases);

		params.setDateSearchType(SearchTypeEnum.EQUALS_GREATER_THAN);
		params.setReleaseUpdateStartDate(DateUtil.getLocalDateTimeStartOfYear(2019));
		params.setReleaseUpdateEndDate(null);

		releases = releaseService.searchReleases(params);
		assertNotNull(releases);
	}
}