/* 
 * 
 * SecurityServiceTest.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import static org.junit.Assert.assertNotNull;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.service.test.Container;

public class JmsMessageSenderServiceTest {

	private JmsMessageSenderService jmsMessageSenderService;
	private UserTransaction userTransaction;
	
	
	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		jmsMessageSenderService = Container.getBean(JmsMessageSenderServiceImpl.class);
	}
	
	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}
	
	
	@Test
	public void test() {
		assertNotNull(jmsMessageSenderService);
	}
	
	@Test
	public void testAddSecurity() throws VbsCheckedException {
		String trackingNumber = JmsMessageTypeEnum.getFmsCashSecurityRequestTracking(1l, "B12345");
		String xmlMessageText = "XML";
		jmsMessageSenderService.insertTransactionLog(trackingNumber, xmlMessageText);;
	}
	
}
