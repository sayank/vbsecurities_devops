/* 
 * 
 * SecurityServiceTest.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.security;

import com.tarion.vbs.common.constants.JMSConstants;
import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.common.dto.security.DeletedSecuritySearchParametersDTO;
import com.tarion.vbs.common.dto.security.SecurityDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchParametersDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchResultDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchResultWrapper;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.enums.SearchTypeEnum;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.service.test.Container;
import com.tarion.vbs.service.util.PropertyService;
import com.tarion.vbs.service.util.PropertyServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class SecurityServiceTest {

	private SecurityService securityService;
	private UserTransaction userTransaction;
	private PropertyService propertyService;
	
	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		securityService = Container.getBean(SecurityServiceImpl.class);
		propertyService = Container.getBean(PropertyServiceImpl.class);
		propertyService.setProperty(JMSConstants.TEST_JMS_PREFIX, JMSConstants.TEST_JMS_PREFIX_VALUE);
	}
	
	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}
	
	
	@Test
	public void test() {
		assertNotNull(securityService);
	}
	
	@Test
	public void testSearchSecurity() {
		SecuritySearchParametersDTO securitySearchParametersDTO = new SecuritySearchParametersDTO();
		securitySearchParametersDTO.setCurrentAmount(new BigDecimal(5000l));
		securitySearchParametersDTO.setCurrentAmountType(SearchTypeEnum.EQUALS);
		SecuritySearchResultWrapper result = securityService.searchSecurity(securitySearchParametersDTO, new UserDTO());
		assertNotNull(result);
	}
	
	// TODO fix this test it is failing with index out of bounds
//	@Test
//	public void testAddSecurity() throws VbsCheckedException {
//		SecurityDTO securityDTO = createNewSecurityDTO();
//		List<ErrorDTO> errors = securityService.validateSecurity(securityDTO);
//		if (errors.isEmpty()) {
//			SecurityDTO newSecurity = securityService.addSecurity(securityDTO, VbsUtil.getSystemUserDTO());
//			assertNotNull(newSecurity);
//			assertNotNull(newSecurity.getPrimaryVb());
//			try {
//				VbDTO primaryVb = securityService.getPrimaryVbForPoolId(newSecurity.getPoolId());
//				assertNotNull(primaryVb);
//				List<VbDTO> vbs = securityService.getVbsForPoolId(newSecurity.getPoolId());
//				assertNotNull(vbs);
//				assertTrue(vbs.size() == 4);
//				boolean primaryFound = false;
//				for (VbDTO vb : vbs) {
//					if (vb.isPrimaryVb()) {
//						primaryFound = true;
//						break;
//					}
//				}
//				assertTrue(primaryFound);
//				assertTrue(newSecurity.getPrimaryVb() != null);
//			} catch (VbsCheckedException e) {
//				e.printStackTrace();
//				fail();
//			}
//			
//		}
//	}
	
	// TODO fix this test it is failing with index out of bounds
//	@Test
//	public void testUnitsToCoverOnSecurity() throws VbsCheckedException {
//		long testUnitToCover = 100l;
//		SecurityDTO securityDTO = createNewSecurityDTO();
//		List<ErrorDTO> errors = securityService.validateSecurity(securityDTO);
//		if (errors.isEmpty()) {
//			SecurityDTO newSecurity = securityService.addSecurity(securityDTO, VbsUtil.getSystemUserDTO());
//			newSecurity.setComment("Test");
//			assertNotNull(newSecurity.getPrimaryVb());
//			try {
//				assertTrue(newSecurity.getUnitsToCover() == 4L);
//				newSecurity.setUnitsToCover(testUnitToCover);
//				SecurityDTO updatedSecurity = securityService.updateSecurity(newSecurity, VbsUtil.getSystemUserDTO());
//				assertTrue(updatedSecurity.getUnitsToCover() == testUnitToCover);
//			} catch (VbsCheckedException e) {
//				e.printStackTrace();
//				fail();
//			}
//		}
//	}
	
	// TODO fix this test it is failing with index out of bounds
//	@Test
//	public void testSimpleUpdateSecurity() throws VbsCheckedException {
//		SecurityDTO securityDTO = createNewSecurityDTO();
//		List<ErrorDTO> errors = securityService.validateSecurity(securityDTO);
//		if (errors.isEmpty()) {
//			SecurityDTO newSecurity = securityService.addSecurity(securityDTO, VbsUtil.getSystemUserDTO());
//			newSecurity.setComment("Test");
//			assertNotNull(newSecurity.getPrimaryVb());
//			try {
//				SecurityDTO updatedSecurity = securityService.updateSecurity(newSecurity, VbsUtil.getSystemUserDTO());
//				assertNotNull(updatedSecurity);
//				assertTrue("Test".equals(updatedSecurity.getComment()));
//			} catch (VbsCheckedException e) {
//				e.printStackTrace();
//				fail();
//			}
//		}
//	}
	
	// TODO fix this test it is failing with index out of bounds
//	@Test
//	public void testUpdateDeleteVbsInSecurity() throws VbsCheckedException {
//		SecurityDTO securityDTO = createNewSecurityDTO();
//		List<ErrorDTO> errors = securityService.validateSecurity(securityDTO);
//		if (errors.isEmpty()) {
//			SecurityDTO security = securityService.addSecurity(securityDTO, VbsUtil.getSystemUserDTO());
//			try {
//				// test deleting VBs
//				// delete primary should throw error
//				security.getPrimaryVb().setDeleteReason(1l);
//				assertTrue(!securityService.validateSecurity(security).isEmpty());
//
//				security.getPrimaryVb().setDeleteReason(null);
//				// test delete non primary
//				for (VbDTO vb : security.getVbList()) {
//					if (!vb.isPrimaryVb()) {
//						vb.setDeleteReason(1l);
//						break;
//					}
//				}
//				security = securityService.updateSecurity(security, VbsUtil.getSystemUserDTO());
//				assertTrue(security.getVbList().size() == 3);
//				assertTrue(security.getPrimaryVb() != null);
//				
//				// test undelete non primary
//				security.getVbList().add(createNonPrimaryVb());
//				security = securityService.updateSecurity(security, VbsUtil.getSystemUserDTO());
//				assertTrue(security.getVbList().size() == 4);
//				assertTrue(security.getPrimaryVb() != null);		
//			} catch (VbsCheckedException e) {
//				e.printStackTrace();
//				fail();
//			}
//		}
//	}
	

	// TODO try to fix this test for some reason it is throwing an exception
//	@Test
//	public void testUpdateAddVbsInSecurity() throws VbsCheckedException {
//		SecurityDTO securityDTO = createNewSecurityDTO();
//		List<ErrorDTO> errors = securityService.validateSecurity(securityDTO);
//		if (errors.isEmpty()) {
//			SecurityDTO security = securityService.addSecurity(securityDTO, VbsUtil.getSystemUserDTO());
//			try {
//				// test add non primary
//				security.getVbList().add(createAnotherNonPrimaryVb());
//				security = securityService.updateSecurity(security, VbsUtil.getSystemUserDTO());
//				assertTrue(security.getVbList().size() == 5);
//				assertTrue(security.getPrimaryVb() != null);
//			} catch (VbsCheckedException e) {
//				e.printStackTrace();
//				fail();
//			}
//		}
//	}
	
	private SecurityDTO createNewSecurityDTO() {
		SecurityDTO securityDTO = new SecurityDTO();
		securityDTO.setReceivedDate(DateUtil.getLocalDateTimeNow());
		securityDTO.setIssuedDate(DateUtil.getLocalDateTimeNow());
		securityDTO.setOriginalAmount(new BigDecimal("10"));
		securityDTO.setSecurityTypeId(1L);
		securityDTO.setSecurityPurposeId(1L);
		securityDTO.setInstrumentNumber("RA 12345");
		securityDTO.setUnitsToCover(1);
		securityDTO.setPoolId(368L);
		securityDTO.setCreateUser(VbsConstants.VBS_SYSTEM_USER_ID);
		securityDTO.setCreateDate(DateUtil.getLocalDateTimeNow());
		List<ContactDTO> vbList = new ArrayList<>();
		vbList.add(createPrimaryVb());
		
		vbList.add(createNonPrimaryVb());
		
		securityDTO.setVbList(vbList);
		
		return securityDTO;
	}
	
	private ContactDTO createPrimaryVb() {
		ContactDTO vbDTO = new ContactDTO();
		vbDTO.setPrimaryVb(true);
		vbDTO.setCrmContactId("B24332");
		vbDTO.setDeleteReason(null);
		return vbDTO;
	}
	
	private ContactDTO createNonPrimaryVb() {
		ContactDTO vbDTO = new ContactDTO();
		vbDTO.setPrimaryVb(false);
		vbDTO.setCrmContactId("B10005");
		vbDTO.setDeleteReason(null);
		return vbDTO;
	}
	
	private ContactDTO createAnotherNonPrimaryVb() {
		ContactDTO vbDTO = new ContactDTO();
		vbDTO.setPrimaryVb(false);
		vbDTO.setCrmContactId("B1234");
		vbDTO.setDeleteReason(null);
		return vbDTO;
	}
	
	@Test
	public void testSearchDeletedSecurity() {
		DeletedSecuritySearchParametersDTO searchParams = new DeletedSecuritySearchParametersDTO();
		searchParams.setUpdateDateType(SearchTypeEnum.BETWEEN);
		searchParams.setUpdateDate(DateUtil.getLocalDateTimeNow().minusYears(3));
		searchParams.setUpdateDateBetweenTo(DateUtil.getLocalDateTimeNow());
		List<SecuritySearchResultDTO> result = securityService.searchDeletedSecurity(searchParams, new UserDTO());
		assertNotNull(result);
//		assertTrue(result.size() > 0);
	}

}
