package com.tarion.vbs.service.webservice;

import static org.junit.Assert.assertNotNull;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.common.dto.security.AmountsDTO;
import com.tarion.vbs.common.dto.security.SecurityPurposeDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchResultWrapper;
import com.tarion.vbs.service.test.Container;

public class VBSWSTest {

	private VBSWS vbsWS;
	private UserTransaction userTransaction;

	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {
			userTransaction.begin();
		} catch (Exception e) {
			e.printStackTrace();
		}
		vbsWS = Container.getBean(VBSWS.class);
	}

	@After
	public void cleanUp() {
		try {
			userTransaction.rollback();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test() {
		assertNotNull(vbsWS);
	}
	
	@Test
	public void testGetSecuritiesForVbNumber() {
		String vbNumber = "B12";
		SecuritySearchResultWrapper securities = vbsWS.getSecuritiesForVbNumber(vbNumber);
		assertNotNull(securities);
	}
	
	@Test
	public void testGetSecuritiesForInstrumentNumber() {
		String instrumentNumber = "9484883-03";
		SecuritySearchResultWrapper securities = vbsWS.getSecuritiesForInstrumentNumber(instrumentNumber);
		assertNotNull(securities);
	}

	@Test
	public void testGetSecuritiesForEnrolment() {
		String enrolmentNumber = "H1000039";
		SecuritySearchResultWrapper securities = vbsWS.getSecuritiesForEnrolment(enrolmentNumber);
		assertNotNull(securities);
	}

	@Test
	public void testGetSecurityAmountsForVB() {
		String vbNumber = "B12";
		AmountsDTO amounts = vbsWS.getSecurityAmountsForVB(vbNumber);
		assertNotNull(amounts);
	}

	@Test
	public void testGetSecurityAmountsForEnrolment() {
		String enrolmentNumber = "H1000039";
		AmountsDTO amounts = vbsWS.getSecurityAmountsForEnrolment(enrolmentNumber);
		assertNotNull(amounts);
	}

	@Test
	public void testGetSecurityPurpose() {
		String vbNumber = "B12";
		SecuritySearchResultWrapper securities = vbsWS.getSecuritiesForVbNumber(vbNumber);
		if (securities != null && securities.getSecurities() != null && !securities.getSecurities().isEmpty()) {
			SecurityPurposeDTO purpose = vbsWS.getSecurityPurpose(securities.getSecurities().get(0).getSecurityNumber().toString());
			assertNotNull(purpose);
		}
	}
}


