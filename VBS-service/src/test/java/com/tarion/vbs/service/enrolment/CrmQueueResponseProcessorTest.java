package com.tarion.vbs.service.enrolment;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.common.dto.financialinstitution.BranchDTO;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.contact.ContactDAO;
import com.tarion.vbs.dao.contact.ContactDAOImpl;
import com.tarion.vbs.orm.entity.contact.ContactEntity;
import com.tarion.vbs.service.financialinstitution.FinancialInstitutionService;
import com.tarion.vbs.service.financialinstitution.FinancialInstitutionServiceImpl;
import com.tarion.vbs.service.jaxb.crm.delta.company.CompanyDelta;
import com.tarion.vbs.service.jaxb.crm.delta.enrolment.EnrolmentsDelta;
import com.tarion.vbs.service.jaxb.crm.delta.person.PersonDelta;
import com.tarion.vbs.service.jaxb.crm.warrantyservicesinput.WarrantyServicesRecommendationResponse;
import com.tarion.vbs.service.test.Container;
import com.tarion.vbs.service.util.CrmQueueResponseProcessor;
import com.tarion.vbs.service.util.CrmQueueResponseProcessorImpl;

public class CrmQueueResponseProcessorTest {

	private CrmQueueResponseProcessor crmQueueResponseProcessor;
	private UserTransaction userTransaction;
	private FinancialInstitutionService fiService;
	private ContactDAO contactDAO;

	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {
			userTransaction.begin();
		} catch (Exception e) {
			e.printStackTrace();
		}
		crmQueueResponseProcessor = Container.getBean(CrmQueueResponseProcessorImpl.class);
		fiService = Container.getBean(FinancialInstitutionServiceImpl.class);
		contactDAO = Container.getDAO(ContactDAOImpl.class);
	}

	@After
	public void cleanUp() {
		try {
			userTransaction.rollback();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	@Test
//	public void testCreateEnrolment() {
//		String xml = "<?xml version=\"1.0\"?><VBSdelta><SITEResponse><ENROLMENT_NUMBER>H999999999</ENROLMENT_NUMBER><VB_NUMBER_BUILDER>B46861</VB_NUMBER_BUILDER><VB_NUMBER_VENDOR>B46861</VB_NUMBER_VENDOR><ENROLLING_VB>B46861</ENROLLING_VB><ROW_ADDED_DTTM>2019-01-02-16.34.02.000000</ROW_ADDED_DTTM><ROW_ADDED_OPRID>TDEWITT</ROW_ADDED_OPRID><ROW_LASTMANT_DTTM>2019-01-02-16.34.04.000000</ROW_LASTMANT_DTTM><ROW_LASTMANT_OPRID>TDEWITT</ROW_LASTMANT_OPRID></SITEResponse></VBSdelta>";
//		crmQueueResponseProcessor.processReceivedCrmXml(xml);
//	}
//	
//	@Test
//	public void testCrmProcessCompanyDelta() {
//		String xmlResponse = 
//				"<?xml version=\"1.0\"?> "
//				+ "<companyDelta>"
//				+ "  <company>"
//				+ "    <crmContactId>B12340</crmContactId>"
//				+ "    <companyType>16</companyType>"
//				+ "    <companyName>Corald Developments Ltd.</companyName>"
//				+ "    <licenceStatus>Revoked</licenceStatus>"
//				+ "    <escrowLicenceApplicationStatus></escrowLicenceApplicationStatus>"
//				+ "    <unwilling>N</unwilling>"
//				+ "    <accessibility>N</accessibility>"
//				+ "    <yellowSticky>Y</yellowSticky>"
//				+ "    <alert>0</alert>"
//				+ "    <apVendorFlag>N</apVendorFlag>"
//				+ "    <phoneNumber>(416)684-9237</phoneNumber>"
//				+ "    <phoneExtension></phoneExtension>"
//				+ "  </company>"
//				+ "  <mailingAddress>"
//				+ "    <addressLine1>21 Elizabeth St.Ste.201,BOJAN</addressLine1>"
//				+ "    <addressLine2></addressLine2>"
//				+ "    <addressLine3></addressLine3>"
//				+ "    <addressLine4></addressLine4>"
//				+ "    <city>ST. CATHARINES</city>"
//				+ "    <postalCode>L2R2K8</postalCode> "
//				+ "    <province>ON</province>"
//				+ "    <country>CAN</country>"
//				+ "    <createDate>2019-03-27-10.17.50.000000</createDate>"
//				+ "    <createUser>RMORRIS</createUser>"
//				+ "    <updateDate>2019-03-27-10.17.50.000000</updateDate>"
//				+ "    <updateUser>RMORRIS</updateUser>"
//				+ "  </mailingAddress>"
//				+ "  <relationships />"
//				+ "</companyDelta>";
//
//		CompanyDelta response = (CompanyDelta)VbsUtil.convertXmltoJaxb(xmlResponse);
//		crmQueueResponseProcessor.process(response, xmlResponse);
//	}
//	
//	@Test
//	public void testCrmProcessCompanyBranchDelta() {
//		String xmlResponse = 
//				"<?xml version=\"1.0\"?> "
//				+ "<companyDelta>"
//				+ "  <company>"
//				+ "    <crmContactId>99999901</crmContactId>"
//				+ "    <financialInstitutionId>22</financialInstitutionId>"
//				+ "    <companyType>16</companyType>"
//				+ "    <companyName>Corald Developments Ltd.</companyName>"
//				+ "    <licenceStatus>Revoked</licenceStatus>"
//				+ "    <escrowLicenceApplicationStatus></escrowLicenceApplicationStatus>"
//				+ "    <unwilling>N</unwilling>"
//				+ "    <accessibility>N</accessibility>"
//				+ "    <yellowSticky>Y</yellowSticky>"
//				+ "    <alert>0</alert>"
//				+ "    <apVendorFlag>Y</apVendorFlag>"
//				+ "    <phoneNumber>(416)684-9237</phoneNumber>"
//				+ "    <phoneExtension></phoneExtension>"
//				+ "  </company>"
//				+ "  <mailingAddress>"
//				+ "    <addressLine1>21 Elizabeth St.Ste.201,BOJAN</addressLine1>"
//				+ "    <addressLine2></addressLine2>"
//				+ "    <addressLine3></addressLine3>"
//				+ "    <addressLine4></addressLine4>"
//				+ "    <city>ST. CATHARINES</city>"
//				+ "    <postalCode>L2R2K8</postalCode> "
//				+ "    <province>ON</province>"
//				+ "    <country>CAN</country>"
//				+ "    <createDate>2019-03-27-10.17.50.000000</createDate>"
//				+ "    <createUser>RMORRIS</createUser>"
//				+ "    <updateDate>2019-03-27-10.17.50.000000</updateDate>"
//				+ "    <updateUser>RMORRIS</updateUser>"
//				+ "  </mailingAddress>"
//				+ "  <relationships />"
//				+ "</companyDelta>";
//
//		CompanyDelta response = (CompanyDelta)VbsUtil.convertXmltoJaxb(xmlResponse);
//		crmQueueResponseProcessor.process(response, xmlResponse);
//	}	
//	
//	@Test
//	public void testCrmProcessCompanyBranchUpdateDelta() {
//		String xmlResponse = 
//				"<?xml version=\"1.0\"?>\n" + 
//				"<companyDelta>\n" + 
//				"  <company>\n" + 
//				"    <crmContactId>12549742</crmContactId>\n" + 
//				"    <companyType>4</companyType>\n" + 
//				"    <companyName>test administrative agent</companyName>\n" + 
//				"    <licenceStatus></licenceStatus>\n" + 
//				"    <escrowLicenceApplicationStatus></escrowLicenceApplicationStatus>\n" + 
//				"    <financialInstitutionId>400</financialInstitutionId>\n" + 
//				"    <unwilling>N</unwilling>\n" + 
//				"    <accessibility>N</accessibility>\n" + 
//				"    <yellowSticky>N</yellowSticky>\n" + 
//				"    <alert>0</alert>\n" + 
//				"    <apVendorFlag>N</apVendorFlag>\n" + 
//				"    <emailAddress>malathi.dhandpani@tarion.com</emailAddress>\n" + 
//				"    <phoneNumber>909-9604</phoneNumber>\n" + 
//				"    <phoneExtension></phoneExtension>\n" + 
//				"  </company>\n" + 
//				"  <mailingAddress>\n" + 
//				"    <addressLine1>5163 Yonge Street</addressLine1>\n" + 
//				"    <addressLine2>ON</addressLine2>\n" + 
//				"    <addressLine3></addressLine3>\n" + 
//				"    <addressLine4></addressLine4>\n" + 
//				"    <city>TORONTO</city>\n" + 
//				"    <postalCode>M2N 3L6</postalCode>\n" + 
//				"    <province>ON</province>\n" + 
//				"    <country>CAN</country>\n" + 
//				"    <createDate>2019-05-30-14.01.25.000000</createDate>\n" + 
//				"    <createUser>RMORRIS</createUser>\n" + 
//				"    <updateDate>2019-05-30-14.01.25.000000</updateDate>\n" + 
//				"    <updateUser>RMORRIS</updateUser>\n" + 
//				"  </mailingAddress>\n" + 
//				"  <relationships />\n" + 
//				"</companyDelta>\n" ;
//
//		CompanyDelta response = (CompanyDelta)VbsUtil.convertXmltoJaxb(xmlResponse);
//		crmQueueResponseProcessor.process(response, xmlResponse);
//		List<BranchDTO> branches = fiService.getBranchesForFinancialInstitution(Long.parseLong(response.getCompany().getFinancialInstitutionId()));
//		assertNotNull(branches);
//	}
//
//	@Test
//	public void testCrmProcessCompanyDeltaEscrow() {
//		String xmlResponse = "<?xml version=\"1.0\"?>\n" + 
//				"<companyDelta>\n" + 
//				"  <company>\n" + 
//				"    <crmContactId>91910620</crmContactId>\n" + 
//				"    <companyType>19</companyType>\n" + 
//				"    <companyName>Green and Spiegel LLP</companyName>\n" + 
//				"    <licenceStatus>ACT</licenceStatus>\n" + 
//				"    <escrowLicenceApplicationStatus>NEW</escrowLicenceApplicationStatus>\n" + 
//				"    <unwilling>N</unwilling>\n" + 
//				"    <accessibility>N</accessibility>\n" + 
//				"    <yellowSticky>N</yellowSticky>\n" + 
//				"    <alert>1</alert>\n" + 
//				"    <apVendorFlag>Y</apVendorFlag>\n" + 
//				"    <emailAddress>davidn@gands.com</emailAddress>\n" + 
//				"    <phoneNumber>416-862-7880</phoneNumber>\n" + 
//				"    <phoneExtension></phoneExtension>\n" + 
//				"  </company>\n" + 
//				"  <mailingAddress>\n" + 
//				"    <addressLine1>390 Bay St., Ste. 2800</addressLine1>\n" + 
//				"    <addressLine2></addressLine2>\n" + 
//				"    <addressLine3></addressLine3>\n" + 
//				"    <addressLine4></addressLine4>\n" + 
//				"    <city>TORONTO</city>\n" + 
//				"    <postalCode>M5H 2Y2</postalCode>\n" + 
//				"    <province>ON</province>\n" + 
//				"    <country>CAN</country>\n" + 
//				"    <createDate>2012-07-26-09.56.43.000000</createDate>\n" + 
//				"    <createUser>RAMO</createUser>\n" + 
//				"    <updateDate>2012-07-26-09.56.43.000000</updateDate>\n" + 
//				"    <updateUser>RAMO</updateUser>\n" + 
//				"  </mailingAddress>\n" + 
//				"  <relationships>\n" + 
//				"    <relationship>\n" + 
//				"      <crmContactId>91910620</crmContactId>\n" + 
//				"      <fromCrmContactId>91910620</fromCrmContactId>\n" + 
//				"      <toCrmContactId>11855745</toCrmContactId>\n" + 
//				"      <startDate>2013-11-01</startDate>\n" + 
//				"      <endDate>2015-03-01</endDate>\n" + 
//				"      <crmRelId>596379503337065582667606048604</crmRelId>\n" + 
//				"      <relationshipType>20166</relationshipType>\n" + 
//				"      <createDate>2013-11-01-11.15.48.000000</createDate>\n" + 
//				"      <createUser>OCHENG</createUser>\n" + 
//				"      <updateDate>2015-04-24-13.10.39.000000</updateDate>\n" + 
//				"      <updateUser>MCRITCHLEY</updateUser>\n" + 
//				"    </relationship>\n" + 
//				"    <relationship>\n" + 
//				"      <crmContactId>91910620</crmContactId>\n" + 
//				"      <fromCrmContactId>91910620</fromCrmContactId>\n" + 
//				"      <toCrmContactId>100137956</toCrmContactId>\n" + 
//				"      <startDate>2013-11-01</startDate>\n" + 
//				"      <endDate>2015-03-01</endDate>\n" + 
//				"      <crmRelId>644230249377877102668327050005</crmRelId>\n" + 
//				"      <relationshipType>20167</relationshipType>\n" + 
//				"      <createDate>2013-11-01-11.16.06.000000</createDate>\n" + 
//				"      <createUser>OCHENG</createUser>\n" + 
//				"      <updateDate>2015-04-24-13.09.18.000000</updateDate>\n" + 
//				"      <updateUser>MCRITCHLEY</updateUser>\n" + 
//				"    </relationship>\n" + 
//				"  </relationships>\n" + 
//				"</companyDelta>\n";
//		CompanyDelta response = (CompanyDelta)VbsUtil.convertXmltoJaxb(xmlResponse);
//		crmQueueResponseProcessor.process(response, xmlResponse);
//
//	}
//	
//	@Test
//	public void testCrmProcessPersonDelta() {
//		String xmlResponse = 
//				"<?xml version=\"1.0\"?> "
//				+ "<personDelta>\n" + 
//				"  <person>\n" + 
//				"    <crmContactId>100127785</crmContactId>\n" + 
//				"    <firstName>Andrew R.C.Bojan</firstName>\n" + 
//				"    <lastName>Webster</lastName>\n" + 
//				"    <sin></sin>\n" + 
//				"    <driversLicence></driversLicence>\n" + 
//				"    <dateOfBirth></dateOfBirth>\n" + 
//				"    <emailAddress>awebster@airdberlis.com</emailAddress>\n" + 
//				"    <phoneNumber>416/865-7777</phoneNumber>\n" + 
//				"    <apVendorFlag>Y</apVendorFlag>\n" + 
//				"    <phoneExtension></phoneExtension>\n" + 
//				"  </person>\n" + 
//				"  <mailingAddress>\n" + 
//				"    <addressLine1>Brookfield PlaceBojan</addressLine1>\n" + 
//				"    <addressLine2>181 Bay Street, Suite 1800</addressLine2>\n" + 
//				"    <addressLine3>Box 754</addressLine3>\n" + 
//				"    <addressLine4></addressLine4>\n" + 
//				"    <city>TORONTO</city>\n" + 
//				"    <postalCode>M5J 2T9</postalCode>\n" + 
//				"    <province>ON</province>\n" + 
//				"    <country>CAN</country>\n" + 
//				"    <createDate>2019-03-27-14.11.24.000000</createDate>\n" + 
//				"    <createUser>RMORRIS</createUser>\n" + 
//				"    <updateDate>2019-03-27-14.11.24.000000</updateDate>\n" + 
//				"    <updateUser>RMORRIS</updateUser>\n" + 
//				"  </mailingAddress>\n" + 
//				"  <relationships>\n" + 
//				"    <relationship>\n" + 
//				"      <crmContactId>100127785</crmContactId>\n" + 
//				"      <fromCrmContactId>11855737</fromCrmContactId>\n" + 
//				"      <toCrmContactId>100127785</toCrmContactId>\n" + 
//				"      <startDate>2013-09-18</startDate>\n" + 
//				"      <endDate>2999-12-31</endDate>\n" + 
//				"      <crmRelId>1139094402758935109151307012920</crmRelId>\n" + 
//				"      <createDate>2013-09-18-11.06.49.000000</createDate>\n" + 
//				"      <createUser>RAMO</createUser>\n" + 
//				"      <updateDate>2013-09-18-11.06.49.000000</updateDate>\n" + 
//				"      <updateUser>RAMO</updateUser>\n" + 
//				"    </relationship>\n" + 
//				"  </relationships>\n" + 
//				"</personDelta>";
//
//		PersonDelta response = (PersonDelta)VbsUtil.convertXmltoJaxb(xmlResponse);
//		crmQueueResponseProcessor.process(response, xmlResponse);
//	}	
//	
	@Test
	public void testCrmProcessPersonDeltaAddPerson() {
		String xmlResponse = 
				"<?xml version=\"1.0\"?> " +
				"<personDelta>\n" + 
				"  <person>\n" + 
				"    <crmContactId>100594735</crmContactId>\n" + 
				"    <firstName>Elly</firstName>\n" + 
				"    <lastName>Beyk</lastName>\n" + 
				"    <sin></sin>\n" + 
				"    <driversLicence></driversLicence>\n" + 
				"    <dateOfBirth></dateOfBirth>\n" + 
				"    <apVendorFlag></apVendorFlag>\n" + 
				"    <emailAddress>ebeyk@bratty.com</emailAddress>\n" + 
				"    <phoneNumber>905/760-2600</phoneNumber>\n" + 
				"    <phoneExtension>358</phoneExtension>\n" + 
				"  </person>\n" + 
				"  <address>\n" + 
				"    <addressLine1>7501 Keele Street, Suite 200</addressLine1>\n" + 
				"    <addressLine2></addressLine2>\n" + 
				"    <addressLine3></addressLine3>\n" + 
				"    <addressLine4></addressLine4>\n" + 
				"    <city>Vaughan</city>\n" + 
				"    <postalCode>L4K 1Y2</postalCode>\n" + 
				"    <province>ON</province>\n" + 
				"    <country>CAN</country>\n" + 
				"    <createDate>2019-09-20-08.38.01.000000</createDate>\n" + 
				"    <createUser>MCRITCHLEY</createUser>\n" + 
				"    <updateDate>2019-09-20-08.38.01.000000</updateDate>\n" + 
				"    <updateUser>MCRITCHLEY</updateUser>\n" + 
				"  </address>\n" + 
				"  <relationships>\n" + 
				"    <relationship>\n" + 
				"      <crmContactId>100594735</crmContactId>\n" + 
				"      <fromCrmContactId>11618110</fromCrmContactId>\n" + 
				"      <toCrmContactId>100594735</toCrmContactId>\n" + 
				"      <startDate>2019-09-20</startDate>\n" + 
				"      <endDate>2999-12-31</endDate>\n" + 
				"      <crmRelId>764031062805604855298831073561</crmRelId>\n" + 
				"      <relationshipType>20167</relationshipType>\n" + 
				"      <createDate>2019-09-20-08.39.06.000000</createDate>\n" + 
				"      <createUser>MCRITCHLEY</createUser>\n" + 
				"      <updateDate>2019-09-20-08.39.06.000000</updateDate>\n" + 
				"      <updateUser>MCRITCHLEY</updateUser>\n" + 
				"    </relationship>\n" + 
				"  </relationships>\n" + 
				"</personDelta>";

		PersonDelta response = (PersonDelta)VbsUtil.convertXmltoJaxb(xmlResponse);
		crmQueueResponseProcessor.process(response, xmlResponse);
		ContactEntity insertedContact = contactDAO.getContactByCrmContactId("100594735");
		assertNotNull(insertedContact);
	}	
	
//	@Test
//	public void testCrmProcessEnrolmentsDelta() {
//		String xmlResponse = 
//				"<?xml version=\"1.0\"?> "
//				+ "<enrolmentsDelta>\n" + 
//				"  <enrolment>\n" + 
//				"    <enrolmentNumber>H1000355</enrolmentNumber>\n" + 
//				"    <builder>B20251</builder>\n" + 
//				"    <vendor>B20251</vendor>\n" + 
//				"    <enrollingVb>B20251</enrollingVb>\n" + 
//				"    <createDate>1999-11-10-00.00.00.000000</createDate>\n" + 
//				"    <createUser>CFLOERKE</createUser>\n" + 
//				"    <updateDate>2019-03-27-15.31.08.000000</updateDate>\n" + 
//				"    <updateUser>RMORRIS</updateUser>\n" + 
//				"  </enrolment>\n" + 
//				"</enrolmentsDelta>";
//
//		EnrolmentsDelta response = (EnrolmentsDelta)VbsUtil.convertXmltoJaxb(xmlResponse);
//		crmQueueResponseProcessor.process(response, xmlResponse);
//	}
//	
////	@Test
//	public void testCrmProcessEscrow() {
//		String xml = "<?xml version=\"1.0\"?>\n" + 
//				"<companyDelta>\n" + 
//				"  <company>\n" + 
//				"    <crmContactId>12549726</crmContactId>\n" + 
//				"    <companyType>19</companyType>\n" + 
//				"    <companyName>Ivy Test EA LLP</companyName>\n" + 
//				"    <licenceStatus></licenceStatus>\n" + 
//				"    <escrowLicenceApplicationStatus>NEW</escrowLicenceApplicationStatus>\n" + 
//				"    <unwilling>N</unwilling>\n" + 
//				"    <accessibility>N</accessibility>\n" + 
//				"    <yellowSticky>N</yellowSticky>\n" + 
//				"    <alert>0</alert>\n" + 
//				"    <apVendorFlag>N</apVendorFlag>\n" + 
//				"    <phoneNumber>416/299-2299</phoneNumber>\n" + 
//				"    <phoneExtension></phoneExtension>\n" + 
//				"  </company>\n" + 
//				"  <mailingAddress>\n" + 
//				"    <addressLine1>5160 Yonge Street</addressLine1>\n" + 
//				"    <addressLine2></addressLine2>\n" + 
//				"    <addressLine3></addressLine3>\n" + 
//				"    <addressLine4></addressLine4>\n" + 
//				"    <city>Toronto</city>\n" + 
//				"    <postalCode>M2N 6L9</postalCode>\n" + 
//				"    <province>ON</province>\n" + 
//				"    <country>CAN</country>\n" + 
//				"    <createDate>2019-05-02-10.58.14.000000</createDate>\n" + 
//				"    <createUser>OCHENG</createUser>\n" + 
//				"    <updateDate>2019-05-02-10.58.14.000000</updateDate>\n" + 
//				"    <updateUser>OCHENG</updateUser>\n" + 
//				"  </mailingAddress>\n" + 
//				"  <relationships />\n" + 
//				"</companyDelta>\n" ;
//		CompanyDelta response = (CompanyDelta)VbsUtil.convertXmltoJaxb(xml);
//		crmQueueResponseProcessor.process(response, xml);
//	}
//	
//	@Test
//	public void testWarrantyServicesRecommendationResponse() {
//		String xmlResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
//				"<warrantyServicesRecommendationResponse>\n" + 
//				"  <enrolmentNumber>H1573361</enrolmentNumber>\n" + 
//				"  <wsRecommendationCode>X</wsRecommendationCode>\n" + 
//				"  <fcmAmountRetained>60000</fcmAmountRetained>\n" + 
//				"  <serviceOrderId>SVC018123</serviceOrderId>\n" + 
//				"  <inputProvidedUser>ASTEVENSON</inputProvidedUser>\n" + 
//				"  <inputProvidedDate>2019-05-02-10.58.14.000000</inputProvidedDate>\n" + 
//				"</warrantyServicesRecommendationResponse>";
//		WarrantyServicesRecommendationResponse response = (WarrantyServicesRecommendationResponse)VbsUtil.convertXmltoJaxb(xmlResponse);
//		crmQueueResponseProcessor.process(response, xmlResponse);
//	}
//	
//	@Test
//	public void testEscrowLicence() {
//		String xmlResponse = "<?xml version=\"1.0\"?>\n" + 
//				"<companyDelta>\n" + 
//				"  <company>\n" + 
//				"    <crmContactId>91963937</crmContactId>\n" + 
//				"    <companyType>19</companyType>\n" + 
//				"    <companyName>Goldberg Stroud LLP</companyName>\n" + 
//				"    <licenceStatus>INA</licenceStatus>\n" + 
//				"    <escrowLicenceApplicationStatus>EXP</escrowLicenceApplicationStatus>\n" + 
//				"    <financialInstitutionId>0</financialInstitutionId>\n" + 
//				"    <unwilling>N</unwilling>\n" + 
//				"    <accessibility>N</accessibility>\n" + 
//				"    <yellowSticky>N</yellowSticky>\n" + 
//				"    <alert>0</alert>\n" + 
//				"    <apVendorFlag>Y</apVendorFlag>\n" + 
//				"    <emailAddress>bpineau@gwshlaw.com</emailAddress>\n" + 
//				"    <phoneNumber>613/237-4922</phoneNumber>\n" + 
//				"    <phoneExtension></phoneExtension>\n" + 
//				"  </company>\n" + 
//				"  <mailingAddress>\n" + 
//				"    <addressLine1>176 Bronson Avenue</addressLine1>\n" + 
//				"    <addressLine2></addressLine2>\n" + 
//				"    <addressLine3></addressLine3>\n" + 
//				"    <addressLine4></addressLine4>\n" + 
//				"    <city>OTTAWA</city>\n" + 
//				"    <postalCode>K1R 6H4</postalCode>\n" + 
//				"    <province>ON</province>\n" + 
//				"    <country>CAN</country>\n" + 
//				"    <createDate>2015-01-09-12.12.45.000000</createDate>\n" + 
//				"    <createUser>MCRITCHLEY</createUser>\n" + 
//				"    <updateDate>2015-01-09-12.12.45.000000</updateDate>\n" + 
//				"    <updateUser>MCRITCHLEY</updateUser>\n" + 
//				"  </mailingAddress>\n" + 
//				"  <relationships>\n" + 
//				"    <relationship>\n" + 
//				"      <crmContactId>91963937</crmContactId>\n" + 
//				"      <fromCrmContactId>91963937</fromCrmContactId>\n" + 
//				"      <toCrmContactId>100183272</toCrmContactId>\n" + 
//				"      <startDate>2013-07-25</startDate>\n" + 
//				"      <endDate>2999-12-31</endDate>\n" + 
//				"      <crmRelId>24476611632651621802349038402</crmRelId>\n" + 
//				"      <relationshipType>20166</relationshipType>\n" + 
//				"      <createDate>2013-07-25-11.36.49.000000</createDate>\n" + 
//				"      <createUser>MCRITCHLEY</createUser>\n" + 
//				"      <updateDate>2013-07-25-11.36.49.000000</updateDate>\n" + 
//				"      <updateUser>MCRITCHLEY</updateUser>\n" + 
//				"    </relationship>\n" + 
//				"    <relationship>\n" + 
//				"      <crmContactId>91963937</crmContactId>\n" + 
//				"      <fromCrmContactId>91963937</fromCrmContactId>\n" + 
//				"      <toCrmContactId>100128132</toCrmContactId>\n" + 
//				"      <startDate>2013-07-25</startDate>\n" + 
//				"      <endDate>2999-12-31</endDate>\n" + 
//				"      <crmRelId>1011695389190915824466091041881</crmRelId>\n" + 
//				"      <relationshipType>20167</relationshipType>\n" + 
//				"      <createDate>2013-07-25-11.21.11.000000</createDate>\n" + 
//				"      <createUser>MCRITCHLEY</createUser>\n" + 
//				"      <updateDate>2013-07-25-11.21.11.000000</updateDate>\n" + 
//				"      <updateUser>MCRITCHLEY</updateUser>\n" + 
//				"    </relationship>\n" + 
//				"    <relationship>\n" + 
//				"      <crmContactId>91963937</crmContactId>\n" + 
//				"      <fromCrmContactId>91963937</fromCrmContactId>\n" + 
//				"      <toCrmContactId>100368419</toCrmContactId>\n" + 
//				"      <startDate>2016-05-31</startDate>\n" + 
//				"      <endDate>2999-12-31</endDate>\n" + 
//				"      <crmRelId>280990697930477246263929274529</crmRelId>\n" + 
//				"      <relationshipType>20167</relationshipType>\n" + 
//				"      <createDate>2016-05-31-10.52.02.000000</createDate>\n" + 
//				"      <createUser>OCHENG</createUser>\n" + 
//				"      <updateDate>2016-05-31-10.52.02.000000</updateDate>\n" + 
//				"      <updateUser>OCHENG</updateUser>\n" + 
//				"    </relationship>\n" + 
//				"  </relationships>\n" + 
//				"</companyDelta>\n" + 
//				"\n"; 
//		
//		CompanyDelta response = (CompanyDelta)VbsUtil.convertXmltoJaxb(xmlResponse);
//		crmQueueResponseProcessor.process(response, xmlResponse);
//	}	
//	
//	@Test
//	public void testEnrolmentDelta() {
//		String xmlResponse = "<?xml version=\"1.0\"?>\n" + 
//				"<enrolmentsDelta>\n" + 
//				"  <enrolment>\n" + 
//				"    <enrolmentNumber>H2245784</enrolmentNumber>\n" + 
//				"    <builder></builder>\n" + 
//				"    <vendor>B44507</vendor>\n" + 
//				"    <enrollingVb>B44507</enrollingVb>\n" + 
//				"    <unitType>001</unitType>\n" + 
//				"    <homeCategory>7</homeCategory>\n" + 
//				"    <condoConversion>N</condoConversion>\n" + 
//				"    <warrantyStartDate></warrantyStartDate>\n" + 
//				"    <entryDate>2019-07-23</entryDate>\n" + 
//				"    <wsFCR></wsFCR>\n" + 
//				"    <condoCorpHoName></condoCorpHoName>\n" + 
//				"    <createDate>2019-07-23-11.15.21.000000</createDate>\n" + 
//				"    <createUser>BHAMA</createUser>\n" + 
//				"    <updateDate>2019-07-23-11.22.37.000000</updateDate>\n" + 
//				"    <updateUser>BHAMA</updateUser>\n" + 
//				"  </enrolment>\n" + 
//				"</enrolmentsDelta>"; 
//		
//		EnrolmentsDelta response = (EnrolmentsDelta)VbsUtil.convertXmltoJaxb(xmlResponse);
//		crmQueueResponseProcessor.process(response, xmlResponse);
//	}
//	
	@Test
	public void testCrmProcessCompanyDeltaApFlagIssue() {
		String xmlResponseCreateCompanyType6 = 
				"<?xml version=\"1.0\"?>\n" + 
				"<companyDelta>\n" + 
				"  <company>\n" + 
				"    <crmContactId>12550832</crmContactId>\n" + 
				"    <companyType>6</companyType>\n" + 
				"    <companyName>The Fuller Landau Group Inc.</companyName>\n" + 
				"    <licenceStatus></licenceStatus>\n" + 
				"    <escrowLicenceApplicationStatus></escrowLicenceApplicationStatus>\n" + 
				"    <financialInstitutionId>0</financialInstitutionId>\n" + 
				"    <unwilling>N</unwilling>\n" + 
				"    <accessibility>N</accessibility>\n" + 
				"    <yellowSticky>N</yellowSticky>\n" + 
				"    <alert>0</alert>\n" + 
				"    <apVendorFlag></apVendorFlag>\n" + 
				"    <createDate>2019-10-07-09.01.14.000000</createDate>\n" + 
				"    <createUser>MCRITCHLEY</createUser>\n" + 
				"    <updateDate>2019-10-07-09.01.14.000000</updateDate>\n" + 
				"    <updateUser>MCRITCHLEY</updateUser>\n" + 
				"  </company>\n" + 
				"  <mailingAddress>\n" + 
				"    <addressLine1>in its capacity as receiver of Homes By Design Inc.</addressLine1>\n" + 
				"    <addressLine2>151 Bloor Street West 12th Floor</addressLine2>\n" + 
				"    <addressLine3></addressLine3>\n" + 
				"    <addressLine4></addressLine4>\n" + 
				"    <city>TORONTO</city>\n" + 
				"    <postalCode>M5S 1S4</postalCode>\n" + 
				"    <province>ON</province>\n" + 
				"    <country>CAN</country>\n" + 
				"    <createDate>2019-10-07-09.01.16.000000</createDate>\n" + 
				"    <createUser>MCRITCHLEY</createUser>\n" + 
				"    <updateDate>2019-10-07-09.01.16.000000</updateDate>\n" + 
				"    <updateUser>MCRITCHLEY</updateUser>\n" + 
				"  </mailingAddress>\n" + 
				"  <relationships />\n" + 
				"</companyDelta>\n" + 
				"\n"; 

		CompanyDelta responseType6 = (CompanyDelta)VbsUtil.convertXmltoJaxb(xmlResponseCreateCompanyType6);
		crmQueueResponseProcessor.process(responseType6, xmlResponseCreateCompanyType6);
		ContactEntity contactType6 = contactDAO.getContactByCrmContactId(responseType6.getCompany().getCrmContactId());
		if (contactType6 != null) {
			if (contactType6.getContactType().getId() == 6l) {
				fail();
			} else {
				assertNotNull(contactType6);
			}
		} else {
			assertNull(contactType6);
		}
	}	
	
	@Test
	public void testCrmProcessCompanyDeltaApFlagFix() {
		String xmlResponseCreateCompanyType2NoApFlag = 
				"<?xml version=\"1.0\"?>\n" + 
				"<companyDelta>\n" + 
				"  <company>\n" + 
				"    <crmContactId>12550832</crmContactId>\n" + 
				"    <companyType>2</companyType>\n" + 
				"    <companyName>The Fuller Landau Group Inc.</companyName>\n" + 
				"    <licenceStatus></licenceStatus>\n" + 
				"    <escrowLicenceApplicationStatus></escrowLicenceApplicationStatus>\n" + 
				"    <financialInstitutionId>0</financialInstitutionId>\n" + 
				"    <unwilling>N</unwilling>\n" + 
				"    <accessibility>N</accessibility>\n" + 
				"    <yellowSticky>N</yellowSticky>\n" + 
				"    <alert>0</alert>\n" + 
				"    <apVendorFlag></apVendorFlag>\n" + 
				"    <createDate>2019-10-07-09.01.14.000000</createDate>\n" + 
				"    <createUser>MCRITCHLEY</createUser>\n" + 
				"    <updateDate>2019-10-07-09.01.14.000000</updateDate>\n" + 
				"    <updateUser>MCRITCHLEY</updateUser>\n" + 
				"  </company>\n" + 
				"  <mailingAddress>\n" + 
				"    <addressLine1>in its capacity as receiver of Homes By Design Inc.</addressLine1>\n" + 
				"    <addressLine2>151 Bloor Street West 12th Floor</addressLine2>\n" + 
				"    <addressLine3></addressLine3>\n" + 
				"    <addressLine4></addressLine4>\n" + 
				"    <city>TORONTO</city>\n" + 
				"    <postalCode>M5S 1S4</postalCode>\n" + 
				"    <province>ON</province>\n" + 
				"    <country>CAN</country>\n" + 
				"    <createDate>2019-10-07-09.01.16.000000</createDate>\n" + 
				"    <createUser>MCRITCHLEY</createUser>\n" + 
				"    <updateDate>2019-10-07-09.01.16.000000</updateDate>\n" + 
				"    <updateUser>MCRITCHLEY</updateUser>\n" + 
				"  </mailingAddress>\n" + 
				"  <relationships />\n" + 
				"</companyDelta>\n" + 
				"\n"; 
		
		CompanyDelta responseType2 = (CompanyDelta)VbsUtil.convertXmltoJaxb(xmlResponseCreateCompanyType2NoApFlag);
		crmQueueResponseProcessor.process(responseType2, xmlResponseCreateCompanyType2NoApFlag);
		ContactEntity contactType2 = contactDAO.getContactByCrmContactId(responseType2.getCompany().getCrmContactId());
		assertNotNull(contactType2);
		
		String xmlResponseCreateCompanyType6ApFlag = 
				"<?xml version=\"1.0\"?>\n" + 
				"<companyDelta>\n" + 
				"  <company>\n" + 
				"    <crmContactId>12550832</crmContactId>\n" + 
				"    <companyType>6</companyType>\n" + 
				"    <companyName>The Fuller Landau Group Inc.</companyName>\n" + 
				"    <licenceStatus></licenceStatus>\n" + 
				"    <escrowLicenceApplicationStatus></escrowLicenceApplicationStatus>\n" + 
				"    <financialInstitutionId>0</financialInstitutionId>\n" + 
				"    <unwilling>N</unwilling>\n" + 
				"    <accessibility>N</accessibility>\n" + 
				"    <yellowSticky>N</yellowSticky>\n" + 
				"    <alert>0</alert>\n" + 
				"    <apVendorFlag>Y</apVendorFlag>\n" + 
				"    <createDate>2019-10-07-09.01.14.000000</createDate>\n" + 
				"    <createUser>MCRITCHLEY</createUser>\n" + 
				"    <updateDate>2019-10-07-09.01.14.000000</updateDate>\n" + 
				"    <updateUser>MCRITCHLEY</updateUser>\n" + 
				"  </company>\n" + 
				"  <mailingAddress>\n" + 
				"    <addressLine1>in its capacity as receiver of Homes By Design Inc.</addressLine1>\n" + 
				"    <addressLine2>151 Bloor Street West 12th Floor</addressLine2>\n" + 
				"    <addressLine3></addressLine3>\n" + 
				"    <addressLine4></addressLine4>\n" + 
				"    <city>TORONTO</city>\n" + 
				"    <postalCode>M5S 1S4</postalCode>\n" + 
				"    <province>ON</province>\n" + 
				"    <country>CAN</country>\n" + 
				"    <createDate>2019-10-07-09.01.16.000000</createDate>\n" + 
				"    <createUser>MCRITCHLEY</createUser>\n" + 
				"    <updateDate>2019-10-07-09.01.16.000000</updateDate>\n" + 
				"    <updateUser>MCRITCHLEY</updateUser>\n" + 
				"  </mailingAddress>\n" + 
				"  <relationships />\n" + 
				"</companyDelta>\n" + 
				"\n"; 
		
		CompanyDelta responseType6ApFlag = (CompanyDelta)VbsUtil.convertXmltoJaxb(xmlResponseCreateCompanyType6ApFlag);
		crmQueueResponseProcessor.process(responseType6ApFlag, xmlResponseCreateCompanyType6ApFlag);
		ContactEntity contactType6ApFlag = contactDAO.getContactByCrmContactId(responseType6ApFlag.getCompany().getCrmContactId());
		assertNotNull(contactType6ApFlag);
		assertTrue(contactType6ApFlag.getApVendorFlag());
	}

	@Test
	public void test() {
		assertNotNull("1");
	}
	

}


