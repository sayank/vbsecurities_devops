package com.tarion.vbs.service.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.*;

import com.tarion.vbs.common.constants.JMSConstants;


public class Container {
	
	private static Container instance;

	private EJBContainer ejbContainer;

	private Container() {}
	
	public static Container getInstance() {
		if(instance == null) {
			instance = new Container();
		}
		return instance;
	}
	
	private static Context getContext() {
		return getInstance().getContainer().getContext();
	}
	
	public void startTheContainer() {
		final Properties p = new Properties();

		// This property makes load persistence.xml from test.persistence.xml
        p.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.client.LocalInitialContextFactory");
        p.setProperty("openejb.altdd.prefix", "test");
        p.setProperty("openejb.vendor.config", "NONE");
        
//		p.setProperty("openejb.deployments.classpath.include", ".*/VBS/.*/target/classes.*");
//		p.setProperty("openejb.deployments.classpath.exclude", ".*/VBS/VBS-service/target/classes/.*topic.*");
//		p.setProperty("openejb.deployments.package.exclude", ".*");
//		p.setProperty("openejb.deployments.package.exclude", ".*com\\.tarion\\.vbs\\.service\\.util\\.topic\\..*");
//		p.setProperty("openejb.deployments.classpath.filter.descriptors", "true");
		

        // this created new data source and setup properties
        // you have to put database name and username in the url
        // and then repeat username in the properties and password as well
		p.put("vbs", "new://Resource?type=DataSource");
		p.put("vbs.JdbcDriver", "com.microsoft.sqlserver.jdbc.SQLServerDataSource");
		try {
			p.put("vbs.JdbcUrl", System.getProperty("test.jdbc.url"));
			p.put("vbs.UserName", System.getProperty("test.jdbc.user"));
			p.put("vbs.Password", System.getProperty("test.jdbc.password"));

//			p.put("jms/XAVbaCF", "new://Resource?type=javax.jms.ConnectionFactory");
			p.put("MyJmsResourceAdapter", "new://Resource?type=ActiveMQResourceAdapter");
			p.put("MyJmsResourceAdapter.ServerUrl", "tcp://localhost:61616");
			p.put("MyJmsResourceAdapter.BrokerXmlConfig", "");

			p.put("jms/XALocalVbsCF", "new://Resource?type=javax.jms.ConnectionFactory");
			p.put("jms/XALocalVbsCF.ResourceAdapter", "MyJmsResourceAdapter");

			p.put("jms/XAVbsCF", "new://Resource?type=javax.jms.ConnectionFactory");
			p.put("jms/XAVbsCF.ResourceAdapter", "MyJmsResourceAdapter");

			p.put("MyJmsMdbContainer", "new://Container?type=MESSAGE");
			p.put("MyJmsMdbContainer.ResourceAdapter", "MyJmsResourceAdapter");

			p.put("jms/VbsFmsRequestQueue", "new://Resource?type=javax.jms.Queue");

			p.put("jms/fmsARItemTopic", "new://Resource?type=javax.jms.Topic");
//			p.put("connectionFactoryJndiName", JMSConstants.VBS_XA_TOPIC_CF_JNDI);
//			p.put("destinationJndiName", "jms/fmsARItemTopic");
//			p.put("topicMessagesDistributionMode", "One-Copy-Per-Application");

		} catch (Exception e) {
			try {
				// needed for Eclipse debugging because it does not get them from pom
				Properties temp = new Properties();
				temp.load(new FileInputStream("src/test/resources/test.vbs.properties"));
				p.put("vbs.JdbcUrl", temp.getProperty("test.jdbc.url"));
				p.put("vbs.UserName", temp.getProperty("test.jdbc.user"));
				p.put("vbs.Password", temp.getProperty("test.jdbc.password"));
				
//				p.put("jms/XAVbaCF", "new://Resource?type=javax.jms.ConnectionFactory");
				p.put("MyJmsResourceAdapter", "new://Resource?type=ActiveMQResourceAdapter");
				p.put("MyJmsResourceAdapter.ServerUrl", "tcp://localhost:61616");
				p.put("MyJmsResourceAdapter.BrokerXmlConfig", "");

				p.put("jms/XALocalVbsCF", "new://Resource?type=javax.jms.ConnectionFactory");
				p.put("jms/XALocalVbsCF.ResourceAdapter", "MyJmsResourceAdapter");

				p.put("MyJmsMdbContainer", "new://Container?type=MESSAGE");
				p.put("MyJmsMdbContainer.ResourceAdapter", "MyJmsResourceAdapter");

				p.put("jms/VbsFmsRequestQueue", "new://Resource?type=javax.jms.Queue");

				p.put("jms/fmsARItemTopic", "new://Resource?type=javax.jms.Topic");
//				p.put("connectionFactoryJndiName", JMSConstants.VBS_XA_TOPIC_CF_JNDI);
//				p.put("destinationJndiName", "jms/fmsARItemTopic");
//				p.put("topicMessagesDistributionMode", "One-Copy-Per-Application");
			} catch (IOException e1) {
				e1.printStackTrace();
				return;
			}

		}
		//Moved to project POM for CI.
		
		// TST DB
//		p.put("vbs.JdbcUrl", "jdbc:sqlserver://devtstmidbd01.cace181e6e585f025.database.windows.net:1433;databaseName=VBS_TST6;user=vbs_tst6_rw");
//		p.put("vbs.UserName", "vbs_tst6_rw");
//		p.put("vbs.Password", "Z9)r![5B&Bs-j=");
		
        // this created new data source and setup properties
        // you have to put database name and username in the url
        // and then repeat username in the properties and password as well
		ejbContainer = EJBContainer.createEJBContainer(p);
	}
	
	public void stopTheContainer() {
		if (ejbContainer != null) {
			ejbContainer.close();
		}
	}
	
	public EJBContainer getContainer() {
		return ejbContainer;
	}
	
	/**
	 * Returns the EJB. Must give the implementing class as parameter.
	 * example:
	 * Container.getBean(ApplicationDAOImpl.class);
	 * 
	 * NOT!
	 * 
	 * Container.getBean(ApplicationDAO.class)
	 * 
	 * @param clazz
	 * @return
	 * @throws NamingException
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getBean(Class<T> clazz) throws NamingException {

		String simpleName = clazz.getSimpleName();
		String jndiName = "java:global/VBS-service/" + simpleName;

		Context context = Container.getContext();
		Object returnObject = null;
		try {
			returnObject = context.lookup(jndiName);
		} catch (NameNotFoundException e) {
			String daoJndiName = "java:global/VBS-dao-0.0.1-SNAPSHOT/" + simpleName;
			returnObject = context.lookup(daoJndiName);
		}
		return (T)returnObject;
	}

	public static <T> T getDAO(Class<T> clazz) throws NamingException {
		String simpleName = clazz.getSimpleName();
		String jndiName = "java:global/VBS-dao/" + simpleName;

		Context context = Container.getContext();
		Object returnObject = null;
		try {
			returnObject = context.lookup(jndiName);
		} catch (NameNotFoundException e) {
			String daoJndiName = "java:global/VBS-dao-0.0.1-SNAPSHOT/" + simpleName;
			returnObject = context.lookup(daoJndiName);
		}
		return (T)returnObject;


	}
}
