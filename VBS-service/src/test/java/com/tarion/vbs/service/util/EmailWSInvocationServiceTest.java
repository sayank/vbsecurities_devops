/* 
 * 
 * SchedulerServiceTest.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.service.test.Container;

public class EmailWSInvocationServiceTest {

	private UserTransaction userTransaction;
	private EmailWSInvocationService emailWSInvocationService;
	
	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		emailWSInvocationService = Container.getBean(EmailWSInvocationServiceImpl.class);
	}
	
	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}
	
	
	@Test
	public void test() {
		assertNotNull(emailWSInvocationService);
	}

	// Have to coment out, my email box is getting overwhelmed
//	@Test
//	public void testSendEmailUsingEsp() {
//		try {
//			emailWSInvocationService.sendEmailUsingEsp("test", "test", "bojan.volcansek@tarion.com", "bojan.volcansek@tarion.com");
//		} catch (VbsCheckedException e) {
//			fail();
//		}
//	}

}
