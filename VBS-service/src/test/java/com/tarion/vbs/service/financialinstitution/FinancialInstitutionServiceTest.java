/* 
 * 
 * SecurityServiceTest.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.financialinstitution;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.common.dto.financialinstitution.BranchDTO;
import com.tarion.vbs.common.dto.financialinstitution.ContactFinancialInstitutionDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionBranchCompositeDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionMaaPoaDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionMaaPoaInstitutionDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionSigningOfficerDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.service.test.Container;

public class FinancialInstitutionServiceTest {

	private FinancialInstitutionService financialInstitutionService;
	private UserTransaction userTransaction;
	
	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		financialInstitutionService = Container.getBean(FinancialInstitutionServiceImpl.class);
	}
	
	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}
	
	
	@Test
	public void test() {
		assertNotNull(financialInstitutionService);
	}
	
	@Test
	public void testGetAllFinancialInstitutionsForNameLike() {
		List<FinancialInstitutionDTO> dtos = financialInstitutionService.getFinancialInstitutionsForNameOrIdLike("Royal", 1);
		assertNotNull(dtos);
		assertTrue(!dtos.isEmpty());
	}
	
	// TODO fix this throws null pointer on line 67
//	@Test
	public void testGetAllFinancialInstitutions() {
		List<FinancialInstitutionDTO> dtos = financialInstitutionService.getAllFinancialInstitutions();
		assertNotNull(dtos);
		assertTrue(!dtos.isEmpty());
	}

	@Test 
	public void testGetFullFinancialInstitutionForSecurity() {
		FinancialInstitutionBranchCompositeDTO dto = financialInstitutionService.getFullFinancialInstitutionForSecurity(0l);
//		assertNull(dto);
		dto = financialInstitutionService.getFullFinancialInstitutionForSecurity(1l);
		assertNotNull(dto);
		assertNotNull(dto.getBranch());
		assertNotNull(dto.getFinancialInstitution());
//		assertNotNull(dto.getFinancialInstitutionType());
	}
	//will be commented back in once data migration for addresses are done
//	@Test 
//	public void testGetFullFinancialInstitutionForSecurity() {
//		FinancialInstitutionBranchCompositeDTO dto = financialInstitutionService.getFullFinancialInstitutionForSecurity(0l);
//		assertNull(dto);
//		dto = financialInstitutionService.getFullFinancialInstitutionForSecurity(1l);
//		assertNotNull(dto);
//		assertNotNull(dto.getBranch());
//		assertNotNull(dto.getFinancialInstitution());
//		assertNotNull(dto.getFinancialInstitutionType());
//	}

	@Test
	public void testGetFinancialInstitution() {
		FinancialInstitutionDTO dto = financialInstitutionService.getFinancialInstitution(VbsConstants.FINANCIAL_INSTITUTION_CIBC_ID);
		assertNotNull(dto);
		assertNotNull(dto.getOriginalAmountOnInstitution());
		assertNotNull(dto.getCurrentAmountOnInstitution());
	}

	@Test
	public void testValidateFinancialInstitution() {
		FinancialInstitutionDTO financialInstitutionDTO = createNewFinancialInstitutionDTO();
		List<ErrorDTO> errors = financialInstitutionService.validateFinancialInstitution(financialInstitutionDTO);
		assertNotNull(errors);
		assertTrue(errors.isEmpty());
	}

	@Test
	public void testAddFinancialInstitution() throws VbsCheckedException {
		FinancialInstitutionDTO financialInstitutionDTO = createNewFinancialInstitutionDTO();

		financialInstitutionDTO = financialInstitutionService.saveFinancialInstitution(financialInstitutionDTO, VbsUtil.getSystemUserDTO());
		assertNotNull(financialInstitutionDTO);

	}

	@Test
	public void testUpdateFinancialInstitution() throws VbsCheckedException {
		FinancialInstitutionDTO financialInstitutionDTO = createExistingFinancialInstitutionDTO();

		financialInstitutionDTO = financialInstitutionService.saveFinancialInstitution(financialInstitutionDTO, VbsUtil.getSystemUserDTO());
		assertNotNull(financialInstitutionDTO);
	}

	@Test
	public void testGetContactsForFinancialInstitution() {
		List<ContactFinancialInstitutionDTO> contacts = financialInstitutionService.getContactsForFinancialInstitution(VbsConstants.FINANCIAL_INSTITUTION_CIBC_ID);
		assertNotNull(contacts);
	}

	// TODO refactor to create Contact Entities to tst
//	@Test
	public void testSaveFinancialInstitutionContacts() {
		List<ContactFinancialInstitutionDTO> financialInstitutionContactsDTO = createFinancialInstitutionContactsDTO();

		financialInstitutionService.saveFinancialInstitutionContacts(financialInstitutionContactsDTO, VbsUtil.getSystemUserDTO());
	}
	
	@Test
	public void testSaveFinancialInstitutionMaaPoas() throws VbsCheckedException {
		FinancialInstitutionDTO fi = createNewSuretyFinancialInstitutionDTO();
		fi = financialInstitutionService.saveFinancialInstitution(fi, VbsUtil.getSystemUserDTO());
		List<FinancialInstitutionMaaPoaDTO> dtos = createFinancialInstitutionMaaPoaDTOList();

		financialInstitutionService.saveFinancialInstitutionMaaPoas(dtos, VbsUtil.getSystemUserDTO(), fi.getId());
	}
	
//	@Test
	public void testAdminAgentMaxAmount() {
		FinancialInstitutionDTO adminAgentFi = financialInstitutionService.getFinancialInstitution(408l);
		if (adminAgentFi != null) {
			
		}
	}
	
	@Test
	public void testGetContentNavigatorUrlForFinancialInstitution() {
		String url = financialInstitutionService.getContentNavigatorUrlForFinancialInstitution(1l);
		assertNotNull(url);
	}

	private List<FinancialInstitutionMaaPoaDTO> createFinancialInstitutionMaaPoaDTOList() throws VbsCheckedException {
		List<FinancialInstitutionMaaPoaDTO> dtoList = new ArrayList<FinancialInstitutionMaaPoaDTO>();
		FinancialInstitutionMaaPoaDTO dto = new FinancialInstitutionMaaPoaDTO();
		
		dto.setDeleted(false);
		dto.setExpirationDate(DateUtil.getLocalDateTimeMax());
		dto.setMaaPoaInstitutions(createFinancialInstitutionMaaPoaInstitutionDTOList(1));
		dto.setSigningOfficers(createFinancialInstitutionSigningOfficersDTOList());
		dtoList.add(dto);
		
		dto = new FinancialInstitutionMaaPoaDTO();
		dto.setDeleted(false);
		dto.setExpirationDate(DateUtil.getLocalDateTimeMax());
		dto.setMaaPoaInstitutions(createFinancialInstitutionMaaPoaInstitutionDTOList(2));
		dto.setSigningOfficers(createFinancialInstitutionSigningOfficersDTOList());
		dtoList.add(dto);

		return dtoList;
	}	
	
	private List<FinancialInstitutionMaaPoaInstitutionDTO> createFinancialInstitutionMaaPoaInstitutionDTOList(int maaPoaSequence) throws VbsCheckedException {
		List<FinancialInstitutionMaaPoaInstitutionDTO> list = new ArrayList<>();
		FinancialInstitutionMaaPoaInstitutionDTO dto = new FinancialInstitutionMaaPoaInstitutionDTO();
		FinancialInstitutionDTO fi = createNewSuretyFinancialInstitutionDTO();
		fi.setName("Test name Surety 1 for MAA" + maaPoaSequence);
		fi = financialInstitutionService.saveFinancialInstitution(fi, VbsUtil.getSystemUserDTO());
		dto.setFinancialInstitution(fi);
		BranchDTO branch = createExistingBranchDTO();
		dto.setBranch(branch);
		dto.setPercentage(BigDecimal.TEN);
		list.add(dto);

		dto = new FinancialInstitutionMaaPoaInstitutionDTO();
		fi = createNewSuretyFinancialInstitutionDTO();
		fi.setName("Test name Surety 2 for MAA" + maaPoaSequence);
		fi = financialInstitutionService.saveFinancialInstitution(fi, VbsUtil.getSystemUserDTO());
		dto.setFinancialInstitution(fi);
		branch = createExistingBranchDTO();
		dto.setBranch(branch);
		dto.setPercentage(BigDecimal.valueOf(90l));
		list.add(dto);
		
		return list;
	}
	
	private List<FinancialInstitutionSigningOfficerDTO> createFinancialInstitutionSigningOfficersDTOList() {
		List<FinancialInstitutionSigningOfficerDTO> financialInstitutionSigningOfficersDTO = new ArrayList<FinancialInstitutionSigningOfficerDTO>();
		FinancialInstitutionSigningOfficerDTO financialInstitutionContact = new FinancialInstitutionSigningOfficerDTO();
		financialInstitutionContact.setCompanyName("test company name");
		financialInstitutionContact.setContactName("test contact name");
		financialInstitutionSigningOfficersDTO.add(financialInstitutionContact);

		financialInstitutionContact = new FinancialInstitutionSigningOfficerDTO();
		financialInstitutionContact.setCompanyName("test company name");
		financialInstitutionContact.setContactName("test contact name");
		financialInstitutionSigningOfficersDTO.add(financialInstitutionContact);

		financialInstitutionContact = new FinancialInstitutionSigningOfficerDTO();
		financialInstitutionContact.setCompanyName("test company name");
		financialInstitutionContact.setContactName("test contact name");
		financialInstitutionSigningOfficersDTO.add(financialInstitutionContact);

		return financialInstitutionSigningOfficersDTO;
	}

	private List<ContactFinancialInstitutionDTO> createFinancialInstitutionContactsDTO() {
		List<ContactFinancialInstitutionDTO> financialInstitutionContactsDTO = new ArrayList<ContactFinancialInstitutionDTO>();
		ContactFinancialInstitutionDTO financialInstitutionContact = new ContactFinancialInstitutionDTO();
		ContactDTO contact = new ContactDTO();
		contact.setCrmContactId("100120408");
		financialInstitutionContact.setContact(contact);
		financialInstitutionContact.setFinancialInstitutionId(VbsConstants.FINANCIAL_INSTITUTION_CIBC_ID);
		financialInstitutionContact.setStartDate(DateUtil.getLocalDateTimeNow());
		financialInstitutionContact.setEndDate(DateUtil.getLocalDateTimeNow());
		financialInstitutionContactsDTO.add(financialInstitutionContact);

		financialInstitutionContact = new ContactFinancialInstitutionDTO();
		financialInstitutionContact.getContact().setCrmContactId("100122182");
		financialInstitutionContact.setFinancialInstitutionId(VbsConstants.FINANCIAL_INSTITUTION_CIBC_ID);
		financialInstitutionContact.setStartDate(DateUtil.getLocalDateTimeNow());
		financialInstitutionContact.setEndDate(DateUtil.getLocalDateTimeNow());
		financialInstitutionContactsDTO.add(financialInstitutionContact);

		financialInstitutionContact = new ContactFinancialInstitutionDTO();
		financialInstitutionContact.getContact().setCrmContactId("100122256");
		financialInstitutionContact.setFinancialInstitutionId(VbsConstants.FINANCIAL_INSTITUTION_CIBC_ID);
		financialInstitutionContact.setStartDate(DateUtil.getLocalDateTimeNow());
		financialInstitutionContact.setEndDate(DateUtil.getLocalDateTimeNow());
		financialInstitutionContactsDTO.add(financialInstitutionContact);

		return financialInstitutionContactsDTO;
	}

	private FinancialInstitutionDTO createNewFinancialInstitutionDTO() {
		FinancialInstitutionDTO financialInstitutionDTO = new FinancialInstitutionDTO();
		financialInstitutionDTO.setName("Test name");
		financialInstitutionDTO.setFinancialInstitutionTypeId(2l);
		financialInstitutionDTO.setDescription("Test description");
		financialInstitutionDTO.setMaxSecurityAmount(new BigDecimal("52000.25"));
		financialInstitutionDTO.setWnSecurityMaxAmount(new BigDecimal("1502000.25"));
		financialInstitutionDTO.setExpiryDate(DateUtil.getLocalDateTimeNow().plusYears(2l));

		return financialInstitutionDTO;
	}
	
	private FinancialInstitutionDTO createNewSuretyFinancialInstitutionDTO() {
		FinancialInstitutionDTO financialInstitutionDTO = new FinancialInstitutionDTO();
		financialInstitutionDTO.setName("Test name");
		financialInstitutionDTO.setFinancialInstitutionTypeId(VbsConstants.FINANCIAL_INSTITUTION_TYPE_SURETY_COMPANIES);
		financialInstitutionDTO.setDescription("Test description");
		financialInstitutionDTO.setMaxSecurityAmount(new BigDecimal("52000.25"));
		financialInstitutionDTO.setWnSecurityMaxAmount(new BigDecimal("1502000.25"));
		financialInstitutionDTO.setExpiryDate(DateUtil.getLocalDateTimeNow().plusYears(2l));

		return financialInstitutionDTO;
	}

	private FinancialInstitutionDTO createExistingFinancialInstitutionDTO() {
		FinancialInstitutionDTO financialInstitutionDTO = createNewFinancialInstitutionDTO();
		financialInstitutionDTO.setId(1l);
		financialInstitutionDTO.setCreateDate(DateUtil.getLocalDateTimeNow());
		financialInstitutionDTO.setCreateUser(VbsConstants.VBS_SYSTEM_USER_ID);

		return financialInstitutionDTO;
	}

	private BranchDTO createNewBranchDTO() {
		BranchDTO branch = new BranchDTO();
		branch.setName("Test name");
		branch.setDescription("Test description");

		return branch;
	}

	private BranchDTO createExistingBranchDTO() {
		BranchDTO branch = createNewBranchDTO();
		branch.setId(145l);
		branch.setCreateDate(DateUtil.getLocalDateTimeNow());
		branch.setCreateUser(VbsConstants.VBS_SYSTEM_USER_ID);

		return branch;
	}
}
