/* 
 * 
 * SecurityServiceTest.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tarion.vbs.common.dto.release.ReleaseDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.service.release.ReleaseService;
import com.tarion.vbs.service.release.ReleaseServiceImpl;
import com.tarion.vbs.service.test.Container;

public class FmsQueueResponseProcessorTest {

	private UserTransaction userTransaction;
	private FmsQueueResponseProcessor fmsQueueResponseProcessor;
	private ReleaseService releaseService;
	
//	private String xmlFmsResponse = "<?xml version=\"1.0\"?><TWC_SEC_STAT_V2><FieldTypes><PSCAMA class=\"R\"><LANGUAGE_CD type=\"CHAR\"/><AUDIT_ACTN type=\"CHAR\"/><BASE_LANGUAGE_CD type=\"CHAR\"/><MSG_SEQ_FLG type=\"CHAR\"/><PROCESS_INSTANCE type=\"NUMBER\"/><PUBLISH_RULE_ID type=\"CHAR\"/><MSGNODENAME type=\"CHAR\"/></PSCAMA></FieldTypes><MsgData><Transaction><TWC_SEC_STAT class=\"R\"><TWC_SECURITY_NBR>12345</TWC_SECURITY_NBR><COMPANYID>B12345</COMPANYID><TWC_UNPOST_REASON>STALE_DATE</TWC_UNPOST_REASON><DESCR>Stale Dated Cheques AAA</DESCR><ROW_ADDED_DTTM>2018-12-14-10.59.40.000000</ROW_ADDED_DTTM></TWC_SEC_STAT></Transaction></MsgData></TWC_SEC_STAT_V2>";
	private String xmlFmsCreateCashSecurityResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
			"<createCashSecurityResponse xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"SendCashSecurityResponse.xsd\">\n" + 
			"  <securityId>39051</securityId>\n" + 
			"  <status>P</status>\n" + 
			"  <error></error>\n" + 
			"</createCashSecurityResponse>\n" + 
			"";
	
	private long relId = 384862l;
	private long secId = 133l;
	private String vbNumber = "B3248";

	private String xmlFmsCreateDrawDownResponse = "<?xml version=\"1.0\"?>\n" +
			"<createDrawDownResponse>\n" +
			"  <id>" + relId + "</id>\n" +
			"  <securityId>" + secId + "</securityId>\n" +
			"  <sequenceNumber>1</sequenceNumber>\n" +
			"  <depositId>36876</depositId>\n" +
			"  <depositDate>2019-03-21</depositDate>\n" +
			"  <amount>750</amount>\n" +
			"  <collector>STHOMPSON</collector>\n" +
			"  <status>P</status>\n" +
			"</createDrawDownResponse>\n" +
			"";

//	private String xmlFmsAllocationResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
//			"<allocationResponse xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"AllocationResponse.xsd\">\n" +
//			"  <securityId>" + secId + "</securityId>\n" +
//			"  <vbNumber>" + vbNumber + "</vbNumber>\n" +
//			"  <status/>\n" +
//			"  <unpostReason/>\n" +
//			"  <unpostByUser/>\n" +
//			"  <unpostDate/>\n"  +
//            "</allocationResponse>\n" +
//			"";
	
	private String xmlFmsAllocationRequest = "<?xml version=\"1.0\"?>\n" + 
			"<allocationRequest>\n" + 
			"  <vbNumber>B12345</vbNumber>\n" + 
			"  <securityId>40892</securityId>\n" + 
			"  <status>P</status>\n" + 
			"  <unpostReason></unpostReason>\n" + 
			"  <unpostByUser></unpostByUser>\n" + 
			"  <unpostDate></unpostDate>\n" + 
			"</allocationRequest>\n" ; 

//    private String xmlFmsUnpostResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
//            "<unpostResponse xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"UnpostResponse.xsd\">\n" +
//            "  <securityId>" + secId + "</securityId>\n" +
//            "  <vbNumber>" + vbNumber + "</vbNumber>\n" +
//            "  <status/>\n" +
//            "  <unpostReason>" + "NSF" + "</unpostReason>\n" +
//            "  <unpostByUser>" + "jdasilva" + "</unpostByUser>\n" +
//            "  <unpostDate>" + "2019-03-21" + "</unpostDate>\n"  +
//            "</unpostResponse>\n" +
//            "";

    private String xmlFmsUnpostRequest = "<?xml version=\"1.0\"?>\n" + 
    		"<unpostRequest>\n" + 
    		"  <vbNumber>B12345</vbNumber>\n" + 
    		"  <securityId>40892</securityId>\n" + 
    		"  <status>F</status>\n" + 
    		"  <unpostReason>Non-Sufficient Funds</unpostReason>\n" + 
    		"  <unpostByUser>TWCBATCH</unpostByUser>\n" + 
    		"  <unpostDate>2019-05-03-16.47.27.000000</unpostDate>\n" + 
    		"</unpostRequest>\n" ; 
    
    private String xmlFmsUnpostRequest2 = "<?xml version=\"1.0\"?>\n" + 
    		"<unpostRequest>\n" + 
    		"  <vbNumber>B12345</vbNumber>\n" + 
    		"  <securityId>40895</securityId>\n" + 
    		"  <status>F</status>\n" + 
    		"  <unpostReason>Non-Sufficient Funds</unpostReason>\n" + 
    		"  <unpostByUser>TWCBATCH</unpostByUser>\n" + 
    		"  <unpostDate>2019-05-06-22.58.40.000000</unpostDate>\n" + 
    		"</unpostRequest>\n";

	@Before
	public void lookupABean() throws NamingException {
		InitialContext ctx = new InitialContext();
		userTransaction = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
		try {userTransaction.begin();} catch (Exception e) {e.printStackTrace();}
		fmsQueueResponseProcessor = Container.getBean(FmsQueueResponseProcessorImpl.class);
		releaseService = Container.getBean(ReleaseServiceImpl.class);
	}
	
	@After
	public void cleanUp() {
		try {userTransaction.rollback();} catch (Exception e) {e.printStackTrace();}
	}


	@Test
	public void test() {
		assertNotNull(fmsQueueResponseProcessor);
	}

	@Test
	public void testResponseCreateCashSecurity() {
		fmsQueueResponseProcessor.processReceivedFmsXml(xmlFmsCreateCashSecurityResponse);
	}

//	@Test
	public void testFmsAllocationRequestCashSecurity() {
		fmsQueueResponseProcessor.processReceivedFmsXml(xmlFmsAllocationRequest);
	}

//    @Test
    public void testFmsUnpostRequestCashSecurity() {
        fmsQueueResponseProcessor.processReceivedFmsXml(xmlFmsUnpostRequest2);
    }

	@Test
	public void testResponseCreateDrawDownResponse() throws VbsCheckedException {
		List<ReleaseDTO> releases = releaseService.getReleases(secId);
		for (ReleaseDTO release : releases) {
			if (release.getId().longValue() == relId) {
				fmsQueueResponseProcessor.processReceivedFmsXml(xmlFmsCreateDrawDownResponse);
			}
		}
	}
}
