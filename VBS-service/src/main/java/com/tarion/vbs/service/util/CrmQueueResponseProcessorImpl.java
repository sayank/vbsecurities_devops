/* 
 * 
 * CrmQueueResponseProcessorImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import com.tarion.crm.contactws.GetContactInfoResponse;
import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.autorelease.AutoReleaseEnrolmentDataDTO;
import com.tarion.vbs.common.dto.autorelease.AutoReleaseVbDataDTO;
import com.tarion.vbs.common.enums.EnrolmentStatus;
import com.tarion.vbs.common.enums.ReleaseStatus;
import com.tarion.vbs.common.exceptions.VbsJAXBConversionException;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.contact.ContactDAO;
import com.tarion.vbs.dao.financialinstitution.FinancialInstitutionDAO;
import com.tarion.vbs.dao.release.AutoReleaseDAO;
import com.tarion.vbs.dao.release.ReleaseDAO;
import com.tarion.vbs.dao.security.SecurityDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.dao.util.JmsTransactionLogDAO;
import com.tarion.vbs.dao.util.LookupDAO;
import com.tarion.vbs.orm.entity.AddressEntity;
import com.tarion.vbs.orm.entity.JmsTransactionLogEntity;
import com.tarion.vbs.orm.entity.autorelease.AutoReleaseRunEntity;
import com.tarion.vbs.orm.entity.contact.*;
import com.tarion.vbs.orm.entity.enrolment.CEEnrolmentDecisionEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
import com.tarion.vbs.orm.entity.enrolment.HomeCategoryEntity;
import com.tarion.vbs.orm.entity.financialinstitution.BranchEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionEntity;
import com.tarion.vbs.orm.entity.release.ReleaseEnrolmentEntity;
import com.tarion.vbs.orm.entity.release.ReleaseEntity;
import com.tarion.vbs.service.jaxb.crm.autorelease.AutoReleaseEnrolmentsResponse;
import com.tarion.vbs.service.jaxb.crm.autorelease.AutoReleaseVBsResponse;
import com.tarion.vbs.service.jaxb.crm.delta.company.AddressDTO;
import com.tarion.vbs.service.jaxb.crm.delta.company.CompanyDelta;
import com.tarion.vbs.service.jaxb.crm.delta.company.CompanyDelta.Relationships;
import com.tarion.vbs.service.jaxb.crm.delta.company.RelationshipDTO;
import com.tarion.vbs.service.jaxb.crm.delta.enrolment.EnrolmentDTO;
import com.tarion.vbs.service.jaxb.crm.delta.enrolment.EnrolmentsDelta;
import com.tarion.vbs.service.jaxb.crm.delta.person.PersonDelta;
import com.tarion.vbs.service.jaxb.crm.financialinstitution.FinancialInstitutionUpdateResponse;
import com.tarion.vbs.service.jaxb.crm.warrantyservicesinput.WarrantyServicesInputResponse;
import com.tarion.vbs.service.jaxb.crm.warrantyservicesinput.WarrantyServicesRecommendationResponse;
import com.tarion.vbs.service.processor.ProcessorTriggeringService;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Processing Responses from Crm Update Queue MDB 
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @date 2018-04-20
 * @version 1.0	
 */
@Stateless(mappedName = "ejb/CrmQueueResponseProcessor")
@Interceptors(EjbLoggingInterceptor.class)
public class CrmQueueResponseProcessorImpl implements CrmQueueResponseProcessor {
	
	@EJB
	GenericDAO genericDAO;
	@EJB
	private ContactDAO contactDAO;
	@EJB
	private LookupDAO lookupDAO;
	@EJB
	private CrmWSInvocationService crmWSInvocationService;
	@EJB
	private JmsTransactionLogDAO jmsTransactionLogDAO;
	@EJB
    private AutoReleaseDAO autoReleaseDAO;
	@EJB
	private ProcessorTriggeringService processorTriggeringService;
	@EJB
	private ReleaseDAO releaseDAO;
	@EJB
	private FinancialInstitutionDAO financialInstitutionDAO;
	@EJB
	private SecurityDAO securityDAO;

	@Override
	public void process(EnrolmentsDelta response, String xmlResponse) {
		if (response != null && !response.getEnrolment().isEmpty()) {
			this.insertTransactionLog(JmsMessageTypeEnum.getCrmUpdateVbsDeltaEnrolmentResponseTracking(xmlResponse), xmlResponse);
			for (EnrolmentDTO siteDTO : response.getEnrolment()) {
				if (siteDTO.getEnrolmentNumber() != null) {
					EnrolmentEntity enrolment = genericDAO.findEntityById(EnrolmentEntity.class, siteDTO.getEnrolmentNumber());
					boolean isNew = false;
					if(enrolment == null) {
					    isNew = true;
						enrolment = new EnrolmentEntity();
						enrolment.setEnrolmentNumber(siteDTO.getEnrolmentNumber());

						if (siteDTO.getCreateDate() != null) {
							enrolment.setCreateDate(DateUtil.getLocalDateTimeFromCrmDateString(siteDTO.getCreateDate()));
						} else {
							enrolment.setCreateDate(DateUtil.getLocalDateTimeNow());
						}
						if (siteDTO.getCreateUser() != null) {
							enrolment.setCreateUser(siteDTO.getCreateUser());
						} else {
							enrolment.setCreateUser(VbsConstants.VBS_CRM_SYSTEM_USER_ID);
						}
						if (siteDTO.getUpdateUser() != null) {
							enrolment.setUpdateUser(siteDTO.getUpdateUser());
						}
						if (siteDTO.getUpdateDate() != null) {
							enrolment.setUpdateDate(DateUtil.getLocalDateTimeFromCrmDateString(siteDTO.getUpdateDate()));
						}
					}

                    ContactEntity vendor = contactDAO.getContactByCrmContactId(siteDTO.getVendor());
                    if(vendor == null) {
                        throw new VbsRuntimeException(String.format("Unable to add Enrolment %s, VB %s not found in database", siteDTO.getEnrolmentNumber(), siteDTO.getVendor()));
                    }
                    enrolment.setVendor(vendor);

                    if(!VbsUtil.isNullorEmpty(siteDTO.getBuilder())){
						ContactEntity builder = contactDAO.getContactByCrmContactId(siteDTO.getBuilder());
						if(builder != null) {
							enrolment.setBuilder(builder);
						}
					}

                    ContactEntity vb = contactDAO.getContactByCrmContactId(siteDTO.getEnrollingVb());
                    if(vb == null) {
                        throw new VbsRuntimeException(String.format("Unable to add Enrolment %s, VB %s not found in database", siteDTO.getEnrolmentNumber(), siteDTO.getEnrollingVb()));
                    }

                    enrolment.setUnitType(siteDTO.getUnitType());
                    enrolment.setEnrollingVb(vb);
                    enrolment.setCondoConversion(VbsUtil.getBooleanPrimitiveFromString(siteDTO.getCondoConversion()));
                    enrolment.setHomeCategory(genericDAO.findEntityById(HomeCategoryEntity.class, siteDTO.getHomeCategory()));
                    enrolment.setWarrantyStartDate(DateUtil.getLocalDateTimeFromCrmDateString(siteDTO.getWarrantyStartDate()));
                    enrolment.setEntryDate(DateUtil.getLocalDateTimeFromCrmDateString(siteDTO.getEntryDate()));
                    
                    enrolment.setWsFcr(siteDTO.getWsFCR());
                    enrolment.setCondoCorpHoName(siteDTO.getCondoCorpHoName());
                    
                    enrolment.setStatus(
                    		siteDTO.getEnrolmentStatus() == ""
                    			? null 
                    			: EnrolmentStatus.valueOf(siteDTO.getEnrolmentStatus().toUpperCase()));
                    
                    if (siteDTO.getAddress() != null) {
                    	AddressEntity enrolmentAddress = enrolment.getAddress();
                    	if (enrolmentAddress == null) {
                    		enrolmentAddress = createNewAddress(siteDTO.getAddress());
                    		enrolmentAddress = (AddressEntity)genericDAO.addEntity(enrolmentAddress, VbsConstants.VBS_CRM_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
                    	} else {
                    		enrolmentAddress = transformDTOToEntity(siteDTO.getAddress(), enrolmentAddress);
                    		enrolmentAddress = (AddressEntity)genericDAO.updateEntity(enrolmentAddress, VbsConstants.VBS_CRM_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
                    	}
                		enrolment.setAddress(enrolmentAddress);
                    }

                    if(isNew) {
                        genericDAO.addEntity(enrolment, VbsConstants.VBS_CRM_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
                        if(enrolment.getUnitType().equalsIgnoreCase(VbsConstants.ENROLMENT_UNIT_TYPE_CE) && !securityDAO.getPoolIdsByVbBlanketSecurity(enrolment.getEnrollingVb().getId()).isEmpty()){
							CEEnrolmentDecisionEntity decisionEntity = new CEEnrolmentDecisionEntity(enrolment);
							securityDAO.addCEEnrolmentDecisionEntity(decisionEntity);
						}
                    } else {
                        genericDAO.updateEntity(enrolment, VbsConstants.VBS_CRM_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
                    }
                }
			}
		}
	}	
	
	private ContactEntity transformDTOToCompanyEntity(CompanyDelta response, ContactEntity company) {
		company.setAccessibility(VbsUtil.getBooleanPrimitiveFromString(response.getCompany().getAccessibility()));
		// Address is done separately
		company.setAlert(response.getCompany().getAlert());
		company.setCompanyName(response.getCompany().getCompanyName());
		// Contact Type is done separately
		company.setCrmContactId(response.getCompany().getCrmContactId());
		if(!StringUtils.isEmpty(response.getCompany().getEmailAddress())) {
			company.setEmailAddress(response.getCompany().getEmailAddress());
		}else{
			company.setEmailAddress(null);
		}
		company.setEscrowLicenseApplicationStatus(getEscrowLicenseApplicationStatusEntity(response.getCompany().getEscrowLicenceApplicationStatus()));
		company.setLicenseStatus(getLicenseStatusEntity(response.getCompany().getLicenceStatus()));
		// Mailing address is done separately
		company.setPhoneExtension(response.getCompany().getPhoneExtension());
		company.setPhone(response.getCompany().getPhoneNumber());
		company.setUnwilling(response.getCompany().getUnwilling());
		company.setYellowSticky(VbsUtil.getBooleanPrimitiveFromString(response.getCompany().getYellowSticky()));
		company.setApVendorFlag(VbsUtil.getBooleanPrimitiveFromString(response.getCompany().getApVendorFlag()));
		
		return company;
	}	
	
	private BranchEntity transformDTOToBranchEntity(CompanyDelta response, BranchEntity branch) {
		branch.setId(Long.parseLong(response.getCompany().getCrmContactId()));
		branch.setDescription(null);
		branch.setName(response.getCompany().getCompanyName());

		return branch;
	}	

	private ContactEntity transformDTOToPersonContactEntity(PersonDelta response, ContactEntity personContact) {
		personContact.setCrmContactId(response.getPerson().getCrmContactId());
		personContact.setDateOfBirth(DateUtil.getLocalDateTimeFromCrmDateString(response.getPerson().getDateOfBirth()));
		personContact.setDriversLicence(response.getPerson().getDriversLicence());
		personContact.setEmailAddress(response.getPerson().getEmailAddress());
		personContact.setFirstName(response.getPerson().getFirstName());
		personContact.setLastName(response.getPerson().getLastName());
		personContact.setPhoneExtension(response.getPerson().getPhoneExtension());
		personContact.setPhone(response.getPerson().getPhoneNumber());
		personContact.setSin(response.getPerson().getSin());
		personContact.setApVendorFlag(VbsUtil.getBooleanPrimitiveFromString(response.getPerson().getApVendorFlag()));
		return personContact;
	}	
	
	private ContactEntity transformCrmWSDTOToContactEntity(GetContactInfoResponse.CONTACTINFO response, ContactEntity contact) {
		AddressEntity address = createNewAddress(response.getCONTACTOTHERADDR());
		if (address != null) {
			address = (AddressEntity)genericDAO.addEntity(address, address.getCreateUser(), address.getCreateDate());
		}
		contact.setAddress(address);
		contact.setCompanyName(response.getCOMPANYNAME());
		contact.setCreateDate(DateUtil.getLocalDateTimeFromCrmDateString(response.getCREATEDDATE()));
		contact.setCreateUser(response.getCREATEDBY());
		contact.setCrmContactId(response.getCONTACTID());
		contact.setEmailAddress(response.getEMAILADDR());
		contact.setFirstName(response.getFIRSTNAME());
		contact.setLastName(response.getLASTNAME());
		AddressEntity mailingAddress = createNewAddress(response.getCONTACTMAILINGADDR());
		if (mailingAddress != null) {
			mailingAddress = (AddressEntity)genericDAO.addEntity(mailingAddress, mailingAddress.getCreateUser(), mailingAddress.getCreateDate());
		}
		contact.setMailingAddress(mailingAddress);
		contact.setPhoneExtension(response.getCONTACTPHONEPRIMARY().getEXTENSION());
		contact.setPhone(response.getCONTACTPHONEPRIMARY().getPHONE());
		if(contact.getFirstName() != null || contact.getLastName() != null) {
			ContactTypeEntity contactType = genericDAO.findEntityById(ContactTypeEntity.class, VbsConstants.CONTACT_TYPE_PERSON);
			contact.setContactType(contactType);
		}
		
		if(contact.getCompanyName() != null) {
			ContactTypeEntity contactType = genericDAO.findEntityById(ContactTypeEntity.class, VbsConstants.CONTACT_TYPE_COMPANY);
			contact.setContactType(contactType);
		}

		contact.setUpdateDate(DateUtil.getLocalDateTimeFromCrmDateString(response.getUPDATEDDATE()));
		contact.setUpdateUser(response.getUPDATEDBY());

		contact = (ContactEntity)genericDAO.addEntity(contact, contact.getCreateUser(), contact.getCreateDate());
		return contact;
	}	
	
	private ContactEntity updateExistingCompany(ContactEntity existingCompany, CompanyDelta response) {
		// All companies if they exist should be updated independent on passed company type
		// contact type is not the same like company type and should not be updated
//		ContactTypeEntity contactType = getContactTypeEntity(response.getCompany().getCompanyType());
//		if (contactType != null) {
			existingCompany = transformDTOToCompanyEntity(response, existingCompany);
			if (response.getAddress() != null) {
				if (existingCompany.getAddress() != null) {
					AddressEntity newAddress = transformDTOToEntity(response.getAddress(), existingCompany.getAddress());
					newAddress = (AddressEntity)genericDAO.updateEntity(newAddress, newAddress.getUpdateUser(), newAddress.getUpdateDate());
					existingCompany.setAddress(newAddress);
				} else {
					AddressEntity newAddress = new AddressEntity();
					newAddress = transformDTOToEntity(response.getAddress(), newAddress);
					newAddress = (AddressEntity)genericDAO.addEntity(newAddress, newAddress.getCreateUser(), newAddress.getCreateDate());
					existingCompany.setAddress(newAddress);
				}
			}
			// contact type remains as already set, cannot be changed via delta
//			existingCompany.setContactType(contactType);
			if (response.getMailingAddress() != null) {
				if (existingCompany.getMailingAddress() != null) {
					AddressEntity newMailingAddress = transformDTOToEntity(response.getMailingAddress(), existingCompany.getMailingAddress());
					newMailingAddress = (AddressEntity)genericDAO.updateEntity(newMailingAddress, newMailingAddress.getUpdateUser(), newMailingAddress.getUpdateDate());
					existingCompany.setMailingAddress(newMailingAddress);
				} else {
					AddressEntity newMailingAddress = new AddressEntity();
					newMailingAddress = transformDTOToEntity(response.getAddress(), newMailingAddress);
					newMailingAddress = (AddressEntity)genericDAO.addEntity(newMailingAddress, newMailingAddress.getCreateUser(), newMailingAddress.getCreateDate());
					existingCompany.setMailingAddress(newMailingAddress);
				}
			}
//		}
		return existingCompany;
	}
	
	private BranchEntity updateExistingBranch(BranchEntity existingBranch, CompanyDelta response) {
		existingBranch = transformDTOToBranchEntity(response, existingBranch);
		if (response.getMailingAddress() != null) {
			if (existingBranch.getAddress() != null) {
				existingBranch.setAddress(transformDTOToEntity(response.getMailingAddress(), existingBranch.getAddress()));
			} else {
				AddressEntity newAddress = new AddressEntity();
				newAddress = transformDTOToEntity(response.getMailingAddress(), newAddress);
				newAddress = (AddressEntity)genericDAO.addEntity(newAddress, newAddress.getCreateUser(), newAddress.getCreateDate());
				existingBranch.setAddress(newAddress);
			}
		} else if (response.getAddress() != null) {
			if (existingBranch.getAddress() != null) {
				existingBranch.setAddress(transformDTOToEntity(response.getAddress(), existingBranch.getAddress()));
			} else {
				AddressEntity newAddress = new AddressEntity();
				newAddress = transformDTOToEntity(response.getAddress(), newAddress);
				newAddress = (AddressEntity)genericDAO.addEntity(newAddress, newAddress.getCreateUser(), newAddress.getCreateDate());
				existingBranch.setAddress(newAddress);
			}
		}

		return existingBranch;
	}
	
	private ContactEntity updateExistingPersonContact(ContactEntity existingPerson, PersonDelta response) {
		existingPerson = transformDTOToPersonContactEntity(response, existingPerson);
		if (response.getAddress() != null) {
			existingPerson.setAddress(transformDTOToEntity(response.getAddress(), existingPerson.getAddress()));
		}
		if (response.getMailingAddress() != null) {
			existingPerson.setMailingAddress(transformDTOToEntity(response.getMailingAddress(), existingPerson.getMailingAddress()));
		}
		return existingPerson;
	}
	
	private ContactTypeEntity getContactTypeEntity(String crmCompanyTypeString) {
		Long crmCompanyType =  null;
		try {
			crmCompanyType = Long.parseLong(crmCompanyTypeString);
		} catch (NumberFormatException e) {
			LoggerUtil.logError(CrmQueueResponseProcessorImpl.class, "getContactTypeEntity", "Invalid Company Type received from CRM " + crmCompanyTypeString);
			crmCompanyType = null;
		}
		ContactTypeEntity contactTypeEntity = null;
		if (crmCompanyType != null) {
			if (VbsConstants.CONTACT_TYPE_PERSON == crmCompanyType) {
				contactTypeEntity = genericDAO.findEntityById(ContactTypeEntity.class, VbsConstants.CONTACT_TYPE_PERSON);
			} else if (VbsConstants.CONTACT_TYPE_COMPANY == crmCompanyType) {
				contactTypeEntity = genericDAO.findEntityById(ContactTypeEntity.class, VbsConstants.CONTACT_TYPE_COMPANY);
			} else if (VbsConstants.CONTACT_TYPE_ESCROW_AGENT == crmCompanyType) {
				contactTypeEntity = genericDAO.findEntityById(ContactTypeEntity.class, VbsConstants.CONTACT_TYPE_ESCROW_AGENT);
			} else if (VbsConstants.CONTACT_TYPE_VB == crmCompanyType) {
				contactTypeEntity = genericDAO.findEntityById(ContactTypeEntity.class, VbsConstants.CONTACT_TYPE_VB);
			}
		}
		return contactTypeEntity;
	}
	
	private EscrowLicenseApplicationStatusEntity getEscrowLicenseApplicationStatusEntity(String crmEscrowLicenseApplicationStatusString) {
		EscrowLicenseApplicationStatusEntity escrowLicenseApplicationStatusEntity = null;
		if (crmEscrowLicenseApplicationStatusString != null) {
			escrowLicenseApplicationStatusEntity = lookupDAO.getEscrowLicenseApplicationStatusEntity(crmEscrowLicenseApplicationStatusString);
		}
		
		return escrowLicenseApplicationStatusEntity;
	}
	
	private LicenseStatusEntity getLicenseStatusEntity(String crmLicenseStatusString) {
		LicenseStatusEntity licenseStatusEntity = null;
		if (crmLicenseStatusString != null) {
			licenseStatusEntity = lookupDAO.getLicenseStatusForName(crmLicenseStatusString);
		}
		return licenseStatusEntity;
	}
	
	private AddressEntity createNewAddress(AddressDTO crmCompanyAddress) {
		if (crmCompanyAddress != null) {
			AddressEntity newAddress = new AddressEntity();
			newAddress = transformDTOToEntity(crmCompanyAddress, newAddress);
			return newAddress;
		}
		return null;
	}
	
	private AddressEntity createNewAddress(com.tarion.vbs.service.jaxb.crm.delta.person.AddressDTO crmPersonAddress) {
		if (crmPersonAddress != null) {
			AddressEntity newAddress = new AddressEntity();
			newAddress = transformDTOToEntity(crmPersonAddress, newAddress);
			return newAddress;
		}
		return null;
	}
	
	private AddressEntity createNewAddress(com.tarion.vbs.service.jaxb.crm.delta.enrolment.AddressDTO enrolmentAddress) {
		if (enrolmentAddress != null) {
			AddressEntity newAddress = new AddressEntity();
			newAddress = transformDTOToEntity(enrolmentAddress, newAddress);
			return newAddress;
		}
		return null;
	}
	
	private AddressEntity createNewAddress(GetContactInfoResponse.CONTACTINFO.CONTACTOTHERADDR crmCompanyAddress) {
		if (crmCompanyAddress != null) {
			AddressEntity newAddress = new AddressEntity();
			if(!StringUtils.isEmpty(crmCompanyAddress.getADDRESS1())) {
				newAddress.setAddressLine1(crmCompanyAddress.getADDRESS1());
			}else{
				newAddress.setAddressLine1(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getADDRESS2())) {
				newAddress.setAddressLine2(crmCompanyAddress.getADDRESS2());
			}else{
				newAddress.setAddressLine2(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getADDRESS3())) {
				newAddress.setAddressLine3(crmCompanyAddress.getADDRESS3());
			}else{
				newAddress.setAddressLine3(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getADDRESS4())) {
				newAddress.setAddressLine4(crmCompanyAddress.getADDRESS4());
			}else{
				newAddress.setAddressLine4(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getCITY())) {
				newAddress.setCity(crmCompanyAddress.getCITY());
			}else{
				newAddress.setCity(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getCOUNTRY())) {
				newAddress.setCountry(crmCompanyAddress.getCOUNTRY());
			}else{
				newAddress.setCountry(null);
			}
			newAddress.setCreateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmCompanyAddress.getCREATEDDATE()));
			newAddress.setCreateUser(crmCompanyAddress.getCREATEDBY());
			if(!StringUtils.isEmpty(crmCompanyAddress.getPOSTAL())) {
				newAddress.setPostalCode(crmCompanyAddress.getPOSTAL());
			}else{
				newAddress.setPostalCode(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getSTATE())) {
				newAddress.setProvince(crmCompanyAddress.getSTATE());
			}else{
				newAddress.setProvince(null);
			}
			newAddress.setUpdateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmCompanyAddress.getUPDATEDDATE()));
			newAddress.setUpdateUser(crmCompanyAddress.getUPDATEDBY());

			return newAddress;
		} 
		return null;
	}
	
	private AddressEntity createNewAddress(GetContactInfoResponse.CONTACTINFO.CONTACTMAILINGADDR crmCompanyAddress) {
		if (crmCompanyAddress != null) {
			AddressEntity newAddress = new AddressEntity();
			if(!StringUtils.isEmpty(crmCompanyAddress.getADDRESS1())) {
				newAddress.setAddressLine1(crmCompanyAddress.getADDRESS1());
			}else{
				newAddress.setAddressLine1(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getADDRESS2())) {
				newAddress.setAddressLine2(crmCompanyAddress.getADDRESS2());
			}else{
				newAddress.setAddressLine2(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getADDRESS3())) {
				newAddress.setAddressLine3(crmCompanyAddress.getADDRESS3());
			}else{
				newAddress.setAddressLine3(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getADDRESS4())) {
				newAddress.setAddressLine4(crmCompanyAddress.getADDRESS4());
			}else{
				newAddress.setAddressLine4(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getCITY())) {
				newAddress.setCity(crmCompanyAddress.getCITY());
			}else{
				newAddress.setCity(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getCOUNTRY())) {
				newAddress.setCountry(crmCompanyAddress.getCOUNTRY());
			}else{
				newAddress.setCountry(null);
			}
			newAddress.setCreateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmCompanyAddress.getCREATEDDATE()));
			newAddress.setCreateUser(crmCompanyAddress.getCREATEDBY());
			if(!StringUtils.isEmpty(crmCompanyAddress.getPOSTAL())) {
				newAddress.setPostalCode(crmCompanyAddress.getPOSTAL());
			}else{
				newAddress.setPostalCode(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getSTATE())) {
				newAddress.setProvince(crmCompanyAddress.getSTATE());
			}else{
				newAddress.setProvince(null);
			}
			newAddress.setUpdateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmCompanyAddress.getUPDATEDDATE()));
			newAddress.setUpdateUser(crmCompanyAddress.getUPDATEDBY());

			return newAddress;
		}
		return null;
	}
	
	private AddressEntity transformDTOToEntity(AddressDTO crmCompanyAddress, AddressEntity newAddress) {
		if (crmCompanyAddress != null) {
			if(!StringUtils.isEmpty(crmCompanyAddress.getAddressLine1())) {
				newAddress.setAddressLine1(crmCompanyAddress.getAddressLine1());
			}else{
				newAddress.setAddressLine1(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getAddressLine2())){
				newAddress.setAddressLine2(crmCompanyAddress.getAddressLine2());
			}else{
				newAddress.setAddressLine2(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getAddressLine3())) {
				newAddress.setAddressLine3(crmCompanyAddress.getAddressLine3());
			}else{
				newAddress.setAddressLine3(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getAddressLine4())){
				newAddress.setAddressLine4(crmCompanyAddress.getAddressLine4());
			}else{
				newAddress.setAddressLine4(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getCity())) {
				newAddress.setCity(crmCompanyAddress.getCity());
			}else{
				newAddress.setCity(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getCountry())) {
				newAddress.setCountry(crmCompanyAddress.getCountry());
			}
			if (crmCompanyAddress.getCreateDate() != null) {
				newAddress.setCreateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmCompanyAddress.getCreateDate()));
			} 
			if (newAddress.getCreateDate() == null) {
				newAddress.setCreateDate(DateUtil.getLocalDateTimeNow());
			}
			if (crmCompanyAddress.getCreateUser() != null) {
				newAddress.setCreateUser(crmCompanyAddress.getCreateUser());
			} else {
				if (newAddress.getCreateUser() == null) {
					newAddress.setCreateUser(VbsConstants.VBS_CRM_SYSTEM_USER_ID);
				}
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getPostalCode())) {
				newAddress.setPostalCode(crmCompanyAddress.getPostalCode());
			}else{
				newAddress.setPostalCode(null);
			}
			if(!StringUtils.isEmpty(crmCompanyAddress.getProvince())) {
				newAddress.setProvince(crmCompanyAddress.getProvince());
			}else{
				newAddress.setProvince(null);
			}
			newAddress.setUpdateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmCompanyAddress.getUpdateDate()));
			newAddress.setUpdateUser(crmCompanyAddress.getUpdateUser());
		}
		return newAddress;
	}	
	
	private AddressEntity transformDTOToEntity(com.tarion.vbs.service.jaxb.crm.delta.enrolment.AddressDTO crmEnrolmentAddress, AddressEntity newAddress) {
		if (crmEnrolmentAddress != null) {
			if(!StringUtils.isEmpty(crmEnrolmentAddress.getAddressLine1())) {
				newAddress.setAddressLine1(crmEnrolmentAddress.getAddressLine1());
			}else{
				newAddress.setAddressLine1(null);
			}
			if(!StringUtils.isEmpty(crmEnrolmentAddress.getAddressLine2())) {
				newAddress.setAddressLine2(crmEnrolmentAddress.getAddressLine2());
			}else{
				newAddress.setAddressLine2(null);
			}
			if(!StringUtils.isEmpty(crmEnrolmentAddress.getAddressLine3())) {
				newAddress.setAddressLine3(crmEnrolmentAddress.getAddressLine3());
			}else{
				newAddress.setAddressLine3(null);
			}
			if(!StringUtils.isEmpty(crmEnrolmentAddress.getAddressLine4())) {
				newAddress.setAddressLine4(crmEnrolmentAddress.getAddressLine4());
			}else{
				newAddress.setAddressLine4(null);
			}
			if(!StringUtils.isEmpty(crmEnrolmentAddress.getCity())) {
				newAddress.setCity(crmEnrolmentAddress.getCity());
			}else{
				newAddress.setCity(null);
			}
			if(!StringUtils.isEmpty(crmEnrolmentAddress.getCountry())) {
				newAddress.setCountry(crmEnrolmentAddress.getCountry());
			}else{
				newAddress.setCountry(null);
			}
			if (crmEnrolmentAddress.getCreateDate() != null) {
				newAddress.setCreateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmEnrolmentAddress.getCreateDate()));
			} 
			if (newAddress.getCreateDate() == null) {
				newAddress.setCreateDate(DateUtil.getLocalDateTimeNow());
			}
			if (crmEnrolmentAddress.getCreateUser() != null) {
				newAddress.setCreateUser(crmEnrolmentAddress.getCreateUser());
			} else {
				if (newAddress.getCreateUser() == null) {
					newAddress.setCreateUser(VbsConstants.VBS_CRM_SYSTEM_USER_ID);
				}
			}
			if(!StringUtils.isEmpty(crmEnrolmentAddress.getPostalCode())) {
				newAddress.setPostalCode(crmEnrolmentAddress.getPostalCode());
			}else{
				newAddress.setPostalCode(null);
			}
			if(!StringUtils.isEmpty(crmEnrolmentAddress.getProvince())) {
				newAddress.setProvince(crmEnrolmentAddress.getProvince());
			}else{
				newAddress.setProvince(null);
			}
			newAddress.setUpdateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmEnrolmentAddress.getUpdateDate()));
			newAddress.setUpdateUser(crmEnrolmentAddress.getUpdateUser());
		}
		return newAddress;
	}
	
	private AddressEntity transformDTOToEntity(com.tarion.vbs.service.jaxb.crm.delta.person.AddressDTO crmPersonAddress, AddressEntity newAddress) {
		if (crmPersonAddress != null) {
			if(!StringUtils.isEmpty(crmPersonAddress.getAddressLine1())) {
				newAddress.setAddressLine1(crmPersonAddress.getAddressLine1());
			}else{
				newAddress.setAddressLine1(null);
			}
			if(!StringUtils.isEmpty(crmPersonAddress.getAddressLine2())) {
				newAddress.setAddressLine2(crmPersonAddress.getAddressLine2());
			}else{
				newAddress.setAddressLine2(null);
			}
			if(!StringUtils.isEmpty(crmPersonAddress.getAddressLine3())) {
				newAddress.setAddressLine3(crmPersonAddress.getAddressLine3());
			}else{
				newAddress.setAddressLine3(null);
			}
			if(!StringUtils.isEmpty(crmPersonAddress.getAddressLine4())) {
				newAddress.setAddressLine4(crmPersonAddress.getAddressLine4());
			}else{
				newAddress.setAddressLine4(null);
			}
			newAddress.setCity(crmPersonAddress.getCity());
			newAddress.setCountry(crmPersonAddress.getCountry());
			if (crmPersonAddress.getCreateDate() != null) {
				newAddress.setCreateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmPersonAddress.getCreateDate()));
			} 
			if (newAddress.getCreateDate() == null) {
				newAddress.setCreateDate(DateUtil.getLocalDateTimeNow());
			}
			if (crmPersonAddress.getCreateUser() != null) {
				newAddress.setCreateUser(crmPersonAddress.getCreateUser());
			} else {
				if (newAddress.getCreateUser() == null) {
					newAddress.setCreateUser(VbsConstants.VBS_CRM_SYSTEM_USER_ID);
				} 
			}
			if(!StringUtils.isEmpty(crmPersonAddress.getPostalCode())) {
				newAddress.setPostalCode(crmPersonAddress.getPostalCode());
			}else{
				newAddress.setPostalCode(null);
			}
			if(!StringUtils.isEmpty(crmPersonAddress.getProvince())) {
				newAddress.setProvince(crmPersonAddress.getProvince());
			}
			newAddress.setUpdateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmPersonAddress.getUpdateDate()));
			newAddress.setUpdateUser(crmPersonAddress.getUpdateUser());
		}
		return newAddress;
	}

	private ContactEntity createNewCompany(CompanyDelta response) {
		ContactTypeEntity contactType = getContactTypeEntity(response.getCompany().getCompanyType());
		ContactEntity newCompany = null;
		if (contactType != null) {
			newCompany = new ContactEntity();
			newCompany = transformDTOToCompanyEntity(response, newCompany);
			newCompany.setAddress(createNewAddress(response.getAddress()));
			newCompany.setContactType(contactType);
			newCompany.setMailingAddress(createNewAddress(response.getMailingAddress()));
		}
		return newCompany;
	}

	private ContactEntity createNewPerson(PersonDelta response) {
		ContactTypeEntity contactType = getContactTypeEntity("" + VbsConstants.CONTACT_TYPE_PERSON);
		ContactEntity newPerson = new ContactEntity();
		newPerson = transformDTOToPersonContactEntity(response, newPerson);
		newPerson.setAddress(createNewAddress(response.getAddress()));
		newPerson.setContactType(contactType);
		newPerson.setMailingAddress(createNewAddress(response.getMailingAddress()));

		return newPerson;
	}


	private BranchEntity createNewBranch(CompanyDelta response) {
		BranchEntity newBranch = new BranchEntity();
		newBranch = transformDTOToBranchEntity(response, newBranch);
		if (response.getMailingAddress() != null) {
			newBranch.setAddress(createNewAddress(response.getMailingAddress()));
		} else if (response.getAddress() != null) {
			newBranch.setAddress(createNewAddress(response.getAddress()));
		}
		return newBranch;
	}
	
	private List<ContactRelationshipEntity> createNewRelationships(Relationships relationships) {
		List<ContactRelationshipEntity> newRelationships = new ArrayList<>();
		if (relationships != null) {
			for (RelationshipDTO crmRelationship : relationships.getRelationship()) {
				ContactRelationshipEntity newRelationship = createNewRelationship(crmRelationship);
				if (newRelationship != null) {
					newRelationships.add(newRelationship);
				}
			}
		}
		
		return newRelationships;
	}

	private List<ContactRelationshipEntity> createNewRelationships(com.tarion.vbs.service.jaxb.crm.delta.person.PersonDelta.Relationships relationships) {
		List<ContactRelationshipEntity> newRelationships = new ArrayList<>();
		if (relationships != null) {
			for (com.tarion.vbs.service.jaxb.crm.delta.person.RelationshipDTO crmRelationship : relationships.getRelationship()) {
				ContactRelationshipEntity newRelationship = createNewRelationship(crmRelationship);
				if (newRelationship != null) {
					newRelationships.add(newRelationship);
				}
			}
		}
		
		return newRelationships;
	}


	private ContactRelationshipEntity createNewRelationship(RelationshipDTO crmRelationship) {
		RelationshipTypeEntity relationshipType = genericDAO.findEntityById(RelationshipTypeEntity.class, crmRelationship.getRelationshipType());
		// we create relationship only of the types we have in VBS
		if (relationshipType != null) {
			ContactRelationshipEntity newRelationship = new ContactRelationshipEntity();
			ContactEntity fromContact = contactDAO.getContactByCrmContactId(crmRelationship.getFromCrmContactId());
			ContactEntity toContact = contactDAO.getContactByCrmContactId(crmRelationship.getToCrmContactId());
			ContactEntity startContact = contactDAO.getContactByCrmContactId(crmRelationship.getCrmContactId());
			
			if (fromContact != null && startContact != null && fromContact.getId().compareTo(startContact.getId()) == 0) {
				if (toContact == null) {
					toContact = createNewContact(crmRelationship.getToCrmContactId());
				}
				newRelationship.setFromContact(fromContact);
				newRelationship.setToContact(toContact);
				newRelationship.setCreateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmRelationship.getCreateDate()));
				newRelationship.setCreateUser(crmRelationship.getCreateUser());
				newRelationship.setCrmRelId(crmRelationship.getCrmRelId());
				newRelationship.setEndDate(DateUtil.getLocalDateTimeFromCrmDateString(crmRelationship.getEndDate()));
				newRelationship.setRelationshipType(relationshipType);
				newRelationship.setStartDate(DateUtil.getLocalDateTimeFromCrmDateString(crmRelationship.getStartDate()));
				newRelationship.setUpdateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmRelationship.getUpdateDate()));
				newRelationship.setUpdateUser(crmRelationship.getUpdateUser());
				
				newRelationship = (ContactRelationshipEntity)genericDAO.addEntity(newRelationship, newRelationship.getCreateUser(), newRelationship.getCreateDate());
				return newRelationship;
			}
		}
		return null;
	}
	
	private ContactRelationshipEntity createNewRelationship(com.tarion.vbs.service.jaxb.crm.delta.person.RelationshipDTO crmRelationship) {
		RelationshipTypeEntity relationshipType = genericDAO.findEntityById(RelationshipTypeEntity.class, crmRelationship.getRelationshipType());
		// we create relationship only of the types we have in VBS
		if (relationshipType != null) {
			ContactRelationshipEntity newRelationship = new ContactRelationshipEntity();
			ContactEntity fromContact = contactDAO.getContactByCrmContactId(crmRelationship.getFromCrmContactId());
			ContactEntity toContact = contactDAO.getContactByCrmContactId(crmRelationship.getToCrmContactId());
			ContactEntity startContact = contactDAO.getContactByCrmContactId(crmRelationship.getCrmContactId());
			
			if (fromContact != null && startContact != null && fromContact.getId().compareTo(startContact.getId()) == 0) {
				if (toContact == null) {
					toContact = createNewContact(crmRelationship.getToCrmContactId());
				}
				newRelationship.setFromContact(fromContact);
				newRelationship.setToContact(toContact);
				newRelationship.setCreateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmRelationship.getCreateDate()));
				newRelationship.setCreateUser(crmRelationship.getCreateUser());
				newRelationship.setCrmRelId(crmRelationship.getCrmRelId());
				newRelationship.setEndDate(DateUtil.getLocalDateTimeFromCrmDateString(crmRelationship.getEndDate()));
				newRelationship.setRelationshipType(relationshipType);
				newRelationship.setStartDate(DateUtil.getLocalDateTimeFromCrmDateString(crmRelationship.getStartDate()));
				newRelationship.setUpdateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmRelationship.getUpdateDate()));
				newRelationship.setUpdateUser(crmRelationship.getUpdateUser());
				
				newRelationship = (ContactRelationshipEntity)genericDAO.addEntity(newRelationship, newRelationship.getCreateUser(), newRelationship.getCreateDate());
				return newRelationship;
			}
		}
		return null;
	}
	private ContactEntity createNewContact(String crmContactId) {
		GetContactInfoResponse.CONTACTINFO crmContact = getCrmContact(crmContactId);
    	ContactEntity newContact = new ContactEntity();
    	newContact = transformCrmWSDTOToContactEntity(crmContact, newContact);
    	return newContact;
    }

    private GetContactInfoResponse.CONTACTINFO getCrmContact(String crmContactId) {
    	GetContactInfoResponse crmContactResponse = crmWSInvocationService.getContactInfoByCrmContactId(crmContactId);
		if (crmContactResponse.getCONTACTINFO().size() > 1) {
			throw new VbsRuntimeException("Got more than one result for a given CRM Contact Id: " + crmContactId);
		} else if (!VbsConstants.CRM_WS_OK.equalsIgnoreCase(crmContactResponse.getSTATUS())) {
			throw new VbsRuntimeException("Received error: " + crmContactResponse.getERRORMSG() + " from CRM for CRM Contact Id: " + crmContactId);
		} else if (crmContactResponse.getCONTACTINFO().size() == 0) {
			throw new VbsRuntimeException("No CRM Contact received from CRM for a given CRM Contact Id: " + crmContactId);
		}
		
		return crmContactResponse.getCONTACTINFO().get(0);
    }
    
    private void processBranch(CompanyDelta response) {
		if (response.getCompany() != null) {
			if (!VbsUtil.isNullorEmpty(response.getCompany().getFinancialInstitutionId())) {
				String crmContactId = response.getCompany().getCrmContactId();
				Long branchId = Long.parseLong(crmContactId);
				BranchEntity branch = genericDAO.findEntityById(BranchEntity.class, branchId);
				FinancialInstitutionEntity fi = genericDAO.findEntityById(FinancialInstitutionEntity.class, Long.parseLong(response.getCompany().getFinancialInstitutionId()));
				if (fi != null) {
					if (branch == null) {
						// add branch
						branch = createNewBranch(response);
						if (branch != null) {
							LocalDateTime createDate = DateUtil.getLocalDateTimeNow();
							branch.setCreateDate(createDate);
							String createUser = VbsConstants.VBS_CRM_SYSTEM_USER_ID;
							if(!VbsUtil.isNullorEmpty(response.getCompany().getCreateUser())) {
								createUser = response.getCompany().getCreateUser();
							}
							branch.setCreateUser(createUser);
							if (branch.getAddress() != null) {
								AddressEntity address = (AddressEntity)genericDAO.addEntity(branch.getAddress(), createUser, createDate);
								branch.setAddress(address);
							}
							branch.setFinancialInstitution(fi);
							branch = (BranchEntity)genericDAO.addEntity(branch, createUser, createDate);
						}
					} else {
						// update branch
						LocalDateTime updateDate = DateUtil.getLocalDateTimeNow();
						String updateUser = VbsConstants.VBS_CRM_SYSTEM_USER_ID;
						if(!VbsUtil.isNullorEmpty(response.getCompany().getUpdateUser())) {
							updateUser = response.getCompany().getCreateUser();
						}
						branch = updateExistingBranch(branch, response);
						branch.setUpdateDate(updateDate);
						branch.setUpdateUser(updateUser);
						genericDAO.updateEntity(branch, updateUser, updateDate);
					}
				}
			}
		}
    }
    
	@Override
	public void process(CompanyDelta response, String xmlResponse) {
		if (response != null) {
			this.insertTransactionLog(JmsMessageTypeEnum.getCrmUpdateVbsDeltaCompanyResponseTracking(xmlResponse), xmlResponse);
			if (response.getCompany() != null) {
				if (!VbsUtil.isNullorEmpty(response.getCompany().getFinancialInstitutionId()) && !VbsConstants.FINANCIAL_INSTITUTION_ZERO.equalsIgnoreCase(response.getCompany().getFinancialInstitutionId())) {
					// this is branch - create / update branch instead of contact
					processBranch(response);
				} else {
					String crmContactId = response.getCompany().getCrmContactId();
					ContactEntity company = contactDAO.getContactByCrmContactId(crmContactId);
					if (company == null) {
						// add company
						// only company with VBS contact type should be added 
						// (check performed in createNewCompany)
						company = createNewCompany(response);
						if (company != null) {
							LocalDateTime createDate = DateUtil.getLocalDateTimeNow();
							company.setCreateDate(createDate);
							String systemUserId = VbsConstants.VBS_CRM_SYSTEM_USER_ID;
							company.setCreateUser(systemUserId);
							if (company.getAddress() != null) {
								AddressEntity address = (AddressEntity)genericDAO.addEntity(company.getAddress(), systemUserId, createDate);
								company.setAddress(address);
							}
							if (company.getMailingAddress() != null) {
								AddressEntity address = (AddressEntity)genericDAO.addEntity(company.getMailingAddress(), systemUserId, createDate);
								company.setMailingAddress(address);
							}
							
							company = (ContactEntity)genericDAO.addEntity(company, systemUserId, createDate);
							createNewRelationships(response.getRelationships());
						} else {
							// no need to add company - it is of type that we don't have in VBS
						}
					} else {
						// update company
						// all crm contacts should be updated independent on contact type.
						// contact type should not be matched with companyTypeId in the message
						company = updateExistingCompany(company, response);
						LocalDateTime updateDate = DateUtil.getLocalDateTimeNow();
						company.setUpdateDate(updateDate);
						String systemUserId = VbsConstants.VBS_CRM_SYSTEM_USER_ID;
						company.setUpdateUser(systemUserId);
						genericDAO.updateEntity(company, systemUserId, updateDate);
						// update relationships
						for (RelationshipDTO relationship : response.getRelationships().getRelationship()) {
							ContactRelationshipEntity matchedRelationship = contactDAO.getRelationshipByCrmRelId(relationship.getCrmRelId());
							if (matchedRelationship != null) {
								// update relationship
								RelationshipTypeEntity relationshipType = genericDAO.findEntityById(RelationshipTypeEntity.class, relationship.getRelationshipType());
								if (relationshipType != null) {
									matchedRelationship.setEndDate(DateUtil.getLocalDateTimeFromCrmDateString(relationship.getEndDate()));
									matchedRelationship.setRelationshipType(relationshipType);
									matchedRelationship.setStartDate(DateUtil.getLocalDateTimeFromCrmDateString(relationship.getStartDate()));
									matchedRelationship.setUpdateDate(DateUtil.getDateFromCrmDateTimeString(relationship.getUpdateDate()));
									matchedRelationship.setUpdateUser(relationship.getUpdateUser());
									genericDAO.updateEntity(matchedRelationship, matchedRelationship.getUpdateUser(), matchedRelationship.getUpdateDate());
								}
							} else {
								createNewRelationship(relationship);
							}
						}
					}
				}
			} else {
				// log that we didnt receive Company part of message
				LoggerUtil.logError(CrmQueueResponseProcessorImpl.class, "process(CompanyDelta)", "Company part of XML is missing");
			}
		}
	}
	@Override
	public void process(FinancialInstitutionUpdateResponse response, String xmlResponse) {
		if (response != null) {
			this.insertTransactionLog(JmsMessageTypeEnum.getCrmFinancialInstitutionUpdateResponseTracking(xmlResponse), xmlResponse);

		}
	}
	@Override
	public void process(PersonDelta response, String xmlResponse) {
		if (response != null) {
			this.insertTransactionLog(JmsMessageTypeEnum.getCrmUpdateVbsDeltaPersonResponseTracking(xmlResponse), xmlResponse);
			if (response.getPerson() != null) {
				String crmContactId = response.getPerson().getCrmContactId();
				ContactEntity contact = contactDAO.getContactByCrmContactId(crmContactId);
				// updating only person - we do not insert new person via delta
				if (contact != null) {
					contact = updateExistingPersonContact(contact, response);
					LocalDateTime updateDate = DateUtil.getLocalDateTimeNow();
					contact.setUpdateDate(updateDate);
					String systemUserId = VbsConstants.VBS_CRM_SYSTEM_USER_ID;
					contact.setUpdateUser(systemUserId);
					genericDAO.updateEntity(contact, systemUserId, updateDate);
				} else {
					// we need to check if delta has relationship of type
					// 	public static final long RELATIONSHIP_TYPE_LAWYER = 20166l;
					//public static final long RELATIONSHIP_TYPE_ASSISTANT = 20167l;
					// if yes, we need to add that person
					boolean addPerson = false;
					if (response.getRelationships() != null && response.getRelationships().getRelationship() != null) {
						for (com.tarion.vbs.service.jaxb.crm.delta.person.RelationshipDTO personRelationship : response.getRelationships().getRelationship()) {
							if (personRelationship.getRelationshipType() == VbsConstants.RELATIONSHIP_TYPE_LAWYER
									|| personRelationship.getRelationshipType() == VbsConstants.RELATIONSHIP_TYPE_ASSISTANT) {
								// we need to add this contact
								addPerson = true;
								break;
							}
						}
					}
					if (addPerson) {
						// add contact, its addresses and relationships
						ContactEntity newPerson = createNewPerson(response);
						LocalDateTime createDate = DateUtil.getLocalDateTimeNow();
						newPerson.setCreateDate(createDate);
						String systemUserId = VbsConstants.VBS_CRM_SYSTEM_USER_ID;
						newPerson.setCreateUser(systemUserId);
						if (newPerson.getAddress() != null) {
							AddressEntity address = (AddressEntity)genericDAO.addEntity(newPerson.getAddress(), systemUserId, createDate);
							newPerson.setAddress(address);
						}
						if (newPerson.getMailingAddress() != null) {
							AddressEntity address = (AddressEntity)genericDAO.addEntity(newPerson.getMailingAddress(), systemUserId, createDate);
							newPerson.setMailingAddress(address);
						}
						
						newPerson = (ContactEntity)genericDAO.addEntity(newPerson, systemUserId, createDate);
						createNewRelationships(response.getRelationships());
					}
				}
			} else {
				// log error that we didnt receive Person part of the message
				LoggerUtil.logError(CrmQueueResponseProcessorImpl.class, "process(PersonDelta)", "Person part of XML is missing");
			}
		}
	}	
	
	@Override
	public void process(WarrantyServicesInputResponse response, String xmlResponse) {
		if (response != null) {
			this.updateTransactionLog(JmsMessageTypeEnum.getCrmWarrantyServiceResponseTrackingUpdate(response.getReleaseId(), response.getEnrolmentNumber()), xmlResponse, JmsMessageTypeEnum.getCrmWarrantyServiceResponseTrackingInsert(response.getReleaseId(), response.getEnrolmentNumber()));
			if (response.getReleaseId() != 0) {
				ReleaseEntity release = genericDAO.findEntityById(ReleaseEntity.class, response.getReleaseId());
				if (release != null) {
					if (response.getServiceOrderId() != null && VbsConstants.CRM_JMS_STATUS_OK.equals(response.getStatus())) {
						release.setServiceOrderId(response.getServiceOrderId());
						release.setFcmAmountRetained(null);
					} else {
						LoggerUtil.logError(CrmQueueResponseProcessorImpl.class, "processWarrantyServicesInputResponse", "No Service Order Id found for CRM response" + xmlResponse);
					}
					genericDAO.updateEntity(release, VbsConstants.VBS_CRM_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
				} else {
					LoggerUtil.logError(CrmQueueResponseProcessorImpl.class, "processWarrantyServicesInputResponse", "No Release found for CRM response" + xmlResponse);
				}
			}
		}
	}

	@Override
	public void process(AutoReleaseEnrolmentsResponse response, String xmlResponse) {
		if (response != null) {
			this.updateTransactionLog(JmsMessageTypeEnum.getCrmAutoReleaseEnrolmentsRequestTracking(response.getAutoReleaseRunId()), xmlResponse, JmsMessageTypeEnum.getCrmAutoReleaseEnrolmentsResponseTracking(response.getAutoReleaseRunId()));

			autoReleaseDAO.addAutoReleaseEnrolmentDataEntities(
			        response
                            .getEnrolments()
                            .getEnrolment()
                            .stream()
                            .map(e -> new AutoReleaseEnrolmentDataDTO(
                                    response.getAutoReleaseRunId(),
                                    e.getEnrolmentNumber(),
                                    DateUtil.getLocalDateTimeFromCrmDateString(e.getDateOfPossession()),
                                    e.getUnresolvedCases(),
                                    VbsConstants.Y.equals(e.getInventoryItem())))
                            .collect(Collectors.toList()));

			AutoReleaseRunEntity run = genericDAO.findEntityById(AutoReleaseRunEntity.class, response.getAutoReleaseRunId());
			run.setEnrolmentsReceived(true);
			genericDAO.updateEntity(run, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
			processorTriggeringService.triggerCheckDataReceived(response.getAutoReleaseRunId());

		}
	}

	@Override
	public void process(AutoReleaseVBsResponse response, String xmlResponse) {
		if (response != null) {
			this.updateTransactionLog(JmsMessageTypeEnum.getCrmAutoReleaseVBsRequestTracking(response.getAutoReleaseRunId()), xmlResponse, JmsMessageTypeEnum.getCrmAutoReleaseVBsResponseTracking(response.getAutoReleaseRunId()));

			List<AutoReleaseVbDataDTO> vbs = response.getVbList().getVb().stream()
                    .map(vb -> new AutoReleaseVbDataDTO(
                            response.getAutoReleaseRunId(),
                            vb.getVbNumber(),
                            VbsConstants.Y.equals(vb.getUnderInvestigation()),
                            vb.getCreditRating(),
                            VbsUtil.parseLong(vb.getLicenseStatus()),
                            VbsConstants.Y.equals(vb.getActiveAlert()),
                            VbsUtil.parseLong(vb.getOsCCPsLicenseExpired()),
                            vb.getAvgGuarantorCreditRating(),
                            vb.getNumGuarantors(),
                            VbsConstants.Y.equals(vb.getUnwillingUnable())))
					.collect(Collectors.toList());

			autoReleaseDAO.addAutoReleaseVbDataEntities(vbs);

			AutoReleaseRunEntity run = genericDAO.findEntityById(AutoReleaseRunEntity.class, response.getAutoReleaseRunId());
			run.setVbsReceived(true);
			genericDAO.updateEntity(run, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
			processorTriggeringService.triggerCheckDataReceived(response.getAutoReleaseRunId());

		}
	}
	
	@Override
	public void process(WarrantyServicesRecommendationResponse response, String xmlResponse) {
		if (response != null) {
			insertTransactionLog(JmsMessageTypeEnum.getCrmWarrantyServiceRecommendationResponseTrackingInsert(response.getEnrolmentNumber(), response.getServiceOrderId()), xmlResponse);
			if (response.getServiceOrderId() != null && response.getEnrolmentNumber() != null) {
				List<ReleaseEnrolmentEntity> releaseEnrolments = releaseDAO.getReleasesByEnrolmentNumber(response.getEnrolmentNumber());
				for (ReleaseEnrolmentEntity releaseEnrolment : releaseEnrolments) {
					ReleaseEntity release = releaseEnrolment.getRelease();
					if (release.getStatus().compareTo(ReleaseStatus.PENDING) != 0 && release.getStatus().compareTo(ReleaseStatus.REJECTED) != 0 && release.getStatus().compareTo(ReleaseStatus.COMPLETED) != 0 &&
                            release.isWsInputRequested() && (release.getServiceOrderId() == null || release.getServiceOrderId() != null) && release.getServiceOrderId().equalsIgnoreCase(response.getServiceOrderId())) {
						release.setServiceOrderId(response.getServiceOrderId());
						release.setWsInputProvidedBy(response.getInputProvidedUser());
						release.setWsReceivedDate(DateUtil.getLocalDateTimeFromCrmDateString(response.getInputProvidedDate()));
						release.setWsRecommendation(lookupDAO.getWarrantyServicesRecommendationTypeEntity(response.getWsRecommendationCode()));
						release.setFcmAmountRetained(VbsUtil.parseMoney(response.getFcmAmountRetained()));
						genericDAO.updateEntity(release, response.getInputProvidedUser(), DateUtil.getLocalDateTimeFromCrmDateString(response.getInputProvidedDate()));
					}
				}
			}
		}
	}
	
	@Override
	public void processReceivedCrmXml(String xmlResponse) {
		Object jaxbObject = null;
		try {
			jaxbObject = VbsUtil.convertXmltoJaxb(xmlResponse);
			Method m = this.getClass().getMethod("process", jaxbObject.getClass(), String.class);
			m.invoke(this, jaxbObject, xmlResponse);
		}catch(VbsJAXBConversionException e) {
			//converting didn't work
			LoggerUtil.logError(CrmQueueResponseProcessorImpl.class, "processReceivedXml ", e, "XML to JAXB Conversion Failed.", xmlResponse);
			throw e;
		}catch(NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
			//error invoking the method (not found/not allowed)
			LoggerUtil.logError(CrmQueueResponseProcessorImpl.class, "processReceivedXml", e, "Unable to invoke process method.", xmlResponse);
			throw new VbsRuntimeException(e.getMessage());
		}catch(Exception e) {
			//other unknown exception/exception thrown from process method
			LoggerUtil.logError(CrmQueueResponseProcessorImpl.class, "processReceivedXml", e, "Error occured during processing.", xmlResponse);
			throw e;
		}
	}

	private void insertTransactionLog(String trackingNumber, String xmlMessageText) {
		JmsTransactionLogEntity entity = new JmsTransactionLogEntity();
		entity.setTrackingNumber(trackingNumber);
		entity.setVersion(1l);
		entity.setMessageName(trackingNumber);
		entity.setResponse(xmlMessageText);
		entity.setResponseReceivedDate(DateUtil.getLocalDateTimeNow());
		genericDAO.addEntity(entity, VbsConstants.VBS_CRM_SYSTEM_USER_ID, entity.getResponseReceivedDate());
	}	
	
	private void updateTransactionLog(String trackingNumberLike, String xmlMessageText, String trackingNumberInsert) {
		List<JmsTransactionLogEntity> logs = jmsTransactionLogDAO.findTranscationLogLikeTrackingNumber(trackingNumberLike);
		if (logs.size() == 1) {
			JmsTransactionLogEntity log = logs.get(0);
			log.setResponse(xmlMessageText);
			log.setResponseReceivedDate(DateUtil.getLocalDateTimeNow());
			genericDAO.updateEntity(log, VbsConstants.VBS_CRM_SYSTEM_USER_ID, log.getResponseReceivedDate());
		} else {
			insertTransactionLog(trackingNumberInsert, xmlMessageText);;
		}
	}	
}
