package com.tarion.vbs.service.enrolment;

import static com.tarion.vbs.common.util.DateUtil.getLocalDateTimeFromCrmDateString;
import static com.tarion.vbs.common.util.VbsUtil.isCRMTrue;
import static com.tarion.vbs.common.util.VbsUtil.isCRMTrueFalseNull;

import java.util.ArrayList;
import java.util.List;

import javax.interceptor.Interceptors;
import javax.persistence.Tuple;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.tarion.crm.vbsws.GetSecHomeInfoResponse;
import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.enrolment.EnrolmentDTO;
import com.tarion.vbs.common.enums.EnrolmentStatus;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentPoolEntity;

/**
 * Transformer helper class used by Enrolment Service to provide transformation between DTO and Entity
 *
 * @author <A href="joni.paananen@tarion.com">Joni Paananen</A>
 * @since 2018-12-14
 * @version 1.0
 */
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class EnrolmentServiceTransformer {
	
	private EnrolmentServiceTransformer() {}
	
	public static List<EnrolmentDTO> transformCrmDTOList(GetSecHomeInfoResponse response) {
		List<EnrolmentDTO> enrolments = new ArrayList<>();
		
		for (GetSecHomeInfoResponse.SECHOMEINFO home : response.getSECHOMEINFO()) {
			EnrolmentDTO enrolment = transformCRMDTO(home);
			enrolments.add(enrolment);
		}
		
		return enrolments;
	}

	public static EnrolmentDTO transformCRMDTO(GetSecHomeInfoResponse.SECHOMEINFO home) {
		EnrolmentDTO enrolment = new EnrolmentDTO();
		enrolment.setEnrolmentNumber(home.getENROLMENTNUMBER());
		enrolment.setCondoCorpHomeOwnerName(home.getCONDOCORPHONAME());
		enrolment.setDopWsd(getLocalDateTimeFromCrmDateString(home.getTWCWARRANTYSTDT()));
		enrolment.setFinalB19Accepted(isCRMTrueFalseNull(home.getB19ACCEPTED()));
		enrolment.setLuReviewedDnd(isCRMTrueFalseNull(home.getLUREVIEWEDDD()));
		enrolment.setNumberOfUnitsOsCcp(NumberUtils.toLong(home.getOUTSTANDINGCCP()));
		enrolment.setOpenCases(isCRMTrue(home.getOPENCASES()));
		enrolment.setRaHomeCategory(home.getRAHOMECATAGORY());
		enrolment.setReleasePerBB28(isCRMTrueFalseNull(home.getRELEASEBB28()));
		enrolment.setTntReceivedPercentage(StringUtils.isEmpty(home.getTTRECEIVEDPERCENTAGE()) ? null : NumberUtils.toLong(home.getTTRECEIVEDPERCENTAGE()));
		enrolment.setTwoYearsAfter(isCRMTrue(home.getSECONDYRAFTREG()));
		enrolment.setWsFcr(home.getWSFCR());
		enrolment.setInventoryItem(isCRMTrue(home.getINVENTORYHOME()));
		enrolment.setAddress(home.getADDRESS());
		enrolment.setFcmAmountRetained(VbsUtil.parseMoney(home.getFCMAMOUNTRETAINED()));
		enrolment.setCondoConversion(isCRMTrue(home.getCONDOCONV()));
		enrolment.setTotalNumberOfCuEnrolled(StringUtils.isEmpty(home.getCUENROLLED()) ? null : NumberUtils.toLong(home.getCUENROLLED()));
		
		enrolment.setPefAmount(VbsUtil.parseMoney(home.getTWCPEFAMT()));
		if (home.getTWCCLRNCLTRISSUE() != null && VbsConstants.Y.equalsIgnoreCase(home.getTWCCLRNCLTRISSUE())) { 
			enrolment.setClearanceLetterIssued(true);
		}
		enrolment.setClearanceDate(getLocalDateTimeFromCrmDateString(home.getTWCCLEARANCEDATE()));
		
		return enrolment;
	}

	public static EnrolmentDTO transformTupleToDTO(Tuple entity) {
		EnrolmentDTO enrolment = new EnrolmentDTO();
		enrolment.setEnrolmentNumber(entity.get("enrolmentNumber", String.class));
		enrolment.setEnrollingVb(entity.get("crmContactId", String.class));
		return enrolment;
	}
	
	public static EnrolmentDTO transformEntityToDTO(EnrolmentEntity entity) {
		EnrolmentDTO enrolment = new EnrolmentDTO();
		enrolment.setEnrolmentNumber(entity.getEnrolmentNumber());
		enrolment.setEnrollingVb((entity.getEnrollingVb()!= null) ? entity.getEnrollingVb().getCrmContactId() : null);
		enrolment.setAutoOneYear(entity.isAutoOneYear());
		enrolment.setAutoOneYearRfc(entity.isAutoOneYearRfc());
		enrolment.setAutoTwoYear(entity.isAutoTwoYear());
		enrolment.setExcessDeposit(entity.isExcessDeposit());
		enrolment.setExcessDepositRelease(entity.isExcessDepositRelease());
		return enrolment;
	}

	public static void transformEntityToDTO(EnrolmentPoolEntity entity, EnrolmentDTO dto) {
		dto.setEnrolmentNumber(entity.getEnrolment().getEnrolmentNumber());
		dto.setAllocatedAmount(entity.getAllocatedAmount());
		dto.setCurrentAllocatedAmount(entity.getCurrentAllocatedAmount());
		dto.setClearanceDate(entity.getClearanceDate());
		dto.setClearanceLetterIssued(entity.isClearanceLetterIssued());
		dto.setExcessAmount(entity.getExcessAmount());
		dto.setInventoryAmount(entity.getInventoryAmount());
		dto.setMaxUnitDeposit(entity.getMaxUnitDeposit());
		dto.setPefAmount(entity.getPefAmount());
		dto.setRaAmount(entity.getRaAmount());
        if(entity.getEnrolment().getEnrollingVb() != null) {
            dto.setEnrollingVb(entity.getEnrolment().getEnrollingVb().getCrmContactId());
        }
        if(entity.getEnrolment().getVendor() != null) {
            dto.setVendor(entity.getEnrolment().getVendor().getCrmContactId());
        }
		if(entity.getEnrolment().getBuilder() != null) {
			dto.setBuilder(entity.getEnrolment().getBuilder().getCrmContactId());
		}
		dto.setUnitType(entity.getEnrolment().getUnitType());
		dto.setHomeCategoryId(entity.getEnrolment().getHomeCategory().getId());
		dto.setCondoConversion(entity.getEnrolment().isCondoConversion());
		dto.setWarrantyStartDate(entity.getEnrolment().getWarrantyStartDate());
		dto.setExcessDeposit(entity.getEnrolment().isExcessDeposit());
		dto.setExcessDepositRelease(entity.getEnrolment().isExcessDepositRelease());
		dto.setAutoOneYear(entity.getEnrolment().isAutoOneYear());
		dto.setAutoOneYearRfc(entity.getEnrolment().isAutoOneYearRfc());
		dto.setAutoTwoYear(entity.getEnrolment().isAutoTwoYear());
		dto.setIsCE(!entity.getEnrolment().getHomeCategory().getId().equals(VbsConstants.HOME_CATEGORY_FREEHOLD));
		dto.setMultiSecurity(entity.getEnrolment().isMultiSecurity());
		dto.setEnrolmentStatus(entity.getEnrolment().getStatus());
	}
	
	public static void transformDTOToEntity(EnrolmentPoolEntity entity, EnrolmentDTO dto) {
		entity.setAllocatedAmount(dto.getAllocatedAmount());
		entity.setCurrentAllocatedAmount(dto.getCurrentAllocatedAmount());
		entity.setClearanceDate(dto.getClearanceDate());
		entity.setClearanceLetterIssued(dto.getClearanceLetterIssued());
		entity.setExcessAmount(dto.getExcessAmount());
		entity.setInventoryAmount(dto.getInventoryAmount());
		entity.setMaxUnitDeposit(dto.getMaxUnitDeposit());
		entity.setPefAmount(dto.getPefAmount());
		entity.setRaAmount(dto.getRaAmount());
	}

	

}
