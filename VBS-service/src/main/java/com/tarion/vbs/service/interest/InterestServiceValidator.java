/* 
 * 
 * SecurityServiceValidator.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.interest;

import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.dto.interest.InterestRateDTO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.interest.InterestRateEntity;

import javax.interceptor.Interceptors;
import java.util.ArrayList;
import java.util.List;


/**
 * Lookup Service provides operations to fetch the data for all lookup tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-09
 * @version 1.0
 */
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class InterestServiceValidator {
	
	private InterestServiceValidator() {}

	public static List<ErrorDTO> validateInterestRate(InterestRateDTO interestRateDTO, InterestRateEntity latestRate) {
		List<ErrorDTO> ret = new ArrayList<>();
		
		if (interestRateDTO.getInterestStartDate() == null) {
			ErrorDTO err = new ErrorDTO("The Interest Rate Start Date cannot be empty.", "interestStartDate");
			ret.add(err);
		}

		if (interestRateDTO.getAnnualRate() == null) {
			ErrorDTO err = new ErrorDTO("The Interest Rate cannot be empty.", "annualRate");
			ret.add(err);
		}

		if (interestRateDTO.getInterestStartDate() != null && latestRate != null && !interestRateDTO.getInterestStartDate().isAfter(latestRate.getInterestStartDate())) {
			ErrorDTO err = new ErrorDTO("The Interest Rate Start Date must be after latest Interest Rate Start Date.", "interestStartDate");
			ret.add(err);
		}
		
		return ret;
	}

}
