package com.tarion.vbs.service.userprofile;

import java.util.List;

import javax.ejb.Remote;

import com.tarion.vbs.common.dto.user.PermissionDTO;
import com.tarion.vbs.common.dto.user.RoleDTO;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.exceptions.VbsDirectoryEnvironmentException;

@Remote
public interface UserServiceRemote {

	//ROLES
	public RoleDTO getRoleById(Long id);
	public List<RoleDTO> getAllRoles();
	public List<RoleDTO> getRolesForUser(String userId);
	public RoleDTO createRole(RoleDTO role, UserDTO actionUser);
	public RoleDTO updateRole(RoleDTO role, UserDTO actionUser);
	public void deleteRole(Long roleId, UserDTO actionUser);
	public RoleDTO addPermissionToRole(Long permissionId, Long roleId, UserDTO actionUser);
	public RoleDTO removePermissionFromRole(Long permissionId, Long roleId, UserDTO actionUser);
	
	//PERMISSIONS
	public PermissionDTO getPermissionById(Long id);
	public List<PermissionDTO> getAllPermissions();
	public List<PermissionDTO> getPermissionsByRole(Long roleId);
	public List<PermissionDTO> getAvailablePermissionsByRole(Long roleId);
	public List<PermissionDTO> getPermissionsForUser(String userId);
	public List<UserDTO> getUsersByPermissionId(Long permissionId) throws VbsDirectoryEnvironmentException;
	public List<UserDTO> getUsersByPermission(PermissionDTO permission) throws VbsDirectoryEnvironmentException;
	public List<UserDTO> getUsersByPermissionName(String permissionName) throws VbsDirectoryEnvironmentException;
	public PermissionDTO updatePermission(PermissionDTO permission);	
	
	//USERS
	public UserDTO getUserById(String userId) throws VbsDirectoryEnvironmentException;
	public List<UserDTO> getUsersByRole(RoleDTO role) throws VbsDirectoryEnvironmentException;
	public List<UserDTO> getUsersByRoleName(String roleName) throws VbsDirectoryEnvironmentException;
	public UserDTO addUserToRole(String userId, Long roleId, UserDTO actionUser) throws VbsDirectoryEnvironmentException;
	public UserDTO removeUserFromRole(String userId, Long roleId, UserDTO actionUser) throws VbsDirectoryEnvironmentException;
	public List<UserDTO> getNewUserList() throws VbsDirectoryEnvironmentException;
	public UserDTO getUser(String userId) throws VbsDirectoryEnvironmentException;

}