package com.tarion.vbs.service.userprofile;

import java.util.List;

import javax.ejb.Local;

import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.exceptions.VbsDirectoryEnvironmentException;

@Local
public interface UserService extends UserServiceRemote {

	public List<UserDTO> getAllUserList() throws VbsDirectoryEnvironmentException;
	
}