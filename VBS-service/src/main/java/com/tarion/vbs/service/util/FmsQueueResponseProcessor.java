/* 
 * 
 * FmsQueueResponseProcessor.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.service.jaxb.fms.createcashsecurity.CreateCashSecurityResponse;
import com.tarion.vbs.service.jaxb.fms.drawdown.CreateDrawDownResponse;
import com.tarion.vbs.service.jaxb.fms.paymentresponse.AllocationRequest;
import com.tarion.vbs.service.jaxb.fms.paymentresponse.UnpostRequest;
import com.tarion.vbs.service.jaxb.fms.paymentresponse.WriteOff;
import com.tarion.vbs.service.jaxb.fms.release.ReplaceCheque;
import com.tarion.vbs.service.jaxb.fms.release.SecurityReleaseResponse;
import javax.ejb.Local;




/**
 * Processing Responses from FMS Queue MDB 
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @date 2018-11-20
 * @version 1.0
 */
@Local
public interface FmsQueueResponseProcessor {
	
	public void processReceivedFmsXml(String xmlResponse);
	public void process(CreateCashSecurityResponse response, String xmlResponse);	
	public void process(SecurityReleaseResponse response, String xmlResponse);

    public void process(UnpostRequest request, String xmlResponse);

    public void process(CreateDrawDownResponse response, String xmlResponse);
	public void process(AllocationRequest request, String xmlResponse);
	public void process(WriteOff response, String xmlResponse);
    public void process(ReplaceCheque request, String xmlResponse);
}
