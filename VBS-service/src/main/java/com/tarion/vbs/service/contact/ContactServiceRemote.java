package com.tarion.vbs.service.contact;

import com.tarion.vbs.common.contact.EscrowAgentSearchParamsDTO;
import com.tarion.vbs.common.contact.EscrowAgnetSearchResultWrapper;
import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.common.enums.CRMSearchTypeEnum;

import javax.ejb.Remote;
import java.util.List;

/**
 * @author <A href="shadi.kajevand@tarion.com">Shadi Kajevand</A>
 * @since 2018-01-11
 * @version 1.0
 */

@Remote
public interface ContactServiceRemote {

	List<ContactDTO> getAllEscrowAgentsLike(String nameOrId);

	List<ContactDTO> getAllPersonAndCompanyContacts(String idFrom, String idTo);

	List<ContactDTO> getAllPersonAndCompanyContactsLike(String idOrName, int maxResults);

	List<ContactDTO> getAllPersonAndCompanyContactsByreleaseId(Long securityId);

	List<ContactDTO> getEscrowAgentsById(String id);

	EscrowAgnetSearchResultWrapper escrowAgentSearch(EscrowAgentSearchParamsDTO escrowAgentSearchParamsDTO);

	List<ContactDTO> getAssistantsByEscrowAgentCrmContactId(String crmContactId);

	List<ContactDTO> getLawyersByEscrowAgentCrmContactId(String crmContactId);

    List<ContactDTO> findPersonOrCompanyInCrm(String name, CRMSearchTypeEnum type);

    ContactDTO findContactByCrmContactId(String crmContactId, String userName);
    
    ContactDTO createContactFromDTO(ContactDTO contactDTO, String userName);

    List<ContactDTO> getAllVbsForVbNumberLike(String vbNumberPrefix);
}
