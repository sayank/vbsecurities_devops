package com.tarion.vbs.service.financialinstitution;

import com.tarion.vbs.common.dto.financialinstitution.FIActivityDTO;
import com.tarion.vbs.dao.history.HistoryDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.List;

/**
 * Financial Institution Activities Service provides operations for Financial Institution Activities
 *
 * @author <A href="joni.paananen@tarion.com">Joni Paananen</A>
 */

@Stateless(mappedName = "ejb/FinancialInstitutionActivitiesService")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class FinancialInstitutionActivitiesServiceImpl implements FinancialInstitutionActivitiesService, FinancialInstitutionActivitiesServiceRemote {

    @EJB
    private HistoryDAO historyDAO;

    @Override
    public List<FIActivityDTO> getActivitiesForFinancialInstitution(Long financialInstitutionId) {
        return historyDAO.getFinancialInstitutionActivities(financialInstitutionId);
    }
}
