package com.tarion.vbs.service.release;

import com.tarion.vbs.orm.entity.autorelease.AutoReleaseEnrolmentEntity;

import javax.ejb.Local;

@Local
public interface AutoReleaseCriteriaService {


    void evaluateCriteria(AutoReleaseEnrolmentEntity autoReleaseEnrolment);

    boolean evaluateCriteria18(AutoReleaseEnrolmentEntity autoReleaseEnrolment);
}
