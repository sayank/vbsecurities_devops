/* 
 * 
 * CrmWSInvocationService.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;


import com.tarion.crm.vbsws.CreateWLRequest;
import com.tarion.crm.vbsws.CreateWLResponse;
import com.tarion.crm.vbsws.GetContactInfoResponse;
import com.tarion.crm.vbsws.GetSecHomeInfoResponse;
import com.tarion.vbs.common.enums.CRMSearchTypeEnum;

import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import createworklistrequest.CreateWorklistRequest;
import createworklistresponse.CreateWorklistResponse;

import javax.ejb.Local;
import java.util.List;

/**
 * Utility Bean to invoke CRM Web Service Client
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-01-31
 * @version 1.0
 */
@Local
public interface CrmWSInvocationService {

	// Enrolments operations
	GetSecHomeInfoResponse getEnrolments(List<String> enrolmentNumbers);
	GetSecHomeInfoResponse.SECHOMEINFO getEnrolment(String enrolmentNumber);
	
	// Contacts Operations
	GetContactInfoResponse getVbsContactInfo(String name);

	GetContactInfoResponse getVbsContactInfo(String name, CRMSearchTypeEnum type);

	com.tarion.crm.contactws.GetContactInfoResponse getContactInfoByCrmContactId(String crmContactId);
	CreateWorklistResponse createCrmWorklistItem(CreateWorklistRequest createWorklistRequest);
}
