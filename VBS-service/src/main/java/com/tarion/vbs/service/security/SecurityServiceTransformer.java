/* 
 * 
 * SecurityServiceTransformer.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.security;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.contact.EscrowAgentLawyerAssistantDTO;
import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.common.dto.financialinstitution.BranchDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionBranchCompositeDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionDTO;
import com.tarion.vbs.common.dto.security.SecurityDTO;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.contact.ContactEntity;
import com.tarion.vbs.orm.entity.financialinstitution.BranchEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import com.tarion.vbs.service.financialinstitution.FinancialInstitutionServiceTransformer;
import org.apache.commons.beanutils.BeanUtils;

import javax.interceptor.Interceptors;
import java.lang.reflect.InvocationTargetException;


/**
 * Transformer helper class used by Security Service to provide transformation between DTO and Entity
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-09
 * @version 1.0
 */
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class SecurityServiceTransformer {
	
	private SecurityServiceTransformer() {}

	public static SecurityEntity transformDTOToEntity(SecurityDTO dto, SecurityEntity entity) {
		try {
			BeanUtils.copyProperties(entity, dto);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new VbsRuntimeException(VbsConstants.FAILED_TO_COPY_PROPERTIES, e); 
		}
		
		return entity;
	}
	
	
	public static ContactDTO transformEntityToDTO(ContactEntity entity) {
		ContactDTO dto = new ContactDTO();
		try {
			BeanUtils.copyProperties(dto, entity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new VbsRuntimeException(VbsConstants.FAILED_TO_COPY_PROPERTIES, e); 
		}
		return dto;
	}

	public static SecurityDTO transformEntityToDTO(SecurityEntity entity) {
		SecurityDTO dto = new SecurityDTO();
		try {
			BeanUtils.copyProperties(dto, entity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new VbsRuntimeException(VbsConstants.FAILED_TO_COPY_PROPERTIES, e); 
		}
		
		dto.setFinancialInstitutionAndBranch(transformFinancialInstition(entity));
		dto.setEscrowAgentLawyerAssistant(new EscrowAgentLawyerAssistantDTO());

		dto.setPoolId(entity.getPool().getId());
		
		return dto;
	}

	public static FinancialInstitutionBranchCompositeDTO transformFinancialInstition(SecurityEntity security) {
		if (security != null) {
			BranchEntity branch = security.getBranch();
			if (branch != null) {
				FinancialInstitutionEntity financialInstitution = branch.getFinancialInstitution();
				
				BranchDTO branchDTO = FinancialInstitutionServiceTransformer.transformEntityToDTO(branch);
				branchDTO.setFinancialInstitutionId(financialInstitution.getId());
				
				FinancialInstitutionBranchCompositeDTO fullDTO = new FinancialInstitutionBranchCompositeDTO();
				FinancialInstitutionDTO financialInstitutionDTO = FinancialInstitutionServiceTransformer.transformEntityToDTO(financialInstitution);
				financialInstitutionDTO.setFinancialInstitutionTypeId(financialInstitution.getFinancialInstitutionType().getId());

				fullDTO.setBranch(branchDTO);
				fullDTO.setFinancialInstitution(financialInstitutionDTO);
				return fullDTO;
			}
		}
		return new FinancialInstitutionBranchCompositeDTO();
	}
	
}
