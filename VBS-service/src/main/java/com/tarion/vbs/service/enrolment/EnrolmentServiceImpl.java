 package com.tarion.vbs.service.enrolment;

 import com.tarion.crm.vbsws.GetSecHomeInfoResponse;
 import com.tarion.vbs.common.constants.VbsConstants;
 import com.tarion.vbs.common.dto.enrolment.EnrolmentActivityDTO;
 import com.tarion.vbs.common.dto.enrolment.EnrolmentDTO;
 import com.tarion.vbs.common.exceptions.VbsRuntimeException;
 import com.tarion.vbs.common.util.VbsUtil;
 import com.tarion.vbs.dao.history.HistoryDAO;
 import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
 import com.tarion.vbs.enrolment.EnrolmentDAO;
 import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
 import com.tarion.vbs.service.util.CrmWSInvocationService;

 import javax.ejb.EJB;
 import javax.ejb.Stateless;
 import javax.interceptor.Interceptors;
 import javax.persistence.Tuple;
 import java.util.ArrayList;
 import java.util.Collections;
 import java.util.List;
 import java.util.stream.Collectors;

@Stateless(mappedName = "ejb/EnrolmentService")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class EnrolmentServiceImpl implements EnrolmentService, EnrolmentServiceRemote{
	
	@EJB
	private EnrolmentDAO enrolmentDAO;
	@EJB
	private CrmWSInvocationService crmWSInvocationService;
	@EJB
	private HistoryDAO historyDAO;


	@Override
	public List<EnrolmentDTO> getAvailableEnrolmentListByPoolId(Long poolId, boolean onlyCondoConv, Long securityIdentityTypeId) {
		List<Tuple> enrolments = enrolmentDAO.getAvailableEnrolmentsByPoolId(poolId, onlyCondoConv, securityIdentityTypeId);
		return enrolments.stream().map(EnrolmentServiceTransformer::transformTupleToDTO).collect(Collectors.toList());
	}
	@Override
	public List<EnrolmentDTO> getCRMEnrolmentListByPoolId(Long poolId) {
		List<String> enrolments = enrolmentDAO.getEnrolmentNumbersInAPoolByPoolId(poolId);
		if(enrolments.isEmpty()) {
			return new ArrayList<>();
		}
		GetSecHomeInfoResponse crmResponse = crmWSInvocationService.getEnrolments(enrolments);
		return EnrolmentServiceTransformer.transformCrmDTOList(crmResponse);
	}
    @Override
    public EnrolmentDTO getCRMEnrolment(String enrolmentNumber) {
        GetSecHomeInfoResponse crmResponse = crmWSInvocationService.getEnrolments(Collections.singletonList(enrolmentNumber));
        if(crmResponse.getSECHOMEINFO().isEmpty()) {
            throw new VbsRuntimeException("Enrolment was not found in CRM, " + enrolmentNumber);
        }
        return EnrolmentServiceTransformer.transformCrmDTOList(crmResponse).get(0);
    }

	@Override
	public EnrolmentDTO getEnrolment(String enrolmentNumber, Long poolId, Long securityIdentityTypeId) {
		enrolmentNumber = VbsUtil.addHtoEnrolmentId(enrolmentNumber.toUpperCase());
		EnrolmentEntity entity = enrolmentDAO.getEnrolmentByEnrolmentNumberPoolId(enrolmentNumber, poolId, securityIdentityTypeId);
		if(entity == null ) {
			return null;
		}
		EnrolmentDTO dto = EnrolmentServiceTransformer.transformEntityToDTO(entity);
		dto.setMultiSecurity(enrolmentDAO.isMultiSecurity(dto.getEnrolmentNumber()));
		dto.setUnitType(entity.getUnitType());
		dto.setIsCE(!entity.getHomeCategory().getId().equals(VbsConstants.HOME_CATEGORY_FREEHOLD));
		dto.setCondoConversion(entity.isCondoConversion());
		return dto;
	}

	@Override
	public EnrolmentDTO getEnrolmentForSecurity(String enrolmentNumber, Long securityId) {
		enrolmentNumber = VbsUtil.addHtoEnrolmentId(enrolmentNumber.toUpperCase());
		EnrolmentEntity entity = enrolmentDAO.getEnrolmentByEnrolmentNumberSecurityId(enrolmentNumber, securityId);
		if(entity == null ) {
			return null;
		}

		return EnrolmentServiceTransformer.transformEntityToDTO(entity);
	}

	@Override
	public List<EnrolmentActivityDTO> getEnrolmentActivitiesForEnrolmentNumber(String enrolmentNumber){
		return historyDAO.getEnrolmentActivities(enrolmentNumber);
	}
	
}
