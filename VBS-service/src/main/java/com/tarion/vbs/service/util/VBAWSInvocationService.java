/* 
 * 
 * VBAWSInvocationService.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import java.util.List;

import javax.ejb.Local;

import com.tarion.vbapplications.service.application.VbNumberStatusIdDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;

@Local
public interface VBAWSInvocationService {

	List<VbNumberStatusIdDTO>  getApplicationStatusForVBs(List<String> vbNumbers) throws VbsCheckedException;
	public String  getVbApplicationAnalyst(String vbNumber) throws VbsCheckedException;

}
