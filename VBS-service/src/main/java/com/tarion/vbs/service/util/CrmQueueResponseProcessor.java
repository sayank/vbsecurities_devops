/* 
 * 
 * InternalQueueProcessor.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import javax.ejb.Local;

import com.tarion.vbs.service.jaxb.crm.autorelease.AutoReleaseEnrolmentsResponse;
import com.tarion.vbs.service.jaxb.crm.autorelease.AutoReleaseVBsResponse;
import com.tarion.vbs.service.jaxb.crm.delta.company.CompanyDelta;
import com.tarion.vbs.service.jaxb.crm.delta.enrolment.EnrolmentsDelta;
import com.tarion.vbs.service.jaxb.crm.delta.person.PersonDelta;
import com.tarion.vbs.service.jaxb.crm.financialinstitution.FinancialInstitutionUpdateResponse;
import com.tarion.vbs.service.jaxb.crm.warrantyservicesinput.WarrantyServicesInputResponse;
import com.tarion.vbs.service.jaxb.crm.warrantyservicesinput.WarrantyServicesRecommendationResponse;



/**
 * Processing Responses from Crm Update Queue MDB 
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @date 2018-12-12
 * @version 1.0
 */
@Local
public interface CrmQueueResponseProcessor {
	
	public void processReceivedCrmXml(String xmlResponse);
	public void process(EnrolmentsDelta response, String xmlResponse);
	public void process(CompanyDelta response, String xmlResponse);
	public void process(PersonDelta response, String xmlResponse);
	public void process(WarrantyServicesInputResponse response, String xmlResponse);
	public void process(AutoReleaseEnrolmentsResponse response, String xmlResponse);
	public void process(AutoReleaseVBsResponse response, String xmlResponse);
	public void process(WarrantyServicesRecommendationResponse response, String xmlResponse);
	public void process(FinancialInstitutionUpdateResponse response, String xmlResponse);
}
