package com.tarion.vbs.service.webservice;

import com.tarion.vbs.common.dto.tip.ActivitiesSecurityDTO;
import com.tarion.vbs.common.dto.tip.ActivityDTO;
import com.tarion.vbs.dao.history.HistoryDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.orm.entity.security.SecurityEntity;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.soap.SOAPException;
import java.util.List;

@Stateless
@WebService(serviceName = "ActivitiesWS")
@LocalBean
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class ActivitiesWS {

    @EJB
    private HistoryDAO historyDAO;
    @EJB
    private GenericDAO genericDAO;

    @WebMethod
    public List<ActivityDTO> getActivitiesForSecurityId(Long securityId){
        return historyDAO.getSecurityActivities(securityId);
    }

    @WebMethod
    public ActivitiesSecurityDTO getSecurityById(Long securityId) throws SOAPException {
        if(securityId == null){throw new SOAPException("No securityId passed.");}
        SecurityEntity security = genericDAO.findEntityById(SecurityEntity.class, securityId);
        if(security == null){throw new SOAPException("Security not found for id: " + securityId);}
        return new ActivitiesSecurityDTO(security.getId(), (security.getPrimaryVb() != null) ? security.getPrimaryVb().getCrmContactId() : "",
                (security.getPrimaryVb() != null) ? security.getPrimaryVb().getCompanyName() : "", (security.getAvailableAmount() != null) ? security.getAvailableAmount().doubleValue() : null);
    }
}
