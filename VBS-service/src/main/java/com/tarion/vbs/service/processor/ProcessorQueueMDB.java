/* 
 * 
 * ProcessorQueueMDB.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.processor;


import com.tarion.vbs.common.constants.JMSConstants;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.MessageDriven;
import javax.interceptor.Interceptors;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Listens for Inteenal JMS messages and triggers processing 
 * 
 */
@Resource(name=JMSConstants.VBS_XA_CF_JNDI, type=javax.jms.ConnectionFactory.class)
@MessageDriven(mappedName=JMSConstants.INTERNAL_QUEUE_NAME)
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class ProcessorQueueMDB implements MessageListener {

	@EJB
	private InternalQueueProcessor processor;

	@Override
	public void onMessage(Message inMessage) {
		String inCaseMsgXML = null;
		try {
			if (inMessage instanceof TextMessage) {
				// skip any ping messages
				String messageType = inMessage.getStringProperty(JMSConstants.MESSAGE_TYPE);
				if (JMSConstants.PING.equalsIgnoreCase(messageType)) {
					String destNodes = inMessage.getStringProperty(JMSConstants.DESTINATION_NODES);
					LoggerUtil.logInfo(ProcessorQueueMDB.class, "Ping received in internal queue for Destination Node[" + destNodes + "]");
					return;
				}

				TextMessage txtMsg = (TextMessage) inMessage;
				inCaseMsgXML = txtMsg.getText();
				if (inCaseMsgXML == null) {
					throw new VbsRuntimeException("Missing message content in JMS message");
				}
				processor.processReceivedXml(inCaseMsgXML);
			} else {
				String warnMsg = "Received JMS Message from CRM that is not Text Message: " + inMessage.getClass().getName();
				LoggerUtil.logWarn(ProcessorQueueMDB.class, warnMsg);
				throw new VbsRuntimeException(warnMsg);
			}
		} catch (JMSException e) {
			throw new VbsRuntimeException(e.getMessage());
		} catch (VbsRuntimeException pie) {
			StringBuilder sb = new StringBuilder("ProcessorQueueMDB error for: [");
			sb.append(inCaseMsgXML).append(']');
			LoggerUtil.logError(ProcessorQueueMDB.class, sb.toString(), pie);
			throw new EJBException(pie);
		}
	}

	
}
