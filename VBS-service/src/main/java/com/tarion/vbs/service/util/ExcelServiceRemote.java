package com.tarion.vbs.service.util;

import javax.ejb.Remote;

import com.tarion.vbs.common.dto.interest.SecurityInterestSearchParamsDTO;
import com.tarion.vbs.common.dto.release.ReleaseSearchParametersDTO;
import com.tarion.vbs.common.dto.security.DeletedSecuritySearchParametersDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchParametersDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchResultWrapper;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;

@Remote
public interface ExcelServiceRemote {

    byte[] createSearchResultsExcel(SecuritySearchParametersDTO params, boolean showInterest, UserDTO userDTO) throws VbsCheckedException;

    byte[] createSearchResultsExcel(SecuritySearchParametersDTO params, SecuritySearchResultWrapper results, boolean showInterest) throws VbsCheckedException;

    public byte[] getAutoReleasePassedResultsExcel(Long runId) throws VbsCheckedException;
	
	public byte[] getAutoReleaseFailedResultsExcel(Long runId) throws VbsCheckedException;
	
	public byte[] getSearchedReleasesExcel(ReleaseSearchParametersDTO releaseSearchParametersDTO) throws VbsCheckedException;

	public byte[] getAutoReleaseForSecurityIdExcel(Long securityId) throws VbsCheckedException;
	
	public byte[] getReleasesExcel(Long securityId, UserDTO userDTO) throws VbsCheckedException;

	public byte[] createInterestSearchResultsExcel(SecurityInterestSearchParamsDTO securityInterestSearchParams) throws VbsCheckedException;

	public byte[] createNoInterestSearchResultsExcel() throws VbsCheckedException;

	public byte[] searchDeletedSecurityExcel(DeletedSecuritySearchParametersDTO deletedSearchParametersDTO, UserDTO userDTO) throws VbsCheckedException;

}
