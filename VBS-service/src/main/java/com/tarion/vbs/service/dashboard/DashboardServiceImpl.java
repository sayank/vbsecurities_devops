package com.tarion.vbs.service.dashboard;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.New;
import javax.interceptor.Interceptors;

import com.tarion.vbs.common.dto.dashboard.ManagerReleaseDTO;
import com.tarion.vbs.common.dto.dashboard.NewBlanketCEDTO;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.dashboard.DashboardDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.enrolment.CEEnrolmentDecisionEntity;

/**
 * Dashboard service provides data for display on dashboard.
 *
 * @author <A href="sam.allen@tarion.com">Sam Allen</A>
 * @since 2019-01-10
 * @version 1.0
 */
@Stateless(mappedName = "ejb/DashboardService")
@LocalBean
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class DashboardServiceImpl implements DashboardServiceRemote {
	
	@EJB
	private DashboardDAO dashboardDAO;
	
	@Override
	public List<ManagerReleaseDTO> getReleasesForManagerUserId(String userId) {
		return dashboardDAO.getReleasesForManagerUserId(userId);
	}

	@Override
	public List<NewBlanketCEDTO> getNewBlanketCEs(){
		List<CEEnrolmentDecisionEntity> entityList = dashboardDAO.getOutstandingCEDecisions();
		return entityList.stream().map(this::transform).collect(Collectors.toList());
	}

	private NewBlanketCEDTO transform(CEEnrolmentDecisionEntity entity){
		NewBlanketCEDTO dto = new NewBlanketCEDTO();
		dto.setEnrolmentAddress(entity.getEnrolment().getAddress().concatAddress());
		dto.setEnrolmentNumber(Integer.parseInt(VbsUtil.removeHFromEnrolment(entity.getEnrolment().getEnrolmentNumber())));
		dto.setVbName(entity.getEnrolment().getEnrollingVb().getCompanyName());
		dto.setVbNumber(Integer.parseInt(VbsUtil.removeBFromVB(entity.getEnrolment().getEnrollingVb().getCrmContactId())));
		return dto;
	}
	
}
