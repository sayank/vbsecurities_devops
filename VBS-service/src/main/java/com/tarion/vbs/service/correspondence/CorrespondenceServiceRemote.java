package com.tarion.vbs.service.correspondence;

import java.util.List;

import javax.ejb.Remote;

import com.tarion.vbs.common.dto.NameDescriptionDTO;
import com.tarion.vbs.common.dto.correspondence.GenerateCorrespondenceDTO;
import com.tarion.vbs.common.enums.CorrespondenceActionEnum;
import com.tarion.vbs.orm.entity.release.ReleaseEntity;

/**
 * @author <A href="joni.paananen@tarion.com">Joni Paananen</A>
 * @since 2019-02-19
 * @version 1.0
 */

@Remote
public interface CorrespondenceServiceRemote {

    List<NameDescriptionDTO> getAllTemplates();
	GenerateCorrespondenceDTO generate(GenerateCorrespondenceDTO generateCorrespondenceDTO);

}
