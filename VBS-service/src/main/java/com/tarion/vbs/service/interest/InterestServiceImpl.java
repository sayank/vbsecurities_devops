/* 
 * 
 * SecurityServiceImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.interest;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.dto.interest.InterestCalcDTO;
import com.tarion.vbs.common.dto.interest.InterestRateDTO;
import com.tarion.vbs.common.dto.interest.InterestSearchResultDTO;
import com.tarion.vbs.common.dto.interest.SecurityInterestSearchParamsDTO;
import com.tarion.vbs.common.dto.security.CashSecurityNoInterestDTO;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.enums.InterestPeriodTypeEnum;
import com.tarion.vbs.common.enums.InterestStatusEnum;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.contact.ContactDAO;
import com.tarion.vbs.dao.interest.InterestDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.orm.entity.interest.InterestCalculationEntity;
import com.tarion.vbs.orm.entity.interest.InterestEntity;
import com.tarion.vbs.orm.entity.interest.InterestRateEntity;
import com.tarion.vbs.service.jaxb.fms.sendcashdailyinterest.DailyInterestRequest;
import com.tarion.vbs.service.jaxb.fms.sendcashdailyinterest.SecurityInterestDTO;
import com.tarion.vbs.service.processor.ProcessorTriggeringService;
import com.tarion.vbs.service.util.JmsMessageSenderService;
import com.tarion.vbs.service.util.JmsMessageTypeEnum;
import com.tarion.vbs.service.util.PropertyService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;
import javax.persistence.Tuple;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.WeekFields;
import java.util.*;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Security Service provides operations for Security and Vb 
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-09
 * @version 1.0
 */
@Stateless(mappedName = "ejb/InterestService")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class InterestServiceImpl implements InterestService, InterestServiceRemote {

	@EJB
	private GenericDAO genericDAO;
	@EJB
	private InterestDAO interestDAO;
	@EJB
	private JmsMessageSenderService jmsMessageSenderService;
	@EJB
	private PropertyService propertyService;
	@EJB
	private ContactDAO contactDAO;
	@EJB
	private ProcessorTriggeringService processorTriggeringService;

	private static final String INTEREST_START_DATE = "interestStartDate";
	private static final String ANNUAL_RATE = "annualRate";

	@Override
	public List<CashSecurityNoInterestDTO> getCashSecuritiesWithoutInterest() {
		List<CashSecurityNoInterestDTO> ret = new ArrayList<>();
		String daysInPastString = propertyService.getValue(VbsConstants.DAYS_IN_THE_PAST_TO_CALCULATE_INTEREST);
		int daysInPast = 30;
		try {
			daysInPast = Integer.parseInt(daysInPastString);
		} catch (NumberFormatException e) {
			// nothing keep default 30
		}

		List<Tuple> secDates = interestDAO.getCashSecuritiesWithoutInterest(daysInPast);
		List<Integer> securityIds = new ArrayList<>();
		Map<Integer, List<LocalDate>> dates = new HashMap<>();
		for(Tuple secDate : secDates){
			Integer securityId = secDate.get(0, Integer.class);
			if(dates.containsKey(securityId)){
				dates.get(securityId).add(secDate.get(1, Date.class).toLocalDate());
			}else{
				List<LocalDate> l = new ArrayList<>();
				l.add(secDate.get(1, Date.class).toLocalDate());
				dates.put(securityId, l);
			}
			if(!securityIds.contains(securityId)){securityIds.add(securityId);}
		}

		List<Tuple> infoList = interestDAO.getSecurityInfoForInterestReport(daysInPast);

		for(Map.Entry<Integer, List<LocalDate>> entry : dates.entrySet()){
			Integer securityId = entry.getKey();
			List<LocalDate> dateList = entry.getValue();
			List<String> outList = new ArrayList<>();
			LocalDate first = dateList.get(0);
			int count = 1;
			for(int i=1;i<dateList.size();i++){
				if(first.plusDays(i).compareTo(dateList.get(i)) != 0){
					if(i+1 == count){
						outList.add(DateUtil.getDateFormattedShort(first.atStartOfDay()));
					}else{
						outList.add(DateUtil.getDateFormattedShort(first.atStartOfDay()) + " - " + DateUtil.getDateFormattedShort(dateList.get(i).atStartOfDay()));
					}
					first = dateList.get(i);
				}

				if(i == dateList.size()-1){
					if(first.compareTo(dateList.get(i)) == 0){
						outList.add(DateUtil.getDateFormattedShort(first.atStartOfDay()));
					}else{
						outList.add(DateUtil.getDateFormattedShort(first.atStartOfDay()) + " - " + DateUtil.getDateFormattedShort(dateList.get(i).atStartOfDay()));
					}
				}
				count++;
			}

			Tuple info = infoList.stream().filter(i -> i.get("ID", Integer.class).compareTo(securityId) == 0).findFirst().orElseThrow(VbsRuntimeException::new);
			CashSecurityNoInterestDTO dto = new CashSecurityNoInterestDTO();
			dto.setCurrentAmount(info.get("CURRENT_AMOUNT", BigDecimal.class));
			dto.setInstrumentNumber(info.get("INSTRUMENT_NUMBER", String.class));
			dto.setIssuedDate(LocalDateTime.ofInstant(info.get("ISSUED_DATE", Timestamp.class).toInstant(), ZoneId.systemDefault()));
			LocalDate lastInterest = info.get("LAST_INTEREST_DATE", java.sql.Date.class) != null ? info.get("LAST_INTEREST_DATE", java.sql.Date.class).toLocalDate() : null;
			dto.setLastInterestCalculatedDate(lastInterest);
			dto.setOriginalAmount(info.get("ORIGINAL_AMOUNT", BigDecimal.class));
			dto.setReceivedDate(LocalDateTime.ofInstant(info.get("RECEIVED_DATE", Timestamp.class).toInstant(), ZoneId.systemDefault()));
			dto.setSecurityNumber(info.get("ID", Integer.class).longValue());
			dto.setVbName(info.get("COMPANY_NAME", String.class));
			dto.setVbNumber(info.get("CRM_CONTACT_ID", String.class));
			dto.setMissingInterestDates(String.join(", ", outList));
			ret.add(dto);
		}
		return ret;
	}

	private List<InterestRateDTO> transformListOfEntitiesToListOfDTOs(List<InterestRateEntity> rates) {
		List<InterestRateDTO> rateDTOs = new ArrayList<>();
		for (InterestRateEntity rate : rates) {
			InterestRateDTO rateDTO = InterestServiceTransformer.transformEntityToDTO(rate);
			rateDTOs.add(rateDTO);
		}
		return rateDTOs;
	}

    @Override
    public List<InterestRateDTO> saveInterestRate(InterestRateDTO interestRateDTO, UserDTO userDTO) {
        if(validateInterestRate(interestRateDTO).isEmpty()){
            InterestRateEntity entity = InterestServiceTransformer.transformDTOToEntity(interestRateDTO, new InterestRateEntity());
            if(interestRateDTO.getId() == null){
                genericDAO.addEntity(entity, userDTO.getUserId(), LocalDateTime.now());
            }else{
                InterestRateEntity current = genericDAO.findEntityById(InterestRateEntity.class, interestRateDTO.getId());
                if(!current.getInterestStartDate().equals(interestRateDTO.getInterestStartDate()) || !current.getAnnualRate().equals(interestRateDTO.getAnnualRate())){
                    genericDAO.updateEntity(entity, userDTO.getUserId(), LocalDateTime.now());
                }
            }
        }
		return getAllInterestRates();
	}

    @Override
	public List<ErrorDTO> validateInterestRate(InterestRateDTO dto){
		List<ErrorDTO> errors = new ArrayList<>();
		if(dto.getInterestStartDate() == null){
			errors.add(new ErrorDTO("Start Date is mandatory",INTEREST_START_DATE));
		}
		if(dto.getAnnualRate() == null){
			errors.add(new ErrorDTO("Interest Rate is mandatory",ANNUAL_RATE));
		}

		if(errors.isEmpty()){
			InterestRateEntity rateAtDate;
			try {
				LocalDate lastCalc = interestDAO.findLastCalculatedDailyInterest();
                rateAtDate = interestDAO.getApplicableInterestRateForDate(dto.getInterestStartDate());

                if(dto.getAnnualRate() == null || dto.getAnnualRate().compareTo(BigDecimal.ZERO) < 0){
                    //annual rate cannot be less than 0.
                    errors.add(new ErrorDTO("Annual Rate cannot be less than zero.", ANNUAL_RATE));
                }else if(!dto.getInterestStartDate().isAfter(lastCalc.atStartOfDay())){
                    //cant save or modify anything with a date before the last calc
                    errors.add(new ErrorDTO("Unable to update or create an Interest Rate with a Start Date before " + DateUtil.getDateFormattedShort(lastCalc.atStartOfDay()), INTEREST_START_DATE));
                }else if(dto.getInterestStartDate().equals(rateAtDate.getInterestStartDate()) && (dto.getId() == null || !dto.getId().equals(rateAtDate.getId()))){
                    //rate being saved cannot have same start date as an existing rate.
                    errors.add(new ErrorDTO("An existing Interest Rate already has this Start Date.", INTEREST_START_DATE));
                }else if(dto.getId() == null && rateAtDate.getAnnualRate().compareTo(dto.getAnnualRate()) == 0){
                    //adding a pointless rate, same rate already applies at the date given.
                    errors.add(new ErrorDTO("The given Annual Interest Rate already applies at the Start Date entered.", ANNUAL_RATE));
                }else if(dto.getId() != null){
                    //confirm not editing something which cannot be edited.
                    InterestRateEntity current = genericDAO.findEntityById(InterestRateEntity.class, dto.getId());
                    if(!current.getInterestStartDate().isAfter(lastCalc.atStartOfDay())){
                        errors.add(new ErrorDTO("Editing this Interest Rate is not allowed as it has been used for interest calculation."));
                    }
                }
			}catch(VbsCheckedException e){
				errors.add(new ErrorDTO("An error occurred while validating the Interest Rate."));
			}
		}

		return errors;
	}

	@Override
	public void deleteInterestRate(InterestRateDTO dto, UserDTO userDTO) throws VbsCheckedException {
		if(dto.getId() != null){
			InterestRateEntity current = genericDAO.findEntityById(InterestRateEntity.class, dto.getId());
			LocalDate lastCalc = interestDAO.findLastCalculatedDailyInterest();
			if(!current.getInterestStartDate().isAfter(lastCalc.atStartOfDay())){
				throw new VbsCheckedException("Attempted to delete an Interest Rate already used in calculation.");
			}
			genericDAO.removeEntity(current, userDTO.getUserId(), LocalDateTime.now());
		}
	}

	@Override
	public List<InterestRateDTO> getAllInterestRates() {
		List<InterestRateEntity> rates = interestDAO.getAllInterestRates();
		LocalDate lastCalculatedDate = interestDAO.findLastCalculatedDailyInterest();
		List<InterestRateDTO> rateDTOs = transformListOfEntitiesToListOfDTOs(rates);
		for (InterestRateDTO interestRateDTO : rateDTOs) {
			interestRateDTO.setCanEdit(lastCalculatedDate.atStartOfDay().isBefore(interestRateDTO.getInterestStartDate()));
		}
		return rateDTOs;
	}

	@Override
	public LocalDate getNextValidStartDate(){
		return interestDAO.findLastCalculatedDailyInterest().plusDays(1L);
	}

	@Override
	public void findDatesRequiringInterestCalculation(){
		this.findDatesRequiringInterestCalculation(Integer.parseInt(DAYS.between(YearMonth.from(LocalDate.now().minusMonths(1)).atDay(1), LocalDate.now()) + ""));
	}

	@Override
	public void findDatesRequiringInterestCalculation(int precedingDays) {
		List<LocalDate> dates = interestDAO.findDatesRequiringInterestRun(precedingDays);
		LocalDate earliestDate = LocalDate.parse(propertyService.getValue(VbsConstants.EARLIEST_DATE_TO_CALCULATE_INTEREST));
		dates.forEach(d -> {
			if(d.isAfter(earliestDate)){
				processorTriggeringService.triggerInterestCalculationForDate(d);
			}
		});
	}

	@Override
	public void performInterestCalcForDate(LocalDate date){
		List<Tuple> securityList = interestDAO.getSecurityInfoForInterestDate(date);
		if(securityList.isEmpty()){
			LoggerUtil.logError(InterestServiceImpl.class, "performInterestCalcForDate", "No missing interest calculations found for date: " + date.toString());
		}else{
			InterestCalculationEntity calc = interestDAO.createRunForDate(date);
			List<InterestCalcDTO> interestList = interestDAO.insertInterestRecords(calc, securityList);
			sendInterestToFms(calc, interestList);
			calc.setStatus(InterestStatusEnum.SENT);
			interestDAO.updateInterestCalculation(calc);
		}
	}

	@Override
	public List<com.tarion.vbs.common.dto.interest.SecurityInterestDTO> getDailyInterestsForSecurity(Long securityId){
		List<InterestEntity> interestList = interestDAO.getInterestsForSecurityId(securityId);
		return interestList.stream().map(this::transform).collect(Collectors.toList());
	}

	private com.tarion.vbs.common.dto.interest.SecurityInterestDTO transform(InterestEntity interestEntity){
		com.tarion.vbs.common.dto.interest.SecurityInterestDTO dto = new com.tarion.vbs.common.dto.interest.SecurityInterestDTO();
		dto.setDailyInterestAmount(interestEntity.getInterestAmount());
		dto.setInterestDate(interestEntity.getInterestCalculation().getInterestDate().atStartOfDay());
		dto.setInterestRateId(interestEntity.getInterestCalculation().getInterestRate().getId());
		dto.setInterestAnnualRate(interestEntity.getInterestCalculation().getInterestRate().getAnnualRate());
		dto.setSecurityId(interestEntity.getSecurityId());
		return dto;
	}

	@Override
	@ExcludeClassInterceptors
	public List<InterestSearchResultDTO> searchInterest(SecurityInterestSearchParamsDTO securityInterestSearchParams) {
		long start = LoggerUtil.logEnter(InterestServiceImpl.class, "searchInterest", securityInterestSearchParams);
		List<InterestSearchResultDTO> ret;
		List<Tuple> dailyInterests = interestDAO.findInterestsForSecurityBetweenDates(securityInterestSearchParams.getSecurityId(), securityInterestSearchParams.getFromDate().toLocalDate(), securityInterestSearchParams.getToDate().toLocalDate());
		if (securityInterestSearchParams.getPeriodType().equals(InterestPeriodTypeEnum.DAILY)) {
			ret = dailyInterests.stream().map(d -> transform(d, InterestPeriodTypeEnum.DAILY)).collect(Collectors.toList());
		} else {
			String interestDate = "interestDate";
			Map<LocalDateTime, InterestSearchResultDTO> combined = new HashMap<>();
			for (Tuple daily : dailyInterests) {
				LocalDateTime firstDayInPeriod = getFirstDayInPeriod(daily.get(interestDate, Date.class).toLocalDate().atStartOfDay(), securityInterestSearchParams.getPeriodType());
				InterestSearchResultDTO dto = combined.get(firstDayInPeriod);
				if(dto == null){
					dto = transform(daily, securityInterestSearchParams.getPeriodType());
				}else{
					if(daily.get(interestDate, Date.class).toLocalDate().atStartOfDay().isBefore(dto.getFromDate())){
						dto.setFromDate(daily.get(interestDate, Date.class).toLocalDate().atStartOfDay());
					}else if(daily.get(interestDate, Date.class).toLocalDate().atStartOfDay().isAfter(dto.getToDate())){
						dto.setToDate(daily.get(interestDate, Date.class).toLocalDate().atStartOfDay());
					}
					dto.setAccruedInterest(dto.getAccruedInterest().add(daily.get("interestAmount", BigDecimal.class)));
				}
				combined.put(firstDayInPeriod, dto);
			}
			ret = combined.values().stream().sorted(Comparator.comparing(InterestSearchResultDTO::getFromDate)).collect(Collectors.toList());
		}
		LoggerUtil.logExit(InterestServiceImpl.class, "searchInterest", start);
		return ret;
	}

	private InterestSearchResultDTO transform(Tuple entity, InterestPeriodTypeEnum periodType){
		InterestSearchResultDTO dto = new InterestSearchResultDTO();
		dto.setAccruedInterest(entity.get("interestAmount", BigDecimal.class));
		dto.setDiscountAmount(BigDecimal.ZERO);
		//dto.setDiscountAmount(entity.get("discountAmount", BigDecimal.class));
		dto.setPeriodType(periodType);
		LocalDate d = entity.get("interestDate", java.sql.Date.class).toLocalDate();
		dto.setFromDate(d.atStartOfDay());
		dto.setToDate(d.atStartOfDay());
		dto.setYear(d.getYear());
		return dto;
	}


	private LocalDateTime getFirstDayInPeriod(LocalDateTime date, InterestPeriodTypeEnum period){
		if(period.equals(InterestPeriodTypeEnum.WEEKLY)){
			return date.with(WeekFields.of(Locale.CANADA).dayOfWeek(), 1);
		}else if(period.equals(InterestPeriodTypeEnum.MONTHLY)){
			return date.with(TemporalAdjusters.firstDayOfMonth());
		}else if(period.equals(InterestPeriodTypeEnum.QUARTERLY)){
			return DateUtil.getLocalDateTimeStartOfQuarter(date);
		}else if(period.equals(InterestPeriodTypeEnum.YEARLY)){
			return date.with(TemporalAdjusters.firstDayOfYear());
		}
		return date;
	}

	private void sendInterestToFms(InterestCalculationEntity calc, List<InterestCalcDTO> interests) {
		DailyInterestRequest request = new DailyInterestRequest();
		request.setInterestDate(DateUtil.getDateFormattedForFms(calc.getInterestDate().atStartOfDay()));

		interests.forEach(i -> {
			SecurityInterestDTO d = new SecurityInterestDTO();
			d.setInterestAmount(VbsUtil.getStringForMoney(i.getInterestAmount()));
			d.setSecurityId(i.getSecurityId());
			d.setVbNumber(i.getVbNumber());
			d.setInstrumentNumber(i.getInstrumentNumber());
			request.getSecurityInterest().add(d);
		});

		String xml = VbsUtil.convertJaxbToXml(request, DailyInterestRequest.class);
		jmsMessageSenderService.postFmsRequestQueueInterestMessage(xml, DailyInterestRequest.class.getSimpleName());
		jmsMessageSenderService.insertInterestTransactionLog(JmsMessageTypeEnum.getFmsInterestRequestTracking(calc.getCalculationDate()), xml);
	}

}
