package com.tarion.vbs.service.release;

import com.tarion.vbs.common.dto.autorelease.AutoReleaseResultDTO;
import com.tarion.vbs.common.dto.release.AutoReleaseDTO;
import com.tarion.vbs.common.dto.autorelease.AutoReleaseResultWrapper;
import com.tarion.vbs.common.dto.autorelease.AutoReleaseRunSearchParametersDTO;
import com.tarion.vbs.common.dto.autorelease.AutoReleaseRunSearchResultDTO;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;

import javax.ejb.Remote;
import java.math.BigDecimal;
import java.util.List;

@Remote
public interface AutoReleaseServiceRemote {

	boolean existsRunInProgress();

	void startAutoReleaseProcess(UserDTO user) throws VbsCheckedException;

    void getApplicationStatusesFromVBA(Long autoReleaseRunId, List<String> vbNumbers) throws VbsCheckedException;

    void checkDataReceived(Long autoReleaseRunId);

    void evaluateCriteria(Long autoReleaseEnrolmentId);

    void checkCriteriaComplete(Long autoReleaseRunId);

    List<AutoReleaseRunSearchResultDTO> searchAutoReleaseRuns(AutoReleaseRunSearchParametersDTO params);

    AutoReleaseResultWrapper getAutoReleaseResults(Long runId) throws VbsCheckedException;

    void finalizeAutoRelease(Long autoReleaseRunId, String userId);

    void processCreateRelease(Long autoReleaseEnrolmentId, boolean fullRelease, BigDecimal releaseAmount, String userId, int numberOfReleases);

    void checkCreateReleasesComplete(Long runId, int numberOfReleases, String userId);

    void saveAutoReleaseEnrolments(Long autoReleaseRunId, List<AutoReleaseResultDTO> autoReleaseEnrolments, UserDTO userDto) throws VbsCheckedException;

    void triggerFinalizeAutoRelease(Long autoReleaseRunId, List<AutoReleaseResultDTO> autoReleaseEnrolments, UserDTO userDto) throws VbsCheckedException, VbsRuntimeException;

    List<AutoReleaseDTO> getAutoReleaseForSecurityId(Long securityId);
}
