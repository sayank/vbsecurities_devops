package com.tarion.vbs.service.contact;

import com.tarion.crm.contactws.GetContactInfoResponse;
import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.contact.EscrowAgentLawyerAssistantDTO;
import com.tarion.vbs.common.contact.EscrowAgentSearchParamsDTO;
import com.tarion.vbs.common.contact.EscrowAgnetSearchResultWrapper;
import com.tarion.vbs.common.dto.AddressDTO;
import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.common.enums.CRMSearchTypeEnum;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.contact.ContactDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.orm.entity.AddressEntity;
import com.tarion.vbs.orm.entity.contact.ContactEntity;
import com.tarion.vbs.orm.entity.contact.ContactTypeEntity;
import com.tarion.vbs.service.util.CrmWSInvocationService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
 * Contact Service provides operations for Contacts 
 *
 * @author <A href="shadi.kajevand@tarion.com">Shadi Kajevand</A>
 * @since 2018-01-11
 * @version 1.0
 */
@Stateless(mappedName = "ejb/ContactService")
@Interceptors(EjbLoggingInterceptor.class)
public class ContactServiceImpl implements ContactService, ContactServiceRemote{
	@EJB
	private ContactDAO contactDAO;
	@EJB
	private GenericDAO genericDAO;
	@EJB
	private CrmWSInvocationService crmWSInvocationService;

	@Override
	public List<ContactDTO> getAllEscrowAgentsLike(String escrowAgentName) {
		try{
			Long crmContactId = Long.parseLong(escrowAgentName.trim());
			return contactDAO.getEscrowAgentsCrmContactIdBeginsWith(crmContactId.toString()).stream().map(ContactServiceTransformer::transformEntityToDTO).collect(Collectors.toList());
		}catch(NumberFormatException nfe){
			return contactDAO.getEscrowAgentsLike(escrowAgentName).stream().map(ContactServiceTransformer::transformEntityToDTO).collect(Collectors.toList());
		}
	}

	@Override
	public List<ContactDTO> getLawyersByEscrowAgentCrmContactId(String crmContactId) {
		return contactDAO.getLawyersForEscrowAgent(crmContactId).stream().map(ContactServiceTransformer::transformEntityToDTO).collect(Collectors.toList());
	}

	@Override
	public List<ContactDTO> getAssistantsByEscrowAgentCrmContactId(String crmContactId) {
		return contactDAO.getAssistantsForEscrowAgent(crmContactId).stream().map(ContactServiceTransformer::transformEntityToDTO).collect(Collectors.toList());
	}

	//This is for the popup where you click in the magnifying glass. Look financial institution how it is implemented there. If all values are null return all escrow agents. CodeType and NameType is wheather is beween <, > etc.
	@Override
	public EscrowAgnetSearchResultWrapper escrowAgentSearch(EscrowAgentSearchParamsDTO escrowAgentSearchParamsDTO){
		return 	contactDAO.searchEscrowAgent(escrowAgentSearchParamsDTO);
	}
	
	@Override
	public List<ContactDTO> getAllPersonAndCompanyContacts(String idFrom, String idTo) {
		// TODO Auto-generated method stub
		return null;
	}
	
	//typeAhead
	@Override
	public List<ContactDTO> getAllPersonAndCompanyContactsLike(String idOrName, int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}
	
	//Used for adding third party to release
	@Override
	public List<ContactDTO> getAllPersonAndCompanyContactsByreleaseId(Long releaseId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ContactDTO> getEscrowAgentsById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ContactDTO> findPersonOrCompanyInCrm(String name, CRMSearchTypeEnum type) {
		return transform(crmWSInvocationService.getVbsContactInfo(name, type));
	}
	
	
    @Override
    public ContactDTO findContactByCrmContactId(String crmContactId, String userName) {
		ContactEntity entity = contactDAO.getContactByCrmContactId(crmContactId);
		if(entity != null) {
			return ContactServiceTransformer.transformEntityToDTO(entity);
		}else {
			List<ContactDTO> contacts = transform(crmWSInvocationService.getContactInfoByCrmContactId(crmContactId));
			if (contacts.size() > 1) {
				throw new VbsRuntimeException("Got more than one result for a given CRM Contact Id: " + crmContactId);
			}
			if (userName == null) {
				return contacts.get(0);
			}
			else {
				return createContactFromDTO(contacts.get(0), userName);
			}
		}
    }

    private List<ContactDTO> transform(GetContactInfoResponse response){
		List<ContactDTO> contacts = new ArrayList<>();
		for (GetContactInfoResponse.CONTACTINFO crmContact : response.getCONTACTINFO()) {
			ContactDTO contact = new ContactDTO();
			contact.setAddress(new AddressDTO());
			contact.setMailingAddress(new AddressDTO());
			GetContactInfoResponse.CONTACTINFO.CONTACTOTHERADDR crmOtherAddress = crmContact.getCONTACTOTHERADDR();
			if(crmOtherAddress != null) {
				contact.getAddress().setAddressConcat(crmOtherAddress.getCONCAT());
				contact.getAddress().setAddressLine1(crmOtherAddress.getADDRESS1());
				contact.getAddress().setAddressLine2(crmOtherAddress.getADDRESS2());
				contact.getAddress().setAddressLine3(crmOtherAddress.getADDRESS3());
				contact.getAddress().setAddressLine4(crmOtherAddress.getADDRESS4());
				contact.getAddress().setCity(crmOtherAddress.getCITY());
				contact.getAddress().setCountry(crmOtherAddress.getCOUNTRY());
				contact.getAddress().setCreateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmOtherAddress.getCREATEDDATE()));
				contact.getAddress().setCreateUser(crmOtherAddress.getCREATEDBY());
				contact.getAddress().setProvince(crmOtherAddress.getSTATE());
				contact.getAddress().setPostalCode(crmOtherAddress.getPOSTAL());
				contact.getAddress().setUpdateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmOtherAddress.getUPDATEDDATE()));
				contact.getAddress().setUpdateUser(crmOtherAddress.getUPDATEDBY());
			}

			GetContactInfoResponse.CONTACTINFO.CONTACTMAILINGADDR crmMailingAddress = crmContact.getCONTACTMAILINGADDR();
			if (crmMailingAddress != null && !crmMailingAddress.getADDRESS1().trim().isEmpty()) {
				contact.getMailingAddress().setAddressConcat(crmMailingAddress.getCONCAT());
				contact.getMailingAddress().setAddressLine1(crmMailingAddress.getADDRESS1());
				contact.getMailingAddress().setAddressLine2(crmMailingAddress.getADDRESS2());
				contact.getMailingAddress().setAddressLine3(crmMailingAddress.getADDRESS3());
				contact.getMailingAddress().setAddressLine4(crmMailingAddress.getADDRESS4());
				contact.getMailingAddress().setCity(crmMailingAddress.getCITY());
				contact.getMailingAddress().setCountry(crmMailingAddress.getCOUNTRY());
				contact.getMailingAddress().setCreateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmMailingAddress.getCREATEDDATE()));
				contact.getMailingAddress().setCreateUser(crmMailingAddress.getCREATEDBY());
				contact.getMailingAddress().setProvince(crmMailingAddress.getSTATE());
				contact.getMailingAddress().setPostalCode(crmMailingAddress.getPOSTAL());
				contact.getMailingAddress().setUpdateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmMailingAddress.getUPDATEDDATE()));
				contact.getMailingAddress().setUpdateUser(crmMailingAddress.getUPDATEDBY());
			}else{
				contact.setMailingAddress(contact.getAddress());
			}
			contact.setCompanyName(crmContact.getCOMPANYNAME());
			contact.setFirstName(crmContact.getFIRSTNAME());
			contact.setLastName(crmContact.getLASTNAME());
			contact.setUpdateUser(crmContact.getUPDATEDBY());
			contact.setUpdateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmContact.getUPDATEDDATE()));
			contact.setEmailAddress(crmContact.getEMAILADDR());
			contact.setCreateUser(crmContact.getCREATEDBY());
			contact.setCreateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmContact.getCREATEDDATE()));
			contact.setCrmContactId(crmContact.getCONTACTID());
			contact.setApVendorFlag(VbsUtil.getBooleanPrimitiveFromString(crmContact.getAPVENDORFLAG()));
			if (crmContact.getCONTACTPHONEPRIMARY() != null) {
				contact.setPhoneNumber(crmContact.getCONTACTPHONEPRIMARY().getPHONE());
				contact.setPhoneNumberExt(crmContact.getCONTACTPHONEPRIMARY().getEXTENSION());
			}
			contacts.add(contact);
		}
		return contacts;
	}

	private List<ContactDTO> transform(com.tarion.crm.vbsws.GetContactInfoResponse response){
		List<ContactDTO> contacts = new ArrayList<>();
		for (com.tarion.crm.vbsws.GetContactInfoResponse.CONTACTINFO crmContact : response.getCONTACTINFO()) {
			ContactDTO contact = new ContactDTO();
			contact.setAddress(new AddressDTO());
			contact.setMailingAddress(new AddressDTO());
			com.tarion.crm.vbsws.GetContactInfoResponse.CONTACTINFO.CONTACTOTHERADDR crmOtherAddress = crmContact.getCONTACTOTHERADDR();
			if(crmOtherAddress != null) {
				contact.getAddress().setAddressConcat(crmOtherAddress.getCONCAT());
				contact.getAddress().setAddressLine1(crmOtherAddress.getADDRESS1());
				contact.getAddress().setAddressLine2(crmOtherAddress.getADDRESS2());
				contact.getAddress().setAddressLine3(crmOtherAddress.getADDRESS3());
				contact.getAddress().setAddressLine4(crmOtherAddress.getADDRESS4());
				contact.getAddress().setCity(crmOtherAddress.getCITY());
				contact.getAddress().setCountry(crmOtherAddress.getCOUNTRY());
				contact.getAddress().setCreateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmOtherAddress.getCREATEDDATE()));
				contact.getAddress().setCreateUser(crmOtherAddress.getCREATEDBY());
				contact.getAddress().setProvince(crmOtherAddress.getSTATE());
				contact.getAddress().setPostalCode(crmOtherAddress.getPOSTAL());
				contact.getAddress().setUpdateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmOtherAddress.getUPDATEDDATE()));
				contact.getAddress().setUpdateUser(crmOtherAddress.getUPDATEDBY());
			}

			com.tarion.crm.vbsws.GetContactInfoResponse.CONTACTINFO.CONTACTMAILINGADDR crmMailingAddress = crmContact.getCONTACTMAILINGADDR();
			if (crmMailingAddress != null && !crmMailingAddress.getADDRESS1().trim().isEmpty()) {
				contact.getMailingAddress().setAddressConcat(crmMailingAddress.getCONCAT());
				contact.getMailingAddress().setAddressLine1(crmMailingAddress.getADDRESS1());
				contact.getMailingAddress().setAddressLine2(crmMailingAddress.getADDRESS2());
				contact.getMailingAddress().setAddressLine3(crmMailingAddress.getADDRESS3());
				contact.getMailingAddress().setAddressLine4(crmMailingAddress.getADDRESS4());
				contact.getMailingAddress().setCity(crmMailingAddress.getCITY());
				contact.getMailingAddress().setCountry(crmMailingAddress.getCOUNTRY());
				contact.getMailingAddress().setCreateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmMailingAddress.getCREATEDDATE()));
				contact.getMailingAddress().setCreateUser(crmMailingAddress.getCREATEDBY());
				contact.getMailingAddress().setProvince(crmMailingAddress.getSTATE());
				contact.getMailingAddress().setPostalCode(crmMailingAddress.getPOSTAL());
				contact.getMailingAddress().setUpdateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmMailingAddress.getUPDATEDDATE()));
				contact.getMailingAddress().setUpdateUser(crmMailingAddress.getUPDATEDBY());
			}else{
				contact.setMailingAddress(contact.getAddress());
			}
			contact.setCompanyName(crmContact.getCOMPANYNAME());
			contact.setFirstName(crmContact.getFIRSTNAME());
			contact.setLastName(crmContact.getLASTNAME());
			contact.setUpdateUser(crmContact.getUPDATEDBY());
			contact.setUpdateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmContact.getUPDATEDDATE()));
			contact.setEmailAddress(crmContact.getEMAILADDR());
			contact.setCreateUser(crmContact.getCREATEDBY());
			contact.setCreateDate(DateUtil.getLocalDateTimeFromCrmDateString(crmContact.getCREATEDDATE()));
			contact.setCrmContactId(crmContact.getCONTACTID());
			contact.setApVendorFlag(VbsUtil.getBooleanPrimitiveFromString(crmContact.getAPVENDORFLAG()));
			if (crmContact.getCONTACTPHONEPRIMARY() != null) {
				contact.setPhoneNumber(crmContact.getCONTACTPHONEPRIMARY().getPHONE());
				contact.setPhoneNumberExt(crmContact.getCONTACTPHONEPRIMARY().getEXTENSION());
			}
			contacts.add(contact);
		}
		return contacts;
	}



	@Override
	public EscrowAgentLawyerAssistantDTO getEscrowAgentLawyerAssistantDTOForSecurityId(Long securityId){
		EscrowAgentLawyerAssistantDTO dto = new EscrowAgentLawyerAssistantDTO();
		dto.setEscrowAgent(ContactServiceTransformer.transformEntityToDTO(contactDAO.getContactBySecurityIdContactType(securityId, VbsConstants.CONTACT_TYPE_ESCROW_AGENT)));
		dto.setAssistant(ContactServiceTransformer.transformEntityToDTO(contactDAO.getContactBySecurityIdContactSecurityRoleId(securityId, VbsConstants.CONTACT_SECURITY_ASSISTANT)));
		dto.setLawyer(ContactServiceTransformer.transformEntityToDTO(contactDAO.getContactBySecurityIdContactSecurityRoleId(securityId, VbsConstants.CONTACT_SECURITY_LAWYER)));
		return dto;
	}

    @Override
    public ContactDTO getThirdPartyContactForSecurityId(Long securityId) {
        return ContactServiceTransformer.transformEntityToDTO(contactDAO.getContactBySecurityIdContactSecurityRoleId(securityId, VbsConstants.CONTACT_SECURITY_THIRD_PARTY));
    }
    
    @Override
	public ContactDTO createContactFromDTO(ContactDTO contactDTO, String userName) {
		ContactEntity contactEntity = new ContactEntity();
		
		ContactServiceTransformer.transformDTOToEntity(contactDTO, contactEntity);
		
		AddressEntity address = ContactServiceTransformer.transformDTOToEntity(contactDTO.getAddress(), contactEntity.getAddress());
		AddressEntity mailingAddress = ContactServiceTransformer.transformDTOToEntity(contactDTO.getMailingAddress(), contactEntity.getMailingAddress());
		
		if (address != null) {
			address = (AddressEntity) genericDAO.addEntity(address, userName, DateUtil.getLocalDateTimeNow());
		}
		if (mailingAddress != null) {
			mailingAddress = (AddressEntity) genericDAO.addEntity(mailingAddress, userName, DateUtil.getLocalDateTimeNow());
		}
		if(contactDTO.getFirstName() != null || contactDTO.getLastName() != null) {
			ContactTypeEntity contactType = genericDAO.findEntityById(ContactTypeEntity.class, VbsConstants.CONTACT_TYPE_PERSON);
			contactEntity.setContactType(contactType);
		}
		
		if(contactDTO.getCompanyName() != null) {
			ContactTypeEntity contactType = genericDAO.findEntityById(ContactTypeEntity.class, VbsConstants.CONTACT_TYPE_COMPANY);
			contactEntity.setContactType(contactType);
		}
		contactEntity.setAddress(address);
		contactEntity.setMailingAddress(mailingAddress);
		contactEntity = (ContactEntity) genericDAO.addEntity(contactEntity, userName, DateUtil.getLocalDateTimeNow());
		return ContactServiceTransformer.transformEntityToDTO(contactEntity);
		
    }

	@Override
	public List<ContactDTO> getAllVbsForVbNumberLike(String vbNumberPrefix) {
		List<ContactEntity> vbs = contactDAO.getAllVbsForVbNumberLike(VbsUtil.addBToVb(vbNumberPrefix));
		List<ContactDTO> vbDTOs = new ArrayList<>();
		for (ContactEntity vb : vbs) {
			ContactDTO vbDTO = ContactServiceTransformer.transformEntityToDTO(vb);
			vbDTOs.add(vbDTO);
		}
		return vbDTOs;
	}
    
}
