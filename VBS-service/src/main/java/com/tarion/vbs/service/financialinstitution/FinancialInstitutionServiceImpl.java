/* 
 * 
 * SecurityServiceImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.financialinstitution;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import com.tarion.vbs.common.dto.security.SecurityDTO;
import org.apache.commons.lang3.StringUtils;

import com.tarion.vbs.common.constants.JMSConstants;
import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.dto.financialinstitution.BranchDTO;
import com.tarion.vbs.common.dto.financialinstitution.ContactFinancialInstitutionDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionAlertDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionAmountsDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionBranchCompositeDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionMaaPoaDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionMaaPoaInstitutionDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionSigningOfficerDTO;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.contact.ContactDAO;
import com.tarion.vbs.dao.financialinstitution.FinancialInstitutionDAO;
import com.tarion.vbs.dao.security.SecurityDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.orm.entity.AddressEntity;
import com.tarion.vbs.orm.entity.contact.ContactEntity;
import com.tarion.vbs.orm.entity.contact.ContactTypeEntity;
import com.tarion.vbs.orm.entity.financialinstitution.BranchEntity;
import com.tarion.vbs.orm.entity.financialinstitution.ContactFinancialInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionAlertEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionMaaPoaEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionMaaPoaInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionSigningOfficerEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionTypeEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import com.tarion.vbs.service.contact.ContactServiceTransformer;
import com.tarion.vbs.service.jaxb.crm.financialinstitution.FinancialInstitutionUpdate;
import com.tarion.vbs.service.security.SecurityServiceTransformer;
import com.tarion.vbs.service.util.JmsMessageSenderService;
import com.tarion.vbs.service.util.JmsMessageTypeEnum;
import com.tarion.vbs.service.util.PropertyService;

/**
 * Security Service provides operations for Security and Vb
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-09
 * @version 1.0
 */
@Stateless(mappedName = "ejb/FinancialInstitutionService")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class FinancialInstitutionServiceImpl implements FinancialInstitutionServiceRemote, FinancialInstitutionService {

	@EJB
	private PropertyService propertyService;
	@EJB
	private FinancialInstitutionDAO financialInstitutionDAO;
	@EJB
	private GenericDAO genericDAO;
	@EJB
	private JmsMessageSenderService jmsMessageSenderService;
	@EJB
    private ContactDAO contactDAO;
	@EJB
	private SecurityDAO securityDAO;

	private static final String NAME = "name";
	private static final String EXPIRATION_DATE = "expiryDate";
	private static final String CRM_CONTACT_ID_DOT = "contacts.";
	private static final String DOT_START_DATE = ".startDate";
	private static final String FinancialInstitutionMaaPoaInstitutionDTO = "FinancialInstitutionMaaPoaInstitutionDTO";
	private static final String MAAPOAS_DOT = "maaPoas.";
	private static final String DOT_ID = ".id";
	private static final String DOT_SIGNING_OFFICERS_DOT = ".signingOfficers.";
	private static final String DOT_COMPANY_NAME = ".companyName";
	private static final String DOT_CONTACT_NAME = ".contactName";
	private static final String DOT_MAAPOA_INSTITUTIONS_DOT = ".maaPoaInstitutions.";
	private static final String DOT_BRANCH = ".branch";
	private static final String DOT_PERCENTAGE = ".percentage";

	@Override
	public List<FinancialInstitutionDTO> getAllFinancialInstitutions() {
		List<FinancialInstitutionEntity> entities = financialInstitutionDAO.getAllFinancialInstitutions();
		List<FinancialInstitutionDTO> dtos = new ArrayList<>();
		for (FinancialInstitutionEntity entity : entities) {
			FinancialInstitutionDTO dto = FinancialInstitutionServiceTransformer.transformEntityToDTO(entity);
			dto.setFinancialInstitutionTypeId(entity.getFinancialInstitutionType().getId());
			dtos.add(dto);
		}
		return dtos;
	}
	
	@Override
	public List<BranchDTO> getAllBranches() {
		List<BranchEntity> entities = financialInstitutionDAO.getAllBranches();
		List<BranchDTO> dtos = new ArrayList<>();
		for (BranchEntity entity : entities) {
			BranchDTO dto = FinancialInstitutionServiceTransformer.transformEntityToDTO(entity);
			dto.setFinancialInstitutionId(entity.getFinancialInstitution().getId());
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public List<FinancialInstitutionDTO> getFinancialInstitutionsForNameOrIdLike(String nameOrId, int maxResults) {
		List<FinancialInstitutionDTO> dtos = new ArrayList<>();
		List<FinancialInstitutionEntity> entities;
		try {
			Long.parseLong(nameOrId);
			entities = financialInstitutionDAO.getFinancialInstitutionsForIdLike(nameOrId, maxResults);
		}catch(NumberFormatException e) {
			entities = financialInstitutionDAO.getFinancialInstitutionsForNameLike(VbsUtil.toUpperCase(nameOrId), maxResults);
		}
		
		for (FinancialInstitutionEntity entity : entities) {
			FinancialInstitutionDTO dto = FinancialInstitutionServiceTransformer.transformEntityToDTO(entity);
			dto.setFinancialInstitutionTypeId(entity.getFinancialInstitutionType().getId());
			dtos.add(dto);
		}
		return dtos;
	}
	
	

	@Override
	public List<BranchDTO> getBranchesForFinancialInstitution(Long financialInstitutionId) {
		List<BranchEntity> branches = financialInstitutionDAO.getBranchesForFinancialInstitution(financialInstitutionId);
		return branches.stream().map(FinancialInstitutionServiceTransformer::transformEntityToDTO).collect(Collectors.toList());
	}

	@Override
	public FinancialInstitutionBranchCompositeDTO getFullFinancialInstitutionForSecurity(Long securityId) {
		SecurityEntity security = genericDAO.findEntityById(SecurityEntity.class, securityId);
		return SecurityServiceTransformer.transformFinancialInstition(security);
	}
	
	@Override
	public List<FinancialInstitutionDTO> getFinancialInstitutionsByCodeOrName(String code, Long codeTo, String codeType, String name, String nameTo, String nameType, Long financialInstitutionTypeId){
		List<FinancialInstitutionDTO> dtos = new ArrayList<>();
		List<FinancialInstitutionEntity> entities = financialInstitutionDAO.getFinancialInstitutionsByCodeOrName(code, codeTo, codeType, name, nameTo, nameType, financialInstitutionTypeId);
		dtos.addAll(entities.stream().map(FinancialInstitutionServiceTransformer::transformEntityToDTO).collect(Collectors.toList()));
		return dtos;
	}

	@Override
	public FinancialInstitutionDTO getFinancialInstitution(Long financialInstitutionId) {
		FinancialInstitutionEntity entity = genericDAO.findEntityById(FinancialInstitutionEntity.class, financialInstitutionId);
		FinancialInstitutionDTO dto = null;
		if (entity != null) {
			dto = FinancialInstitutionServiceTransformer.transformEntityToDTO(entity);

			FinancialInstitutionAmountsDTO amounts = getAmounts(financialInstitutionId, false);
			dto.setOriginalAmountOnInstitution(amounts.getTotalOriginalAmount());
			dto.setCurrentAmountOnInstitution(amounts.getTotalCurrentAmount());
			if(entity.getMaxSecurityAmount() != null){
				dto.setAvailableAmountFi(entity.getMaxSecurityAmount().subtract(amounts.getTotalCurrentAmount()));
			}

			List<ContactFinancialInstitutionDTO> contacts = getContactsForFinancialInstitution(entity.getId());
			dto.setContacts(contacts);
			List<FinancialInstitutionMaaPoaDTO> maaPoas = getFinancialInstitutionMaaPoas(entity.getId());
			dto.setMaaPoas(maaPoas);
			List<FinancialInstitutionAlertDTO> alerts = getFinancialInstitutionAlerts(entity.getId());
			dto.setAlerts(alerts);
			
			// calculation of MAX amounts for type Administrative Agent
			/* TODO to be removed later when confirmed with user
			if (entity.getMaxSecurityAmount() != null && entity.getWnSecurityMaxAmount() != null && entity.getFinancialInstitutionType() != null && VbsConstants.FINANCIAL_INSTITUTION_TYPE_ADMINISTRATIVE_AGENT == entity.getFinancialInstitutionType().getId()) {
				dto = calculateAdminAgentMaxAmounts(dto);
			}
			*/
		}
		return dto;
	}

	@Override
	public FinancialInstitutionAmountsDTO getAmounts(Long financialInstitutionId, boolean includePEFDTA) {
		FinancialInstitutionAmountsDTO results = new FinancialInstitutionAmountsDTO();
		results.setTotalCurrentAmount(BigDecimal.ZERO);
		results.setTotalOriginalAmount(BigDecimal.ZERO);
		FinancialInstitutionAmountsDTO directAmounts = financialInstitutionDAO.getDirectFinancialInstitutionAmounts(financialInstitutionId, includePEFDTA);
		if (directAmounts != null) {
			if (directAmounts.getTotalOriginalAmount() != null) {
				results.setTotalOriginalAmount(results.getTotalOriginalAmount().add(directAmounts.getTotalOriginalAmount()));
			}
			if (directAmounts.getTotalCurrentAmount() != null) {
				results.setTotalCurrentAmount(results.getTotalCurrentAmount().add(directAmounts.getTotalCurrentAmount()));
			}
		}

		FinancialInstitutionAmountsDTO maaPoaAmounts = financialInstitutionDAO.getMaaPoaFinancialInstitutionAmounts(financialInstitutionId, includePEFDTA);
		if (maaPoaAmounts != null) {
			if (maaPoaAmounts.getTotalOriginalAmount() != null) {
				results.setTotalOriginalAmount(results.getTotalOriginalAmount().add(maaPoaAmounts.getTotalOriginalAmount()));
			}
			if (maaPoaAmounts.getTotalCurrentAmount() != null) {
				results.setTotalCurrentAmount(results.getTotalCurrentAmount().add(maaPoaAmounts.getTotalCurrentAmount()));
			}
		}
		return results;
	}

	// TODO to be removed later when confirmed with user
	private FinancialInstitutionDTO calculateAdminAgentMaxAmounts(FinancialInstitutionDTO dto) {
		List<FinancialInstitutionMaaPoaDTO> maaPoas = dto.getMaaPoas();
		BigDecimal totalMaxSecurityAmount = BigDecimal.ZERO;
		BigDecimal totalWarningMaxSecurityAmount = BigDecimal.ZERO;
		if (maaPoas != null) {
			for (FinancialInstitutionMaaPoaDTO maaPoa : maaPoas) {
				if (maaPoa.getMaaPoaInstitutions() != null) {
					for (FinancialInstitutionMaaPoaInstitutionDTO maaPoaInstitution : maaPoa.getMaaPoaInstitutions()) {
						if (maaPoaInstitution.getFinancialInstitution() != null && maaPoaInstitution.getPercentage() != null) {
							if (maaPoaInstitution.getFinancialInstitution().getMaxSecurityAmount() != null) {
								totalMaxSecurityAmount = totalMaxSecurityAmount.add(maaPoaInstitution.getFinancialInstitution().getMaxSecurityAmount().multiply(maaPoaInstitution.getPercentage().divide(VbsConstants.BIGDECIMAL_HUNDRED)));
							}
							if (maaPoaInstitution.getFinancialInstitution().getWnSecurityMaxAmount() != null) {
								totalWarningMaxSecurityAmount = totalWarningMaxSecurityAmount.add(maaPoaInstitution.getFinancialInstitution().getWnSecurityMaxAmount().multiply(maaPoaInstitution.getPercentage().divide(VbsConstants.BIGDECIMAL_HUNDRED)));
							}
						}
					}
				}
			}
		}
		dto.setMaxSecurityAmount(totalMaxSecurityAmount);
		dto.setWnSecurityMaxAmount(totalWarningMaxSecurityAmount);
		return dto;
	}

	@Override
	public FinancialInstitutionDTO saveFinancialInstitution(FinancialInstitutionDTO financialInstitutionDTO, UserDTO userDTO) throws VbsCheckedException {
		List<ErrorDTO> errors = validateFinancialInstitution(financialInstitutionDTO);
		
		if (errors.isEmpty()) {
			FinancialInstitutionEntity newFinancialInstitution = FinancialInstitutionServiceTransformer.transformDTOToEntity(financialInstitutionDTO, new FinancialInstitutionEntity());
			if(financialInstitutionDTO.getFinancialInstitutionTypeId()!=null) {
				newFinancialInstitution.setFinancialInstitutionType(genericDAO.findEntityById(FinancialInstitutionTypeEntity.class, financialInstitutionDTO.getFinancialInstitutionTypeId()));
			}

			if(newFinancialInstitution.getId() == null) {
				newFinancialInstitution = (FinancialInstitutionEntity) genericDAO.addEntity(newFinancialInstitution, userDTO.getUserId(), DateUtil.getLocalDateTimeNow());
			} else {
				newFinancialInstitution = (FinancialInstitutionEntity) genericDAO.updateEntity(newFinancialInstitution, userDTO.getUserId(), DateUtil.getLocalDateTimeNow());
			}
			
			
			if (financialInstitutionDTO.getContacts() != null) {
				saveFinancialInstitutionContacts(financialInstitutionDTO.getContacts(), userDTO);
			}
			if (financialInstitutionDTO.getMaaPoas() != null) {
				saveFinancialInstitutionMaaPoas(financialInstitutionDTO.getMaaPoas(), userDTO, newFinancialInstitution.getId());
			}
			if (financialInstitutionDTO.getAlerts() != null) {
				saveFinancialInstitutionAlerts(financialInstitutionDTO.getAlerts(), userDTO, newFinancialInstitution.getId());
			}
			
			financialInstitutionDTO = getFinancialInstitution(newFinancialInstitution.getId());

			 String jmsPrefix = propertyService.getValueOrNull(JMSConstants.TEST_JMS_PREFIX);
			 if (jmsPrefix == null) {
			 	sendFinancialInstitutionJmsMessageToCrm(newFinancialInstitution);
			 }
			
			return financialInstitutionDTO;
		} else {
			throw new VbsRuntimeException("Errors found when saving: " + errors.toString());
		}
	}
	
	private void sendFinancialInstitutionJmsMessageToCrm(FinancialInstitutionEntity financialInstittution) throws VbsCheckedException {
		FinancialInstitutionUpdate request = new FinancialInstitutionUpdate();
		request.setCreateDate(DateUtil.getCrmXMLDateStringFromLocalDateTime(financialInstittution.getCreateDate()));
		request.setCreateUser(financialInstittution.getCreateUser());
		request.setExpirationDate(DateUtil.getCrmXMLDateStringFromLocalDateTime(financialInstittution.getExpiryDate()));
		request.setId(financialInstittution.getId());
		request.setName(financialInstittution.getName());
		if (financialInstittution.getFinancialInstitutionType() != null) {
			request.setTypeId(financialInstittution.getFinancialInstitutionType().getId());
		}
		request.setUpdateDate(DateUtil.getCrmXMLDateStringFromLocalDateTime(financialInstittution.getUpdateDate()));
		request.setUpdateUser(financialInstittution.getUpdateUser());
		String xml = VbsUtil.convertJaxbToXml(request, FinancialInstitutionUpdate.class);
		jmsMessageSenderService.postCrmRequestQueue(xml, FinancialInstitutionUpdate.class.getSimpleName());
		jmsMessageSenderService.insertTransactionLog(JmsMessageTypeEnum.getCrmFinancialInstitutionUpdateTracking(financialInstittution.getId()), xml);
	}


	@Override
	public List<ErrorDTO> validateFinancialInstitution(FinancialInstitutionDTO dto){
		List<ErrorDTO> errors = new ArrayList<>();
		if(StringUtils.isEmpty(dto.getName())){
			errors.add(new ErrorDTO("Name is mandatory", NAME));
		}
		if(dto.getExpiryDate() == null){
			errors.add(new ErrorDTO("Expiration Date is mandatory", EXPIRATION_DATE));
		}
		if(!StringUtils.isEmpty(dto.getName()) && dto.getFinancialInstitutionTypeId()!=null) {
			List<FinancialInstitutionEntity> existingFIs = financialInstitutionDAO.getFinancialInstitutionsByCodeOrName(null, null, null, dto.getName(), null, "EQUALS", dto.getFinancialInstitutionTypeId());
			for (FinancialInstitutionEntity existingFI : existingFIs) {
				if(!DateUtil.isDateBeforeToday(existingFI.getExpiryDate()) && (dto.getId()==null || existingFI.getId().compareTo(dto.getId()) != 0)) {
					errors.add(new ErrorDTO("Financial Institution with same name and type already exists.", NAME));
					break;
				}
			}
		}

		if (dto.getContacts() != null) {
			for (int i=0;i< dto.getContacts().size();i++) {
				List<ErrorDTO> contactErrors = validateFinancialInstitutionContact(dto.getContacts().get(i), i);
				errors.addAll(contactErrors);
			}
		}
		if (dto.getMaaPoas() != null) {
			for (int i=0;i< dto.getMaaPoas().size();i++) {
				List<ErrorDTO> maaErrors = validateFinancialInstitutionMaaPoa(dto.getMaaPoas().get(i), i);
				errors.addAll(maaErrors);
			}
		}
		return errors;
	}

	@Override
	public String getCrmCompanyUrl() {
		return propertyService.getValue(VbsConstants.CRM_COMPANY_URL);
	}

	@Override
	public String getCrmCreateCompanyUrl() {
		return propertyService.getValue(VbsConstants.CRM_CREATE_COMPANY_URL);
	}

	@Override
	public List<ContactFinancialInstitutionDTO> getContactsForFinancialInstitution(Long financialInstitutionId) {
		List<ContactFinancialInstitutionEntity> contacts = financialInstitutionDAO.getContactsForFinancialInstitution(financialInstitutionId);
		return contacts.stream().map(FinancialInstitutionServiceTransformer::transformEntityToDTO).collect(Collectors.toList());
	}
	
	@Override
	public String getContentNavigatorUrlForFinancialInstitution(Long financialInstitutionId) {
		return propertyService.getValue(VbsConstants.CONTENT_NAVIGATOR_FI_DOCUMENTS_URL) + financialInstitutionId;
	}

	@Override
	public String getContentNavigatorUrl() {
		return propertyService.getValue(VbsConstants.CONTENT_NAVIGATOR_FI_DOCUMENTS_URL);
	}
	
	@Override
	public List<FinancialInstitutionMaaPoaDTO> getFinancialInstitutionMaaPoas(Long financialInstitutionId) {
		List<FinancialInstitutionMaaPoaDTO> returnList = new ArrayList<>();
		List<FinancialInstitutionMaaPoaEntity> maaPoas = financialInstitutionDAO.getMaaPoasForFinancialInstitution(financialInstitutionId);
		for (FinancialInstitutionMaaPoaEntity maaPoa : maaPoas) {
			FinancialInstitutionMaaPoaDTO dto = FinancialInstitutionServiceTransformer.transformEntityToDTO(maaPoa);
			List<FinancialInstitutionMaaPoaInstitutionEntity> maaPoaInstitutions = financialInstitutionDAO.getMaaPoaInstitutionsForMaaPoa(maaPoa.getId());
			for (FinancialInstitutionMaaPoaInstitutionEntity maaPoaInstitution : maaPoaInstitutions) {
				FinancialInstitutionMaaPoaInstitutionDTO maaPoaInstitutionDto = FinancialInstitutionServiceTransformer.transformEntityToDTO(maaPoaInstitution);
				FinancialInstitutionDTO maaPoaFIDto = FinancialInstitutionServiceTransformer.transformEntityToDTO(maaPoaInstitution.getFinancialInstitution());
				maaPoaInstitutionDto.setFinancialInstitution(maaPoaFIDto);
				if(maaPoaInstitution.getBranch()!=null) {
					BranchDTO maaPoaBranchDto = FinancialInstitutionServiceTransformer.transformEntityToDTO(maaPoaInstitution.getBranch());
					maaPoaInstitutionDto.setBranch(maaPoaBranchDto);
				}
				if (dto.getMaaPoaInstitutions() == null) {
					dto.setMaaPoaInstitutions(new ArrayList<>());
				}
				dto.getMaaPoaInstitutions().add(maaPoaInstitutionDto);
			}

			List<FinancialInstitutionSigningOfficerEntity> signingOfficers = financialInstitutionDAO.getSigningOfficersForFinancialInstitutionMaaPoa(maaPoa.getId());
			for (FinancialInstitutionSigningOfficerEntity signingOfficer : signingOfficers) {
				FinancialInstitutionSigningOfficerDTO signingOfficersDto = FinancialInstitutionServiceTransformer.transformEntityToDTO(signingOfficer);
				if (dto.getSigningOfficers() == null) {
					dto.setSigningOfficers(new ArrayList<>());
				}
				dto.getSigningOfficers().add(signingOfficersDto);
			}

			returnList.add(dto);
		}
		return returnList;
	}

	@Override
	public void saveFinancialInstitutionContacts(List<ContactFinancialInstitutionDTO> financialInstitutionContactsDTO, UserDTO userDTO) {
		for (int i=0;i< financialInstitutionContactsDTO.size();i++) {
			if(financialInstitutionContactsDTO.get(i).getDeleted()) {
				if(financialInstitutionContactsDTO.get(i).getId() != null) {
					ContactFinancialInstitutionEntity deleteEntity = genericDAO.findEntityById(ContactFinancialInstitutionEntity.class, financialInstitutionContactsDTO.get(i).getId());
					if(deleteEntity!=null) {
						genericDAO.removeEntity(deleteEntity, userDTO.getUserId(), DateUtil.getLocalDateTimeNow());
					}
				}

			} else {
				List<ErrorDTO> errors = validateFinancialInstitutionContact(financialInstitutionContactsDTO.get(i), i);

				if (errors.isEmpty()) {
					ContactFinancialInstitutionEntity newEntity = FinancialInstitutionServiceTransformer.transformDTOToEntity(financialInstitutionContactsDTO.get(i), new ContactFinancialInstitutionEntity());
					
					if(financialInstitutionContactsDTO.get(i).getFinancialInstitutionId()!=null) {
						newEntity.setFinancialInstitution(genericDAO.findEntityById(FinancialInstitutionEntity.class, financialInstitutionContactsDTO.get(i).getFinancialInstitutionId()));
					}

					ContactEntity contactEntity = null;
					if(financialInstitutionContactsDTO.get(i).getContact()!=null && financialInstitutionContactsDTO.get(i).getContact().getId() != null) {
						contactEntity = genericDAO.findEntityById(ContactEntity.class, financialInstitutionContactsDTO.get(i).getContact().getId());
					} else if(financialInstitutionContactsDTO.get(i).getContact()!=null && financialInstitutionContactsDTO.get(i).getContact().getCrmContactId() != null) {
						contactEntity = contactDAO.getContactByCrmContactId(financialInstitutionContactsDTO.get(i).getContact().getCrmContactId());
					}
					if(financialInstitutionContactsDTO.get(i).getContact()!=null && contactEntity==null) {
						AddressEntity addressEntity = null;
						AddressEntity mailingAddressEntity = null;
						if(financialInstitutionContactsDTO.get(i).getContact().getAddress()!=null) {
							addressEntity = (AddressEntity)genericDAO.addEntity(ContactServiceTransformer.transformDTOToEntity(financialInstitutionContactsDTO.get(i).getContact().getAddress(), new AddressEntity()), userDTO.getUserId(), DateUtil.getLocalDateTimeNow());
						}
						if(financialInstitutionContactsDTO.get(i).getContact().getMailingAddress()!=null) {
							mailingAddressEntity = (AddressEntity)genericDAO.addEntity(ContactServiceTransformer.transformDTOToEntity(financialInstitutionContactsDTO.get(i).getContact().getMailingAddress(), new AddressEntity()), userDTO.getUserId(), DateUtil.getLocalDateTimeNow());
						}
						contactEntity = ContactServiceTransformer.transformDTOToEntity(financialInstitutionContactsDTO.get(i).getContact(), new ContactEntity());
						contactEntity.setAddress(addressEntity);
						contactEntity.setMailingAddress(mailingAddressEntity);
						if(financialInstitutionContactsDTO.get(i).getContact().getFirstName() != null || financialInstitutionContactsDTO.get(i).getContact().getLastName() != null) {
							ContactTypeEntity contactType = genericDAO.findEntityById(ContactTypeEntity.class, VbsConstants.CONTACT_TYPE_PERSON);
							contactEntity.setContactType(contactType);
						}
						if(financialInstitutionContactsDTO.get(i).getContact().getCompanyName() != null) {
							ContactTypeEntity contactType = genericDAO.findEntityById(ContactTypeEntity.class, VbsConstants.CONTACT_TYPE_COMPANY);
							contactEntity.setContactType(contactType);
						}
						contactEntity = (ContactEntity) genericDAO.addEntity(contactEntity, userDTO.getUserId(), DateUtil.getLocalDateTimeNow());
					}

					newEntity.setContact(contactEntity);
		
					if(newEntity.getId() == null) {
						genericDAO.addEntity(newEntity, userDTO.getUserId(), DateUtil.getLocalDateTimeNow());
					} else {
						genericDAO.updateEntity(newEntity, userDTO.getUserId(), DateUtil.getLocalDateTimeNow());
					}
		
				} else {
					throw new VbsRuntimeException("Errors found when saving: " + errors.toString());
				}
			}
		}
	}


	private List<ErrorDTO> validateFinancialInstitutionContact(ContactFinancialInstitutionDTO dto, int i) {
		List<ErrorDTO> errors = new ArrayList<>();
		if(dto.getStartDate() == null){
			errors.add(new ErrorDTO("Start Date is mandatory", CRM_CONTACT_ID_DOT+i+DOT_START_DATE));
		}
		
		return errors;
	}
	
	@Override
	public void saveFinancialInstitutionMaaPoas(List<FinancialInstitutionMaaPoaDTO> financialInstitutionMaaPoasDTO, UserDTO userDTO, Long financialInstitutionId) {
		LocalDateTime now = DateUtil.getLocalDateTimeNow();
		for (int i=0; i<financialInstitutionMaaPoasDTO.size(); i++) {
			FinancialInstitutionMaaPoaDTO dto = financialInstitutionMaaPoasDTO.get(i);
			if(dto.isDeleted()) {
				if(dto.getId() != null) {
					FinancialInstitutionMaaPoaEntity deleteEntity = genericDAO.findEntityById(FinancialInstitutionMaaPoaEntity.class, dto.getId());
					if(deleteEntity != null) {
						for (FinancialInstitutionMaaPoaInstitutionDTO childDto : dto.getMaaPoaInstitutions()) {
							if(childDto.getId() != null) {
								FinancialInstitutionMaaPoaInstitutionEntity childEntity = genericDAO.findEntityById(FinancialInstitutionMaaPoaInstitutionEntity.class, childDto.getId());
								if (childEntity != null) {
									genericDAO.removeEntity(childEntity, userDTO.getUserId(), now);
								}
							}
						}
						for (FinancialInstitutionSigningOfficerDTO childDto : dto.getSigningOfficers()) {
							if(childDto.getId() != null) {
								FinancialInstitutionSigningOfficerEntity childEntity = genericDAO.findEntityById(FinancialInstitutionSigningOfficerEntity.class, childDto.getId());
								if (childEntity != null) {
									genericDAO.removeEntity(childEntity, userDTO.getUserId(), now);
								}
							}
						}
						genericDAO.removeEntity(deleteEntity, userDTO.getUserId(), now);
					}
				}
			} else {
				List<ErrorDTO> errors = validateFinancialInstitutionMaaPoa(dto, i);

				if (errors.isEmpty()) {
					FinancialInstitutionMaaPoaEntity newEntity = FinancialInstitutionServiceTransformer.transformDTOToEntity(dto, new FinancialInstitutionMaaPoaEntity());
					
					if(newEntity.getFinancialInstitution() == null) {
						newEntity.setFinancialInstitution(genericDAO.findEntityById(FinancialInstitutionEntity.class, financialInstitutionId));
					}

					if(newEntity.getId() == null) {
						long count = financialInstitutionDAO.getMaaPoasForFinancialInstitution(financialInstitutionId).size();
						newEntity.setMaaPoaSequence(count + 1);
						newEntity = (FinancialInstitutionMaaPoaEntity) genericDAO.addEntity(newEntity, userDTO.getUserId(), DateUtil.getLocalDateTimeNow());
						
						for (FinancialInstitutionMaaPoaInstitutionDTO childDto : dto.getMaaPoaInstitutions()) {
							FinancialInstitutionMaaPoaInstitutionEntity newChildEntity = FinancialInstitutionServiceTransformer.transformDTOToEntity(childDto, new FinancialInstitutionMaaPoaInstitutionEntity());
							newChildEntity.setFinancialInstitution(genericDAO.findEntityById(FinancialInstitutionEntity.class, childDto.getFinancialInstitution().getId()));
							newChildEntity.setBranch(genericDAO.findEntityById(BranchEntity.class, childDto.getBranch().getId()));
							newChildEntity.setFinancialInstitutionMaaPoa(newEntity);
							genericDAO.addEntity(newChildEntity, userDTO.getUserId(), now);
						}
						for (FinancialInstitutionSigningOfficerDTO childDto : dto.getSigningOfficers()) {
							FinancialInstitutionSigningOfficerEntity newChildEntity = FinancialInstitutionServiceTransformer.transformDTOToEntity(childDto, new FinancialInstitutionSigningOfficerEntity());
							newChildEntity.setFinancialInstitutionMaaPoa(newEntity);
							genericDAO.addEntity(newChildEntity, userDTO.getUserId(), now);
						}

					} else {
						newEntity = (FinancialInstitutionMaaPoaEntity) genericDAO.updateEntity(newEntity, userDTO.getUserId(), DateUtil.getLocalDateTimeNow());

						boolean updateSequences = false;
						for (FinancialInstitutionMaaPoaInstitutionDTO childDto : dto.getMaaPoaInstitutions()) {
							if(childDto.getDeleted()) {
								if(childDto.getId() != null) {
									FinancialInstitutionMaaPoaInstitutionEntity deleteEntity = genericDAO.findEntityById(FinancialInstitutionMaaPoaInstitutionEntity.class, childDto.getId());
									if(deleteEntity!=null) {
										updateSequences = true;
										genericDAO.removeEntity(deleteEntity, userDTO.getUserId(), DateUtil.getLocalDateTimeNow());
									}
								}
				
							} else {
								FinancialInstitutionMaaPoaInstitutionEntity newChildEntity = FinancialInstitutionServiceTransformer.transformDTOToEntity(childDto, new FinancialInstitutionMaaPoaInstitutionEntity());
								newChildEntity.setFinancialInstitution(genericDAO.findEntityById(FinancialInstitutionEntity.class, childDto.getFinancialInstitution().getId()));
								newChildEntity.setBranch(genericDAO.findEntityById(BranchEntity.class, childDto.getBranch().getId()));
								newChildEntity.setFinancialInstitutionMaaPoa(newEntity);
							
								if(childDto.getId() == null) {
									genericDAO.addEntity(newChildEntity, userDTO.getUserId(), now);
								} else {
									genericDAO.updateEntity(newChildEntity, userDTO.getUserId(), now);
								}
							}
						}

						if(updateSequences){
							//TODO this will cause an extra audit log and the other maa's to have their update date/user changed for the sake of updating the sequence number.
							//if this causes problems we'll have to remove the sequence and make it a generated value.
							List<FinancialInstitutionMaaPoaEntity> maaPoas = financialInstitutionDAO.getMaaPoasForFinancialInstitution(financialInstitutionId);
							if(!maaPoas.isEmpty()) {
								for (int j=0;j<maaPoas.size();j++) {
									maaPoas.get(j).setMaaPoaSequence(j+1L);
									genericDAO.updateEntity(maaPoas.get(j), userDTO.getUserId().toUpperCase(), now);
								}
							}
						}


						List<FinancialInstitutionSigningOfficerEntity> currentList = financialInstitutionDAO.getSigningOfficersForFinancialInstitutionMaaPoa(newEntity.getId());
						currentList.forEach(c -> {
							Optional<FinancialInstitutionSigningOfficerDTO> opt = dto.getSigningOfficers().stream().filter(s -> s.getId() != null && s.getId().compareTo(c.getId()) == 0).findFirst();
							if(opt.isPresent()){
								FinancialInstitutionSigningOfficerDTO signingDto = opt.get();
								if(signingDto.getDeleted()){
									genericDAO.removeEntity(c, userDTO.getUserId().toUpperCase(), now);
								}else if(!c.getCompanyName().equalsIgnoreCase(signingDto.getCompanyName()) && c.getContactName().equalsIgnoreCase(signingDto.getCompanyName())){
									c.setCompanyName(signingDto.getCompanyName());
									c.setContactName(signingDto.getContactName());
									genericDAO.updateEntity(c, userDTO.getUserId().toUpperCase(), now);
								}
							}else{
								genericDAO.removeEntity(c, userDTO.getUserId().toUpperCase(), now);
							}
						});

						for(FinancialInstitutionSigningOfficerDTO d : dto.getSigningOfficers()){
							if(d.getId() == null){
								FinancialInstitutionSigningOfficerEntity newEntitySigningOfficer = FinancialInstitutionServiceTransformer.transformDTOToEntity(d, new FinancialInstitutionSigningOfficerEntity());
								newEntitySigningOfficer.setFinancialInstitutionMaaPoa(newEntity);
								genericDAO.addEntity(newEntitySigningOfficer, userDTO.getUserId(), DateUtil.getLocalDateTimeNow());
							}
						}
					}
				} else {
					throw new VbsRuntimeException("Errors found when saving: " + errors.toString());
				}
			}
		}
	}

	@Override
	public List<ErrorDTO> validateFinancialInstitutionMaaPoa(FinancialInstitutionMaaPoaDTO dto, int maaPoaIndex) {
		List<ErrorDTO> errors = new ArrayList<>();
		BigDecimal totalPercentage = BigDecimal.ZERO;
		for (int i=0; i<dto.getMaaPoaInstitutions().size(); i++) {
			FinancialInstitutionMaaPoaInstitutionDTO relationDto = dto.getMaaPoaInstitutions().get(i);
			if(!relationDto.getDeleted()) {
				if (relationDto.getFinancialInstitution() == null) {
					errors.add(new ErrorDTO("Maa / POA has to have Linked Financial Institutions", FinancialInstitutionMaaPoaInstitutionDTO));
				} else {
					if (relationDto.getPercentage() == null){
						errors.add(new ErrorDTO("Percentage is mandatory", MAAPOAS_DOT+maaPoaIndex+DOT_MAAPOA_INSTITUTIONS_DOT+i+DOT_PERCENTAGE));
					} else {
						totalPercentage = totalPercentage.add(relationDto.getPercentage());
					}
					if (relationDto.getFinancialInstitution().getFinancialInstitutionTypeId() == null
							|| VbsConstants.FINANCIAL_INSTITUTION_TYPE_SURETY_COMPANIES != relationDto.getFinancialInstitution().getFinancialInstitutionTypeId()) {
						errors.add(new ErrorDTO("Maa / POA company must be Surety Company Type", MAAPOAS_DOT+maaPoaIndex+DOT_ID));
					}
				}
			}
		}

		for (int i=0; i<dto.getSigningOfficers().size(); i++) {
			if(VbsUtil.isNullorEmpty(dto.getSigningOfficers().get(i).getCompanyName())){
				errors.add(new ErrorDTO("Company Name is mandatory", MAAPOAS_DOT+maaPoaIndex+DOT_SIGNING_OFFICERS_DOT+i+DOT_COMPANY_NAME));
			}
			if(VbsUtil.isNullorEmpty(dto.getSigningOfficers().get(i).getContactName())){
				errors.add(new ErrorDTO("Contact Name is mandatory", MAAPOAS_DOT+maaPoaIndex+DOT_SIGNING_OFFICERS_DOT+i+DOT_CONTACT_NAME));
			}
		}
		if ((dto.getMaaPoaInstitutions().isEmpty() && !dto.isDeleted())) {
			errors.add(new ErrorDTO("You must add a surity company", "financialInstitutionTypeId"));
		}else if(totalPercentage.compareTo(VbsConstants.BIGDECIMAL_HUNDRED) != 0 && !dto.isDeleted()) {
			errors.add(new ErrorDTO("Percentage sum is " + totalPercentage + " but must be 100%", "financialInstitutionTypeId"));
		}
		
		if (dto.isDeleted()) {
			// check if it is used in any security
			List<SecurityEntity> securities = securityDAO.getSecuritiesWithMaaPoa(dto.getId());
			if (!securities.isEmpty()) {
				errors.add(new ErrorDTO("MAA / POA Cannot be deleted because it is linked to security ", MAAPOAS_DOT+maaPoaIndex+DOT_ID));
			}
		}
		
		return errors;
	}

	@Override
	public void saveFinancialInstitutionAlerts(List<FinancialInstitutionAlertDTO> financialInstitutionAlertDTO, UserDTO userDTO, Long financialInstitutionId) {

		for (FinancialInstitutionAlertDTO alertDTO : financialInstitutionAlertDTO) {
			if(alertDTO.getAlertType() != null) {
				FinancialInstitutionAlertEntity alertEntity = new FinancialInstitutionAlertEntity();
				FinancialInstitutionEntity financialInstitution = genericDAO.findEntityById(FinancialInstitutionEntity.class, financialInstitutionId);
				FinancialInstitutionServiceTransformer.transformDTOToEntity(alertDTO, alertEntity, financialInstitution);
				if(alertEntity.getEndUser() != null && alertEntity.getEndDate() == null) {
					alertEntity.setEndDate(DateUtil.getLocalDateTimeNow());
				}
				if (alertEntity.getId() != null) {
					FinancialInstitutionAlertEntity dbAlert = genericDAO.findEntityById(FinancialInstitutionAlertEntity.class, alertEntity.getId());					
					if (!alertEntity.equals(dbAlert)) {
						genericDAO.updateEntity(alertEntity, userDTO.getUserId(), DateUtil.getLocalDateTimeNow());	
					}
				} else {
					genericDAO.addEntity(alertEntity, userDTO.getUserId(), DateUtil.getLocalDateTimeNow());
				}
			}
		}
	}

	@Override
	public List<FinancialInstitutionAlertDTO> getFinancialInstitutionAlerts(Long financialInstitutionId) {
		List<FinancialInstitutionAlertEntity> alerts = financialInstitutionDAO.getAlertsForFinancialInstitution(financialInstitutionId);
		return alerts.stream().map(FinancialInstitutionServiceTransformer::transformEntityToDTO).collect(Collectors.toList());
	}

	@Override
	public FinancialInstitutionMaaPoaDTO getMaaPoa(Long securityId) {
		SecurityEntity securityEntity = genericDAO.findEntityById(SecurityEntity.class, securityId);
		//TODO re-work these amounts to be "available" where necessary.
		return getMaaPoa(securityEntity.getFinancialInstitutionMaaPoa().getId(), securityEntity.getOriginalAmount(), securityEntity.getCurrentAmount());
	}

	@Override
	public FinancialInstitutionMaaPoaDTO getMaaPoa(SecurityDTO security) {
		if (security != null && security.getFinancialInstitutionMaaPoaDTO() != null) {
			return getMaaPoa(security.getFinancialInstitutionMaaPoaDTO().getId(), security.getOriginalAmount(), security.getCurrentAmount());
		}
		return null;
	}

	@Override
	public FinancialInstitutionMaaPoaDTO getMaaPoa(Long maaPoaId, BigDecimal originalAmount, BigDecimal currentAmount) {
		FinancialInstitutionMaaPoaDTO maaPoaDto = null;
		if (maaPoaId != null) {
			FinancialInstitutionMaaPoaEntity maaPoa = genericDAO.findEntityById(FinancialInstitutionMaaPoaEntity.class, maaPoaId);
			if (maaPoa != null) {
				maaPoaDto = FinancialInstitutionServiceTransformer.transformEntityToDTO(maaPoa);
				List<FinancialInstitutionMaaPoaInstitutionEntity> maaPoaInstitutions = financialInstitutionDAO.getMaaPoaInstitutionsForMaaPoa(maaPoa.getId());
				for (FinancialInstitutionMaaPoaInstitutionEntity maaPoaInstitution : maaPoaInstitutions) {
					FinancialInstitutionMaaPoaInstitutionDTO maaPoaInstitutionDto = FinancialInstitutionServiceTransformer.transformEntityToDTO(maaPoaInstitution);
					if(originalAmount != null && originalAmount.compareTo(BigDecimal.ZERO) != 0) {
						maaPoaInstitutionDto.setOriginalAmountOnInstitution(originalAmount.multiply(maaPoaInstitution.getPercentage()).divide(VbsConstants.BIGDECIMAL_HUNDRED, 4, VbsConstants.ROUNDING_MODE));
					}
					if(currentAmount != null && currentAmount.compareTo(BigDecimal.ZERO) != 0) {
						maaPoaInstitutionDto.setCurrentAmountOnInstitution(currentAmount.multiply(maaPoaInstitution.getPercentage()).divide(VbsConstants.BIGDECIMAL_HUNDRED, 4, VbsConstants.ROUNDING_MODE));
					}
					maaPoaInstitutionDto.setFinancialInstitution(FinancialInstitutionServiceTransformer.transformEntityToDTO(maaPoaInstitution.getFinancialInstitution()));
					maaPoaInstitutionDto.setBranch(FinancialInstitutionServiceTransformer.transformEntityToDTO(maaPoaInstitution.getBranch()));
					if (maaPoaDto.getMaaPoaInstitutions() == null) {
						maaPoaDto.setMaaPoaInstitutions(new ArrayList<>());
					}
					maaPoaDto.getMaaPoaInstitutions().add(maaPoaInstitutionDto);
				}
			}
		}
		return maaPoaDto;
	}

}
