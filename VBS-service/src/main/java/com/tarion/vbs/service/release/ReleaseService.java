package com.tarion.vbs.service.release;

import com.tarion.vbs.common.dto.release.ReleaseDTO;
import com.tarion.vbs.orm.entity.release.ReleaseEntity;

import javax.ejb.Local;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Local
public interface ReleaseService extends ReleaseServiceRemote{

    void setReleaseInterestAmounts(ReleaseDTO releaseDTO, ReleaseEntity entity, BigDecimal authorizedAmount, String userId, LocalDateTime time);
    void setReleaseInterestAmounts(ReleaseEntity entity, BigDecimal authorizedAmount, String userId, LocalDateTime time);

}
