package com.tarion.vbs.service.webservice;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.tarion.vbs.common.constants.PermissionConstants;
import com.tarion.vbs.common.dto.enrolment.EnrolmentSecurityInfoDTO;
import com.tarion.vbs.common.dto.security.AmountsDTO;
import com.tarion.vbs.common.dto.security.SecurityPurposeDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchResultWrapper;
import com.tarion.vbs.common.dto.tip.ReleaseApproverDTO;
import com.tarion.vbs.common.dto.tip.SecurityTipInfoDTO;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.exceptions.VbsDirectoryEnvironmentException;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.security.SecurityDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.dao.view.ViewDAO;
import com.tarion.vbs.enrolment.EnrolmentDAO;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentPoolEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import com.tarion.vbs.orm.entity.view.MsWordSecRelLtrViewEntity;
import com.tarion.vbs.service.security.SecurityService;
import com.tarion.vbs.service.userprofile.UserService;
import com.tarion.vbs.service.util.LookupService;



/**
 * The VBS WebService Bean.
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-03-23
 * @version 1.0
 */
@Stateless
@Local
@WebService(serviceName = "VBSWS")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class VBSWS {

	@EJB
	private EnrolmentDAO enrolmentDAO;
	@EJB
	private SecurityService securityService;
	@EJB
	private SecurityDAO securityDAO;
	@EJB
	private ViewDAO viewDAO;
	@EJB
	private GenericDAO genericDAO;
	@EJB
	private LookupService lookupService;
	@EJB
	private UserService userService;


	@WebMethod
	public long getUnitsToCoverForEnrolment(String enrolmentNumber) {
		return enrolmentDAO.getPoolUnitsToCoverByEnrolmentNumber(enrolmentNumber);
	}
	
	@WebMethod
	public List<EnrolmentSecurityInfoDTO> getEnrolmentSecurityInfo(@WebParam(name = "enrolment") List<String> enrolments) {
		return enrolmentDAO.getEnrolmentSecurityInfo(enrolments);
	}

	@WebMethod
	public List<String> checkCurrentEnrolments(@WebParam(name = "enrolment")List<String> enrolments) {
		return enrolmentDAO.getCurrentEnrolmentsIn(enrolments);
	}

	@WebMethod
	public AmountsDTO getSecurityAmountsForVB(@WebParam(name = "vbNumber") String vbNumber){
		return securityService.getSecurityAmountsForVB(vbNumber);
	}
	
	@WebMethod
	public AmountsDTO getBlanketSecurityAmountsForVB(@WebParam(name = "vbNumber") String vbNumber){
		return securityService.getBlanketSecurityAmountsForVB(vbNumber);
	}
	
	@WebMethod
	public AmountsDTO getSecurityAmountsForEnrolment(@WebParam(name = "enrolmentNumber") String enrolmentNumber){
		return securityService.getSecurityAmountsForEnrolment(enrolmentNumber);
	}
	
	@WebMethod
	public List<MsWordSecRelLtrViewEntity> getAllMsWordSecRelLtrs() {
		return viewDAO.getAllMsWordSecRelLtrs();
	}

	@WebMethod
	public MsWordSecRelLtrViewEntity findMsWordSecRelLtrByReferenceFieldName(@WebParam(name = "referenceFieldName") String referenceFieldName) {
		return viewDAO.findMsWordSecRelLtrByReferenceFieldName(referenceFieldName);
	}
	
	@WebMethod
	public SecuritySearchResultWrapper getSecuritiesForInstrumentNumber(@WebParam(name = "instrumentNumber") String instrumentNumber) {
		return securityService.getSecuritiesByInstrumentNumber(instrumentNumber);
	}
	
	@WebMethod
	public SecuritySearchResultWrapper getSecuritiesForVbNumber(@WebParam(name = "vbNumber") String vbNumber) {
		return securityService.getSecuritiesByVbNumber(vbNumber);
	}
	
	@WebMethod
	public SecuritySearchResultWrapper getSecuritiesForEnrolment(@WebParam(name = "enrolment") String enrolmentNumber) {
		return securityService.getSecuritiesByEnrolment(enrolmentNumber);
	}
	
	@WebMethod
	public SecurityPurposeDTO getSecurityPurpose(@WebParam(name = "securityId") String securityId) {
		Long id = VbsUtil.parseLong(securityId);
		SecurityEntity security = (id == null) ? null : genericDAO.findEntityById(SecurityEntity.class, id);
		return (security == null) ? null : lookupService.transformSecurityPurpose(security.getSecurityPurpose());
	}

	@WebMethod
	public List<String> getEnrolmentNumbersBySecurityId(@WebParam(name = "securityId") String securityId){
		List<EnrolmentPoolEntity> enrolmentPoolList = enrolmentDAO.getEnrolmentsByPoolId(securityDAO.getPoolForSecurityById(Long.parseLong(securityId)));
		List<String> enrolmentIds = new ArrayList<String>();
		for(EnrolmentPoolEntity enrolmentPool : enrolmentPoolList) {
			enrolmentIds.add(enrolmentPool.getEnrolment().getEnrolmentNumber());
		}
		return enrolmentIds;
	}

	@WebMethod
	public List<SecurityTipInfoDTO> getBlanketSecuritiesByVB(@WebParam(name = "vbNumber") String vbNumber) {
		return securityDAO.getSecurityByVbNumberBlanket(vbNumber);
	}

	@WebMethod
	public List<ReleaseApproverDTO> getSecurityReleaseApproversList() throws VbsDirectoryEnvironmentException {
		List<UserDTO> list = userService.getUsersByPermissionName(PermissionConstants.P_APPROVE_RELEASE_FINAL);
		return list.stream().map(this::transform).collect(Collectors.toList());
	}

	private ReleaseApproverDTO transform(UserDTO userDTO){
		ReleaseApproverDTO dto = new ReleaseApproverDTO();
		dto.setEmail(userDTO.getEmailAddress());
		dto.setName(userDTO.getFirstName() + " " + userDTO.getLastName());
		dto.setUserName(userDTO.getUserId());
		return dto;
	}
}
