package com.tarion.vbs.service.util;

import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.common.util.DateUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Util for creating simple, single sheet excel from a list of objects.
 * Suited for large data sets as it tracks column size independently.
 */

class ExcelUtil {

    static byte[] createExcel(String sheetName, List<ColumnConfiguration> config, List<?> data) throws VbsCheckedException {
        SXSSFWorkbook wb = new SXSSFWorkbook();
        wb.setCompressTempFiles(true);
        SXSSFSheet sheet = wb.createSheet(sheetName);

        CellStyle headerStyle = getHeaderStyle(wb);
        CellStyle thinStyle = getThinBorderStyle(wb);
        CellStyle currencyStyle = getCurrencyStyle(wb);

        Map<Integer, Integer> colLengths = new HashMap<>();
        int rowNum = 0;
        Row header = sheet.createRow(rowNum);
        for(ColumnConfiguration col : config){
            Cell cell = header.createCell(col.getColNum());
            cell.setCellValue(col.getColumnName());
            cell.setCellStyle(headerStyle);
            colLengths.put(col.colNum, col.getColumnName().length());
        }
        rowNum++;
        sheet.createFreezePane(0, 1);

        if(!data.isEmpty()) {
            Map<Integer, Method> methods = new HashMap<>();
            for(Method m : data.get(0).getClass().getMethods()){
                if(m.getName().startsWith("get")){
                    for(ColumnConfiguration col : config){
                        if(m.getName().equalsIgnoreCase("get"+col.getPropertyName()) || m.getName().equalsIgnoreCase("is"+col.getPropertyName())){
                            methods.put(col.colNum, m);
                        }
                    }
                }
            }

            for(Object d : data){
                Row row = sheet.createRow(rowNum);
                for(ColumnConfiguration col : config){
                    Cell cell = row.createCell(col.getColNum());
                    if(col.getColumnType().equals(ColumnType.CURRENCY)){
                        cell.setCellStyle(currencyStyle);
                    }else{
                        cell.setCellStyle(thinStyle);
                    }
                    String sValue = "";
                    try {
                        if(col.getDataClass().equals(String.class)){
                            sValue = (String) methods.get(col.getColNum()).invoke(d);
                            cell.setCellValue(sValue);
                        }else if(col.getDataClass().equals(Integer.class)){
                            Integer value = (Integer) methods.get(col.getColNum()).invoke(d);
                            cell.setCellValue(value);
                            sValue = value.toString();
                        }else if(col.getDataClass().equals(Long.class)){
                            Long value = (Long) methods.get(col.getColNum()).invoke(d);
                            cell.setCellValue(value);
                            sValue = value.toString();
                        }else if(col.getDataClass().equals(BigDecimal.class)){
                            BigDecimal value = (BigDecimal) methods.get(col.getColNum()).invoke(d);
                            cell.setCellValue(value.doubleValue());
                            sValue = VbsUtil.getCurrencyFormattedString(value);
                        }else if(col.getDataClass().equals(LocalDateTime.class)){
                            LocalDateTime value = (LocalDateTime) methods.get(col.getColNum()).invoke(d);
                            sValue = col.getColumnType().equals(ColumnType.DATETIME) ?
                                    DateUtil.getDateFormattedLong(value) : DateUtil.getDateFormattedShort(value);
                            cell.setCellValue(sValue);
                        }else if(col.getDataClass().equals(Boolean.class)){
                            Boolean value = (Boolean) methods.get(col.getColNum()).invoke(d);
                            sValue = (value != null && value) ? "Yes" : "No";
                            cell.setCellValue(sValue);
                        }
                    }catch(IllegalAccessException| InvocationTargetException e){
                        LoggerUtil.logError(ExcelUtil.class, "createExcel", e);
                    }

                    if(sValue != null && colLengths.get(col.colNum).compareTo(sValue.length()) < 0){
                        colLengths.put(col.colNum, sValue.length());
                    }
                }
                rowNum++;
            }
        }

        for(Map.Entry<Integer, Integer> col : colLengths.entrySet()){
            int maxChars = col.getValue();
            if(maxChars > 245){
                sheet.setColumnWidth(col.getKey(), 65280);
            }else{
                sheet.setColumnWidth(col.getKey(), (maxChars*240));
            }
        }

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        try {
            wb.write(b);
            byte[] file = b.toByteArray();
            b.close();
            wb.dispose();
            return file;
        } catch (IOException io) {
            throw new VbsCheckedException("Failed to create Excel sheet.", io);
        }
    }

    private static CellStyle getHeaderStyle(SXSSFWorkbook wb){
        CellStyle headerStyle = getThinBorderStyle(wb);
        Font f = getFont(wb);
        f.setBold(true);
        headerStyle.setFont(f);
        headerStyle.setAlignment(HorizontalAlignment.CENTER);
        return headerStyle;
    }

    private static CellStyle getCurrencyStyle(SXSSFWorkbook wb) {
        CellStyle currencyStyle = wb.createCellStyle();
        currencyStyle.cloneStyleFrom(getThinBorderStyle(wb));
        currencyStyle.setDataFormat((short) 7);
        return currencyStyle;
    }

    private static CellStyle getThinBorderStyle(SXSSFWorkbook wb) {
        CellStyle thinStyle = wb.createCellStyle();
        thinStyle.setBorderBottom(BorderStyle.THIN);
        thinStyle.setBorderTop(BorderStyle.THIN);
        thinStyle.setBorderRight(BorderStyle.THIN);
        thinStyle.setBorderLeft(BorderStyle.THIN);
        thinStyle.setFont(getFont(wb));
        return thinStyle;
    }

    private static Font getFont(SXSSFWorkbook wb){
        Font font = wb.createFont();
        font.setFontHeightInPoints((short)10);
        font.setFontName("Arial");
        return font;
    }

    static class ColumnConfiguration {

        private int colNum;
        private String columnName;
        private String propertyName;
        private ColumnType columnType;
        private Class dataClass;

        ColumnConfiguration(int colNum, String columnName, String propertyName, ColumnType columnType, Class dataClass){
            this.colNum = colNum;
            this.columnName = columnName;
            this.propertyName = propertyName;
            this.columnType = columnType;
            this.dataClass = dataClass;
        }

        int getColNum() {
            return colNum;
        }

        String getColumnName() {
            return columnName;
        }

        String getPropertyName() {
            return propertyName;
        }

        ColumnType getColumnType() {
            return columnType;
        }

        Class getDataClass() {
            return dataClass;
        }
    }

    public enum ColumnType {
        STRING, DATE, DATETIME, CURRENCY
    }

}
