/* 
 * 
 * PropertyServiceImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import com.tarion.vbs.common.dto.interest.InterestRateDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.dao.interest.InterestDAO;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.orm.entity.interest.InterestRateEntity;
import com.tarion.vbs.service.interest.InterestService;

import javax.ejb.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Scheduler Service with operations that need to be scheduled
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-12-20
 * @version 1.0
 */
@Stateless(mappedName = "ejb/SchedulerService")
@LocalBean
public class SchedulerService {
	
	@EJB
	InterestService interestService;

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Schedule(hour = "3", minute = "5")
	public void triggerDailyInterestSend() {
		interestService.findDatesRequiringInterestCalculation();
	}	


}
