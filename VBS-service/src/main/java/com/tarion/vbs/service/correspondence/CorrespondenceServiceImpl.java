package com.tarion.vbs.service.correspondence;

import com.tarion.vbs.common.dto.NameDescriptionDTO;
import com.tarion.vbs.common.dto.correspondence.CorrespondenceWSRequest;
import com.tarion.vbs.common.dto.correspondence.CorrespondenceWSResponse;
import com.tarion.vbs.common.dto.correspondence.GenerateCorrespondenceDTO;
import com.tarion.vbs.common.dto.correspondence.TokenDTO;
import com.tarion.vbs.common.enums.CorrespondenceActionEnum;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.contact.ContactDAO;
import com.tarion.vbs.dao.correspondence.CorrespondenceDAO;
import com.tarion.vbs.dao.release.ReleaseDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.LookupDAO;
import com.tarion.vbs.orm.entity.correspondence.CorrespondenceTemplateEntity;
import com.tarion.vbs.orm.entity.correspondence.CorrespondenceTokenEntity;
import com.tarion.vbs.orm.entity.release.ReleaseEnrolmentEntity;
import com.tarion.vbs.orm.entity.release.ReleaseEntity;
import com.tarion.vbs.service.jaxb.correspondence.ExternalCorrGenGenericRequest;
import com.tarion.vbs.service.util.JmsMessageSenderService;
import com.tarion.vbs.service.util.JmsMessageTypeEnum;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.Tuple;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service for getting correspondence tokens
 * 
 * @author <A href="joni.paananen@tarion.com">Joni Paananen</A>
 * @since 2019-02-19
 * @version 1.0
 */
@Stateless(mappedName = "ejb/CorrespondenceService")
@Interceptors(EjbLoggingInterceptor.class)
public class CorrespondenceServiceImpl implements CorrespondenceService, CorrespondenceServiceRemote {

	@EJB
	private CorrespondenceDAO correspondenceDAO;
	@EJB
	private ReleaseDAO releaseDAO;
	@EJB
	private ContactDAO contactDAO;
	@EJB
	private JmsMessageSenderService jmsMessageSenderService;
	@EJB
	private LookupDAO lookupDAO;

	@Override
	public CorrespondenceWSResponse getTokens(CorrespondenceWSRequest request) {
		if (VbsUtil.isNullorEmpty(request.getTemplateName())) {
			throw new VbsRuntimeException("CORR01 - No Template Name passed");
		}
		if (request.getSecurityNumber() == 0) {
			throw new VbsRuntimeException("CORR02 - No Security Id passed");
		}
		if (request.getSequenceNumber() == 0) {
			throw new VbsRuntimeException("CORR03 - No Sequence Number passed");
		}

		CorrespondenceTemplateEntity template = correspondenceDAO.getCorrespondenceTemplateByName(request.getTemplateName());
		if(template == null){
			throw new VbsRuntimeException("CORR04 - Template not found for Template Name :" + request.getTemplateName());
		}

		ReleaseEntity release;
		try{
			release = releaseDAO.getReleaseBySecurityIdSequenceNumber(request.getSecurityNumber(), request.getSequenceNumber());
		}catch(Exception e){
			throw new VbsRuntimeException("CORR05 - Release not found. Security ID: " + request.getSecurityNumber() + ", Sequence Number: " + request.getSequenceNumber());
		}

		if (template.getSecurityType() != null && template.getSecurityType().getId().compareTo(release.getSecurity().getSecurityType().getId()) != 0) {
			throw new VbsRuntimeException("CORR06 - Security Type for Security #" + release.getSecurity().getId() + " does not match that required for the template name: " + request.getTemplateName());
		}

		List<CorrespondenceTokenEntity> templateTokens = correspondenceDAO.getTokensForTemplate(request.getTemplateName());
		if (templateTokens == null || templateTokens.isEmpty()) {
			throw new VbsRuntimeException("CORR07 - No tokens found for template name: " + request.getTemplateName());
		}

		Tuple vbsTokenValues = correspondenceDAO.getVbsTokenValuesForRelease(release.getId());
		List<TokenDTO> tokens = templateTokens.stream().map(t -> {
			TokenDTO d = new TokenDTO();
			d.setToken(t.getName());
			try{
				Object val = vbsTokenValues.get(t.getName());
				if(val instanceof BigDecimal) {
					d.setValue(VbsUtil.getCurrencyFormattedString((BigDecimal) val));
				} else {
					d.setValue(val == null ? null : val.toString());
				}
			}catch(IllegalArgumentException e){
				//token doesnt have value in the return, NON-vbs value
			}
			return d;
		}).collect(Collectors.toList());

		CorrespondenceWSResponse response = new CorrespondenceWSResponse();
		response.setReleaseId(release.getId());
		response.setTemplateName(request.getTemplateName());
		response.setTokens(tokens);

		return response;
	}

	@Override
	public void sendAutoReleaseCorrespondenceRequest(CorrespondenceActionEnum action, String enrolmentNumber, ReleaseEntity release, String userId){

		String vbNumber = (release.getSecurity().getPrimaryVb() != null) ? release.getSecurity().getPrimaryVb().getCrmContactId() : null;
		ExternalCorrGenGenericRequest request = new ExternalCorrGenGenericRequest();

		request.setAction(action.toString());
		request.setSecurityId(release.getSecurity().getId().toString());
		request.setSourceApplication("VBS");
		request.setSecurityId(release.getSecurity().getId() + "");
		request.setApplicationId(release.getSequenceNumber() + "");
		request.setInstrumentNumber(release.getSecurity().getInstrumentNumber());
		request.setEnrolmentNumber(VbsUtil.removeEnrolmentStartingH(enrolmentNumber));
		request.setPoolId(release.getSecurity().getPool().getId().toString());
		request.setVbNumber(vbNumber);
		request.setUserId(userId);

		String xml = VbsUtil.convertJaxbToXml(request, ExternalCorrGenGenericRequest.class);
		String trackingNumber = JmsMessageTypeEnum.getTipCorrespondenceGenerationRequestTracking(Long.parseLong(request.getSecurityId()));

		jmsMessageSenderService.postTipCorrespondenceMessage(xml);
		jmsMessageSenderService.insertTransactionLog(trackingNumber, xml, null);
	}

	@Override
	public List<NameDescriptionDTO> getAllTemplates() {
		List<CorrespondenceTemplateEntity> templates = lookupDAO.getAllTemplateTypes();
		List<NameDescriptionDTO> result = new ArrayList<>();
		for (CorrespondenceTemplateEntity template : templates) {
			result.add(new NameDescriptionDTO(template.getId(), template.getName(), null));
		}
		return result;
	}

	@Override
	public GenerateCorrespondenceDTO generate(GenerateCorrespondenceDTO generateCorrespondenceDTO) {
		return generateCorrespondenceDTO;
	}

}
