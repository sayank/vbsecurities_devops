/* 
 * 
 * EmailWSInvocationService.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.service.jaxb.email.EmailAttachmentDto;

import javax.ejb.Local;
import java.util.List;


/**
 * Utility Bean to invoke ESP Web Service Client
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-01-25
 * @version 1.0
 */
@Local
public interface EmailWSInvocationService {

	void sendEmailUsingEsp(String subject, String body, String fromAddress, String toAddresses) throws VbsCheckedException;
	void sendEmailUsingEspWithFile(String subject, String body, String fromAddress, String toAddresses, byte[] emailAttachment, String fileName, String mimeType) throws VbsCheckedException;
	void sendEmailUsingEspWithListOfFiles(String subject, String body, String fromAddress, String toAddresses, List<EmailAttachmentDto> emailAttachments) throws VbsCheckedException;

}
