/* 
 * 
 * InternalJmsMessageSenderServiceImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.processor;

import com.tarion.vbs.common.constants.JMSConstants;
import com.tarion.vbs.common.enums.ProcessorTriggerTypeEnum;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;

import javax.ejb.Singleton;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;
import javax.jms.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;


/**
 * This class is used to post a message to a local Internal JMS queue for the triggering processing
 * 
 */
@Singleton(mappedName = "ejb/InternalJmsMessageSenderService")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class InternalJmsMessageSenderServiceImpl implements InternalJmsMessageSenderService {

	@Override
	@ExcludeClassInterceptors
	public void sendTriggerMessage(ProcessorTriggerTypeEnum triggerType, String xmlMessageText)  {
		long start = LoggerUtil.logEnter(InternalJmsMessageSenderServiceImpl.class, "sendTriggerMessage", triggerType);
		send(xmlMessageText, triggerType);
		LoggerUtil.logExit(InternalJmsMessageSenderServiceImpl.class, "sendTriggerMessage", start, triggerType);
	}
	
	private void send(String msg, ProcessorTriggerTypeEnum messageType) {
		try {
			sendTxtMessageToInternalQueue(msg, messageType);
		} catch (NamingException e) {
			throw new VbsRuntimeException(e.getMessage());
		}
	}
	
	private void sendTxtMessageToInternalQueue(String msg, ProcessorTriggerTypeEnum messageType) throws NamingException {
		
		String connectionFactoryName = JMSConstants.VBS_XA_CF_JNDI;
		String queueJndiName = JMSConstants.INTERNAL_QUEUE_NAME_SAF;
		
		InitialContext ic = new InitialContext();
		ConnectionFactory connectionFactory = (ConnectionFactory) ic.lookup(connectionFactoryName);
		Queue theQueue = (Queue) ic.lookup(queueJndiName);
		
		try(
			Connection connection = connectionFactory.createConnection();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageProducer producer = session.createProducer(theQueue)
		){
		
			TextMessage textMessage = session.createTextMessage();

			setHeaderForInternalMessage(textMessage, messageType);

			textMessage.setText(msg);

			producer.send(textMessage);
		} catch (JMSException je) {
			String errMsg = "sendTxtMessageToQueue for queue=[" + queueJndiName + "] failed: " + je.toString();
			throw new VbsRuntimeException(errMsg, je);
		}
		
	}
	
	private void setHeaderForInternalMessage(TextMessage txtMsg, ProcessorTriggerTypeEnum messageType) throws JMSException {
		txtMsg.setStringProperty(JMSConstants.MESSAGE_NAME, messageType.toString());
	}

}
