/* 
 * 
 * SecurityServiceTransformer.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.release;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.NameDescriptionDTO;
import com.tarion.vbs.common.dto.release.*;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.release.*;
import com.tarion.vbs.orm.entity.security.SecurityEntity;

import javax.interceptor.Interceptors;
import java.math.BigDecimal;
import java.time.LocalDateTime;


/**
 * Transformer helper class used by Release Service to provide transformation between DTO and Entity
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-09
 * @version 1.0
 */
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class ReleaseServiceTransformer {
	
	private ReleaseServiceTransformer() {}

	public static void transformDTOToEntity(ReleaseEnrolmentDTO releaseEnrolment, ReleaseEnrolmentEntity releaseEnrolmentEntity) {

		releaseEnrolmentEntity.setAuthorizedAmount(releaseEnrolment.getAuthorizedAmount());
		releaseEnrolmentEntity.setRequestedAmount(releaseEnrolment.getRequestAmount());
		if (releaseEnrolment.isFullRelease() != null) {
			releaseEnrolmentEntity.setFullRelease(releaseEnrolment.isFullRelease());
		}
	}

	public static void transformDTOToEntity(ReleaseDTO dto, ReleaseEntity entity) {

		if(dto.getEnrolments().isEmpty()) {
			entity.setRequestAmount(dto.getRequestedAmount());
			entity.setAuthorizedAmount(dto.getAuthorizedReleaseAmount());
		}

		entity.setComment(dto.getComment());
		if(entity.getCreateDate() == null) {
			entity.setCreateDate(dto.getCreateDate());
			entity.setCreateUser(dto.getCreateUser());			
			entity.setRequestDate(dto.getRequestDate());
		}
		entity.setUpdateDate(dto.getUpdateDate());
		entity.setUpdateUser(dto.getUpdateUser());
				
		entity.setWsInputRequested(dto.isWsInputRequested());
	}
	
	
	public static NameDescriptionDTO transformEntityToDTO(ReasonEntity entity) {
		NameDescriptionDTO dto = new NameDescriptionDTO();
		dto.setDescription(entity.getDescription());
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		
		return dto;
	}

	public static NameDescriptionDTO transformEntityToDTO(RejectReasonEntity entity) {
		NameDescriptionDTO dto = new NameDescriptionDTO();
		dto.setDescription(entity.getDescription());
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		
		return dto;
	}

	public static ReleaseEnrolmentDTO transformEntityToDTO(ReleaseEnrolmentEntity entity, Long releaseId) {
		ReleaseEnrolmentDTO dto = new ReleaseEnrolmentDTO();
		dto.setAnalystApprovalDateTime(entity.getAnalystApprovalDate());
		dto.setAnalystApprovalStatus(entity.getAnalystApprovalStatus());
		dto.setAnalystApprovedBy(entity.getAnalystApprovalBy());
		dto.setAnalystApprovedDate(entity.getAnalystApprovalDate());
		dto.setAuthorizedAmount(entity.getAuthorizedAmount());
		dto.setFullRelease(entity.getFullRelease());
		dto.setId(entity.getId());
		if(entity.getRejectReason() != null) {
			dto.setRejectReason(entity.getRejectReason().getId());
		}
		dto.setReleaseId(releaseId);
		dto.setRequestAmount(entity.getRequestedAmount());
		dto.setVersion(entity.getVersion());
		return dto;
	}
	
	
	public static ReleaseDTO transformEntityToDTO(ReleaseEntity entity) {
		ReleaseDTO dto = new ReleaseDTO();
		dto.setComment(entity.getComment());
		dto.setCreateDate(entity.getCreateDate());
		dto.setCreateUser(entity.getCreateUser());
		dto.setFcmAmountRetained(entity.getFcmAmountRetained());
		dto.setFinalApprovalDate(entity.getFinalApprovalDate());
		dto.setFinalApprovalRequestor(entity.getAnalystApprovalBy());
		dto.setFinalApprovalSentTo(entity.getFinalApprovalSentTo());
		dto.setFinalApprovalStatus(entity.getFinalApprovalStatus());
		dto.setFinalApprovedBy(entity.getFinalApprovalBy());
		dto.setId(entity.getId());
		dto.setInterestAmount(entity.getInterestAmount());
		dto.setPrincipalAmount(entity.getPrincipalAmount());
		if(entity.getReplacedBySecurity() != null) {
			ReplacedByDTO replacedBy = new ReplacedByDTO();
			replacedBy.setSecurityId(entity.getReplacedBySecurity().getId());
			replacedBy.setInstrumentNumber(entity.getReplacedBySecurity().getInstrumentNumber());
			dto.setReplacedBy(replacedBy);
		}
		dto.setAnalystApprovalDateTime(entity.getAnalystApprovalDate());
		dto.setRecommendedRejectedBy(entity.getAnalystApprovalBy());
		dto.setReferenceNumber(entity.getReferenceNumber());
		dto.setRegularRelease(entity.isRegularRelease());
		dto.setRequestDate(entity.getCreateDate());
		dto.setSecurityId(entity.getSecurity().getId());
		dto.setUpdateDate(entity.getUpdateDate());
		dto.setUpdateUser(entity.getUpdateUser());
		dto.setVersion(entity.getVersion());
		dto.setWsInputProvidedBy(entity.getWsInputProvidedBy());
		dto.setWsInputRequested(entity.isWsInputRequested());		
		dto.setWsRecievedDate(entity.getWsReceivedDate());
		if (entity.getWsRecommendation() != null) {
			NameDescriptionDTO wsRecommendation = new NameDescriptionDTO();
			wsRecommendation.setDescription(entity.getWsRecommendation().getDescription());
			wsRecommendation.setId(entity.getId());
			wsRecommendation.setName(entity.getWsRecommendation().getName());
			dto.setWsRecommendation(wsRecommendation);
		}
		dto.setWsRequestDate(entity.getWsRequestedDate());
		dto.setWsRequestor(entity.getWsRequestor());
		dto.setAuthorizedReleaseAmount(entity.getAuthorizedAmount());
		dto.setRequestedAmount(entity.getRequestAmount());
		dto.setAnalystApprovedBy(entity.getAnalystApprovalBy());
		dto.setAnalystApprovalStatus(entity.getAnalystApprovalStatus());
		dto.setStatus(entity.getStatus());
		dto.setServiceOrderId(entity.getServiceOrderId());
		dto.setChequeDate(entity.getChequeDate());
		dto.setChequeNumber(entity.getChequeNumber());
		dto.setChequeStatus(entity.getChequeStatus());
		dto.setReplacementChequeDate(entity.getReplacementChequeDate());
		dto.setReplacementChequeNumber(entity.getReplacementChequeNumber());
		dto.setVoucherId(entity.getVoucherId());
		dto.setSettlementInstructions(entity.getSettlementInstructions());
		dto.setUnwindProRata(entity.getUnwindProRata());
		dto.setReplacementChequeStatus(entity.getReplacementChequeStatus());
		return dto;
	}	

	public static PendingReleaseDTO transformEntityToDTO(PendingReleaseDTO dto, ReleaseEntity entity) {
		SecurityEntity security = entity.getSecurity();
		dto.setReleaseId(entity.getId());
		dto.setSecurityId(security.getId());
		dto.setComment(entity.getComment());

		if(security.getSecurityType().getId().equals(VbsConstants.SECURITY_TYPE_CASH) && entity.getReleaseType().getId().equals(VbsConstants.RELEASE_TYPE_RELEASE) && entity.isRegularRelease()){
			dto.setInterestAmount(entity.getPrincipalAmount().divide(security.getCurrentAmount(), 5, VbsConstants.ROUNDING_MODE).multiply(security.getCurrentInterest()).setScale(VbsConstants.DECIMAL_SCALE, VbsConstants.ROUNDING_MODE));
		}else{
			dto.setInterestAmount(entity.getInterestAmount());
		}

		dto.setPrincipalAmount(entity.getPrincipalAmount());
		dto.setReferenceNumber(entity.getReferenceNumber());
		dto.setApprovalManager(entity.getFinalApprovalBy());
		dto.setCurrentAmount(security.getCurrentAmount());
		dto.setInterestAmount(security.getCurrentInterest());
		dto.setInstrumentNumber(security.getInstrumentNumber());
		dto.setManagerApprovalDate(entity.getFinalApprovalDate());
		dto.setOriginalAmount(security.getOriginalAmount());
		dto.setReferenceNumber(entity.getReferenceNumber());
		if(security.getCurrentAmount() != null && security.getCurrentInterest() != null) {
			dto.setInterestCurrentAmountTotal(security.getCurrentAmount().add(security.getCurrentInterest()));
		} else {
			dto.setInterestCurrentAmountTotal(security.getCurrentAmount());
		}
		dto.setAuthorizedAmount(entity.getAuthorizedAmount());
		if(security.getCurrentAmount() != null && security.getCurrentAmount().compareTo(BigDecimal.ZERO) != 0) {
			dto.setReleasePercentage(entity.getAuthorizedAmount().divide(security.getCurrentAmount(), 20, VbsConstants.ROUNDING_MODE));
		}
		LocalDateTime dateNovember2004 = LocalDateTime.of(2004, 11, 1, 00, 00);
		dto.setReceivedPriorNov2004(security.getReceivedDate().isBefore(dateNovember2004) && security.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_CASH) == 0);
		return dto;
	}
	
	public static ReleaseTypeDTO transformEntityToDTO(ReleaseTypeEntity entity) {
		ReleaseTypeDTO dto = new ReleaseTypeDTO();
		dto.setDescription(entity.getDescription());
		dto.setId(entity.getId());
		dto.setName(entity.getName());

		return dto;
	}

}
