package com.tarion.vbs.service.financialinstitution;

import javax.ejb.Local;

/**
 * Financial Institution Activities Service provides operations for Financial Institution Activities
 *
 * @author <A href="joni.paananen@tarion.com">Joni Paananen</A>
 */
@Local
public interface FinancialInstitutionActivitiesService extends FinancialInstitutionActivitiesServiceRemote {
}
