package com.tarion.vbs.service.alert;

import com.tarion.vbs.common.dto.alert.AlertDTO;
import com.tarion.vbs.orm.entity.alert.AlertEntity;
import com.tarion.vbs.orm.entity.alert.AlertTypeEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;

public class AlertServiceTransformer {
	
	private AlertServiceTransformer() {}
	
	public static AlertDTO transformEntityToDTO(AlertEntity entity, Long securityId) {
		AlertDTO alertDTO = new AlertDTO();
		if (entity == null) {
			return alertDTO;
		}
		alertDTO.setId(entity.getId());
		alertDTO.setVersion(entity.getVersion());
		alertDTO.setSecurityId(securityId);
		alertDTO.setAlertTypeId(entity.getAlertType().getId());
		alertDTO.setName(entity.getName());
		alertDTO.setDescription(entity.getDescription());
		alertDTO.setCreateDate(entity.getCreateDate());
		alertDTO.setCreateUser(entity.getCreateUser());
		alertDTO.setUpdateDate(entity.getUpdateDate());
		alertDTO.setUpdateUser(entity.getUpdateUser());
		alertDTO.setStartDate(entity.getStartDate());
		alertDTO.setEndDate(entity.getEndDate());
		alertDTO.setStatus(entity.getStatus());
		
		return alertDTO;
	}

	public static AlertEntity transformDTOToEntity(AlertDTO dto, AlertTypeEntity alertType, SecurityEntity securityEntity) {
		AlertEntity alertEntity = new AlertEntity();
		alertEntity.setId(dto.getId());
		alertEntity.setVersion(dto.getVersion());
		alertEntity.setSecurity(securityEntity);
		alertEntity.setAlertType(alertType);
		alertEntity.setName(dto.getName());
		alertEntity.setDescription(dto.getDescription());
		alertEntity.setCreateDate(dto.getCreateDate());
		alertEntity.setCreateUser(dto.getCreateUser());
		alertEntity.setUpdateDate(dto.getUpdateDate());
		alertEntity.setUpdateUser(dto.getUpdateUser());
		alertEntity.setStartDate(dto.getStartDate());
		alertEntity.setEndDate(dto.getEndDate());
		return alertEntity;
	}
}
