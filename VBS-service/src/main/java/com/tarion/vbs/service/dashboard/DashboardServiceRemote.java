package com.tarion.vbs.service.dashboard;

import java.util.List;

import javax.ejb.Remote;

import com.tarion.vbs.common.dto.dashboard.ManagerReleaseDTO;
import com.tarion.vbs.common.dto.dashboard.NewBlanketCEDTO;

@Remote
public interface DashboardServiceRemote {

	public List<ManagerReleaseDTO> getReleasesForManagerUserId(String userId);

    List<NewBlanketCEDTO> getNewBlanketCEs();
}
