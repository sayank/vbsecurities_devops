package com.tarion.vbs.service.release;

import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentDTO;
import com.tarion.vbs.common.dto.release.PendingReleaseDTO;
import com.tarion.vbs.common.dto.release.ReleaseDTO;
import com.tarion.vbs.common.dto.release.ReleaseSearchParametersDTO;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.enums.ReleaseStatus;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.orm.entity.release.ReleaseEntity;

import javax.ejb.Remote;
import java.time.LocalDateTime;
import java.util.List;

@Remote
public interface ReleaseServiceRemote {

	List<ReleaseDTO> getReleases(Long securityId) throws VbsCheckedException;

	Long saveRelease(ReleaseDTO releaseDTO, UserDTO userDTO) throws VbsCheckedException;

	//TODO review
	List<EnrolmentDTO> getUnreleasedEnrolmentsForSecurity(Long securityId);

	//TODO review
	Long cancelRelease(ReleaseDTO release, UserDTO userDTO) throws VbsCheckedException;

	//TODO review
	List<PendingReleaseDTO> getReleaseListByStatus(ReleaseStatus releaseStatus) throws VbsCheckedException;

	//TODO review
	List<PendingReleaseDTO> getReleaseListByStatuses(ReleaseStatus releaseStatus1, ReleaseStatus releaseStatus2) throws VbsCheckedException;

	//TODO review
	List<PendingReleaseDTO> searchReleases(ReleaseSearchParametersDTO params);

	//TODO review
	List<PendingReleaseDTO> sendReleaseListToFms(List<PendingReleaseDTO> pendingReleaseDTOList, UserDTO user) throws VbsCheckedException;

	//TODO review
	ReleaseDTO setupRelease(Long releaseTypeId, Long securityId, UserDTO userDTO);

	//TODO review
	List<PendingReleaseDTO> getReleasesByStatusesFromToDate(List<ReleaseStatus> releaseStatuses, LocalDateTime startDate, LocalDateTime endDate) throws VbsCheckedException;

    List<ErrorDTO> validateRelease(ReleaseDTO release, UserDTO user);


	ReleaseDTO getReleaseDTO(Long releaseId);

    void sendReleaseApprovalWorklistToCrm(ReleaseEntity releaseEntity, String userId);
	List<ErrorDTO> validateSendReleaseListToFms(List<PendingReleaseDTO> pendingReleaseDTOList);
}
