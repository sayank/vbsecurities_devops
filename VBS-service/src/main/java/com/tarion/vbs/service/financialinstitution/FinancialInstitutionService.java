/* 
 * 
 * SecurityServiceRemote.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.financialinstitution;

import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionMaaPoaDTO;

import javax.ejb.Local;
import javax.ejb.Remote;
import java.math.BigDecimal;

/**
 * Security Service provides operations for Securities tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-15
 * @version 1.0
 */
@Local
public interface FinancialInstitutionService extends FinancialInstitutionServiceRemote {

  FinancialInstitutionMaaPoaDTO getMaaPoa(Long maaPoaId, BigDecimal originalAmount, BigDecimal currentAmount);

}
