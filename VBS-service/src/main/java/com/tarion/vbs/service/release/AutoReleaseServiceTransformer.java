/* 
 * 
 * AutoReleaseServiceTransformer.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.release;

import com.tarion.vbs.common.dto.autorelease.AutoReleaseRunSearchResultDTO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.autorelease.AutoReleaseRunEntity;

import javax.interceptor.Interceptors;


/**
 * Transformer helper class used by Auto Release Service to provide transformation between DTO and Entity
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2019-04-05
 * @version 1.0
 */
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class AutoReleaseServiceTransformer {
	
	private AutoReleaseServiceTransformer() {}

	public static AutoReleaseRunSearchResultDTO transformEntityToDTO(AutoReleaseRunEntity entity, Long statusId) {
		AutoReleaseRunSearchResultDTO dto = new AutoReleaseRunSearchResultDTO();
		dto.setCreateDate(entity.getCreateDate());
		dto.setCreateUser(entity.getCreateUser());
		dto.setId(entity.getId());
		dto.setRunStatusId(statusId);
		dto.setUpdateDate(entity.getUpdateDate());
		dto.setUpdateUser(entity.getUpdateUser());
		dto.setVersion(entity.getVersion());

		return dto;
	}
}
