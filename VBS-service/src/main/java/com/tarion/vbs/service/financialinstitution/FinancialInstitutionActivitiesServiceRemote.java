/* 
 * 
 * FinancialInstitutionService.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.financialinstitution;

import com.tarion.vbs.common.dto.financialinstitution.FIActivityDTO;

import javax.ejb.Remote;
import java.util.List;

/**
 * Financial Institution Activities Service provides operations for Financial Institution Activities
 *
 * @author <A href="joni.paananen@tarion.com">Joni Paananen</A>
 */
@Remote
public interface FinancialInstitutionActivitiesServiceRemote {
	
	List<FIActivityDTO> getActivitiesForFinancialInstitution(Long financialInstitutionId);

}
