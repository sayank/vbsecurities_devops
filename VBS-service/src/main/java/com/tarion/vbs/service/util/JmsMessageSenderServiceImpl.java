/* 
 * 
 * JmsMessageSenderServiceImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import com.tarion.vbs.common.constants.JMSConstants;
import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.orm.entity.InterestTransactionLogEntity;
import com.tarion.vbs.orm.entity.JmsTransactionLogEntity;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;
import javax.jms.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.time.LocalDateTime;

/**
 * This class is used to post a message to a CRM JMS queue
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-11-20
 * @version 1.0
 */
@Singleton(mappedName = "ejb/JmsMessageSenderService")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class JmsMessageSenderServiceImpl implements JmsMessageSenderService {

	@EJB
    private PropertyService propertyService;
	@EJB
	private GenericDAO genericDAO;
	
	@Override
	public void postFmsRequestQueue(String xmlMessageText, String requestClassName) {
		send(JMSConstants.VBS_XA_CF_JNDI, JMSConstants.FMS_REQUEST_QUEUE_NAME, xmlMessageText, requestClassName);
	}
	
	@Override
	@ExcludeClassInterceptors
	public void postFmsRequestQueueInterestMessage(String xmlMessageText, String requestClassName) {
		send(JMSConstants.VBS_XA_CF_JNDI, JMSConstants.FMS_REQUEST_QUEUE_NAME, xmlMessageText, requestClassName);
	}

	private void send(String connectionFactoryName, String queueJndiName, String xmlMessageText, String requestClassName) {
		try {
			sendTxtMessageToQueue(queueJndiName, xmlMessageText, null, true, connectionFactoryName, requestClassName);
		}catch(NamingException ne) {
			throw new VbsRuntimeException(ne.getMessage());
		}catch (JMSException je) {
			String errMsg = "sendTxtMessageToQueue for queue=[" + queueJndiName + "] failed: " + je.toString();
			throw new VbsRuntimeException(errMsg, je);
		}
	}

	/**
	 * Send text message to CRM
	 * 
	 * @param queueJndiName
	 *            The JNDI name of the queue
	 * @param msg
	 *            The message to send
	 * @param trackingNumber
	 *            Tracking number
	 * @throws VbsRuntimeException
	 *             Problem sending the message
	 */
	private void sendTxtMessageToQueue(String queueJndiName, String msg, String trackingNumber, boolean setHeader, String connectionFactoryName, String requestClassName) throws NamingException, JMSException {
		InitialContext ic = new InitialContext();
		String jmsPrefix = propertyService.getValueOrNull(JMSConstants.TEST_JMS_PREFIX);
		if(jmsPrefix == null){jmsPrefix = "";}

		ConnectionFactory connectionFactory = (ConnectionFactory) ic.lookup(jmsPrefix + connectionFactoryName);
		Queue theQueue = (Queue) ic.lookup(jmsPrefix + queueJndiName);
		try (
			Connection connection = connectionFactory.createConnection();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageProducer producer = session.createProducer(theQueue);
		) {
			TextMessage textMessage = session.createTextMessage();
			if (setHeader) {
				setHeader(textMessage, queueJndiName, requestClassName);
			}
			textMessage.setText(msg);
			// use the tracking number as the correlation id
			textMessage.setJMSCorrelationID(trackingNumber);

			producer.send(textMessage);
		}
	}

	/**
	 * Set the CRM Header information for create case
	 * 
	 * @param txtMsg
	 *            Current TextMessage
	 * @param queueJndiName
	 *            JNDI name of the queue
	 * @throws JMSException
	 *             Problem setting the property
	 * @throws VbsRuntimeException
	 *             Problem with the properties file
	 */
	private void setHeader(TextMessage txtMsg, String queueJndiName, String requestClassName) throws JMSException {
//		# MessageName and RequestingNode need to match with CRM/FMS IB Routing
//		# as well DestinationNode and FinalDestinationNode which are read from DB
//		# all others are not important
		if (JMSConstants.FMS_REQUEST_QUEUE_NAME.equalsIgnoreCase(queueJndiName)) {
			txtMsg.setStringProperty(JMSConstants.JMS_PROVIDER, propertyService.getValue("FmsRequestQueue.JMSProvider"));
			txtMsg.setStringProperty(JMSConstants.FINAL_DESTINATION_NODE, propertyService.getValue(JMSConstants.FMS_JMS_DESTINATION));
			txtMsg.setStringProperty(JMSConstants.DESTINATION_NODE, propertyService.getValue(JMSConstants.FMS_JMS_DESTINATION));
			txtMsg.setStringProperty(JMSConstants.JMS_PROVIDER, propertyService.getValue("FmsRequestQueue.JMSProvider"));
			txtMsg.setStringProperty(JMSConstants.MESSAGE_TYPE, propertyService.getValue("FmsRequestQueue.MessageType"));
			txtMsg.setStringProperty(JMSConstants.VERSION, propertyService.getValue("FmsRequestQueue.version"));
			txtMsg.setStringProperty(JMSConstants.JMS_MESSAGE_TYPE, propertyService.getValue("FmsRequestQueue.JMSMessageType"));
			txtMsg.setStringProperty(JMSConstants.CRED, propertyService.getValue("FmsRequestQueue.Password"));
			setHeaderMessageNameRequestingNode(txtMsg, "FmsRequestQueue", requestClassName);
		} else if (JMSConstants.CRM_REQUEST_QUEUE_NAME.equalsIgnoreCase(queueJndiName)) {
			txtMsg.setStringProperty(JMSConstants.JMS_PROVIDER, propertyService.getValue("CrmRequestQueue.JMSProvider"));
			txtMsg.setStringProperty(JMSConstants.FINAL_DESTINATION_NODE, propertyService.getValue(JMSConstants.CRM_JMS_DESTINATION));
			txtMsg.setStringProperty(JMSConstants.DESTINATION_NODE, propertyService.getValue(JMSConstants.CRM_JMS_DESTINATION));
			txtMsg.setStringProperty(JMSConstants.JMS_PROVIDER, propertyService.getValue("CrmRequestQueue.JMSProvider"));
			txtMsg.setStringProperty(JMSConstants.MESSAGE_TYPE, propertyService.getValue("CrmRequestQueue.MessageType"));
			txtMsg.setStringProperty(JMSConstants.VERSION, propertyService.getValue("CrmRequestQueue.version"));
			txtMsg.setStringProperty(JMSConstants.JMS_MESSAGE_TYPE, propertyService.getValue("CrmRequestQueue.JMSMessageType"));
			txtMsg.setStringProperty(JMSConstants.CRED, propertyService.getValue("CrmRequestQueue.Password"));
			// CRM Needs all messages to have same message name
			setHeaderMessageNameRequestingNode(txtMsg, "CrmRequestQueue", null);
		}
	}
	
	private void setHeaderMessageNameRequestingNode(TextMessage txtMsg, String prefix, String suffix) throws JMSException {
		if (suffix != null) {
			txtMsg.setStringProperty(JMSConstants.MESSAGE_NAME, propertyService.getValue(prefix + "." + suffix + "." + "MessageName"));
		} else {
			txtMsg.setStringProperty(JMSConstants.MESSAGE_NAME, propertyService.getValue(prefix + "." + "MessageName"));
		}
		// there is only one requesting node
		txtMsg.setStringProperty(JMSConstants.REQUESTING_NODE, propertyService.getValue(prefix + "." + "RequestingNode"));
	}

	@Override
	public JmsTransactionLogEntity insertTransactionLog(String trackingNumber, String xmlMessageText) {
		return insertTransactionLog(trackingNumber, xmlMessageText, null);
	}

	@Override
	public JmsTransactionLogEntity insertTransactionLog(String trackingNumber, String requestMessageText, String responseMessageText) {
		LocalDateTime time = LocalDateTime.now();
		JmsTransactionLogEntity entity = new JmsTransactionLogEntity();
		entity.setTrackingNumber(trackingNumber);
		entity.setVersion(1l);
		entity.setMessageName(trackingNumber);
		entity.setRequest(requestMessageText);
		entity.setRequestSendDate(time);
		if(responseMessageText != null){
			entity.setResponse(responseMessageText);
			entity.setRequestSendDate(time);
		}
		return (JmsTransactionLogEntity)genericDAO.addEntity(entity, VbsConstants.VBS_FMS_SYSTEM_USER_ID, entity.getRequestSendDate());
	}

	@Override
	@ExcludeClassInterceptors
	public void insertInterestTransactionLog(String trackingNumber, String xmlMessageText) {
		InterestTransactionLogEntity entity = new InterestTransactionLogEntity();
		entity.setTrackingNumber(trackingNumber);
		entity.setVersion(1l);
		entity.setMessageName(trackingNumber);
		entity.setRequest(xmlMessageText);
		entity.setRequestSendDate(DateUtil.getLocalDateTimeNow());
		genericDAO.addEntity(entity, VbsConstants.VBS_FMS_SYSTEM_USER_ID, entity.getRequestSendDate());
	}

	@Override
	public void postCrmRequestQueue(String xmlMessageText, String requestClassName) {
		send(JMSConstants.VBS_XA_CF_JNDI, JMSConstants.CRM_REQUEST_QUEUE_NAME, xmlMessageText, requestClassName);
	}
	@Override
	public void postBsaResponseQueue(String xmlMessageText) {
		try {
			sendTxtMessageToQueue(JMSConstants.BSA_RESPONSE_QUEUE_NAME, xmlMessageText, null, false, JMSConstants.VBS_XA_CF_JNDI, null);
		}catch(NamingException ne) {
			throw new VbsRuntimeException(ne.getMessage());
		}catch (JMSException je) {
			String errMsg = "sendTxtMessageToQueue for queue=[" + JMSConstants.BSA_RESPONSE_QUEUE_NAME + "] failed: " + je.toString();
			throw new VbsRuntimeException(errMsg, je);
		}
	}

	@Override
	public void postTipCorrespondenceMessage(String xmlMessageText){
		try {
			sendTxtMessageToQueue(JMSConstants.TIP_GENERATE_CORRESPONDENCE_QUEUE_NAME, xmlMessageText, null, false, JMSConstants.VBS_XA_CF_JNDI, null);
		}catch(NamingException ne) {
			throw new VbsRuntimeException(ne.getMessage());
		}catch (JMSException je) {
			String errMsg = "sendTxtMessageToQueue for queue=[" + JMSConstants.TIP_GENERATE_CORRESPONDENCE_QUEUE_NAME + "] failed: " + je.toString();
			throw new VbsRuntimeException(errMsg, je);
		}
	}
}
