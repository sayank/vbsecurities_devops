package com.tarion.vbs.service.util;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.autorelease.AutoReleaseResultDTO;
import com.tarion.vbs.common.dto.autorelease.AutoReleaseResultWrapper;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionMaaPoaDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionMaaPoaInstitutionDTO;
import com.tarion.vbs.common.dto.interest.InterestSearchResultDTO;
import com.tarion.vbs.common.dto.interest.SecurityInterestSearchParamsDTO;
import com.tarion.vbs.common.dto.release.*;
import com.tarion.vbs.common.dto.security.CashSecurityNoInterestDTO;
import com.tarion.vbs.common.dto.security.DeletedSecuritySearchParametersDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchParametersDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchResultDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchResultWrapper;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.enums.ReleaseApprovalStatusEnum;
import com.tarion.vbs.common.enums.SearchTypeEnum;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.financialinstitution.FinancialInstitutionDAO;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import com.tarion.vbs.service.financialinstitution.FinancialInstitutionService;
import com.tarion.vbs.service.interest.InterestService;
import com.tarion.vbs.service.release.AutoReleaseService;
import com.tarion.vbs.service.release.ReleaseService;
import com.tarion.vbs.service.security.SecurityService;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.*;

@Stateless(mappedName = "ejb/ExcelService")
public class ExcelServiceImpl implements ExcelService, ExcelServiceRemote {

	@EJB
	private SecurityService securityService;
	@EJB
	private FinancialInstitutionService financialInstitutionService;
	@EJB
	private AutoReleaseService autoReleaseService;
	@EJB
	private ReleaseService releaseService;
	@EJB
	private InterestService interestService;
	@EJB
    private FinancialInstitutionDAO financialInstitutionDAO;
    @EJB
    private GenericDAO genericDAO;
    @EJB
    private PropertyService propertyService;

	@Override
	public byte[] createSearchResultsExcel(SecuritySearchParametersDTO params, boolean showInterest, UserDTO userDTO) throws VbsCheckedException {
		SecuritySearchResultWrapper results = securityService.searchSecurity(params, userDTO);
		return createSearchResultsExcel(params, results, showInterest);
	}

	@Override
	public byte[] createSearchResultsExcel(SecuritySearchParametersDTO params, SecuritySearchResultWrapper results, boolean showInterest) throws VbsCheckedException {
		long startTime = LoggerUtil.logEnter(ExcelServiceImpl.class, "createSearchResultsExcel", params, showInterest);
		List<SecuritySearchResultDTO> securities = results.getSecurities();
		
		int rowNum = 0;
		SXSSFWorkbook wb = new SXSSFWorkbook();
        wb.setCompressTempFiles(true);
        SXSSFSheet sheet = wb.createSheet("Security Search Results");

        CellStyle headerStyle = getHeaderStyle(wb);
        CellStyle rowStyle = getRowStyle(wb);
        CellStyle currencyStyle = getCurrencyStyle(wb);
        CellStyle boldStyle = getBoldStyle(wb);
        CellStyle percentageStyle = getPercentageFormat(wb);
        CellStyle currencyNoBorderStyle = getCurrencyStyleNoBorder(wb);
        CellStyle wordWrapStyle = getWordWrapStyle(wb);

        if(!VbsUtil.isNullorEmpty(params.getVbNumber()) && params.getVbNumberType() == SearchTypeEnum.EQUALS) {
            /* VB Header */
            Row vbRow1 = sheet.createRow(rowNum);
            Cell cellr1c0 = vbRow1.createCell(0); cellr1c0.setCellValue("V/B #"); cellr1c0.setCellStyle(boldStyle);
            Cell cellr1c2 = vbRow1.createCell(2); cellr1c2.setCellValue("B"+securities.get(0).getVbNumber());
            addMergeRegion(rowNum, sheet, 0, 1);
            rowNum++;
        }

        Row vbRow2 = sheet.createRow(rowNum);
        Cell cellr2c0 = vbRow2.createCell(0); cellr2c0.setCellValue("Total Original Amount:"); cellr2c0.setCellStyle(boldStyle);
        Cell cellr2c2 = vbRow2.createCell(2); cellr2c2.setCellValue(results.getTotals().getTotalOriginalAmount().doubleValue());cellr2c2.setCellStyle(currencyNoBorderStyle);
        Cell cellr2c4 = vbRow2.createCell(4); cellr2c4.setCellValue("Total Current Amount:"); cellr2c4.setCellStyle(boldStyle);
        Cell cellr2c6 = vbRow2.createCell(6); cellr2c6.setCellValue(results.getTotals().getTotalCurrentAmount().doubleValue());cellr2c6.setCellStyle(currencyNoBorderStyle);
        Cell cellr2c7 = vbRow2.createCell(7); cellr2c7.setCellValue("Total Cash Amount (No Int):"); cellr2c7.setCellStyle(boldStyle);
        Cell cellr2c8 = vbRow2.createCell(8); cellr2c8.setCellValue(results.getTotals().getTotalCashAmount().doubleValue());cellr2c8.setCellStyle(currencyNoBorderStyle);
        addMergeRegion(rowNum, sheet, 0, 1, 4, 5, 8, 9);
        rowNum++;

        Row vbRow3 = sheet.createRow(rowNum);
        Cell cellr3c0 = vbRow3.createCell(0); cellr3c0.setCellValue("Original Amount Excluding DTA/PEF:"); cellr3c0.setCellStyle(boldStyle);
        Cell cellr3c2 = vbRow3.createCell(2); cellr3c2.setCellValue(results.getTotals().getTotalOriginalAmountExcludingDtaPef().doubleValue());cellr3c2.setCellStyle(currencyNoBorderStyle);
        Cell cellr3c4 = vbRow3.createCell(4); cellr3c4.setCellValue("Current Amount Excluding DTA/PEF:"); cellr3c4.setCellStyle(boldStyle);
        Cell cellr3c6 = vbRow3.createCell(6); cellr3c6.setCellValue(results.getTotals().getTotalCurrentAmountExcludingDtaPef().doubleValue());cellr3c6.setCellStyle(currencyNoBorderStyle);
        addMergeRegion(rowNum, sheet, 0, 1, 4, 5);
        rowNum++;

        if(showInterest) {
            Row vbRow4 = sheet.createRow(rowNum);
            Cell cellr4c0 = vbRow4.createCell(0); cellr4c0.setCellValue("Total Current Interest Amount:"); cellr4c0.setCellStyle(boldStyle);
            Cell cellr4c2 = vbRow4.createCell(2); cellr4c2.setCellValue(results.getTotals().getTotalCurrentInterestAmount().doubleValue());cellr4c2.setCellStyle(currencyNoBorderStyle);
            Cell cellr4c7 = vbRow4.createCell(7); cellr4c7.setCellValue("Total Available Cash Amount (Inc Int):"); cellr4c7.setCellStyle(boldStyle);
            Cell cellr4c8 = vbRow4.createCell(8); cellr4c8.setCellValue(results.getTotals().getTotalAvailableCashAmount().doubleValue()); cellr4c8.setCellStyle(currencyNoBorderStyle);
            addMergeRegion(rowNum, sheet, 0, 1, 8, 9);
            rowNum++;
        }

        rowNum++;

        /* Header */
        Row header = sheet.createRow(rowNum);
        header.createCell(0).setCellValue("Security Number");
        header.createCell(1).setCellValue("Instrument Number");
        header.createCell(2).setCellValue("Received Date");
        header.createCell(3).setCellValue("Pool Id");
        header.createCell(4).setCellValue("JBA");
        header.createCell(5).setCellValue("Security Type Name");
        header.createCell(6).setCellValue("VB Number");
        header.createCell(7).setCellValue("VB Name");
        header.createCell(8).setCellValue("Multi VB");
        header.createCell(9).setCellValue("Security Purpose");
        header.createCell(10).setCellValue("Identity Type");
        header.createCell(11).setCellValue("Original Amount");
        header.createCell(12).setCellValue("Current Amount");
        if(showInterest) {
            header.createCell(13).setCellValue("Current Interest Amount");
            header.createCell(14).setCellValue("Total Available Cash Amount (Inc Interest)");
            header.createCell(15).setCellValue("Enrolment Numbers");
            header.createCell(16).setCellValue("DTA Status");
            header.createCell(17).setCellValue("DTA Account Number");
        }else {
            header.createCell(13).setCellValue("Enrolment Numbers");
            header.createCell(14).setCellValue("DTA Status");
            header.createCell(15).setCellValue("DTA Account Number");
        }

        header.forEach(c -> c.setCellStyle(headerStyle));
        rowNum++;
        /* End Header */

        Long financialInstType = 0L;
        if(params.getFinancialInstitutionId() != null) {
            financialInstType = financialInstitutionDAO.getFinancialInstitutionTypeId(params.getFinancialInstitutionId());
        }

        Map<Integer, Integer> counts = new HashMap<>();
        counts.put(0, 17);
        counts.put(1, 20);
        counts.put(2, 15);
        counts.put(3, 7);
        counts.put(4, 4);
        counts.put(5, 25);
        counts.put(6, 15);
        counts.put(8, 9);
        counts.put(10, 12);
        counts.put(11, 16);
        counts.put(12, 16);
        if(showInterest) {
            counts.put(13, 25);
            counts.put(14, 30);
            counts.put(15, 15);
            counts.put(16, 16);
            counts.put(17, 20);
        }else{
            counts.put(13, 15);
            counts.put(14, 16);
            counts.put(15, 20);
        }

        int startRow = (!VbsUtil.isNullorEmpty(params.getVbNumber()) && params.getVbNumberType() == SearchTypeEnum.EQUALS) ? 1 : 0;
        int endRow = (showInterest) ? startRow + 2 : startRow + 1;
        for(int i=startRow;i<=endRow;i++){
            int c2length = Double.toString(sheet.getRow(i).getCell(2).getNumericCellValue()).length();
            int c6length = (sheet.getRow(i).getCell(6) != null) ? Double.toString(sheet.getRow(i).getCell(6).getNumericCellValue()).length() : 0;
            if(counts.get(2) == null || counts.get(2) < c2length){
                counts.put(2, c2length);
            }
            if(counts.get(6) == null || counts.get(6) < c6length){
                counts.put(6, c6length);
            }
        }

        for (SecuritySearchResultDTO security : securities) {
            BigDecimal originalAmount = BigDecimal.ZERO;
            BigDecimal currentAmount = BigDecimal.ZERO;
            if(security.getOriginalAmount() != null) {
                originalAmount = security.getOriginalAmount();
            }
            if(security.getCurrentAmount() != null) {
                currentAmount = security.getCurrentAmount();
            }

            FinancialInstitutionMaaPoaDTO maaPoaDetails = null;
            if(params.getFinancialInstitutionId() != null && security.getMaaPoaId() !=null) {
                maaPoaDetails = financialInstitutionService.getMaaPoa(security.getSecurityNumber().longValue());
                if(maaPoaDetails != null && maaPoaDetails.getMaaPoaInstitutions() != null && !maaPoaDetails.getMaaPoaInstitutions().isEmpty()) {
                    Optional<FinancialInstitutionMaaPoaInstitutionDTO> maaPoaInstitution = maaPoaDetails.getMaaPoaInstitutions()
                        .stream().filter(f -> f.getFinancialInstitution() != null && f.getFinancialInstitution().getId().equals(params.getFinancialInstitutionId())).findFirst();
                    if(maaPoaInstitution.isPresent()) {
                        if(maaPoaInstitution.get().getOriginalAmountOnInstitution() != null) {
                            originalAmount = maaPoaInstitution.get().getOriginalAmountOnInstitution();
                        }
                        if(maaPoaInstitution.get().getCurrentAmountOnInstitution() != null) {
                            currentAmount = maaPoaInstitution.get().getCurrentAmountOnInstitution();
                        }
                    }
                }
            }

            Row row = sheet.createRow(rowNum);
            List<CellConfig> configs = new ArrayList<>();
            configs.add(new CellConfig(0, security.getSecurityNumber(), rowStyle));
            configs.add(new CellConfig(1, security.getInstrumentNumber(), rowStyle));
            configs.add(new CellConfig(2, DateUtil.getDateFormattedShort(security.getReceivedDate()), rowStyle));
            configs.add(new CellConfig(3, security.getPoolId(), rowStyle));
            configs.add(new CellConfig(4, security.getJba(), rowStyle));
            configs.add(new CellConfig(5, security.getSecurityTypeName(), rowStyle));
            configs.add(new CellConfig(6, (security.getVbNumber() != null) ? "B" + security.getVbNumber() : "", rowStyle));
            configs.add(new CellConfig(7, (security.getVbName() != null) ? security.getVbName() : "", rowStyle));
            configs.add(new CellConfig(8, security.getMultiVb(), rowStyle));
            configs.add(new CellConfig(9, security.getSecurityPurposeName(), rowStyle));
            configs.add(new CellConfig(10, security.getIdentityTypeName(), rowStyle));
            configs.add(new CellConfig(11, originalAmount.doubleValue(), currencyStyle));
            configs.add(new CellConfig(12, currentAmount.doubleValue(), currencyStyle));
            if (showInterest) {
                configs.add(new CellConfig(13, (security.getCurrentInterestAmount() != null) ? security.getCurrentInterestAmount().doubleValue() : 0d, currencyStyle));
                configs.add(new CellConfig(14, (security.getTotalAvailableAmount() != null) ? security.getTotalAvailableAmount().doubleValue() : 0d, currencyStyle));
                configs.add(new CellConfig(15, security.serializeEnrolments(), wordWrapStyle));
                configs.add(new CellConfig(16, security.getDtaStatus() != null ? security.getDtaStatus() : "", rowStyle));
                configs.add(new CellConfig(17, security.getDtaAccountNumber() != null ? security.getDtaAccountNumber() : "", rowStyle));
            }else{
                configs.add(new CellConfig(13, security.serializeEnrolments(), wordWrapStyle));
                configs.add(new CellConfig(14, security.getDtaStatus() != null ? security.getDtaStatus() : "", rowStyle));
                configs.add(new CellConfig(15, security.getDtaAccountNumber() != null ? security.getDtaAccountNumber() : "", rowStyle));
            }
            addExcelRow(counts, configs, row);
            rowNum++;

            if(VbsUtil.onlyFinancialInstitutionSearch(params) && maaPoaDetails != null && maaPoaDetails.getMaaPoaInstitutions() != null && !maaPoaDetails.getMaaPoaInstitutions().isEmpty()) {
                row = sheet.createRow(rowNum);
                Cell cell = row.createCell(1);
                cell.setCellValue(VbsConstants.FINANCIAL_INSTITUTION_TYPE_ADMINISTRATIVE_AGENT == financialInstType ? "Administrative Agent" : "Administrative Broker");cell.setCellStyle(boldStyle);

                rowNum++;
                row = sheet.createRow(rowNum);
                cell = row.createCell(1);
                cell.setCellValue((VbsConstants.FINANCIAL_INSTITUTION_TYPE_ADMINISTRATIVE_AGENT == financialInstType ? "MAA" : "POA") + maaPoaDetails.getMaaPoaSequence());

                rowNum++;
                row = sheet.createRow(rowNum);
                cell = row.createCell(1); cell.setCellValue("Financial Institution"); cell.setCellStyle(boldStyle);
                cell = row.createCell(2); cell.setCellValue("Percentage"); cell.setCellStyle(boldStyle);
                cell = row.createCell(3); cell.setCellValue("Original Amount"); cell.setCellStyle(boldStyle);
                cell = row.createCell(4); cell.setCellValue("Current Amount"); cell.setCellStyle(boldStyle);
                rowNum++;

                for (FinancialInstitutionMaaPoaInstitutionDTO institutionDTO: maaPoaDetails.getMaaPoaInstitutions()) {
                    row = sheet.createRow(rowNum);
                    cell = row.createCell(1); cell.setCellValue(institutionDTO.getFinancialInstitution().getId() + " - " + institutionDTO.getFinancialInstitution().getName());
                    cell = row.createCell(2); cell.setCellValue(institutionDTO.getPercentage().divide(new BigDecimal(100), RoundingMode.HALF_UP).doubleValue());cell.setCellStyle(percentageStyle);
                    cell = row.createCell(3); cell.setCellValue(institutionDTO.getOriginalAmountOnInstitution().doubleValue());cell.setCellStyle(currencyNoBorderStyle);
                    cell = row.createCell(4); cell.setCellValue(institutionDTO.getCurrentAmountOnInstitution().doubleValue());cell.setCellStyle(currencyNoBorderStyle);
                    rowNum++;
                }
            }
        }

        int freezeTo = 4;
        if(!VbsUtil.isNullorEmpty(params.getVbNumber()) && params.getVbNumberType() == SearchTypeEnum.EQUALS) { freezeTo++; }
        if(showInterest) { freezeTo++; }
        sheet.createFreezePane(0, freezeTo);

        for(Map.Entry<Integer, Integer> colCount : counts.entrySet()){
            int maxChars = colCount.getValue();
            if(maxChars > 245){
                sheet.setColumnWidth(colCount.getKey(), 65280);
            }else{
                sheet.setColumnWidth(colCount.getKey(), (maxChars*256));
            }
        }
        return returnExcelFile(wb, startTime, "createSearchResultsExcel");
	}

	private void addExcelRow(Map<Integer, Integer> counts, List<CellConfig> cols, Row row){
        for(CellConfig col : cols){
            Cell cell = row.createCell(col.getColNum());
            cell.setCellStyle(col.getStyle());
            int valSize = 0;
            if(col.getData() != null) {
                if (col.getData().getClass().equals(Double.class)) {
                    cell.setCellValue((Double) col.getData());
                    valSize = Double.toString((Double) col.getData()).length()+5;
                } else if (col.getData().getClass().equals(Integer.class)) {
                    cell.setCellValue((Integer) col.getData());
                    valSize = ("" + (col.getData())).length();
                } else {
                    cell.setCellValue((String) col.getData());
                    String value = ((String) col.getData());
                    valSize = value.length();
                    if(cell.getCellStyle().getWrapText()) {
                        valSize = value.indexOf(System.lineSeparator());
                    }
                }
            }
            if(counts.get(col.colNum) == null || counts.get(col.colNum) < valSize){
                counts.put(col.colNum, valSize);
            }
        }
    }

    @Override
	public byte[] getAutoReleasePassedResultsExcel(Long runId) throws VbsCheckedException {
		long startTime = LoggerUtil.logEnter(ExcelServiceImpl.class, "getAutoReleasePassedResultsExcel", runId);
		byte[] result = getAutoReleaseResultsExcel(runId, false);
		LoggerUtil.logExit(ExcelService.class, "getAutoReleasePassedResultsExcel", startTime);
		return result;

	}
	
	@Override
	public byte[] getAutoReleaseFailedResultsExcel(Long runId) throws VbsCheckedException {
		long startTime = LoggerUtil.logEnter(ExcelServiceImpl.class, "getAutoReleaseFailedResultsExcel", runId);
		byte[] result = getAutoReleaseResultsExcel(runId, true);
		LoggerUtil.logExit(ExcelService.class, "getAutoReleaseFailedResultsExcel", startTime);
		return result;
	}

	private byte[] getAutoReleaseResultsExcel(Long runId, boolean failed) throws VbsCheckedException {
        AutoReleaseResultWrapper results = autoReleaseService.getAutoReleaseResults(runId);
        List<AutoReleaseResultDTO> autoReleaseFailedResults = failed ? results.getAutoReleaseFailedResults() : results.getAutoReleasePassedResults();

        List<ExcelUtil.ColumnConfiguration> configs = new ArrayList<>();
        configs.add(new ExcelUtil.ColumnConfiguration(0, "VB", "vbNameNumber", ExcelUtil.ColumnType.STRING, String.class));
        configs.add(new ExcelUtil.ColumnConfiguration(1, "License Status", "licenseStatus", ExcelUtil.ColumnType.STRING, String.class));
        configs.add(new ExcelUtil.ColumnConfiguration(2, "Security Received Date", "securityReceivedDate", ExcelUtil.ColumnType.DATE, LocalDateTime.class));
        configs.add(new ExcelUtil.ColumnConfiguration(3, "Instrument #", "instrumentNumber", ExcelUtil.ColumnType.STRING, String.class));
        configs.add(new ExcelUtil.ColumnConfiguration(4, "Enrolment #", "enrolmentNumber", ExcelUtil.ColumnType.STRING, Integer.class));
        configs.add(new ExcelUtil.ColumnConfiguration(5, "WSD (MM/DD/YYYY)", "warrantyStartDate", ExcelUtil.ColumnType.DATE, LocalDateTime.class));
        configs.add(new ExcelUtil.ColumnConfiguration(6, "Address", "enrolmentAddress", ExcelUtil.ColumnType.STRING, String.class));
        configs.add(new ExcelUtil.ColumnConfiguration(7, "Security Type", "securityType", ExcelUtil.ColumnType.STRING, String.class));
        configs.add(new ExcelUtil.ColumnConfiguration(8, "Release Timing Type", "releaseTimingType", ExcelUtil.ColumnType.STRING, String.class));
        configs.add(new ExcelUtil.ColumnConfiguration(9, failed ? "Eliminated Due to Criteria" : "Deleted By", failed ? "eliminationCriteria" : "deletedBy", ExcelUtil.ColumnType.STRING, String.class));

        return ExcelUtil.createExcel(failed ? "Auto Release Failed Results" : "Auto Release Passed Results", configs, autoReleaseFailedResults);
    }

	private byte[] returnExcelFile(SXSSFWorkbook wb, long start, String methodName) throws VbsCheckedException {
        SXSSFSheet sheet = wb.getSheetAt(0);
        Set<Integer> cols = sheet.getTrackedColumnsForAutoSizing();
	    for (Integer col : cols) {
            sheet.autoSizeColumn(col, true);
        }
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        try {
            wb.write(b);
            byte[] file = b.toByteArray();
            b.close();
            wb.dispose();
            LoggerUtil.logExit(ExcelServiceImpl.class, methodName, start);
            return file;
        } catch (IOException io) {
            throw new VbsCheckedException("Failed to create Excel sheet.", io);
        }
    }

	@Override
	public byte[] getSearchedReleasesExcel(ReleaseSearchParametersDTO releaseSearchParametersDTO) throws VbsCheckedException {
		long startTime = LoggerUtil.logEnter(ExcelServiceImpl.class, "getSearchedReleasesExcel", releaseSearchParametersDTO);
		List<PendingReleaseDTO> results = releaseService.searchReleases(releaseSearchParametersDTO);
		
		int rowNum = 0;
		SXSSFWorkbook wb = new SXSSFWorkbook();
        wb.setCompressTempFiles(true);
        SXSSFSheet sheet = wb.createSheet("Releases Report Results");
        sheet.trackAllColumnsForAutoSizing();

        CellStyle headerStyle = getHeaderStyle(wb);
        CellStyle rowStyle = getRowStyle(wb);
        CellStyle currencyStyle = getCurrencyStyle(wb);
        CellStyle percentageStyle = getPercentageFormatRowStyle(wb);
        CellStyle hyperlinkStyle = getHyperLinkStyle(wb);
        CreationHelper createHelper = wb.getCreationHelper();

        /* Header */
        Row header = sheet.createRow(rowNum);
        header.createCell(0).setCellValue("VB");
        header.createCell(1).setCellValue("Instrument Number");
        header.createCell(2).setCellValue("Security Type");
        header.createCell(3).setCellValue("Release Type");
        header.createCell(4).setCellValue("Reference Number");
        header.createCell(5).setCellValue("Payee");
        header.createCell(6).setCellValue("Rcvd Before Nov 2004");
        header.createCell(7).setCellValue("Original Amount");
        header.createCell(8).setCellValue("Current Amount");
        header.createCell(9).setCellValue("Current Interest Amount");
        header.createCell(10).setCellValue("Current Amount (incl interest)");
        header.createCell(11).setCellValue("Released Amount");
        header.createCell(12).setCellValue("Released Interest Amount");
        header.createCell(13).setCellValue("Released Amount (incl interest)");
        header.createCell(14).setCellValue("Release Percentage");
        header.createCell(15).setCellValue("Manager Approval Date");
        header.createCell(16).setCellValue("Approval Manager");
        header.createCell(17).setCellValue("Comment");

        header.forEach(c -> c.setCellStyle(headerStyle));
        rowNum++;
        /* End Header */
        for (PendingReleaseDTO result : results) {
            Row row = sheet.createRow(rowNum);
            row.createCell(0).setCellValue(result.getVbNumber() + " - " + result.getVbName());
            row.createCell(1).setCellValue(result.getInstrumentNumber());
            row.createCell(2).setCellValue(result.getSecurityTypeName());
            row.createCell(3).setCellValue(result.getReleaseTypeName());
            Hyperlink link = createHelper.createHyperlink(HyperlinkType.URL);
            String securityId = result.getReferenceNumber().split("-")[0];
            link.setAddress(propertyService.getValue(VbsConstants.VBS_BASE_URL)+"/security?id="+securityId+"&tab=tab-release&referenceNumber="+result.getReferenceNumber());
            row.createCell(4).setHyperlink(link);
            row.getCell(4).setCellValue(result.getReferenceNumber());
            row.createCell(5).setCellValue((result.getPayee() != null) ? (result.getPayee().getCompanyName() != null ? result.getPayee().getCompanyName() : result.getPayee().getFirstName() + " " + result.getPayee().getLastName()) : "");
            row.createCell(6).setCellValue(result.isReceivedPriorNov2004() ? "Yes" : "No");
            row.createCell(7).setCellValue(result.getOriginalAmount() != null ? result.getOriginalAmount().doubleValue() : 0d);
            row.createCell(8).setCellValue(result.getCurrentAmount() != null ? result.getCurrentAmount().doubleValue() : 0d);
            row.createCell(9).setCellValue(result.getCurrentInterestAmount() != null ? result.getCurrentInterestAmount().doubleValue() : 0d);
            row.createCell(10).setCellValue(result.getInterestCurrentAmountTotal() != null ? result.getInterestCurrentAmountTotal().doubleValue() : 0d);
            row.createCell(11).setCellValue(result.getAuthorizedAmount() != null ? result.getAuthorizedAmount().doubleValue() : 0d);
            row.createCell(12).setCellValue(result.getInterestAmount() != null ? result.getInterestAmount().doubleValue() : 0d);
            row.createCell(13).setCellValue(result.getInterestAuthorizedAmountTotal() != null ? result.getInterestAuthorizedAmountTotal().doubleValue() : 0d);
            row.createCell(14).setCellValue(result.getReleasePercentage() != null ? result.getReleasePercentage().divide(VbsConstants.BIGDECIMAL_HUNDRED, RoundingMode.HALF_UP).doubleValue() : 0d);
            row.createCell(15).setCellValue(DateUtil.getDateFormattedShort(result.getManagerApprovalDate()));
            row.createCell(16).setCellValue(result.getApprovalManager());
            row.createCell(17).setCellValue(result.getComment());

            row.forEach(c -> {
                if (c.getColumnIndex() == 4){
                    c.setCellStyle(hyperlinkStyle);
                } else if (c.getColumnIndex() >= 7 && c.getColumnIndex() <= 13) {
                    c.setCellStyle(currencyStyle);
                } else if (c.getColumnIndex() == 14) {
                    c.setCellStyle(percentageStyle);
                } else {
                    c.setCellStyle(rowStyle);
                }
            });

            rowNum++;
        }

        sheet.createFreezePane(0, 1);

        return returnExcelFile(wb, startTime, "getSearchedReleasesExcel");
	}

	@Override
	public byte[] getAutoReleaseForSecurityIdExcel(Long securityId) throws VbsCheckedException {
		long startTime = LoggerUtil.logEnter(ExcelServiceImpl.class, "getAutoReleaseForSecurityIdExcel", securityId);
		List<AutoReleaseDTO> results = autoReleaseService.getAutoReleaseForSecurityId(securityId);
		
		int rowNum = 0;

        SXSSFWorkbook wb = new SXSSFWorkbook();
        wb.setCompressTempFiles(true);
        SXSSFSheet sheet = wb.createSheet("Auto Releases");
        sheet.trackAllColumnsForAutoSizing();

        /* Styles */
        Font defaultFont= wb.createFont();
        defaultFont.setBold(false);
        defaultFont.setFontHeightInPoints((short)10);
        defaultFont.setFontName("Arial");

        Font strong = wb.createFont();
        strong.setBold(true);
        strong.setFontHeightInPoints((short)10);
        strong.setFontName("Arial");

        CellStyle headerStyle = setThinBorder(wb);
        headerStyle.setFont(strong);
        headerStyle.setAlignment(HorizontalAlignment.CENTER);

        CellStyle currencyStyle = getCurrencyStyle(wb);
        currencyStyle.setDataFormat((short)7);

        CellStyle boldStyle = wb.createCellStyle();
        boldStyle.setFont(strong);

        /* End Styles */

        /* Header */
        Row header = sheet.createRow(rowNum);
        header.createCell(0).setCellValue("Selection Run Date Time");
        header.createCell(1).setCellValue("Based on Home");
        header.createCell(2).setCellValue("Release Timing Type");
        header.createCell(3).setCellValue("Eliminated Due to Criteria");
        header.createCell(4).setCellValue("Resulted in Release");
        header.createCell(5).setCellValue("Deleted by User");

        header.forEach(c -> c.setCellStyle(headerStyle));
        rowNum++;
        /* End Header */

        for (AutoReleaseDTO result : results) {
            Row row = sheet.createRow(rowNum);

            row.createCell(0).setCellValue(DateUtil.getDateFormattedShortTime(result.getRunDate()));
            row.createCell(1).setCellValue(result.getEnrolmentNumber() + " - " + result.getEnrolmentAddress());
            row.createCell(2).setCellValue(result.getTimingType());
            StringBuilder sb = new StringBuilder();
            if (result.getEliminations() != null && result.getEliminations().size() > 0) {
                for (int ide = 0; ide < result.getEliminations().size(); ide++) {
                    sb.append(result.getEliminations().get(ide)).append("\n");
                }
            }
            row.createCell(3).setCellValue(sb.toString());
            row.createCell(4).setCellValue(DateUtil.getDateFormattedShortTime(result.getReleaseDate()));
            row.createCell(5).setCellValue(result.getDeletedBy());

            rowNum++;
        }
        sheet.createFreezePane(0, 1);

        return returnExcelFile(wb, startTime, "getSearchedReleasesExcel");
	}
	
	@Override
	public byte[] getReleasesExcel(Long securityId, UserDTO userDTO) throws VbsCheckedException {
        long startTime = LoggerUtil.logEnter(ExcelServiceImpl.class, "getReleasesExcel", securityId);
        SecurityEntity securityEntity = genericDAO.findEntityById(SecurityEntity.class, securityId);
		List<ReleaseDTO> results = releaseService.getReleases(securityId);
		
        int rowNum = 0;
        int columnIndex = 0;
		SXSSFWorkbook wb = new SXSSFWorkbook();
        wb.setCompressTempFiles(true);
        SXSSFSheet sheet = wb.createSheet("Releases Summary");
        sheet.trackAllColumnsForAutoSizing();

        CellStyle headerStyle = getHeaderStyle(wb);
        CellStyle rowStyle = getRowStyle(wb);
        CellStyle currencyStyle = getCurrencyStyle(wb);

        /* Header */
        Row header = sheet.createRow(rowNum);
        header.createCell(columnIndex).setCellValue("Requested Date"); columnIndex++;
        header.createCell(columnIndex).setCellValue("VB #"); columnIndex++;
        header.createCell(columnIndex).setCellValue("Enrolment #"); columnIndex++;
        header.createCell(columnIndex).setCellValue("Request Type"); columnIndex++;
        header.createCell(columnIndex).setCellValue("Total Requested Amount"); columnIndex++;
        header.createCell(columnIndex).setCellValue("Total Authorized Amount"); columnIndex++;
        if(securityEntity.getSecurityType().getId() == 1 && userDTO.hasPermission("CREATE_DEMAND_FINANCE")) {
            header.createCell(columnIndex).setCellValue("Interest Amount"); columnIndex++;
        }
        if(!userDTO.hasPermission("CREATE_DEMAND_FINANCE")) {
            header.createCell(columnIndex).setCellValue("LU Retained Amount"); columnIndex++;
        }
        header.createCell(columnIndex).setCellValue("WS Input Requested"); columnIndex++;
        header.createCell(columnIndex).setCellValue("Final Approval Status"); columnIndex++;
        header.createCell(columnIndex).setCellValue("Final Approval Date/Time"); columnIndex++;
        header.createCell(columnIndex).setCellValue("Recommended/Rejected By"); columnIndex++;
        header.createCell(columnIndex).setCellValue("Reference Number");
        header.forEach(c -> c.setCellStyle(headerStyle));
        rowNum++;
        /* End Header */

        for (ReleaseDTO result : results) {
            columnIndex = 0;
            Row row = sheet.createRow(rowNum);

            row.createCell(columnIndex).setCellValue(DateUtil.getDateFormattedShortTime(result.getRequestDate())); columnIndex++;
            StringBuilder enrolmentsNumbers = new StringBuilder();
            if(result.getEnrolments()!=null && result.getEnrolments().size() > 0) {
                for (ReleaseEnrolmentDTO enrolment : result.getEnrolments()) {
                    enrolmentsNumbers.append(enrolment.getEnrolment() != null && !VbsUtil.isNullorEmpty(enrolment.getEnrolment().getEnrolmentNumber()) ? enrolment.getEnrolment().getEnrolmentNumber() + ";" : "");
                    row.createCell(columnIndex).setCellValue(enrolment.getEnrolment().getEnrollingVb());
                }
            } else {
                row.createCell(columnIndex).setCellValue("");
            }
            columnIndex++;

            row.createCell(columnIndex).setCellValue(enrolmentsNumbers.toString()); columnIndex++;
            row.createCell(columnIndex).setCellValue(result.getReleaseType() != null ? result.getReleaseType().getDescription() : ""); columnIndex++;
            row.createCell(columnIndex).setCellValue(result.getRequestedAmount() != null ? result.getRequestedAmount().doubleValue() : 0d); columnIndex++;
            row.createCell(columnIndex).setCellValue(result.getAuthorizedReleaseAmount() != null ? result.getAuthorizedReleaseAmount().doubleValue() : 0d); columnIndex++;
            if(securityEntity.getSecurityType().getId() == 1 && userDTO.hasPermission("CREATE_DEMAND_FINANCE")) {
                row.createCell(columnIndex).setCellValue(result.getInterestAmount() != null ? result.getInterestAmount().doubleValue() : 0d); columnIndex++;
            }
            if(!userDTO.hasPermission("CREATE_DEMAND_FINANCE")) {
                row.createCell(columnIndex).setCellValue(result.getLuRetainAmount() != null ? result.getLuRetainAmount().doubleValue() : 0d); columnIndex++;
            }
            row.createCell(columnIndex).setCellValue(result.isWsInputRequested() ? "Yes" : "No"); columnIndex++;
            String finalApprovalStatus;
            if (result.getFinalApprovalStatus() == null && result.getAnalystApprovalStatus().equals(ReleaseApprovalStatusEnum.Rejected)) {
                finalApprovalStatus = result.getAnalystApprovalStatus().toString();
            } else {
                finalApprovalStatus = (result.getFinalApprovalStatus() != null) ? result.getFinalApprovalStatus().toString() : "";
            }
            LocalDateTime finalApprovalDate;
            if (result.getFinalApprovalStatus() == null && result.getAnalystApprovalStatus().equals(ReleaseApprovalStatusEnum.Rejected)) {
                finalApprovalDate = result.getAnalystApprovalDateTime();
            } else {
                finalApprovalDate = result.getFinalApprovalDate();
            }
            row.createCell(columnIndex).setCellValue(finalApprovalStatus); columnIndex++;
            row.createCell(columnIndex).setCellValue(DateUtil.getDateFormattedShortTime(finalApprovalDate)); columnIndex++;
            row.createCell(columnIndex).setCellValue(result.getRecommendedRejectedBy()); columnIndex++;
            row.createCell(columnIndex).setCellValue(result.getReferenceNumber());

            for (Cell c : row) {
                if (c.getColumnIndex() == 4 || c.getColumnIndex() == 5 || (columnIndex == 11 && c.getColumnIndex() == 6)) {
                    c.setCellStyle(currencyStyle);
                } else {
                    c.setCellStyle(rowStyle);
                }
            }

            rowNum++;
        }
        sheet.createFreezePane(0, 1);
        return returnExcelFile(wb, startTime, "getReleasesExcel");
	}
			

	@Override
	public byte[] createInterestSearchResultsExcel(SecurityInterestSearchParamsDTO securityInterestSearchParams) throws VbsCheckedException{
		long startTime = LoggerUtil.logEnter(ExcelService.class, "createInterestSearchResultsExcel", securityInterestSearchParams);
		List<InterestSearchResultDTO> results = interestService.searchInterest(securityInterestSearchParams);

		int rowNum = 0;
        SXSSFWorkbook wb = new SXSSFWorkbook();
        wb.setCompressTempFiles(true);
        SXSSFSheet sheet = wb.createSheet("Estimated Interest - #"+securityInterestSearchParams.getSecurityId());
        sheet.trackAllColumnsForAutoSizing();

        CellStyle boldStyle = getBoldStyle(wb);
        CellStyle headerStyle = getHeaderStyle(wb);

        Row header1 = sheet.createRow(rowNum);
        header1.createCell(0).setCellValue("Security ID");
        header1.createCell(2).setCellValue("From Date");
        header1.createCell(3).setCellValue("To Date");
        header1.forEach(c -> c.setCellStyle(boldStyle));
        rowNum++;
        Row header2 = sheet.createRow(rowNum);
        header2.createCell(0).setCellValue(securityInterestSearchParams.getSecurityId());
        header2.createCell(2).setCellValue(DateUtil.getDateFormattedShort(securityInterestSearchParams.getFromDate()));
        header2.createCell(3).setCellValue(DateUtil.getDateFormattedShort(securityInterestSearchParams.getToDate()));
        rowNum++;rowNum++;

        /* Header */
        Row header = sheet.createRow(rowNum);
        header.createCell(0).setCellValue("Year");
        header.createCell(1).setCellValue("From Date");
        header.createCell(2).setCellValue("To Date");
        header.createCell(3).setCellValue("Accrued Interest");
        header.createCell(4).setCellValue("Discount Amount");

        header.forEach(c -> c.setCellStyle(headerStyle));
        rowNum++;
        /* End Header */

        CellStyle rowStyle = getRowStyle(wb);
        CellStyle interestDecimalFormat = getInterestDecimalFormat(wb);

        for (InterestSearchResultDTO result : results) {
            Row row = sheet.createRow(rowNum);

            row.createCell(0).setCellValue(result.getYear());
            row.createCell(1).setCellValue(DateUtil.getDateFormattedShort(result.getFromDate()));
            row.createCell(2).setCellValue(DateUtil.getDateFormattedShort(result.getToDate()));
            row.createCell(3).setCellValue(result.getAccruedInterest().doubleValue());
            row.createCell(4).setCellValue(result.getDiscountAmount().doubleValue());

            row.forEach(c -> {
                if (c.getColumnIndex() >= 3) {
                    c.setCellStyle(interestDecimalFormat);
                } else {
                    c.setCellStyle(rowStyle);
                }
            });

            rowNum++;
        }
        sheet.createFreezePane(0, 4);
        return returnExcelFile(wb, startTime, "createInterestSearchResultsExcel");
	}

	@Override
	public byte[] createNoInterestSearchResultsExcel() throws VbsCheckedException {
		long startTime = LoggerUtil.logEnter(ExcelServiceImpl.class, "createNoInterestSearchResultsExcel");
		List<CashSecurityNoInterestDTO> securitiesNoInterest = interestService.getCashSecuritiesWithoutInterest();
		
		int rowNum = 0;
        SXSSFWorkbook wb = new SXSSFWorkbook();
        wb.setCompressTempFiles(true);
        SXSSFSheet sheet = wb.createSheet("Security With No Interest Results");
        sheet.trackAllColumnsForAutoSizing();

        /* Header */
        Row header = sheet.createRow(rowNum);
        header.createCell(0).setCellValue("Security Number");
        header.createCell(1).setCellValue("VB Number");
        header.createCell(2).setCellValue("VB Name");
        header.createCell(3).setCellValue("Instrument Number");
        header.createCell(4).setCellValue("Issued Date");
        header.createCell(5).setCellValue("Received Date");
        header.createCell(6).setCellValue("Original Amount");
        header.createCell(7).setCellValue("Current Amount");
        header.createCell(8).setCellValue("Last Interest Calculated Date");
        header.createCell(9).setCellValue("Missing Interest Date(s)");

        CellStyle headerStyle = getHeaderStyle(wb);
        header.forEach(c -> c.setCellStyle(headerStyle));
        rowNum++;
        /* End Header */

        for (CashSecurityNoInterestDTO security : securitiesNoInterest) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(security.getSecurityNumber());
            row.createCell(1).setCellValue(security.getVbNumber());
            row.createCell(2).setCellValue(security.getVbName());
            row.createCell(3).setCellValue(security.getInstrumentNumber());
            row.createCell(4).setCellValue(DateUtil.getDateFormattedShort(security.getIssuedDate()));
            row.createCell(5).setCellValue(DateUtil.getDateFormattedShort(security.getReceivedDate()));
            row.createCell(6).setCellValue((security.getOriginalAmount() != null) ? security.getOriginalAmount().doubleValue() : 0d);
            row.createCell(7).setCellValue((security.getCurrentAmount() != null) ? security.getCurrentAmount().doubleValue() : 0d);
            row.createCell(8).setCellValue(security.getLastInterestCalculatedDate() != null ? DateUtil.getDateFormattedShort(security.getLastInterestCalculatedDate().atStartOfDay()) : "");
            row.createCell(9).setCellValue(security.getMissingInterestDates());
        }

        return returnExcelFile(wb, startTime, "createNoInterestSearchResultsExcel");
    }
    
    @Override
	public byte[] searchDeletedSecurityExcel(DeletedSecuritySearchParametersDTO deletedSearchParametersDTO, UserDTO userDTO) throws VbsCheckedException {
		long startTime = LoggerUtil.logEnter(ExcelServiceImpl.class, "searchDeletedSecurityExcel", deletedSearchParametersDTO);
		List<SecuritySearchResultDTO> results = securityService.searchDeletedSecurity(deletedSearchParametersDTO, userDTO);
		
		int rowNum = 0;

        SXSSFWorkbook wb = new SXSSFWorkbook();
        wb.setCompressTempFiles(true);
        SXSSFSheet sheet = wb.createSheet("Deleted Securities");
        sheet.trackAllColumnsForAutoSizing();

        /* Styles */
        Font defaultFont= wb.createFont();
        defaultFont.setBold(false);
        defaultFont.setFontHeightInPoints((short)10);
        defaultFont.setFontName("Arial");

        Font strong = wb.createFont();
        strong.setBold(true);
        strong.setFontHeightInPoints((short)10);
        strong.setFontName("Arial");

        CellStyle headerStyle = setThinBorder(wb);
        headerStyle.setFont(strong);
        headerStyle.setAlignment(HorizontalAlignment.CENTER);

        CellStyle currencyStyle = getCurrencyStyle(wb);
        currencyStyle.setDataFormat((short)7);

        CellStyle boldStyle = wb.createCellStyle();
        boldStyle.setFont(strong);

        /* End Styles */

        /* Header */
        Row header = sheet.createRow(rowNum);
        header.createCell(0).setCellValue("Security Number");
        header.createCell(1).setCellValue("Instrument Number");
        header.createCell(2).setCellValue("Received Date");
        header.createCell(3).setCellValue("Pool Id");
        header.createCell(4).setCellValue("Security Type Name");
        header.createCell(5).setCellValue("VB Number");
        header.createCell(6).setCellValue("VB Name");
        header.createCell(7).setCellValue("Security Purpose");
        header.createCell(8).setCellValue("Original Amount");
        header.createCell(9).setCellValue("Current Amount");
        header.createCell(10).setCellValue("Current Interest Amount");
        header.createCell(11).setCellValue("Total Available Cash Amount (Inc Interest)");

        header.forEach(c -> c.setCellStyle(headerStyle));
        rowNum++;
        /* End Header */

        for (SecuritySearchResultDTO result : results) {
            Row row = sheet.createRow(rowNum);

            row.createCell(0).setCellValue(result.getSecurityNumber());
            row.createCell(1).setCellValue(result.getInstrumentNumber());
            row.createCell(2).setCellValue(DateUtil.getDateFormattedShort(result.getReceivedDate()));
            row.createCell(3).setCellValue(result.getPoolId());
            row.createCell(4).setCellValue(result.getSecurityTypeName());
            row.createCell(5).setCellValue(result.getVbNumber() != null ? "B" + result.getVbNumber() : "");
            row.createCell(6).setCellValue(result.getVbName() != null ? result.getVbName() : "");
            row.createCell(7).setCellValue(result.getSecurityPurposeName());
            row.createCell(8).setCellValue(result.getOriginalAmount() != null ? result.getOriginalAmount().doubleValue() : 0d);
            row.createCell(9).setCellValue(result.getCurrentAmount() != null ? result.getCurrentAmount().doubleValue() : 0d);
            row.createCell(10).setCellValue(result.getCurrentInterestAmount() != null ? result.getCurrentInterestAmount().doubleValue() : 0d);
            row.createCell(11).setCellValue(result.getTotalAvailableAmount() != null ? result.getTotalAvailableAmount().doubleValue() : 0d);
            rowNum++;
        }
        sheet.createFreezePane(0, 1);

        return returnExcelFile(wb, startTime, "searchDeletedSecurityExcel");
	}


	/* Styles */
	private Font getStrongFont(SXSSFWorkbook wb){
		Font strong = wb.createFont();
		strong.setBold(true);
		strong.setFontHeightInPoints((short)10);
		strong.setFontName("Arial");
		return strong;
	}

	private CellStyle getHeaderStyle(SXSSFWorkbook wb){
		CellStyle headerStyle = setThinBorder(wb);
		headerStyle.setFont(getStrongFont(wb));
		headerStyle.setAlignment(HorizontalAlignment.CENTER);
		return headerStyle;
	}

	private CellStyle setThinBorder(SXSSFWorkbook wb) {
		CellStyle headerStyle = wb.createCellStyle();
		headerStyle.setBorderBottom(BorderStyle.THIN);
		headerStyle.setBorderTop(BorderStyle.THIN);
		headerStyle.setBorderRight(BorderStyle.THIN);
		headerStyle.setBorderLeft(BorderStyle.THIN);
		return headerStyle;
	}

	private CellStyle getHyperLinkStyle(SXSSFWorkbook wb) {
        CellStyle hyperlinkStyle = wb.createCellStyle();
        hyperlinkStyle.cloneStyleFrom(getRowStyle(wb));
        Font font = wb.createFont();
        font.setUnderline(Font.U_SINGLE);
        font.setColor(IndexedColors.BLUE.getIndex());
        hyperlinkStyle.setFont(font);
        return hyperlinkStyle;
    }

	private CellStyle getRowStyle(SXSSFWorkbook wb) {
		CellStyle style = setThinBorder(wb);
		style.setVerticalAlignment(VerticalAlignment.TOP);
		return style;
	}

	private CellStyle getInterestDecimalFormat(SXSSFWorkbook wb) {
		CellStyle interestStyle = wb.createCellStyle();
		interestStyle.cloneStyleFrom(getRowStyle(wb));
		interestStyle.setDataFormat(wb.createDataFormat().getFormat("0.0000"));
		return interestStyle;
	}

	private CellStyle getPercentageFormat(SXSSFWorkbook wb) {
        CellStyle style = wb.createCellStyle();
		style.setDataFormat(wb.createDataFormat().getFormat("0%"));
		return style;
    }
    
    private CellStyle getPercentageFormatRowStyle(SXSSFWorkbook wb) {
        CellStyle style = wb.createCellStyle();
        style.cloneStyleFrom(getRowStyle(wb));
		style.setDataFormat(wb.createDataFormat().getFormat("0%"));
		return style;
	}

	private CellStyle getCurrencyStyle(SXSSFWorkbook wb) {
		CellStyle currencyStyle = wb.createCellStyle();
		currencyStyle.cloneStyleFrom(getRowStyle(wb));
		currencyStyle.setDataFormat((short) 7);
		return currencyStyle;
	}

	private CellStyle getCurrencyStyleNoBorder(SXSSFWorkbook wb) {
		CellStyle currencyStyle = wb.createCellStyle();
		currencyStyle.setDataFormat((short) 7);
		return currencyStyle;
	}

	private CellStyle getBoldStyle(SXSSFWorkbook wb) {
		CellStyle boldStyle = wb.createCellStyle();
		boldStyle.setFont(getStrongFont(wb));
		return boldStyle;
	}

	private CellStyle getWordWrapStyle(SXSSFWorkbook wb) {
        CellStyle style = wb.createCellStyle();
        style.cloneStyleFrom(getRowStyle(wb));
        style.setWrapText(true);
        return style;
    }
	/* End Styles */

	private void addMergeRegion(int rowNum, Sheet sheet, int... col) {
		for (int i = 0; i <col.length; i = i+2) {
			sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, col[i], col[i+1]));
		}
	}

    public static class CellConfig {
        private int colNum;
        private Object data;
        private CellStyle style;

        CellConfig(int colNum, Object data, CellStyle style){
            this.colNum = colNum;
            this.data = data;
            this.style = style;
        }

        int getColNum() {
            return colNum;
        }

        Object getData() {
            return data;
        }

        CellStyle getStyle() {
            return style;
        }
    }
}
