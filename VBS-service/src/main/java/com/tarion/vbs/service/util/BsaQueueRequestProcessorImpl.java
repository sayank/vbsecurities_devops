/* 
 * 
 * BsaQueueRequestProcessorImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentDTO;
import com.tarion.vbs.common.dto.security.SecurityDTO;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.exceptions.VbsJAXBConversionException;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.contact.ContactDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.dao.util.JmsTransactionLogDAO;
import com.tarion.vbs.orm.entity.JmsTransactionLogEntity;
import com.tarion.vbs.service.contact.ContactServiceTransformer;
import com.tarion.vbs.service.jaxb.bsa.createcashsecurity.CreateCashSecurityRequest;
import com.tarion.vbs.service.security.SecurityService;
import com.tarion.vbs.service.webservice.VBSWS;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Processing Requests from BSA Queue MDB 
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @date 2018-04-20
 * @version 1.0	
 */
@Stateless(mappedName = "ejb/BsaQueueRequestProcessor")
@Interceptors(EjbLoggingInterceptor.class)
public class BsaQueueRequestProcessorImpl implements BsaQueueRequestProcessor {
	
	@EJB
	GenericDAO genericDAO;
	@EJB
	private JmsTransactionLogDAO jmsTransactionLogDAO;
	@EJB
	private ContactDAO contactDAO;
	@EJB
	private SecurityService securityService;
	@EJB
	private JmsMessageSenderService jmsMessageSenderService;
	
	private static final String PROCESS = "process";
	private static final String FAILED_PROCESSING = "Failed processing ";
	
	@Override
	public void processReceivedBsaXml(String xmlResponse) {
		Object jaxbObject = null;
		try {
			jaxbObject = VbsUtil.convertXmltoJaxb(xmlResponse);
			Method m = this.getClass().getMethod(PROCESS, jaxbObject.getClass(), String.class);
			m.invoke(this, jaxbObject, xmlResponse);

		}catch(VbsJAXBConversionException e) {
			//converting didn't work
			LoggerUtil.logError(BsaQueueRequestProcessorImpl.class, "processReceivedBsaXml ", e, "XML to JAXB Conversion Failed.", xmlResponse);
			throw e;
		}catch(NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
			//error invoking the method (not found/not allowed)
			LoggerUtil.logError(BsaQueueRequestProcessorImpl.class, "processReceivedBsaXml", e, "Unable to invoke process method.", xmlResponse);
			throw new VbsRuntimeException(e.getMessage());
		}catch(Exception e) {
			//other unknown exception/exception thrown from process method
			LoggerUtil.logError(BsaQueueRequestProcessorImpl.class, "processReceivedBsaXml", e, "Error occured during processing.", xmlResponse);
			throw e;
		}
	}

	@Override
	public void process(CreateCashSecurityRequest request, String xmlResponse) {
		if (request != null) {
			JmsTransactionLogEntity logEntity = insertTransactionLog(JmsMessageTypeEnum.getBsaCashSecurityRequestTracking(request.getRequestId(), request.getVbNumber()), xmlResponse);

			int unitsToCover = (request.getEnrolments() != null) ? request.getEnrolments().getEnrolmentNumber().size() : 1;
			SecurityDTO securityDTO = new SecurityDTO();
			securityDTO.setUnitsToCover(unitsToCover);
			ContactDTO vb = ContactServiceTransformer.transformEntityToDTO(contactDAO.getContactByCrmContactId(request.getVbNumber()));
			vb.setPrimaryVb(true);
			List<ContactDTO> vbList = new ArrayList<>();
			vbList.add(vb);
			securityDTO.setVbList(vbList);

			securityDTO.setSecurityTypeId(VbsConstants.SECURITY_TYPE_CASH);
			securityDTO.setSecurityPurposeId(VbsConstants.SECURITY_PURPOSE_DEPOSIT_AND_WARRANTY);

			BigDecimal amount = BigDecimal.ZERO.add(BigDecimal.valueOf(request.getAmount()));
			securityDTO.setOriginalAmount(amount);
			securityDTO.setCurrentAmount(amount);
			securityDTO.setInstrumentNumber(request.getInstrumentNumber());
			securityDTO.setIssuedDate(LocalDate.parse(request.getCreateDate()).atStartOfDay());
			securityDTO.setReceivedDate(LocalDate.parse(request.getCreateDate()).atStartOfDay());

			BigDecimal enrolmentAmount = BigDecimal.valueOf(request.getAmount())
					.divide(BigDecimal.valueOf(request.getEnrolments().getEnrolmentNumber().size()), VbsConstants.DECIMAL_SCALE, VbsConstants.ROUNDING_MODE);
			List<EnrolmentDTO> enrolmentList = new ArrayList<>();
			request.getEnrolments().getEnrolmentNumber().forEach(e -> {
				EnrolmentDTO dto = new EnrolmentDTO();
				dto.setAllocatedAmount(enrolmentAmount);
				dto.setCurrentAllocatedAmount(enrolmentAmount);
				dto.setExcessAmount(BigDecimal.ZERO);
				dto.setInventoryAmount(BigDecimal.ZERO);
				dto.setMaxUnitDeposit(BigDecimal.ZERO);
				dto.setPefAmount(BigDecimal.ZERO);
				dto.setRaAmount(BigDecimal.ZERO);
				dto.setEnrolmentNumber(e);
				enrolmentList.add(dto);
			});
			securityDTO.setEnrolments(enrolmentList);

			UserDTO u = new UserDTO();
			u.setUserId(VbsConstants.VBS_BSA_SYSTEM_USER_ID);
			Long securityId = securityService.saveSecurity(securityDTO, new ArrayList<>(), u);


			com.tarion.vbs.service.jaxb.bsa.createcashsecurity.CreateCashSecurityResponse bsaResponse = new com.tarion.vbs.service.jaxb.bsa.createcashsecurity.CreateCashSecurityResponse();
			bsaResponse.setRequestId(request.getRequestId());
			bsaResponse.setSecurityId(securityId);
			String xml = VbsUtil.convertJaxbToXml(bsaResponse, com.tarion.vbs.service.jaxb.bsa.createcashsecurity.CreateCashSecurityResponse.class);
			jmsMessageSenderService.postBsaResponseQueue(xml);
			logEntity.setResponse(xml);
			LocalDateTime now = DateUtil.getLocalDateTimeNow();
			logEntity.setResponseReceivedDate(now);
			logEntity.setUpdateDate(now);
			logEntity.setUpdateUser(VbsConstants.VBS_BSA_SYSTEM_USER_ID);
			genericDAO.updateEntity(logEntity, VbsConstants.VBS_BSA_SYSTEM_USER_ID, now);
		} else {
			//error reading response
			String errorMessage = FAILED_PROCESSING + " - empty ressponse object. XML is:" + xmlResponse;
			LoggerUtil.logError(BsaQueueRequestProcessorImpl.class, PROCESS, errorMessage);
			throw new VbsRuntimeException(errorMessage);
		}
	}	

	private JmsTransactionLogEntity insertTransactionLog(String trackingNumber, String xmlMessageText) {
		JmsTransactionLogEntity entity = new JmsTransactionLogEntity();
		entity.setTrackingNumber(trackingNumber);
		entity.setVersion(1L);
		entity.setMessageName(trackingNumber);
		entity.setRequest(xmlMessageText);
		LocalDateTime now = DateUtil.getLocalDateTimeNow();
		entity.setRequestSendDate(now);
		entity.setResponse(null);
		entity.setResponseReceivedDate(null);
		return (JmsTransactionLogEntity)genericDAO.addEntity(entity, VbsConstants.VBS_BSA_SYSTEM_USER_ID, now);
	}


}
