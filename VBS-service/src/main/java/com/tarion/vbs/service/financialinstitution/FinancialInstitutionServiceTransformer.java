/* 
 * 
 * FinancialInstitutionServiceTransformer.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.financialinstitution;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

import javax.interceptor.Interceptors;

import org.apache.commons.beanutils.BeanUtils;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.NameDescriptionDTO;
import com.tarion.vbs.common.dto.financialinstitution.BranchDTO;
import com.tarion.vbs.common.dto.financialinstitution.ContactFinancialInstitutionDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionAlertDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionMaaPoaDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionMaaPoaInstitutionDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionSigningOfficerDTO;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.alert.AlertTypeEntity;
import com.tarion.vbs.orm.entity.financialinstitution.BranchEntity;
import com.tarion.vbs.orm.entity.financialinstitution.ContactFinancialInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionAlertEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionMaaPoaEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionMaaPoaInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionSigningOfficerEntity;
import com.tarion.vbs.service.contact.ContactServiceTransformer;


/**
 * Transformer helper class used by Financial Institution Service to provide transformation between DTO and Entity
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-11-01
 * @version 1.0
 */
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class FinancialInstitutionServiceTransformer {
	
	private FinancialInstitutionServiceTransformer() {}

	public static FinancialInstitutionDTO transformEntityToDTO(FinancialInstitutionEntity entity) {
		FinancialInstitutionDTO dto = new FinancialInstitutionDTO();
		try {
			BeanUtils.copyProperties(dto, entity);
			if(entity.getFinancialInstitutionType()!=null && entity.getFinancialInstitutionType().getId()!=null) {
				dto.setFinancialInstitutionTypeId(entity.getFinancialInstitutionType().getId());
			}
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new VbsRuntimeException(VbsConstants.FAILED_TO_COPY_PROPERTIES, e); 
		}
		return dto;
	}

	public static BranchDTO transformEntityToDTO(BranchEntity entity) {
		BranchDTO dto = new BranchDTO();
		try {
			BeanUtils.copyProperties(dto, entity);
			if(entity.getAddress() != null && entity.getAddress().concatAddress() != null) {
				dto.setAddressConcat(entity.getAddress().concatAddress());
			}
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new VbsRuntimeException(VbsConstants.FAILED_TO_COPY_PROPERTIES, e); 
		}
		return dto;
	}
	
	public static FinancialInstitutionEntity transformDTOToEntity(FinancialInstitutionDTO dto, FinancialInstitutionEntity entity) {
		entity.setId(dto.getId());
		entity.setVersion(dto.getVersion());
		entity.setName(dto.getName());
		entity.setDescription(dto.getDescription());
		entity.setExpiryDate(dto.getExpiryDate());
		entity.setMaxSecurityAmount(dto.getMaxSecurityAmount());
		entity.setWnSecurityMaxAmount(dto.getWnSecurityMaxAmount());

		entity.setCreateDate(dto.getCreateDate());
		entity.setCreateUser(dto.getCreateUser());
		entity.setUpdateDate(dto.getUpdateDate());
		entity.setUpdateUser(dto.getUpdateUser());
		
		return entity;
	}

	public static FinancialInstitutionMaaPoaEntity transformDTOToEntity(FinancialInstitutionMaaPoaDTO dto, FinancialInstitutionMaaPoaEntity entity) {
		entity.setId(dto.getId());
		entity.setVersion(dto.getVersion());
		entity.setExpirationDate(dto.getExpirationDate());
		if (dto.getMaaPoaSequence() != null) {
			entity.setMaaPoaSequence(dto.getMaaPoaSequence());
		}
		entity.setCreateDate(dto.getCreateDate());
		entity.setCreateUser(dto.getCreateUser());
		entity.setUpdateDate(dto.getUpdateDate());
		entity.setUpdateUser(dto.getUpdateUser());
		
		return entity;
	}
	
	public static FinancialInstitutionMaaPoaInstitutionEntity transformDTOToEntity(FinancialInstitutionMaaPoaInstitutionDTO dto, FinancialInstitutionMaaPoaInstitutionEntity entity) {
		entity.setId(dto.getId());
		entity.setVersion(dto.getVersion());
		entity.setPercentage(dto.getPercentage());

		entity.setCreateDate(dto.getCreateDate());
		entity.setCreateUser(dto.getCreateUser());
		entity.setUpdateDate(dto.getUpdateDate());
		entity.setUpdateUser(dto.getUpdateUser());
		
		return entity;
	}
	
	public static FinancialInstitutionMaaPoaDTO transformEntityToDTO(FinancialInstitutionMaaPoaEntity entity) {
		FinancialInstitutionMaaPoaDTO dto = new FinancialInstitutionMaaPoaDTO();
		dto.setAdminAgentBroker(transformEntityToDTO(entity.getFinancialInstitution()));
		dto.setCreateDate(entity.getCreateDate());
		dto.setCreateUser(entity.getCreateUser());
		dto.setExpirationDate(entity.getExpirationDate());
		dto.setId(entity.getId());
		dto.setMaaPoaSequence(entity.getMaaPoaSequence());
		dto.setUpdateDate(entity.getUpdateDate());
		dto.setUpdateUser(entity.getUpdateUser());
		dto.setVersion(entity.getVersion());
		return dto;
	}

	public static FinancialInstitutionMaaPoaInstitutionDTO transformEntityToDTO(FinancialInstitutionMaaPoaInstitutionEntity entity) {
		FinancialInstitutionMaaPoaInstitutionDTO dto = new FinancialInstitutionMaaPoaInstitutionDTO();
		dto.setCurrentAmountOnInstitution(BigDecimal.ZERO);
		dto.setOriginalAmountOnInstitution(BigDecimal.ZERO);
		dto.setCreateDate(entity.getCreateDate());
		dto.setCreateUser(entity.getCreateUser());
		dto.setId(entity.getId());
		dto.setPercentage(entity.getPercentage());
		dto.setUpdateDate(entity.getUpdateDate());
		dto.setUpdateUser(entity.getUpdateUser());
		dto.setVersion(entity.getVersion());
		return dto;
	}

	public static ContactFinancialInstitutionDTO transformEntityToDTO(ContactFinancialInstitutionEntity entity) {
		ContactFinancialInstitutionDTO dto = new ContactFinancialInstitutionDTO();

		dto.setId(entity.getId());
		dto.setVersion(entity.getVersion());
		dto.setStartDate(entity.getStartDate());
		dto.setEndDate(entity.getEndDate());
		dto.setCreateDate(entity.getCreateDate());
		dto.setCreateUser(entity.getCreateUser());
		dto.setUpdateDate(entity.getUpdateDate());
		dto.setUpdateUser(entity.getUpdateUser());

		if(entity.getFinancialInstitution() != null) {
			dto.setFinancialInstitutionId(entity.getFinancialInstitution().getId());
		}
	
		if(entity.getContact() != null) {
			dto.setContact(ContactServiceTransformer.transformEntityToDTO(entity.getContact()));

			if(dto.getContact() !=null && entity.getContact().getAddress() != null) {
				dto.getContact().setAddress(ContactServiceTransformer.transformEntityToDTO(entity.getContact().getAddress()));
			}
			if(dto.getContact() !=null && entity.getContact().getMailingAddress() != null) {
				dto.getContact().setMailingAddress(ContactServiceTransformer.transformEntityToDTO(entity.getContact().getMailingAddress()));
			}
		}

		return dto;
	}

	public static FinancialInstitutionSigningOfficerDTO transformEntityToDTO(FinancialInstitutionSigningOfficerEntity entity) {
		FinancialInstitutionSigningOfficerDTO dto = new FinancialInstitutionSigningOfficerDTO();
		try {
			BeanUtils.copyProperties(dto, entity);
			if(entity.getFinancialInstitutionMaaPoa() != null) {
				dto.setFinancialInstitutionMaaPoaId(entity.getFinancialInstitutionMaaPoa().getId());
			}
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new VbsRuntimeException(VbsConstants.FAILED_TO_COPY_PROPERTIES, e); 
		}
		return dto;
	}

	public static ContactFinancialInstitutionEntity transformDTOToEntity(ContactFinancialInstitutionDTO dto, ContactFinancialInstitutionEntity entity) {
		entity.setId(dto.getId());
		entity.setVersion(dto.getVersion());
		entity.setStartDate(dto.getStartDate());
		entity.setEndDate(dto.getEndDate());
		entity.setCreateDate(dto.getCreateDate());
		entity.setCreateUser(dto.getCreateUser());
		entity.setUpdateDate(dto.getUpdateDate());
		entity.setUpdateUser(dto.getUpdateUser());

		return entity;
	}

	public static FinancialInstitutionSigningOfficerEntity transformDTOToEntity(FinancialInstitutionSigningOfficerDTO dto, FinancialInstitutionSigningOfficerEntity entity) {
		try {
			BeanUtils.copyProperties(entity, dto);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new VbsRuntimeException(VbsConstants.FAILED_TO_COPY_PROPERTIES, e); 
		}

		return entity;
	}

	public static FinancialInstitutionAlertEntity transformDTOToEntity(FinancialInstitutionAlertDTO dto, FinancialInstitutionAlertEntity entity, FinancialInstitutionEntity financialInstitution) {
		entity.setId(dto.getId());
		entity.setVersion(dto.getVersion());
		entity.setFinancialInstitution(financialInstitution);
		
		NameDescriptionDTO alertTypeDTO = dto.getAlertType();
		if (alertTypeDTO != null) {
			AlertTypeEntity alertTypeEntity = new AlertTypeEntity();
			alertTypeEntity.setId(alertTypeDTO.getId());
			alertTypeEntity.setName(alertTypeDTO.getName());
			alertTypeEntity.setDescription(alertTypeDTO.getDescription());
			entity.setAlertType(alertTypeEntity);
		}
		entity.setName(dto.getName());
		entity.setDescription(dto.getDescription());
		entity.setCreateDate(dto.getCreateDate());
		entity.setCreateUser(dto.getCreateUser());
		entity.setUpdateDate(dto.getUpdateDate());
		entity.setUpdateUser(dto.getUpdateUser());
		entity.setStartDate(dto.getStartDate());
		entity.setEndDate(dto.getEndDate());
		entity.setEndUser(dto.getEndUser());
		return entity;
	}
	
	public static FinancialInstitutionAlertDTO transformEntityToDTO(FinancialInstitutionAlertEntity entity) {
		FinancialInstitutionAlertDTO dto = new FinancialInstitutionAlertDTO();

		if (entity == null) {
			return dto;
		}
		dto.setId(entity.getId());
		dto.setVersion(entity.getVersion());
		if (entity.getFinancialInstitution() != null)
			dto.setFinancialInstitutionId(entity.getFinancialInstitution().getId());
		if (entity.getAlertType() != null) {
			AlertTypeEntity alertTypeEntity = entity.getAlertType();
			NameDescriptionDTO alertTypeDTO = new NameDescriptionDTO(alertTypeEntity.getId(), alertTypeEntity.getName(), alertTypeEntity.getDescription());
			dto.setAlertType(alertTypeDTO);
		}

		dto.setName(entity.getName());
		dto.setDescription(entity.getDescription());
		dto.setCreateDate(entity.getCreateDate());
		dto.setCreateUser(entity.getCreateUser());
		dto.setUpdateDate(entity.getUpdateDate());
		dto.setUpdateUser(entity.getUpdateUser());
		dto.setStartDate(entity.getStartDate());
		dto.setEndDate(entity.getEndDate());
		dto.setEndUser(entity.getEndUser());
		dto.setStatus(entity.getStatus());
		
		return dto;		
	}

}
