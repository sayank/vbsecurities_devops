/* 
 * 
 * LookupService.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import javax.ejb.Local;

import com.tarion.vbs.common.dto.security.SecurityPurposeDTO;
import com.tarion.vbs.orm.entity.security.SecurityPurposeEntity;

/**
 * Lookup Service provides operations to fetch the data for all lookup tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-09
 * @version 1.0
 */
@Local
public interface LookupService extends LookupServiceRemote {
	
	SecurityPurposeDTO transformSecurityPurpose(SecurityPurposeEntity e);

}
