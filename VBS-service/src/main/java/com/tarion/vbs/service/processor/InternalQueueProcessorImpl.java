/* 
 * 
 * InternalQueueProcessorImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.processor;

import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.exceptions.VbsJAXBConversionException;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.service.interest.InterestService;
import com.tarion.vbs.service.jaxb.autorelease.*;
import com.tarion.vbs.service.jaxb.email.allocation.AllocationEmailRequest;
import com.tarion.vbs.service.jaxb.email.ceBlanket.CeBlanketEmailRequest;
import com.tarion.vbs.service.jaxb.interest.CreateInterestCalculation;
import com.tarion.vbs.service.release.AutoReleaseService;
import com.tarion.vbs.service.security.SecurityService;
import com.tarion.vbs.service.util.FmsQueueResponseProcessor;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;

/**
 * Processing messages on the internal JMS queue
 *
 */
@Stateless(mappedName = "ejb/InternalQueueProcessor")
@Interceptors(EjbLoggingInterceptor.class)
public class InternalQueueProcessorImpl implements InternalQueueProcessor {

	@EJB
	private AutoReleaseService autoReleaseService;
	@EJB
	private InterestService interestService;
	@EJB
	private FmsQueueResponseProcessor fmsQueueResponseProcessor;
	@EJB
	private SecurityService securityService;

	@Override
	public void process(AutoReleaseVBApplicationStatusRequest request) {
		try {
			autoReleaseService.getApplicationStatusesFromVBA(request.getAutoReleaseRunId(), request.getVbNumbers());
		} catch (VbsCheckedException e) {
			//Pass exception forward, and let the JMS retry mechanism try the read again
			throw new VbsRuntimeException(e);
		}
	}

	@Override
	public void process(AutoReleaseEvaluateCriteriaRequest request) {
		autoReleaseService.evaluateCriteria(request.getAutoReleaseEnrolmentId());
	}

	@Override
	public void process(AutoReleaseCheckDataReceivedRequest request) {
		autoReleaseService.checkDataReceived(request.getAutoReleaseRunId());
	}

	@Override
	public void process(AutoReleaseCheckCriteriaCompleteRequest request) {
		autoReleaseService.checkCriteriaComplete(request.getAutoReleaseRunId());
	}

	@Override
	public void process(AutoReleaseTriggerFinalizeRequest request) {
		autoReleaseService.finalizeAutoRelease(request.getAutoReleaseRunId(), request.getUserId());
	}

	@Override
	public void process(AutoReleaseCreateReleaseRequest request) {
		autoReleaseService.processCreateRelease(request.getAutoReleaseEnrolmentId(), request.isFullRelease(), request.getReleaseAmount(), request.getUserId(), request.getNumberOfReleases());
	}

	@Override
	public void process(AutoReleaseCheckCreateReleasesCompleteRequest request) {
		autoReleaseService.checkCreateReleasesComplete(request.getAutoReleaseRunId(), request.getNumberOfReleases(), request.getUserId());
	}

	@Override
	public void process(CreateInterestCalculation request){
		interestService.performInterestCalcForDate(LocalDate.parse(request.getInterestDate()));
	}

	@Override
	public void process(AllocationEmailRequest request) throws VbsCheckedException {
		securityService.sendDtaVbApplicationAnalystEmail(request.getSecurityId());
	}

	@Override
	public void process(CeBlanketEmailRequest request) throws VbsCheckedException {
		securityService.sendCEBlanketEmail(request);
	}


	@Override
	public void processReceivedXml(String xmlResponse) {
		Object jaxbObject = null;
		try {
			jaxbObject = VbsUtil.convertXmltoJaxb(xmlResponse);
			Method m = this.getClass().getMethod("process", jaxbObject.getClass());
			m.invoke(this, jaxbObject);
		}catch(VbsJAXBConversionException e) {
			//converting didn't work
			LoggerUtil.logError(InternalQueueProcessorImpl.class, "processReceivedXml ", e, "XML to JAXB Conversion Failed.", xmlResponse);
			throw e;
		}catch(NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
			//error invoking the method (not found/not allowed)
			LoggerUtil.logError(InternalQueueProcessorImpl.class, "processReceivedXml", e, "Unable to invoke process method.", xmlResponse);
			throw new VbsRuntimeException(e.getMessage());
		}catch(Exception e) {
			//other unknown exception/exception thrown from process method
			LoggerUtil.logError(InternalQueueProcessorImpl.class, "processReceivedXml", e, "Error occured during processing.", xmlResponse);
			throw e;
		}
	}


}
