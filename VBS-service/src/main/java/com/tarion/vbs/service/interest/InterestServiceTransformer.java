/* 
 * 
 * SecurityServiceTransformer.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.interest;

import com.tarion.vbs.common.dto.interest.InterestRateDTO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.interest.InterestRateEntity;

import javax.interceptor.Interceptors;


/**
 * Transformer helper class used by Interest Service to provide transformation between DTO and Entity
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-09
 * @version 1.0
 */
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class InterestServiceTransformer {
	
	private InterestServiceTransformer() {}
	
	public static InterestRateEntity transformDTOToEntity(InterestRateDTO dto, InterestRateEntity entity) {
		entity.setId(dto.getId());
		entity.setInterestStartDate(dto.getInterestStartDate());
		entity.setVersion(dto.getVersion());
		entity.setAnnualRate(dto.getAnnualRate());
		return entity;
	}
	
	
	public static InterestRateDTO transformEntityToDTO(InterestRateEntity entity) {
		InterestRateDTO dto = new InterestRateDTO();
		dto.setId(entity.getId());
		dto.setInterestStartDate(entity.getInterestStartDate());
		dto.setVersion(entity.getVersion());
		dto.setAnnualRate(entity.getAnnualRate());
		dto.setCreateDate(entity.getCreateDate());
		dto.setCreateUser(entity.getCreateUser());
		return dto;
	}

}
