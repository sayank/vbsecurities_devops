/* 
 * 
 * SecurityService.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.interest;

import javax.ejb.Local;
import javax.ejb.Schedule;

/**
 * Security Service provides operations for Securities tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-15
 * @version 1.0
 */
@Local
public interface InterestService extends InterestServiceRemote {

    void findDatesRequiringInterestCalculation();

}
