package com.tarion.vbs.service.release;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.autorelease.DataMartVbInfoDTO;
import com.tarion.vbs.common.enums.CriteriaEnum;
import com.tarion.vbs.common.enums.ReleaseStatus;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.dao.alert.AlertDAO;
import com.tarion.vbs.dao.contact.ContactDAO;
import com.tarion.vbs.dao.release.AutoReleaseDAO;
import com.tarion.vbs.dao.release.ReleaseDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.enrolment.EnrolmentDAO;
import com.tarion.vbs.orm.entity.autorelease.*;
import com.tarion.vbs.orm.entity.contact.ContactEntity;
import com.tarion.vbs.orm.entity.contact.LicenseStatusEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import com.tarion.vbs.orm.entity.security.VbPoolEntity;
import org.apache.commons.lang3.math.NumberUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Stateless(mappedName = "ejb/AutoReleaseCriteriaService")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class AutoReleaseCriteriaServiceImpl implements AutoReleaseCriteriaService {

    private static final BigDecimal MAX_VB_DEBT = new BigDecimal(300);
    private static final BigDecimal MAX_PDOG_DEBT = new BigDecimal(500);
    private static final Long[] invalidStatusIds = {
        4L, //NOP to Refuse Application
        5L,	//NOP to Revoke License
        6L,	//NOP to Suspend License
        8L,	//Refused
        9L,	//Revoked
        10L	//Suspended
    };
    private static final int MIN_CREDIT_RATING = 650;
    private static final long LICENSE_STATUS_EXPIRED_ID = 5;

    @EJB
    private EnrolmentDAO enrolmentDAO;
    @EJB
    private ContactDAO contactDAO;
    @EJB
    private GenericDAO genericDAO;
    @EJB
    private AutoReleaseDAO autoReleaseDAO;
    @EJB
    private AlertDAO alertDAO;
    @EJB
    private ReleaseDAO releaseDAO;

    @Override
    public void evaluateCriteria(AutoReleaseEnrolmentEntity autoReleaseEnrolment) {

        SecurityEntity security = autoReleaseEnrolment.getSecurity();
        boolean possessedMoreThanTwoYears = possessedMoreThanTwoYears(security, autoReleaseEnrolment.getEnrolment());

        List<CriteriaEnum> failedCriteria = new ArrayList<>();

        //Rule 1
        //HOME has more than one SECURITY linked to it
        boolean hasMoreThanOneSecurity = enrolmentDAO.countSecuritiesByEnrolmentNumber(autoReleaseEnrolment.getEnrolment().getEnrolmentNumber()) > 1;
        if(hasMoreThanOneSecurity) {
            failedCriteria.add(CriteriaEnum.C01_MORE_THAN_ONE_SECURITY);
        } else {
            //Rule 2
            //SECURITY is not of type Cash or type Letter of Credit
            long secType = security.getSecurityType().getId();
            if(secType != VbsConstants.SECURITY_TYPE_CASH && secType != VbsConstants.SECURITY_TYPE_LETTER_OF_CREDIT) {
                failedCriteria.add(CriteriaEnum.C02_NOT_CASH_OR_LC);
            }

            //Rule 4
            //SECURITY provided by more than one VB
            List<VbPoolEntity> vbList = contactDAO.getVbsForPoolId(security.getPool().getId());
            if(vbList.size() != 1) {
                failedCriteria.add(CriteriaEnum.C04_MORE_THAN_ONE_VB);
            } else {
                ContactEntity vb = vbList.get(0).getVb();
                autoReleaseEnrolment.setVbNumber(vb.getCrmContactId());
                genericDAO.updateEntity(autoReleaseEnrolment, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());

                checkDataMartCriteria(vb, failedCriteria);

                checkVbCriteria(autoReleaseEnrolment, vb, possessedMoreThanTwoYears, failedCriteria);
            }
        }

        checkEnrolmentAndSecurityCriteria(autoReleaseEnrolment, security, possessedMoreThanTwoYears, failedCriteria);

        List<Long> existingCriteria = autoReleaseDAO.getEliminationsForEnrolment(autoReleaseEnrolment.getId());
        for (CriteriaEnum criteria: failedCriteria) {
            createEliminationEntity(autoReleaseEnrolment, existingCriteria, criteria);
        }


    }

    @Override
    public boolean evaluateCriteria18(AutoReleaseEnrolmentEntity autoReleaseEnrolment) {

        //Rule 18
        //Original SECURITY amt per home not exactly $5,000, $10,000 or $15,000 based on Units Intended to Cover
        if(isFailedRule18(autoReleaseEnrolment.getSecurity())) {
            List<Long> existingCriteria = autoReleaseDAO.getEliminationsForEnrolment(autoReleaseEnrolment.getId());
            createEliminationEntity(autoReleaseEnrolment, existingCriteria, CriteriaEnum.C18_ORIGINAL_SECURITY_AMOUNT);
            return false;
        }
        return true;

    }

    private void createEliminationEntity(AutoReleaseEnrolmentEntity autoReleaseEnrolment, List<Long> existingCriteria, CriteriaEnum criteria) {
        if(existingCriteria.stream().noneMatch(c -> c.longValue() == criteria.getId().longValue())) {
            AutoReleaseEliminationEntity eliminationEntity = new AutoReleaseEliminationEntity();
            eliminationEntity.setAutoReleaseEnrolment(autoReleaseEnrolment);
            eliminationEntity.setAutoReleaseCriteria(genericDAO.findEntityById(AutoReleaseCriteriaEntity.class, criteria.getId()));
            genericDAO.addEntity(eliminationEntity, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
        }
    }

    private void checkEnrolmentAndSecurityCriteria(AutoReleaseEnrolmentEntity autoReleaseEnrolment, SecurityEntity security, boolean possessedMoreThanTwoYears, List<CriteriaEnum> failedCriteria) {
        AutoReleaseEnrolmentDataEntity enrolmentData = autoReleaseDAO.getEnrolmentData(autoReleaseEnrolment.getEnrolment().getEnrolmentNumber(), autoReleaseEnrolment.getAutoReleaseRun().getId());
        if(enrolmentData != null) {
            //Rule 6
            //HOME is an inventory risk and possessed for less than two years
            if(enrolmentData.isInventoryItem() && !possessedMoreThanTwoYears) {
                failedCriteria.add(CriteriaEnum.C06_INVENTORY_RISK_LESS_THAN_TWO_YEARS);
            }

            //Rule 19
            //Major un-resolved CRM Case (30-Day, etc.) received for the Home
            if(enrolmentData.getUnresolvedCases() > 0) {
                failedCriteria.add(CriteriaEnum.C19_UNRESOLVED_CASES);
            }

            //If a security instrument has the Security Received Date that is after the WSD/DOP
            if(enrolmentData.getDateOfPossession() != null && security.getReceivedDate().isAfter(enrolmentData.getDateOfPossession())) {
                failedCriteria.add(CriteriaEnum.C25_SECURITY_RECEIVED_DATE_IS_AFTER_WSD_DOP);
            }
        }

        //Rule 13
        //SECURITY to be paid to a third party and not directly to VB
        if(security.isThirdParty()) {
            failedCriteria.add(CriteriaEnum.C13_THIRD_PARTY);
        }

        if(releaseDAO.getNumberOfPendingApprovalReleaseByStatuses(security.getId(), ReleaseStatus.INITIATED) > 0) {
            failedCriteria.add(CriteriaEnum.C26_PENDING_FOR_MANAGERS_APPROVAL);
        }

        //Rule 18
        //Original SECURITY amt per home not exactly $5,000, $10,000 or $15,000 based on Units Intended to Cover
        if(isFailedRule18(security)) {
            failedCriteria.add(CriteriaEnum.C18_ORIGINAL_SECURITY_AMOUNT);
        }
        
        if(!alertDAO.getAlertsBySecurityIdExceptNoteType(security.getId()).isEmpty()) {
        	failedCriteria.add(CriteriaEnum.C24_SECURITY_HAS_ACTIVE_ALERT);
        }
        
    }

    private boolean isFailedRule18(SecurityEntity security) {
        boolean failedRule18 = false;
        int unitsToCover = security.getPool().getUnitsToCover();
        if(security.getOriginalAmount() != null) {
            BigDecimal amountPerHome = security.getOriginalAmount().divide(new BigDecimal(unitsToCover), VbsConstants.ROUNDING_MODE);
            failedRule18 = amountPerHome.compareTo(new BigDecimal(5000)) != 0 &&
                    amountPerHome.compareTo(new BigDecimal(10000)) != 0 &&
                    amountPerHome.compareTo(new BigDecimal(15000)) != 0;
        }
        return failedRule18;
    }

    private boolean possessedMoreThanTwoYears(SecurityEntity security, EnrolmentEntity enrolment) {
        long daysTwoYear = genericDAO.findEntityById(AutoReleaseTimingTypeEntity.class, VbsConstants.AUTO_RELEASE_TIMING_TYPE_TWO_YEAR).getNumberOfDays();
        LocalDateTime date = security.getReceivedDate();
        LocalDateTime wsd = enrolment.getWarrantyStartDate();
        if(date == null && wsd == null) {
            return false;
        }
        if(date == null || wsd.isAfter(date)) {
            date = wsd;
        }

        date = date.truncatedTo(ChronoUnit.DAYS);

        long daysPossessed = Duration.between(date, DateUtil.getLocalDateTimeStartOfDay()).toDays();

        return daysPossessed > daysTwoYear;
    }

    private void checkDataMartCriteria(ContactEntity vb, List<CriteriaEnum> failedCriteria) {
        DataMartVbInfoDTO dataMartInfo = autoReleaseDAO.getDataMartVbInfo(vb.getCrmContactId());
        if(dataMartInfo != null) {
            //Rule 5
            //VB has enrolments pending SECURITY
            if (dataMartInfo.getEnrolmentPendingSecurity() == 1) {
                failedCriteria.add(CriteriaEnum.C05_VB_ENROLMENTS_PENDING_SECURITY);
            }

            //Rule 9
            //VB owes Tarion more than $300
            if (dataMartInfo.getVbDebt() != null && dataMartInfo.getVbDebt().compareTo(MAX_VB_DEBT) > 0) {
                failedCriteria.add(CriteriaEnum.C09_VB_DEBT);
            }

            //Rule 10
            //VBs Total PDOG Debt > $500
            if (dataMartInfo.getPdogDebt() != null && dataMartInfo.getPdogDebt().compareTo(MAX_PDOG_DEBT) > 0) {
                failedCriteria.add(CriteriaEnum.C10_VB_PDOG_DEBT);
            }

            //Rule 11
            //VB has active reserves
            if(dataMartInfo.getActiveReserve() == 1) {
                failedCriteria.add(CriteriaEnum.C11_VB_HAS_RESERVES);
            }

            //Rule 16
            //VB has had demands on any of their SECURITY in the past
            if(dataMartInfo.getDemand() == 1) {
                failedCriteria.add(CriteriaEnum.C16_VB_HAS_HAD_DEMANDS);
            }
        }
    }


    private void checkVbCriteria(AutoReleaseEnrolmentEntity autoReleaseEnrolment, ContactEntity vb, boolean possessedMoreThanTwoYears, List<CriteriaEnum> failedCriteria) {

        AutoReleaseVbApplicationStatusEntity vbApplicationStatus = autoReleaseDAO.getVbApplicationStatus(vb.getCrmContactId(), autoReleaseEnrolment.getAutoReleaseRun().getId());
        //Rule 17
        //VB is currently Revoked, Refused, Suspended, NOPed or Pending Approval
        if(!isValidApplicationStatus(vbApplicationStatus)) {
            failedCriteria.add(CriteriaEnum.C17_VB_INVALID_APPLICATION_STATUS);
        }

        AutoReleaseVbDataEntity vbData = autoReleaseDAO.getVbData(vb.getCrmContactId(), autoReleaseEnrolment.getAutoReleaseRun().getId());
        if(vbData != null) {

            //Rule 12
            //Vb is under investigation
            if(vbData.isUnderInvestigation()) {
                failedCriteria.add(CriteriaEnum.C12_VB_UNDER_INVESTIGATION);
            }

            //Rule 15
            //VB has active alerts
            if(vbData.isActiveAlert()) {
                failedCriteria.add(CriteriaEnum.C15_VB_ACTIVE_ALERTS);
            }


            //Rule 20
            //VB Expired and has outstanding CCPs
            if(vbData.getOsCCPS() != null && vbData.getOsCCPS() > 0) {
                failedCriteria.add(CriteriaEnum.C20_VB_EXPIRED_OS_CCPS);
            }

            //Rule 21
            //VB credit rating < 650 or Average of Guarantor credit rating < 650
            boolean checkGuarantors = true;
            if(NumberUtils.isParsable(vbData.getCreditRating())) {
                int creditRating = Integer.parseInt(vbData.getCreditRating());
                if(creditRating < MIN_CREDIT_RATING) {
                    failedCriteria.add(CriteriaEnum.C21_CREDIT_RATING);
                    checkGuarantors = false;
                }
            }
            if(checkGuarantors && vbData.getNumGuarantors() != null && vbData.getNumGuarantors() > 0 && NumberUtils.isParsable(vbData.getAvgGuarantorCreditRating())) {
                int creditRating = Integer.parseInt(vbData.getAvgGuarantorCreditRating());
                if(creditRating < MIN_CREDIT_RATING) {
                    failedCriteria.add(CriteriaEnum.C21_CREDIT_RATING);
                }
            }

            //Rule 22
            //VB is Unwilling/Unable
            if(vbData.isUnwillingUnable()) {
                failedCriteria.add(CriteriaEnum.C22_VB_UNWILLING_UNABLE);
            }

            //Rule 23
            //VB is expired and possessed for less than two years
            if(!possessedMoreThanTwoYears) {
                LicenseStatusEntity licenseStatus = contactDAO.getLicenseStatusByCrmContactId(vbData.getVbNumber());
                if (licenseStatus != null && licenseStatus.getId() == LICENSE_STATUS_EXPIRED_ID) {
                    failedCriteria.add(CriteriaEnum.C23_VB_EXPIRED);
                }
            }
        }
    }

    private boolean isValidApplicationStatus(AutoReleaseVbApplicationStatusEntity vbApplicationStatus) {
        if(vbApplicationStatus == null || vbApplicationStatus.getApplicationStatus() == null) {
            return true;
        }

        return Arrays.stream(invalidStatusIds).noneMatch(id -> vbApplicationStatus.getApplicationStatus().longValue() == id.longValue());
    }
}
