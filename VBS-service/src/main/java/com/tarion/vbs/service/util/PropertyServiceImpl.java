/* 
 * 
 * PropertyServiceImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.dao.util.PropertyDAO;
import com.tarion.vbs.orm.entity.util.PropertiesEntity;


/**
 * Session Bean to Access the Properties File table, to use DAO to retrieve properties stored in VBS_CONFIG table
 * and merge them into file properties
 *
 */
@Singleton(mappedName = "ejb/PropertyService")
public class PropertyServiceImpl implements PropertyService {
	
	private Properties props;

	@EJB
	private PropertyDAO propertyDAO;
    /**
     * Default constructor. 
     */
    public PropertyServiceImpl() {
    	//constructor
    }
    
    /**
     * Startup.
     */
    @Override
	@PostConstruct
    public void startup() {
		try {
			props = new Properties();
			InputStream in = this.getClass().getClassLoader().getResourceAsStream(VbsConstants.PROPERTIES_FILE);
			props.load(in);
			in.close();
		} catch (Exception e) {
			String errMsg = "Problem loading " + VbsConstants.PROPERTIES_FILE;
			LoggerUtil.logError(PropertyServiceImpl.class, "startup", errMsg);
		}

        List<PropertiesEntity> propertiesList = propertyDAO.findAllVBApplicationsProperties();
        for (PropertiesEntity properties : propertiesList) {
            props.setProperty(properties.getName(), properties.getValue());
        }
    }
    
    /**
     * Given a key, return the value in the database.
     *
     * @param key the key
     * @return The value
     * @throws VbsRuntimeException if the property is not found
     */
    @Override
    public String getValue(String key) {
        String prop = props.getProperty(key);
        if(prop == null){
            throw new VbsRuntimeException("Property Not Found: " + key);
        }
        return props.getProperty(key);
    }

    @Override
    public String getValueOrNull(String key){
        return props.getProperty(key);
    }

    /**
     * Method to get value of property key with list of values to format the
     * string.
     *
     * @param key the key
     * @param values The values to substitute into the string
     * @throws VbsRuntimeException if the property is not found
     * @return The property
     */
    @Override
    public String getValue(String key, String[] values) {
        String tempString = props.getProperty(key);
        if(tempString == null){
            throw new VbsRuntimeException("Property Not Found: " + key);
        }

        if (tempString != null && values != null) {
            for (int i = 0; i < values.length; i++) {
                tempString = tempString.replaceAll("\\{" + i + "\\}", values[i]);
            }
        }
        return tempString;
    }

    /**
     * Returns true if and only if the value is "true" in the properties table.
     *
     * @param key the key
     * @throws VbsRuntimeException if the property is not found
     * @return True if and only if the value is "true" in the properties table
     */
    @Override
    public boolean getBooleanValue(String key) {
        String testStr = getValue(key);
        if(testStr == null){
            throw new VbsRuntimeException("Property Not Found: " + key);
        }
        return VbsConstants.TRUE.equalsIgnoreCase(testStr);
    }   
    
    @Override
    public void setProperty(String key, String value) {
    	props.setProperty(key, value);
    }

}
