/* 
 * 
 * LookupServiceRemote.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import java.util.Map;
import javax.ejb.Remote;

/**
 * Lookup Service provides operations to fetch the data for all lookup tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-09
 * @version 1.0
 */
@Remote
public interface LookupServiceRemote {


	Map<String, Object> getUiSetupMap();
}
