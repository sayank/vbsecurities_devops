package com.tarion.vbs.service.release;

import com.tarion.vbs.common.constants.PermissionConstants;
import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.dto.NameDescriptionDTO;
import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentDTO;
import com.tarion.vbs.common.dto.release.PendingReleaseDTO;
import com.tarion.vbs.common.dto.release.ReleaseDTO;
import com.tarion.vbs.common.dto.release.ReleaseEnrolmentDTO;
import com.tarion.vbs.common.dto.release.ReleaseSearchParametersDTO;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.enums.ErrorTypeEnum;
import com.tarion.vbs.common.enums.ReleaseApprovalStatusEnum;
import com.tarion.vbs.common.enums.ReleaseStatus;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.exceptions.VbsValidationException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.contact.ContactDAO;
import com.tarion.vbs.dao.release.ReleaseDAO;
import com.tarion.vbs.dao.security.SecurityDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.dao.util.LookupDAO;
import com.tarion.vbs.enrolment.EnrolmentDAO;
import com.tarion.vbs.orm.entity.contact.ContactEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentPoolEntity;
import com.tarion.vbs.orm.entity.release.*;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import com.tarion.vbs.service.contact.ContactService;
import com.tarion.vbs.service.contact.ContactServiceTransformer;
import com.tarion.vbs.service.enrolment.EnrolmentServiceTransformer;
import com.tarion.vbs.service.jaxb.crm.warrantyservicesinput.WarrantyServicesInputRequest;
import com.tarion.vbs.service.jaxb.fms.drawdown.CreateDrawDownRequest;
import com.tarion.vbs.service.jaxb.fms.release.SecurityReleaseRequest;
import com.tarion.vbs.service.userprofile.UserService;
import com.tarion.vbs.service.util.*;
import createworklistrequest.CreateWorklistRequest;
import createworklistresponse.CreateWorklistResponse;

import org.apache.commons.lang3.StringUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.Tuple;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Release Service provides operations for release
 *
 * @author <A href="shadi.kajevand@tarion.com">Shadi Kajevand</A>
 * @since 2018-12-01
 * @version 1.0
 */
@Stateless(mappedName = "ejb/ReleaseService")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class ReleaseServiceImpl implements ReleaseService, ReleaseServiceRemote {

	@EJB
	private ReleaseDAO releaseDAO;
	@EJB
	private LookupDAO lookupDAO;
	@EJB
	private SecurityDAO securityDAO;
	@EJB
	private ContactDAO contactDAO;
	@EJB
	private GenericDAO genericDAO;
	@EJB
	private EnrolmentDAO enrolmentDAO;
	@EJB
	private PropertyService propertyService;
	@EJB
	private UserService userService;
	@EJB
	private EmailWSInvocationService emailWSInvocationService;
	@EJB
	private JmsMessageSenderService jmsMessageSenderService;
	@EJB
	private ContactService contactService;
	@EJB
	private CrmWSInvocationService crmWSInvocationService;
	
	private static final String REPLACED_BY = "replacedBy";

	@Override
	public List<ReleaseDTO> getReleases(Long securityId) {
		List<ReleaseEntity> releases = releaseDAO.getReleases(securityId);
		List<ReleaseDTO> releaseDTOs = new ArrayList<>();
		for (ReleaseEntity release : releases) {
			ReleaseDTO dto = getReleaseDTO(release);
			releaseDTOs.add(dto);
		}
		return releaseDTOs;
	}

	@Override
	public Long saveRelease(ReleaseDTO releaseDTO, UserDTO userDTO) throws VbsCheckedException {

		ReleaseEntity currentEntity = (releaseDTO.getId() != null) ? genericDAO.findEntityById(ReleaseEntity.class, releaseDTO.getId()) : null;
		List<ErrorDTO> errors = validateRelease(releaseDTO, currentEntity, userDTO);

		if(!errors.isEmpty()){
			throw new VbsRuntimeException("Attempted to save with validation errors");
		}

		LocalDateTime time = LocalDateTime.now();
		ReleaseEntity entity = this.createReleaseEntity(releaseDTO, currentEntity, userDTO, time);

		this.setReleaseReasons(releaseDTO, entity, currentEntity, userDTO, time);
		this.setReleaseEnrolments(releaseDTO, currentEntity, entity, userDTO, time);
		this.sendEmail(entity, currentEntity, userDTO);
		this.sendWarrantyServiceToCrm(releaseDTO, entity, currentEntity, time);
		this.updateAutoReleaseValues(releaseDTO, entity, currentEntity, releaseDTO.getEnrolments(), userDTO);
		return entity.getId();
	}

    private void updateAutoReleaseValues(ReleaseDTO release, ReleaseEntity entity, ReleaseEntity currentEntity, List<ReleaseEnrolmentDTO> releaseEnrolmentDTOList, UserDTO userDTO){
        if(currentEntity == null || currentEntity.getFinalApprovalStatus() == null) {
            if(release.getFinalApprovalStatus() != null && release.getFinalApprovalStatus().equals(ReleaseApprovalStatusEnum.Authorized)) {
                List<String> enrolmentNumberList = new ArrayList<>();
                for (ReleaseEnrolmentDTO releaseEnrolmentDTO : releaseEnrolmentDTOList) {

                    if (releaseEnrolmentDTO.isFullRelease() != null && releaseEnrolmentDTO.isFullRelease()) {
                        enrolmentNumberList = new ArrayList<>();
                        enrolmentNumberList.add(releaseEnrolmentDTO.getEnrolment().getEnrolmentNumber());
                    }
                }
                if(!enrolmentNumberList.isEmpty()) {
                    List<EnrolmentEntity> enrolmentList = enrolmentDAO.getEnrolmentsByEnrolmentNumber(enrolmentNumberList);
                    for (EnrolmentEntity enrolmentEntity : enrolmentList) {
                        enrolmentEntity.setExcessDeposit(false);
                        enrolmentEntity.setExcessDepositRelease(false);
                        enrolmentEntity.setAutoOneYear(false);
                        enrolmentEntity.setAutoOneYearRfc(false);
                        enrolmentEntity.setAutoTwoYear(false);
                        genericDAO.updateEntity(enrolmentEntity, userDTO.getUserId(), LocalDateTime.now());
                    }
                }
            }
        }
    }

	@Override
	public List<EnrolmentDTO> getUnreleasedEnrolmentsForSecurity(Long securityId) {
		List<EnrolmentDTO> enrolmentList = new ArrayList<>();
		List<EnrolmentEntity> enrolments = enrolmentDAO.getUnreleasedEnrolmentsForSecurity(securityId);
		for (EnrolmentEntity enrolment : enrolments) {
			EnrolmentDTO dto = EnrolmentServiceTransformer.transformEntityToDTO(enrolment);
			enrolmentList.add(dto);
		}
		return enrolmentList;
	}

	@Override
	public Long cancelRelease(ReleaseDTO release, UserDTO userDTO) throws VbsCheckedException {
		ReleaseEntity releaseEntity = genericDAO.findEntityById(ReleaseEntity.class, release.getId());
		if (releaseEntity == null) {
			throw new VbsCheckedException("No release found to cancel for: " , release.toString());
		}
		if (releaseEntity.getStatus() != ReleaseStatus.PENDING) {
			throw new VbsCheckedException("Could not cancel release, it has already been sent");
		}

		RejectReasonEntity rejectReasonEntity = genericDAO.findEntityById(RejectReasonEntity.class, VbsConstants.REJECT_REASON_CANCEL_REVERSE_PAYMENT);

		releaseEntity.setStatus(ReleaseStatus.CANCELLED);
		releaseEntity.setFinalApprovalStatus(ReleaseApprovalStatusEnum.Rejected);
		releaseEntity.setFinalApprovalBy(userDTO.getUserId());
		releaseEntity.setFinalApprovalDate(LocalDateTime.now());
		releaseEntity.setManagerRejectReason(rejectReasonEntity);
		releaseEntity.setAuthorizedAmount(BigDecimal.ZERO);
		releaseEntity.setAnalystApprovalBy(userDTO.getUserId());
		releaseEntity.setAnalystApprovalDate(LocalDateTime.now());
		releaseEntity.setAnalystApprovalStatus(ReleaseApprovalStatusEnum.Rejected);
		releaseEntity.setAnalystRejectReason(rejectReasonEntity);
		genericDAO.updateEntity(releaseEntity, userDTO.getUserId(), LocalDateTime.now());

		List<ReleaseEnrolmentEntity> enrolments = releaseDAO.getEnrolmentsByReleaseId(release.getId());
		if(enrolments!=null && !enrolments.isEmpty()) {
			for (ReleaseEnrolmentEntity enrolment : enrolments) {
				enrolment.setAnalystApprovalStatus(ReleaseApprovalStatusEnum.Rejected);
				enrolment.setAuthorizedAmount(BigDecimal.ZERO);
				enrolment.setRejectReason(rejectReasonEntity);
				genericDAO.updateEntity(enrolment, userDTO.getUserId(), LocalDateTime.now());
			}
		}

		return releaseEntity.getId();
	}

	@Override //TODO review
	public List<PendingReleaseDTO> getReleaseListByStatus(ReleaseStatus releaseStatus) {
		List<ReleaseEntity> releaseList = releaseDAO.getReleaseByStatus(releaseStatus);
		return getPendingReleaseDTOS(releaseList);
	}

	@Override //TODO review
	public List<PendingReleaseDTO> getReleaseListByStatuses(ReleaseStatus releaseStatus1, ReleaseStatus releaseStatus2) {
		List<ReleaseEntity> releaseList = releaseDAO.getReleaseByStatuses(releaseStatus1, releaseStatus2);
		return getPendingReleaseDTOS(releaseList);
	}

	@Override
	public List<PendingReleaseDTO> searchReleases(ReleaseSearchParametersDTO params) {
		return releaseDAO.searchReleases(params).stream().map(this::recalculateReleaseInterest).collect(Collectors.toList());
	}
	
	@Override
	public List<ErrorDTO> validateSendReleaseListToFms(List<PendingReleaseDTO> pendingReleaseDTOList){
		List<ErrorDTO> errorDTOs = new ArrayList<ErrorDTO>();
		
		List<Long> releaseIds = pendingReleaseDTOList
									.stream()
									.map(pendingReleaseDTO -> pendingReleaseDTO.getReleaseId())
									.distinct()
									.collect(Collectors.toList());
		List<Long> securityIds = pendingReleaseDTOList
									.stream()
									.map(pendingReleaseDTO -> pendingReleaseDTO.getSecurityId())
									.distinct()
									.collect(Collectors.toList());;
		
		List<Tuple> tuples =  releaseDAO.getSecurityIdReleaseIdByStatus(ReleaseStatus.PENDING, securityIds);
		
		if (VbsUtil.isNullorEmpty(tuples)) {
			return errorDTOs;
		}
		
		tuples.forEach((item) -> {
			if (!releaseIds.contains(item.get("releaseId"))) {
				errorDTOs.add(
					new ErrorDTO(
						"Not all security releases selected.",
						"pendingReleases",
						ErrorTypeEnum.ALERT_FAIL,
						"Not all security releases selected",
						"You've selected a release from a security that has more pending releases. Please select all the pending releases from a single security and try again.",
						"PR_NOT_ALL_SELECTED"));
				throw new VbsValidationException(errorDTOs);
			}
		});

		return errorDTOs;
	}

	@Override
	public List<PendingReleaseDTO> sendReleaseListToFms(List<PendingReleaseDTO> pendingReleaseDTOList, UserDTO user) throws VbsCheckedException {
		validateSendReleaseListToFms(pendingReleaseDTOList);
		
		LocalDateTime time = LocalDateTime.now();
		for (PendingReleaseDTO pendingReleaseDTO : pendingReleaseDTOList) {
			if(pendingReleaseDTO.isSelected()) {
				SecurityReleaseRequest request = new SecurityReleaseRequest();
				ReleaseEntity releaseEntity = genericDAO.findEntityById(ReleaseEntity.class, pendingReleaseDTO.getReleaseId());
				if(releaseEntity.getStatus() == ReleaseStatus.PENDING) {
					SecurityEntity security = releaseEntity.getSecurity();
					request.setAuthorizedAmount(VbsUtil.getStringForMoney(pendingReleaseDTO.getPrincipalAmount()));
					request.setFinalApprovalBy(pendingReleaseDTO.getApprovalManager());
					request.setFinalApprovalDate(DateUtil.getDateFormattedForFms(pendingReleaseDTO.getManagerApprovalDate()));
					request.setId(releaseEntity.getId());
					request.setInstrumentNumber(pendingReleaseDTO.getInstrumentNumber());
					if(pendingReleaseDTO.getPayee() != null && pendingReleaseDTO.getPayee().getCrmContactId() != null) {
						request.setPayee(pendingReleaseDTO.getPayee().getCrmContactId());
					}
					if (pendingReleaseDTO.getInterestAmount() != null) {
						releaseEntity.setInterestAmount(pendingReleaseDTO.getInterestAmount());
						request.setInterestAmount(VbsUtil.getStringForMoney(pendingReleaseDTO.getInterestAmount()));
					}
					request.setRegularRelease(releaseEntity.isRegularRelease());
					request.setSecurityId(security.getId());
					request.setSequenceNumber(releaseEntity.getSequenceNumber());
					request.setVbNumber(pendingReleaseDTO.getVbNumber());
					releaseEntity.setStatus(ReleaseStatus.SENT);
					String xmlMessageText = VbsUtil.convertJaxbToXml(request, SecurityReleaseRequest.class);

					sendDemandOrDrawDownToFMS(releaseEntity);

					if(VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR != releaseEntity.getReleaseType().getId()) {
						jmsMessageSenderService.postFmsRequestQueue(xmlMessageText, SecurityReleaseRequest.class.getSimpleName());
						jmsMessageSenderService.insertTransactionLog(JmsMessageTypeEnum.getFmsSecurityReleaseRequestTracking(request.getId(), request.getSequenceNumber()), xmlMessageText);
					}

					if(pendingReleaseDTO.getPrincipalAmount() != null && pendingReleaseDTO.getPrincipalAmount().compareTo(BigDecimal.ZERO) != 0){
						security.setCurrentAmount(security.getCurrentAmount().subtract(pendingReleaseDTO.getPrincipalAmount()));
						genericDAO.updateEntity(security, user.getUserId().toUpperCase(), time);
					}

					genericDAO.updateEntity(releaseEntity, user.getUserId(), time);
				}
			}
		}
		return pendingReleaseDTOList;
	}

	@Override //TODO review
	public ReleaseDTO setupRelease(Long releaseTypeId, Long securityId, UserDTO userDTO){
		if(releaseTypeId == null || securityId == null || userDTO == null){ throw new VbsRuntimeException("Attempted to setup release without releaseTypeId/securityId/userDTO");}
		ReleaseTypeEntity releaseType = lookupDAO.getReleaseTypeByReleaseTypeId(releaseTypeId);
		if(releaseType == null){ throw new VbsRuntimeException("Release type " + releaseTypeId + " not found."); }
		SecurityEntity securityEntity = genericDAO.findEntityById(SecurityEntity.class, securityId);
		if(securityEntity == null){ throw new VbsRuntimeException("Security " + securityId + " not found."); }

		ReleaseDTO dto = new ReleaseDTO();
		dto.setReleaseType(new NameDescriptionDTO(releaseType.getId(), releaseType.getName(), releaseType.getDescription()));
		dto.setRegularRelease(securityEntity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_CASH) != 0);

		dto.setReasons(new ArrayList<>());
		dto.setProvidedByVb(ContactServiceTransformer.transformEntityToDTO(securityEntity.getPrimaryVb()));

		ReasonEntity reason = null;

		if(releaseTypeId.compareTo(VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR) == 0 || releaseTypeId.compareTo(VbsConstants.RELEASE_TYPE_DEMAND_LU) == 0){
			ContactDTO payTo = ContactServiceTransformer.transformEntityToDTO(contactDAO.getContactByCrmContactId(VbsConstants.CONTACT_TARION_ID));
			dto.setPayTo(payTo);
		}
		if(releaseTypeId.compareTo(VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR) == 0){
			reason = lookupDAO.getReasonByReasonId(7L);
			dto.setAnalystApprovalStatus(ReleaseApprovalStatusEnum.Authorized);
			dto.setAnalystApprovalDateTime(DateUtil.getLocalDateTimeNow());
			dto.setAnalystApprovedBy(userDTO.getUserId());
			dto.setFinalApprovalSentTo(userDTO.getUserId());
		}else if(releaseTypeId.compareTo(VbsConstants.RELEASE_TYPE_DEMAND_LU) == 0){
			reason = lookupDAO.getReasonByReasonId(12L);
		}else if(releaseTypeId.compareTo(VbsConstants.RELEASE_TYPE_RELEASE) == 0){
			if(securityEntity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_CASH) == 0) {
				dto.setRegularRelease(null);
			}
			List<EnrolmentPoolEntity> enrolmentPoolList = enrolmentDAO.getEnrolmentsByPoolId(securityEntity.getPool().getId());
			if(enrolmentPoolList != null && enrolmentPoolList.size() == 1){
				ReleaseEnrolmentDTO reDTO = new ReleaseEnrolmentDTO();
				EnrolmentDTO enrolmentDTO = new EnrolmentDTO();
				enrolmentDTO.setEnrolmentNumber(enrolmentPoolList.get(0).getEnrolment().getEnrolmentNumber());
				reDTO.setEnrolment(enrolmentDTO);
				dto.getEnrolments().add(reDTO);
			}

		}else if(releaseTypeId.compareTo(VbsConstants.RELEASE_TYPE_REPLACE) == 0){
			reason = lookupDAO.getReasonByReasonId(13L);
		}
		if(reason != null) {
			dto.getReasons().add(new NameDescriptionDTO(reason.getId(), reason.getName(), reason.getDescription()));
		}

		return dto;
	}

	@Override //TODO review
	public List<PendingReleaseDTO> getReleasesByStatusesFromToDate(List<ReleaseStatus> releaseStatuses, LocalDateTime startDate, LocalDateTime endDate) {
		List<ReleaseEntity> releases = releaseDAO.getReleasesByStatusesFromToDate(releaseStatuses, startDate, endDate);
		List<PendingReleaseDTO> releaseDTOList = new ArrayList<>();
		for (ReleaseEntity releaseEntity : releases) {
			PendingReleaseDTO pendingReleaseDTO = new PendingReleaseDTO();
			ReleaseServiceTransformer.transformEntityToDTO(pendingReleaseDTO, releaseEntity);
			pendingReleaseDTO.setVbNumber(releaseEntity.getSecurity().getPrimaryVb().getCrmContactId());
			pendingReleaseDTO.setVbName(releaseEntity.getSecurity().getPrimaryVb().getCompanyName());
			releaseDTOList.add(pendingReleaseDTO);
		}
		return releaseDTOList;
	}

	@Override
	public List<ErrorDTO> validateRelease(ReleaseDTO release, UserDTO user){
		ReleaseEntity currentEntity = (release.getId() != null) ? genericDAO.findEntityById(ReleaseEntity.class, release.getId()) : new ReleaseEntity();
		return this.validateRelease(release, currentEntity, user);
	}

	@Override
	public void setReleaseInterestAmounts(ReleaseEntity entity, BigDecimal authorizedAmount, String userId, LocalDateTime time) {
		setReleaseInterestAmounts(null, entity, authorizedAmount, userId, time);
	}

	@Override
	public void setReleaseInterestAmounts(ReleaseDTO releaseDTO, ReleaseEntity entity, BigDecimal authorizedAmount, String userId, LocalDateTime time){
		entity.setAuthorizedAmount(authorizedAmount);
		SecurityEntity security = entity.getSecurity();
		if(ReleaseApprovalStatusEnum.Rejected.equals(entity.getFinalApprovalStatus())){
			entity.setInterestAmount(BigDecimal.ZERO);
			entity.setPrincipalAmount(BigDecimal.ZERO);
			entity.setAuthorizedAmount(BigDecimal.ZERO);
		}else if(authorizedAmount != null){
			BigDecimal availableAmount = entity.getSecurity().getAvailableAmount();
			if(entity.getReleaseType().getId().longValue() != VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR && authorizedAmount.compareTo(availableAmount) > 0){
				throw new VbsRuntimeException("Trying to release more than what is available.");
			}else if(entity.getReleaseType().getId().longValue() == VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR && authorizedAmount.compareTo(availableAmount.add(entity.getSecurity().getAvailableInterest())) > 0){
				throw new VbsRuntimeException("Trying to release more than what is available.");
			}

			if(security.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_CASH) == 0){
				BigDecimal availableInterest = entity.getSecurity().getAvailableInterest();

				if(entity.getReleaseType().getId().compareTo(VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR) == 0){
					if(authorizedAmount.compareTo(availableInterest) < 1){
						entity.setInterestAmount(authorizedAmount);
						entity.setPrincipalAmount(BigDecimal.ZERO);
					}else{
						entity.setInterestAmount(availableInterest);
						entity.setPrincipalAmount(authorizedAmount.subtract(availableInterest));
					}
				}else if(entity.getReleaseType().getId().compareTo(VbsConstants.RELEASE_TYPE_RELEASE) == 0){
					if(entity.isRegularRelease()){
						entity.setPrincipalAmount(authorizedAmount);
						entity.setInterestAmount(authorizedAmount.divide(availableAmount, 5, VbsConstants.ROUNDING_MODE).multiply(availableInterest).setScale(VbsConstants.DECIMAL_SCALE, VbsConstants.ROUNDING_MODE));
					}else{
						entity.setInterestAmount(authorizedAmount);
						entity.setPrincipalAmount(BigDecimal.ZERO);
					}
				}else if(entity.getReleaseType().getId().compareTo(VbsConstants.RELEASE_TYPE_REPLACE) == 0){
					entity.setInterestAmount(availableInterest);
					entity.setPrincipalAmount(availableAmount);
				}
			}else{
				entity.setInterestAmount(BigDecimal.ZERO);
				entity.setPrincipalAmount(authorizedAmount);
			}
			SecurityEntity securityEntity = entity.getSecurity();
			if(releaseDTO != null && entity.getSecurity().getSecurityType().getId().longValue() == VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) {
				if(entity.getReleaseType().getId().compareTo(VbsConstants.RELEASE_TYPE_REPLACE) == 0 && security.getAvailableAmount().compareTo(authorizedAmount) == 0){
					changeSecurityStatus(releaseDTO, VbsConstants.RELEASE_DTA_REPLACED, VbsConstants.SECURITY_STATUS_REPLACED, securityEntity);
				}
				changeSecurityStatus(releaseDTO, VbsConstants.RELEASE_DTA_SPLIT, VbsConstants.SECURITY_STATUS_ACCEPTED, securityEntity);

				Optional<ReleaseEnrolmentDTO> optFullRelease = releaseDTO.getEnrolments().stream().filter(e -> (e.isFullRelease() != null && e.isFullRelease())).findFirst();
				if (optFullRelease.isPresent()) {
					changeSecurityStatus(releaseDTO, VbsConstants.RELEASE_DTA_MERGE, VbsConstants.SECURITY_STATUS_TERMINATED, securityEntity);
					changeSecurityStatus(releaseDTO, VbsConstants.RELEASE_DTA_END_OF_PROJECT, VbsConstants.SECURITY_STATUS_TERMINATED, securityEntity);
					changeSecurityStatus(releaseDTO, VbsConstants.RELEASE_CANCEL_PROJ_ENRO, VbsConstants.SECURITY_STATUS_CANCELLED, securityEntity);
				}
			}
			if(entity.getStatus().equals(ReleaseStatus.COMPLETED)){
				entity.getSecurity().setCurrentAmount(entity.getSecurity().getCurrentAmount().subtract(entity.getPrincipalAmount()));
				genericDAO.updateEntity(securityEntity, userId.toUpperCase(), time);
			}
		}
	}
	private void changeSecurityStatus(ReleaseDTO releaseDTO, long releaseReason, long securityStatus, SecurityEntity securityEntity) {
		Optional<NameDescriptionDTO> dtaReason = releaseDTO.getReasons().stream().filter(r -> r.getId().equals(releaseReason)).findFirst();
		if (dtaReason.isPresent() && releaseDTO.getFinalApprovalStatus() == ReleaseApprovalStatusEnum.Authorized) {
			securityEntity.setStatus(lookupDAO.getSecurityStatusById(securityStatus));
		}
	}
	@Override
	public ReleaseDTO getReleaseDTO(Long releaseId) {
		ReleaseEntity release = genericDAO.findEntityById(ReleaseEntity.class, releaseId);
		return getReleaseDTO(release);
	}

	private ReleaseDTO getReleaseDTO(ReleaseEntity release){
		ReleaseDTO releaseDTO = ReleaseServiceTransformer.transformEntityToDTO(release);
		List<ReleaseEnrolmentEntity> enrolments = releaseDAO.getEnrolmentsByReleaseId(release.getId());
		List<ReleaseEnrolmentDTO> enrolmentsDTOList = new ArrayList<>();
		NameDescriptionDTO releaseType = new NameDescriptionDTO();
		if(release.getReleaseType() != null) {
			releaseType.setId(release.getReleaseType().getId());
			releaseType.setName(release.getReleaseType().getName());
			releaseType.setDescription(release.getReleaseType().getDescription());
			releaseDTO.setReleaseType(releaseType);
		}

		releaseDTO.setRecommendedRejectedBy(release.getAnalystApprovalBy());
		Boolean fullRelease = true;
		for (ReleaseEnrolmentEntity enrolment : enrolments) {
			ReleaseEnrolmentDTO releaseEnrolment = ReleaseServiceTransformer.transformEntityToDTO(enrolment, release.getId());
			EnrolmentDTO enrolmentDTO = new EnrolmentDTO();
			enrolmentDTO.setEnrolmentNumber(enrolment.getEnrolment().getEnrolmentNumber());
			if(enrolment.getEnrolment() != null && enrolment.getEnrolment().getEnrollingVb() != null && enrolment.getEnrolment().getEnrollingVb().getCrmContactId() != null) {
				enrolmentDTO.setEnrollingVb(enrolment.getEnrolment().getEnrollingVb().getCrmContactId());
			}
			releaseEnrolment.setEnrolment(enrolmentDTO);
			releaseDTO.setRecommendedRejectedBy(enrolment.getAnalystApprovalBy());
			enrolmentsDTOList.add(releaseEnrolment);
			if(releaseEnrolment.isFullRelease() != null && fullRelease != null &&!releaseEnrolment.isFullRelease()) {
				fullRelease = false;
			}
			if(releaseEnrolment.isFullRelease() == null) {
				fullRelease = null;
			}
		}
		releaseDTO.setEnrolments(enrolmentsDTOList);
		releaseDTO.setFullRelease(enrolments.isEmpty() ? null : fullRelease);
		if (release.getSecurity() != null) {
			releaseDTO.setProvidedByVb(ContactServiceTransformer.transformEntityToDTO(release.getSecurity().getPrimaryVb()));
		}
		if (release.getRequestAmount() != null && release.getAuthorizedAmount() != null) {
			releaseDTO.setLuRetainAmount(release.getRequestAmount().subtract(release.getAuthorizedAmount()));
		}
		if(release.getContact() != null) {
			releaseDTO.setPayTo(ContactServiceTransformer.transformEntityToDTO(release.getContact()));
		}
		if(release.getReplacementPayeeContact() != null) {
			releaseDTO.setReplacementPayeeCRMContactId(release.getReplacementPayeeContact().getCrmContactId());
			String replacementPayee = "";
			if(release.getReplacementPayeeContact().getCompanyName() != null && !release.getReplacementPayeeContact().getCompanyName().equals("")) {
				replacementPayee = replacementPayee + ", " + release.getReplacementPayeeContact().getCompanyName();
			}else if((release.getReplacementPayeeContact().getFirstName() != null && !release.getReplacementPayeeContact().getFirstName().equals("")) || (release.getReplacementPayeeContact().getLastName() != null && !release.getReplacementPayeeContact().getLastName().equals(""))) {
				replacementPayee = replacementPayee + ", " + release.getReplacementPayeeContact().getFirstName() + " " + release.getReplacementPayeeContact().getLastName();
			}
			releaseDTO.setReplacementPayee(replacementPayee);
		}

		// add reject reasons
		RejectReasonEntity managerRejectReason = release.getManagerRejectReason();
		RejectReasonEntity analystRejectReason = release.getAnalystRejectReason();
		if (managerRejectReason != null) {
			releaseDTO.setManagerRejectReason(managerRejectReason.getId());
		}
		if (analystRejectReason != null) {
			releaseDTO.setAnalystRejectReason(analystRejectReason.getId());
		}
		// add reasons
		List<ReleaseReasonEntity> releaseReasons = releaseDAO.getReleaseReasonsByReleaseId(release.getId());

		List<NameDescriptionDTO> releaseReasonDTOs = new ArrayList<>();

		for (ReleaseReasonEntity releaseReason : releaseReasons) {
			NameDescriptionDTO releaseReasonDTO = new NameDescriptionDTO();
			releaseReasonDTO.setId(releaseReason.getReason().getId());
			releaseReasonDTO.setName(releaseReason.getReason().getName());
			releaseReasonDTO.setDescription(releaseReason.getReason().getDescription());
			releaseReasonDTOs.add(releaseReasonDTO);
		}
		releaseDTO.setAssignToVp(VbsConstants.VP_TO_APPROVE_RELEASE);
		releaseDTO.setReasons(releaseReasonDTOs);
		return releaseDTO;
	}

	private List<PendingReleaseDTO> getPendingReleaseDTOS(List<ReleaseEntity> releaseList) {
		List<PendingReleaseDTO> releaseDTOList = new ArrayList<>();
		for (ReleaseEntity releaseEntity : releaseList) {
			PendingReleaseDTO pendingReleaseDTO = new PendingReleaseDTO();
			ReleaseServiceTransformer.transformEntityToDTO(pendingReleaseDTO, releaseEntity);
			releaseDTOList.add(pendingReleaseDTO);
		}
		return releaseDTOList;
	}

	//TODO review
	private List<ErrorDTO> validateRelease(ReleaseDTO release, ReleaseEntity existingEntity, UserDTO user){
		BigDecimal totalRequestedAmount = new BigDecimal(0);
		BigDecimal totalAuthorizedReleasedAmount = new BigDecimal(0);
		for (ReleaseEnrolmentDTO releaseEnrolment : release.getEnrolments()) {
			if (releaseEnrolment.getRequestAmount() != null) {
				totalRequestedAmount = totalRequestedAmount.add(releaseEnrolment.getRequestAmount());
			}
			if (releaseEnrolment.getAuthorizedAmount() != null) {
				totalAuthorizedReleasedAmount = totalAuthorizedReleasedAmount.add(releaseEnrolment.getAuthorizedAmount());
			}
		}
		if (release.getEnrolments().isEmpty()) {
			totalRequestedAmount = release.getRequestedAmount();
			totalAuthorizedReleasedAmount = release.getAuthorizedReleaseAmount();
		}


		SecurityEntity security = genericDAO.findEntityById(SecurityEntity.class, release.getSecurityId());
		BigDecimal releasableAmount = security.getReleasableAmount();
		if(existingEntity != null && existingEntity.getAuthorizedAmount() != null) {
			releasableAmount = releasableAmount.add(existingEntity.getAuthorizedAmount());
		}
		List<ErrorDTO> ret = ReleaseServiceValidator.validateRelease(release,existingEntity, security, totalRequestedAmount, totalAuthorizedReleasedAmount, releasableAmount, user);
		if (release.getReleaseType() != null && (VbsConstants.RELEASE_TYPE_REPLACE == release.getReleaseType().getId())) {

			if(release.getReplacedBy() == null || (release.getReplacedBy().getSecurityId() == null && StringUtils.isEmpty(release.getReplacedBy().getInstrumentNumber()))) {
				ErrorDTO err = new ErrorDTO("Replace By instrument number can not be empty.", REPLACED_BY);
				ret.add(err);
			}

			else if(release.getReplacedBy().getSecurityId() != null) {
				SecurityEntity replacedBySecurity = genericDAO.findEntityById(SecurityEntity.class, release.getReplacedBy().getSecurityId());
				if(replacedBySecurity == null) {
					ErrorDTO err = new ErrorDTO("Security does not exist.", REPLACED_BY);
					ret.add(err);
				}
			}

			else if(!StringUtils.isEmpty(release.getReplacedBy().getInstrumentNumber())) {
				List<SecurityEntity> securities = securityDAO.getSecurityByInstrumentNumber(release.getReplacedBy().getInstrumentNumber());
				if(securities.size() > 1) {
					ErrorDTO err = new ErrorDTO("There exist more than one security with this instrument number.", REPLACED_BY);
					ret.add(err);
				}
				if(securities.isEmpty()) {
					ErrorDTO err = new ErrorDTO("Security does not exist.", REPLACED_BY);
					ret.add(err);
				}
			}
		}

		ret.addAll(ReleaseServiceValidator.validateReleaseEnrolments(release.getEnrolments(), existingEntity, totalAuthorizedReleasedAmount, security.getAvailableAmount(), releasableAmount));

		return ret;
	}

	private void sendDemandOrDrawDownToFMS(ReleaseEntity releaseEntity) {
		if (VbsConstants.SECURITY_TYPE_CASH == releaseEntity.getSecurity().getSecurityType().getId() && VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR == releaseEntity.getReleaseType().getId()) {
			sendDrawDownToFms(releaseEntity);
		} else if (VbsConstants.SECURITY_TYPE_CASH != releaseEntity.getSecurity().getSecurityType().getId() && VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR == releaseEntity.getReleaseType().getId()) {
			sendDemandToFms(releaseEntity);
		}
	}

	private void sendDemandToFms(ReleaseEntity releaseEntity) {
		sendDrawDownToFms(releaseEntity);
	}

	private void sendDrawDownToFms(ReleaseEntity releaseEntity) {
		CreateDrawDownRequest request = new CreateDrawDownRequest();
		request.setAmount(VbsUtil.getStringForMoney(releaseEntity.getPrincipalAmount()));
		request.setInterestAmount(VbsUtil.getStringForMoney(releaseEntity.getInterestAmount()));
		request.setCreateDate(DateUtil.getDateFormattedForFms(releaseEntity.getAnalystApprovalDate()));
		request.setCreateUser(releaseEntity.getFinalApprovalSentTo());
		request.setId(releaseEntity.getId());
		request.setInstrumentNumber(releaseEntity.getSecurity().getInstrumentNumber());
		request.setSecurityId(releaseEntity.getSecurity().getId());
		request.setSequenceNumber(releaseEntity.getSequenceNumber());
		request.setSecurityType(releaseEntity.getSecurity().getSecurityType().getId());

		if(releaseEntity.getSecurity().getPrimaryVb() == null){
			String errorMessage = "Cannot find primary vb for Release: " + releaseEntity + " and Security: " + releaseEntity.getSecurity();
			LoggerUtil.logError(ReleaseServiceImpl.class, "sendReleaseToFms", errorMessage);
			throw new VbsRuntimeException(errorMessage);
		}
		request.setVbNumber(releaseEntity.getSecurity().getPrimaryVb().getCrmContactId());
		String xmlMessageText = VbsUtil.convertJaxbToXml(request, CreateDrawDownRequest.class);
		jmsMessageSenderService.postFmsRequestQueue(xmlMessageText, CreateDrawDownRequest.class.getSimpleName());
		jmsMessageSenderService.insertTransactionLog(JmsMessageTypeEnum.getFmsSecurityDrawdownRequestTracking(request.getId(), request.getSequenceNumber()), xmlMessageText);
	}

	private ReleaseEntity createReleaseEntity(ReleaseDTO releaseDTO, ReleaseEntity currentEntity, UserDTO userDTO, LocalDateTime time) throws VbsCheckedException {
		ReleaseEntity entity = new ReleaseEntity();
		if(currentEntity == null){currentEntity = new ReleaseEntity();}

		entity.setId(releaseDTO.getId());

		//can always be changed
		entity.setComment(releaseDTO.getComment());

		//cannot be changed after creation
		entity.setSecurity((releaseDTO.getId() == null) ? genericDAO.findEntityById(SecurityEntity.class, releaseDTO.getSecurityId()) : currentEntity.getSecurity());
		entity.setRegularRelease((releaseDTO.getId() == null) ? releaseDTO.getRegularRelease() : currentEntity.isRegularRelease());
		entity.setReleaseType((releaseDTO.getId() == null) ? lookupDAO.getReleaseTypeByReleaseTypeId(releaseDTO.getReleaseType().getId()) : currentEntity.getReleaseType());
		entity.setSequenceNumber((releaseDTO.getId() == null) ? releaseDAO.getNumberOfReleases(releaseDTO.getSecurityId())+1 : currentEntity.getSequenceNumber());
		entity.setRequestDate((releaseDTO.getId() == null) ? time : currentEntity.getRequestDate());

		//cannot be set back to false
		if(!currentEntity.isWsInputRequested() && currentEntity.getFinalApprovalStatus() == null && releaseDTO.isWsInputRequested()){
			entity.setWsInputRequested(true);
			entity.setWsRequestor(userDTO.getUserId().toUpperCase());
			entity.setWsRequestedDate(time);
		}else{
			entity.setWsInputRequested(currentEntity.isWsInputRequested());
			entity.setWsRequestor(currentEntity.getWsRequestor());
			entity.setWsRequestedDate(currentEntity.getWsRequestedDate());
		}


		if(currentEntity.getFinalApprovalStatus() == null){
			if (((releaseDTO.getPayTo() == null) != (currentEntity.getContact() == null)) ||
					(releaseDTO.getPayTo() != null && currentEntity.getContact() != null &&
							releaseDTO.getPayTo().getCrmContactId().compareTo(currentEntity.getContact().getCrmContactId()) != 0)) {

				if(releaseDTO.getPayTo() == null){
					entity.setContact(null);
				}else if(releaseDTO.getPayTo().getId() != null){
					ContactEntity payTo = genericDAO.findEntityById(ContactEntity.class, releaseDTO.getPayTo().getId());
					entity.setContact(payTo);
				}else if(!VbsUtil.isNullorEmpty(releaseDTO.getPayTo().getCrmContactId())){
					ContactEntity payTo = contactDAO.getContactByCrmContactId(releaseDTO.getPayTo().getCrmContactId());
					if(payTo == null){
						entity.setContact(ContactServiceTransformer.transformDTOToEntity(contactService.createContactFromDTO(releaseDTO.getPayTo(), userDTO.getUserId()), new ContactEntity()));
					}else {
						entity.setContact(payTo);
					}
				}
			} else {
				entity.setContact(currentEntity.getContact());
			}

			if(((releaseDTO.getReplacedBy() == null) != (currentEntity.getReplacedBySecurity() == null)) ||
					(releaseDTO.getReplacedBy() != null && currentEntity.getReplacedBySecurity() != null &&
							releaseDTO.getReplacedBy().getSecurityId().compareTo(currentEntity.getReplacedBySecurity().getId()) != 0)){
				entity.setReplacedBySecurity(this.getReplacedBySecurity(releaseDTO));
			}else{
				entity.setReplacedBySecurity(currentEntity.getReplacedBySecurity());
			}
		}else{
			entity.setContact(currentEntity.getContact());
			entity.setReplacedBySecurity(currentEntity.getReplacedBySecurity());
		}

		this.setStatuses(releaseDTO, entity, currentEntity, userDTO, time);
		this.setAmounts(releaseDTO, entity, currentEntity, userDTO, time);

		//system controlled values - UI can never change these.
		entity.setVersion(currentEntity.getVersion());
		entity.setVoucherId(currentEntity.getVoucherId());
		entity.setWsReceivedDate(currentEntity.getWsReceivedDate());
		entity.setWsRecommendation(currentEntity.getWsRecommendation());
		entity.setWsInputProvidedBy(currentEntity.getWsInputProvidedBy());
		entity.setFcmAmountRetained(currentEntity.getFcmAmountRetained());
		entity.setSettlementInstructions(currentEntity.getSettlementInstructions());
		entity.setChequeNumber(currentEntity.getChequeNumber());
		entity.setChequeDate(currentEntity.getChequeDate());
		entity.setChequeStatus(currentEntity.getChequeStatus());
		entity.setReplacementChequeNumber(currentEntity.getReplacementChequeNumber());
		entity.setReplacementChequeDate(currentEntity.getReplacementChequeDate());
		entity.setReplacementChequeStatus(currentEntity.getReplacementChequeStatus());
		entity.setServiceOrderId(currentEntity.getServiceOrderId());
		this.updateReplacedBySecurityComments(releaseDTO, entity, currentEntity, userDTO, time);

		if(releaseDTO.getId() == null){
			return (ReleaseEntity) genericDAO.addEntity(entity, userDTO.getUserId().toUpperCase(), time);
		}else{
			entity.setCreateUser(currentEntity.getCreateUser());
			entity.setCreateDate(currentEntity.getCreateDate());
			genericDAO.detach(currentEntity);
			return (ReleaseEntity) genericDAO.updateEntity(entity, userDTO.getUserId().toUpperCase(), time);
		}
	}

	private void setAmounts(ReleaseDTO releaseDTO, ReleaseEntity entity, ReleaseEntity currentEntity, UserDTO userDTO, LocalDateTime time){
		if(currentEntity.getFinalApprovalStatus() == null){
			entity.setRequestAmount(releaseDTO.getRequestedAmount());
			BigDecimal authorizedAmount = releaseDTO.getAuthorizedReleaseAmount();
			this.setReleaseInterestAmounts(releaseDTO, entity, authorizedAmount, userDTO.getUserId(), time);
		}else{
			entity.setRequestAmount(currentEntity.getRequestAmount());
			entity.setAuthorizedAmount(currentEntity.getAuthorizedAmount());
			entity.setInterestAmount(currentEntity.getInterestAmount());
			entity.setPrincipalAmount(currentEntity.getPrincipalAmount());
		}
	}

	private void setStatuses(ReleaseDTO releaseDTO, ReleaseEntity entity, ReleaseEntity currentEntity, UserDTO userDTO, LocalDateTime time){
		if(currentEntity.getFinalApprovalStatus() == null){
			entity.setStatus(ReleaseStatus.INITIATED);
			if(releaseDTO.getAnalystApprovalStatus() != null) {
				if (!releaseDTO.getAnalystApprovalStatus().equals(currentEntity.getAnalystApprovalStatus())) {
					entity.setAnalystApprovalBy(userDTO.getUserId().toUpperCase());
					entity.setAnalystApprovalDate(time);
					entity.setAnalystApprovalStatus(releaseDTO.getAnalystApprovalStatus());
				} else {
					entity.setAnalystApprovalBy(currentEntity.getAnalystApprovalBy());
					entity.setAnalystApprovalDate(currentEntity.getAnalystApprovalDate());
					entity.setAnalystApprovalStatus(currentEntity.getAnalystApprovalStatus());
				}

				if (releaseDTO.getAnalystApprovalStatus().equals(ReleaseApprovalStatusEnum.Rejected)) {
					if (releaseDTO.getAnalystRejectReason() == 0 && releaseDTO.getEnrolments().isEmpty()) {
						throw new VbsRuntimeException("Must have reject reason");
					} else {
						if(releaseDTO.getEnrolments().isEmpty()) {
							entity.setAnalystRejectReason(lookupDAO.getRejectReasonById(releaseDTO.getAnalystRejectReason()));
						}
						entity.setManagerRejectReason(lookupDAO.getRejectReasonById(releaseDTO.getAnalystRejectReason()));
						entity.setAnalystApprovalStatus(ReleaseApprovalStatusEnum.Rejected);
						entity.setFinalApprovalStatus(ReleaseApprovalStatusEnum.Rejected);
						entity.setStatus(ReleaseStatus.REJECTED);
					}
				} else if (releaseDTO.getAnalystApprovalStatus().equals(ReleaseApprovalStatusEnum.Authorized)) {
					entity.setFinalApprovalSentTo(releaseDTO.getFinalApprovalSentTo());
					if (releaseDTO.getFinalApprovalStatus() != null && (userDTO.hasPermission(PermissionConstants.P_APPROVE_RELEASE_FINAL) ||
									(entity.getReleaseType().getId().equals(VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR) && userDTO.hasPermission(PermissionConstants.P_CREATE_DEMAND_FINANCE)))) {

						entity.setFinalApprovalDate(time);
						entity.setFinalApprovalBy(userDTO.getUserId().toUpperCase());
						entity.setFinalApprovalStatus(releaseDTO.getFinalApprovalStatus());
						if (ReleaseApprovalStatusEnum.Rejected.equals(releaseDTO.getFinalApprovalStatus())) {
							if (releaseDTO.getManagerRejectReason() == 0) {
								throw new VbsRuntimeException("Must have reject reason");
							} else {
								entity.setManagerRejectReason(lookupDAO.getRejectReasonById(releaseDTO.getManagerRejectReason()));
								entity.setStatus(ReleaseStatus.REJECTED);
							}
						}else if(ReleaseApprovalStatusEnum.Authorized.equals(releaseDTO.getFinalApprovalStatus())){
							if(entity.getReleaseType().getId().compareTo(VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR) == 0 ||
									entity.getSecurity().getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_CASH) == 0){
								entity.setStatus(ReleaseStatus.PENDING);
								sendReleaseApprovalWorklistToCrm(entity, userDTO.getUserId());
							}else{
								entity.setStatus(ReleaseStatus.COMPLETED);
							}
						}
					}
				}
			}else if(currentEntity.getAnalystApprovalStatus() != null){
				throw new VbsRuntimeException("Cannot change analyst approval status after rejection");
			}
		}else{
			entity.setStatus(currentEntity.getStatus());
			entity.setAnalystApprovalBy(currentEntity.getAnalystApprovalBy());
			entity.setAnalystApprovalDate(currentEntity.getAnalystApprovalDate());
			entity.setAnalystApprovalStatus(currentEntity.getAnalystApprovalStatus());
			entity.setFinalApprovalDate(currentEntity.getFinalApprovalDate());
			entity.setFinalApprovalBy(currentEntity.getFinalApprovalBy());
			entity.setFinalApprovalStatus(currentEntity.getFinalApprovalStatus());
			entity.setFinalApprovalSentTo(currentEntity.getFinalApprovalSentTo());
			entity.setAnalystRejectReason(currentEntity.getAnalystRejectReason());
			entity.setManagerRejectReason(currentEntity.getManagerRejectReason());
		}
	}

	private void setReleaseReasons(ReleaseDTO releaseDTO, ReleaseEntity entity, ReleaseEntity currentEntity, UserDTO user, LocalDateTime time){
 		if(currentEntity == null || currentEntity.getFinalApprovalStatus() == null) {
			List<ReleaseReasonEntity> currentReasons = releaseDAO.getReleaseReasonsByReleaseId(entity.getId());
			if (!releaseDTO.getReasons().isEmpty()) {
				for (NameDescriptionDTO reason : releaseDTO.getReasons()) {
					Optional<ReleaseReasonEntity> opt = currentReasons.stream().filter(r -> r.getReason().getId().equals(reason.getId())).findFirst();
					if (!opt.isPresent()) {
						ReleaseReasonEntity reasonEntity = new ReleaseReasonEntity();
						reasonEntity.setReason(lookupDAO.getReasonByReasonId(reason.getId()));
						reasonEntity.setRelease(entity);
						genericDAO.addEntity(reasonEntity, user.getUserId().toUpperCase(), time);
					}
				}

				for (ReleaseReasonEntity reasonEntity : currentReasons) {
					if (releaseDTO.getReasons().stream().noneMatch(r -> r.getId().equals(reasonEntity.getReason().getId()))) {
						genericDAO.removeEntity(reasonEntity, user.getUserId().toUpperCase(), time);
					}
				}

			} else {
				throw new VbsRuntimeException("Must have at least one reason");
			}
		}
	}

	private void setReleaseEnrolments(ReleaseDTO releaseDTO, ReleaseEntity currentRelease,  ReleaseEntity entity, UserDTO user, LocalDateTime time){
		if(currentRelease == null || currentRelease.getFinalApprovalStatus() == null) {
			List<ReleaseEnrolmentEntity> currentEnrolments = releaseDAO.getEnrolmentsByReleaseId(entity.getId());
			if (!releaseDTO.getEnrolments().isEmpty()) {
				for (ReleaseEnrolmentDTO dto : releaseDTO.getEnrolments()) {
					Optional<ReleaseEnrolmentEntity> opt = currentEnrolments.stream().
							filter(e -> e.getEnrolment().getEnrolmentNumber().equals(dto.getEnrolment().getEnrolmentNumber())).findFirst();
					ReleaseEnrolmentEntity enrEntity = opt.orElseGet(ReleaseEnrolmentEntity::new);

					enrEntity.setEnrolment((enrEntity.getEnrolment() != null) ? enrEntity.getEnrolment() : genericDAO.findEntityById(EnrolmentEntity.class, dto.getEnrolment().getEnrolmentNumber()));
					enrEntity.setRelease(entity);
					enrEntity.setFullRelease(dto.isFullRelease());

					if(dto.getAuthorizedAmount() != null) {
						enrEntity.setAuthorizedAmount(dto.getAuthorizedAmount());
					}
					enrEntity.setRequestedAmount(dto.getRequestAmount());

					enrEntity.setAnalystApprovalStatus(dto.getAnalystApprovalStatus());
					if(dto.getAnalystApprovalStatus() != null && releaseDTO.getFinalApprovalStatus() == null){
						enrEntity.setAnalystApprovalBy(user.getUserId().toUpperCase());
						enrEntity.setAnalystApprovalDate(time);
						if(dto.getAnalystApprovalStatus().equals(ReleaseApprovalStatusEnum.Rejected)){
							if(entity.getFinalApprovalStatus() != null && !entity.getFinalApprovalStatus().equals(ReleaseApprovalStatusEnum.Rejected)){
								throw new VbsRuntimeException("Release Status must be rejected if any enrolment is rejected");
							}
							if(dto.getRejectReason() == null || dto.getRejectReason() == 0){
								throw new VbsRuntimeException("Enrolment must have reject reason.");
							}
						}
						enrEntity.setRejectReason(lookupDAO.getRejectReasonById(dto.getRejectReason()));
					}

					if (enrEntity.getId() == null) {
						genericDAO.addEntity(enrEntity, user.getUserId().toUpperCase(), time);
					} else {
						//TODO see if changed.
						genericDAO.updateEntity(enrEntity, user.getUserId().toUpperCase(), time);
					}
				}

				for (ReleaseEnrolmentEntity enrEntity : currentEnrolments) {
					if (releaseDTO.getEnrolments().stream().noneMatch(e -> e.getEnrolment().getEnrolmentNumber().equalsIgnoreCase(enrEntity.getEnrolment().getEnrolmentNumber()))) {
						genericDAO.removeEntity(enrEntity, user.getUserId().toUpperCase(), time);
					}
				}
			} else if (!currentEnrolments.isEmpty()) {
				for (ReleaseEnrolmentEntity enrEntity : currentEnrolments) {
					genericDAO.removeEntity(enrEntity, user.getUserId().toUpperCase(), time);
				}
			}
		}
	}

	private void sendEmail(ReleaseEntity entity, ReleaseEntity currentEntity, UserDTO user){
		if(currentEntity != null && currentEntity.getFinalApprovalStatus() == null && entity.getFinalApprovalStatus() != null){
			if(entity.getSecurity().getSecurityType().getId() == VbsConstants.SECURITY_TYPE_CASH && entity.getAuthorizedAmount().compareTo(VbsConstants.SEND_RELEASE_EMAIL_AMOUNT) > -1) {
				try {
					String releaseEmailSubject = propertyService.getValue(VbsConstants.LARGE_RELEASE_EMAIL_SUBJECT);
					String releaseEmailBody = String.format(propertyService.getValue(VbsConstants.LARGE_RELEASE_EMAIL), entity.getSecurity().getPrimaryVb().getCrmContactId(),
							"$" + VbsUtil.getStringForMoney(entity.getPrincipalAmount().add(entity.getInterestAmount()).setScale(VbsConstants.DECIMAL_SCALE, VbsConstants.ROUNDING_MODE)), entity.getSecurity().getId(), entity.getReferenceNumber());
					List<UserDTO> users = userService.getUsersByPermissionName(PermissionConstants.P_LARGE_RELEASE_EMAIL);
					if (users != null && !users.isEmpty()) {
						for (UserDTO u : users) {
							if(!VbsUtil.isNullorEmpty(user.getEmailAddress())) {
								emailWSInvocationService.sendEmailUsingEsp(releaseEmailSubject, releaseEmailBody, user.getEmailAddress(), u.getEmailAddress());
							}
						}
					}
				}catch(VbsCheckedException e){
					LoggerUtil.logError(ReleaseServiceImpl.class, "sendEmail", e);
					throw new VbsRuntimeException("Failed to send release email");
				}
			}
		}
	}

	private void sendWarrantyServiceToCrm(ReleaseDTO releaseDTO, ReleaseEntity entity, ReleaseEntity currentEntity, LocalDateTime time) {
		if((currentEntity == null || !currentEntity.isWsInputRequested()) && entity.isWsInputRequested()) {
			WarrantyServicesInputRequest request = new WarrantyServicesInputRequest();
			request.setSecurityId(entity.getSecurity().getId());
			request.setEnrolmentNumber(releaseDTO.getEnrolments().get(0).getEnrolment().getEnrolmentNumber());
			request.setReleaseId(entity.getId());
			request.setReleaseReason(releaseDTO.getReasons().get(0).getName());
			request.setRequestedDate(DateUtil.getDateFormattedForCrm(time));
			request.setRequestedUser(entity.getWsRequestor());

			String xmlMessageText = VbsUtil.convertJaxbToXml(request, WarrantyServicesInputRequest.class);
			jmsMessageSenderService.postCrmRequestQueue(xmlMessageText, WarrantyServicesInputRequest.class.getSimpleName());
			jmsMessageSenderService.insertTransactionLog(JmsMessageTypeEnum.getCrmWarrantyServiceRequestTracking(request.getReleaseId(), request.getEnrolmentNumber()), xmlMessageText);
		}
	}

	private SecurityEntity getReplacedBySecurity(ReleaseDTO release) throws VbsCheckedException {
		if(release.getReleaseType().getId() != null && release.getReleaseType().getId() == VbsConstants.RELEASE_TYPE_REPLACE && (release.getReplacedBy() == null || (release.getReplacedBy().getSecurityId() == null && StringUtils.isEmpty( release.getReplacedBy().getInstrumentNumber())))){
			throw new VbsCheckedException("Replaced by cannot be empty for Replace");
		}

		if (release.getReleaseType().getId() != null
				&& (release.getReleaseType().getId() == VbsConstants.RELEASE_TYPE_REPLACE || release.getReleaseType().getId() == VbsConstants.RELEASE_TYPE_DEMAND_LU)
				&& (release.getReplacedBy() != null && (release.getReplacedBy().getSecurityId() != null || !StringUtils.isEmpty(release.getReplacedBy().getInstrumentNumber())))) {

			SecurityEntity replacedBySecurity = null;

			if(release.getReplacedBy().getSecurityId() != null) {
				replacedBySecurity = genericDAO.findEntityById(SecurityEntity.class, release.getReplacedBy().getSecurityId());
				if(replacedBySecurity == null) {
					throw new VbsCheckedException("Replaced by security not found");
				}
			}

			else if(!StringUtils.isEmpty(release.getReplacedBy().getInstrumentNumber())) {
				List<SecurityEntity> securities = securityDAO.getSecurityByInstrumentNumber(release.getReplacedBy().getInstrumentNumber());
				if(securities.size() > 1) {
					throw new VbsCheckedException("There exist more than one security with this replace by instrument number");
				}
				if(securities.isEmpty()) {
					throw new VbsCheckedException("Replaced by security not found");
				}
				replacedBySecurity = securities.get(0);
			}

			return replacedBySecurity;

		}

		return null;
	}

	private void updateReplacedBySecurityComments(ReleaseDTO releaseDTO, ReleaseEntity entity, ReleaseEntity currentEntity, UserDTO userDTO, LocalDateTime time) throws VbsCheckedException {
		if(currentEntity == null){currentEntity = new ReleaseEntity();}

		if(releaseDTO.getFinalApprovalStatus() != null && currentEntity.getFinalApprovalStatus() == null) {
            SecurityEntity mainSecurity = entity.getSecurity();
            SecurityEntity replacedBySecurity = entity.getReplacedBySecurity();

            if (mainSecurity != null && replacedBySecurity != null) {
                String newComment = mainSecurity.getSecurityType().getDescription() + " No. " + mainSecurity.getInstrumentNumber() + " is replaced by "
                        + replacedBySecurity.getSecurityType().getDescription() + " No. " + replacedBySecurity.getInstrumentNumber() + " By User: " + userDTO.getUserId() + " Date and time: " + DateUtil.getDateFormattedShortTime(LocalDateTime.now());

                mainSecurity = genericDAO.findEntityById(SecurityEntity.class, mainSecurity.getId());
                mainSecurity.setComment(newComment + (mainSecurity.getComment() == null ? "" : "\n" + mainSecurity.getComment()));
                genericDAO.updateEntity(mainSecurity, userDTO.getUserId().toUpperCase(), time);

                replacedBySecurity = genericDAO.findEntityById(SecurityEntity.class, replacedBySecurity.getId());
                replacedBySecurity.setComment(newComment + (replacedBySecurity.getComment() == null ? "" :  "\n" + replacedBySecurity.getComment()));
                genericDAO.updateEntity(replacedBySecurity, userDTO.getUserId().toUpperCase(), time);
				entity.setComment(newComment + (entity.getComment() == null ? "" : "\n" + entity.getComment()));

            }
        }
	}

	private PendingReleaseDTO recalculateReleaseInterest(PendingReleaseDTO dto){
		if(dto.getReleaseStatus().equals(ReleaseStatus.PENDING) && dto.getSecurityTypeName().equalsIgnoreCase("CAS") &&
				(dto.getReleaseTypeName().equalsIgnoreCase("RE") || dto.getReleaseTypeName().equalsIgnoreCase("RP"))
				&& dto.getPrincipalAmount().compareTo(BigDecimal.ZERO) != 0){
			SecurityEntity security = genericDAO.findEntityById(SecurityEntity.class, dto.getSecurityId());
            if(security != null && security.getCurrentInterest() != null) {
                dto.setInterestAmount(security.getCurrentInterest().multiply(dto.getReleasePercentage()).setScale(2, VbsConstants.ROUNDING_MODE));
            }
			dto.setInterestAuthorizedAmountTotal(dto.getPrincipalAmount().add(dto.getInterestAmount()));
		}
		dto.setReleasePercentage(dto.getReleasePercentage().multiply(VbsConstants.BIGDECIMAL_HUNDRED));
		return dto;
	}

	@Override
	public void sendReleaseApprovalWorklistToCrm(ReleaseEntity releaseEntity, String userId) {
		
		CreateWorklistRequest createWorklistRequest = new CreateWorklistRequest();
		createWorklistRequest.setBUSINESSPROCESS(VbsConstants.CRM_FINANCE_WORKLIST_BUSINESS_PROCESS);
		createWorklistRequest.setACTIVITYNAME(VbsConstants.CRM_FINANCE_WORKLIST_ACTIVITY_NAME);
		createWorklistRequest.setEVENTNAME(VbsConstants.CRM_FINANCE_WORKLIST_EVENT_NAME);
		createWorklistRequest.setWLNAME(VbsConstants.CRM_FINANCE_WORKLIST_WL_NAME);
		createWorklistRequest.setOPRID(VbsConstants.CRM_FINANCE_WORKLIST_USER);
		createWorklistRequest.setORIGINATORID(userId.toUpperCase());
		createWorklistRequest.setPRIORITY(VbsConstants.CRM_FINANCE_WORKLIST_WORKLIST_PRIORITY);
		createWorklistRequest.setWORKLISTDESCR(VbsConstants.CRM_FINANCE_WORKLIST_WORKLIST_DESCR);
		createWorklistRequest.setWORKLISTPURPOSE(VbsConstants.CRM_FINANCE_WORKLIST_WORKLIST_PURPOSE);
		createWorklistRequest.setWORKLISTNOTES(VbsConstants.CRM_FINANCE_WORKLIST_WORKLIST_NOTES);
		createWorklistRequest.setTWCSECURITYNBR("" + releaseEntity.getSecurity().getId());
		createWorklistRequest.setTWCVBSSECRELNBR("" + releaseEntity.getSequenceNumber());

		crmWSInvocationService.createCrmWorklistItem(createWorklistRequest);

	}
}
