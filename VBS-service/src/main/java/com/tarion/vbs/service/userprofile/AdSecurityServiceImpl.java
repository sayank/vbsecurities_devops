/* 
 * 
 * AdSecurityBean.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.userprofile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.PagedResultsControl;
import javax.naming.ldap.PagedResultsResponseControl;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.exceptions.IncorrectCredentialsException;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.exceptions.VbsDirectoryEnvironmentException;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.service.util.PropertyService;

/**
 * Ad Service provides operations against Active Directory
 *
 * @author <A href="joni.paananen@tarion.com">Joni Paananen</A>
 * @since 2018-01-05
 * @version 1.0
 */
@Stateless(mappedName = "ejb/AdSecurityService")
@PermitAll
public class AdSecurityServiceImpl implements AdSecurityService, AdSecurityServiceRemote {

	private static final String AD_SEARCH_FILTER_USER_ACCOUNT_TYPE = "&(sAMAccountType=805306368)";
	private static final String AD_SEARCH_FILTER_REMOVE_DISABLED = "(!(userAccountControl:1.2.840.113556.1.4.803:=2))";
	
	private static final String GIVEN_NAME = "givenName";
	private static final String FACSIMILE_TELEPHONE_NUMBER = "facsimileTelephoneNumber";
	
	@EJB
	private PropertyService propertyService;
	@EJB
	private UserService userService;

	private LocalDateTime tarionUserCacheDate;
	private List<UserDTO> tarionUsers;
	
	@Override
	public UserDTO authenticate(String userId, String password) throws VbsCheckedException {
		UserDTO userDTO = new UserDTO();
		
		try {
			Hashtable<String, String> environment = getADEnvironment();
			environment.put(Context.SECURITY_PRINCIPAL, userId + propertyService.getValue(VbsConstants.ACTIVE_DIRECTORY_DOMAIN_CONTEXT));
			environment.put(Context.SECURITY_CREDENTIALS, password);
			
			String searchFilter = "(&(sAMAccountType=805306368)(sAMAccountName=" + userId + "))";
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			DirContext ctx = new InitialDirContext(environment);
			
			NamingEnumeration<SearchResult> results = ctx.search(propertyService.getValue(VbsConstants.ACTIVE_DIRECTORY_SEARCH), searchFilter, searchControls);

			SearchResult searchResult;
			if (results.hasMoreElements()) {
				searchResult = results.nextElement();
				if (results.hasMoreElements()) {
					LoggerUtil.logDebug(AdSecurityServiceImpl.class, "More than one account found in AD matching the search filter.");
					throw new IncorrectCredentialsException("Duplicate Users!");
				} else {
					String firstName = (String)searchResult.getAttributes().get(GIVEN_NAME).get();
					String lastName = (String) searchResult.getAttributes().get("sn").get();
					Attribute mailAttr = searchResult.getAttributes().get("mail");
					String emailAddress = (mailAttr != null) ? (String) mailAttr.get() : "";
					String telephone = searchResult.getAttributes().get(FACSIMILE_TELEPHONE_NUMBER) != null ? (String) searchResult.getAttributes().get(FACSIMILE_TELEPHONE_NUMBER).get() : null;
										
					userDTO = new UserDTO();
					userDTO.setFirstName(firstName);
					userDTO.setLastName(lastName);
					userDTO.setEmailAddress(emailAddress);
					userDTO.setUserId(userId.toUpperCase());
					userDTO.setTelephone(telephone);
					
					userDTO.setPermissions(userService.getPermissionsForUser(userId));
				}
			}
			
		}catch(NamingException ne) {
			//credentials/lookup fail
			LoggerUtil.logError(AdSecurityServiceImpl.class, "authenticate", ne, userId);
			throw new IncorrectCredentialsException("Incorrect Credentials");
		}

		return userDTO;
	}
	
	@Override
	public List<UserDTO> getAllADUsers() throws VbsDirectoryEnvironmentException {
		
		if(this.tarionUsers == null || (tarionUserCacheDate == null || tarionUserCacheDate.isBefore(LocalDateTime.now().minusHours(6)))){
			List<UserDTO> userList = new ArrayList<>();
			
			LdapContext contextLdap = null;
			try{
				contextLdap = new InitialLdapContext(getADEnvironment(), null);
				
				int pageSize = 1000;
				byte[] cookie = null;
				String searchFilter = "(" + AD_SEARCH_FILTER_USER_ACCOUNT_TYPE + AD_SEARCH_FILTER_REMOVE_DISABLED + ")";
				contextLdap.setRequestControls(new Control[] { new PagedResultsControl(pageSize, Control.NONCRITICAL) });
				SearchControls searchControls = new SearchControls();
				searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
				String searchBase = propertyService.getValue(VbsConstants.ACTIVE_DIRECTORY_SEARCH);
		
				do {
					NamingEnumeration<SearchResult> results = contextLdap.search(searchBase, searchFilter, searchControls);
					SearchResult searchResult;
					while(results.hasMoreElements()){
						searchResult = results.next();
						UserDTO dto = new UserDTO();
						
						dto.setEmailAddress(searchResult.getAttributes().get("mail") != null ? (String) searchResult.getAttributes().get("mail").get() : null);
						dto.setFirstName(searchResult.getAttributes().get(GIVEN_NAME) != null ? (String) searchResult.getAttributes().get(GIVEN_NAME).get() : null);
						dto.setLastName(searchResult.getAttributes().get("sn") != null ? (String) searchResult.getAttributes().get("sn").get() : null);
						dto.setUserId(searchResult.getAttributes().get("sAMAccountName") != null ? searchResult.getAttributes().get("sAMAccountName").get().toString().toUpperCase() : null);
						String telephone = searchResult.getAttributes().get(FACSIMILE_TELEPHONE_NUMBER) != null ? (String) searchResult.getAttributes().get(FACSIMILE_TELEPHONE_NUMBER).get() : null;
						dto.setTelephone(telephone);

						userList.add(dto);
					}
					// Examine the paged results control response
					Control[] controls = contextLdap.getResponseControls();
					if (controls != null) {
						for (Control control : controls) {
							if (control instanceof PagedResultsResponseControl) {
								PagedResultsResponseControl prrc = (PagedResultsResponseControl) control;
								cookie = prrc.getCookie();
							}
						}
					}
					// Re-activate paged results
					contextLdap.setRequestControls(new Control[] { new PagedResultsControl(pageSize, cookie, Control.CRITICAL) });
				} while (cookie != null);
			}catch(NamingException | IOException ne){
				String errMsg = "Error trying to enumerate search results in getAdUserDtoList";
				LoggerUtil.logError(AdSecurityServiceImpl.class, "getAllADUsers", ne);
				throw new VbsDirectoryEnvironmentException(errMsg);
			}finally{
				if (contextLdap != null) {
					try {
						contextLdap.close();
					} catch (NamingException ne) {
						LoggerUtil.logError(AdSecurityServiceImpl.class, "getAllADUsers - closing context", ne);
					}
				}
			}
			this.tarionUserCacheDate = LocalDateTime.now();
			this.tarionUsers = userList;
		}
		
		return this.tarionUsers;
	}
		
	@Override
	public UserDTO getUser(String userId) throws VbsDirectoryEnvironmentException {
		List<UserDTO> userList = getAllADUsers();
		Optional<UserDTO> opt = userList.stream().filter(u -> u.getUserId().equalsIgnoreCase(userId)).findFirst();
		return opt.orElse(null);
	}
	
	private Hashtable<String, String> getADEnvironment() throws VbsDirectoryEnvironmentException {
		DirContext contextAd = null;
		Hashtable<String, String> environment = null;
		
		try {
			InitialContext initialContext = new InitialContext();
			contextAd = (DirContext) initialContext.lookup(VbsConstants.ACTIVE_DIRECTORY_JNDI);
			environment = new Hashtable<>();
			for(Entry<?, ?> entry : contextAd.getEnvironment().entrySet()){
				String key = (String) entry.getKey();
				String value = (String) entry.getValue();
				environment.put(key, value);
			}
		}catch(NamingException ne) {
			//catch
		} finally {
			if(contextAd != null) {
				try {
					contextAd.close();
				}catch(NamingException ne) { 
					//catch
				}
			}
		}
		
		if(environment == null) {
			throw new VbsDirectoryEnvironmentException("Error Instantiating AD Environment");
		}
		
		return environment;
	}
	
}