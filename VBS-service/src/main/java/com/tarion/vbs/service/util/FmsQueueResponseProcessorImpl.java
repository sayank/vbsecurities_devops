/* 
 * 
 * FmsQueueResponseProcessorImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.enums.ReleaseStatus;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.exceptions.VbsJAXBConversionException;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.contact.ContactDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.dao.util.JmsTransactionLogDAO;
import com.tarion.vbs.dao.util.LookupDAO;
import com.tarion.vbs.orm.entity.JmsTransactionLogEntity;
import com.tarion.vbs.orm.entity.contact.ContactEntity;
import com.tarion.vbs.orm.entity.release.ReleaseEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import com.tarion.vbs.orm.entity.security.SecurityStatusEntity;
import com.tarion.vbs.service.contact.ContactService;
import com.tarion.vbs.service.contact.ContactServiceTransformer;
import com.tarion.vbs.service.jaxb.fms.createcashsecurity.CreateCashSecurityResponse;
import com.tarion.vbs.service.jaxb.fms.drawdown.CreateDrawDownResponse;
import com.tarion.vbs.service.jaxb.fms.paymentresponse.*;
import com.tarion.vbs.service.jaxb.fms.release.ReplaceCheque;
import com.tarion.vbs.service.jaxb.fms.release.SecurityReleaseResponse;
import com.tarion.vbs.service.processor.ProcessorTriggeringService;
import com.tarion.vbs.service.release.ReleaseService;
import com.tarion.vbs.service.userprofile.AdSecurityService;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Processing Responses from FMS Queue MDB 
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @date 2018-04-20
 * @version 1.0	
 */
@Stateless(mappedName = "ejb/FmsQueueResponseProcessor")
@Interceptors(EjbLoggingInterceptor.class)
public class FmsQueueResponseProcessorImpl implements FmsQueueResponseProcessor {
	
	@EJB
	GenericDAO genericDAO;
	@EJB
	private ContactDAO contactDAO;
	@EJB
	private JmsMessageSenderService jmsMessageSenderService;
	@EJB
	private JmsTransactionLogDAO jmsTransactionLogDAO;
	@EJB
	private ReleaseService releaseService;
	@EJB
	private LookupDAO lookupDAO;
	@EJB
	private VBAWSInvocationService vbaWS;

	@EJB
	private EmailWSInvocationService emailWSInvocationService;
	@EJB
	private PropertyService propertyService;
	@EJB
	private ContactService contactService;
	@EJB
	private ProcessorTriggeringService processorTriggeringService;

	private static final String PROCESS = "process";
	private static final String FAILED_PROCESSING = "Failed processing ";
	private static final String RELEASE_NOT_FOUND = " - Release not found for Release Id:";
	
	@Override
	public void processReceivedFmsXml(String xmlResponse) {
		if(VbsUtil.isNullorEmpty(xmlResponse)){
			LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, "processReceivedFmsXml", "Empty message received.");
		}else {
			Object jaxbObject = null;
			try {
				jaxbObject = VbsUtil.convertXmltoJaxb(xmlResponse);
				Method m = this.getClass().getMethod(PROCESS, jaxbObject.getClass(), String.class);
				m.invoke(this, jaxbObject, xmlResponse);

			} catch (VbsJAXBConversionException e) {
				//converting didn't work
				LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, "processReceivedFmsXml ", e, "XML to JAXB Conversion Failed.", xmlResponse);
				throw e;
			} catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
				//error invoking the method (not found/not allowed)
				LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, "processReceivedFmsXml", e, "Unable to invoke process method.", xmlResponse);
				throw new VbsRuntimeException(e.getMessage());
			} catch (Exception e) {
				//other unknown exception/exception thrown from process method
				LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, "processReceivedFmsXml", e, "Error occured during processing.", xmlResponse);
				throw e;
			}
		}
	}

	@Override
	public void process(SecurityReleaseResponse response, String xmlResponse) {
		if (response != null && response.getStatus() != null) {
			String updateUserId = VbsConstants.VBS_FMS_SYSTEM_USER_ID;
			LocalDateTime updateTime = DateUtil.getLocalDateTimeNow();
			if (VbsConstants.FMS_RELEASE_FAILURE.equalsIgnoreCase(response.getStatus())) {
				String className = SecurityReleaseResponse.class.getSimpleName();
				String errorMessage = "FMS Failure processing Release:" + response.getId();
				LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, PROCESS + className, errorMessage);
				Long releaseId = response.getId();
				ReleaseEntity release = genericDAO.findEntityById(ReleaseEntity.class, releaseId);
				if (release == null) {
					errorMessage = FAILED_PROCESSING + className + RELEASE_NOT_FOUND + response.getId();
					LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, PROCESS + className, errorMessage);
					throw new VbsRuntimeException(errorMessage);
				}
				release.setVoucherId(response.getVoucherId());
				release.setUpdateDate(updateTime);
				release.setUpdateUser(updateUserId);
                release.setSettlementInstructions(response.getSettlementInstruction());
				genericDAO.updateEntity(release, updateUserId, updateTime);
			} else if (VbsConstants.FMS_RELEASE_VOUCHER_CREATED.equalsIgnoreCase(response.getStatus())) {
				Long releaseId = response.getId();
				ReleaseEntity release = genericDAO.findEntityById(ReleaseEntity.class, releaseId);
				if (release == null) {
					String className = SecurityReleaseResponse.class.getSimpleName();
					String errorMessage = FAILED_PROCESSING + className + RELEASE_NOT_FOUND + response.getId();
					LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, PROCESS + className, errorMessage);
					throw new VbsRuntimeException(errorMessage);
				}
				release.setVoucherId(response.getVoucherId());
				release.setStatus(ReleaseStatus.VOUCHER_CREATED);
				release.setUpdateDate(updateTime);
				release.setUpdateUser(updateUserId);
				genericDAO.updateEntity(release, updateUserId, updateTime);
			} else if (VbsConstants.FMS_RELEASE_PAYMENT_CREATED.equalsIgnoreCase(response.getStatus())) {
				Long releaseId = response.getId();
				ReleaseEntity release = genericDAO.findEntityById(ReleaseEntity.class, releaseId);
				if (release == null) {
					String className = SecurityReleaseResponse.class.getSimpleName();
					String errorMessage = FAILED_PROCESSING + className + RELEASE_NOT_FOUND + response.getId();
					LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, PROCESS + className, errorMessage);
					throw new VbsRuntimeException(errorMessage);
				}else if(release.getStatus().equals(ReleaseStatus.COMPLETED)){
					if(release.getChequeStatus().equalsIgnoreCase(response.getChequeStatus())){
						throw new VbsRuntimeException("Attempted to update completed release");
					}
					release.setChequeStatus(response.getChequeStatus());
				}else{
					release.setVoucherId(response.getVoucherId());
					release.setChequeNumber(response.getChequeNumber());
					release.setChequeDate(DateUtil.getLocalDateTimeFromCrmDateString(response.getChequeDate()));
					release.setChequeStatus(response.getChequeStatus());
					release.setSettlementInstructions(response.getSettlementInstruction());
					release.setStatus(ReleaseStatus.COMPLETED);
				}
				release.setUpdateDate(updateTime);
				release.setUpdateUser(updateUserId);
				genericDAO.updateEntity(release, updateUserId, updateTime);
			}
			insertResponseTransactionLog(JmsMessageTypeEnum.getFmsSecurityReleaseResponseTracking(response.getId()), xmlResponse);
		} else {
			//error reading response
			String className = ((response == null) ? "" : response.getClass().getSimpleName());
			String errorMessage = FAILED_PROCESSING + className + " - empty ressponse object. XML is:" + xmlResponse;
			LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, PROCESS + className, errorMessage);
			throw new VbsRuntimeException(errorMessage);
		}
	}	
	
	@Override
	public void process(CreateCashSecurityResponse response, String xmlResponse) {
		if (response != null) {
			this.updateTransactionLog(JmsMessageTypeEnum.getFmsCashSecurityResponseTrackingUpdate(response.getSecurityId()), xmlResponse, JmsMessageTypeEnum.getFmsCashSecurityResponseTracking(response.getSecurityId()));
//			this.insertTransactionLog(JmsMessageTypeEnum.getFmsCashSecurityResponseTracking(response.getSecurityId()), xmlResponse);
		}
	}
	@Override
	public void process(WriteOff response, String xmlResponse) {
		if (response != null) {
			JmsTransactionLogEntity transactionLog = jmsMessageSenderService.insertTransactionLog(JmsMessageTypeEnum.getFmsWriteOffTracking(response.getSecurityId()), xmlResponse);
			LocalDateTime t = DateUtil.getLocalDateTimeNow();
			try {
				SecurityEntity securityEntity = genericDAO.findEntityById(SecurityEntity.class, response.getSecurityId());
				securityEntity.setWriteOff(true);
				securityEntity.setWriteOffReason(response.getReason());
				securityEntity.setWriteOffDate(DateUtil.getLocalDateTimeFromCrmDateString(response.getAccountingDate()));
				securityEntity.setCurrentAmount(BigDecimal.ZERO);
				genericDAO.updateEntity(securityEntity, response.getWriteOffUser(), t);

			}catch(Exception e) {
				//other unknown exception/exception thrown from process method
				LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, "processReceivedFmsXml", e, "Error occurred during processing.", xmlResponse);
				throw new VbsRuntimeException(e);
			}
		}
	}

	@Override
	public void process(AllocationRequest request, String xmlResponse) {
		if(request != null) {
			JmsTransactionLogEntity transactionLog = jmsMessageSenderService.insertTransactionLog(JmsMessageTypeEnum.getFmsCashSecurityAllocationRequestTracking(request.getSecurityId()), xmlResponse);

			LocalDateTime t = DateUtil.getLocalDateTimeNow();
			try {
				SecurityEntity securityEntity = genericDAO.findEntityById(SecurityEntity.class, request.getSecurityId());
				securityEntity.setAllocated(true);
				securityEntity.setUnpostUser(null);
				securityEntity.setUnpostDate(null);
				securityEntity.setUnpostReason(null);
				securityEntity.setWriteOff(false);
				securityEntity.setWriteOffReason(null);
				securityEntity.setWriteOffDate(null);

				if(securityEntity.getSecurityType().getId().equals(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) && (securityEntity.getStatus() == null || !securityEntity.getStatus().getId().equals(VbsConstants.SECURITY_STATUS_WITHDRAWN))){
					SecurityStatusEntity statusPendingWithFee =  lookupDAO.getSecurityStatusById(VbsConstants.SECURITY_STATUS_PENDING_WITH_FEE);
					securityEntity.setStatus(statusPendingWithFee);
                }
				genericDAO.updateEntity(securityEntity, request.getUnpostByUser(), t);

				AllocationResponse response = new AllocationResponse();
				response.setSecurityId(securityEntity.getId());
				response.setAmount(VbsUtil.getStringForMoney(securityEntity.getOriginalAmount()));
				response.setSecurityType(securityEntity.getSecurityType().getName());
				response.setUpdateDate(DateUtil.getDateFormattedForCrmLong(securityEntity.getReceivedDate()));
				response.setVbNumber(request.getVbNumber());

				String responseXml = VbsUtil.convertJaxbToXml(response, AllocationResponse.class);
				jmsMessageSenderService.postFmsRequestQueue(responseXml, AllocationResponse.class.getSimpleName());

				transactionLog.setResponse(responseXml);
				transactionLog.setResponseReceivedDate(t);
				transactionLog.setUpdateDate(t);
				transactionLog.setUpdateUser(VbsConstants.VBS_SYSTEM_USER_ID);
				genericDAO.updateEntity(transactionLog, transactionLog.getUpdateUser(), transactionLog.getUpdateDate());
			}catch(Exception e) {
				LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, "process", e, "Error occurred during processing Allocation Response", xmlResponse);
				throw new VbsRuntimeException(e);
			}
		}
	}



	@Override
    public void process(ReplaceCheque request, String xmlResponse) {
        if(request != null) {
            JmsTransactionLogEntity transactionLog = jmsMessageSenderService.insertTransactionLog(JmsMessageTypeEnum.getFmsCashSecurityAllocationRequestTracking(request.getReleaseId()), xmlResponse);

            LocalDateTime t = DateUtil.getLocalDateTimeNow();
            try {
                ReleaseEntity releaseEntity = genericDAO.findEntityById(ReleaseEntity.class, request.getReleaseId());
                releaseEntity.setReplacementChequeNumber(request.getReplacementChequeNumber());
				releaseEntity.setReplacementChequeStatus(request.getReplacementChequeStatus());
				releaseEntity.setChequeStatus(VbsConstants.FMS_CHEQUE_STATUS_CANCELLED);
                releaseEntity.setReplacementChequeDate(DateUtil.getLocalDateTimeFromCrmDateString(request.getReplacementChequeDate()));
                SecurityEntity securityEntity = releaseEntity.getSecurity();
                releaseEntity.setSettlementInstructions(request.getSettlementInstructions());
                if(securityEntity.getSecurityType().getId().equals(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT)){
                    SecurityStatusEntity statusPendingWithFee = lookupDAO.getSecurityStatusById(VbsConstants.SECURITY_STATUS_PENDING_WITH_FEE);
                    securityEntity.setStatus(statusPendingWithFee);
                }
                if (request.getReplacementPayee()!= null) {
                	ContactEntity contactEntity = new ContactEntity();
                	releaseEntity.setReplacementPayeeContact(ContactServiceTransformer.transformDTOToEntity(contactService.findContactByCrmContactId(request.getReplacementPayee(), VbsConstants.VBS_SYSTEM_USER_ID),contactEntity));
                }
                
                genericDAO.updateEntity(securityEntity, request.getReplacementUser(), t);
                genericDAO.updateEntity(releaseEntity, request.getReplacementUser(), t);
                transactionLog.setResponse(xmlResponse);
                transactionLog.setResponseReceivedDate(t);
                transactionLog.setUpdateDate(t);
                transactionLog.setUpdateUser(VbsConstants.VBS_SYSTEM_USER_ID);
                genericDAO.updateEntity(transactionLog, transactionLog.getUpdateUser(), transactionLog.getUpdateDate());
            }catch(Exception e) {
                //other unknown exception/exception thrown from process method
                LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, "processReceivedFmsXml", e, "Error occurred during processing.", xmlResponse);
                throw new VbsRuntimeException(e);
            }
        }
    }

	@Override
	public void process(UnpostRequest request, String xmlResponse) {
		if(request != null) {
			JmsTransactionLogEntity transactionLog = jmsMessageSenderService.insertTransactionLog(JmsMessageTypeEnum.getFmsCashSecurityUnpostRequestTracking(request.getSecurityId()), xmlResponse);
			try {
				LocalDateTime time = LocalDateTime.now();

				SecurityEntity securityEntity = genericDAO.findEntityById(SecurityEntity.class, request.getSecurityId());

				securityEntity.setUnpostReason(request.getUnpostReason());
				securityEntity.setUnpostDate(DateUtil.getLocalDateTimeFromCrmDateString(request.getUnpostDate()));
				securityEntity.setUnpostUser(request.getUnpostByUser());
                if(securityEntity.getSecurityType().getId().equals(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) && request.getUnpostReason() != null && (request.getUnpostReason().equals("Non-Sufficient Funds") || request.getUnpostReason().equals("NSF"))){
                    SecurityStatusEntity statusPendingWithOsFee =  lookupDAO.getSecurityStatusById(VbsConstants.SECURITY_STATUS_PENDING_WITH_OS_FEE);
                    securityEntity.setStatus(statusPendingWithOsFee);
                }
                
                if (securityEntity.getSecurityType().getId().longValue() == (VbsConstants.SECURITY_TYPE_CASH)) {
    				String comment = securityEntity.getComment();
    				if(StringUtils.isEmpty(comment)){
    					comment = "";
    				}    				
    				securityEntity.setComment(request.getUnpostReason() + " - current amount reduced to $0.00 - " + DateUtil.getDateFormattedShortTime(DateUtil.getLocalDateTimeFromCrmDateString(request.getUnpostDate())) + " Unpost by User: " + request.getUnpostByUser() + "\n\n" + comment);                	
                }

				UnpostResponse response = new UnpostResponse();
				response.setSecurityId(securityEntity.getId());
				response.setVbNumber(securityEntity.getPrimaryVb().getCrmContactId());
				response.setSecurityType(securityEntity.getSecurityType().getName());
				response.setAmount(VbsUtil.getStringForMoney(securityEntity.getCurrentAmount()));
				response.setInterest(VbsUtil.getStringForMoney(securityEntity.getCurrentInterest()));
				response.setTotal(VbsUtil.getStringForMoney(securityEntity.getCurrentAmount().add(securityEntity.getCurrentInterest())));
				response.setUpdateDate(DateUtil.getDateFormattedForCrmLong(time));

				String responseXml = VbsUtil.convertJaxbToXml(response, UnpostResponse.class);
				jmsMessageSenderService.postFmsRequestQueue(responseXml, UnpostResponse.class.getSimpleName());

				transactionLog.setResponse(responseXml);
				transactionLog.setResponseReceivedDate(time);
				transactionLog.setUpdateDate(time);
				transactionLog.setUpdateUser(VbsConstants.VBS_SYSTEM_USER_ID);
				if(securityEntity.getSecurityType() != null && securityEntity.getSecurityType().getId().longValue() == (VbsConstants.SECURITY_TYPE_CASH)) {
					securityEntity.setCurrentAmount(BigDecimal.ZERO);
				}
				genericDAO.updateEntity(securityEntity, request.getUnpostByUser(), time);
				genericDAO.updateEntity(transactionLog, transactionLog.getUpdateUser(), transactionLog.getUpdateDate());
			}
			catch(Exception e) {
				//other unknown exception/exception thrown from process method
				LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, "processReceivedFmsXml", e, "Error occured during processing.", xmlResponse);
				throw new VbsRuntimeException(e);
			}
		}
	}

	private JmsTransactionLogEntity insertResponseTransactionLog(String trackingNumber, String xmlMessageText) {
		JmsTransactionLogEntity entity = new JmsTransactionLogEntity();
		entity.setTrackingNumber(trackingNumber);
		entity.setVersion(1l);
		entity.setMessageName(trackingNumber);
		entity.setRequest("THIS IS RESPONSE FROM FMS");
		LocalDateTime now = DateUtil.getLocalDateTimeNow();
		entity.setRequestSendDate(now);
		entity.setResponse(xmlMessageText);
		entity.setResponseReceivedDate(now);
		return (JmsTransactionLogEntity)genericDAO.addEntity(entity, VbsConstants.VBS_FMS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
	}
	
	private void updateTransactionLog(String trackingNumberLike, String xmlMessageText, String trackingNumberInsert) {
		List<JmsTransactionLogEntity> logs = jmsTransactionLogDAO.findTranscationLogLikeTrackingNumber(trackingNumberLike);
		if (logs.size() == 1) {
			JmsTransactionLogEntity log = logs.get(0);
			log.setResponse(xmlMessageText);
			log.setResponseReceivedDate(DateUtil.getLocalDateTimeNow());
			genericDAO.updateEntity(log, VbsConstants.VBS_FMS_SYSTEM_USER_ID, log.getResponseReceivedDate());
		} else {
			insertResponseTransactionLog(trackingNumberInsert, xmlMessageText);;
		}
	}	


	@Override
	public void process(CreateDrawDownResponse response, String xmlResponse) {
		if (response != null && response.getStatus() != null) {
			String updateUserId = VbsConstants.VBS_FMS_SYSTEM_USER_ID;
			LocalDateTime updateTime = DateUtil.getLocalDateTimeNow();
			if (VbsConstants.FMS_RELEASE_FAILURE.equalsIgnoreCase(response.getStatus())) {
				String className = SecurityReleaseResponse.class.getSimpleName();
				String errorMessage = "FMS Failure processing Release:" + response.getId();
				LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, PROCESS + className, errorMessage);
				Long releaseId = response.getId();
				ReleaseEntity release = genericDAO.findEntityById(ReleaseEntity.class, releaseId);
				if (release == null) {
					errorMessage = FAILED_PROCESSING + className + RELEASE_NOT_FOUND + response.getId();
					LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, PROCESS + className, errorMessage);
					throw new VbsRuntimeException(errorMessage);
				}
				release.setUpdateDate(updateTime);
				release.setUpdateUser(updateUserId);
				genericDAO.updateEntity(release, updateUserId, updateTime);
			} else if (VbsConstants.FMS_RELEASE_VOUCHER_CREATED.equalsIgnoreCase(response.getStatus())) {
				Long releaseId = response.getId();
				ReleaseEntity release = genericDAO.findEntityById(ReleaseEntity.class, releaseId);
				if (release == null) {
					String className = SecurityReleaseResponse.class.getSimpleName();
					String errorMessage = FAILED_PROCESSING + className + RELEASE_NOT_FOUND + response.getId();
					LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, PROCESS + className, errorMessage);
					throw new VbsRuntimeException(errorMessage);
				}
				release.setStatus(ReleaseStatus.VOUCHER_CREATED);
				release.setUpdateDate(updateTime);
				release.setUpdateUser(updateUserId);
				genericDAO.updateEntity(release, updateUserId, updateTime);
			} else if (VbsConstants.FMS_RELEASE_PAYMENT_CREATED.equalsIgnoreCase(response.getStatus())) {
				Long releaseId = response.getId();
				ReleaseEntity release = genericDAO.findEntityById(ReleaseEntity.class, releaseId);
				if (release == null) {
					String className = SecurityReleaseResponse.class.getSimpleName();
					String errorMessage = FAILED_PROCESSING + className + RELEASE_NOT_FOUND + response.getId();
					LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, PROCESS + className, errorMessage);
					throw new VbsRuntimeException(errorMessage);
				}else if(release.getStatus().equals(ReleaseStatus.COMPLETED)){
					throw new VbsRuntimeException("Attempted to update completed release");
				}
				release.setChequeStatus(response.getChequeStatus());
				release.setStatus(ReleaseStatus.COMPLETED);
				release.setChequeNumber("" + response.getDepositId());
				release.setChequeDate(DateUtil.getLocalDateTimeFromCrmDateString(response.getDepositDate()));
				release.setUpdateDate(updateTime);
				release.setUpdateUser(response.getCollector());


				//TODO discuss the use of this code and the paid amount field on release with Keith.
//				SecurityEntity security = release.getSecurity();
//				release.setPaidAmount(VbsUtil.parseMoney(response.getAmount()));
//				if (release.getPaidAmount() != null) {
//					if (security.getCurrentAmount() != null) {
//						security.setCurrentAmount(security.getCurrentAmount().subtract(release.getPaidAmount()));
//					} else {
//						security.setCurrentAmount(BigDecimal.ZERO.subtract(release.getPaidAmount()));
//					}
//				}
//				if (release.getInterestAmount() != null) {
//					if (security.getCurrentInterest() != null) {
//						security.setCurrentInterest(security.getCurrentInterest().subtract(release.getInterestAmount()));
//					} else {
//						security.setCurrentInterest(BigDecimal.ZERO.subtract(release.getInterestAmount()));
//					}
//				}
//				security.setUpdateDate(updateTime);
//				security.setUpdateUser(updateUserId);
//				security = (SecurityEntity)genericDAO.updateEntity(security, updateUserId, updateTime);
//				release.setSecurity(security);

				genericDAO.updateEntity(release, updateUserId, updateTime);
			}
			insertResponseTransactionLog(JmsMessageTypeEnum.getFmsSecurityDrawdownResponseTracking(response.getId()), xmlResponse);
		} else {
			//error reading response
			String className = ((response == null) ? "" : response.getClass().getSimpleName());
			String errorMessage = FAILED_PROCESSING + className + " - empty ressponse object. XML is:" + xmlResponse;
			LoggerUtil.logError(FmsQueueResponseProcessorImpl.class, PROCESS + className, errorMessage);
			throw new VbsRuntimeException(errorMessage);
		}		
	}

}
