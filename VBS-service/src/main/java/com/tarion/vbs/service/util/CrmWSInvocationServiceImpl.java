/* 
 * 
 * CrmWSInvocationServiceImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import com.tarion.crm.contactws.TWCCONTACTWS;
import com.tarion.crm.contactws.TWCCONTACTWSPortType;
import com.tarion.crm.vbsws.*;
import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.enums.CRMSearchTypeEnum;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;

import createworklistrequest.CreateWorklistRequest;
import createworklistresponse.CreateWorklistResponse;
import twc_service_namespace.twc_worklist_ws.TWCWORKLISTWS;
import twc_service_namespace.twc_worklist_ws.TWCWORKLISTWSPortType;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;



/**
 * Utility Bean to invoke CRM Web Service Client
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-01-31
 * @version 1.0
 */
@Stateless(mappedName = "ejb/CrmWSInvocationService")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged -- removed because of pointless log spam
public class CrmWSInvocationServiceImpl implements CrmWSInvocationService {
	
	private TWCVBSWSPortType crmVbsWS = null;
	private TWCCONTACTWSPortType crmContactWS = null;
	private TWCWORKLISTWSPortType crmWorklistWS = null;

	@EJB
	private PropertyService propertyService;

	private TWCVBSWSPortType getCrmVbsWS() throws MalformedURLException {
		if (crmVbsWS == null) {
			String wsdlUrlString = propertyService.getValue(VbsConstants.CRM_VBS_WEB_SERVICE_WSDL_URL);
			URL webServiceUrl = new URL(wsdlUrlString);
			QName serviceName = new QName("TWC_SERVICE_NAMESPACE/TWC_VBS_WS.1", "TWC_VBS_WS");

			TWCVBSWS service = new TWCVBSWS(webServiceUrl, serviceName);
			crmVbsWS = service.getTWCVBSWSPort();
			
			int n = wsdlUrlString.lastIndexOf('/');
			String endPointUrl = wsdlUrlString.substring(0, n);

			BindingProvider bp = (BindingProvider) crmVbsWS;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPointUrl);
			
		}
		return crmVbsWS;
	}
	
	private TWCCONTACTWSPortType getCrmContactWS() throws MalformedURLException {
		if (crmContactWS == null) {
			String wsdlUrlString = propertyService.getValue(VbsConstants.CRM_CONTACT_WEB_SERVICE_WSDL_URL);
			URL webServiceUrl = new URL(wsdlUrlString);
			QName serviceName = new QName("TWC_SERVICE_NAMESPACE/TWC_CONTACT_WS.1", "TWC_CONTACT_WS");

			TWCCONTACTWS service = new TWCCONTACTWS(webServiceUrl, serviceName);
			crmContactWS = service.getTWCCONTACTWSPort();

			int n = wsdlUrlString.lastIndexOf('/');
			String endPointUrl = wsdlUrlString.substring(0, n);

			BindingProvider bp = (BindingProvider) crmContactWS;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPointUrl);
		}
		return crmContactWS;
	}
	
	private TWCWORKLISTWSPortType getCrmWorklistWS() throws MalformedURLException {
		if (crmWorklistWS == null) {
			String wsdlUrlString = propertyService.getValue(VbsConstants.CRM_WORKLIST_WEB_SERVICE_URL);
			URL webServiceUrl = new URL(wsdlUrlString);
			QName serviceName = new QName("TWC_SERVICE_NAMESPACE/TWC_WORKLIST_WS.1", "TWC_WORKLIST_WS");

			TWCWORKLISTWS service = new TWCWORKLISTWS(webServiceUrl, serviceName);
			crmWorklistWS = service.getTWCWORKLISTWSPort();

			int n = wsdlUrlString.lastIndexOf('/');
			String endPointUrl = wsdlUrlString.substring(0, n);

			BindingProvider bp = (BindingProvider) crmWorklistWS;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPointUrl);
		}
		return crmWorklistWS;
	}

	@Override
	public CreateWorklistResponse createCrmWorklistItem(CreateWorklistRequest createWorklistRequest) {
		try {
			CreateWorklistResponse response = getCrmWorklistWS().twcWORKLISTCREATE(createWorklistRequest);
			if(!response.getSTATUS().equals(VbsConstants.CRM_WS_PASS)) {
				String exceptionMessage = "Error occured while invoking CRM Web Service TWCWORKLISTWSPortType.twcWORKLISTCREATE, error message: " + response.getSTATUS();
				LoggerUtil.logError(CrmWSInvocationServiceImpl.class, "createCrmWorklistItem", exceptionMessage);
			}

			return response;
		} catch (Exception e) {
			String exceptionMessage = "Exception occured while invoking CRM Web Service TWCWORKLISTWSPortType.twcWORKLISTCREATE";
			LoggerUtil.logError(CrmWSInvocationServiceImpl.class, "createCrmWorklistItem", e, exceptionMessage);
		}
		return null;
	}

	@Override
	public GetSecHomeInfoResponse getEnrolments(List<String> enrolmentNumbers) {
		try {
			
			GetSecHomeInfoRequest request = new GetSecHomeInfoRequest();
			request.getENROLMENTNUMBER().addAll(enrolmentNumbers);
			GetSecHomeInfoResponse response = getCrmVbsWS().twcVBSSECHOMEINFO(request);
			if(!response.getSTATUS().equals("OK")) {
				String exceptionMessage = "Error occured while invoking CRM Web Service TWCVBSWSPortType.twcVBSSECHOMEINFO, error message: " + response.getERRORMSG();
				LoggerUtil.logError(CrmWSInvocationServiceImpl.class, "getEnrolments");
				throw new VbsRuntimeException(exceptionMessage);				
			}

			return response;
			
		} catch (Exception e) {
			String exceptionMessage = "Error occured while invoking CRM Web Service TWCVBSWSPortType.twcVBSSECHOMEINFO";
			LoggerUtil.logError(CrmWSInvocationServiceImpl.class, "getEnrolments", e);
			throw new VbsRuntimeException(exceptionMessage, e);
		}
	}
	
	@Override
	public GetSecHomeInfoResponse.SECHOMEINFO getEnrolment(String enrolmentNumber) {
		GetSecHomeInfoResponse response = getEnrolments(Collections.singletonList(enrolmentNumber));
		if(response.getSECHOMEINFO().isEmpty()) {
			return null;
		}
		return response.getSECHOMEINFO().get(0);

	}
	
	@Override
	public GetContactInfoResponse getVbsContactInfo(String name) {
		return getVbsContactInfo(name, null);
	}

	@Override
	public GetContactInfoResponse getVbsContactInfo(String name, CRMSearchTypeEnum type) {
		GetContactInfoRequest request = new GetContactInfoRequest();
		request.setCONTACTNAME(name);
		request.setSEARCHTYPE(type.getType());
		GetContactInfoResponse response;
		try {
			response = getCrmVbsWS().twcVBSCONTACTINFO(request);
			return response;
		} catch (Exception e) {
			String exceptionMessage = "Error occured while invoking CRM Web Service TWCVBSWSPortType.twcVBSCONTACTINFO";
			LoggerUtil.logError(CrmWSInvocationServiceImpl.class, "getVbsContactInfo", e);
			throw new VbsRuntimeException(exceptionMessage, e);
		}
	}

	@Override
	public com.tarion.crm.contactws.GetContactInfoResponse getContactInfoByCrmContactId(String crmContactId){
		com.tarion.crm.contactws.GetContactInfoRequest request = new com.tarion.crm.contactws.GetContactInfoRequest();
		request.setCONTACTID(crmContactId);
		try{
			return getCrmContactWS().twcCONTACTINFO(request);
		} catch (Exception e) {
			String exceptionMessage = "Error occured while invoking CRM Web Service TWCCONTACTWSPortType.twcCONTACTINFO";
			LoggerUtil.logError(CrmWSInvocationServiceImpl.class, "getVbsContactInfo", e);
			throw new VbsRuntimeException(exceptionMessage, e);
		}
	}
}
