/* 
 * 
 * ProcessorTriggerTypeEnum.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import java.time.LocalDateTime;

/**
 * This class is used to post a message to a JMS queue
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-02-14
 * @version 1.0
 */
public enum JmsMessageTypeEnum {
	FMS_CASH_SECURITY_REQUEST
	, FMS_CASH_SECURITY_RESPONSE
	, FMS_CREATE_DTA_REQUEST
	, FMS_INTEREST_REQUEST
	, EMAIL_SENT_MAIL
	, FMS_SECURITY_RELEASE_REQUEST
	, FMS_SECURITY_RELEASE_RESPONSE
	, FMS_SECURITY_DRAWDOWN_REQUEST
	, FMS_SECURITY_DRAWDOWN_RESPONSE
	, FMS_WRITE_OFF
	, CRM_UPDATE_VBS_DELTA_ENROLMENT_RESPONSE
	, CRM_UPDATE_VBS_DELTA_COMPANY_RESPONSE
	, CRM_UPDATE_VBS_DELTA_PERSON_RESPONSE
	, CRM_WARRANTY_SERVICE_REQUEST
	, CRM_WARRANTY_SERVICE_RESPONSE
	, CRM_WARRANTY_SERVICE_RECOMMENDATION_RESPONSE
	, CRM_AUTO_RELEASE_ENROLMENTS_REQUEST
	, CRM_AUTO_RELEASE_ENROLMENTS_RESPONSE
	, CRM_AUTO_RELEASE_VBS_REQUEST
	, CRM_AUTO_RELEASE_VBS_RESPONSE
	, CRM_FINANCIAL_INSTITUTION_UPDATE
	, CRM_FINANCIAL_INSTITUTION_UPDATE_RESPONSE
	, FMS_CASH_SECURITY_ALLOCATION_REQUEST
	, FMS_CASH_SECURITY_UNPOST_REQUEST
	, FMS_CASH_SECURITY_UNPOST_RESPONSE
	, BSA_CASH_SECURITY_REQUEST
	, BSA_CASH_SECURITY_RESPONSE
	, TIP_CORRESPONDENCE_GENERATION_REQUEST
	;

	public static String getTipCorrespondenceGenerationRequestTracking(Long securityId) {
		return TIP_CORRESPONDENCE_GENERATION_REQUEST.toString() + "-" + securityId.toString() + "-" + System.currentTimeMillis();
	}
	public static String getBsaCashSecurityRequestTracking(Long requestId, String vbNumber) {
		return BSA_CASH_SECURITY_REQUEST.toString() + "-" + requestId + "-" + vbNumber;
	}
	public static String getBsaCashSecurityResponseTracking(Long requestId, String vbNumber) {
		return BSA_CASH_SECURITY_RESPONSE.toString() + "-" + requestId + "-" + vbNumber;
	}
	public static String getFmsCashSecurityRequestTracking(Long securityId, String vbNumber) {
		return FMS_CASH_SECURITY_REQUEST.toString() + "-" + securityId + "-" + vbNumber + "-" + System.currentTimeMillis();
	}
	public static String getFmsCreateDTARequestTracking(Long securityId, String vbNumber) {
		return FMS_CREATE_DTA_REQUEST.toString() + "-" + securityId + "-" + vbNumber + "-" + System.currentTimeMillis();
	}
	public static String getFmsCashSecurityResponseTracking(Long securityId) {
		return FMS_CASH_SECURITY_RESPONSE.toString() + "-" + securityId + "-" + System.currentTimeMillis();
	}
	public static String getFmsCashSecurityResponseTrackingUpdate(Long securityId) {
		return FMS_CASH_SECURITY_REQUEST.toString() + "-" + securityId;
	}
	public static String getFmsCashSecurityAllocationRequestTracking(Long securityId) {
		return FMS_CASH_SECURITY_ALLOCATION_REQUEST.toString() + "-" + securityId + "-" + System.currentTimeMillis();
	}
	public static String getFmsCashSecurityUnpostRequestTracking(Long securityId) {
		return FMS_CASH_SECURITY_UNPOST_REQUEST.toString() + "-" + securityId + "-" + System.currentTimeMillis();
	}
	public static String getFmsCashSecurityUnpostResponseTracking(Long securityId) {
		return FMS_CASH_SECURITY_UNPOST_RESPONSE.toString() + "-" + securityId + "-" + System.currentTimeMillis();
	}
	public static String getFmsSecurityReleaseRequestTracking(Long releaseId, Long sequence) {
		return FMS_SECURITY_RELEASE_REQUEST.toString() + "-" + releaseId + "-" + sequence + "-" + System.currentTimeMillis();
	}

	public static String getFmsSecurityReleaseResponseTracking(Long releaseId) {
		return FMS_SECURITY_RELEASE_RESPONSE.toString() + "-" + releaseId + "-" + System.currentTimeMillis();
	}
	
	public static String getFmsSecurityDrawdownRequestTracking(Long releaseId, Long sequence) {
		return FMS_SECURITY_DRAWDOWN_REQUEST.toString() + "-" + releaseId + "-" + sequence + "-" + System.currentTimeMillis();
	}
	public static String getFmsWriteOffTracking(long securityId) {
		return FMS_WRITE_OFF.toString() + "-" + securityId +  "-" + System.currentTimeMillis();
	}
	public static String getFmsSecurityDrawdownResponseTracking(Long releaseId) {
		return FMS_SECURITY_DRAWDOWN_RESPONSE.toString() + "-" + releaseId + "-" + System.currentTimeMillis();
	}

	public static String getFmsInterestRequestTracking(LocalDateTime interestDate) {
		return FMS_INTEREST_REQUEST.toString() + "-" + interestDate + System.currentTimeMillis();
	}
	
	public static String getCrmUpdateVbsDeltaEnrolmentResponseTracking(String xmlResponse) {
		int hash = (xmlResponse + System.currentTimeMillis()).hashCode();
		return CRM_UPDATE_VBS_DELTA_ENROLMENT_RESPONSE.toString() + "-" + hash + System.currentTimeMillis();
	}
	
	public static String getCrmUpdateVbsDeltaCompanyResponseTracking(String xmlResponse) {
		int hash = (xmlResponse + System.currentTimeMillis()).hashCode();
		return CRM_UPDATE_VBS_DELTA_COMPANY_RESPONSE.toString() + "-" + hash + System.currentTimeMillis();
	}
	
	public static String getCrmUpdateVbsDeltaPersonResponseTracking(String xmlResponse) {
		int hash = (xmlResponse + System.currentTimeMillis()).hashCode();
		return CRM_UPDATE_VBS_DELTA_PERSON_RESPONSE.toString() + "-" + hash + System.currentTimeMillis();
	}
	
	public static String getCrmWarrantyServiceRequestTracking(Long releaseId, String enrolmentNumber) {
		return CRM_WARRANTY_SERVICE_REQUEST.toString() + "-" + releaseId + "-" + enrolmentNumber + "-" + System.currentTimeMillis();
	}
	public static String getCrmWarrantyServiceResponseTrackingUpdate(Long releaseId, String enrolmentNumber) {
		return CRM_WARRANTY_SERVICE_REQUEST.toString() + "-" + releaseId + "-" + enrolmentNumber;
	}
	public static String getCrmWarrantyServiceResponseTrackingInsert(Long releaseId, String enrolmentNumber) {
		return CRM_WARRANTY_SERVICE_RESPONSE.toString() + "-" + releaseId + "-" + enrolmentNumber  + System.currentTimeMillis();
	}
	public static String getCrmWarrantyServiceRecommendationResponseTrackingInsert(String enrolmentNumber, String serviceOrderId) {
		return CRM_WARRANTY_SERVICE_RECOMMENDATION_RESPONSE.toString() + enrolmentNumber + "-" + serviceOrderId + System.currentTimeMillis();
	}
	
	public static String getEmailSentMailTracking() {
		return EMAIL_SENT_MAIL.toString() + "-" + System.currentTimeMillis();
	}

	public static String getCrmAutoReleaseEnrolmentsRequestTracking(Long runId) {
		return CRM_AUTO_RELEASE_ENROLMENTS_REQUEST + "-" + runId;
	}

	public static String getCrmAutoReleaseEnrolmentsResponseTracking(Long runId) {
		return CRM_AUTO_RELEASE_ENROLMENTS_RESPONSE + "-" + runId;
	}

	public static String getCrmAutoReleaseVBsRequestTracking(Long runId) {
		return CRM_AUTO_RELEASE_VBS_REQUEST + "-" + runId;
	}

	public static String getCrmAutoReleaseVBsResponseTracking(Long runId) {
		return CRM_AUTO_RELEASE_VBS_RESPONSE + "-" + runId;
	}

	public static String getCrmFinancialInstitutionUpdateTracking(Long id) {
		return CRM_FINANCIAL_INSTITUTION_UPDATE + "-" + id + System.currentTimeMillis();
	}
	public static String getCrmFinancialInstitutionUpdateResponseTracking(String xmlResponse) {
		int hash = (xmlResponse + System.currentTimeMillis()).hashCode();
		return CRM_FINANCIAL_INSTITUTION_UPDATE_RESPONSE.toString() + "-" + hash + System.currentTimeMillis();
	}
}