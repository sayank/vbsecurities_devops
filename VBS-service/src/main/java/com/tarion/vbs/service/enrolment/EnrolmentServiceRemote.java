package com.tarion.vbs.service.enrolment;

import com.tarion.vbs.common.dto.enrolment.EnrolmentActivityDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentDTO;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface EnrolmentServiceRemote {

	EnrolmentDTO getCRMEnrolment(String enrolmentNumber);
	EnrolmentDTO getEnrolment(String enrolmentNumber, Long poolId, Long securityIdentityTypeId);
	EnrolmentDTO getEnrolmentForSecurity(String enrolmentNumber, Long securityId);
	List<EnrolmentDTO> getAvailableEnrolmentListByPoolId(Long poolId, boolean onlyCondoConv, Long securityIdentityTypeId);
	List<EnrolmentDTO> getCRMEnrolmentListByPoolId(Long poolId);
	List<EnrolmentActivityDTO> getEnrolmentActivitiesForEnrolmentNumber(String enrolmentNumber);

}
