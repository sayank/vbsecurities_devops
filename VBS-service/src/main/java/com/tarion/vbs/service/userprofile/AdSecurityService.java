/* 
 * 
 * AdSecurityBeanRemote.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.userprofile;
import java.util.List;

import javax.ejb.Local;

import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.exceptions.VbsDirectoryEnvironmentException;


/**
 * @author Joni Paananen
 * 
 * AdSecurityService for connecting to the AD server
 *
 */
@Local
public interface AdSecurityService {
	
	public UserDTO authenticate(String userId, String password) throws VbsCheckedException;
	public List<UserDTO> getAllADUsers() throws VbsDirectoryEnvironmentException;
	public UserDTO getUser(String userId) throws VbsDirectoryEnvironmentException;
	
}
