package com.tarion.vbs.service.contact;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.contact.EscrowAgentSearchResultDTO;
import com.tarion.vbs.common.dto.AddressDTO;
import com.tarion.vbs.common.dto.NameDescriptionDTO;
import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.orm.entity.AddressEntity;
import com.tarion.vbs.orm.entity.contact.ContactEntity;
import com.tarion.vbs.orm.entity.contact.EscrowLicenseApplicationStatusEntity;
import com.tarion.vbs.orm.entity.contact.LicenseStatusEntity;

public class ContactServiceTransformer {
	private ContactServiceTransformer() {}

	public static EscrowAgentSearchResultDTO transformEntityToDTO(EscrowAgentSearchResultDTO dto, ContactEntity entity) {

		dto.setId(entity.getId().intValue());
		dto.setCrmContactId(entity.getCrmContactId());
		dto.setEscrowAgent(entity.getCompanyName());
		if(entity.getEscrowLicenseApplicationStatus() != null) {
			dto.setEscrowAgentLicenceStatus(entity.getLicenseStatus().getDescription());
		}
		if(entity.getEscrowLicenseApplicationStatus() != null) {
			dto.setLicenceApplicationStatus(entity.getEscrowLicenseApplicationStatus().getDescription());
		}
		return dto;
	}

	public static ContactDTO transformEntityToDTO(ContactEntity entity){
		ContactDTO dto = new ContactDTO();
		if(entity == null){return dto;}
		dto.setCreateDate(entity.getCreateDate());
		dto.setCreateUser(entity.getCreateUser());
		dto.setCompanyName(entity.getCompanyName());
		dto.setCrmContactId(entity.getCrmContactId());
		dto.setFirstName(entity.getFirstName());
		dto.setLastName(entity.getLastName());
		dto.setPhoneNumber(entity.getPhone());
		dto.setPhoneNumberExt(entity.getPhoneExtension());
		dto.setDob(DateUtil.getDateFormattedMedium(entity.getDateOfBirth()));
		dto.setDriversLicense(entity.getDriversLicence());
		dto.setEmailAddress(entity.getEmailAddress());
		dto.setId(entity.getId());
		dto.setVersion(entity.getVersion());
		dto.setSin(entity.getSin());
		dto.setUpdateDate(entity.getUpdateDate());
		dto.setUpdateUser(entity.getUpdateUser());
		dto.setAlert(entity.getAlert());
		dto.setUnwilling(entity.getUnwilling());
		dto.setAccessibility(entity.isAccessibility());
		dto.setAddress(transformEntityToDTO(entity.getAddress()));
		dto.setMailingAddress(transformEntityToDTO(entity.getMailingAddress()));
		dto.setYellowSticky(entity.isYellowSticky());
		dto.setApVendorFlag(entity.getApVendorFlag());
		if(entity.getContactType()!=null && (entity.getContactType().getId().equals(VbsConstants.CONTACT_TYPE_ESCROW_AGENT) || (entity.getContactType().getId().equals(VbsConstants.CONTACT_TYPE_VB)))){
			LicenseStatusEntity as = entity.getLicenseStatus();
			if(as != null) {
                NameDescriptionDTO asDTO = new NameDescriptionDTO(as.getId(), as.getName(), as.getDescription());
                dto.setLicenseStatus(asDTO);
            }

			if(entity.getContactType().getId().equals(VbsConstants.CONTACT_TYPE_ESCROW_AGENT)) {
				EscrowLicenseApplicationStatusEntity las = entity.getEscrowLicenseApplicationStatus();
				if(las != null){
				    NameDescriptionDTO lasDTO = new NameDescriptionDTO(las.getId(), las.getName(), las.getDescription());
				    dto.setEscrowAgentLicenseApplicationStatus(lasDTO);
				}
			}
		}

		return dto;
	}

	public static AddressDTO transformEntityToDTO(AddressEntity addressEntity) {
		if(addressEntity == null){return null;}

		AddressDTO addressDto = new AddressDTO();
		addressDto.setAddressLine1(addressEntity.getAddressLine1());
		addressDto.setAddressLine2(addressEntity.getAddressLine2());
		addressDto.setAddressLine3(addressEntity.getAddressLine3());
		addressDto.setAddressLine4(addressEntity.getAddressLine4());
		addressDto.setCity(addressEntity.getCity());
		addressDto.setCountry(addressEntity.getCountry());
		addressDto.setProvince(addressEntity.getProvince());
		addressDto.setPostalCode(addressEntity.getPostalCode());
		addressDto.setCreateDate(addressEntity.getCreateDate());
		addressDto.setCreateUser(addressEntity.getCreateUser());
		addressDto.setId(addressEntity.getId());
		addressDto.setUpdateDate(addressEntity.getUpdateDate());
		addressDto.setUpdateUser(addressEntity.getUpdateUser());
		addressDto.setAddressConcat(addressEntity.concatAddress());

		return addressDto;
	}
	
	public static AddressEntity transformDTOToEntity(AddressDTO addressDTO, AddressEntity addressEntity) {
		if(addressDTO == null){return null;}

		if(addressEntity == null) {
			addressEntity = new AddressEntity();
		}
		addressEntity.setAddressLine1(addressDTO.getAddressLine1());
		addressEntity.setAddressLine2(addressDTO.getAddressLine2());
		addressEntity.setAddressLine3(addressDTO.getAddressLine3());
		addressEntity.setAddressLine4(addressDTO.getAddressLine4());
		addressEntity.setCity(addressDTO.getCity() == null ? "" : addressDTO.getCity() );
		addressEntity.setCountry(addressDTO.getCountry() == null ? "" : addressDTO.getCity() );
		addressEntity.setPostalCode(addressDTO.getPostalCode()  == null ? "" : addressDTO.getPostalCode() );
		addressEntity.setProvince(addressDTO.getProvince() == null ? "" : addressDTO.getProvince() );
		addressEntity.setCreateDate(addressDTO.getCreateDate());
		addressEntity.setCreateUser(addressDTO.getCreateUser());
		addressEntity.setId(addressDTO.getId());
		addressEntity.setUpdateDate(addressDTO.getUpdateDate());
		addressEntity.setUpdateUser(addressDTO.getUpdateUser());

		return addressEntity;
	}
	
	public static ContactEntity transformDTOToEntity(ContactDTO contactDTO, ContactEntity contactEntity) {
		contactEntity.setId(contactDTO.getId());
		contactEntity.setVersion(contactDTO.getVersion());
		contactEntity.setCompanyName(contactDTO.getCompanyName());
		contactEntity.setCreateDate(contactDTO.getCreateDate());
		contactEntity.setCreateUser(contactDTO.getCreateUser());
		contactEntity.setCrmContactId(contactDTO.getCrmContactId());
		contactEntity.setFirstName(contactDTO.getFirstName());
		contactEntity.setLastName(contactDTO.getLastName());
		contactEntity.setPhone(contactDTO.getPhoneNumber());
		contactEntity.setPhoneExtension(contactDTO.getPhoneNumberExt());
		contactEntity.setPhoneExtension(contactDTO.getPhoneNumberExt());
		contactEntity.setEmailAddress(contactDTO.getEmailAddress());
		transformDTOToEntity(contactDTO.getAddress(), contactEntity.getAddress());
		transformDTOToEntity(contactDTO.getMailingAddress(), contactEntity.getMailingAddress());
		contactEntity.setAlert(contactDTO.getAlert());
		contactEntity.setUnwilling(contactDTO.getUnwilling());
		contactEntity.setAccessibility(contactDTO.isAccessibility());
		contactEntity.setYellowSticky(contactDTO.isYellowSticky());
		contactEntity.setApVendorFlag(contactDTO.getApVendorFlag());
		return contactEntity;
	}
}
