package com.tarion.vbs.service.correspondence;

import com.tarion.vbs.common.dto.correspondence.CorrespondenceWSRequest;
import com.tarion.vbs.common.dto.correspondence.CorrespondenceWSResponse;
import com.tarion.vbs.common.enums.CorrespondenceActionEnum;
import com.tarion.vbs.orm.entity.release.ReleaseEntity;

import javax.ejb.Local;

/**
 * @author <A href="joni.paananen@tarion.com">Joni Paananen</A>
 * @since 2019-02-19
 * @version 1.0
 */

@Local
public interface CorrespondenceService extends CorrespondenceServiceRemote {

    CorrespondenceWSResponse getTokens(CorrespondenceWSRequest request);

    void sendAutoReleaseCorrespondenceRequest(CorrespondenceActionEnum action, String enrolmentNumber, ReleaseEntity release, String userId);
}
