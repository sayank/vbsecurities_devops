package com.tarion.vbs.service.release;

import com.tarion.vbapplications.service.application.VbNumberStatusIdDTO;
import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.autorelease.*;
import com.tarion.vbs.common.dto.release.AutoReleaseDTO;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.enums.CorrespondenceActionEnum;
import com.tarion.vbs.common.enums.ReleaseApprovalStatusEnum;
import com.tarion.vbs.common.enums.ReleaseStatus;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.release.AutoReleaseDAO;
import com.tarion.vbs.dao.release.ReleaseDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.dao.util.LookupDAO;
import com.tarion.vbs.orm.entity.autorelease.AutoReleaseEnrolmentEntity;
import com.tarion.vbs.orm.entity.autorelease.AutoReleaseEnrolmentStatusEntity;
import com.tarion.vbs.orm.entity.autorelease.AutoReleaseRunEntity;
import com.tarion.vbs.orm.entity.autorelease.AutoReleaseRunStatusEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
import com.tarion.vbs.orm.entity.release.*;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import com.tarion.vbs.service.correspondence.CorrespondenceService;
import com.tarion.vbs.service.jaxb.autorelease.AutoReleaseCreateReleaseRequest;
import com.tarion.vbs.service.jaxb.crm.autorelease.request.AutoReleaseEnrolmentsRequest;
import com.tarion.vbs.service.jaxb.crm.autorelease.request.AutoReleaseVBsRequest;
import com.tarion.vbs.service.processor.ProcessorTriggeringService;
import com.tarion.vbs.service.userprofile.UserService;
import com.tarion.vbs.service.util.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Release Service provides operations for release 
 *
 * @author <A href="shadi.kajevand@tarion.com">Shadi Kajevand</A>
 * @since 2018-01-22
 * @version 1.0
 */
@Stateless(mappedName = "ejb/AutoReleaseService")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class AutoReleaseServiceImpl implements AutoReleaseService, AutoReleaseServiceRemote{


	@EJB
	private AutoReleaseDAO autoReleaseDAO;
	@EJB
	private ReleaseService releaseService;
	@EJB
	private AutoReleaseCriteriaService autoReleaseCriteriaService;

	@EJB
	private GenericDAO genericDAO;
	@EJB
	private JmsMessageSenderService jmsMessageSenderService;
	@EJB
	private ProcessorTriggeringService processorTriggeringService;
	@EJB
	private VBAWSInvocationService vbawsInvocationService;
	@EJB
	private LookupDAO lookupDAO;
	@EJB
	private ReleaseDAO releaseDAO;
	@EJB
	private PropertyService propertyService;
	@EJB
	private EmailWSInvocationService emailWSInvocationService;
	@EJB
	private UserService userService;
	@EJB
	private CorrespondenceService correspondenceService;


	@Override
	public boolean existsRunInProgress() {
		return autoReleaseDAO.getAutoReleaseRunInProgress() != null;
	}

	@Override
	public void startAutoReleaseProcess(UserDTO user) throws VbsCheckedException {

		if(autoReleaseDAO.getAutoReleaseRunInProgress() != null) {
			throw new VbsCheckedException("Cannot start auto release run, another run is in progress");
		}

		AutoReleaseRunEntity run = createRun(user);

		List<AutoReleaseCandidateDTO> candidates = autoReleaseDAO.getAutoReleaseCandidates();
		Set<String> enrolmentNumbers = new HashSet<>();
		Set<String> vbNumbers = new HashSet<>();
		Set<Pair<Long, String>> addedEnrolments = new HashSet<>();

		List<AutoReleaseCandidateDTO> enrolments = new ArrayList<>();

		for (AutoReleaseCandidateDTO candidate : candidates) {
			vbNumbers.add(candidate.getVbNumber());

			Pair<Long, String> key = Pair.of(candidate.getSecurityId(), candidate.getEnrolmentNumber());
			if(!addedEnrolments.contains(key)) {
				addedEnrolments.add(key);
				enrolmentNumbers.add(candidate.getEnrolmentNumber());
				candidate.setAutoReleaseRunId(run.getId());
				enrolments.add(candidate);
			}
		}

        autoReleaseDAO.addAutoReleaseEnrolments(enrolments);
		sendAutoReleaseMessages(run.getId(), vbNumbers, enrolmentNumbers);
	}

	private void sendAutoReleaseMessages(Long runId, Set<String> vbNumbers, Set<String> enrolmentNumbers) {

		AutoReleaseEnrolmentsRequest enrolmentsRequest = new AutoReleaseEnrolmentsRequest();
		enrolmentsRequest.setAutoReleaseRunId(runId);
		enrolmentsRequest.setEnrolments(new AutoReleaseEnrolmentsRequest.Enrolments());
		for (String enrolment: enrolmentNumbers) {
			enrolmentsRequest.getEnrolments().getEnrolmentNumber().add(enrolment);
		}

		String xmlMessageText = VbsUtil.convertJaxbToXml(enrolmentsRequest, AutoReleaseEnrolmentsRequest.class);
		jmsMessageSenderService.postCrmRequestQueue(xmlMessageText, AutoReleaseEnrolmentsRequest.class.getSimpleName());
		jmsMessageSenderService.insertTransactionLog(JmsMessageTypeEnum.getCrmAutoReleaseEnrolmentsRequestTracking(runId), xmlMessageText);

		AutoReleaseVBsRequest vBsRequest = new AutoReleaseVBsRequest();
		vBsRequest.setAutoReleaseRunId(runId);
		vBsRequest.setVbList(new AutoReleaseVBsRequest.VbList());

		for (String vbNumber: vbNumbers) {
			vBsRequest.getVbList().getVbNumber().add(vbNumber);
		}

		xmlMessageText = VbsUtil.convertJaxbToXml(vBsRequest, AutoReleaseVBsRequest.class);
		jmsMessageSenderService.postCrmRequestQueue(xmlMessageText, AutoReleaseVBsRequest.class.getSimpleName());
		jmsMessageSenderService.insertTransactionLog(JmsMessageTypeEnum.getCrmAutoReleaseVBsRequestTracking(runId), xmlMessageText);

		processorTriggeringService.triggerAutoReleaseGetVBApplicationStatus(runId, new ArrayList<>(vbNumbers));

	}


	private AutoReleaseRunEntity createRun(UserDTO user) {
		AutoReleaseRunEntity run = new AutoReleaseRunEntity();
		AutoReleaseRunStatusEntity status = genericDAO.findEntityById(AutoReleaseRunStatusEntity.class, VbsConstants.AUTO_RELEASE_RUN_STATUS_INIT_PROCESSING);

		run.setEnrolmentsReceived(false);
		run.setRunStatus(status);
		run.setVbaReceived(false);
		run.setVbsReceived(false);

		return (AutoReleaseRunEntity) genericDAO.addEntity(run, user.getUserId(), DateUtil.getLocalDateTimeNow());
	}

	@Override
	public void getApplicationStatusesFromVBA(Long autoReleaseRunId, List<String> vbNumbers) throws VbsCheckedException {
		List<VbNumberStatusIdDTO> response = vbawsInvocationService.getApplicationStatusForVBs(vbNumbers);

		List<AutoReleaseVbApplicationStatusDTO> dtos = response.stream().map(dto -> new AutoReleaseVbApplicationStatusDTO(autoReleaseRunId, dto.getVbNumber(), dto.getStatusId())).collect(Collectors.toList());
		autoReleaseDAO.addAutoReleaseVbApplicationStatusEntities(dtos);

		AutoReleaseRunEntity run = genericDAO.findEntityById(AutoReleaseRunEntity.class, autoReleaseRunId);
		run.setVbaReceived(true);
		genericDAO.updateEntity(run, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());

		processorTriggeringService.triggerCheckDataReceived(autoReleaseRunId);
	}

	@Override
	public void checkDataReceived(Long autoReleaseRunId) {
		AutoReleaseRunEntity run = genericDAO.findEntityById(AutoReleaseRunEntity.class, autoReleaseRunId);
		if(run.isEnrolmentsReceived() && run.isVbaReceived() && run.isVbsReceived() && run.getRunStatus().getId() == VbsConstants.AUTO_RELEASE_RUN_STATUS_INIT_PROCESSING) {
			run.setRunStatus(genericDAO.findEntityById(AutoReleaseRunStatusEntity.class, VbsConstants.AUTO_RELEASE_RUN_STATUS_INIT_PROCESSING_CRITERIA));
			genericDAO.updateEntity(run, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());

			for (AutoReleaseEnrolmentEntity enrolment : autoReleaseDAO.getAutoReleaseEnrolmentsForRun(autoReleaseRunId)) {
				processorTriggeringService.triggerEvaluateCriteria(enrolment.getId());
			}
		}
	}

	@Override
	public void evaluateCriteria(Long autoReleaseEnrolmentId) {

		AutoReleaseEnrolmentEntity enrolment = genericDAO.findEntityById(AutoReleaseEnrolmentEntity.class, autoReleaseEnrolmentId);

		autoReleaseCriteriaService.evaluateCriteria(enrolment);

		enrolment.setStatus(genericDAO.findEntityById(AutoReleaseEnrolmentStatusEntity.class, VbsConstants.AUTO_RELEASE_ENROLMENT_STATUS_INIT_COMPLETE));
		genericDAO.updateEntity(enrolment, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());

		updateEnrolmentTimingCode(enrolment);

		processorTriggeringService.triggerCheckCriteriaComplete(enrolment.getAutoReleaseRun().getId());

	}

	private void updateEnrolmentTimingCode(AutoReleaseEnrolmentEntity enrolment) {

		if(enrolment.getTimingType().getId() == VbsConstants.AUTO_RELEASE_TIMING_TYPE_EXCESS_DEPOSIT) {
			enrolment.getEnrolment().setExcessDepositRelease(false);
		} else if(enrolment.getTimingType().getId() == VbsConstants.AUTO_RELEASE_TIMING_TYPE_ONE_YEAR) {
			enrolment.getEnrolment().setAutoOneYear(false);
		} else if(enrolment.getTimingType().getId() == VbsConstants.AUTO_RELEASE_TIMING_TYPE_RFC) {
			enrolment.getEnrolment().setAutoOneYearRfc(false);
		} else if(enrolment.getTimingType().getId() == VbsConstants.AUTO_RELEASE_TIMING_TYPE_TWO_YEAR) {
			enrolment.getEnrolment().setAutoTwoYear(false);
		}

		genericDAO.updateEntity(enrolment.getEnrolment(), VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
	}

	@Override
	public void checkCriteriaComplete(Long autoReleaseRunId) {
		AutoReleaseRunEntity run = genericDAO.findEntityById(AutoReleaseRunEntity.class, autoReleaseRunId);
		if(run.getRunStatus().getId() == VbsConstants.AUTO_RELEASE_RUN_STATUS_INIT_PROCESSING_CRITERIA) {
			Long count = autoReleaseDAO.getCountIncompleteEnrolmentsForRun(autoReleaseRunId);
			if(count == 0) {
				run.setRunStatus(genericDAO.findEntityById(AutoReleaseRunStatusEntity.class, VbsConstants.AUTO_RELEASE_RUN_STATUS_INIT_COMPLETE));
				genericDAO.updateEntity(run, VbsConstants.VBS_SYSTEM_USER_ID, DateUtil.getLocalDateTimeNow());
				sendEmail(autoReleaseRunId, run.getCreateUser(), true);
			}
		}
	}

	private void sendEmail(Long autoReleaseRunId, String userId, boolean first) {
		String releaseEmailSubject = String.format(propertyService.getValue(first ? VbsConstants.AUTO_RELEASE_FIRST_STEP_EMAIL_SUBJECT : VbsConstants.AUTO_RELEASE_FINAL_STEP_EMAIL_SUBJECT), autoReleaseRunId);
		String releaseEmailBody = String.format(propertyService.getValue(first ? VbsConstants.AUTO_RELEASE_FIRST_STEP_EMAIL : VbsConstants.AUTO_RELEASE_FINAL_STEP_EMAIL), autoReleaseRunId, autoReleaseRunId);
		try {
			UserDTO user = userService.getUser(userId);
			emailWSInvocationService.sendEmailUsingEsp(releaseEmailSubject, releaseEmailBody, VbsConstants.EMAIL_NO_REPLY_ADDRESS, user.getEmailAddress());
		} catch (Exception e) {
			LoggerUtil.logError(AutoReleaseServiceImpl.class, "sendEmail", e);
		}

	}

	@Override
	public List<AutoReleaseRunSearchResultDTO> searchAutoReleaseRuns(AutoReleaseRunSearchParametersDTO params) {
		List<Pair<AutoReleaseRunEntity, Long>> runs = autoReleaseDAO.searchAutoReleaseRuns(params);
		List<AutoReleaseRunSearchResultDTO> results = new ArrayList<>();
		for (Pair<AutoReleaseRunEntity, Long> run : runs) {
			AutoReleaseRunSearchResultDTO dto = AutoReleaseServiceTransformer.transformEntityToDTO(run.getLeft(), run.getRight());
			results.add(dto);
		}

		return results;
	}

	@Override
	public AutoReleaseResultWrapper getAutoReleaseResults(Long runId) {

		AutoReleaseRunEntity run = genericDAO.findEntityById(AutoReleaseRunEntity.class, runId);
		List<AutoReleaseResultDTO> enrolmentListSuccess = autoReleaseDAO.getAutoReleaseResultsForRunSuccess(runId);
		List<AutoReleaseResultDTO> enrolmentListEliminated = autoReleaseDAO.getAutoReleaseResultsForRunEliminated(runId);

		AutoReleaseResultWrapper resultWrapper = new AutoReleaseResultWrapper();
		resultWrapper.setAutoReleasePassedResults(enrolmentListSuccess);
		resultWrapper.setAutoReleaseFailedResults(enrolmentListEliminated);
		resultWrapper.setAutoReleaseRun(AutoReleaseServiceTransformer.transformEntityToDTO(run, run.getRunStatus().getId()));

		
		return resultWrapper;
	}

	@Override
	public void triggerFinalizeAutoRelease(Long autoReleaseRunId, List<AutoReleaseResultDTO> autoReleaseEnrolments, UserDTO userDto) throws VbsRuntimeException {

		AutoReleaseRunEntity run = genericDAO.findEntityById(AutoReleaseRunEntity.class, autoReleaseRunId);
		if(run.getRunStatus().getId() != VbsConstants.AUTO_RELEASE_RUN_STATUS_INIT_COMPLETE) {
			throw new VbsRuntimeException("Unable to trigger finalize auto release run, status is not INIT_COMPLETE");
		}

		saveAutoReleaseEnrolments(autoReleaseRunId, autoReleaseEnrolments, userDto);
		run.setRunStatus(genericDAO.findEntityById(AutoReleaseRunStatusEntity.class, VbsConstants.AUTO_RELEASE_RUN_STATUS_FINAL_PROCESSING));
		genericDAO.updateEntity(run, userDto.getUserId(), DateUtil.getLocalDateTimeNow());
		processorTriggeringService.triggerFinalizeAutoReleaseRun(autoReleaseRunId, userDto.getUserId());

	}

	@Override
	public void finalizeAutoRelease(Long autoReleaseRunId, String userId) {
		AutoReleaseRunEntity run = genericDAO.findEntityById(AutoReleaseRunEntity.class, autoReleaseRunId);
		if(run.getRunStatus().getId() != VbsConstants.AUTO_RELEASE_RUN_STATUS_FINAL_PROCESSING) {
			LoggerUtil.logError(AutoReleaseServiceImpl.class, "finalizeAutoRelease", "Unable to finalize auto release run " + autoReleaseRunId + ", run status is " + run.getRunStatus().getDescription());
			return;
		}

		List<AutoReleaseEnrolmentEntity> entities = autoReleaseDAO.getAutoReleaseEnrolmentsForRunSuccess(autoReleaseRunId);
		Map<Long, List<AutoReleaseEnrolmentEntity>> securityMap = new HashMap<>();
		for (AutoReleaseEnrolmentEntity enrolment: entities) {
			List<AutoReleaseEnrolmentEntity> enrolments = securityMap.computeIfAbsent(enrolment.getSecurity().getId(), k -> new ArrayList<>());
			enrolments.add(enrolment);
		}

		List<AutoReleaseCreateReleaseRequest> requests = new ArrayList<>();
		for (Map.Entry<Long, List<AutoReleaseEnrolmentEntity>> entry: securityMap.entrySet()) {
			calculateCurrentAmountAndTriggerCreateRelease(entry.getValue(), requests, userId);
		}

		if(requests.size() > 0) {
			for (AutoReleaseCreateReleaseRequest request: requests) {
				request.setNumberOfReleases(requests.size());
				this.processorTriggeringService.triggerCreateRelease(request);
			}
		} else {
			checkCreateReleasesComplete(autoReleaseRunId, 0, userId);
		}
	}

	private void calculateCurrentAmountAndTriggerCreateRelease(List<AutoReleaseEnrolmentEntity> autoReleaseEnrolmentList, List<AutoReleaseCreateReleaseRequest> requests, String userId){

		BigDecimal currentAmount = autoReleaseEnrolmentList.get(0).getSecurity().getAvailableAmount();
		for (AutoReleaseEnrolmentEntity autoReleaseEnrolment : autoReleaseEnrolmentList) {
			SecurityEntity securityEntity = autoReleaseEnrolment.getSecurity();
			if (autoReleaseEnrolment.getDeletedBy() == null && !securityHasActiveDemand(securityEntity)) {
				BigDecimal maxAmount = autoReleaseEnrolment.getTimingType().getTotalAmount();
				BigDecimal releaseAmount;
				boolean fullRelease = false;

				if (autoReleaseEnrolment.getTimingType().getId() == VbsConstants.AUTO_RELEASE_TIMING_TYPE_EXCESS_DEPOSIT) {
					if (currentAmount.compareTo(maxAmount) > 0) {
						releaseAmount = maxAmount;
					} else {
						fullRelease = true;
						releaseAmount = currentAmount;
					}
				} else {
					if (securityEntity.getPool().getUnitsToCover() <= 0) {
						LoggerUtil.logError(AutoReleaseServiceImpl.class, "createRelease", String.format("Unable to create release for enrolment %s, unitsToCover is less than 1", autoReleaseEnrolment.getEnrolment().getEnrolmentNumber()));
						releaseAmount = BigDecimal.ZERO;
					} else if (!autoReleaseCriteriaService.evaluateCriteria18(autoReleaseEnrolment)) {
						LoggerUtil.logInfo(AutoReleaseServiceImpl.class, String.format("Unable to create release for enrolment %s, failed rule 18", autoReleaseEnrolment.getEnrolment().getEnrolmentNumber()));
						releaseAmount = BigDecimal.ZERO;
					} else {
						BigDecimal originalAmount = securityEntity.getOriginalAmount();
						BigDecimal unitsToCover = new BigDecimal(securityEntity.getPool().getUnitsToCover());
						maxAmount = originalAmount.divide(unitsToCover, VbsConstants.ROUNDING_MODE);
						fullRelease = true;
						if (currentAmount.compareTo(maxAmount) > 0) {
							releaseAmount = maxAmount;
						} else {
							releaseAmount = currentAmount;
						}
					}

				}

				if (releaseAmount != null && releaseAmount.compareTo(BigDecimal.ZERO) > 0) {
					currentAmount = currentAmount.subtract(releaseAmount);

					AutoReleaseCreateReleaseRequest request = new AutoReleaseCreateReleaseRequest();
					request.setAutoReleaseEnrolmentId(autoReleaseEnrolment.getId());
					request.setFullRelease(fullRelease);
					request.setReleaseAmount(releaseAmount);
					request.setUserId(userId);
					requests.add(request);
				}
			}
		}
	}

	@Override
	public void processCreateRelease(Long autoReleaseEnrolmentId, boolean fullRelease, BigDecimal releaseAmount, String userId, int numberOfReleases) {
		AutoReleaseEnrolmentEntity enrolment = genericDAO.findEntityById(AutoReleaseEnrolmentEntity.class, autoReleaseEnrolmentId);
		ReleaseTypeEntity releaseType = lookupDAO.getReleaseTypeByReleaseTypeId(VbsConstants.RELEASE_TYPE_RELEASE);
		ReasonEntity reasonEntity = genericDAO.findEntityById(ReasonEntity.class, VbsConstants.AUTO_RELEASE_REASON);
		AutoReleaseEnrolmentStatusEntity releasedStatus = genericDAO.findEntityById(AutoReleaseEnrolmentStatusEntity.class, VbsConstants.AUTO_RELEASE_ENROLMENT_STATUS_RELEASED);

		ReleaseEntity release = createRelease(enrolment, releaseType, reasonEntity, releaseAmount, fullRelease, userId);
		enrolment.setRelease(release);
		enrolment.setStatus(releasedStatus);

		LocalDateTime now = DateUtil.getLocalDateTimeNow();
		enrolment = (AutoReleaseEnrolmentEntity) genericDAO.updateEntity(enrolment, userId, now);
		resetTimingFlags(enrolment, now);

		processorTriggeringService.triggerCheckCreateReleasesCompleted(enrolment.getAutoReleaseRun().getId(), numberOfReleases, userId);
	}

	@Override
	public void checkCreateReleasesComplete(Long runId, int numberOfReleases, String userId) {
		long releasesCreated = autoReleaseDAO.getCountReleasesInRun(runId);
		if(releasesCreated >= numberOfReleases) {
			AutoReleaseRunEntity run = genericDAO.findEntityById(AutoReleaseRunEntity.class, runId);
			run.setRunStatus(genericDAO.findEntityById(AutoReleaseRunStatusEntity.class, VbsConstants.AUTO_RELEASE_RUN_STATUS_FINAL_COMPLETE));
			genericDAO.updateEntity(run, userId, DateUtil.getLocalDateTimeNow());
			sendEmail(runId, userId, false);
		}
	}

	private boolean securityHasActiveDemand(SecurityEntity securityEntity){
		if(securityEntity.getSecurityType().getId() == VbsConstants.SECURITY_TYPE_CASH) {
			return releaseDAO.getReleaseDemandCollectorByStatusAndSecurityId(ReleaseStatus.PENDING, securityEntity.getId()) > 0;
		}
		else{
			return false;
		}
	}

	private void sendAutoReleaseLetters(ReleaseEntity release, String enrolmentNumber, boolean excessDeposit, boolean fullRelease, String userId) {

		CorrespondenceActionEnum vbLetter = null;
		CorrespondenceActionEnum bankLetter = null;
		if(release.getSecurity().getSecurityType().getId() == VbsConstants.SECURITY_TYPE_LETTER_OF_CREDIT) {
			if(!fullRelease) {
				bankLetter = CorrespondenceActionEnum.PARTIAL_LC_RELEASE_BANK;
				vbLetter = excessDeposit ? CorrespondenceActionEnum.EXCESS_LC_DEPOSIT_VB : CorrespondenceActionEnum.PARTIAL_LC_RELEASE_VB;
			} else {
				bankLetter = CorrespondenceActionEnum.FULL_LC_RELEASE_BANK;
				vbLetter = CorrespondenceActionEnum.FULL_LC_RELEASE_VB;
			}
		} else if (release.getSecurity().getSecurityType().getId() == VbsConstants.SECURITY_TYPE_CASH) {
			if(!fullRelease) {
				vbLetter = excessDeposit ? CorrespondenceActionEnum.EXCESS_CASH_DEPOSIT_VB : CorrespondenceActionEnum.PARTIAL_CASH_RELEASE_VB;
			} else {
				vbLetter = CorrespondenceActionEnum.FULL_CASH_RELEASE_VB;
			}
		}

		if(vbLetter != null) {
			correspondenceService.sendAutoReleaseCorrespondenceRequest(vbLetter, enrolmentNumber, release, userId);
		}
		if(bankLetter != null) {
			correspondenceService.sendAutoReleaseCorrespondenceRequest(bankLetter, enrolmentNumber, release, userId);
		}
	}

	private void resetTimingFlags(AutoReleaseEnrolmentEntity autoReleaseEnrolment, LocalDateTime updateDate) {
		EnrolmentEntity e = autoReleaseEnrolment.getEnrolment();
		int timingType = autoReleaseEnrolment.getTimingType().getId().intValue();
		boolean update = false;
		switch (timingType) {
			case (int)VbsConstants.AUTO_RELEASE_TIMING_TYPE_EXCESS_DEPOSIT:
				if(e.isExcessDepositRelease()) {
					e.setExcessDepositRelease(false);
					update =true;
				}
				break;
			case (int)VbsConstants.AUTO_RELEASE_TIMING_TYPE_ONE_YEAR:
				if(e.isAutoOneYear() || e.isAutoOneYearRfc() || e.isAutoTwoYear()) {
					e.setAutoOneYear(false);
					e.setAutoOneYearRfc(false);
					e.setAutoTwoYear(false);
					update = true;
				}
				break;
			case (int)VbsConstants.AUTO_RELEASE_TIMING_TYPE_RFC:
				if(e.isAutoOneYearRfc() || e.isAutoTwoYear()) {
					e.setAutoOneYearRfc(false);
					e.setAutoTwoYear(false);
					update = true;
				}
				break;
			case (int)VbsConstants.AUTO_RELEASE_TIMING_TYPE_TWO_YEAR:
				if(e.isAutoTwoYear()) {
					e.setAutoTwoYear(false);
					update = true;
				}
				break;
		}
		if(update) {
			genericDAO.updateEntity(e, VbsConstants.VBS_SYSTEM_USER_ID, updateDate);
		}
	}

	private ReleaseEntity createRelease(AutoReleaseEnrolmentEntity autoReleaseEnrolment, ReleaseTypeEntity releaseType, ReasonEntity reasonEntity, BigDecimal releaseAmount, boolean fullRelease, String userId) {

		SecurityEntity securityEntity = autoReleaseEnrolment.getSecurity();
		ReleaseEntity releaseEntity = new ReleaseEntity();
		if(securityEntity.getSecurityType().getId().equals(VbsConstants.SECURITY_TYPE_CASH)) {
			releaseEntity.setStatus(ReleaseStatus.PENDING);
		}else{
			releaseEntity.setStatus(ReleaseStatus.COMPLETED);
		}

		LocalDateTime time = LocalDateTime.now();
		boolean letterFullRelease = securityEntity.getAvailableAmount().subtract(releaseAmount).compareTo(BigDecimal.ZERO) == 0;

		long numberOfReleases = releaseDAO.getNumberOfReleases(securityEntity.getId());
		releaseEntity.setSequenceNumber(numberOfReleases + 1);
		releaseEntity.setFinalApprovalStatus(ReleaseApprovalStatusEnum.Authorized);
		releaseEntity.setFinalApprovalSentTo(userId);
		releaseEntity.setFinalApprovalBy(userId);
		releaseEntity.setFinalApprovalDate(time);
		releaseEntity.setRegularRelease(true);
		releaseEntity.setReleaseType(releaseType);
		releaseEntity.setSecurity(securityEntity);
		releaseEntity.setRequestAmount(releaseAmount);
		releaseEntity.setAuthorizedAmount(releaseAmount);

		releaseService.setReleaseInterestAmounts(releaseEntity, releaseAmount, userId, time);
		releaseEntity = (ReleaseEntity) genericDAO.addEntity(releaseEntity, userId, time);

		ReleaseReasonEntity releaseReasonEntity = new ReleaseReasonEntity();
		releaseReasonEntity.setReason(reasonEntity);
		releaseReasonEntity.setRelease(releaseEntity);
		genericDAO.addEntity(releaseReasonEntity, userId, time);

		//create releaseEnrolment
		ReleaseEnrolmentEntity releaseEnrolmentEntity = new ReleaseEnrolmentEntity();
		releaseEnrolmentEntity.setRequestedAmount(releaseAmount);
		releaseEnrolmentEntity.setAuthorizedAmount(releaseAmount);
		releaseEnrolmentEntity.setAnalystApprovalBy(userId);
		releaseEnrolmentEntity.setAnalystApprovalDate(time);
		releaseEnrolmentEntity.setAnalystApprovalStatus(ReleaseApprovalStatusEnum.Authorized);
		releaseEnrolmentEntity.setFullRelease(fullRelease);
		releaseEnrolmentEntity.setRelease(releaseEntity);
		releaseEnrolmentEntity.setEnrolment(autoReleaseEnrolment.getEnrolment());
		genericDAO.addEntity(releaseEnrolmentEntity, userId, time);

		sendAutoReleaseLetters(releaseEntity, autoReleaseEnrolment.getEnrolment().getEnrolmentNumber(),autoReleaseEnrolment.getTimingType().getId() == VbsConstants.AUTO_RELEASE_TIMING_TYPE_EXCESS_DEPOSIT, letterFullRelease, userId);

		if(securityEntity.getSecurityType().getId().equals(VbsConstants.SECURITY_TYPE_CASH)) {
			releaseService.sendReleaseApprovalWorklistToCrm(releaseEntity, userId);
		}

		return releaseEntity;
	}


	@Override
	public void saveAutoReleaseEnrolments(Long autoReleaseRunId, List<AutoReleaseResultDTO> autoReleaseEnrolments, UserDTO userDto) {
		List<AutoReleaseEnrolmentEntity> entities = autoReleaseDAO.getAutoReleaseEnrolmentsForRunSuccess(autoReleaseRunId);
		saveAutoReleaseEnrolments(autoReleaseEnrolments, entities, userDto);
	}

	private void saveAutoReleaseEnrolments(List<AutoReleaseResultDTO> autoReleaseEnrolments, List<AutoReleaseEnrolmentEntity> entities , UserDTO userDto) {
		List<AutoReleaseEnrolmentEntity> updates = new ArrayList<>();

		for (AutoReleaseResultDTO dto : autoReleaseEnrolments) {
			Long autoReleaseEnrolmentId = dto.getId();
			if (autoReleaseEnrolmentId != null) {
				Optional<AutoReleaseEnrolmentEntity> entity = entities.stream().filter(e -> e.getId().longValue() == dto.getId().longValue()).findFirst();
				if (entity.isPresent() && !StringUtils.equals(dto.getDeletedBy(), entity.get().getDeletedBy())) {
					entity.get().setDeletedBy(dto.getDeletedBy());
					updates.add(entity.get());
				}
			}
		}

		if(!updates.isEmpty()) {
			genericDAO.updateEntityList(updates, userDto.getUserId(), DateUtil.getLocalDateTimeNow());
		}
	}

	@Override
	public List<AutoReleaseDTO> getAutoReleaseForSecurityId(Long securityId) {
		return this.autoReleaseDAO.getAutoReleaseForSecurityId(securityId);
	}
}
