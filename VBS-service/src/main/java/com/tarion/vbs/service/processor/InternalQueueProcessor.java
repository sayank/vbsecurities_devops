/* 
 * 
 * InternalQueueProcessor.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.processor;

import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.service.jaxb.autorelease.*;
import com.tarion.vbs.service.jaxb.email.allocation.AllocationEmailRequest;
import com.tarion.vbs.service.jaxb.email.ceBlanket.CeBlanketEmailRequest;
import com.tarion.vbs.service.jaxb.interest.CreateInterestCalculation;

import javax.ejb.Local;


/**
 * Processing messages on the internal JMS queue
 * 
 */
@Local
public interface InternalQueueProcessor {

    void process(AutoReleaseCheckCriteriaCompleteRequest request);

    void process(AutoReleaseTriggerFinalizeRequest request);

    void process(AutoReleaseCreateReleaseRequest request);

    void process(AutoReleaseCheckCreateReleasesCompleteRequest request);

    void process(CreateInterestCalculation request);

    void process(AllocationEmailRequest request) throws VbsCheckedException;

    void process(CeBlanketEmailRequest request) throws VbsCheckedException;

    void processReceivedXml(String xmlResponse);

    void process(AutoReleaseCheckDataReceivedRequest request);

    void process(AutoReleaseEvaluateCriteriaRequest request);

	void process(AutoReleaseVBApplicationStatusRequest request);


}
