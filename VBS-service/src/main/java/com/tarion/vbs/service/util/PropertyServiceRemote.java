/* 
 * 
 * PropertyServiceRemote.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import javax.ejb.Remote;

/**
 * Session Bean to Access the Properties File table.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-01-03
 * @version 1.0
 */
@Remote
public interface PropertyServiceRemote {

	/**
	 * Given a key, return the value in the database.
	 *
	 * @param key
	 *            the key
	 * @return The value, or Null if not found
	 */
	public String getValue(String key);

	/**
	 * Method to get value of property key with list of values to format the
	 * string.
	 *
	 * @param key
	 *            the key
	 * @param values
	 *            The values to substitute into the string
	 * @return The property, null if not found
	 */
	public String getValue(String key, String[] values);

	/**
	 * Returns true if and only if the value is "true" in the properties table.
	 *
	 * @param key
	 *            the key
	 * @return True if and only if the value is "true" in the properties table
	 */
	public boolean getBooleanValue(String key);

	public void startup();

}
