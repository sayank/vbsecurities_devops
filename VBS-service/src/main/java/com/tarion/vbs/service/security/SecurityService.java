/* 
 * 
 * SecurityService.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.security;

import javax.ejb.Local;

import com.tarion.vbs.common.dto.enrolment.EnrolmentPoolActivityDTO;
import com.tarion.vbs.common.dto.security.AmountsDTO;
import com.tarion.vbs.common.dto.security.SecuritySearchResultWrapper;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.service.jaxb.email.ceBlanket.CeBlanketEmailRequest;

import java.util.List;

/**
 * Security Service provides operations for Securities tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-15
 * @version 1.0
 */
@Local
public interface SecurityService extends SecurityServiceRemote {

    AmountsDTO getSecurityAmountsForVB(String vbNumber);
	AmountsDTO getSecurityAmountsForEnrolment(String enrolmentNumber);
	AmountsDTO getBlanketSecurityAmountsForVB(String vbNumber); 
	SecuritySearchResultWrapper getSecuritiesByInstrumentNumber(String instrumentNumber);
	SecuritySearchResultWrapper getSecuritiesByVbNumber(String vbNumber);
	SecuritySearchResultWrapper getSecuritiesByEnrolment(String enrolmentNumber);
	void sendCEBlanketEmail(CeBlanketEmailRequest request) throws VbsCheckedException;
	void sendDtaVbApplicationAnalystEmail(Long securityId) throws VbsCheckedException;
}
