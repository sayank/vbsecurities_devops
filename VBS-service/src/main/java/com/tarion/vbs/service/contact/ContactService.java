package com.tarion.vbs.service.contact;

import com.tarion.vbs.common.contact.EscrowAgentLawyerAssistantDTO;
import com.tarion.vbs.common.dto.contact.ContactDTO;

import javax.ejb.Local;

/**
 * @author <A href="shadi.kajevand@tarion.com">Shadi Kajevand</A>
 * @since 2018-01-11
 * @version 1.0
 */

@Local
public interface ContactService extends ContactServiceRemote {

    EscrowAgentLawyerAssistantDTO getEscrowAgentLawyerAssistantDTOForSecurityId(Long securityId);
    ContactDTO getThirdPartyContactForSecurityId(Long securityId);
}
