/* 
 * 
 * InterestServiceRemote.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.interest;

import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.dto.interest.InterestRateDTO;
import com.tarion.vbs.common.dto.interest.InterestSearchResultDTO;
import com.tarion.vbs.common.dto.interest.SecurityInterestSearchParamsDTO;
import com.tarion.vbs.common.dto.security.CashSecurityNoInterestDTO;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;

import javax.ejb.Remote;
import javax.interceptor.ExcludeClassInterceptors;
import java.time.LocalDate;
import java.util.List;


/**
 * Interest Rate Service provides operations for Interest Rate tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-15
 * @version 1.0
 */
@Remote
public interface InterestServiceRemote {
	List<CashSecurityNoInterestDTO> getCashSecuritiesWithoutInterest();

	List<InterestRateDTO> saveInterestRate(InterestRateDTO interestRateDTO, UserDTO userDTO);

	List<ErrorDTO> validateInterestRate(InterestRateDTO dto);

	void deleteInterestRate(InterestRateDTO dto, UserDTO userDTO) throws VbsCheckedException;

	List<InterestRateDTO> getAllInterestRates();

	LocalDate getNextValidStartDate();

	void findDatesRequiringInterestCalculation(int precedingDays);

	void performInterestCalcForDate(LocalDate date);

	List<com.tarion.vbs.common.dto.interest.SecurityInterestDTO> getDailyInterestsForSecurity(Long securityId);

	List<InterestSearchResultDTO> searchInterest(SecurityInterestSearchParamsDTO securityInterestSearchParams);
/*
	public List<CashSecurityNoInterestDTO> getCashSecuritiesWithoutInterest();

	public List<ErrorDTO> validateInterestRate(InterestRateDTO interestRateDTO);
	List<InterestRateDTO> saveInterestRate(InterestRateDTO interestRateDTO, UserDTO userDTO) throws VbsCheckedException;

    void deleteInterestRate(InterestRateDTO dto, UserDTO userDTO) throws VbsCheckedException;

    List<InterestRateDTO> getAllInterestRates();

	LocalDateTime getNextValidStartDate();

	public int calculateDailyInterestBatch(LocalDateTime dayToCalculateInterest, InterestRateEntity interestRate, int batchSize) throws VbsCheckedException;
	public void sendDailyInterestToFms(LocalDateTime dayToSendInterest) throws VbsCheckedException;

	public InterestRateDTO getApplicableInterestRateForDate(LocalDateTime interestDate) throws VbsCheckedException;

	public List<SecurityInterestDTO> getDailyInterests(Long securityId);
	public SecurityInterestDTO findLastCalculatedDailyInterestForSecurity(Long securityId);
	public LocalDateTime findLastCalculatedDailyInterest();
	public LocalDateTime findEarliestCalculatedInterestDayToSend();
	
	List<InterestSearchResultDTO> searchInterest(SecurityInterestSearchParamsDTO securityInterestSearchParams);

	public void calculateDailyInterestForSecurity(LocalDateTime dayToCalculateInterest, InterestRateEntity interestRate, SecurityEntity security) throws VbsCheckedException;
*/
}
