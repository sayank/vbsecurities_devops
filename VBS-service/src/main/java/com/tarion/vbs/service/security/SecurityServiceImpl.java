/* 
 * 
 * SecurityServiceImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.security;

import com.tarion.vbs.common.constants.PermissionConstants;
import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.dto.alert.AlertDTO;
import com.tarion.vbs.common.dto.contact.ContactDTO;
import com.tarion.vbs.common.dto.enrolment.CEEnrolmentDecisionDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentPoolActivityDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentWarrantyServiceFieldsDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionAmountsDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionMaaPoaInstitutionDTO;
import com.tarion.vbs.common.dto.security.*;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.enums.*;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.exceptions.VbsDirectoryEnvironmentException;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.exceptions.VbsValidationException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.ValidationUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.alert.AlertDAO;
import com.tarion.vbs.dao.contact.ContactDAO;
import com.tarion.vbs.dao.financialinstitution.FinancialInstitutionDAO;
import com.tarion.vbs.dao.history.HistoryDAO;
import com.tarion.vbs.dao.release.ReleaseDAO;
import com.tarion.vbs.dao.security.SecurityDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.dao.util.LookupDAO;
import com.tarion.vbs.enrolment.EnrolmentDAO;
import com.tarion.vbs.orm.entity.alert.AlertEntity;
import com.tarion.vbs.orm.entity.contact.ContactEntity;
import com.tarion.vbs.orm.entity.contact.ContactSecurityEntity;
import com.tarion.vbs.orm.entity.enrolment.CEEnrolmentDecisionEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentPoolEntity;
import com.tarion.vbs.orm.entity.financialinstitution.BranchEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionMaaPoaEntity;
import com.tarion.vbs.orm.entity.release.ReleaseEnrolmentEntity;
import com.tarion.vbs.orm.entity.security.*;
import com.tarion.vbs.service.alert.AlertServiceTransformer;
import com.tarion.vbs.service.contact.ContactService;
import com.tarion.vbs.service.contact.ContactServiceTransformer;
import com.tarion.vbs.service.enrolment.EnrolmentServiceTransformer;
import com.tarion.vbs.service.financialinstitution.FinancialInstitutionService;
import com.tarion.vbs.service.financialinstitution.FinancialInstitutionServiceTransformer;
import com.tarion.vbs.service.jaxb.email.ceBlanket.CeBlanketEmailRequest;
import com.tarion.vbs.service.jaxb.fms.createcashsecurity.CreateCashSecurityRequest;
import com.tarion.vbs.service.jaxb.fms.createdtarequest.CreateDTARequest;
import com.tarion.vbs.service.processor.ProcessorTriggeringService;
import com.tarion.vbs.service.userprofile.AdSecurityService;
import com.tarion.vbs.service.userprofile.UserService;
import com.tarion.vbs.service.util.*;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;
import java.util.stream.Collectors;



/**
 * Security Service provides operations for Security and Vb
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-09
 * @version 1.0
 */
@Stateless(mappedName = "ejb/SecurityService")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class SecurityServiceImpl implements SecurityService, SecurityServiceRemote {

	@EJB
	private GenericDAO genericDAO;
	@EJB
	private EnrolmentDAO enrolmentDAO;
	@EJB
	private FinancialInstitutionService financialInstitutionService;
	@EJB
	private FinancialInstitutionDAO financialInstitutionDAO;
	@EJB
	private ContactDAO contactDAO;
	@EJB
	private SecurityDAO securityDAO;
	@EJB
	private LookupDAO lookupDAO;
	@EJB
	private AlertDAO alertDAO;
	@EJB
	private ReleaseDAO releaseDAO;
	@EJB
	private UserService userService;
	@EJB
	private ContactService contactService;
	@EJB
	private HistoryDAO historyDAO;
	@EJB
	private JmsMessageSenderService jmsMessageSenderService;
	@EJB
	private ProcessorTriggeringService processorTriggeringService;
	@EJB
	private EmailWSInvocationService emailWSInvocationService;
	@EJB
	private PropertyService propertyService;
	@EJB
	private VBAWSInvocationService vbaWS;
	@EJB
	private AdSecurityService adSecurityService;

	private static final String ESCROW_AGENT_FIELD = "escrowAgentLawyerAssistant.escrowAgent";
	private static final String RECEIVED_DATE = "receivedDate";
	private static final String INSTRUMENT_NUMBER = "instrumentNumber";
	private static final String POOL_ID = "poolId";

	@Override
	public SecuritySearchResultWrapper searchSecurity(SecuritySearchParametersDTO searchParametersDTO, UserDTO userDTO) {
		this.validateSecuritySearch(searchParametersDTO);
		return this.searchSecurity(searchParametersDTO, !userDTO.hasPermission(PermissionConstants.P_CREATE_DEMAND_FINANCE));
	}

	@Override
	public SecurityDTO getSecurity(Long securityId) {
		if (securityId == null) {
			return null;
		}
		SecurityEntity entity = genericDAO.findEntityById(SecurityEntity.class, securityId);
		return this.transform(entity);
	}

	@Override
	public Long saveSecurity(SecurityDTO securityDTO, List<String> displayedAlertsConfirmations, UserDTO userDTO) {
		SecurityEntity currentEntity = (securityDTO.getId() != null) ? genericDAO.findEntityById(SecurityEntity.class, securityDTO.getId()) : new SecurityEntity();
		this.validateSecurity(securityDTO, currentEntity, displayedAlertsConfirmations);
		LocalDateTime time = LocalDateTime.now();
		SecurityEntity entity = this.createEntity(securityDTO, currentEntity, userDTO, time);
		this.saveAlerts(securityDTO, entity, userDTO, time);

		this.saveEnrolmentAutoReleaseIndicators(securityDTO, userDTO, time);
		if(!this.isSecurityFullyReleased(entity) || currentEntity.getWriteOffReason() != null){
			this.saveEnrolments(securityDTO, currentEntity, entity, userDTO, time);
		}
		if (!this.isSecurityFullyReleased(entity)) {
			this.saveVbList(securityDTO, entity, userDTO, time);
			this.saveThirdParty(securityDTO, entity, userDTO, time);
			if(isAnySecurityType(entity, Arrays.asList(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT, VbsConstants.SECURITY_TYPE_PRE_EXISTING_ELEMENTS_FUND))){
				this.saveEscrowAgent(securityDTO, entity, userDTO, time);
			}
			if(entity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) == 0){
				this.saveMonthlyReport(securityDTO, entity, userDTO, time);
			}
			if(currentEntity.getId() == null){
				this.sendJmsMessageToFms(entity, userDTO);
			}
		}
		return entity.getId();
	}

	@Override
	public List<ErrorDTO> validateDeleteSecurity(Long securityId) {
		SecurityEntity security = genericDAO.findEntityById(SecurityEntity.class, securityId);
		List<ErrorDTO> ret = new ArrayList<>();

		if (security.getPool() != null && security.getPool().getId() != null) {
			List<EnrolmentPoolEntity> enrolments = enrolmentDAO.getEnrolmentsByPoolId(security.getPool().getId());
			if (enrolments != null && !enrolments.isEmpty()) {
				ret.add(new ErrorDTO("Linked enrolments", "enrolments", ErrorTypeEnum.ALERT_FAIL, "Linked enrolments", "You cannot delete security with linked enrolments", "SEC_LINKED_ENROLMENTS"));
			}
		}

		return ret;
	}

	@Override
	public void deleteSecurity(Long securityId, UserDTO userDTO) {
		List<ErrorDTO> errors = validateDeleteSecurity(securityId);

		if (errors.isEmpty()) {
			SecurityEntity security = genericDAO.findEntityById(SecurityEntity.class, securityId);
			if (!security.getSecurityType().getId().equals(VbsConstants.SECURITY_TYPE_CASH)
					|| (security.getSecurityType().getId().equals(VbsConstants.SECURITY_TYPE_CASH)
					&& security.isWriteOff())) {
				security.setDeleted(true);
				security.setCurrentAmount(BigDecimal.ZERO);
				genericDAO.updateEntity(security, userDTO.getUserId(), DateUtil.getLocalDateTimeNow());
			}
		} else {
			throw new VbsRuntimeException("Errors found when deleting: " + errors.toString());
		}
	}


	@Override
	public List<ErrorDTO> poolSecurities(PoolSecuritiesParametersDTO poolSecuritiesParametersDTO, UserDTO userDTO) {
		Long poolId = poolSecuritiesParametersDTO.getPoolId();
		List<ErrorDTO> errors = new ArrayList<>();
		PoolEntity pool = genericDAO.findEntityById(PoolEntity.class, poolId);
		if (pool == null) {
			errors.add(new ErrorDTO("Pool with Id " + poolId + " Does Not Exists."));
		}
		else {
			List<VbPoolEntity> vbsForThePool = contactDAO.getVbsForPoolId(poolId);
			SecurityPurposeEntity securityPurpose = lookupDAO.getSecurityPurposeById(poolSecuritiesParametersDTO.getSecurityPurposeId());
			for (Long securityId : poolSecuritiesParametersDTO.getSecurityIds()) {
				if (releaseDAO.getNumberOfPendingApprovalReleaseByStatuses(securityId, ReleaseStatus.INITIATED) > 0) {
					errors.add(new ErrorDTO("There are releases pending approval for Security Number: " + securityId));
					break;
				}
			}
			if(errors.isEmpty()) {
				List<SecurityEntity> securityEntities = new ArrayList();
				for (Long securityId : poolSecuritiesParametersDTO.getSecurityIds()) {
					SecurityEntity security = genericDAO.findEntityById(SecurityEntity.class, securityId);
					securityEntities.add(security);
					SecurityDTO securityDTO = this.transform(security);
					if (!vbsListsMatch(vbsForThePool, securityDTO)) {
						errors.add(new ErrorDTO("VB(s) for Security Id " + securityId + " Do Not Match VB(s) for the Pool Id " + poolId + "."));
						break;
					}
				}
				if(errors.isEmpty()) {
					for (SecurityEntity security : securityEntities) {
						SecurityDTO securityDTO = this.transform(security);
						securityDTO.setVbList(contactDAO.getVbsForPoolId(securityDTO.getPoolId())
								.stream().map((VbPoolEntity entity) -> ContactServiceTransformer.transformEntityToDTO(entity.getVb())).collect(Collectors.toList()));    // not initialized by trasformer
							security.setPool(pool);
							security.setSecurityPurpose(securityPurpose);
							security.setIdentityType(lookupDAO.getIdentityTypeById(VbsConstants.IDENTITY_TYPE_FREEHOLD));

							String oldComment = security.getComment();
							if (oldComment == null) {
								oldComment = "";
							}
							String comment = "This Security Instrument has been merged into Pool ID " + poolSecuritiesParametersDTO.getPoolId() + ".\n";
							security.setComment(comment + oldComment);
							genericDAO.updateEntity(security, userDTO.getUserId(), DateUtil.getLocalDateTimeNow());
					}
				}
			}
		}
		if(errors.isEmpty()){
		    List<SecurityEntity> securityList = securityDAO.getSecurityByPoolId(poolSecuritiesParametersDTO.getPoolId());
		    for(SecurityEntity security: securityList) {
				if (releaseDAO.getNumberOfPendingApprovalReleaseByStatuses(security.getId(), ReleaseStatus.INITIATED) > 0) {
					errors.add(new ErrorDTO("There are releases pending approval for Security Number: " + security.getId()));
					break;
				}
			}
			if(errors.isEmpty() && pool != null) {
				for(SecurityEntity security: securityList){
					if(security.getIdentityType().getId().compareTo(VbsConstants.IDENTITY_TYPE_FREEHOLD) == 0) {
						security.setIdentityType(lookupDAO.getIdentityTypeById(VbsConstants.IDENTITY_TYPE_FREEHOLD));
					}
					if(security.getSecurityPurpose().getId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) == 0) {
						security.setSecurityPurpose(lookupDAO.getSecurityPurposeById(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY));
					}
					genericDAO.updateEntity(security, userDTO.getUserId(), LocalDateTime.now());
				}
				pool.setUnitsToCover(poolSecuritiesParametersDTO.getUnitsIntendedToCover().intValue());
				pool.setFHUnitsToCover(poolSecuritiesParametersDTO.getUnitsIntendedToCover().intValue());
				List<EnrolmentPoolEntity> enrolmentPoolEntityList = enrolmentDAO.getEnrolmentsByPoolId(poolId);
				for(EnrolmentPoolEntity enrolmentPoolEntity: enrolmentPoolEntityList) {
					genericDAO.removeEntity(enrolmentPoolEntity, userDTO.getUserId(), LocalDateTime.now());
				}
				genericDAO.updateEntity(pool, userDTO.getUserId(), LocalDateTime.now());
			}
		}
		return errors;
	}

	private boolean canDeleteEnrolments(Long poolId, List<EnrolmentPoolEntity> enrolmentPoolList){
		List<SecurityEntity> securities = securityDAO.getSecurityByPoolId(poolId);
		for (SecurityEntity security: securities) {
			List<ReleaseEnrolmentEntity> releaseEnrolments = releaseDAO.getReleasesBySecurityId(security.getId());
			for (EnrolmentPoolEntity enrolment : enrolmentPoolList) {
				if (!canDeleteEnrolment(enrolment.getEnrolment().getEnrolmentNumber(), releaseEnrolments)) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public AmountsDTO getSecurityAmountsForVB(String vbNumber) {
		SecuritySearchParametersDTO params = new SecuritySearchParametersDTO();
		params.setVbNumberType(SearchTypeEnum.EQUALS);
		params.setVbNumber(vbNumber);
		SecuritySearchResultWrapper wrapper = searchSecurity(params, false);

		
		AmountsDTO result = getAmountsDTO(wrapper, true);
		return result;
	}
	
	@Override
	public AmountsDTO getBlanketSecurityAmountsForVB(String vbNumber) {
		SecuritySearchParametersDTO params = new SecuritySearchParametersDTO();
		params.setVbNumberType(SearchTypeEnum.EQUALS);
		params.setVbNumber(vbNumber);
		params.setSecurityPurposeId(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY);
		SecuritySearchResultWrapper wrapper = searchSecurity(params, true);
		
		AmountsDTO result = getAmountsDTO(wrapper, false);
		return result;
	}


	@Override
	public AmountsDTO getSecurityAmountsForEnrolment(String enrolmentNumber) {
		SecuritySearchParametersDTO params = new SecuritySearchParametersDTO();
		params.setEnrolmentNumber(enrolmentNumber);
		SecuritySearchResultWrapper wrapper = searchSecurity(params, true);

		AmountsDTO result = getAmountsDTO(wrapper, false);

		return result;
	}
	
	private AmountsDTO getAmountsDTO(SecuritySearchResultWrapper wrapper, boolean includeInterest) {
		AmountsDTO result = new AmountsDTO();
		result.setTotalCashAmount(wrapper.getTotals().getTotalAvailableCashAmount());
		if(includeInterest) {
			result.setTotalSecurityAmount(wrapper.getTotals().getTotalCurrentAmount().add(wrapper.getTotals().getTotalCurrentInterestAmount()));
		} else {
			result.setTotalSecurityAmount(wrapper.getTotals().getTotalCurrentAmount());
		}
		result.setTotalOriginalAmount(wrapper.getTotals().getTotalOriginalAmount());

		if (wrapper.getSecurities().isEmpty()) {
			return result;
		}

		String type = wrapper.getSecurities().get(0).getSecurityTypeName();
		for (SecuritySearchResultDTO dto : wrapper.getSecurities()) {
			if (dto.getSecurityTypeName() != null && !dto.getSecurityTypeName().equals(type)) {
				type = "Multi";
			}
		}
		result.setSecurityTypeDescription(type);

		return result;
	}
	
	@Override
	public List<HistoryFieldDTO> getSecurityCommentHistory(Long securityId) {
		return historyDAO.getSecurityCommentHistory(securityId);
	}

	@Override
	public List<EnrolmentPoolActivityDTO> getDeletedEnrolmentsActivities(Long securityId) {
		return historyDAO.deletedEnrolmentsActivities(securityId);
	}

	@Override
	public List<EnrolmentPoolActivityDTO> getDeletedBbActivities(Long poolId) {
		return historyDAO.deletedVbActivities(poolId);
	}

	@Override
	public List<String> getAllInstrumentNumbersLike(String instrumentNumber) {
		return securityDAO.getAllInstrumentNumbersLike(instrumentNumber);
	}

	@Override
	public List<SecurityDTO> getAllSecuritiesByInstrumentNumbersLike(String instrumentNumber) {
		List<SecurityEntity> securityEntityList = securityDAO.getAllSecuritiesByInstrumentNumbersLike(instrumentNumber);
		return securityEntityList.stream().map(this::transform).collect(Collectors.toList());
	}

	@Override
	public SecuritySearchResultWrapper getSecuritiesByInstrumentNumber(String instrumentNumber) {
		SecuritySearchParametersDTO params = new SecuritySearchParametersDTO();
		params.setInstrumentNumber(instrumentNumber);
		params.setInstrumentNumberSearchType(SearchTypeEnum.EQUALS);
		SecuritySearchResultWrapper wrapper = searchSecurity(params, false);
		return wrapper;
	}

	@Override
	public SecuritySearchResultWrapper getSecuritiesByVbNumber(String vbNumber) {
		SecuritySearchParametersDTO params = new SecuritySearchParametersDTO();
		params.setVbNumberType(SearchTypeEnum.EQUALS);
		params.setVbNumber(vbNumber);
		SecuritySearchResultWrapper wrapper = searchSecurity(params, false);
		return wrapper;
	}

	@Override
	public SecuritySearchResultWrapper getSecuritiesByEnrolment(String enrolmentNumber) {
		SecuritySearchParametersDTO params = new SecuritySearchParametersDTO();
		params.setEnrolmentNumber(enrolmentNumber);
		SecuritySearchResultWrapper wrapper = searchSecurity(params, false);
		return wrapper;
	}

	@Override
	public void validateAlert(AlertDTO alertDTO, List<ErrorDTO> errors, int index){
		List<ErrorDTO> ret = new ArrayList<>();
		if(alertDTO.getAlertTypeId() == null){
			ret.add(new ErrorDTO("An Alert Type must be chosen.", ((errors != null) ? "alerts."+index+ "." : "") + "alertTypeId"));
		}
		if(alertDTO.getStartDate() == null){
			ret.add(new ErrorDTO("A Start Date must be entered.", ((errors != null) ? "alerts."+index+ "." : "") + "startDate"));
		}else if(alertDTO.getEndDate() != null && alertDTO.getEndDate().compareTo(alertDTO.getStartDate()) < 1){
			ret.add(new ErrorDTO("The End Date cannot be the same or before the Start Date.", ((errors != null) ? "alerts."+index+ "." : "") + "endDate"));
		}

		if(!ret.isEmpty()){
			if(errors != null){
				errors.addAll(ret);
			}else {
				throw new VbsValidationException(ret);
			}
		}
	}

	private SecuritySearchResultWrapper searchSecurity(SecuritySearchParametersDTO securitySearchParametersDTO, boolean displayAvailableAmount) {
		this.validateSecuritySearch(securitySearchParametersDTO);
		SecuritySearchResultWrapper ret = securityDAO.searchSecurity(securitySearchParametersDTO, displayAvailableAmount);

		if(VbsUtil.onlyFinancialInstitutionSearch(securitySearchParametersDTO) && VbsConstants.FINANCIAL_INSTITUTION_TYPE_SURETY_COMPANIES == financialInstitutionDAO.getFinancialInstitutionTypeId(securitySearchParametersDTO.getFinancialInstitutionId())) {
			FinancialInstitutionAmountsDTO amounts = financialInstitutionService.getAmounts(securitySearchParametersDTO.getFinancialInstitutionId(), true);
			FinancialInstitutionAmountsDTO amountsExclude = financialInstitutionService.getAmounts(securitySearchParametersDTO.getFinancialInstitutionId(), false);
			ret.setTotals(new SecuritySearchResultTotalsDTO());
			ret.getTotals().addTotalAvailableCashAmount(amounts.getTotalCurrentAmount());
			ret.getTotals().addTotalCurrentAmount(amounts.getTotalCurrentAmount());
			ret.getTotals().addTotalOriginalAmount(amounts.getTotalOriginalAmount());
			ret.getTotals().addTotalCurrentAmountExcludingDtaPef(amountsExclude.getTotalCurrentAmount());
			ret.getTotals().addTotalOriginalAmountExcludingDtaPef(amountsExclude.getTotalOriginalAmount());
		}

		else if (!ret.getSecurities().isEmpty()) {
			List<SecuritySearchResultDTO> securities = ret.getSecurities().stream().map(this::totalAmounts)
					.collect(Collectors.toList());
			securities.forEach(s -> {

				ret.getTotals().addTotalCurrentAmount(s.getCurrentAmount());
				ret.getTotals().addTotalOriginalAmount(s.getOriginalAmount());
				if (s.getSecurityTypeName().equalsIgnoreCase("Cash")) {
					ret.getTotals().addTotalAvailableCashAmount(s.getTotalAvailableAmount());
					ret.getTotals().addTotalCashAmount(s.getCurrentAmount());
					ret.getTotals().addTotalCurrentInterestAmount(s.getCurrentInterestAmount());
				}

				if(!s.getSecurityTypeName().equalsIgnoreCase("Deposit Trust Agreement") && !s.getSecurityTypeName().equalsIgnoreCase("Pre-Existing Elements Fund")){
					ret.getTotals().addTotalOriginalAmountExcludingDtaPef(s.getOriginalAmount());
					ret.getTotals().addTotalCurrentAmountExcludingDtaPef(s.getCurrentAmount());
				}
			});
			ret.setSecurities(securities);
		}


		return ret;
	}

	private SecuritySearchResultDTO totalAmounts(SecuritySearchResultDTO dto) {
		if (dto.getCurrentInterestAmount() != null) {
			dto.setCurrentInterestAmount(dto.getCurrentInterestAmount().setScale(VbsConstants.DECIMAL_SCALE, VbsConstants.ROUNDING_MODE));
			dto.setTotalAvailableAmount(dto.getCurrentAmount().add(dto.getCurrentInterestAmount()));
		}
		return dto;
	}

	private SecurityEntity createEntity(SecurityDTO securityDTO, SecurityEntity currentEntity, UserDTO userDTO, LocalDateTime time) {
		SecurityEntity entity = new SecurityEntity();

		boolean hasCompletedRelease = (currentEntity.getId() != null) && releaseDAO.securityHasCompletedPrincipalReleases(currentEntity.getId());
		boolean fullyReleased = this.isSecurityFullyReleased(currentEntity);

		if (!fullyReleased) {
			int unitsToCover = 0;
			if((securityDTO.getSecurityPurposeId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) != 0 && securityDTO.getUnitsToCover() != null) 
			|| (securityDTO.getSecurityPurposeId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) == 0 && securityDTO.getIdentityTypeId() == null && securityDTO.getUnitsToCover() != null)) {
				unitsToCover = securityDTO.getUnitsToCover();
				securityDTO.setFhUnitsToCover(null);
				securityDTO.setCeUnitsToCover(null);
			} else if(securityDTO.getSecurityPurposeId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) == 0 && securityDTO.getIdentityTypeId() != null && securityDTO.getIdentityTypeId().compareTo(VbsConstants.IDENTITY_TYPE_FREEHOLD) == 0) {
				unitsToCover = securityDTO.getFhUnitsToCover();
				securityDTO.setCeUnitsToCover(null);
			} else if(securityDTO.getSecurityPurposeId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) == 0 && securityDTO.getIdentityTypeId() != null && securityDTO.getIdentityTypeId().compareTo(VbsConstants.IDENTITY_TYPE_CONDO) == 0) {
				unitsToCover = securityDTO.getCeUnitsToCover();
				securityDTO.setFhUnitsToCover(null);
			} else if(securityDTO.getSecurityPurposeId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) == 0 && securityDTO.getIdentityTypeId() != null && securityDTO.getIdentityTypeId().compareTo(VbsConstants.IDENTITY_TYPE_BOTH) == 0) {
				unitsToCover = securityDTO.getFhUnitsToCover() + securityDTO.getCeUnitsToCover();
			}
			if((currentEntity.getIdentityType() == null && securityDTO.getIdentityTypeId() != null)
					|| (currentEntity.getIdentityType() != null && securityDTO.getIdentityTypeId() == null)
					|| (securityDTO.getIdentityTypeId() != null && currentEntity.getIdentityType() != null && securityDTO.getIdentityTypeId().compareTo(currentEntity.getIdentityType().getId()) != 0)){
				for(SecurityEntity securityEntity: securityDAO.getSecurityByPoolId(securityDTO.getPoolId())){
					boolean updateEntity = false;
					if((securityDTO.getId() == null || securityEntity.getId().compareTo(securityDTO.getId()) != 0)) {
						securityEntity.setIdentityType(lookupDAO.getIdentityTypeById(securityDTO.getIdentityTypeId()));
						updateEntity = true;
					}
					if((securityDTO.getId() == null || securityEntity.getId().compareTo(securityDTO.getId()) != 0) && securityEntity.getSecurityPurpose().getId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) != 0) {
						securityEntity.setSecurityPurpose(lookupDAO.getSecurityPurposeById(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY));
						updateEntity = true;
					}
					if(updateEntity) {
						genericDAO.updateEntity(securityEntity, userDTO.getUserId(), LocalDateTime.now());
					}
				}
			}
			if (currentEntity.getId() != null) {
				if (securityDTO.getPoolId() == null) {
					throw new VbsRuntimeException("Cannot create a fresh pool for existing security");
				}
				entity.setPool((currentEntity.getPool().getId().compareTo(securityDTO.getPoolId()) == 0)
						? currentEntity.getPool() : genericDAO.findEntityById(PoolEntity.class, securityDTO.getPoolId()));
				
				if(currentEntity.getPool().getId().compareTo(securityDTO.getPoolId()) == 0) {
					entity.getPool().setUnitsToCover(unitsToCover);
					entity.getPool().setFHUnitsToCover(securityDTO.getFhUnitsToCover());
					entity.getPool().setCEUnitsToCover(securityDTO.getCeUnitsToCover());
				}
			} else {
				PoolEntity newPoolEntity = new PoolEntity(unitsToCover);
				newPoolEntity.setFHUnitsToCover(securityDTO.getFhUnitsToCover());
				newPoolEntity.setCEUnitsToCover(securityDTO.getCeUnitsToCover());
				entity.setPool((PoolEntity) genericDAO.addEntity(newPoolEntity, userDTO.getUserId().toUpperCase(), time));
			}

			entity.setSecurityType((currentEntity.getId() == null || currentEntity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_CASH) != 0)
					? lookupDAO.getSecurityTypeById(securityDTO.getSecurityTypeId()) : currentEntity.getSecurityType());
			entity.setSecurityPurpose(lookupDAO.getSecurityPurposeById(securityDTO.getSecurityPurposeId()));
			if(securityDTO.getSecurityTypeId().equals(VbsConstants.SECURITY_TYPE_CASH)) {
				entity.setInstrumentNumber(securityDTO.getInstrumentNumber().toUpperCase());
			}else{
				entity.setInstrumentNumber(securityDTO.getInstrumentNumber());
			}

			if (currentEntity.getId() == null || (currentEntity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_CASH) != 0)) {
				entity.setCurrentAmount(securityDTO.getCurrentAmount());
				entity.setOriginalAmount(securityDTO.getOriginalAmount());
			} else {
				entity.setOriginalAmount(currentEntity.getOriginalAmount());
				entity.setCurrentAmount(currentEntity.getCurrentAmount());
			}

			if (currentEntity.getId() == null || (currentEntity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_CASH) != 0 && !hasCompletedRelease)) {
				entity.setIssuedDate(securityDTO.getIssuedDate());
				entity.setReceivedDate(securityDTO.getReceivedDate());
			} else {
				entity.setIssuedDate(currentEntity.getIssuedDate());
				entity.setReceivedDate(currentEntity.getReceivedDate());
			}
			if (entity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_CASH) == 0) {
				entity.setThirdParty(securityDTO.isThirdParty());
			} else {
				Long branchId = (securityDTO.getFinancialInstitutionAndBranch() != null && securityDTO.getFinancialInstitutionAndBranch().getBranch() != null)
						? securityDTO.getFinancialInstitutionAndBranch().getBranch().getId() : null;
				if (branchId == null && !isAnySecurityType(entity, Arrays.asList(VbsConstants.SECURITY_TYPE_CASH, VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT))) {
					throw new VbsRuntimeException("Non DTA/Cash Securities must have an FI Branch.");
				}
				if (entity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) == 0) {
					entity.setStatus(lookupDAO.getSecurityStatusById(securityDTO.getSecurityStatusId()));
					entity.setPpsaNumber(securityDTO.getPpsaNumber());
					entity.setPpsaFileNumber(securityDTO.getPpsaFileNumber());
					entity.setPpsaExpiryDate(securityDTO.getPpsaExpiryDate());
					entity.setDtaAccountNumber(securityDTO.getDtaAccountNumber());

				} else if (entity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_SURETY_BOND) == 0) {
					entity.setJointBond(securityDTO.isJointBond());
					entity.setJointAut(securityDTO.isJointAut());
					entity.setReleaseFirst(securityDTO.getReleaseFirst());
					entity.setDemandFirst(securityDTO.getDemandFirst());
					if (entity.getReleaseFirst() == YesNoEnum.YES || entity.getDemandFirst()== YesNoEnum.YES) {
						List<SecurityEntity> securitiesInPool = securityDAO.getSecurityByPoolId(securityDTO.getPoolId());
						if (entity.getReleaseFirst()== YesNoEnum.YES) {
							if (this.checkReleaseFirstExistsInPool(securitiesInPool, securityDTO)) {
								updateSecurityReleaseFirstInPool(securitiesInPool, userDTO, time);
							}
						}
						if (entity.getDemandFirst()== YesNoEnum.YES) {
							if (this.checkDemandFirstExistsInPool(securitiesInPool, securityDTO)) {
								updateSecurityDemandFirstInPool(securitiesInPool, userDTO, time);
							}
						}
					}
				}else if (entity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_PRE_EXISTING_ELEMENTS_FUND) == 0) {
					entity.setPefAccountNumber(securityDTO.getPefAccountNumber());
				}
				entity.setBranch((branchId != null) ? ((currentEntity.getBranch() != null && currentEntity.getBranch().getId().compareTo(branchId) == 0)
						? currentEntity.getBranch() : genericDAO.findEntityById(BranchEntity.class, branchId)) : null);
				entity.setFinancialInstitutionMaaPoa((securityDTO.getFinancialInstitutionMaaPoaDTO() != null && securityDTO.getFinancialInstitutionMaaPoaDTO().getId() != null)
						? genericDAO.findEntityById(FinancialInstitutionMaaPoaEntity.class, securityDTO.getFinancialInstitutionMaaPoaDTO().getId()) : null);
			}

			if (entity.getSecurityPurpose().getId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) == 0) {
				entity.setIdentityType(lookupDAO.getIdentityTypeById(securityDTO.getIdentityTypeId()));
			}

			Optional<ContactDTO> opt = securityDTO.getVbList().stream().filter(ContactDTO::isPrimaryVb).findFirst();
			if (opt.isPresent()){
				ContactDTO primaryVb = opt.get();
				entity.setPrimaryVb((currentEntity.getId() == null || currentEntity.getPrimaryVb().getId().compareTo(primaryVb.getId()) != 0)
					? genericDAO.findEntityById(ContactEntity.class, primaryVb.getId()) : currentEntity.getPrimaryVb());
			}else {
				throw new VbsRuntimeException("Security MUST have a Primary VB");
			}
			if(securityDTO.getId() != null && securityDTO.getSecurityTypeId().equals(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) && currentEntity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) == 0
					&& securityDTO.getSecurityStatusId() != null
					&& securityDTO.getSecurityStatusId().equals(VbsConstants.SECURITY_STATUS_ACCEPTED)
					&& (currentEntity.getStatus() == null || !currentEntity.getStatus().getId().equals(VbsConstants.SECURITY_STATUS_ACCEPTED))
			){
				processorTriggeringService.triggerAllocationRequestEmail(securityDTO.getId());
			}
			entity.setComment(securityDTO.getComment());

			//non UI-updatable
			entity.setId(currentEntity.getId());
			entity.setVersion(securityDTO.getVersion());
			entity.setUnpostReason(currentEntity.getUnpostReason());
			entity.setUnpostUser(currentEntity.getUnpostUser());
			entity.setUnpostDate(currentEntity.getUnpostDate());
			entity.setWriteOff(currentEntity.isWriteOff());
			entity.setWriteOffReason(currentEntity.getWriteOffReason());
			entity.setWriteOffDate(currentEntity.getWriteOffDate());
			entity.setAllocated(currentEntity.isAllocated());
			entity.setDeleted(currentEntity.isDeleted());
			entity.setCreateUser(currentEntity.getCreateUser());
			entity.setCreateDate(currentEntity.getCreateDate());

			if (entity.getId() == null) {
				entity = (SecurityEntity) genericDAO.addEntity(entity, userDTO.getUserId().toUpperCase(), time);
			} else {
				entity.setUpdateUser(currentEntity.getUpdateUser());
				entity.setUpdateDate(currentEntity.getUpdateDate());
				genericDAO.detach(currentEntity);
				if(!entity.equals(currentEntity)){
					try {
						entity = (SecurityEntity) genericDAO.updateEntity(entity, userDTO.getUserId().toUpperCase(), time);
					}catch(Exception e){
						List<ErrorDTO> errors = new ArrayList<>();
						errors.add(new ErrorDTO("This security has been saved by another User, data on this page is outdated. Please click OK and search this security again to get the latest version of data.", "oldVersion",
								ErrorTypeEnum.ALERT_FAIL, "Message",
								"This security has been saved by another User, data on this page is outdated. Please click OK and search this security again to get the latest version of data.",
								"OLD_VERSION"));
						throw new VbsValidationException(errors);
					}
				}
			}
		} else if(!Objects.equals(securityDTO.getComment(), currentEntity.getComment()) || (securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) == 0 && currentEntity.getStatus() != null && securityDTO.getSecurityStatusId() != null && currentEntity.getStatus().getId().compareTo(securityDTO.getSecurityStatusId()) != 0)){
			currentEntity.setComment(securityDTO.getComment());
			if (securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) == 0) {
				currentEntity.setStatus(lookupDAO.getSecurityStatusById(securityDTO.getSecurityStatusId()));
			}
			try {
				entity = (SecurityEntity) genericDAO.updateEntity(currentEntity, userDTO.getUserId().toUpperCase(), time);
			}catch(Exception e){
				List<ErrorDTO> errors = new ArrayList<>();
				errors.add(new ErrorDTO("This security has been saved by another User, data on this page is outdated. Please click OK and search this security again to get the latest version of data.", "oldVersion",
						ErrorTypeEnum.ALERT_FAIL, "Message",
						"This security has been saved by another User, data on this page is outdated. Please click OK and search this security again to get the latest version of data.",
						"OLD_VERSION"));
				throw new VbsValidationException(errors);
			}

		} else {
			entity = currentEntity;
		}

		return entity;
	}
	@Override
	public void sendDtaVbApplicationAnalystEmail(Long securityId) throws VbsCheckedException {
		SecurityEntity security = genericDAO.findEntityById(SecurityEntity.class, securityId);
		ContactEntity vb = security.getPrimaryVb();
		if (vb != null) {
			String analystUserId = vbaWS.getVbApplicationAnalyst(vb.getCrmContactId());
			if(!StringUtils.isEmpty(analystUserId)) {
				UserDTO user = adSecurityService.getUser(analystUserId);
				if(user != null && !StringUtils.isEmpty(user.getEmailAddress())) {
					String emailSubject = String.format(propertyService.getValue(VbsConstants.DTA_SUBJECT), security.getInstrumentNumber());
					String emailBody = String.format(propertyService.getValue(VbsConstants.DTA_EMAIL), vb.getCrmContactId(), vb.getCompanyName());
					emailWSInvocationService.sendEmailUsingEsp(emailSubject, emailBody, VbsConstants.EMAIL_NO_REPLY_ADDRESS, user.getEmailAddress());
				}
			}
		}
	}
	private void updateSecurityReleaseFirstInPool(List<SecurityEntity> securitiesInPool, UserDTO userDTO, LocalDateTime time){
		for (SecurityEntity entity : securitiesInPool) {
			entity.setReleaseFirst(YesNoEnum.NO);
			genericDAO.updateEntity(entity, userDTO.getUserId().toLowerCase(), time);
		}
	}

	private void updateSecurityDemandFirstInPool(List<SecurityEntity> securitiesInPool, UserDTO userDTO, LocalDateTime time){
		for (SecurityEntity entity : securitiesInPool) {
			entity.setDemandFirst(YesNoEnum.NO);
			genericDAO.updateEntity(entity, userDTO.getUserId().toLowerCase(), time);
		}
	}
	private boolean isSecurityFullyReleased(SecurityEntity security) {
		if (security.getId() != null) {
			if (security.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) == 0) {
				return (security.getStatus() != null && (security.getStatus().getId().equals(VbsConstants.SECURITY_STATUS_WITHDRAWN)
						|| security.getStatus().getId().equals(VbsConstants.SECURITY_STATUS_CANCELLED)
						|| security.getStatus().getId().equals(VbsConstants.SECURITY_STATUS_REPLACED)
						|| security.getStatus().getId().equals(VbsConstants.SECURITY_STATUS_TERMINATED)));
			} else if (security.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_PRE_EXISTING_ELEMENTS_FUND) != 0) {
				return security.getAvailableAmount() != null && BigDecimal.ZERO.compareTo(security.getAvailableAmount()) == 0;
			}
		}
		return false;
	}

	private SecurityDTO transform(SecurityEntity entity) {
		SecurityDTO dto = new SecurityDTO();

		dto.setId(entity.getId());
		dto.setVersion(entity.getVersion());
		dto.setPoolId(entity.getPool().getId());
		dto.setUnitsToCover(entity.getPool().getUnitsToCover());
		dto.setFhUnitsToCover(entity.getPool().getFHUnitsToCover());
		dto.setCeUnitsToCover(entity.getPool().getCEUnitsToCover());
		dto.setSecurityStatusId(entity.getStatus() != null ? entity.getStatus().getId() : null);
		dto.setSecurityTypeId(entity.getSecurityType().getId());
		dto.setSecurityPurposeId(entity.getSecurityPurpose().getId());
		dto.setIdentityTypeId((entity.getIdentityType() != null) ? entity.getIdentityType().getId() : null);
		if(entity.getSecurityType().getId().equals(VbsConstants.SECURITY_TYPE_CASH)) {
			dto.setInstrumentNumber(entity.getInstrumentNumber().toUpperCase());
		}else{
			dto.setInstrumentNumber(entity.getInstrumentNumber());
		}
		dto.setDeleted(entity.isDeleted());
		dto.setOriginalAmount(entity.getOriginalAmount());
		dto.setCurrentAmount(entity.getCurrentAmount());
		dto.setCurrentInterest(entity.getCurrentInterest());
		dto.setIssuedDate(entity.getIssuedDate());
		dto.setReceivedDate(entity.getReceivedDate());
		dto.setPpsaNumber(entity.getPpsaNumber());
		dto.setPpsaExpiryDate(entity.getPpsaExpiryDate());
		dto.setComment(entity.getComment());
		dto.setThirdParty(entity.isThirdParty());
		dto.setDtaAccountNumber(entity.getDtaAccountNumber());
		dto.setPpsaFileNumber(entity.getPpsaFileNumber());
		dto.setAvailableAmount(entity.getAvailableAmount());
		dto.setAvailableInterest(entity.getAvailableInterest());
		dto.setCreateDate(entity.getCreateDate());
		dto.setCreateUser(entity.getCreateUser());
		dto.setUpdateDate(entity.getUpdateDate());
		dto.setUpdateUser(entity.getUpdateUser());
		dto.setAllocated(entity.isAllocated());
		dto.setUnpostReason(entity.getUnpostReason());
		dto.setWriteOff(entity.isWriteOff());
		dto.setWriteOffReason(entity.getWriteOffReason());
		dto.setPefAccountNumber(entity.getPefAccountNumber());

		dto.setAlerts(alertDAO.getAlertsBySecurityId(entity.getId()).stream()
				.map(a -> AlertServiceTransformer.transformEntityToDTO(a, entity.getId()))
				.collect(Collectors.toList()));
		dto.setEnrolments(this.getEnrolmentList(entity));
		dto.setVbList(this.getVbsForSecurityEntity(entity));
		dto.setPrimaryVb(dto.getVbList().stream().filter(ContactDTO::isPrimaryVb).findFirst().orElse(null));
		dto.setFullyReleased(this.isSecurityFullyReleased(entity));

		if (entity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_SURETY_BOND) == 0) {
			dto.setJointBond(entity.isJointBond());
			dto.setJointAut(entity.isJointAut());

			if(entity.isJointBond()) {
				dto.setReleaseFirst(entity.getReleaseFirst());
				dto.setDemandFirst(entity.getDemandFirst());
				dto.setLinkedToMultipleCE(enrolmentDAO.poolIsLinkedToMultipleCE(entity.getPool().getId()));
				dto.setJointBondSecurities(securityDAO.getSecurityByPoolId(entity.getPool().getId()).
						stream().filter(s -> s.getId().compareTo(entity.getId()) != 0).
						map(this::getJointBondDisplay).collect(Collectors.toList()));
				dto.setJointBondOriginalAmount(
						dto.getJointBondSecurities().stream().map(SecurityDTO::getOriginalAmount).reduce(entity.getOriginalAmount(), BigDecimal::add));
				dto.setJointBondCurrentAmount(
						dto.getJointBondSecurities().stream().map(SecurityDTO::getAvailableAmount).reduce(entity.getAvailableAmount(), BigDecimal::add));
			}
		} else if (entity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_CASH) == 0) {
			if (entity.isThirdParty()) {
				dto.setThirdPartyContact(contactService.getThirdPartyContactForSecurityId(entity.getId()));
			}
			if (entity.getInstrumentNumber() != null) {
				dto.setInterestPeriodStartWarning(entity.getReceivedDate().isBefore(LocalDateTime.of(2004, 11, 1, 0, 0)));
			}
		} else if (entity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) == 0 ||
				entity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_PRE_EXISTING_ELEMENTS_FUND) == 0) {

			dto.setEscrowAgentLawyerAssistant(contactService.getEscrowAgentLawyerAssistantDTOForSecurityId(entity.getId()));
		}

		if (entity.getBranch() != null) {
			dto.getFinancialInstitutionAndBranch().setFinancialInstitution(FinancialInstitutionServiceTransformer.transformEntityToDTO(entity.getBranch().getFinancialInstitution()));
			dto.getFinancialInstitutionAndBranch().setBranch(FinancialInstitutionServiceTransformer.transformEntityToDTO(entity.getBranch()));
			if (entity.getFinancialInstitutionMaaPoa() != null) {
				dto.setFinancialInstitutionMaaPoaDTO(financialInstitutionService.getMaaPoa(entity.getFinancialInstitutionMaaPoa().getId(), entity.getOriginalAmount(), entity.getCurrentAmount()));
			}
		}
		return dto;
	}

	private SecurityDTO getJointBondDisplay(SecurityEntity entity) {
		SecurityDTO dto = new SecurityDTO();
		dto.setId(entity.getId());
		dto.setInstrumentNumber(entity.getInstrumentNumber());
		dto.setReleaseFirst(entity.getReleaseFirst());
		dto.setDemandFirst(entity.getDemandFirst());
		dto.setLinkedToMultipleCE(enrolmentDAO.poolIsLinkedToMultipleCE(entity.getPool().getId()));
		dto.setOriginalAmount(entity.getOriginalAmount());
		dto.setCurrentAmount(entity.getCurrentAmount());
		dto.setAvailableAmount(entity.getAvailableAmount());
		dto.setIssuedDate(entity.getIssuedDate());
		dto.setReceivedDate(entity.getReceivedDate());
		return dto;
	}

	private void saveAlerts(SecurityDTO securityDTO, SecurityEntity entity, UserDTO user, LocalDateTime time) {
		List<AlertEntity> alerts = alertDAO.getAlertsBySecurityId(entity.getId());
		alerts.forEach(a -> {
			Optional<AlertDTO> opt = securityDTO.getAlerts().stream().filter(b -> b.getId() != null && b.getId().compareTo(a.getId()) == 0).findFirst();
			if (opt.isPresent()) {
				AlertDTO dto = opt.get();
				AlertDTO current = AlertServiceTransformer.transformEntityToDTO(a, entity.getId());
				if (!current.equals(dto)) {
					a.setAlertType(lookupDAO.getAlertTypeById(dto.getAlertTypeId()));
					a.setSecurity(entity);
					a.setDescription(dto.getDescription());
					a.setName(dto.getName());
					a.setStartDate(dto.getStartDate());
					a.setEndDate(dto.getEndDate());
					genericDAO.updateEntity(a, user.getUserId().toUpperCase(), time);
				}
			} else {
				genericDAO.removeEntity(a, user.getUserId().toUpperCase(), time);
			}
		});

		securityDTO.getAlerts().stream().filter(d -> d.getId() == null).forEach(d -> {
			AlertEntity e = new AlertEntity();
			e.setStartDate(d.getStartDate());
			e.setEndDate(d.getEndDate());
			e.setName(d.getName());
			e.setDescription(d.getDescription());
			e.setSecurity(entity);
			e.setAlertType(lookupDAO.getAlertTypeById(d.getAlertTypeId()));
			genericDAO.addEntity(e, user.getUserId().toUpperCase(), time);
		});
	}

	private void saveEnrolmentAutoReleaseIndicators (SecurityDTO securityDTO, UserDTO userDTO, LocalDateTime time) {
		List<EnrolmentEntity> enrolments = enrolmentDAO.getEnrolmentsByEnrolmentNumber(securityDTO.getEnrolments().stream().map(EnrolmentDTO::getEnrolmentNumber).collect(Collectors.toList()));
		enrolments.forEach(e -> {
			Optional<EnrolmentDTO> opt = securityDTO.getEnrolments().stream().filter(b -> b.getEnrolmentNumber() != null && b.getEnrolmentNumber().equalsIgnoreCase(e.getEnrolmentNumber())).findFirst();
			if (opt.isPresent()) {
				this.updateEnrolmentIndicators(opt.get(), e, userDTO, time);
			}
		});
	}

	private void saveEnrolments(SecurityDTO securityDTO, SecurityEntity currentEntity, SecurityEntity entity, UserDTO userDTO, LocalDateTime time) {
		if (currentEntity.getPool() == null || currentEntity.getPool().getId().compareTo(entity.getPool().getId()) == 0) {
			List<EnrolmentPoolEntity> enrolments = enrolmentDAO.getEnrolmentsByPoolId(entity.getPool().getId());
			enrolments.forEach(e -> {
				Optional<EnrolmentDTO> opt = securityDTO.getEnrolments().stream().filter(b -> b.getEnrolmentNumber() != null && b.getEnrolmentNumber().equalsIgnoreCase(e.getEnrolment().getEnrolmentNumber())).findFirst();
				if (opt.isPresent()) {
					EnrolmentDTO dto = opt.get();
					EnrolmentPoolEntity ep = new EnrolmentPoolEntity();
					if (entity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_PRE_EXISTING_ELEMENTS_FUND) == 0) {
						ep.setClearanceDate(dto.getClearanceDate());
						ep.setClearanceLetterIssued(dto.getClearanceLetterIssued());
						ep.setPefAmount(dto.getPefAmount());
					}
					if (e.getEnrolment().getHomeCategory().getId().compareTo(VbsConstants.HOME_CATEGORY_FREEHOLD) == 0) {
						ep.setInventoryAmount(dto.getInventoryAmount());
						ep.setExcessAmount(dto.getExcessAmount());
					}

					ep.setCurrentAllocatedAmount(dto.getCurrentAllocatedAmount());
					ep.setAllocatedAmount(dto.getAllocatedAmount());
					ep.setRaAmount(dto.getRaAmount());
					ep.setEnrolment(e.getEnrolment());
					ep.setMaxUnitDeposit(dto.getMaxUnitDeposit());
					ep.setPefAmount(dto.getPefAmount());
					ep.setExcessAmount(dto.getExcessAmount());
					ep.setInventoryAmount(dto.getInventoryAmount());
					ep.setPool(e.getPool());
					ep.setId(e.getId());
					ep.setVersion(e.getVersion());
					ep.setCreateDate(e.getCreateDate());
					ep.setCreateUser(e.getCreateUser());
					ep.setUpdateDate(e.getUpdateDate());
					ep.setUpdateUser(e.getUpdateUser());

					if (!ep.equals(e)) {
						genericDAO.updateEntity(ep, userDTO.getUserId().toUpperCase(), time);
					}
				} else {
					genericDAO.removeEntity(e, userDTO.getUserId().toUpperCase(), time);
				}
			});


			securityDTO.getEnrolments().stream().filter(e -> e.getEnrolmentNumber() != null && enrolments.stream().noneMatch(ee -> ee.getEnrolment().getEnrolmentNumber().equalsIgnoreCase(e.getEnrolmentNumber()))).
					forEach(d -> {
						EnrolmentPoolEntity ep = new EnrolmentPoolEntity();
						EnrolmentServiceTransformer.transformDTOToEntity(ep, d);
						ep.setPool(entity.getPool());
						ep.setEnrolment(genericDAO.findEntityById(EnrolmentEntity.class, d.getEnrolmentNumber()));
                        this.updateEnrolmentIndicators(d, ep.getEnrolment(), userDTO, time);
						genericDAO.addEntity(ep, userDTO.getUserId().toUpperCase(), time);
					});
		}
	}

	private void updateEnrolmentIndicators(EnrolmentDTO dto, EnrolmentEntity entity, UserDTO userDTO, LocalDateTime time){
        if(dto.getExcessDeposit() != entity.isExcessDeposit() ||
                dto.isExcessDepositRelease() != entity.isExcessDepositRelease() ||
                dto.isAutoOneYear() != entity.isAutoOneYear() ||
                dto.isAutoOneYearRfc() != entity.isAutoOneYearRfc() ||
                dto.isAutoTwoYear() != entity.isAutoTwoYear()){
            entity.setExcessDeposit(dto.getExcessDeposit());
            entity.setExcessDepositRelease(dto.getExcessDeposit());
            entity.setAutoOneYear(dto.isAutoOneYear());
            entity.setAutoOneYearRfc(dto.isAutoOneYearRfc());
            entity.setAutoTwoYear(dto.isAutoTwoYear());
            genericDAO.updateEntity(entity, userDTO.getUserId().toUpperCase(), time);
        }
    }

	private void saveVbList(SecurityDTO securityDTO, SecurityEntity entity, UserDTO userDTO, LocalDateTime time) {
		List<VbPoolEntity> currentVbSecurities = contactDAO.getAllVbsEvenDeletedForPoolId(entity.getPool().getId());
		currentVbSecurities.forEach(vb -> {
			Optional<ContactDTO> opt = securityDTO.getVbList().stream().filter(b -> b.getId().compareTo(vb.getVb().getId()) == 0).findFirst();
			if (opt.isPresent()) {
				ContactDTO dto = opt.get();
				if (((vb.getVbDeleteReason() == null) != (dto.getDeleteReason() == null)) ||
						(dto.getDeleteReason() != null && dto.getDeleteReason().compareTo(vb.getVbDeleteReason().getId()) != 0)) {
					vb.setVbDeleteReason((dto.getDeleteReason() != null) ? lookupDAO.getVbDeleteReasonById(dto.getDeleteReason()) : null);
					if(dto.getDeleteReason() != null){
						List<EnrolmentPoolEntity> enrolmentPool =  enrolmentDAO.getEnrolmentPoolByPoolIdAndVbNumber(securityDTO.getPoolId(), dto.getCrmContactId());
						// Fix for https://jira.tarion.com/browse/VBS2-92
						// business wants to remove VB no matter the status of enrolments
//						if(this.canDeleteEnrolments(securityDTO.getPoolId(), enrolmentPool)){
							for(EnrolmentPoolEntity enrolmentPoolEntity: enrolmentPool){
								genericDAO.removeEntity(enrolmentPoolEntity, userDTO.getUserId(), LocalDateTime.now());
							}
//						}else{
//							List<ErrorDTO> errors = new ArrayList<>();
//							errors.add(new ErrorDTO("VB: " + dto.getCrmContactId() + " can not be deleted, it has released enrolments.", "vbList",
//									ErrorTypeEnum.ALERT_FAIL, "Message",
//									"VB: " + dto.getCrmContactId() + " can not be deleted, it has released enrolments.",
//									"ENROLMENT_HAS_BEEN_RELEASED"));
//									throw new VbsValidationException(errors);
//						}
					}
					genericDAO.updateEntity(vb, userDTO.getUserId().toUpperCase(), time);
				}
			}
		});

		securityDTO.getVbList().stream().filter(d -> currentVbSecurities.stream().noneMatch(c -> c.getVb().getId().compareTo(d.getId()) == 0)).forEach(dto -> {
			VbPoolEntity vbp = new VbPoolEntity();
			vbp.setVbDeleteReason((dto.getDeleteReason() != null) ? lookupDAO.getVbDeleteReasonById(dto.getDeleteReason()) : null);
			vbp.setVb(genericDAO.findEntityById(ContactEntity.class, dto.getId()));
			vbp.setPool(entity.getPool());
			genericDAO.addEntity(vbp, userDTO.getUserId().toUpperCase(), time);
		});
	}

	private void saveThirdParty(SecurityDTO securityDTO, SecurityEntity entity, UserDTO userDTO, LocalDateTime time) {
		ContactSecurityEntity thirdPartyEntity = contactDAO.getContactSecurityBySecurityIdContactSecurityRoleId(securityDTO.getId(), VbsConstants.CONTACT_SECURITY_THIRD_PARTY);
		if ( ((thirdPartyEntity == null) != (securityDTO.getThirdPartyContact() == null || securityDTO.getThirdPartyContact().getCrmContactId() == null)) ||
				(thirdPartyEntity != null && securityDTO.getThirdPartyContact().getCrmContactId() != null && thirdPartyEntity.getContact().getCrmContactId().compareTo(securityDTO.getThirdPartyContact().getCrmContactId()) != 0) ) {
			if (securityDTO.getThirdPartyContact() == null && thirdPartyEntity != null) {
				genericDAO.removeEntity(thirdPartyEntity, userDTO.getUserId().toUpperCase(), time);
			} else {

				ContactEntity newThirdParty = contactDAO.getContactByCrmContactId(securityDTO.getThirdPartyContact().getCrmContactId());
				if (thirdPartyEntity == null) {
					thirdPartyEntity = new ContactSecurityEntity();
				}
				if (newThirdParty == null) {
					ContactDTO dto = contactService.createContactFromDTO(securityDTO.getThirdPartyContact(), userDTO.getUserId().toUpperCase());
					thirdPartyEntity.setContact(genericDAO.findEntityById(ContactEntity.class, dto.getId()));
				} else {
					thirdPartyEntity.setContact(newThirdParty);
				}
				thirdPartyEntity.setContactSecurityRole(lookupDAO.getContactSecurityRoleById(VbsConstants.CONTACT_SECURITY_THIRD_PARTY));
				thirdPartyEntity.setSecurity(entity);
				if (thirdPartyEntity.getId() == null) {
					genericDAO.addEntity(thirdPartyEntity, userDTO.getUserId().toUpperCase(), time);
				} else {
					genericDAO.updateEntity(thirdPartyEntity, userDTO.getUserId().toUpperCase(), time);
				}
			}
		}
	}

	private void saveEscrowAgent(SecurityDTO securityDTO, SecurityEntity entity, UserDTO userDTO, LocalDateTime time) {
		if (securityDTO.getEscrowAgentLawyerAssistant() == null || securityDTO.getEscrowAgentLawyerAssistant().getEscrowAgent() == null) {
			throw new VbsRuntimeException("DTA/PEF must have an Escrow Agent");
		}
		this.updateOrRemoveContactSecurity(entity, securityDTO.getEscrowAgentLawyerAssistant().getEscrowAgent(), VbsConstants.CONTACT_SECURITY_ESCROW, userDTO, time);
		this.updateOrRemoveContactSecurity(entity, securityDTO.getEscrowAgentLawyerAssistant().getLawyer(), VbsConstants.CONTACT_SECURITY_LAWYER, userDTO, time);
		this.updateOrRemoveContactSecurity(entity, securityDTO.getEscrowAgentLawyerAssistant().getAssistant(), VbsConstants.CONTACT_SECURITY_ASSISTANT, userDTO, time);
	}

	private void updateOrRemoveContactSecurity(SecurityEntity securityEntity, ContactDTO contactDTO, Long contactSecurityRoleId, UserDTO userDTO, LocalDateTime time){
		ContactSecurityEntity contactSecurityEntity = contactDAO.getContactSecurityBySecurityIdContactSecurityRoleId(securityEntity.getId(), contactSecurityRoleId);
		if ( ((contactSecurityEntity == null) != (contactDTO == null || contactDTO.getId() == null)) || (contactSecurityEntity != null && contactSecurityEntity.getContact().getId().compareTo(contactDTO.getId()) != 0)) {
			if (contactSecurityEntity != null && (contactDTO == null || contactDTO.getId() == null) && contactSecurityRoleId.compareTo(VbsConstants.CONTACT_SECURITY_ESCROW) != 0) {
				genericDAO.removeEntity(contactSecurityEntity, userDTO.getUserId().toUpperCase(), time);
			} else if(contactDTO != null && contactDTO.getId() != null) {
				this.saveContactSecurity(contactSecurityEntity, securityEntity, contactDTO.getId(), contactSecurityRoleId, userDTO, time);
			}
		}
	}

	private void saveContactSecurity(ContactSecurityEntity contactSecurityEntity, SecurityEntity securityEntity, Long contactId, Long contactSecurityRoleId, UserDTO userDTO, LocalDateTime time) {
		if (contactSecurityEntity == null) {
			contactSecurityEntity = new ContactSecurityEntity();
			contactSecurityEntity.setContactSecurityRole(lookupDAO.getContactSecurityRoleById(contactSecurityRoleId));
		}
		contactSecurityEntity.setContact(genericDAO.findEntityById(ContactEntity.class, contactId));
		contactSecurityEntity.setSecurity(securityEntity);
		if (contactSecurityEntity.getId() == null) {
			genericDAO.addEntity(contactSecurityEntity, userDTO.getUserId().toUpperCase(), time);
		} else {
			genericDAO.updateEntity(contactSecurityEntity, userDTO.getUserId().toUpperCase(), time);
		}
	}

	private void saveMonthlyReport(SecurityDTO securityDTO, SecurityEntity entity, UserDTO userDTO, LocalDateTime time){
		if(securityDTO.getMonthlyDate() != null) {
			MonthlyReportEntity monthlyReportExistingEntity = securityDAO.getMonthlyReportForYearMonthBySecurity(entity.getId().intValue(), securityDTO.getMonthlyDate().getYear(), securityDTO.getMonthlyDate().getMonthValue());
			if(monthlyReportExistingEntity != null) {
				monthlyReportExistingEntity.setSecurityDeposit(lookupDAO.getSecurityDepositById(securityDTO.getSecurityDeposit()));
				genericDAO.updateEntity(monthlyReportExistingEntity, userDTO.getUserId().toUpperCase(), time);
			} else {
				MonthlyReportEntity monthlyReportEntity = new MonthlyReportEntity();
				monthlyReportEntity.setSecurityEntity(entity.getId().intValue());
				monthlyReportEntity.setSecurityDeposit(lookupDAO.getSecurityDepositById(securityDTO.getSecurityDeposit()));
				monthlyReportEntity.setReportYear(securityDTO.getMonthlyDate().getYear());
				monthlyReportEntity.setReportMonth(securityDTO.getMonthlyDate().getMonthValue());
				genericDAO.addEntity(monthlyReportEntity, userDTO.getUserId().toUpperCase(), time);
			}
		}
	}

	private boolean monthlyReportExistForMonth(int securityID, LocalDateTime time){
		return securityDAO.monthlyReportExistForYearMonthBySecurity(securityID, time.getYear(), time.getMonthValue());
	}

	private boolean isAnySecurityType(SecurityEntity securityEntity, List<Long> types){
		return types.stream().anyMatch(t -> securityEntity.getSecurityType().getId().compareTo(t) == 0);
	}

	private boolean vbsListsMatch(List<VbPoolEntity> newVbList, SecurityDTO securityDTO) {
		//TODO change this to falsy as it'll be quicker and easier.
		if (securityDTO.getVbList() == null || securityDTO.getVbList().isEmpty()) {
			return false;
		}
		boolean vbsMatch = newVbList.stream().allMatch(newVb -> securityDTO.getVbList().stream().anyMatch(oldVb -> oldVb.getCrmContactId().equals(newVb.getVb().getCrmContactId())));
		vbsMatch = vbsMatch && securityDTO.getVbList().stream().allMatch(oldVb -> newVbList.stream().anyMatch(newVb -> oldVb.getCrmContactId().equals(newVb.getVb().getCrmContactId())));
		return vbsMatch;
	}

	private List<EnrolmentDTO> getEnrolmentList(SecurityEntity securityEntity) {
		List<EnrolmentPoolEntity> enrolments = enrolmentDAO.getEnrolmentsByPoolId(securityEntity.getPool().getId());
		if(enrolments.isEmpty()) { return new ArrayList<>(); }

        List <ReleaseEnrolmentEntity> releaseEnrolments =  releaseDAO.getReleasesBySecurityId(securityEntity.getId());
		List<String> enrolmentNumbers = new ArrayList<>();
		List<EnrolmentDTO> enrolmentDTOs = new ArrayList<>();
		for (EnrolmentPoolEntity poolEntity : enrolments) {
			EnrolmentDTO dto = new EnrolmentDTO();
			EnrolmentServiceTransformer.transformEntityToDTO(poolEntity, dto);
            if(securityEntity.isWriteOff() || securityEntity.getUnpostDate() != null || !this.isSecurityFullyReleased(securityEntity)) {
                dto.setCanDelete(canDeleteEnrolment(dto.getEnrolmentNumber(), releaseEnrolments));
            }
            enrolmentDTOs.add(dto);
			enrolmentNumbers.add(dto.getEnrolmentNumber());
		}

		List<EnrolmentWarrantyServiceFieldsDTO> wsList = releaseDAO.getEnrolmentWarrantyServiceFieldsList(enrolmentNumbers);
		for (EnrolmentWarrantyServiceFieldsDTO wsDto : wsList) {
			StringBuilder sb = new StringBuilder();
			sb.append("Requested ");
			if(!StringUtils.isEmpty(wsDto.getWsRequestor())) {
				sb.append("by ");
				try {
					UserDTO userDTO = userService.getUserById(wsDto.getWsRequestor());
					if (userDTO != null) {
						sb.append(userDTO.getFirstName()).append(" ").append(userDTO.getLastName());
					} else {
						sb.append(wsDto.getWsRequestor());
					}
				} catch (VbsDirectoryEnvironmentException e) {
					sb.append(wsDto.getWsRequestor());
				}
			}
			sb.append(" on ").append(DateUtil.getDateFormattedShort(wsDto.getWsRequestedDate()));
			for (EnrolmentDTO enrolmentDTO : enrolmentDTOs) {
				if (wsDto.getEnrolmentNumber().equalsIgnoreCase(enrolmentDTO.getEnrolmentNumber()) && enrolmentDTO.getWsInputRequested() == null) {
					if (wsDto.isWsInputRequested()) { enrolmentDTO.setWsInputRequested(sb.toString()); }
					if (wsDto.getWsReceivedDate() != null && enrolmentDTO.getWsInputReceived() == null) {
						enrolmentDTO.setWsInputReceived(wsDto.getWsReceivedDate());
						enrolmentDTO.setFcmAmountRetained(wsDto.getFcmAmountRetained());
					}
				}
			}
		}
		return enrolmentDTOs;
	}

	private boolean canDeleteEnrolment(String enrolmentNumber, List<ReleaseEnrolmentEntity> releaseEnrolments) {
        if(!releaseEnrolments.isEmpty()) {
            for(ReleaseEnrolmentEntity releaseEnrolment: releaseEnrolments){
                if(releaseEnrolment.getEnrolment().getEnrolmentNumber().equalsIgnoreCase(enrolmentNumber)
                        && releaseEnrolment.getRelease().getFinalApprovalStatus() != null
                        && releaseEnrolment.getRelease().getFinalApprovalStatus().equals(ReleaseApprovalStatusEnum.Authorized)
                        && releaseEnrolment.getAnalystApprovalStatus() != null
                        && releaseEnrolment.getAnalystApprovalStatus().equals(ReleaseApprovalStatusEnum.Authorized)){
                    return false;
                }
            }
        }
        return true;
    }

    private List<ContactDTO> getVbsForSecurityEntity(SecurityEntity securityEntity) {
		List<VbPoolEntity> vbPools = contactDAO.getVbsForPoolId(securityEntity.getPool().getId());
		List<ContactDTO> vbDTOs = new ArrayList<>();
		for (VbPoolEntity vbPool : vbPools) {
			ContactDTO vbDTO = ContactServiceTransformer.transformEntityToDTO(vbPool.getVb());
			if(securityEntity.getPrimaryVb() != null) {
				vbDTO.setPrimaryVb(vbPool.getVb().getId().compareTo(securityEntity.getPrimaryVb().getId()) == 0);
			}
			vbDTOs.add(vbDTO);
		}
		return vbDTOs;
	}

	private void validateSecurity(SecurityDTO securityDTO, SecurityEntity currentEntity, List<String> displayedAlertsConfirmations){
		List<ErrorDTO> errors = new ArrayList<>();

		if(securityDTO.getSecurityTypeId() == null) {
			errors.add(new ErrorDTO("The Security Type cannot be empty.", "securityTypeId"));
		}
		if(securityDTO.getSecurityPurposeId() == null) {
			errors.add(new ErrorDTO("The Security Purpose cannot be empty.", "securityPurposeId"));
		}

		if(!errors.isEmpty()){ throw new VbsValidationException(errors); }

		if(securityDTO.getSecurityPurposeId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) == 0 &&
				securityDTO.getIdentityTypeId() != null && lookupDAO.getIdentityTypeById(securityDTO.getIdentityTypeId()) == null){
			errors.add(new ErrorDTO("Identity Type not found.", "identityTypeId"));
		}

		if(securityDTO.getOriginalAmount() == null) {
			errors.add(new ErrorDTO("The Security Original Amount cannot be empty.", "originalAmount"));
		} else if( !(securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) == 0 ||
				securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_PRE_EXISTING_ELEMENTS_FUND) == 0) &&
				securityDTO.getOriginalAmount().compareTo(BigDecimal.ZERO) == 0) {
			errors.add(new ErrorDTO("Original Amount is mandatory. Please enter Original Amount.","originalAmount"));
		}

		if(securityDTO.getCurrentAmount() == null) {
			errors.add(new ErrorDTO("The Security Current Amount cannot be empty.", "currentAmount"));
		}

		if((securityDTO.getSecurityPurposeId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) != 0 && securityDTO.getUnitsToCover() == null) 
			|| (securityDTO.getSecurityPurposeId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) == 0 && securityDTO.getIdentityTypeId() == null && securityDTO.getUnitsToCover() == null)) {
			errors.add(new ErrorDTO("Units Intended to Cover must not be empty.", "unitsToCover"));
		}else if((securityDTO.getSecurityPurposeId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) != 0 && securityDTO.getUnitsToCover() != null && securityDTO.getUnitsToCover() < 1) 
			|| (securityDTO.getSecurityPurposeId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) == 0 && securityDTO.getIdentityTypeId() == null && securityDTO.getUnitsToCover() != null && securityDTO.getUnitsToCover() < 1)){
			errors.add(new ErrorDTO("Units Intended to Cover must be a positive number.", "unitsToCover"));
		}

		if(securityDTO.getSecurityPurposeId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) == 0 && securityDTO.getIdentityTypeId() != null 
			&& (securityDTO.getIdentityTypeId().compareTo(VbsConstants.IDENTITY_TYPE_FREEHOLD) == 0 || securityDTO.getIdentityTypeId().compareTo(VbsConstants.IDENTITY_TYPE_BOTH) == 0) 
			&& securityDTO.getFhUnitsToCover() == null){
			errors.add(new ErrorDTO("Freehold Units Intended to Cover must not be empty.", "fhUnitsToCover"));
		}else if(securityDTO.getSecurityPurposeId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) == 0 && securityDTO.getIdentityTypeId() != null 
			&& (securityDTO.getIdentityTypeId().compareTo(VbsConstants.IDENTITY_TYPE_FREEHOLD) == 0 || securityDTO.getIdentityTypeId().compareTo(VbsConstants.IDENTITY_TYPE_BOTH) == 0) 
			&& securityDTO.getFhUnitsToCover() != null && securityDTO.getFhUnitsToCover() < 1){
			errors.add(new ErrorDTO("Freehold Units Intended to Cover must be a positive number.", "fhUnitsToCover"));
		}

		if(securityDTO.getSecurityPurposeId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) == 0 && securityDTO.getIdentityTypeId() != null 
			&& (securityDTO.getIdentityTypeId().compareTo(VbsConstants.IDENTITY_TYPE_CONDO) == 0 || securityDTO.getIdentityTypeId().compareTo(VbsConstants.IDENTITY_TYPE_BOTH) == 0) 
			&& securityDTO.getCeUnitsToCover() == null){
			errors.add(new ErrorDTO("Condo Units Intended to Cover must not be empty.", "ceUnitsToCover"));
		}else if(securityDTO.getSecurityPurposeId().compareTo(VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) == 0 && securityDTO.getIdentityTypeId() != null 
			&& (securityDTO.getIdentityTypeId().compareTo(VbsConstants.IDENTITY_TYPE_CONDO) == 0 || securityDTO.getIdentityTypeId().compareTo(VbsConstants.IDENTITY_TYPE_BOTH) == 0) 
			&& securityDTO.getCeUnitsToCover() != null && securityDTO.getCeUnitsToCover() < 1){
			errors.add(new ErrorDTO("Condo Units Intended to Cover must be a positive number.", "ceUnitsToCover"));
		}


		if(VbsUtil.isNullorEmpty(securityDTO.getInstrumentNumber())) {
			ErrorDTO err = new ErrorDTO("The Security Instrument Number cannot be empty.", INSTRUMENT_NUMBER);
			errors.add(err);
		} else {
			if(currentEntity.getInstrumentNumber() == null || !currentEntity.getInstrumentNumber().equalsIgnoreCase(securityDTO.getInstrumentNumber())){
				if(securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_PRE_EXISTING_ELEMENTS_FUND) == 0 && !securityDTO.getInstrumentNumber().toUpperCase().matches("(\\d+)-PEF-(\\d{5,})")) {
					ErrorDTO err = new ErrorDTO("The Security Instrument Number must be entered in the format xxxxx-PEF-xxxxxx.", INSTRUMENT_NUMBER);
					errors.add(err);
				}
				
				if (securityDAO.securityWithTheSameInstrumentNumberExists(securityDTO.getInstrumentNumber().toUpperCase().trim())
					&& !displayedAlertsConfirmations.contains("INSTRUMENT_NUMBER_ALREADY_EXISTS")) {
					errors.add(new ErrorDTO("Instrument number already exists.", INSTRUMENT_NUMBER,
							ErrorTypeEnum.CONFIRM, "Instrument number already exists",
							"The same instrument number already exists. Would you like to proceed?",
							"INSTRUMENT_NUMBER_ALREADY_EXISTS"));
				}
			}
		}

		if(securityDTO.getIssuedDate() == null) {
			errors.add(new ErrorDTO("The Security Issued Date cannot be empty.", "issuedDate"));
		}else if((currentEntity.getIssuedDate() == null || currentEntity.getIssuedDate().compareTo(securityDTO.getIssuedDate()) != 0) &&
				DateUtil.isDateGreaterThanToday(securityDTO.getIssuedDate())){
			errors.add(new ErrorDTO("The Security Issued Date cannot be in the future.", "issuedDate"));
		}

		if(securityDTO.getReceivedDate() == null) {
			errors.add(new ErrorDTO("The Security Received Date cannot be empty.", RECEIVED_DATE));
		}else{
			if(currentEntity.getReceivedDate() == null || currentEntity.getReceivedDate().compareTo(securityDTO.getReceivedDate()) != 0){
				if(securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_CASH) == 0 && DateUtil.getLocalDateTimeStartOfCurrentMonth().isAfter(securityDTO.getReceivedDate())){
					errors.add(new ErrorDTO("The Security Received Date cannot be before the first of the current month.", RECEIVED_DATE));
				}
				if(securityDTO.getIssuedDate() != null && DateUtil.isDateGreaterThanToday(securityDTO.getReceivedDate())){
					errors.add(new ErrorDTO("The Security Received Date cannot be in the future.", RECEIVED_DATE));
				}
			}
		}

		if(securityDTO.getVbList() == null || securityDTO.getVbList().isEmpty()){
			errors.add(new ErrorDTO("You must have at least one VB.", "vbList"));
		}else{
			List<Integer> primaries = new ArrayList<>();
			int nonDeleted = 0;
			Map<String, String> vbs = new HashMap<>();
			for (int i=0;i<securityDTO.getVbList().size();i++){
				if(securityDTO.getVbList().get(i).getCrmContactId() != null) {
					if (securityDTO.getVbList().get(i).getDeleteReason() == null) {
						nonDeleted++;
						if (securityDTO.getVbList().get(i).isPrimaryVb()) { primaries.add(i); }
					}
					if (!vbs.containsKey(securityDTO.getVbList().get(i).getCrmContactId())) {
						vbs.put(securityDTO.getVbList().get(i).getCrmContactId(), securityDTO.getVbList().get(i).getCrmContactId());
					} else if (securityDTO.getVbList().get(i).getDeleteReason() != null) {
						errors.add(new ErrorDTO("VB# " + securityDTO.getVbList().get(i).getCrmContactId() + " can only be assigned to the security once.", "vbList"));
					}
				}
			}
			if(nonDeleted == 0 || primaries.isEmpty()){
				errors.add(new ErrorDTO("One VB must be selected as Primary VB.", "vbList"));
			}else if(primaries.size() > 1){
				errors.add(new ErrorDTO("Only one VB can be selected as Primary VB.", "vbList"));
				primaries.forEach(i -> errors.add(new ErrorDTO("", "vbList."+i+".primaryVb")));
			}
		}
		if(securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_CASH) == 0 || securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) == 0) {
			List<String> vbList = securityDTO.getVbList().stream().filter(v -> v.getDeleteReason() == null).map(ContactDTO::getCrmContactId).collect(Collectors.toList());
			long numSecWithInstrVb = securityDAO.getNumSecuritiesWithInstrNumVbNumber(securityDTO.getInstrumentNumber(), vbList);
			if ((securityDTO.getId() == null && numSecWithInstrVb > 0 || securityDTO.getId() != null && numSecWithInstrVb > 1) && !securityDTO.getInstrumentNumber().equalsIgnoreCase(currentEntity.getInstrumentNumber())) {
				errors.add(new ErrorDTO("Instrument number already exists for a VB, cash and DTA security instrument numbers must be unique per VB.", INSTRUMENT_NUMBER));
			}
		}
		if(!displayedAlertsConfirmations.contains("IDENTITY_TYPE_CHANGED") && ((currentEntity.getIdentityType() == null && securityDTO.getIdentityTypeId() != null) || (currentEntity.getIdentityType() != null && securityDTO.getIdentityTypeId() == null)
				|| (securityDTO.getIdentityTypeId() != null && currentEntity.getIdentityType() != null && securityDTO.getIdentityTypeId().compareTo(currentEntity.getIdentityType().getId()) != 0))){
			List<SecurityEntity> securitiesInPool = securityDAO.getSecurityByPoolId(securityDTO.getPoolId());
			if(!this.checkAllSecuritiesInPoolHasIdentityType(securitiesInPool, securityDTO.getIdentityTypeId())){
				errors.add(new ErrorDTO("Identity Type Changed.", "identityTypeChanged",
						ErrorTypeEnum.CONFIRM, "Identity Type Changed.",
						"Identity type of all the securities in a pool has to be the same, click yes if you want to change the Identity Type of all securities in pool: " + securityDTO.getPoolId() +", click no if you want to stop the saving.",
						"IDENTITY_TYPE_CHANGED"));
			}
		}
		if(securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_CASH) == 0){
			if (securityDTO.isThirdParty()) {
				if ((securityDTO.getThirdPartyContact() == null || VbsUtil.isNullorEmpty(securityDTO.getThirdPartyContact().getCrmContactId()))) {
					errors.add(new ErrorDTO("Third Party Contact must be selected when Pay Out To Third Party is checked.","thirdPartyContact"));
				} else if (VbsUtil.isNullorEmpty(securityDTO.getThirdPartyContact().getCompanyName())
						&& VbsUtil.isNullorEmpty(securityDTO.getThirdPartyContact().getFirstName())
						&& VbsUtil.isNullorEmpty(securityDTO.getThirdPartyContact().getLastName())) {
					//TODO doesn't this check exclude being able to type in the contactID directly? i.e. we should be attempting to collect the contact (pre-validation)
					errors.add(new ErrorDTO("Third Party Contact not found.", "thirdPartyContact"));
				}
			}
		}else {
			if(securityDTO.getFinancialInstitutionAndBranch().getBranch() == null || securityDTO.getFinancialInstitutionAndBranch().getBranch().getId() == null){
				if(securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) != 0 || (securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) == 0 && !isPendingWithdrawnOtherStatus(securityDTO))){
					errors.add(new ErrorDTO("You must choose a branch.", "financialInstitutionAndBranch.branch"));
					errors.add(new ErrorDTO("You must choose a Financial Institution and Branch.","financialInstitutionAndBranch.financialInstitution"));
				}
			}else {
				FinancialInstitutionEntity fi = genericDAO.findEntityById(FinancialInstitutionEntity.class, securityDTO.getFinancialInstitutionAndBranch().getFinancialInstitution().getId());
				if((fi.getFinancialInstitutionType().getId().compareTo(VbsConstants.FINANCIAL_INSTITUTION_TYPE_ADMINISTRATIVE_AGENT) == 0 || fi.getFinancialInstitutionType().getId().compareTo(VbsConstants.FINANCIAL_INSTITUTION_TYPE_ADMINISTRATIVE_BROKER) == 0) && securityDTO.getFinancialInstitutionMaaPoaDTO() == null){
					String validationMessage = fi.getFinancialInstitutionType().getId().compareTo(VbsConstants.FINANCIAL_INSTITUTION_TYPE_ADMINISTRATIVE_AGENT) == 0 ? "You must add a MAA.": "You must add a POA.";
					errors.add(new ErrorDTO(validationMessage, "financialInstitutionMaaPoaDTO"));
				}

				if((currentEntity.getBranch() == null || currentEntity.getBranch().getFinancialInstitution().getId().compareTo(securityDTO.getFinancialInstitutionAndBranch().getFinancialInstitution().getId()) != 0) &&
						fi.getExpiryDate().isBefore(DateUtil.getLocalDateTimeStartOfDay()) && securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) != 0){
					errors.add(new ErrorDTO("Financial Institution entered is expired.","financialInstitutionAndBranch.financialInstitution"));
				}else if(securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_CASH) != 0 && securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) != 0 &&
								securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_PRE_EXISTING_ELEMENTS_FUND) != 0){

					FinancialInstitutionAmountsDTO fiAmounts = financialInstitutionService.getAmounts(fi.getId(), false);
					BigDecimal newAmount;
					if (securityDTO.getId() == null) {
						newAmount = fiAmounts.getTotalCurrentAmount().add(securityDTO.getCurrentAmount());
					}else {
						if(currentEntity.getCurrentAmount().compareTo(securityDTO.getCurrentAmount()) != 0){
							newAmount = fiAmounts.getTotalCurrentAmount().subtract(currentEntity.getCurrentAmount()).add(securityDTO.getCurrentAmount());
						}else {
							newAmount = fiAmounts.getTotalCurrentAmount().add(securityDTO.getCurrentAmount());
						}
					}
					if ((currentEntity.getBranch() == null && securityDTO.getFinancialInstitutionAndBranch().getBranch().getId() != null)|| currentEntity.getCurrentAmount() == null ||
                            currentEntity.getCurrentAmount().compareTo(securityDTO.getCurrentAmount()) != 0 || currentEntity.getBranch().getFinancialInstitution().getId().compareTo(securityDTO.getFinancialInstitutionAndBranch().getFinancialInstitution().getId()) != 0) {
                        if (fi.getMaxSecurityAmount() != null && ((newAmount.compareTo(BigDecimal.ZERO) == 0 && fi.getMaxSecurityAmount().compareTo(BigDecimal.ZERO) == 0) ||  newAmount.compareTo(fi.getMaxSecurityAmount()) > 0)) {
							errors.add(new ErrorDTO("Financial Institution Limit Reached.", "financialInstitutionAndBranch.financialInstitution", ErrorTypeEnum.ALERT_FAIL, "Financial Institution Maximum Security Amount Reached",
									"Saving this security would breach the limit set on the Financial Institution. FI Code: " + fi.getId() + ", FI Name: " + fi.getName(), "FI_MAX_AMOUNT"));
						} else if (fi.getWnSecurityMaxAmount() != null && !displayedAlertsConfirmations.contains("FI_WARN_AMOUNT") && newAmount.compareTo(fi.getWnSecurityMaxAmount()) > 0) {
							errors.add(new ErrorDTO("Financial Institution Warning Amount Reached, Code: " + fi.getId(), "financialInstitutionAndBranch.financialInstitution", ErrorTypeEnum.CONFIRM, "Financial Institution Warning Maximum Security Amount Reached",
									"Saving this security will increase the Financial Institution's Current Amount over the defined Warning threshold, FI Code: "
											+ fi.getId() + ", FI Name: " + fi.getName(), "FI_WARN_AMOUNT"));
						}
					}

					if (securityDTO.getFinancialInstitutionMaaPoaDTO() != null) {
						if(currentEntity.getId() == null || currentEntity.getFinancialInstitutionMaaPoa() == null || (currentEntity.getFinancialInstitutionMaaPoa() != null && currentEntity.getFinancialInstitutionMaaPoa().getId().compareTo(securityDTO.getFinancialInstitutionMaaPoaDTO().getId()) != 0
								|| currentEntity.getCurrentAmount().compareTo(securityDTO.getCurrentAmount()) != 0 )){
							for (FinancialInstitutionMaaPoaInstitutionDTO maaPoaInstitution : securityDTO.getFinancialInstitutionMaaPoaDTO().getMaaPoaInstitutions()) {
								FinancialInstitutionAmountsDTO amounts = financialInstitutionService.getAmounts(maaPoaInstitution.getFinancialInstitution().getId(), false);
								BigDecimal newMAAPOAAmount;
								if (amounts.getTotalCurrentAmount() != null && maaPoaInstitution.getPercentage() != null) {
									if (securityDTO.getId() == null) {
										newMAAPOAAmount = amounts.getTotalCurrentAmount().add(securityDTO.getCurrentAmount().multiply(maaPoaInstitution.getPercentage().divide(VbsConstants.BIGDECIMAL_HUNDRED, VbsConstants.DECIMAL_SCALE, VbsConstants.ROUNDING_MODE)));
									} else {
										if(currentEntity.getCurrentAmount().compareTo(securityDTO.getCurrentAmount()) != 0){
											newMAAPOAAmount = (amounts.getTotalCurrentAmount().subtract(currentEntity.getCurrentAmount().multiply(maaPoaInstitution.getPercentage().divide(VbsConstants.BIGDECIMAL_HUNDRED, VbsConstants.DECIMAL_SCALE, VbsConstants.ROUNDING_MODE)))).add(securityDTO.getCurrentAmount().multiply(maaPoaInstitution.getPercentage().divide(VbsConstants.BIGDECIMAL_HUNDRED, VbsConstants.DECIMAL_SCALE, VbsConstants.ROUNDING_MODE)));
										}else {
											newMAAPOAAmount = amounts.getTotalCurrentAmount().add(securityDTO.getCurrentAmount().multiply(maaPoaInstitution.getPercentage().divide(VbsConstants.BIGDECIMAL_HUNDRED, VbsConstants.DECIMAL_SCALE, VbsConstants.ROUNDING_MODE)));
										}
									}
									if (((maaPoaInstitution.getFinancialInstitution().getMaxSecurityAmount() != null && newMAAPOAAmount.compareTo(maaPoaInstitution.getFinancialInstitution().getMaxSecurityAmount()) > 0) || (maaPoaInstitution.getFinancialInstitution().getMaxSecurityAmount() != null && maaPoaInstitution.getFinancialInstitution().getMaxSecurityAmount().compareTo(BigDecimal.ZERO) == 0)) && maaPoaInstitution.getPercentage().compareTo(BigDecimal.ZERO) != 0) {
										errors.add(new ErrorDTO("MAA/POA Financial Institution Limit Reached. FI Code: " + maaPoaInstitution.getFinancialInstitution().getId()
												+ ", FI Name: " + maaPoaInstitution.getFinancialInstitution().getName(),
												"financialInstitutionAndBranch.financialInstitution",
												ErrorTypeEnum.ALERT_FAIL,
												"MAA/POA Financial Institution Maximum Security Amount Reached. FI Code: " + maaPoaInstitution.getFinancialInstitution().getId()
														+ ", FI Name: " + maaPoaInstitution.getFinancialInstitution().getName(),
												"Saving this security would breach the limit set on the MAA/POA Financial Institution. FI Code: " + maaPoaInstitution.getFinancialInstitution().getId()
														+ ", FI Name: " + maaPoaInstitution.getFinancialInstitution().getName(),
												"FI_MAX_AMOUNT"));
									} else if (!displayedAlertsConfirmations.contains("FI_MAA_WARN_AMOUNT") && maaPoaInstitution.getFinancialInstitution().getWnSecurityMaxAmount() != null && newMAAPOAAmount.compareTo(maaPoaInstitution.getFinancialInstitution().getWnSecurityMaxAmount()) > 0 && maaPoaInstitution.getPercentage().compareTo(BigDecimal.ZERO) != 0) {
										errors.add(new ErrorDTO("MAA/POA Financial Institution Warning Amount Reached. FI Code"
												+ maaPoaInstitution.getFinancialInstitution().getId(), "financialInstitutionAndBranch.financialInstitution", ErrorTypeEnum.CONFIRM, "MAA/POA Financial Institution Warning Maximum Security Amount Reached",
												"Saving this security will increase the Financial Institution's Current Amount over the defined MAA/POA Warning threshold. FI Code: "
														+ maaPoaInstitution.getFinancialInstitution().getId() + ", FI Name: " + maaPoaInstitution.getFinancialInstitution().getName(), "FI_MAA_WARN_AMOUNT"));
									}
								}
							}
						}
					}
				}
			}

			if((securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) == 0 && !isPendingWithdrawnOtherStatus(securityDTO)) || securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_PRE_EXISTING_ELEMENTS_FUND) == 0) {
				if(securityDTO.getEscrowAgentLawyerAssistant() == null || securityDTO.getEscrowAgentLawyerAssistant().getEscrowAgent() == null || securityDTO.getEscrowAgentLawyerAssistant().getEscrowAgent().getCrmContactId() == null) {
					errors.add(new ErrorDTO("You must enter an escrow agent", ESCROW_AGENT_FIELD));
				} else {
					ContactEntity currentEscrow = (currentEntity.getId() != null) ? contactDAO.getContactBySecurityIdContactSecurityRoleId(currentEntity.getId(), VbsConstants.CONTACT_SECURITY_ESCROW) : null;
					if(currentEscrow == null || securityDTO.getEscrowAgentLawyerAssistant().getEscrowAgent().getId().compareTo(currentEntity.getId()) != 0){
						ContactEntity newEscrow = contactDAO.getContactByCrmContactId(securityDTO.getEscrowAgentLawyerAssistant().getEscrowAgent().getCrmContactId());
						if(newEscrow == null || newEscrow.getContactType().getId().compareTo(VbsConstants.CONTACT_TYPE_ESCROW_AGENT) != 0) {
							errors.add(new ErrorDTO("Escrow agent Does not exist", ESCROW_AGENT_FIELD));
						} else if(currentEscrow == null && securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) == 0 &&
								newEscrow.getEscrowLicenseApplicationStatus() != null && newEscrow.getEscrowLicenseApplicationStatus().getId().compareTo(VbsConstants.LICENCE_APPLICATION_STATUS_PEF) == 0) {
							errors.add(new ErrorDTO("PEF only escrow agent can't be added to a DTA", ESCROW_AGENT_FIELD));
						}else if(currentEscrow == null && newEscrow.getLicenseStatus() != null && newEscrow.getLicenseStatus().getId().compareTo(VbsConstants.LICENCE_STATUS_ACTIVE) != 0) {
							errors.add(new ErrorDTO("Escrow Agent entered is inactive.", ESCROW_AGENT_FIELD));
						}
					}
				}
			}


			if (securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) == 0) {
				if(securityDTO.isMonthlyReport()){
					if(securityDTO.getMonthlyDate() == null){
						errors.add(new ErrorDTO("You must select a date for the Monthly Report", "monthlyDate"));
					}else if(securityDTO.getSecurityDeposit() == null) {
						errors.add(new ErrorDTO("You must select a Security Deposit value for the Monthly Report.", "securityDeposit"));
					}else if(securityDTO.getMonthlyDate().isAfter(LocalDateTime.now())){
						errors.add(new ErrorDTO("The report date cannot be in the future.", "monthlyDate"));
					}
				}

				if(VbsUtil.isNullorEmpty(securityDTO.getDtaAccountNumber()) && !isPendingWithdrawnOtherStatus(securityDTO)) {
					errors.add(new ErrorDTO("You must enter DTA account number", "dtaAccountNumber"));
				}

				if (VbsUtil.isNullorEmpty(securityDTO.getPpsaNumber()) && !isPendingWithdrawnOtherStatus(securityDTO)) {
					errors.add(new ErrorDTO("The Security PPSA Number cannot be empty.", "ppsaNumber"));
				}

				if (currentEntity.getPpsaExpiryDate() != null && securityDTO.getPpsaExpiryDate() != null &&
						currentEntity.getPpsaExpiryDate().compareTo(securityDTO.getPpsaExpiryDate()) != 0 && DateUtil.isDateBeforeToday(securityDTO.getPpsaExpiryDate())) {
					errors.add(new ErrorDTO("The Security PPSA Expiry Date cannot be in the past.", "ppsaExpiryDate"));
				}
			}

			if(securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_PRE_EXISTING_ELEMENTS_FUND) == 0){
				if(VbsUtil.isNullorEmpty(securityDTO.getPefAccountNumber())){
					errors.add(new ErrorDTO("You must enter PEF account number", "pefAccountNumber"));
				}
			}
			if (securityDTO.getSecurityTypeId().longValue() == VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT && currentEntity != null && currentEntity.getOriginalAmount() != null && securityDTO.getOriginalAmount().compareTo(currentEntity.getOriginalAmount()) != 0  && securityDTO.getId() != null && releaseDAO.securityHasCompletedPrincipalReleases(securityDTO.getId()) && !displayedAlertsConfirmations.contains("DTA_ORIGINAL_AMOUNT_CHANGE")) {
				errors.add(new ErrorDTO("Original Amount Change.", "dtaOriginalAmountChange",
						ErrorTypeEnum.CONFIRM, "Original Amount Change",
						"This DTA has been released/reduced. Are you sure you want to change the Original amount",
						"DTA_ORIGINAL_AMOUNT_CHANGE"));
			}
			if (securityDTO.getSecurityTypeId().longValue() == VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT && currentEntity != null && currentEntity.getCurrentAmount() != null && securityDTO.getCurrentAmount().compareTo(currentEntity.getCurrentAmount()) != 0 && securityDTO.getId() != null && releaseDAO.securityHasCompletedPrincipalReleases(securityDTO.getId()) && !displayedAlertsConfirmations.contains("DTA_CURRENT_AMOUNT_CHANGE")) {
				errors.add(new ErrorDTO("Current Amount Change.", "dtaCurrentAmountChange",
						ErrorTypeEnum.CONFIRM, "Current Amount Change",
						"This DTA has been released/reduced. Are you sure you want to change the Current amount",
						"DTA_CURRENT_AMOUNT_CHANGE"));
			}
			if (securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_SURETY_BOND) == 0) {
				if (securityDTO.isJointBond()) {
					if (securityDTO.getReleaseFirst() == null) {
						errors.add(new ErrorDTO("The Release First must be set when Security is Joint Bond.", "releaseFirst"));
					}
					if (securityDTO.getDemandFirst() == null) {
						errors.add(new ErrorDTO("The Demand First must be set when Security is Joint Bond.", "demandFirst"));
					}

					if (securityDTO.getReleaseFirst() != null && securityDTO.getDemandFirst() != null && (securityDTO.getPoolId() != null && currentEntity.getPool() != null)) {
						List<SecurityEntity> securitiesInPool = securityDAO.getSecurityByPoolId(securityDTO.getPoolId());
						if (securitiesInPool.size() >= 2 && securitiesInPool.stream().noneMatch(s -> s.getId().compareTo(securityDTO.getId()) == 0)) {
							errors.add(new ErrorDTO("There can be no more than two Surety Bonds in Joint Bond Security", "jointBond"));
						} else if (securitiesInPool.stream().anyMatch(s -> s.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_SURETY_BOND) != 0)) {
							errors.add(new ErrorDTO("Bonds in Joint Bond security must be Security Bond Type", "jointBond"));
						}

						if (securitiesInPool.size() > 1 && !checkReleaseFirstExistsInPool(securitiesInPool, securityDTO) && securityDTO.isJointBond() && (securityDTO.getReleaseFirst() != null && securityDTO.getReleaseFirst().equals(YesNoEnum.NO))) {
							errors.add(new ErrorDTO("At least one security within the Pool must have Release Security First set to Yes.", "releaseFirst"));
						}

						if (securitiesInPool.size() > 1 && !checkDemandFirstExistsInPool(securitiesInPool, securityDTO) && securityDTO.isJointBond() && (securityDTO.getDemandFirst() != null && securityDTO.getDemandFirst().equals(YesNoEnum.NO))) {
							errors.add(new ErrorDTO("At least one security within the Pool must have Demand Security First set to Yes.", "demandFirst"));
						}

						if (securityDTO.getReleaseFirst().equals(YesNoEnum.YES) || securityDTO.getDemandFirst().equals(YesNoEnum.YES)) {
							boolean duplicateReleaseFirst = (securityDTO.getReleaseFirst().equals(YesNoEnum.YES) && checkReleaseFirstExistsInPool(securitiesInPool, securityDTO));
							boolean duplicateDemandFirst = (securityDTO.getDemandFirst().equals(YesNoEnum.YES) && checkDemandFirstExistsInPool(securitiesInPool, securityDTO));

							if (duplicateReleaseFirst && duplicateDemandFirst && !displayedAlertsConfirmations.contains("DUPLICATE_RELEASE_DEMAND_FIRST")) {
								errors.add(new ErrorDTO("Duplicate release first in pool.", "releaseFirst",
										ErrorTypeEnum.CONFIRM, "Duplicate Release and Demand First in Pool",
										"Another Security exists in the same pool with both Release Security First and Demand Security first set to Yes. Please click Yes to save or No to go back without saving the Security.",
										"DUPLICATE_RELEASE_DEMAND_FIRST"));
								errors.add(new ErrorDTO("Duplicate demand first in pool.", "demandFirst"));
							} else if (duplicateReleaseFirst && !displayedAlertsConfirmations.contains("DUPLICATE_RELEASE_FIRST")) {
								errors.add(new ErrorDTO("Duplicate release first in pool.", "releaseFirst",
										ErrorTypeEnum.CONFIRM, "Duplicate Release First in Pool",
										"Another Security exists in the same pool with Release Security First set to Yes. Please click Yes to save or No to go back without saving the Security.", "DUPLICATE_RELEASE_FIRST"));
							} else if (duplicateDemandFirst && !displayedAlertsConfirmations.contains("DUPLICATE_DEMAND_FIRST")) {
								errors.add(new ErrorDTO("Duplicate demand first in pool.", "demandFirst",
										ErrorTypeEnum.CONFIRM, "Duplicate Demand First in Pool",
										"Another Security exists in the same pool with Demand Security First set to Yes. Please click Yes to save or No to go back without saving the Security.", "DUPLICATE_DEMAND_FIRST"));
							}
						}
					}
				}
			}
		}

        if(securityDTO.getAlerts() != null && !securityDTO.getAlerts().isEmpty()){
            for(int i=0;i<securityDTO.getAlerts().size();i++){
                this.validateAlert(securityDTO.getAlerts().get(i), errors, i);
            }
        }

		if(securityDTO.getEnrolments() != null && !securityDTO.getEnrolments().isEmpty()){
		    List<String> seen = new ArrayList<>();
		    List<String> duplicates = new ArrayList<>();
		    securityDTO.getEnrolments().forEach(e -> {
		        if(!seen.contains(e.getEnrolmentNumber())){
		            seen.add(e.getEnrolmentNumber());
                }else{
		            duplicates.add(e.getEnrolmentNumber());
                }
            });
            if(!duplicates.isEmpty()){
                for(int i=0;i<securityDTO.getEnrolments().size();i++){
                    if(duplicates.contains(securityDTO.getEnrolments().get(i).getEnrolmentNumber())){
                        errors.add(new ErrorDTO("Duplicate Enrolment Number", "enrolments."+i+".enrolmentNumber"));
                    }
                }
            }

			BigDecimal sumAllocatedAmount = securityDTO.getEnrolments().stream().
					filter(e -> e.getAllocatedAmount() != null).
					map(EnrolmentDTO::getAllocatedAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
			if(sumAllocatedAmount.compareTo(securityDTO.getOriginalAmount()) > 0) {
				errors.add(new ErrorDTO("Sum of security allocated amount cannot be greater than Original amount.", "originalAmount",
						ErrorTypeEnum.ALERT_FAIL, "Message",
						"Sum of security allocated amount cannot be greater than Original amount.",
						"ALLOCATED_AMOUNT_GREATER_THAN"));
			}
		}

		if(currentEntity.getId() == null){
			if(securityDTO.getPoolId() != null) {
				errors.add(new ErrorDTO("The Pool ID must be empty when creating a security.", "poolId"));
			}
		}else{
			if(securityDTO.getSecurityTypeId().compareTo(currentEntity.getSecurityType().getId()) != 0 && currentEntity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_CASH) == 0){
				errors.add(new ErrorDTO("The Security Type cannot be changed on a Cash Security.", "securityTypeId"));
			}
			if(securityDTO.getPoolId() == null) {
				errors.add(new ErrorDTO("A valid Pool Id, or the current Pool Id of "+currentEntity.getPool().getId()+" must be entered.", "poolId"));
			}else if(securityDTO.getPoolId().compareTo(currentEntity.getPool().getId()) != 0){
				//validate pool change
				PoolEntity poolEntity = genericDAO.findEntityById(PoolEntity.class, securityDTO.getPoolId());
				if(poolEntity == null){
					errors.add(new ErrorDTO("The Pool Id entered does not exist.", "poolId"));
				}else{
					if(securityDTO.getSecurityTypeId().longValue() != VbsConstants.SECURITY_TYPE_SURETY_BOND && !securityDAO.allSecuritiesInPoolAreBlanketPurpose(poolEntity.getId())){
						errors.add(new ErrorDTO("All securities in the target pool must have the security purpose Blanket Security", POOL_ID));
					}
					if(securityDTO.getSecurityTypeId().longValue() != VbsConstants.SECURITY_TYPE_SURETY_BOND && !securityDAO.allSecuritiesInPoolAreFreeholdType(poolEntity.getId())){
						errors.add(new ErrorDTO("All securities in the target pool must have identity type freehold", POOL_ID));
					}else {
						List<VbPoolEntity> currentPoolContacts = contactDAO.getVbsForPoolId(currentEntity.getPool().getId());
						List<VbPoolEntity> targetPoolContacts = contactDAO.getVbsForPoolId(securityDTO.getPoolId());

						if (currentPoolContacts.size() != targetPoolContacts.size() || currentPoolContacts.stream().noneMatch(c -> targetPoolContacts.stream().anyMatch(t -> t.getVb().getId().compareTo(c.getVb().getId()) == 0))) {
							errors.add(new ErrorDTO("The securities in the same pool requires same exact VB(s).", POOL_ID));
						} else if (securityDTO.getSecurityTypeId().longValue() != VbsConstants.SECURITY_TYPE_SURETY_BOND && securityDTO.getIdentityTypeId() != null && !securityDTO.getIdentityTypeId().equals(VbsConstants.IDENTITY_TYPE_FREEHOLD)) {
							errors.add(new ErrorDTO("The security must have identity type freehold", POOL_ID));
						} else if (securityDTO.getSecurityTypeId().longValue() != VbsConstants.SECURITY_TYPE_SURETY_BOND && securityDTO.getSecurityPurposeId() != VbsConstants.SECURITY_PURPOSE_BLANKET_SECURITY) {
							errors.add(new ErrorDTO("The security must have security purpose Blanket", POOL_ID));
						}
					}
				}
			}
		}

        if(errors.isEmpty() &&
                securityDTO.getFinancialInstitutionAndBranch() != null &&
				securityDTO.getFinancialInstitutionAndBranch().getFinancialInstitution() != null &&
				securityDTO.getFinancialInstitutionAndBranch().getFinancialInstitution().getId() != null &&
				(securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_LETTER_OF_CREDIT) == 0 ||
				  securityDTO.getSecurityTypeId().compareTo(VbsConstants.SECURITY_TYPE_SURETY_BOND) == 0)){
            //Only check if the fin. inst. or instr. no has actually changed
            if(!displayedAlertsConfirmations.contains("DUPLICATE_INSTRUMENT_NUMBER") && (
					(currentEntity.getInstrumentNumber() == null || !securityDTO.getInstrumentNumber().equalsIgnoreCase(currentEntity.getInstrumentNumber())) ||
					currentEntity.getBranch() == null ||
					securityDTO.getFinancialInstitutionAndBranch().getFinancialInstitution().getId().compareTo(currentEntity.getBranch().getFinancialInstitution().getId()) != 0)){
                List<Long> idList = securityDAO.getSecurityIdsWithInstrumentNumberFinancialInst(securityDTO.getInstrumentNumber().toUpperCase().trim(), securityDTO.getFinancialInstitutionAndBranch().getFinancialInstitution().getId());
                if(idList.stream().anyMatch(id -> securityDTO.getId() == null || id.compareTo(securityDTO.getId()) != 0)) {
                    errors.add(new ErrorDTO("Duplicate Instrument Number", "instrumentNumber",
                            ErrorTypeEnum.CONFIRM, "Duplicate Instrument Number", "This instrument number exists under the same Financial Institution. Please click Yes to save or No to go back without saving the Security.", "DUPLICATE_INSTRUMENT_NUMBER"));
                }
            }
        }
		if(!displayedAlertsConfirmations.contains("ANALYST_NOT_FOUND") && securityDTO.getId() != null && currentEntity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) == 0 && securityDTO.getSecurityStatusId() != null && securityDTO.getSecurityStatusId().equals(VbsConstants.SECURITY_STATUS_ACCEPTED) && !securityDAO.hasSecurityHadStatus(securityDTO.getId(), VbsConstants.SECURITY_STATUS_ACCEPTED)){
			try {
				String analystUserId = vbaWS.getVbApplicationAnalyst(securityDTO.getPrimaryVb().getCrmContactId());
				if(analystUserId == null) {
					errors.add(new ErrorDTO("Not able to send email to underwriting group to notify DTA set up. Please notify them manually", "oldVersion",
							ErrorTypeEnum.ALERT, "Message",
							"Not able to send email to underwriting group to notify DTA set up. Please notify them manually",
							"ANALYST_NOT_FOUND"));
					throw new VbsValidationException(errors);
				}
			}
			catch(VbsCheckedException e){
				errors.add(new ErrorDTO("Not able to send email to underwriting group to notify DTA set up. Please notify them manually", "oldVersion",
						ErrorTypeEnum.ALERT, "Message",
						"Not able to send email to underwriting group to notify DTA set up. Please notify them manually",
						"ANALYST_NOT_FOUND"));
				throw new VbsValidationException(errors);
			}
		}
		if(!errors.isEmpty()){
			throw new VbsValidationException(errors);
		}
	}

	private boolean checkAllSecuritiesInPoolHasIdentityType(List<SecurityEntity> securitiesInPool, Long identityType) {
		return securitiesInPool.stream().allMatch(securityEntity -> (identityType == null && securityEntity.getIdentityType() == null) || identityType != null && securityEntity.getIdentityType() != null && securityEntity.getIdentityType().getId().compareTo(identityType) == 0);
	}
    private boolean checkReleaseFirstExistsInPool(List<SecurityEntity> securitiesInPool, SecurityDTO security) {
        return securitiesInPool.stream().anyMatch(securityEntity -> !securityEntity.getId().equals(security.getId()) && securityEntity.isJointBond() && securityEntity.getReleaseFirst().equals(YesNoEnum.YES));
    }

    private boolean checkDemandFirstExistsInPool(List<SecurityEntity> securitiesInPool, SecurityDTO security) {
        return securitiesInPool.stream().anyMatch(securityEntity -> !securityEntity.getId().equals(security.getId()) && securityEntity.isJointBond() && securityEntity.getDemandFirst().equals(YesNoEnum.YES));
    }

	private void validateSecuritySearch(SecuritySearchParametersDTO securitySearchParametersDTO){
		List<ErrorDTO> errors = new ArrayList<>();

		if (!VbsUtil.isNullorEmpty(securitySearchParametersDTO.getSecurityNumber()) && !securitySearchParametersDTO.getSecurityNumberSearchType().equals(SearchTypeEnum.IN)) {
			if(!ValidationUtil.isNumericStringValid(securitySearchParametersDTO.getSecurityNumber())) {
				errors.add(new ErrorDTO("The Security Number must be an integer greater than zero.","securityNumber"));
			}else if(ValidationUtil.isNumberZeroOrNegative(Long.parseLong(securitySearchParametersDTO.getSecurityNumber()))) {
				errors.add(new ErrorDTO("The Security Number must be a Positive Number.", "securityNumber"));
			}
		}

		if(securitySearchParametersDTO.getPoolId() != null) {
			if(ValidationUtil.isNumberZeroOrNegative(securitySearchParametersDTO.getPoolId())) {
				errors.add(new ErrorDTO("The Pool Number must be a Positive Number.", POOL_ID));
			}else{
				PoolEntity pool = genericDAO.findEntityById(PoolEntity.class, securitySearchParametersDTO.getPoolId());
				if(pool == null) {
					errors.add(new ErrorDTO("The given Pool Number does not exist.", "poolId"));
				}
			}
		}

		if(!errors.isEmpty()){
			throw new VbsValidationException(errors);
		}
	}

	private void sendJmsMessageToFms(SecurityEntity securityEntity, UserDTO userDTO) {
		if (securityEntity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_CASH) == 0){
			this.sendCashSecurityMessageToFms(securityEntity, userDTO);
		}else if (securityEntity.getSecurityType().getId().compareTo(VbsConstants.SECURITY_TYPE_DEPOSIT_TRUST_AGREEMENT) == 0){
			this.sendCreateDTARequestMessageToFms(securityEntity, userDTO);
		}
	}

	private void sendCreateDTARequestMessageToFms(SecurityEntity security, UserDTO userDTO) {
		CreateDTARequest dtaRequest = new CreateDTARequest();
		dtaRequest.setVbNumber(security.getPrimaryVb().getCrmContactId());
		dtaRequest.setInstrumentNumber(security.getInstrumentNumber());
		dtaRequest.setOriginalAmount(VbsUtil.getStringForMoney(security.getOriginalAmount()));
		dtaRequest.setSecurityId(security.getId());
		dtaRequest.setCreateDate(DateUtil.getDateFormattedForFms(security.getCreateDate()));
		dtaRequest.setCreateUser(userDTO.getUserId());
		String xml = VbsUtil.convertJaxbToXml(dtaRequest, CreateDTARequest.class);
		jmsMessageSenderService.postFmsRequestQueue(xml, CreateDTARequest.class.getSimpleName());
		jmsMessageSenderService.insertTransactionLog(JmsMessageTypeEnum.getFmsCreateDTARequestTracking(security.getId(), security.getPrimaryVb().getCrmContactId()), xml);
	}

	private void sendCashSecurityMessageToFms(SecurityEntity security, UserDTO userDTO) {
		CreateCashSecurityRequest cashRequest = new CreateCashSecurityRequest();
		cashRequest.setVbNumber(security.getPrimaryVb().getCrmContactId());
		cashRequest.setCreateDate(DateUtil.getDateFormattedForFms(security.getCreateDate()));
		cashRequest.setCreateUser(userDTO.getUserId());
		cashRequest.setInstrumentNumber(security.getInstrumentNumber());
		cashRequest.setOriginalAmount(VbsUtil.getStringForMoney(security.getOriginalAmount()));
		cashRequest.setSecurityId(security.getId());
		String xml = VbsUtil.convertJaxbToXml(cashRequest, CreateCashSecurityRequest.class);
		jmsMessageSenderService.postFmsRequestQueue(xml, CreateCashSecurityRequest.class.getSimpleName());
		jmsMessageSenderService.insertTransactionLog(JmsMessageTypeEnum.getFmsCashSecurityRequestTracking(security.getId(), security.getPrimaryVb().getCrmContactId()), xml);
	}

	private boolean isPendingWithdrawnOtherStatus(SecurityDTO securityDTO){
		return securityDTO.getSecurityStatusId() != null && (securityDTO.getSecurityStatusId().compareTo(VbsConstants.SECURITY_STATUS_PENDING_WITH_FEE) == 0
				|| securityDTO.getSecurityStatusId().compareTo(VbsConstants.SECURITY_STATUS_PENDING_WITH_OS_FEE) == 0
				|| securityDTO.getSecurityStatusId().compareTo(VbsConstants.SECURITY_STATUS_OTHER) == 0
				|| securityDTO.getSecurityStatusId().compareTo(VbsConstants.SECURITY_STATUS_WITHDRAWN) == 0);
	}

	@Override
	public List<SecuritySearchResultDTO> searchDeletedSecurity(DeletedSecuritySearchParametersDTO deletedSearchParametersDTO, UserDTO userDTO) {
		List<SecuritySearchResultDTO> securities = securityDAO.searchDeletedSecurity(deletedSearchParametersDTO);
		securities = securities.stream().map(this::totalAmounts).collect(Collectors.toList());
		return securities;
	}

	@Override
	public CEEnrolmentDecisionDTO getOutstandingDecisionByEnrolmentNumber(String enrolmentNumber){
		CEEnrolmentDecisionEntity entity = securityDAO.getOutstandingCEEnrolmentDecisionEntityByEnrolmentNumber(enrolmentNumber);
		CEEnrolmentDecisionDTO dto = new CEEnrolmentDecisionDTO();
		dto.setEnrolmentNumber(entity.getEnrolment().getEnrolmentNumber());
		dto.setEnrolmentAddress(entity.getEnrolment().getAddress().concatAddress());
		dto.setVbName(entity.getEnrolment().getEnrollingVb().getCompanyName());
		dto.setVbNumber(entity.getEnrolment().getEnrollingVb().getCrmContactId());
		for(SecurityEntity securityEntity : securityDAO.getBlanketSecuritiesByEnrollingVb(enrolmentNumber)){
			SecurityDTO securityDTO = new SecurityDTO();
			securityDTO.setId(securityEntity.getId());
			securityDTO.setCurrentAmount(securityEntity.getCurrentAmount());
			securityDTO.setOriginalAmount(securityEntity.getOriginalAmount());
			securityDTO.setPoolId(securityEntity.getPool().getId());
			securityDTO.setUnitsToCover(securityEntity.getPool().getUnitsToCover());
			securityDTO.setSecurityTypeId(securityEntity.getSecurityType().getId());
			securityDTO.setInstrumentNumber(securityEntity.getInstrumentNumber());
			if(securityEntity.getIdentityType() != null) {
                securityDTO.setIdentityTypeId(securityEntity.getIdentityType().getId());
            }
			dto.getBlanketSecurities().add(securityDTO);
		}
		return dto;
	}

	@Override
	public void updateCEEnrolmentDecision(CEEnrolmentDecisionDTO decisionDTO, UserDTO userDTO){
		CEEnrolmentDecisionEntity entity = securityDAO.getOutstandingCEEnrolmentDecisionEntityByEnrolmentNumber(decisionDTO.getEnrolmentNumber());
		entity.setApprovalAnalyst(userDTO.getUserId());
		entity.setApprovalDate(LocalDateTime.now());
		entity.setApprovalDecision(decisionDTO.getDecision());
		entity.setApprovalReason(decisionDTO.getReason());
		securityDAO.saveCEEnrolmentDecisionEntity(entity);
		if(decisionDTO.getDecision().equals(YesNoEnum.YES)) {
			List<SecurityEntity> blankets = securityDAO.getBlanketSecuritiesByEnrollingVb(decisionDTO.getEnrolmentNumber());
			List<PoolEntity> pools = new ArrayList<>();
			for(SecurityEntity blanket : blankets){
				if(!pools.contains(blanket.getPool())){
					pools.add(blanket.getPool());
				}
			}
			LocalDateTime t = LocalDateTime.now();
			EnrolmentEntity enrolment = genericDAO.findEntityById(EnrolmentEntity.class, decisionDTO.getEnrolmentNumber());
			for(PoolEntity pool : pools){
				if(pool.getCEUnitsToCover() != null && pool.getCEUnitsToCover() > 0){
					pool.setCEUnitsToCover(pool.getCEUnitsToCover() + decisionDTO.getUnitsToCover());
				}else{
					pool.setUnitsToCover(pool.getUnitsToCover() + decisionDTO.getUnitsToCover());
				}
				genericDAO.updateEntity(pool, userDTO.getUserId().toUpperCase(), t);

				List<EnrolmentPoolEntity> enrolments = enrolmentDAO.getEnrolmentsByPoolId(pool.getId());
				Optional<EnrolmentPoolEntity> opt = enrolments.stream().filter(e -> e.getEnrolment().getEnrolmentNumber().equals(decisionDTO.getEnrolmentNumber())).findFirst();
				if(!opt.isPresent()){
					EnrolmentPoolEntity ep = new EnrolmentPoolEntity();
					ep.setEnrolment(enrolment);
					ep.setPool(pool);
					ep.setAllocatedAmount(BigDecimal.ZERO);
					ep.setRaAmount(BigDecimal.ZERO);
					ep.setMaxUnitDeposit(BigDecimal.ZERO);
					ep.setInventoryAmount(BigDecimal.ZERO);
					ep.setExcessAmount(BigDecimal.ZERO);
					ep.setCurrentAllocatedAmount(BigDecimal.ZERO);
					ep.setPefAmount(BigDecimal.ZERO);
					genericDAO.addEntity(ep, userDTO.getUserId(), t);
				}
			}
			processorTriggeringService.triggerCEBlanketEmail(decisionDTO, userDTO);
		}
	}


	@Override
	public void sendCEBlanketEmail(CeBlanketEmailRequest request) throws VbsCheckedException {
		String subject = String.format(propertyService.getValue(VbsConstants.CE_BLANKET_EMAIL_SUBJECT), request.getEnrolmentNumber());
		String body = String.format(propertyService.getValue(VbsConstants.CE_BLANKET_EMAIL_BODY),
			request.getEnrolmentNumber(), request.getVbNumber(), request.getVbName(), request.getPoolId().stream().map(Object::toString).collect(Collectors.joining(", ")),
				request.getUserName(), request.getDateAdded());
		emailWSInvocationService.sendEmailUsingEsp(subject, body, VbsConstants.EMAIL_NO_REPLY_ADDRESS, propertyService.getValue(VbsConstants.SECURITY_GROUP_EMAIL));
	}
}
