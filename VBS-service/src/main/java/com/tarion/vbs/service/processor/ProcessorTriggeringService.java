package com.tarion.vbs.service.processor;

import com.tarion.vbs.common.dto.enrolment.CEEnrolmentDecisionDTO;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
import com.tarion.vbs.service.jaxb.autorelease.AutoReleaseCreateReleaseRequest;

import javax.ejb.Local;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;


/**
 * This class is used to post a message to an internal JMS queue to trigger processing events
 *
 * @author <A href="joni.paananen@tarion.com">Joni Paananen</A>
 */
@Local
public interface ProcessorTriggeringService {

    void triggerAutoReleaseGetVBApplicationStatus(Long autoReleaseRunId, List<String> vbNumbers);

    void triggerCheckDataReceived(Long autoReleaseRunId);

    void triggerEvaluateCriteria(Long autoReleaseEnrolmentId);

    void triggerCheckCriteriaComplete(Long autoReleaseRunId);

    void triggerFinalizeAutoReleaseRun(Long runId, String userId);

    void triggerCreateRelease(AutoReleaseCreateReleaseRequest request);

    void triggerCheckCreateReleasesCompleted(Long autoReleaseRunId, int numberOfReleases, String userId);

    void triggerInterestCalculationForDate(LocalDate date);

    void triggerAllocationRequestEmail(Long securityId);

    void triggerCEBlanketEmail(CEEnrolmentDecisionDTO decisionDTO, UserDTO userDTO);
}
