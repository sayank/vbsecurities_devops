package com.tarion.vbs.service.correspondence;

import javax.ejb.*;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.soap.SOAPException;

import com.tarion.vbs.common.dto.correspondence.CorrespondenceWSRequest;
import com.tarion.vbs.common.dto.correspondence.CorrespondenceWSResponse;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;

/**
 * The Correspondence WebService.
 * 
 * @author <A href="shadi.kajevand@tarion.com">Shadi Kajevand</A>
 */
@Stateless
@Local
@WebService(serviceName="CorrespondenceWS")
public class CorrespondenceWS {
	
	@EJB
	private CorrespondenceService correspondenceTokenService;
	
	@WebMethod
	public CorrespondenceWSResponse getParagraphs(CorrespondenceWSRequest request) throws SOAPException {
		try {
			return correspondenceTokenService.getTokens(request);
		}catch(Exception e){
			if(e.getCause() instanceof VbsRuntimeException){
				throw new SOAPException(e.getCause().getMessage(), e.getCause());
			}else if(e.getCause().getCause() instanceof VbsRuntimeException){
				throw new SOAPException(e.getCause().getCause().getMessage(), e.getCause().getCause());
			}
			throw new SOAPException(e.getMessage(), e);
		}
	}
}
