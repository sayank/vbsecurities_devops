/* 
 * 
 * FinancialInstitutionService.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.financialinstitution;

import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.dto.financialinstitution.*;
import com.tarion.vbs.common.dto.security.SecurityDTO;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;

import javax.ejb.Remote;
import java.math.BigDecimal;
import java.util.List;

/**
 * Financial Institution Service provides operations for Financial Institution  tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-15
 * @version 1.0
 */
@Remote
public interface FinancialInstitutionServiceRemote {
	
	List<FinancialInstitutionDTO> getAllFinancialInstitutions();
	List<BranchDTO> getAllBranches();
	List<FinancialInstitutionDTO> getFinancialInstitutionsForNameOrIdLike(String nameOrId, int maxResults);
	FinancialInstitutionBranchCompositeDTO getFullFinancialInstitutionForSecurity(Long securityId);
	List<BranchDTO> getBranchesForFinancialInstitution(Long financialInstitutionId);
	List<FinancialInstitutionDTO> getFinancialInstitutionsByCodeOrName(String code, Long codeTo, String codeType, String name, String nameTo, String nameType, Long financialInstitutionTypeId);
	FinancialInstitutionDTO getFinancialInstitution(Long financialInstitutionId);
    FinancialInstitutionAmountsDTO getAmounts(Long financialInstitutionId, boolean includePEFDTA);
    FinancialInstitutionDTO saveFinancialInstitution(FinancialInstitutionDTO financialInstitutionDTO, UserDTO userDTO) throws VbsCheckedException;
	List<ErrorDTO> validateFinancialInstitution(FinancialInstitutionDTO dto);
	String getCrmCompanyUrl();
	String getCrmCreateCompanyUrl();
	List<ContactFinancialInstitutionDTO> getContactsForFinancialInstitution(Long financialInstitutionId);
	void saveFinancialInstitutionContacts(List<ContactFinancialInstitutionDTO> financialInstitutionContactsDTO, UserDTO userDTO);
	void saveFinancialInstitutionMaaPoas(List<FinancialInstitutionMaaPoaDTO> financialInstitutionMaaPoasDTO, UserDTO userDTO, Long financialInstitutionId);
	List<ErrorDTO> validateFinancialInstitutionMaaPoa(FinancialInstitutionMaaPoaDTO dto, int maaPoaIndex);
	List<FinancialInstitutionMaaPoaDTO> getFinancialInstitutionMaaPoas(Long financialInstitutionId);
	void saveFinancialInstitutionAlerts(List<FinancialInstitutionAlertDTO> financialInstitutionAlertDTO, UserDTO userDTO, Long financialInstitutionId);
	List<FinancialInstitutionAlertDTO> getFinancialInstitutionAlerts(Long financialInstitutionId);
	String getContentNavigatorUrlForFinancialInstitution(Long financialInstitutionId);
	String getContentNavigatorUrl();

	FinancialInstitutionMaaPoaDTO getMaaPoa(Long securityId);

	FinancialInstitutionMaaPoaDTO getMaaPoa(SecurityDTO security);

}
