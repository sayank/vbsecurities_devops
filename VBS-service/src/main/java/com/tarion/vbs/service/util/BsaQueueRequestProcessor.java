/* 
 * 
 * BsaQueueRequestProcessor.java
 *
 * Copyright (c) 2019 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import javax.ejb.Local;

import com.tarion.vbs.service.jaxb.bsa.createcashsecurity.CreateCashSecurityRequest;




/**
 * Processing Responses from FMS Queue MDB 
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @date 2018-11-20
 * @version 1.0
 */
@Local
public interface BsaQueueRequestProcessor {
	
	public void processReceivedBsaXml(String xmlResponse);
	public void process(CreateCashSecurityRequest request, String xmlResponse);	

}
