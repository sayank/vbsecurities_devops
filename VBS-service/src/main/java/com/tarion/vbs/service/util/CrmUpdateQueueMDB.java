/* 
 * 
 * CrmUpdateQueueMDB.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.interceptor.Interceptors;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.tarion.vbs.common.constants.JMSConstants;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;

/**
 * Listens for JMS message from CRM on Crm Update Queue
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @date 2018-12-12
 * @version 1.0
 */
@Resource(name=JMSConstants.VBS_XA_CF_JNDI, type=javax.jms.ConnectionFactory.class)
@MessageDriven(mappedName = JMSConstants.CRM_UPDATE_QUEUE_NAME)
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class CrmUpdateQueueMDB implements MessageListener {
	@Resource
	public MessageDrivenContext mdc;
	
	@EJB
	private CrmQueueResponseProcessor crmQueueResponseProcessor;


	@Override
	public void onMessage(Message inMessage) {
		String inCaseMsgXML = null;
		try {
			if (inMessage instanceof TextMessage) {
				// skip any ping messages
				String messageType = inMessage.getStringProperty(JMSConstants.MESSAGE_TYPE);
				if (JMSConstants.PING.equalsIgnoreCase(messageType)) {
					String destNodes = inMessage.getStringProperty(JMSConstants.DESTINATION_NODES);
					LoggerUtil.logInfo(CrmUpdateQueueMDB.class, "Ping received from FMS for Destination Node[" + destNodes + "]");
					return;
				}
								
				TextMessage txtMsg = (TextMessage) inMessage;
				inCaseMsgXML = txtMsg.getText();
				if (inCaseMsgXML == null) {
					throw new VbsRuntimeException("Missing Renewals Submit to CRM Response Object");
				}
				crmQueueResponseProcessor.processReceivedCrmXml(inCaseMsgXML);
			} else {
				String warnMsg = "Received JMS Message from CRM that is not Text Message: " + inMessage.getClass().getName();
				LoggerUtil.logWarn(CrmUpdateQueueMDB.class, warnMsg);
				throw new VbsRuntimeException(warnMsg);
			}
		} catch (JMSException e) {
			throw new VbsRuntimeException(e.getMessage());
		} catch (VbsRuntimeException pie) {
			StringBuilder sb = new StringBuilder("CrmUpdateQueueMDB error for: [");
			sb.append(inCaseMsgXML).append(']');
			LoggerUtil.logError(CrmUpdateQueueMDB.class, sb.toString(), pie);
			throw new EJBException(pie);
		}
	}
	
}

