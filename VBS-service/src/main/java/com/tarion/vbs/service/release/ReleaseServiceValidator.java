package com.tarion.vbs.service.release;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.dto.release.ReleaseDTO;
import com.tarion.vbs.common.dto.release.ReleaseEnrolmentDTO;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.enums.ReleaseApprovalStatusEnum;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.release.ReleaseEntity;
import com.tarion.vbs.orm.entity.security.SecurityEntity;
import org.apache.commons.lang3.StringUtils;

import javax.interceptor.Interceptors;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Interceptors(EjbLoggingInterceptor.class)
class ReleaseServiceValidator {

	private static final String REQUESTED_AMOUNT = "requestedAmount";
	private static final String AUTHORIZED_RELEASE_AMOUNT = "authorizedReleaseAmount";
	private static final String ENROLMENTS_DOT = "enrolments.";
	private static final String DOT_REQUEST_AMOUNT = ".requestAmount";

	private ReleaseServiceValidator() {}

	static List<ErrorDTO> validateRelease(ReleaseDTO release, ReleaseEntity oldRelease, SecurityEntity security, BigDecimal totalRequestedAmount, BigDecimal totalAuthorizedReleasedAmount, BigDecimal releasableAmount, UserDTO userDTO) {
		//TODO add validation that the amount of the release being created (not update) is not greater than the releasableAmount(s) on SecurityEntity

		List<ErrorDTO> ret = new ArrayList<>();
		if(oldRelease == null || oldRelease.getFinalApprovalStatus() == null) {
			if(release.getReleaseType().getId() == VbsConstants.RELEASE_TYPE_RELEASE && security.getSecurityType().getId() == VbsConstants.SECURITY_TYPE_CASH && release.getRegularRelease() == null) {
				ErrorDTO err = new ErrorDTO("Select either Regular Release or Interest Only.", "regularRelease");
				ret.add(err);
			}

			if (release.getReasons() == null || release.getReasons().isEmpty()) {
				ErrorDTO err = new ErrorDTO("Reasons cannot be empty.", "reasons");
				ret.add(err);
			}


			if(release.getRequestedAmount() == null) {
				ErrorDTO err = new ErrorDTO("Total Request Amount Cannot be empty.", REQUESTED_AMOUNT);
				ret.add(err);
			}else if(release.getRequestedAmount().compareTo(BigDecimal.ZERO) == 0 && release.getEnrolments().isEmpty()){
				ErrorDTO err = new ErrorDTO("Total Request Amount Cannot be zero.", REQUESTED_AMOUNT);
				ret.add(err);
			}
			else if(release.getReleaseType().getId().longValue() != VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR && security.getAvailableAmount().compareTo(totalRequestedAmount) < 0){
				ErrorDTO err = new ErrorDTO("Total Request Amount Cannot be more than Current Available Amount.", REQUESTED_AMOUNT);
				ret.add(err);
			}else if(release.getReleaseType().getId().longValue() == VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR && (security.getAvailableAmount().add(security.getReleasableInterest())).compareTo(totalRequestedAmount) < 0){
				ErrorDTO err = new ErrorDTO("Total Request Amount Cannot be more than Current Available Amount plus available interest amount.", REQUESTED_AMOUNT);
				ret.add(err);
			}

			if (totalAuthorizedReleasedAmount != null && totalRequestedAmount != null && totalAuthorizedReleasedAmount.compareTo(totalRequestedAmount) > 0) {
				ErrorDTO err = new ErrorDTO("Total Authorized Release Amount Cannot be more than Requested Amount.", AUTHORIZED_RELEASE_AMOUNT);
				ret.add(err);
			}
			else {
				if(release.getReleaseType().getId().longValue() != VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR && totalAuthorizedReleasedAmount != null && oldRelease != null && oldRelease.getAuthorizedAmount() != null && oldRelease.getAuthorizedAmount().compareTo(totalAuthorizedReleasedAmount) != 0 && releasableAmount.compareTo(totalAuthorizedReleasedAmount) < 0){
					ErrorDTO err = new ErrorDTO("Authorized Release Amount cannot exceed the Security Current Amount less any releases waiting to be approved.", AUTHORIZED_RELEASE_AMOUNT);
					ret.add(err);
				}else if(release.getReleaseType().getId().longValue() == VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR && totalAuthorizedReleasedAmount != null && oldRelease != null && oldRelease.getAuthorizedAmount() != null && oldRelease.getAuthorizedAmount().compareTo(totalAuthorizedReleasedAmount) != 0 && releasableAmount.add(security.getReleasableInterest()).compareTo(totalAuthorizedReleasedAmount) < 0){
					ErrorDTO err = new ErrorDTO("Authorized Release Amount cannot exceed the Security Current Amount less any releases waiting to be approved.", AUTHORIZED_RELEASE_AMOUNT);
					ret.add(err);
				}
				else if (security.getSecurityType().getId() == VbsConstants.SECURITY_TYPE_CASH  && oldRelease != null && oldRelease.getAuthorizedAmount() != null && oldRelease.getAuthorizedAmount().compareTo(totalAuthorizedReleasedAmount) != 0 && Boolean.FALSE.equals(release.getRegularRelease()) && release.getReleaseType().getId() == VbsConstants.RELEASE_TYPE_RELEASE && security.getAvailableInterest() != null && totalAuthorizedReleasedAmount != null && security.getAvailableInterest().compareTo(totalAuthorizedReleasedAmount) < 0) {
					ErrorDTO err = new ErrorDTO("Total Authorized Release Amount Cannot be more than the Total Interest Amount.", AUTHORIZED_RELEASE_AMOUNT);
					ret.add(err);
				}
				else if (security.getSecurityType().getId() == VbsConstants.SECURITY_TYPE_CASH  && oldRelease != null && oldRelease.getAuthorizedAmount() != null && oldRelease.getAuthorizedAmount().compareTo(totalAuthorizedReleasedAmount) != 0 && Boolean.FALSE.equals(release.getRegularRelease()) && release.getReleaseType().getId()  == VbsConstants.RELEASE_TYPE_RELEASE && security.getReleasableInterest() != null && totalAuthorizedReleasedAmount != null && security.getReleasableInterest().compareTo(totalAuthorizedReleasedAmount) < 0) {
					ErrorDTO err = new ErrorDTO("Authorized Release Amount cannot exceed the Security Current Amount less any releases waiting to be approved.", AUTHORIZED_RELEASE_AMOUNT);
					ret.add(err);
				}
			}

			if (release.getAnalystApprovalStatus() != null && release.getAnalystApprovalStatus().equals(ReleaseApprovalStatusEnum.Authorized) && release.getAuthorizedReleaseAmount() == null) {
				ErrorDTO err = new ErrorDTO("Authorized Amount can not be empty.", AUTHORIZED_RELEASE_AMOUNT);
				ret.add(err);
			}

			if (security.getSecurityType().getId() == VbsConstants.SECURITY_TYPE_CASH && Boolean.FALSE.equals(release.getRegularRelease()) && release.getReleaseType().getId() == VbsConstants.RELEASE_TYPE_RELEASE  && release.getEnrolments().isEmpty() && security.getAvailableInterest() != null && totalRequestedAmount != null && security.getAvailableInterest().compareTo(totalRequestedAmount) < 0) {
				ErrorDTO err = new ErrorDTO("Request Amount cannot be more than the Total Interest Amount of " + VbsUtil.getCurrencyFormattedString(security.getAvailableInterest()), REQUESTED_AMOUNT);
				ret.add(err);
			}

			if (ReleaseApprovalStatusEnum.Authorized.equals(release.getAnalystApprovalStatus())) {
				if (release.getFinalApprovalSentTo() == null) {
					ErrorDTO err = new ErrorDTO("When Analyst Status is Approved, Request Approval Send To cannot be empty.", "finalApprovalSentTo");
					ret.add(err);
				}
			}

			if (ReleaseApprovalStatusEnum.Rejected.equals(release.getFinalApprovalStatus()) && !userDTO.hasPermission(VbsConstants.CREATE_DEMAND_FINANCE)) {
				if (release.getManagerRejectReason() == 0L) {
					ErrorDTO err = new ErrorDTO("When Manager Approval Status is Rejected, Manager Reject Reason cannot be empty.", "managerRejectReason");
					ret.add(err);
				}
			}

			if ((release.getEnrolments() == null || release.getEnrolments().size() == 0) && release.isWsInputRequested()) {
				ErrorDTO err = new ErrorDTO("You must select a freehold enrolment when warranty service Input requested is checked.", "wsInputRequested");
				ret.add(err);
			}
			else if (release.getEnrolments().size() > 1 && release.isWsInputRequested()) {
				ErrorDTO err = new ErrorDTO("Only one enrolment can be selected when warranty service Input requested is checked.", "wsInputRequested");
				ret.add(err);
			}

			if (release.getEnrolments().isEmpty()) {
				if (ReleaseApprovalStatusEnum.Rejected.equals(release.getAnalystApprovalStatus())) {
					if (release.getAnalystRejectReason() == 0L) {
						ErrorDTO err = new ErrorDTO("Analyst Approval Status is Rejected, Analyst Reject Reason cannot be empty.", "analystRejectReason");
						ret.add(err);
					}
				}
			}

			if((release.getPayTo() != null && !StringUtils.isEmpty(release.getPayTo().getCrmContactId())) &&
					StringUtils.isEmpty(release.getPayTo().getCompanyName()) && StringUtils.isEmpty(release.getPayTo().getFirstName()) && StringUtils.isEmpty(release.getPayTo().getLastName())) {
				ErrorDTO err = new ErrorDTO("Pay To Contact not found.", "payTo");
				ret.add(err);
			}

			if((release.getPayTo() != null && (security.getSecurityType().getId() == VbsConstants.SECURITY_TYPE_CASH || release.getReleaseType().equals(VbsConstants.RELEASE_TYPE_DEMAND_COLLECTOR))&& !StringUtils.isEmpty(release.getPayTo().getCrmContactId())) &&
					(!StringUtils.isEmpty(release.getPayTo().getCompanyName()) || !StringUtils.isEmpty(release.getPayTo().getFirstName()) || !StringUtils.isEmpty(release.getPayTo().getLastName())) &&
					release.getPayTo().getApVendorFlag() != true) {
				ErrorDTO err = new ErrorDTO("Pay To Contact is not AP vendor.", "payTo");
				ret.add(err);
			}
		}

		return ret;
	}

	static List<ErrorDTO> validateReleaseEnrolments(List<ReleaseEnrolmentDTO> releaseEnrolments, ReleaseEntity oldRelease,  BigDecimal totalAuthorizedReleasedAmount, BigDecimal currentAvailableAmount, BigDecimal releasableAmount) {

		List<ErrorDTO> ret = new ArrayList<>();
		if (!releaseEnrolments.isEmpty()) {
			if(oldRelease != null && oldRelease.getId() == null || (oldRelease != null && oldRelease.getAuthorizedAmount() != null && totalAuthorizedReleasedAmount != null && oldRelease.getAuthorizedAmount().compareTo(totalAuthorizedReleasedAmount) != 0)){
				BigDecimal enrolmentsRequestTotal = BigDecimal.ZERO;
				for (int i = 0; i < releaseEnrolments.size(); i++) {
					if (releaseEnrolments.get(i).getRequestAmount() == null) {
						ErrorDTO err = new ErrorDTO("Request Amount cannot be empty.", ENROLMENTS_DOT + i + DOT_REQUEST_AMOUNT);
						ret.add(err);
					} else if (releaseEnrolments.get(i).getRequestAmount().compareTo(BigDecimal.ZERO) == 0) {
						ErrorDTO err = new ErrorDTO("Request Amount cannot be zero.", ENROLMENTS_DOT + i + DOT_REQUEST_AMOUNT);
						ret.add(err);
					} else if (releaseEnrolments.get(i).getRequestAmount().compareTo(currentAvailableAmount) > 0) {
						ErrorDTO err = new ErrorDTO("Request Amount cannot be greater than the Current Available Amount.", ENROLMENTS_DOT + i + DOT_REQUEST_AMOUNT);
						ret.add(err);
					}

					if (releaseEnrolments.get(i).getAnalystApprovalStatus() != null && releaseEnrolments.get(i).getAnalystApprovalStatus().equals(ReleaseApprovalStatusEnum.Authorized) && releaseEnrolments.get(i).getAuthorizedAmount() == null) {
						ErrorDTO err = new ErrorDTO("Authorized Released Amount cannot be empty.", ENROLMENTS_DOT + i + ".authorizedAmount");
						ret.add(err);
					} else if (releaseEnrolments.get(i).getAuthorizedAmount() != null && releaseEnrolments.get(i).getAuthorizedAmount().compareTo(releaseEnrolments.get(i).getRequestAmount()) > 0) {
						ErrorDTO err = new ErrorDTO("Authorized Released Amount cannot greater than Request Amount.", ENROLMENTS_DOT + i + ".authorizedAmount");
						ret.add(err);
					} else if (totalAuthorizedReleasedAmount != null && oldRelease != null && oldRelease.getAuthorizedAmount() != null && oldRelease.getAuthorizedAmount().compareTo(totalAuthorizedReleasedAmount) != 0 && releasableAmount.compareTo(totalAuthorizedReleasedAmount) < 0) {
						ErrorDTO err = new ErrorDTO("Authorized Release Amount cannot exceed the Security Current Amount less any releases waiting to be approved", ENROLMENTS_DOT + i + ".authorizedAmount");
						ret.add(err);
					}
					if (ReleaseApprovalStatusEnum.Rejected.equals(releaseEnrolments.get(i).getAnalystApprovalStatus())) {
						if (releaseEnrolments.get(i).getRejectReason() == null) {
							ErrorDTO err = new ErrorDTO("When Analyst Approval Status is Rejected, Analyst Reject Reason cannot be empty.", ENROLMENTS_DOT + i + ".rejectReason");
							ret.add(err);
						}
					}
					enrolmentsRequestTotal = enrolmentsRequestTotal.add(releaseEnrolments.get(i).getRequestAmount());
				}

				if (ret.isEmpty() && enrolmentsRequestTotal.compareTo(currentAvailableAmount) > 0) {
					for (int i = 0; i < releaseEnrolments.size(); i++) {
						ErrorDTO err = new ErrorDTO("", ENROLMENTS_DOT + i + DOT_REQUEST_AMOUNT);
						ret.add(err);
					}
					ErrorDTO err = new ErrorDTO("The total Request Amount cannot be greater than the Current Available Amount", "enrolments");
					ret.add(err);
				}
			}
		}
		return ret;
	}
}
