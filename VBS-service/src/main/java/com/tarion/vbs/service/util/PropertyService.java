/* 
 * 
 * PropertyService.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import javax.ejb.Local;

/**
 * Session Bean to Access the Properties File table.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-01-03
 * @version 1.0
 */
@Local
public interface PropertyService extends PropertyServiceRemote {

    String getValueOrNull(String key);

    public void setProperty(String key, String value);


}
