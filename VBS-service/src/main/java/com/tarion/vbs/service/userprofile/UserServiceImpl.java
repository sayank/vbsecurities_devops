package com.tarion.vbs.service.userprofile;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import com.tarion.vbs.common.dto.user.PermissionDTO;
import com.tarion.vbs.common.dto.user.RoleDTO;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.exceptions.VbsDirectoryEnvironmentException;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.dao.userprofile.UserDAO;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.GenericDAO;
import com.tarion.vbs.orm.entity.userprofile.PermissionEntity;
import com.tarion.vbs.orm.entity.userprofile.RoleEntity;
import com.tarion.vbs.orm.entity.userprofile.RolePermissionEntity;
import com.tarion.vbs.orm.entity.userprofile.UserRoleEntity;

@Stateless(mappedName = "ejb/UserService")
@Interceptors(EjbLoggingInterceptor.class)
public class UserServiceImpl implements UserService, UserServiceRemote {
	
	@EJB
	private UserDAO userDAO;
	@EJB
	private AdSecurityService adSecurityService;
	@EJB
	private GenericDAO genericDAO;
	
	@Override
	public RoleDTO getRoleById(Long id) {
		RoleEntity entity = userDAO.getRoleById(id);
		return populateRoleDTO(entity);
	}

	@Override
	public List<RoleDTO> getAllRoles() {
		List<RoleEntity> entityList = userDAO.getAllRoles();
		return entityList.stream()
				.map(this::populateRoleDTO)
				.collect(Collectors.toList());
	}

	@Override
	public List<RoleDTO> getRolesForUser(String userId) {
		List<RoleEntity> entityList = userDAO.getRolesForUser(userId);
		return entityList.stream()
				.map(this::populateRoleDTO)
				.collect(Collectors.toList());
	}

	@Override
	public RoleDTO createRole(RoleDTO role, UserDTO actionUser) {
		RoleEntity entity = new RoleEntity();
		entity.setDescription(role.getDescription());
		entity.setName(role.getName());
		entity = userDAO.createRole(entity);
		return populateRoleDTO(entity);
	}

	@Override
	public RoleDTO updateRole(RoleDTO role, UserDTO actionUser) {
		if(role != null && role.getId() != null) {
			RoleEntity entity = userDAO.getRoleById(role.getId());
			if(entity != null) {
				entity.setDescription(role.getDescription());
				entity.setName(role.getName());
				entity = userDAO.updateRole(entity);
				return populateRoleDTO(entity);
			}
		}
		throw new VbsRuntimeException("Failed to update Role");
	}

	@Override
	public void deleteRole(Long roleId, UserDTO actionUser) {
		if(roleId != null) {
			RoleEntity entity = userDAO.getRoleById(roleId);
			entity.setUpdateDate(LocalDateTime.now());
			entity.setUpdateUser(actionUser.getUserId());
			genericDAO.removeEntity(entity, actionUser.getUserId(), LocalDateTime.now());
		}
		throw new VbsRuntimeException("Passed null roleId");
	}

	@Override
	public RoleDTO addPermissionToRole(Long permissionId, Long roleId, UserDTO actionUser) {
		RolePermissionEntity rpe = new RolePermissionEntity();
		rpe.setPermissionId(permissionId);
		rpe.setRoleId(roleId);
		userDAO.addPermissionToRole(rpe);
		return getRoleById(roleId);
	}

	@Override
	public RoleDTO removePermissionFromRole(Long permissionId, Long roleId, UserDTO actionUser) {
		userDAO.removePermissionFromRole(permissionId, roleId, actionUser);
		return getRoleById(roleId);
	}

	@Override
	public PermissionDTO getPermissionById(Long id) {
		PermissionEntity entity = userDAO.getPermissionById(id);
		return populatePermissionDTO(entity);
	}

	@Override
	public List<PermissionDTO> getAllPermissions() {
		List<PermissionEntity> entityList = userDAO.getAllPermissions();
		return entityList.stream()
				.map(this::populatePermissionDTO)
				.collect(Collectors.toList());
	}

	@Override
	public List<PermissionDTO> getPermissionsByRole(Long roleId) {
		List<PermissionEntity> entityList = userDAO.getPermissionsByRole(roleId);
		return entityList.stream()
				.map(this::populatePermissionDTO)
				.collect(Collectors.toList());
	}

	@Override
	public List<PermissionDTO> getAvailablePermissionsByRole(Long roleId) {
		List<PermissionEntity> allPermissions = userDAO.getAllPermissions();
		List<PermissionEntity> rolePermissions = userDAO.getPermissionsByRole(roleId);
		List<PermissionEntity> available = allPermissions.stream()
	            .filter(e -> !rolePermissions.contains(e))
	            .collect(Collectors.toList());
		
		return available.stream()
				.map(this::populatePermissionDTO)
				.collect(Collectors.toList());
	}

	@Override
	public List<PermissionDTO> getPermissionsForUser(String userId) {
		List<PermissionEntity> entityList = userDAO.getPermissionsForUser(userId);
		return entityList.stream()
				.map(this::populatePermissionDTO)
				.collect(Collectors.toList());
	}

	@Override
	public List<UserDTO> getUsersByPermissionId(Long permissionId) throws VbsDirectoryEnvironmentException {
		List<String> userList = userDAO.getUsersByPermissionId(permissionId);
		return getUserDTOsForUserList(userList);
	}

	@Override
	public List<UserDTO> getUsersByPermission(PermissionDTO permission) throws VbsDirectoryEnvironmentException {
		if(permission == null || permission.getId() == null) {
			return new ArrayList<>();
		}
		return getUsersByPermissionId(permission.getId());
	}

	@Override
	public List<UserDTO> getUsersByPermissionName(String permissionName) throws VbsDirectoryEnvironmentException {
		List<String> userList = userDAO.getUsersByPermissionName(permissionName);
		return getUserDTOsForUserList(userList);
	}

	@Override
	public PermissionDTO updatePermission(PermissionDTO permission) {
		if(permission == null || permission.getId() == null) {
			return permission;
		}else {
			PermissionEntity entity =  userDAO.getPermissionById(permission.getId());
			entity.setDescription(permission.getDescription());
			entity = userDAO.updatePermission(entity);
			return populatePermissionDTO(entity);
		}
	}

	@Override
	public UserDTO getUserById(String userId) throws VbsDirectoryEnvironmentException {
		List<UserDTO> userList = adSecurityService.getAllADUsers();
		Optional<UserDTO> opt = userList.stream().filter(u -> u.getUserId().equalsIgnoreCase(userId)).findFirst();
		return opt.orElse(null);
	}

	@Override
	public List<UserDTO> getUsersByRole(RoleDTO role) throws VbsDirectoryEnvironmentException {
		List<String> roleUsers = userDAO.getUsersByRole(role.getId());
		return getUserDTOsForUserList(roleUsers);
	}

	@Override
	public List<UserDTO> getUsersByRoleName(String roleName) throws VbsDirectoryEnvironmentException {
		List<String> roleUsers = userDAO.getUsersByRoleName(roleName);
		return getUserDTOsForUserList(roleUsers);
	}

	@Override
	public UserDTO addUserToRole(String userId, Long roleId, UserDTO actionUser) throws VbsDirectoryEnvironmentException {
		UserRoleEntity ure = new UserRoleEntity();
		ure.setRoleId(roleId);
		ure.setUserId(userId.toUpperCase());
		userDAO.addUserToRole(ure);
		return getUser(userId);
	}

	@Override
	public UserDTO removeUserFromRole(String userId, Long roleId, UserDTO actionUser) throws VbsDirectoryEnvironmentException {
		UserRoleEntity ure = new UserRoleEntity();
		ure.setRoleId(roleId);
		ure.setUserId(userId.toUpperCase());
		genericDAO.removeEntity(ure, actionUser.getUserId(), LocalDateTime.now());
		return getUser(userId);
	}

	@Override
	public List<UserDTO> getNewUserList() throws VbsDirectoryEnvironmentException {
		List<String> userList = userDAO.getUsersWithAnyRole();
		List<UserDTO> users = adSecurityService.getAllADUsers();
		return users.stream()
	            .filter(e -> !userList.contains(e.getUserId()))
	            .collect(Collectors.toList());
	}
	
	@Override
	public List<UserDTO> getAllUserList() throws VbsDirectoryEnvironmentException {
		List<String> userList = userDAO.getUsersWithAnyRole();
		List<UserDTO> users = adSecurityService.getAllADUsers();
		List<UserDTO> newUsers = new ArrayList<>();
		for (UserDTO user : users) {
			for (String username : userList) {
				if (user.getUserId().equalsIgnoreCase(username)) {
					newUsers.add(user);
					break;
				}
			}
		}

		return newUsers;
	}

	@Override
	public UserDTO getUser(String userId) throws VbsDirectoryEnvironmentException {
		UserDTO user = adSecurityService.getUser(userId);
		if(user != null) {
			user.setPermissions(getPermissionsForUser(user.getUserId()));
		}
		return user;
	}
		
	private List<UserDTO> getUserDTOsForUserList(List<String> userList) throws VbsDirectoryEnvironmentException {
		List<UserDTO> users = adSecurityService.getAllADUsers();
		return users.stream()
	            .filter(e -> userList.stream().anyMatch(s -> s.equalsIgnoreCase(e.getUserId())))
	            .collect(Collectors.toList());
	}
	
	private RoleDTO populateRoleDTO(RoleEntity entity) {
		RoleDTO dto = new RoleDTO();
		dto.setDescription(entity.getDescription());
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setPermissions(getPermissionsByRole(entity.getId()));
		return dto;
	}
	
	private PermissionDTO populatePermissionDTO(PermissionEntity entity) {
		PermissionDTO dto = new PermissionDTO();
		dto.setDescription(entity.getDescription());
		dto.setId(entity.getId());
		dto.setPermissionName(entity.getName());
		return dto;
	}

}