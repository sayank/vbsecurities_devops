/* 
 * 
 * AdSecurityBeanRemote.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.userprofile;

import javax.ejb.Remote;

import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;

/**
 * Ad Service provides operations against Active Directory
 */
@Remote
public interface AdSecurityServiceRemote {
	public UserDTO authenticate(String userId, String password) throws VbsCheckedException;
}
