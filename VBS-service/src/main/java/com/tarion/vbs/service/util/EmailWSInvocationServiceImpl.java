/* 
 * 
 * CrmWSInvocationServiceImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import com.tarion.tbi.esp.service.EmailSender;
import com.tarion.tbi.esp.service.EmailSenderWSBean;
import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.EmailTransactionLogDAO;
import com.tarion.vbs.orm.entity.EmailTransactionLogEntity;
import com.tarion.vbs.service.jaxb.email.EmailAttachmentDto;
import com.tarion.vbs.service.jaxb.email.EmailAttachmentListDto;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.xml.namespace.QName;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;



/**
 * Utility Bean to invoke CRM Web Service Client
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-01-31
 * @version 1.0
 */
@Stateless(mappedName = "ejb/EmailWSInvocationService")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged -- removed because of pointless log spam
public class EmailWSInvocationServiceImpl implements EmailWSInvocationService {
	
	private EmailSenderWSBean emailSenderWebService = null;
	
	@EJB
	private PropertyService propertyService;
	@EJB
	private EmailTransactionLogDAO emailTransactionLogDAO;
	
	@Override
	public void sendEmailUsingEsp(String subject, String body, String fromAddress, String toAddresses) throws VbsCheckedException {
		LocalDateTime sentDate = DateUtil.getLocalDateTimeNow();
		if(toAddresses != null && !toAddresses.equalsIgnoreCase("")) {
			try {
				saveEmailTransactionLog(subject, body, fromAddress, toAddresses, null, sentDate, null, VbsConstants.SENT_SUCCESS);
				getEmailSenderWebService().sendEmailWithText(subject, body, fromAddress, toAddresses, null, null, false, null, null, null);
			} catch (Exception e) {
				String exceptionMessage = "Exception occured while invoking TBI Web Service EmailSender.sendEmailWithText";
				LoggerUtil.logError(EmailWSInvocationServiceImpl.class, exceptionMessage, e);
				saveEmailTransactionLog(subject, body, fromAddress, toAddresses, null, sentDate, exceptionMessage, VbsConstants.SENT_FAILURE);
				throw new VbsCheckedException(e.getMessage(), e);
			}
		}
	}
	
	@Override
	public void sendEmailUsingEspWithFile(String subject, String body, String fromAddress, String toAddresses, byte[] emailAttachment, String fileName, String mimeType) throws VbsCheckedException {
		if (toAddresses != null && !toAddresses.equalsIgnoreCase("")) {

			LocalDateTime sentDate = DateUtil.getLocalDateTimeNow();
			EmailAttachmentDto attachmentDto = new EmailAttachmentDto();
			attachmentDto.setDocData(emailAttachment);
			attachmentDto.setFilename(fileName);
			attachmentDto.setMimeFileType(mimeType);
			List<EmailAttachmentDto> list = new ArrayList<>();
			list.add(attachmentDto);
			try {
				getEmailSenderWebService().sendEmailWithFile(subject, body, fromAddress, toAddresses, null, null, emailAttachment, fileName, mimeType, false, null, null, null);
				saveEmailTransactionLog(subject, body, fromAddress, toAddresses, list, sentDate, null, VbsConstants.SENT_SUCCESS);
			} catch (Exception e) {
				String exceptionMessage = "Exception occured while invoking TBI Web Service EmailSender.sendEmailWithFile";
				LoggerUtil.logError(EmailWSInvocationServiceImpl.class, exceptionMessage, e);
				saveEmailTransactionLog(subject, body, fromAddress, toAddresses, list, sentDate, exceptionMessage, VbsConstants.SENT_FAILURE);
				throw new VbsCheckedException(e.getMessage(), e);
			}
		}
	}
	@Override
	public void sendEmailUsingEspWithListOfFiles(String subject, String body, String fromAddress, String toAddresses, List<EmailAttachmentDto> emailAttachments) throws VbsCheckedException {
		if (toAddresses != null && !toAddresses.equalsIgnoreCase("")) {

			LocalDateTime sentDate = DateUtil.getLocalDateTimeNow();
			try {
				List<com.tarion.tbi.esp.emailattachment.EmailAttachmentDto> webServiceList = new ArrayList<>();
				for (EmailAttachmentDto dto : emailAttachments) {
					com.tarion.tbi.esp.emailattachment.EmailAttachmentDto webServiceDto = new com.tarion.tbi.esp.emailattachment.EmailAttachmentDto();
					webServiceDto.setDocData(dto.getDocData());
					webServiceDto.setFilename(dto.getFilename());
					webServiceDto.setMimeFileType(dto.getMimeFileType());
					webServiceList.add(webServiceDto);
				}
				getEmailSenderWebService().sendEmailWithListOfFiles(subject, body, fromAddress, toAddresses, null, null, webServiceList, false, null, null, null);
				saveEmailTransactionLog(subject, body, fromAddress, toAddresses, emailAttachments, sentDate, null, VbsConstants.SENT_SUCCESS);
			} catch (Exception e) {
				String exceptionMessage = "Exception occured while invoking TBI Web Service EmailSender.sendEmailUsingEspWithListOfFiles";
				LoggerUtil.logError(EmailWSInvocationServiceImpl.class, exceptionMessage, e);
				saveEmailTransactionLog(subject, body, fromAddress, toAddresses, emailAttachments, sentDate, exceptionMessage, VbsConstants.SENT_FAILURE);
				throw new VbsCheckedException(e.getMessage(), e);
			}
		}
	}
	private void saveEmailTransactionLog(String subject, String body, String fromAddress, String toAddresses, List<EmailAttachmentDto> emailAttachments, LocalDateTime sentDate, String errorMessage, String status) {
		if (toAddresses != null && !toAddresses.equalsIgnoreCase("")) {
			EmailTransactionLogEntity entity = new EmailTransactionLogEntity();
			entity.setBody(body);
			entity.setCreateDate(sentDate);
			entity.setCreateUser(VbsConstants.VBS_SYSTEM_USER_ID);
			if (emailAttachments != null && !emailAttachments.isEmpty()) {
				EmailAttachmentListDto attachmentList = new EmailAttachmentListDto();
				attachmentList.getEmailAttachmentList().addAll(emailAttachments);
				String emailAttachmentsXml = VbsUtil.convertJaxbToXml(attachmentList, EmailAttachmentListDto.class);
				entity.setEmailAttachmentsXml(emailAttachmentsXml);
			}
			entity.setEmailSentDate(sentDate);
			String shortenedExceptionText = null;
			if (errorMessage != null) {
				if (errorMessage.length() < VbsConstants.EMAIL_MAX_ERROR_TEXT) {
					shortenedExceptionText = errorMessage;
				} else {
					shortenedExceptionText = errorMessage.substring(0, VbsConstants.EMAIL_MAX_ERROR_TEXT - 1);
				}
			}
			entity.setErrorMessage(shortenedExceptionText);
			entity.setFromAddress(fromAddress);
			entity.setStatus(status);
			entity.setSubject(subject);
			entity.setToAddresses(toAddresses);
			entity.setTrackingNumber(JmsMessageTypeEnum.getEmailSentMailTracking());

			emailTransactionLogDAO.addEntity(entity, VbsConstants.VBS_SYSTEM_USER_ID, sentDate);
		}
	}
//	@Override
//	public SubmissionResponse sendEmailUsingEspUploadToCm(String subject, String body, String fromAddress, String toAddresses, List<EmailCmAttribute> emailCmAttributeList, String itemType, String pdfConversionTemplateId) throws VbsCheckedException {
//		SubmissionResponse response = null;
//		try {
//			response = getEmailSenderWebService().sendEmailWithText(subject, body, fromAddress, toAddresses, null, null, true, emailCmAttributeList, itemType, pdfConversionTemplateId);
//		} catch (Exception e) {
//			String exceptionMessage = "Exception occured while invoking TBI Web Service EmailSender.sendEmailUsingEspUploadToCm";
//			LoggerUtil.logError(EmailWSInvocationServiceImpl.class, exceptionMessage, e);
//			throw new VbsCheckedException(e.getMessage(), e);
//		}
//		
//		return response;
//	}

	private EmailSenderWSBean getEmailSenderWebService() throws MalformedURLException {
		if (emailSenderWebService == null) {
			String wsdlUrlString = propertyService.getValue(VbsConstants.ESP_EMAIL_WEB_SERVICE_WSDL_URL);
			URL webServiceUrl = new URL(wsdlUrlString);
			QName serviceName = new QName("http://service.esp.tbi.tarion.com/", "EmailSender");

			EmailSender service = new EmailSender(webServiceUrl, serviceName);
			emailSenderWebService = service.getEmailSenderWSBeanPort();
		}
		return emailSenderWebService;
	}
}
