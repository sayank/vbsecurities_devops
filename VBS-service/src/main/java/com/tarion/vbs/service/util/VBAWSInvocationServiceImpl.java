/* 
 * 
 * CrmWSInvocationServiceImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.xml.namespace.QName;

import com.tarion.vbapplications.service.application.VBApplication;
import com.tarion.vbapplications.service.application.VBApplicationWS;
import com.tarion.vbapplications.service.application.VbNumberStatusIdDTO;
import com.tarion.vbapplications.service.vbs.VBApplicationsVbs;
import com.tarion.vbapplications.service.vbs.VBApplicationsVbsWS;
import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.exceptions.VbsCheckedException;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;


/**
 * Utility Bean to invoke VBA Web Service Client
 *
 */
@Stateless(mappedName = "ejb/VBAWSInvocationService")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged -- removed because of pointless log spam
public class VBAWSInvocationServiceImpl implements VBAWSInvocationService {

	private VBApplication vbaWebService = null;
	private VBApplicationsVbsWS vbaVbsWebService = null;

	@EJB
	private PropertyService propertyService;

	@Override
	public List<VbNumberStatusIdDTO>  getApplicationStatusForVBs(List<String> vbNumbers) throws VbsCheckedException {
		try {
			return getVbaWebService().findApplicationStatusForVBs(vbNumbers);
		} catch (Exception e) {
			String exceptionMessage = "Exception occurred while invoking VBA Web Service VBApplication.findApplicationStatusForVBs";
			LoggerUtil.logError(VBAWSInvocationServiceImpl.class, exceptionMessage, e);
			throw new VbsCheckedException(e.getMessage(), e);
		}

	}

	private VBApplication getVbaWebService() throws MalformedURLException {
		if (vbaWebService == null) {
			String wsdlUrlString = propertyService.getValue(VbsConstants.VBA_WEB_SERVICE_WSDL_URL );
			URL webServiceUrl = new URL(wsdlUrlString);
			QName serviceName = new QName("http://application.service.vbapplications.tarion.com/", "VBApplicationWS");

			VBApplicationWS service = new VBApplicationWS(webServiceUrl, serviceName);
			vbaWebService = service.getVBApplicationWSImplPort();
		}
		return vbaWebService;
	}
	
	private VBApplicationsVbsWS getVbaVbsWebService() throws MalformedURLException {
		if (vbaVbsWebService == null) {
			String wsdlUrlString = propertyService.getValue(VbsConstants.VBA_VBS_WEB_SERVICE_WSDL_URL );
			URL webServiceUrl = new URL(wsdlUrlString);
			QName serviceName = new QName("http://vbs.service.vbapplications.tarion.com/", "VBApplicationsVbs");

			VBApplicationsVbs service = new VBApplicationsVbs(webServiceUrl, serviceName);
			vbaVbsWebService = service.getVBApplicationsVbsWSPort();
		}
		return vbaVbsWebService;
	}
	
	@Override
	public String  getVbApplicationAnalyst(String vbNumber) throws VbsCheckedException {
		try {
			return getVbaVbsWebService().getAnalyst(vbNumber);
		} catch (Exception e) {
			String exceptionMessage = "Exception occurred while invoking VBA Vbs Web Service VBApplicationsVbsWS.findUser";
			LoggerUtil.logError(VBAWSInvocationServiceImpl.class, exceptionMessage, e);
			throw new VbsCheckedException(e.getMessage(), e);
		}
	}

}
