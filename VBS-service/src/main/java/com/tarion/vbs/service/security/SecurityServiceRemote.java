/* 
 * 
 * SecurityServiceRemote.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.security;

import com.tarion.vbs.common.dto.ErrorDTO;
import com.tarion.vbs.common.dto.alert.AlertDTO;
import com.tarion.vbs.common.dto.enrolment.CEEnrolmentDecisionDTO;
import com.tarion.vbs.common.dto.enrolment.EnrolmentPoolActivityDTO;
import com.tarion.vbs.common.dto.security.*;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.exceptions.VbsCheckedException;

import javax.ejb.Remote;
import java.util.List;


/**
 * Security Service provides operations for Securities tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-15
 * @version 1.0
 */
@Remote
public interface SecurityServiceRemote {

	SecuritySearchResultWrapper searchSecurity(SecuritySearchParametersDTO searchParametersDTO, UserDTO userDTO);

	SecurityDTO getSecurity(Long securityId);

	Long saveSecurity(SecurityDTO securityDTO, List<String> displayedAlertsConfirmations, UserDTO userDTO);

	List<HistoryFieldDTO> getSecurityCommentHistory(Long securityId);

	List<ErrorDTO> validateDeleteSecurity(Long securityId);

	void deleteSecurity(Long securityId, UserDTO userDTO) throws VbsCheckedException;

	List<ErrorDTO> poolSecurities(PoolSecuritiesParametersDTO poolSecuritiesParametersDTO, UserDTO userDTO) throws VbsCheckedException;

    List<String> getAllInstrumentNumbersLike(String instrumentNumber);

	List<SecurityDTO> getAllSecuritiesByInstrumentNumbersLike(String instrumentNumber);

    void validateAlert(AlertDTO alertDTO, List<ErrorDTO> errors, int index);

	List<SecuritySearchResultDTO> searchDeletedSecurity(DeletedSecuritySearchParametersDTO deletedSearchParametersDTO, UserDTO userDTO);
	List<EnrolmentPoolActivityDTO> getDeletedEnrolmentsActivities(Long securityId);
	List<EnrolmentPoolActivityDTO> getDeletedBbActivities(Long poolId);

    CEEnrolmentDecisionDTO getOutstandingDecisionByEnrolmentNumber(String enrolmentNumber);

	void updateCEEnrolmentDecision(CEEnrolmentDecisionDTO decisionDTO, UserDTO userDTO);
	void sendDtaVbApplicationAnalystEmail(Long securityId) throws VbsCheckedException;
}
