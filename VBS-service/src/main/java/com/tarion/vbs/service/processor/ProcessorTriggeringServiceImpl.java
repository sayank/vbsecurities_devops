package com.tarion.vbs.service.processor;

import com.tarion.vbs.common.dto.enrolment.CEEnrolmentDecisionDTO;
import com.tarion.vbs.common.dto.security.SecurityDTO;
import com.tarion.vbs.common.dto.user.UserDTO;
import com.tarion.vbs.common.enums.ProcessorTriggerTypeEnum;
import com.tarion.vbs.common.util.DateUtil;
import com.tarion.vbs.common.util.VbsUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.orm.entity.enrolment.EnrolmentEntity;
import com.tarion.vbs.service.jaxb.autorelease.*;
import com.tarion.vbs.service.jaxb.email.allocation.AllocationEmailRequest;
import com.tarion.vbs.service.jaxb.email.ceBlanket.CeBlanketEmailRequest;
import com.tarion.vbs.service.jaxb.interest.CreateInterestCalculation;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.interceptor.Interceptors;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * This class is used to post a message to an internal JMS queue to trigger processing events
 *
 * @author <A href="joni.paananen@tarion.com">Joni Paananen</A>
 */
@Singleton(mappedName = "ejb/ProcessorTriggeringService")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class ProcessorTriggeringServiceImpl implements  ProcessorTriggeringService{

    @EJB
    InternalJmsMessageSenderService jmsMessageSenderService;

    @Override
    public void triggerAutoReleaseGetVBApplicationStatus(Long autoReleaseRunId, List<String> vbNumbers) {

        AutoReleaseVBApplicationStatusRequest request = new AutoReleaseVBApplicationStatusRequest();
        request.setAutoReleaseRunId(autoReleaseRunId);
        request.getVbNumbers().addAll(vbNumbers);
        String messageText = VbsUtil.convertJaxbToXml(request, AutoReleaseVBApplicationStatusRequest.class);
        jmsMessageSenderService.sendTriggerMessage(ProcessorTriggerTypeEnum.ACTION_AUTO_RELEASE_GET_VB_APPLICATION_STATUS, messageText);

    }

    @Override
    public void triggerCheckDataReceived(Long autoReleaseRunId) {

        AutoReleaseCheckDataReceivedRequest request = new AutoReleaseCheckDataReceivedRequest();
        request.setAutoReleaseRunId(autoReleaseRunId);
        String messageText = VbsUtil.convertJaxbToXml(request, AutoReleaseCheckDataReceivedRequest.class);
        jmsMessageSenderService.sendTriggerMessage(ProcessorTriggerTypeEnum.ACTION_AUTO_RELEASE_CHECK_DATA_RECEIVED, messageText);

    }

    @Override
    public void triggerEvaluateCriteria(Long autoReleaseEnrolmentId) {

        AutoReleaseEvaluateCriteriaRequest request = new AutoReleaseEvaluateCriteriaRequest();
        request.setAutoReleaseEnrolmentId(autoReleaseEnrolmentId);
        String messageText = VbsUtil.convertJaxbToXml(request, AutoReleaseEvaluateCriteriaRequest.class);
        jmsMessageSenderService.sendTriggerMessage(ProcessorTriggerTypeEnum.ACTION_AUTO_RELEASE_EVALUATE_CRITERIA, messageText);

    }

    @Override
    public void triggerCheckCriteriaComplete(Long autoReleaseRunId) {

        AutoReleaseCheckCriteriaCompleteRequest request = new AutoReleaseCheckCriteriaCompleteRequest();
        request.setAutoReleaseRunId(autoReleaseRunId);
        String messageText = VbsUtil.convertJaxbToXml(request, AutoReleaseCheckCriteriaCompleteRequest.class);
        jmsMessageSenderService.sendTriggerMessage(ProcessorTriggerTypeEnum.ACTION_AUTO_RELEASE_CHECK_CRITERIA_COMPLETE, messageText);

    }

    @Override
    public void triggerFinalizeAutoReleaseRun(Long runId, String userId) {
        AutoReleaseTriggerFinalizeRequest request = new AutoReleaseTriggerFinalizeRequest();
        request.setAutoReleaseRunId(runId);
        request.setUserId(userId);
        String messageText = VbsUtil.convertJaxbToXml(request, AutoReleaseTriggerFinalizeRequest.class);
        jmsMessageSenderService.sendTriggerMessage(ProcessorTriggerTypeEnum.ACTION_AUTO_RELEASE_FINALIZE, messageText);
    }

    @Override
    public void triggerCreateRelease(AutoReleaseCreateReleaseRequest request) {
        String messageText = VbsUtil.convertJaxbToXml(request, AutoReleaseCreateReleaseRequest.class);
        jmsMessageSenderService.sendTriggerMessage(ProcessorTriggerTypeEnum.ACTION_AUTO_RELEASE_CREATE_RELEASE, messageText);
    }

    @Override
    public void triggerCheckCreateReleasesCompleted(Long autoReleaseRunId, int numberOfReleases, String userId) {
        AutoReleaseCheckCreateReleasesCompleteRequest request = new AutoReleaseCheckCreateReleasesCompleteRequest();
        request.setAutoReleaseRunId(autoReleaseRunId);
        request.setNumberOfReleases(numberOfReleases);
        request.setUserId(userId);
        String messageText = VbsUtil.convertJaxbToXml(request, AutoReleaseCheckCreateReleasesCompleteRequest.class);
        jmsMessageSenderService.sendTriggerMessage(ProcessorTriggerTypeEnum.ACTION_AUTO_RELEASE_CREATE_RELEASE, messageText);
    }

    @Override
    public void triggerInterestCalculationForDate(LocalDate date) {
        CreateInterestCalculation request = new CreateInterestCalculation();
        request.setInterestDate(date.toString());
        String messageText = VbsUtil.convertJaxbToXml(request, CreateInterestCalculation.class);
        jmsMessageSenderService.sendTriggerMessage(ProcessorTriggerTypeEnum.ACTION_CREATE_INTEREST_CALCULATION, messageText);
    }

    @Override
    public void triggerAllocationRequestEmail(Long securityId) {
        AllocationEmailRequest request = new AllocationEmailRequest();
        request.setSecurityId(securityId);
        String messageText = VbsUtil.convertJaxbToXml(request, AllocationEmailRequest.class);
        jmsMessageSenderService.sendTriggerMessage(ProcessorTriggerTypeEnum.SEND_ALLOCATION_EMAIL, messageText);
    }

    @Override
    public void triggerCEBlanketEmail(CEEnrolmentDecisionDTO decisionDTO, UserDTO userDTO) {
        CeBlanketEmailRequest request = new CeBlanketEmailRequest();
        request.setEnrolmentNumber(decisionDTO.getEnrolmentNumber());
        request.setVbName(decisionDTO.getVbName());
        request.setVbNumber(decisionDTO.getVbNumber());
        for(SecurityDTO sec : decisionDTO.getBlanketSecurities()){
            request.getPoolId().add(sec.getPoolId());
        }
        request.setDateAdded(DateUtil.getDateFormattedShortTime(LocalDateTime.now()));
        request.setUserName(userDTO.getUserId().toUpperCase());
        String messageText = VbsUtil.convertJaxbToXml(request, CeBlanketEmailRequest.class);
        jmsMessageSenderService.sendTriggerMessage(ProcessorTriggerTypeEnum.SEND_CE_BLANKET_EMAIL, messageText);
    }
}
