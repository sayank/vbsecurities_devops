/* 
 * 
 * FmsQueueMDB.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.interceptor.Interceptors;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.tarion.vbs.common.constants.JMSConstants;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.common.util.LoggerUtil;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;

/**
 * Listens for JMS message response from FMS 
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @date 2018-11-20
 * @version 1.0
 */
@Resource(name=JMSConstants.VBS_XA_TOPIC_CF_JNDI, type=javax.jms.ConnectionFactory.class)
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
@MessageDriven(mappedName = "jms/FmsARItemTopic",
activationConfig = {
@ActivationConfigProperty(propertyName  = "destinationType",
propertyValue = "javax.jms.Topic"),
@ActivationConfigProperty(propertyName  = "subscriptionDurability",
propertyValue = "Durable"),
@ActivationConfigProperty(propertyName  = "connectionFactoryJndiName",
propertyValue = JMSConstants.VBS_XA_TOPIC_CF_JNDI), // External JNDI Name
@ActivationConfigProperty(propertyName  = "destinationJndiName",
propertyValue = "jms/fmsARItemTopic"), // Ext. JNDI Name
@ActivationConfigProperty(propertyName  = "topicMessagesDistributionMode",
propertyValue = "One-Copy-Per-Application")
})
public class FmsTopicMDB implements MessageListener {
	@Resource
	public MessageDrivenContext mdc;
	
	@EJB
	private FmsQueueResponseProcessor fmsQueueResponseProcessor;


	@Override
	public void onMessage(Message inMessage) {
		String inCaseMsgXML = null;
		try {
			if (inMessage instanceof TextMessage) {
				// skip any ping messages
				String messageType = inMessage.getStringProperty(JMSConstants.MESSAGE_TYPE);
				if (JMSConstants.PING.equalsIgnoreCase(messageType)) {
					String destNodes = inMessage.getStringProperty(JMSConstants.DESTINATION_NODES);
					LoggerUtil.logInfo(FmsTopicMDB.class, "Ping received from FMS for Destination Node[" + destNodes + "]");
					return;
				}
								
				TextMessage txtMsg = (TextMessage) inMessage;
				inCaseMsgXML = txtMsg.getText();
				if (inCaseMsgXML == null) {
					throw new VbsRuntimeException("Missing FMS Response Object");
				}else if(inCaseMsgXML.isEmpty()){
					LoggerUtil.logError(FmsTopicMDB.class, "Received Empty message on FmsTopicMDB");
				}
				fmsQueueResponseProcessor.processReceivedFmsXml(inCaseMsgXML);
			} else {
				String warnMsg = "Received JMS Message from FMS that is not Text Message: " + inMessage.getClass().getName();
				LoggerUtil.logWarn(FmsTopicMDB.class, warnMsg);
				throw new VbsRuntimeException(warnMsg);
			}
		} catch (JMSException e) {
			throw new VbsRuntimeException(e.getMessage());
		} catch (VbsRuntimeException pie) {
			StringBuilder sb = new StringBuilder("FmsQueueMDB error for: [");
			sb.append(inCaseMsgXML).append(']');
			LoggerUtil.logError(FmsTopicMDB.class, sb.toString(), pie);
			throw new EJBException(pie);
		}
	}
	
}

