/* 
 * 
 * InternalJmsMessageSenderService.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.processor;

import com.tarion.vbs.common.enums.ProcessorTriggerTypeEnum;

import javax.ejb.Local;

/**
 * This class is used to post a message to a local Internal JMS queue for the triggering processing
 * 
 */
@Local
public interface InternalJmsMessageSenderService {
	void sendTriggerMessage(ProcessorTriggerTypeEnum triggerType, String xmlMessageText);
}