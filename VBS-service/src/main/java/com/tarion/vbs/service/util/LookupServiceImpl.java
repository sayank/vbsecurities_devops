/* 
 * 
 * LookupServiceImpl.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import com.tarion.vbs.common.constants.VbsConstants;
import com.tarion.vbs.common.dto.NameDescriptionDTO;
import com.tarion.vbs.common.dto.alert.AlertTypeDTO;
import com.tarion.vbs.common.dto.financialinstitution.FinancialInstitutionTypeDTO;
import com.tarion.vbs.common.dto.release.ReleaseTypeDTO;
import com.tarion.vbs.common.dto.security.*;
import com.tarion.vbs.common.exceptions.VbsDirectoryEnvironmentException;
import com.tarion.vbs.common.exceptions.VbsRuntimeException;
import com.tarion.vbs.dao.util.EjbLoggingInterceptor;
import com.tarion.vbs.dao.util.LookupDAO;
import com.tarion.vbs.orm.entity.alert.AlertTypeEntity;
import com.tarion.vbs.orm.entity.contact.EscrowLicenseApplicationStatusEntity;
import com.tarion.vbs.orm.entity.contact.LicenseStatusEntity;
import com.tarion.vbs.orm.entity.correspondence.CorrespondenceTemplateEntity;
import com.tarion.vbs.orm.entity.financialinstitution.FinancialInstitutionTypeEntity;
import com.tarion.vbs.orm.entity.release.ReasonEntity;
import com.tarion.vbs.orm.entity.release.RejectReasonEntity;
import com.tarion.vbs.orm.entity.release.ReleaseTypeEntity;
import com.tarion.vbs.orm.entity.security.*;
import com.tarion.vbs.service.userprofile.UserService;
import org.apache.commons.beanutils.BeanUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Lookup Service provides operations to fetch the data for all lookup tables
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-10-09
 * @version 1.0
 */
@Stateless(mappedName = "ejb/LookupService")
@Interceptors(EjbLoggingInterceptor.class) // Entry and Exit of all public methods will be logged
public class LookupServiceImpl implements LookupService, LookupServiceRemote {

	@EJB
	private LookupDAO lookupDAO;
	@EJB
	private PropertyService propertyService;
	@EJB
    private UserService userService;

	private Map<String, Object> uiSetupMap;

    @Override
	public Map<String, Object> getUiSetupMap(){
		if(this.uiSetupMap == null){
			this.uiSetupMap = new HashMap<>();

			uiSetupMap.put("financialInstitutionTypes", transformFinancialInstitutionTypes(this.lookupDAO.getAllFinancialInstitutionTypes()));
			uiSetupMap.put("releaseReasons", transformReleaseReasons(this.lookupDAO.getAllReasons()));
			uiSetupMap.put("releaseRejectReasons", transformRejectReasons(this.lookupDAO.getAllRejectReasons()));
			uiSetupMap.put("alertTypes", transformAlertTypes(this.lookupDAO.getAllAlertTypes()));
			uiSetupMap.put("escrowLicenseApplicationStatuses", transformEscrowStatuses(this.lookupDAO.getAllEscrowLicenseApplicationStatuses()));
			uiSetupMap.put("identityTypes", transformIdentityTypes(this.lookupDAO.getAllIdentityTypes()));
			uiSetupMap.put("releaseTypes", transformReleaseTypes(this.lookupDAO.getAllReleaseTypes()));
			uiSetupMap.put("licenceStatuses", transformLicenseStatus(this.lookupDAO.getAllLicenseStatuses()));
			uiSetupMap.put("securityDeposits", transformSecurityDeposit(this.lookupDAO.getAllSecurityDeposit()));
			uiSetupMap.put("securityPurposes", transformSecurityPurpose(this.lookupDAO.getAllSecurityPurposes()));
			uiSetupMap.put("securityStatuses", transformSecurityStatus(this.lookupDAO.getAllSecurityStatuses()));
			uiSetupMap.put("securityTypes", transformSecurityType(this.lookupDAO.getAllSecurityTypes()));
			uiSetupMap.put("vbDeleteReasons", transformVbDelete(this.lookupDAO.getAllVbDeleteReasons()));
            uiSetupMap.put("correspondenceTemplates", transformTemplates(this.lookupDAO.getAllTemplateTypes()));

			uiSetupMap.put("contentNavigatorUrl", propertyService.getValue(VbsConstants.CONTENT_NAVIGATOR_URL));
            uiSetupMap.put("contentNavigatorFIUrl", propertyService.getValue(VbsConstants.CONTENT_NAVIGATOR_FI_DOCUMENTS_URL));
			uiSetupMap.put("contentNavigatorSecurityReleaseUrl", propertyService.getValue(VbsConstants.CONTENT_NAVIGATOR_SECURITY_RELEASE_URL));
			uiSetupMap.put("iaActivitiesUrl", propertyService.getValue(VbsConstants.IA_ACTIVITIES_URL));
			uiSetupMap.put("withdrawUrl", propertyService.getValue(VbsConstants.WITHDRAW_URL));
			uiSetupMap.put("financialTransactionUrl", propertyService.getValue(VbsConstants.FINANCIAL_TRANSACTION_URL));
			uiSetupMap.put("financialHistoryUrl", propertyService.getValue(VbsConstants.TRANSACTION_HISTORY_URL));

            uiSetupMap.put("crmCompanyUrl", propertyService.getValue(VbsConstants.CRM_COMPANY_URL));
            uiSetupMap.put("crmEnrolmentUrl", propertyService.getValue(VbsConstants.CRM_ENROLMENT_URL));
            uiSetupMap.put("crmWarrantyServiceOrderUrl", propertyService.getValue(VbsConstants.CRM_WARRANTY_SERVICE_ORDER_URL));
            uiSetupMap.put("crmCreateCompanyUrl", propertyService.getValue(VbsConstants.CRM_CREATE_COMPANY_URL));
            uiSetupMap.put("ceViewerUrl", propertyService.getValue(VbsConstants.CE_VIEWER_URL));
            uiSetupMap.put("viewer360Url", propertyService.getValue(VbsConstants.VIEWER_360_URL));
            uiSetupMap.put("apcsdVoucherUrl", propertyService.getValue(VbsConstants.APCSD_VOUCHER_URL));
            uiSetupMap.put("finalApprovalManagerDefault", propertyService.getValue(VbsConstants.FINAL_APPROVAL_DEFAULT));
            uiSetupMap.put("vpManagerDefault", propertyService.getValue(VbsConstants.FINAL_APPROVAL_MANAGER_RELEASE_HIGH));
            uiSetupMap.put("crmCreatePersonUrl", propertyService.getValue(VbsConstants.CREATE_PERSON_CONTACT_LINK));
            uiSetupMap.put("allSecuritiesUnderVB", propertyService.getValue(VbsConstants.ALL_SECURITIES_UNDER_VB));
            uiSetupMap.put("summaryBySecurityInstrumentUrl", propertyService.getValue(VbsConstants.SUMMARY_BY_SECURITY_URL));
            uiSetupMap.put("refundsUrl", propertyService.getValue(VbsConstants.CASH_SECURITY_REFUNDS_URL));
            
            uiSetupMap.put("flagUrl", propertyService.getValue(VbsConstants.CRM_FLAG_URL));
            uiSetupMap.put("yellowStickyUrl", propertyService.getValue(VbsConstants.CRM_YELLOW_STICKY_URL));

            uiSetupMap.put("securityReleaseBase", propertyService.getValue(VbsConstants.DM_SECURITY_RELEASE_BASE));

            try {
                uiSetupMap.put("managersList", userService.getUsersByRoleName(VbsConstants.MANAGER_ROLE_NAME));
            }catch(VbsDirectoryEnvironmentException e){
                throw new VbsRuntimeException("Couldn't populate managerList.", e);
            }
		}
		return this.uiSetupMap;
	}

	private List<SecurityTypeDTO> transformSecurityType(List<SecurityTypeEntity> allSecurityTypes) {
        return allSecurityTypes.stream().map(e -> {
            SecurityTypeDTO d = new SecurityTypeDTO();
            d.setDescription(e.getDescription());
            d.setId(e.getId());
            d.setName(e.getName());
            return d;
        }).collect(Collectors.toList());
	}

    private List<VbDeleteReasonDTO> transformVbDelete(List<VbDeleteReasonEntity> allVbDeleteReasons) {
        return allVbDeleteReasons.stream().map(e -> {
            VbDeleteReasonDTO d = new VbDeleteReasonDTO();
            d.setDescription(e.getDescription());
            d.setId(e.getId());
            d.setName(e.getName());
            return d;
        }).collect(Collectors.toList());
    }

    private List<SecurityStatusDTO> transformSecurityStatus(List<SecurityStatusEntity> allSecurityStatuses) {
        return allSecurityStatuses.stream().map(e -> {
            SecurityStatusDTO d = new SecurityStatusDTO();
            d.setDescription(e.getDescription());
            d.setId(e.getId());
            d.setName(e.getName());
            return d;
        }).collect(Collectors.toList());
	}

	private List<SecurityPurposeDTO> transformSecurityPurpose(List<SecurityPurposeEntity> allSecurityPurposes) {
        return allSecurityPurposes.stream().map(e -> {
            return transformSecurityPurpose(e);
        }).collect(Collectors.toList());
	}
	
	public SecurityPurposeDTO transformSecurityPurpose(SecurityPurposeEntity e) {
		SecurityPurposeDTO d = new SecurityPurposeDTO();
        d.setDescription(e.getDescription());
        d.setId(e.getId());
        d.setName(e.getName());
        return d;
	}

	private List<SecurityDepositDTO> transformSecurityDeposit(List<SecurityDepositEntity> allSecurityDeposit) {
        return allSecurityDeposit.stream().map(e -> {
            SecurityDepositDTO d = new SecurityDepositDTO();
            d.setDescription(e.getDescription());
            d.setId(e.getId());
            d.setName(e.getName());
            return d;
        }).collect(Collectors.toList());
	}

	private List<LicenseStatusDTO> transformLicenseStatus(List<LicenseStatusEntity> allLicenseStatuses) {
        return allLicenseStatuses.stream().map(e -> {
            LicenseStatusDTO d = new LicenseStatusDTO();
            d.setDescription(e.getDescription());
            d.setId(e.getId());
            d.setName(e.getName());
            return d;
        }).collect(Collectors.toList());
	}

	private List<ReleaseTypeDTO> transformReleaseTypes(List<ReleaseTypeEntity> allReleaseTypes) {
        return allReleaseTypes.stream().map(e -> {
            ReleaseTypeDTO d = new ReleaseTypeDTO();
            d.setDescription(e.getDescription());
            d.setId(e.getId());
            d.setName(e.getName());
            return d;
        }).collect(Collectors.toList());
	}

	private List<IdentityTypeDTO> transformIdentityTypes(List<IdentityTypeEntity> allIdentityTypes) {
        return allIdentityTypes.stream().map(e -> {
            IdentityTypeDTO d = new IdentityTypeDTO();
            d.setDescription(e.getDescription());
            d.setId(e.getId());
            d.setName(e.getName());
            return d;
        }).collect(Collectors.toList());
	}

	private List<NameDescriptionDTO> transformEscrowStatuses(List<EscrowLicenseApplicationStatusEntity> allEscrowLicenseApplicationStatuses) {
        return allEscrowLicenseApplicationStatuses.stream().map(this::transform).collect(Collectors.toList());
	}

	private List<AlertTypeDTO> transformAlertTypes(List<AlertTypeEntity> allAlertTypes) {
        return allAlertTypes.stream().map(e -> {
            AlertTypeDTO d = new AlertTypeDTO();
            d.setDescription(e.getDescription());
            d.setId(e.getId());
            d.setName(e.getName());
            return d;
        }).collect(Collectors.toList());
	}

	private List<NameDescriptionDTO> transformRejectReasons(List<RejectReasonEntity> allRejectReasons) {
        return allRejectReasons.stream().map(this::transform).collect(Collectors.toList());
	}

	private List<NameDescriptionDTO> transformReleaseReasons(List<ReasonEntity> allReasons) {
        return allReasons.stream().map(this::transform).collect(Collectors.toList());
	}

	private List<NameDescriptionDTO> transformTemplates(List<CorrespondenceTemplateEntity> templates){
        return templates.stream().map(this::transform).collect(Collectors.toList());
    }

	private List<FinancialInstitutionTypeDTO> transformFinancialInstitutionTypes(List<FinancialInstitutionTypeEntity> entityList){
		return entityList.stream().map(e -> {
			FinancialInstitutionTypeDTO d = new FinancialInstitutionTypeDTO();
			d.setDescription(e.getDescription());
			d.setId(e.getId());
			d.setName(e.getName());
			return d;
		}).collect(Collectors.toList());
	}

    private NameDescriptionDTO transform(Object entity) {
        NameDescriptionDTO d = new NameDescriptionDTO();
        try {
            BeanUtils.copyProperties(d, entity);
        } catch (IllegalAccessException| InvocationTargetException e) {
            //nothing
        }
        return d;
    }

}
