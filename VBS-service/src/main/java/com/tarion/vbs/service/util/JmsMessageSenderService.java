/* 
 * 
 * JmsMessageSenderService.java
 *
 * Copyright (c) 2018 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.vbs.service.util;

import javax.ejb.Local;

import com.tarion.vbs.orm.entity.JmsTransactionLogEntity;

/**
 * This class is used to post a message to a JMS queue
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2018-11-20
 * @version 1.0
 */
@Local
public interface JmsMessageSenderService {
	
	public void postCrmRequestQueue(String xmlMessageText, String requestClassName);
	public void postFmsRequestQueue(String xmlMessageText, String requestClassName);
	public void postFmsRequestQueueInterestMessage(String xmlMessageText, String requestClassName);
	public void postBsaResponseQueue(String xmlMessageText);

	public JmsTransactionLogEntity insertTransactionLog(String trackingNumber, String xmlMessageText);
    public JmsTransactionLogEntity insertTransactionLog(String trackingNumber, String requestMessageText, String responseMessageText);
    public void insertInterestTransactionLog(String trackingNumber, String xmlMessageText);

    void postTipCorrespondenceMessage(String xmlMessageText);
}